const rollup = require('rollup');
const nodeResolve = require('rollup-plugin-node-resolve');
const babel = require('rollup-plugin-babel');
const commonjs = require('rollup-plugin-commonjs');
const replace = require('rollup-plugin-replace');
const json = require('rollup-plugin-json');

const postcss = require('postcss');
const postcssCssnext = require('postcss-cssnext');
const postcssImport = require('postcss-import');

const fs = require('fs');
const colors = require('colors/safe');

function buildCss(version, wss) {
  return function() {
    var to;
    var from;

    if (version === 'old') {
      from = 'assets/css/old/index.css';
      to = 'public/css/index.css';
    } else if (version === 'new') {
      from = 'assets/css/index.css';
      to = 'public/css/bundle.css';
    }

    const css = fs.readFileSync(from);
    postcss([ postcssImport, postcssCssnext ])
    .process(css, { from: from, to: to })
    .then(result => {
      log(`Writing CSS bundle [${version}]`);
      fs.writeFileSync(to, result.css);
      // TODO: write file hash and compare on startup

      if (result.map) {
        fs.writeFileSync(to + '.map', result.map);
      }

      log(`CSS Bundle Written [${version}]`);
    })
    .then(reload('css', wss))
    .catch(err => {
      log(err);
    });
  };
}

function buildJs(wss) {
  return function() {
    // On JS file changes, Rollup bundle
    log('Rolling JS up.');

    var cache;

    try {
      log('Attempting to load previous cache.', { level: 'debug' });
      const start = Date.now();
      cache = JSON.parse(fs.readFileSync('bundle_cache.json'));
      log(`Previous cache loaded in ${(Date.now() - start)/1000} secs.`, { level: 'debug' });
    } catch(e) {
      log('Previous cache not found.', { level: 'debug' });
    }

    rollup.rollup({
      entry: 'assets/js/App.js',
      onwarn: message => log(`Rollup warning: ${message}`, { level: 'warning' }),
      cache: cache,
      plugins: [
        nodeResolve({
          main: true,
          jsnext: true,
          preferBuiltins: true,
          browser: true
        }),
        commonjs(),
        json({
          include: 'charmming/urls.json'
        }),
        babel(),
        replace({
          'process.env.NODE_ENV': JSON.stringify('development')
        }),
      ],
    }).then(bundle => {
      // Save bundle in cache for incremental bundling later
      // Dramatically speeds up bundle creation
      fs.writeFileSync('bundle_cache.json', JSON.stringify(bundle));

      bundle.write({
        format: 'cjs',
        moduleName: 'charmming2',
        dest: 'public/js/bundle.js',
      }).then(() => {
        log('JS Bundle written');
      }).then(reload('js', wss));
    }).catch(e => {
      log(e.stack);

      const stack = e.stack.replace(/\[[0-9]{0,2}m/g, '');
      sendError(stack, wss)();
    });
  };
}

function createDirs() {
  try {
    fs.mkdirSync('public', 0o755);
  } catch (e) {
    // Folder exists, disregard
  }

  for (var name of [ 'js', 'css' ]) {
    try {
      fs.mkdirSync(`public/${name}`, 0o755);
    } catch (e) {
      // Folder exists, disregard
    }
  }
}

// Send reload command to front end
function reload(type, _wss) {
  return function(__wss) {
    __wss.clients.forEach((client, i) => {
      log(`Client ${i+1} reloading: reloading ${type}`);
      client.send(JSON.stringify({ reload: type }));
    });
  }.bind(this, _wss);
}

// Send error message to front end
function sendError(message, wss) {
  return function() {
    wss.clients.forEach((client, i) => {
      log(`Client ${i+1}: sending error message`);
      client.send(JSON.stringify({ error: true, message: message }));
    });
  };
}

function log(message, options) {
  const allowedLevels = {
    critical: 'red',
    warning: 'magenta',
    info: 'blue',
    debug: 'yellow'
  };

  if (options == null) {
    options = {};
  }

  if (options.level == null) {
    options.level = 'info';
  }

  if (!allowedLevels[options.level]) {
    options.level = 'info';
  }

  if (options.name == null) {
    options.name = 'Hot Swap';
  }

  const level = `${colors[allowedLevels[options.level]](options.level)}`;

  console.log(`[${new Date().toISOString()}] (${level}) ${options.name}: ${message}`);
}

module.exports = {
  buildCss: buildCss,
  buildJs: buildJs,
  createDirs: createDirs,
  log: log
};
