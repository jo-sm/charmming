# Development of CHARMMing

Before you begin development, follow the [installation instructions](INSTALLATION.md). Once you have the Docker image available, you can start the application using `start.sh`. `start.sh` will handle any Docker configuration that's necessary to get your code changes to appear in the container, as well as some other development configurations.

## Linting

`charmming` makes use of various linters to ensure that code follows sane guidelines; we use `eslint` for Javascript, `stylelint` for CSS, and a combination of `flake8` and `pylint` for Python. Linting is mandatory for merge requests and any code added must pass linting. In addition to dedicated linters, EditorConfig is used to ensure all files are consistently spaced.

Note that Vim usage assumes `syntastic` usage. If you don't use `syntastic`, please check the respective documentation for the linters for alternative plugin information.

Note: Every merge request must pass linting specs. This is enforced on the CI.

### Eslint

[`eslint_d`](https://github.com/mantoni/eslint_d.js), which is a faster version of `eslint`, has already been installed during Node.js package installation. Here are a few editor plugins:

Sublime Text: https://github.com/roadhump/SublimeLinter-contrib-eslint_d
vim: Add the following to your .vimrc:

```
let g:syntastic_javascript_checkers = ['eslint']
let g:syntastic_javascript_eslint_exec = 'eslint_d'
```

### Stylelint

As with `eslint_d`, `stylelint_d` has already been installed during Node.js package installation. Here's a few editor plugins:

Sublime Text: https://github.com/jo-sm/SublimeLinter-contrib-stylelint_d
vim: Add the following to your .vimrc:

```
let g:syntastic_css_checkers = ['stylelint']
let g:syntastic_css_stylelint_exec = 'stylelint_d'
```
### Python Linting

Each linter can be installed with `pip`:

```
> pip install flake8
> pip install pylint
```

Here are some editor plugins for each:

*Flake8*

Sublime Text: https://github.com/SublimeLinter/SublimeLinter-flake8

*PyLint*

Sublime Text: https://github.com/SublimeLinter/SublimeLinter-pylint

*Vim*

For both, you can add them to your `.vimrc`:

```
let g:syntastic_python_checkers = [ 'flake8', 'pylint']
let g:syntastic_python_flake8_exec = 'flake8'
let g:syntastic_python_pylint_exec = 'pylint'
```

### EditorConfig

Please see [EditorConfig Plugins](http://editorconfig.org/#download) for a list of plugins for various editors.
