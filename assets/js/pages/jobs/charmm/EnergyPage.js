import React from 'react';
import Form from '../../../components/Form';
import Button from '../../../components/Button';
import RadioInput from '../../../components/RadioInput';

export default class EnergyPage extends React.Component {
  render() {
    const { submit } = this.props;

    return <Form onSubmit={submit}>
      <h1>Energy Page!</h1>
      <RadioInput name='implicit_solvation' label='No Implicit Solvent' value='none' />
      <RadioInput name='implicit_solvation' label='Solvate implicitly using GBMV' value='gbmv' />
      <RadioInput name='implicit_solvation' label='Solvate implicitly using SCPISM' value='scpism' />
      <Button type='submit'>Submit</Button>
    </Form>;
  }
}
