import PageComponent from '../components/PageComponent';
import Button from '../components/Button';
import Form from '../components/Form';
import Input from '../components/Input';
import { alphanum, notEmpty } from '../validators';
import { dispatch } from '../store';

import get from 'lodash.get';

export default class LoginPage extends PageComponent {
  static title = 'Login';
  static watch = [ 'loading' ];

  submit(data) {
    dispatch('login', {
      username: data.username,
      password: data.password
    });
  }

  render() {
    const { loading, error } = this.props;
    const loginLoading = get(loading, 'login') === true;
    const disabled = loginLoading === true;

    return <div>
      {
        error && <span className='error'>{ error }</span>
      }

      <Form center name='login' onSubmit={::this.submit}>
        <Input type='text' name='username' disabled={disabled} placeholder='Username' validator={alphanum} errorText='Enter a valid username (a-z, 0-9)' />
        <Input type='text' name='password' password disabled={disabled} placeholder='Password' validator={notEmpty} errorText='Enter a password' />
        <Button green type='submit' disabled={disabled}>Login</Button>
      </Form>
    </div>;
  }
}
