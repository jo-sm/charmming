import PageComponent from '../components/PageComponent';
import Viewer from '../components/Viewer';
import Button from '../components/Button';
import SectionHolder from '../components/SectionHolder';
import VerticalSection from '../components/VerticalSection';
import PageContainer from '../components/PageContainer';
import JobPageContainer from '../components/JobPageContainer';

import { request } from '../utils/request';

export default class StructurePage extends PageComponent {
  static title = 'Structure';

  constructor(...args) {
    super(...args);

    const { meta } = this.props;
    const { id } = meta;

    this.state = {};

    this.requestStructure(id);
  }

  componentDidUpdate() {
    const { meta } = this.props;

    if (!meta) {
      return;
    }

    const id = meta.id;

    console.log(`Testing if StructurePage component should update (prev ${this.prevId}, cur ${id})`);

    if (!this.prevId) {
      this.prevId = id;
    } else if (this.prevId === id) {
      return;
    } else {
      this.prevId = id;
    }

    console.log(`Requesting new structure ${id}`);
    this.requestStructure(id);
  }

  requestStructure(id) {
    request('structure', {
      id
    }, {}).then(d => {
      this.setState({
        structure: d.structure
      });
    });
  }

  toggleSegment(name) {
    return () => {
      const viewer = this.refs.viewer;

      viewer.toggleSegment(name);
    };
  }

  displayCreateChildPage(status) {
    return () => {
      this.setState({
        createChild: status,
        job: null,
      });
    };
  }

  displayEnergyPage(status) {
    return () => {
      this.setState({
        job: {
          display: status,
          program: 'charmm',
          job: 'energy'
        },
        createChild: false
      });
    };
  }

  render() {
    const { structure, createChild, job } = this.state;

    if (!structure) {
      return null;
    }

    return <SectionHolder>
      <VerticalSection>
        <h1>Structure: {structure.name}</h1>
        <Viewer id={structure.id} ref='viewer' />
      </VerticalSection>
      <VerticalSection>
        {
          job && <JobPageContainer structure={structure} program={job.program} job={job.job} />
        }
        { createChild && <PageContainer
            page='CreateChild'
            extraProps={{
              structure,
              viewer: this.refs.viewer,
              close: ::this.displayCreateChildPage(false)
            }}
          />
        }
        { !createChild && !job &&
          <span>
          <div>
            <Button green onClick={::this.displayCreateChildPage(true)}>Create Child</Button>
            <Button blue onClick={::this.displayEnergyPage(true)}>Calculate Energy</Button>
          </div>
          <table>
            <thead>
              <tr>
                <th>Segment Name</th>
                <th>First Patch</th>
                <th>Last Patch</th>
              </tr>
            </thead>
            <tbody>
              {
                structure.segments.map(segment => {
                  return <tr key={segment.id} onClick={::this.toggleSegment(segment.name)}>
                    <td>{segment.name}</td>
                    <td>{segment.firstPatch}</td>
                    <td>{segment.lastPatch}</td>
                  </tr>;
                })
              }
            </tbody>
          </table>
          </span>
        }
      </VerticalSection>
    </SectionHolder>;
  }
}
