import CreateChildPage from './CreateChildPage';
import FileuploadPage from './FileuploadPage';
import LoginPage from './LoginPage';
import MainPage from './MainPage';
import StructurePage from './StructurePage';

import * as jobs from './jobs';

export {
  CreateChildPage,
  FileuploadPage,
  LoginPage,
  MainPage,
  StructurePage,
  jobs,
};
