import PageComponent from '../components/PageComponent';

export default class MainPage extends PageComponent {
  static title = 'Dashboard';
  static watch = [ 'profile' ];

  render() {
    const { profile } = this.props;

    return <div><strong>Welcome back, { profile.username }!</strong> Let's continue where you left off...</div>;
  }
}
