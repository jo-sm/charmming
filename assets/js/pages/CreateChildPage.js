import PageComponent from '../components/PageComponent';
import Button from '../components/Button';

export default class CreateChildPage extends PageComponent {
  constructor(props, ...args) {
    super(props, ...args);

    props.viewer.displayNone();
  }

  toggleSegment(name) {
    return () => {
      const { viewer } = this.props;
      viewer.toggleSegment(name, { displayNone: true });
    };
  }

  render() {
    const { close, structure } = this.props;

    return <div>
      <p>
        <strong>What is a Child Structure?</strong>
      </p>
      <p>
        A Child Structure is a Structure that contains some or all of the segments in an original uploaded Structure, with additional modifications. For example, you may want to change the first or last patch of a segment, or modify the protonation states of specific titratable residues.
      </p>

      <p>
      On the left, the PDB will display the segments you select.
      </p>

      <table>
        <thead>
          <tr>
            <th>Segment Name</th>
            <th>First Patch</th>
            <th>Last Patch</th>
          </tr>
        </thead>
        <tbody>
          {
            structure.segments.map(segment => {
              return <tr key={segment.id} onClick={::this.toggleSegment(segment.name)}>
                <td>{segment.name}</td>
                <td>{segment.firstPatch}</td>
                <td>{segment.lastPatch}</td>
              </tr>;
            })
          }
        </tbody>
      </table>

      <Button green>Create</Button>
      <Button onClick={close}>Cancel</Button>
    </div>;
  }
}
