// import React from 'react';

import PageComponent from '../components/PageComponent';
import Form from '../components/Form';
import FileInput from '../components/FileInput';
import Input from '../components/Input';
import Button from '../components/Button';
import ButtonGroup from '../components/ButtonGroup';
import ButtonGroupSection from '../components/ButtonGroupSection';

import { dispatch } from '../store';

export default class FileuploadPage extends PageComponent {
  static title = 'Structure Upload';
  static watch = [ 'loading' ];

  submit(data) {
    dispatch('upload', {
      file: data.file,
      pdb_id: data.pdb_id
    });
  }

  validatePdb(file) {
    if (!file) {
      return false;
    }

    // if (typeof file !== 'File') {
    //   return false;
    // }

    if (file.size > 1024 * 1024 * 10) {
      // 10MB
      return false;
    }

    return true;
  }

  render() {
    const { loading } = this.props;
    const isUploading = loading.uploading === true;

    console.log(`Is uploading? ${isUploading}`);

    return <div>
      <h1>Upload a Structure</h1>
      <Form name='fileupload' onSubmit={::this.submit}>
        <ButtonGroup>
          <Button green section='pdbid'>PDB ID</Button>
          <Button green section='upload'>Upload file</Button>
          <Button green>Custom Sequence</Button>
          <ButtonGroupSection name='pdbid'>
            <Input type='text' label='PDB ID' placeholder='1dna' name='pdb_id' disabled={isUploading} />
          </ButtonGroupSection>
          <ButtonGroupSection name='upload'>
            <span>Max file size is 10 MB</span>
            <FileInput name='file' validator={this.validatePdb} disabled={isUploading} />
          </ButtonGroupSection>
        </ButtonGroup>
        <Button green type='submit' disabled={isUploading}>Submit</Button>
      </Form>
    </div>;
  }
}
