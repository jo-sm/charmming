import { request } from '../utils/request';

export default async ({ fire, data }) => {
  await request('structure', {
    id: data.id
  }, {
    method: 'delete'
  });

  fire('removeStructure', data.id);
};
