import { request } from '../utils/request';

export default async ({ fire }) => {
  await request('logout');

  fire('logout');
  fire('navigateTo', {
    page: 'login'
  });
  document.dispatchEvent(new Event('teardownNotificationsWebsocket'));
};
