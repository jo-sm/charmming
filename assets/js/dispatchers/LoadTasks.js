import { request } from '../utils/request';

export default async ({ fire }) => {
  fire('toggleLoading', {
    tasks: true
  });

  const taskData = await request('tasks', {
    data: {
      limit: 5
    }
  });

  fire('setTasks', taskData.tasks);
  fire('toggleLoading', {
    tasks: false
  });
};
