import { request } from '../utils/request';

export default async ({ fire, data }) => {
  const { program, job, jobData } = data;

  const taskData = await request('job', { program: program, job: job }, { method: 'post', data: jobData });

  if (taskData.status.type === 'error') {
    fire('setError', {
      error: taskData.status.message
    });
  } else {
    fire('setTask', {
      id: taskData.task,
      status: 'queued',
      program,
      job
    });
  }
};
