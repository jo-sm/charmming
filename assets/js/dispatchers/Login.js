import { request } from '../utils/request';
import { getProfile } from '../utils/profile';

export default async ({ fire, data }) => {
  fire('toggleLoading', {
    login: true
  });
  fire('setError');

  let result;

  try {
    result = await request('login', {
      data: {
        username: data.username,
        password: data.password
      },
      method: 'POST'
    });
  } catch(e) {
    fire('setError', 'An error occurred. Try again.');
    fire('toggleLoading', {
      login: false
    });
    return;
  }

  const status = result.status;

  if (status.type === 'error') {
    fire('login', false);
    fire('setError', status.message);
  } else {
    const profile = await getProfile();

    fire('setProfile', profile);
    fire('navigateTo', {
      page: 'main'
    });
    fire('login', true);

    document.dispatchEvent(new Event('setupNotificationsWebsocket'));
  }

  fire('toggleLoading', {
    login: false
  });
};
