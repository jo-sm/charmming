import { request } from '../utils/request';

import set from 'lodash.set';

export default async ({ fire, data }) => {
  fire('toggleLoading', {
    structures: true
  });

  const structuresData = await request('structures', {
    data: {
      limit: 5
    }
  });

  fire('setStructures', structuresData.structures);
  fire('toggleLoading', {
    structures: false
  });
};
