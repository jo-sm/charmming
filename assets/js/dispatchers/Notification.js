import * as notifications from '../actions/notifications';
import { capitalize } from '../utils/string';

export default async ({ fire, data }) => {
  const notificationType = capitalize(data.type);
  const notificationData = data.data;

  if (!notificationType) {
    console.error('Notification type not supplied.');
    return;
  }

  if (!notifications[notificationType]) {
    console.error(`Notification type does not exist: ${notificationType}`);
  }

  fire(`notifications.${notificationType}`, notificationData);
};
