import { determinePageFromUrl } from '../utils/navigation';
import { getProfile, isLoggedIn } from '../utils/profile';

export default async ({ fire }) => {
  const loggedIn = isLoggedIn();

  fire('login', loggedIn);

  if (loggedIn) {
    const profile = await getProfile();
    const page = determinePageFromUrl(window.location.pathname);

    // Setup the Notification Websocket in the App
    document.dispatchEvent(new Event('setupNotificationsWebsocket'));

    fire('setProfile', profile);
    fire('navigateTo', {
      page: page.name,
      options: page.options
    });
  } else {
    fire('navigateTo', {
      page: 'login'
    });
  }
};
