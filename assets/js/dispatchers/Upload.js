import { request } from '../utils/request';

export default async ({ fire, data}) => {
  fire('toggleLoading', {
    uploading: true
  });
  fire('setError');

  const uploadData = await request('fileupload', {
    data: {
      file: data.file,
      pdb_id: data.pdb_id
    },
    method: 'POST'
  });

  const status = uploadData.status;

  if (status.type === 'error') {
    fire('setError', status.message);
  } else {
    fire('navigateTo', {
      page: 'structure',
      options: {
        id: uploadData.structure
      }
    });
  }

  fire('toggleLoading', {
    uploading: false
  });
};

