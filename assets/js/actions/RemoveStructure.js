import get from 'lodash.get';
import set from 'lodash.set';

export default (state, data) => {
  let structures = get(state, 'structures');

  if (!structures) {
    return state;
  }

  structures = structures.filter(structure => {
    return structure.id !== data;
  });

  set(state, 'structures', structures);

  return state;
};
