import { updateNavigationState } from '../utils/navigation';

export default (state, data) => {
  const { page } = data;
  let options = data.options;

  if (!options) {
    options = {};
  }

  options.replace = Boolean(options.replace);

  state = updateNavigationState(state, page, options);

  return state;
};
