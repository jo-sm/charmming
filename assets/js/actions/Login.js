export default (state, data) => {
  if (typeof data !== 'boolean') {
    return state;
  }

  state.loggedIn = data;

  return state;
};
