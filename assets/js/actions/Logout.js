export default (state) => {
  state.loggedIn = false;
  state.profile = null;

  return state;
};

