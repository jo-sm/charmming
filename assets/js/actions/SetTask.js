export default (state, data) => {
  if (!state.tasks) {
    state.tasks = [];
  }

  state.tasks.splice(0, 0, data);

  return state;
};
