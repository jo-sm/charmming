import get from 'lodash.get';
import set from 'lodash.set';

export default (state, data) => {
  if (!data) {
    return state;
  }

  if (!data.id) {
    console.error('UpdateTask called without task id given');
    return state;
  }

  if (!data.status) {
    console.error('UpdateTask called without updated task status given');
    return state;
  }

  const taskId = data.id;
  const taskStatus = data.status;

  if (!get(state, 'tasks')) {
    console.error('Tasks key does not exist in state');
    return state;
  }

  const task = state.tasks.find(t => t.id === taskId);

  if (!task) {
    console.error(`Task ${taskId} does not exist in state`);
    return state;
  }

  set(task, 'status', taskStatus);

  return state;
};
