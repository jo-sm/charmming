import { request } from '../utils/request';
import { updateNavigationState } from '../utils/navigation';

export default (state, data) => {
  return request('fileupload', {
    data: {
      file: data.file,
      pdb_id: data.pdb_id
    },
    method: 'POST'
  }).then(d => {
    const status = d.status;

    if (status.type === 'error') {
      console.error(status);
    } else {
      state = updateNavigationState(state, 'structure', { id: d.structure });
    }

    return Promise.resolve(state);
  });
};
