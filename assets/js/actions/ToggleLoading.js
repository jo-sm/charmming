export default (state, data) => {
  if (!state.loading) {
    state.loading = {};
  }

  for (const key in data) {
    const value = data[key];

    if (typeof value !== 'boolean') {
      continue;
    }

    state.loading[key] = value;
  }

  return state;
};
