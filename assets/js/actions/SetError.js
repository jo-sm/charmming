export default (state, data) => {
  if (data == null) {
    state.error = null;
  } else {
    state.error = data;
  }

  return state;
};
