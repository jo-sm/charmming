export function alphanum(value) {
  if (!value) {
    return false;
  }

  if (!/^[A-Za-z0-9]+$/.test(value)) {
    return false;
  }

  return true;
}

export function notEmpty(value) {
  return Boolean(value);
}
