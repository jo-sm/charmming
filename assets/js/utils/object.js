export function get(object, path) {
  const pathParts = path.split('.');
  let current = object;

  for(const part of pathParts) {
    if (current[part]) {
      current = current[part];
    } else {
      return null;
    }
  }

  return current;
}

export function set(object, path, value) {
  const pathParts = path.split('.');
  let current = object;
  let part;

  for(part of pathParts) {
    if (current[part]) {
      current = current[part];
    } else {
      current[part] = {};
    }
  }

  object[part] = value;

  return object;
}
