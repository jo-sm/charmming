import { getPageComponent } from './page';
import { getUrl } from './request';
import urls from '../../../charmming/urls.json';

export function updateNavigationState(state, page, meta = {}) {
  // We update the title using the title on the page component
  // We update to the next page in the browser

  const component = getPageComponent(page);

  if (!component) {
    console.error(`Cannot update naviation state: ${page} does not exist. Returning original state unmodified.`);
    return state;
  }

  const title = component.title + " | " || "";

  document.title = title + "CHARMMing";

  const url = getUrl(page, meta);

  if (!url) {
    console.error(`Could not determine URL for ${page} with meta ${meta}`);
    return state;
  }

  // Replacing happens when popstate occurs
  if (meta.replace) {
    history.replaceState({
      page,
      meta
    }, title, url);
  } else {
    history.pushState({
      page,
      meta
    }, title, url);
  }

  delete meta.replace;

  state.page = page;
  state.meta = meta;

  console.log(`Returning state from updateNavigationState`);

  return state;
}

export function determinePageFromUrl(pathname) {
  const pathParts = pathname.split('/').slice(1); // Slicing the initial ''

  if (pathParts.length === 1 && pathParts[0] === '') {
    return {
      name: 'main',
      options: {}
    };
  }

  const possiblePaths = urls.filter(url => {
    if (typeof url.pattern === 'string') {
      return url.pattern === pathParts[0];
    } else {
      const firstPart = url.pattern[0];

      if (typeof firstPart === 'string') {
        return firstPart.match(new RegExp(`^${pathParts[0]}`));
      } else if (typeof firstPart === 'object') {
        return new RegExp(firstPart.regex).test(pathParts[0]);
      }
    }
  });
  const getOptions = (pattern) => {
    const options = {};
    let valid = true;

    if (Array.isArray(pattern)) {
      // We need to remove '/' only pieces from `pattern`
      // to prevent incorrect indexing of `pathParts`
      pattern = pattern.filter(piece => {
        return piece !== '/';
      });

      for(const i in pattern) {
        const piece = pattern[i];

        if (!pathParts[i]) {
          valid = false;
          break;
        }

        if (typeof piece === 'string') {
          if (!(new RegExp(pathParts[i])).test(piece)) {
            valid = false;
            break;
          }
        } else if (typeof piece === 'object') {
          if (!(new RegExp(piece.regex)).test(pathParts[i])) {
            valid = false;
            break;
          }

          options[piece.name] = pathParts[i];
        }
      }
    }

    if (valid) {
      return options;
    } else {
      return null;
    }
  };

  if (possiblePaths.length === 0) {
    // TODO: Redirect to 404
    return {
      name: 'main',
      options: {}
    };
  } else if (possiblePaths.length === 1) {
    const path = possiblePaths[0];
    const options = getOptions(path.pattern);

    if (!options) {
      // The URL was invalid
      return {
        name: 'main',
        options: {}
      };
    } else {
      return {
        name: path.name,
        options
      };
    }
  } else {
    const results = possiblePaths.map(path => {
      const options = getOptions(path.pattern);

      if (options) {
        return {
          name: path.name,
          options
        };
      } else {
        return null;
      }
    }).reduce((memo, i) => {
      if (i) {
        memo.push(i);
      }

      return memo;
    }, []);

    if (results.length === 1) {
      return results[0];
    } else {
      return {
        name: 'main',
        options: {}
      };
    }
  }
}
