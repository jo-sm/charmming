export function capitalize(string) {
  if (!string) {
    return;
  }

  return string.substr(0,1).toUpperCase() + string.substr(1);
}
