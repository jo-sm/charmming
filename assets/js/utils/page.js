import * as pages from '../pages';

import { capitalize } from './string';

import get from 'lodash.get';

export function getPageComponent(name) {
  const pageParts = name.split('.');
  const pagePath = pageParts.slice(0, -1).join('.');

  const pageName = capitalize(pageParts.slice(-1)[0]);
  const fullPageComponentName = pagePath !== '' ? `${pagePath}.${pageName}Page` : `${pageName}Page`;
  const component = get(pages, fullPageComponentName);

  if (!component) {
    console.error(`Invalid page name: ${pageName}`);
    return null;
  }

  return component;
}

export function jobPageExists(program, job) {
  const jobName = capitalize(job);
  return Boolean(get(pages.jobs, `${program}.${jobName}Page`));
}
