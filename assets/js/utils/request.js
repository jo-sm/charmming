import urls from '../../../charmming/urls.json';

/*
  Requests some data using `fetch`.

  Defaults to GET but `option.method` can be set to non-GET.
  If sending data,
 */
export function request(urlName, keys = {}, options = {}) {
  // Check to see if we are given any keys
  if (arguments.length === 2) {
    options = keys;
    keys = {};
  }

  const url = getUrl(urlName, keys);
  const fetchOptions = {
    method: 'GET',
    headers: {
      'Accept': 'application/json'
    },
    credentials: 'same-origin'
  };

  if (options.method) {
    fetchOptions.method = options.method;
  }

  if (options.data) {
    if (fetchOptions.method !== 'GET') {
      fetchOptions.body = JSON.stringify(options.data);
    }

    fetchOptions.headers['Content-Type'] = 'application/json';
  }

  return fetch(url, fetchOptions)
    .then(checkStatus)
    .then(resp => {
      return resp.json();
    });
}
window.request = request;

function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  } else {
    var error = new Error(response.statusText);
    error.response = response;
    throw error;
  }
}

export function getUrl(name, options = {}) {
  let pathParts;
  let path;

  for(const urlData of urls) {
    if (urlData.name === name) {
      pathParts = urlData.pattern;
      break;
    }
  }

  if (!pathParts) {
    console.error(`Could not find ${name} in urls.json.`);
    return null;
  }

  if (typeof pathParts === 'string') {
    path = pathParts;
  } else {
    path = pathParts.reduce((memo, i) => {
      if (typeof i === 'string') {
        return memo + i;
      } else if (typeof i === 'object') {
        const partName = i.name;
        const value = options[partName];

        if (!value) {
          console.error(`Missing value for URL ${name}: ${partName}`);
        }

        const regex = new RegExp(i.regex);

        if (!regex.test(value)) {
          console.error(`Regex mismatch for URL ${name}: Part: ${partName} | regex: ${regex} | value: ${value}`);
        }

        return memo + value;
      } else {
        console.error(`Type of path piece is not valid for URL ${name}: ${i}`);

        return memo;
      }
    }, "");
  }

  return `/${path}`;
}
