import Cookies from "js-cookie";

import { request } from '../utils/request';

export function getProfile() {
  return request('user').then(data => {
    const user = data.user;
    const profile = {
      username: user.username,
      firstName: user.firstName,
      lastName: user.lastName
    };

    return profile;
  });
}

export function isLoggedIn() {
  return Boolean(Cookies.get('charmming-token'));
}
