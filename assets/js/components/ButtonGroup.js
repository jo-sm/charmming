import React from 'react';

export default class ButtonGroup extends React.Component {
  constructor(props, ...args) {
    super(props, ...args);

    const { children } = props;

    this.children = children.map(child => {
      if (child.type.name === 'Button') {
        // Create onClick events if none exists
        if (child.props.section) {
          if (!child.props.onClick) {
            const newChild = React.cloneElement(child, { onClick: ::this.setVisible(child.props.section) });

            return newChild;
          } else {
            return child;
          }
        } else {
          return child;
        }
      } else if (child.type.name === 'ButtonGroupSection') {
        // Don't do anything yet
        return child;
      } else {
        return null;
      }
    });

    this.buttons = children.filter(child => {
      return child.type.name === 'Button';
    });

    this.buttonGroupSections = children.filter(child => {
      return child.type.name === 'ButtonGroupSection';
    });
  }

  setVisible(sectionName) {
    return () => {
      this.children = this.children.map(child => {
        if (child.type.name === 'Button') {
          return child;
        } else if (child.type.name === 'ButtonGroupSection') {
          const name = child.props.name;

          if (name === sectionName) {
            return React.cloneElement(child, { visible: true });
          } else {
            return React.cloneElement(child, { visible: false });
          }
        } else {
          return null;
        }
      });

      this.forceUpdate();
    };
  }

  render() {
    const { children } = this;

    // Only Button and ButtonGroupSection are allowed within
    // a ButtonGroup. Render `null` if other types of children
    // exist

    if (this.buttons.length + this.buttonGroupSections.length !== children.length) {
      console.error('ButtonGroups can only contain Button and ButtonGroupSection as its immediate children.');
      return null;
    }

    return <div className='ButtonGroup'>
      { children }
    </div>;
  }
}
