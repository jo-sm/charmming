import React from 'react';
import classnames from 'classnames';

export default class Button extends React.Component {
  onClick(e) {
    const { onClick, type } = this.props;

    console.log('Button clicked...');

    // Ignore for the special "type='submit'" case

    if (type !== 'submit') {
      e.preventDefault();
      e.stopPropagation();

      onClick && onClick(e);
    }
  }

  render() {
    const { green, red, orange, white, blue, children, disabled, section, onClick, ...rest } = this.props;
    const className = classnames("Button", { white, green, red, orange, blue });

    return <button className={className} disabled={disabled} onClick={::this.onClick} {...rest}>
      {children}
    </button>;
  }
}
