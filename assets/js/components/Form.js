import React from 'react';

import classnames from 'classnames';

export default class Form extends React.Component {
  static childContextTypes = {
    formName: React.PropTypes.string,
    setFormState: React.PropTypes.func,
    attachValidator: React.PropTypes.func,
    detachValidator: React.PropTypes.func,
  };

  constructor(props, context) {
    super(props, context);

    this.formName = props.name;
    this.validators = {};
    this.state = {};
  }

  getChildContext() {
    return {
      formName: this.formName,
      setFormState: ::this.setFormState,
      attachValidator: ::this.attachValidator,
      detachValidator: ::this.detachValidator
    };
  }

  setFormState(name, value) {
    this.setState({
      [name]: value
    });
  }

  attachValidator(name, validator) {
    if (this.validators[name]) {
      console.error(`Cannot attach: Validator exists for ${name}.`);
      return;
    }

    console.log(`Attaching validator for ${name}`);

    this.validators[name] = validator;
  }

  detachValidator(name) {
    if (!this.validators[name]) {
      console.error(`Cannot detach: Validator does not exist for ${name}.`);
      return;
    }

    console.log(`Detaching validator for ${name}`);

    delete this.validators[name];
  }

  onSubmit(e) {
    const { onSubmit } = this.props;

    e.preventDefault();
    e.stopPropagation();

    const isValid = Object.keys(this.validators).reduce((valid, name) => {
      const validator = this.validators[name];

      if (!validator) {
        console.log(`Validator not set for ${name}. Ignoring.`);
        return valid;
      }

      let value = this.state[name];

      if (!value) {
        value = '';
      }

      if (validator(value) === false) {
        console.log(`Validator for ${name} returned false.`);
        return false;
      }

      console.log(`Validator for ${name} returned ${valid}`);
      return valid;
    }, true);

    if (!isValid) {
      // Do something with the error
      return;
    }

    debugger;

    onSubmit && onSubmit(this.state);
  }

  render() {
    const { children, onSubmit, center, ...rest } = this.props;
    const className = classnames("Form", { center });

    return <form className={className} onSubmit={::this.onSubmit} {...rest}>
      {children}
    </form>;
  }
}
