import React from 'react';

import ListMenu from './ListMenu';

export default class SecondarySideNavMenu extends React.Component {
  constructor(props, ...args) {
    super(props, ...args);

    if (props.onLoad) {
      this.onLoad = props.onLoad;
    }

    this.updated = false;
  }

  componentDidUpdate() {
    if (!this.updated && this.props.visible && this.onLoad) {
      // To prevent this being called in a loop
      this.updated = true;

      this.onLoad();
    } else if (!this.props.visible && this.updated) {
      this.updated = false;
    }
  }

  render() {
    const { children, visible } = this.props;

    return <ListMenu light secondary {...{visible}}>
      { children }
    </ListMenu>;
  }
}
