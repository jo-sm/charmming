import React from 'react';

export default class JobStatus extends React.Component {
  render() {
    const { task, loading } = this.props;

    console.log(task);

    // const structureName = structure.name;
    // const structureName = '';

    return <div className='JobStatus'>
      <p className='status'>
        <strong>Current Job Status</strong>
      </p>
      <p className='job'>
      { loading && <span>Loading...</span> }
      { !loading && task &&
        <span className={task.status}>{ task.job } ({ task.program }): { task.status }</span>
      }
      </p>
    </div>;
  }
}
