import React from 'react';
import classnames from 'classnames';

export default class ListMenu extends React.Component {
  render() {
    const { children, dark, light, secondary, visible } = this.props;
    const className = classnames('ListMenu', { dark, light, secondary, visible });

    return <ul {...{ className }}>
      { children }
    </ul>;
  }
}
