import React from 'react';

import { capitalize } from '../utils/string';

export default class TextInput extends React.Component {
  render() {
    const { password, errorText, label, name, placeholder, disabled, errorful, onChange } = this.props;
    const type = password ? 'password' : 'text';
    const inputLabel = label ? label : capitalize(name);

    return <span>
      <label htmlFor={name}>{inputLabel}</label>
      <input id={name} name={name} type={type} placeholder={placeholder} disabled={disabled} onChange={onChange} />
      { errorful && errorText ? <span>{errorText}</span> : null }
    </span>;
  }
}
