import React from 'react';

export default class VerticalSection extends React.Component {
  render() {
    const { children } = this.props;

    return <div className='VerticalSection'>
      { children }
    </div>;
  }
}
