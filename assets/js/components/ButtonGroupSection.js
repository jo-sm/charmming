import React from 'react';

export default class ButtonGroupSection extends React.Component {
  constructor(...args) {
    super(...args);

    this.state = {};
  }
  render() {
    const { children, visible, name } = this.props;

    // First, check if there is a name. Don't render if not
    if (!name) {
      console.error('ButtonGroupSection must have name property.');
      return null;
    }

    if (visible) {
      return <span>{ children }</span>;
    } else {
      return null;
    }
  }
}
