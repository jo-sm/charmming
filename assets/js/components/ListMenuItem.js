import React from 'react';

import classnames from 'classnames';

export default class ListMenuItem extends React.Component {
  render() {
    const { children, action, selected } = this.props;
    const className = classnames('ListMenuItem', { selected });

    return <li className={className} onClick={action}>
      { children }
    </li>;
  }
}
