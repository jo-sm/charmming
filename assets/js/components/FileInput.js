import React from 'react';

export default class FileInput extends React.Component {
  validate() {
    // Validation will happen before here, so we just
    // return the stored value

    return this.isValid || false;
  }

  onChange(e) {
    const { validator } = this.props;

    // Instead of getting the valid from e.target.value,
    // we instead get the file and base64 encode it
    const reader = new FileReader();
    const file = e.target.files[0];

    reader.addEventListener('load', () => {
      const value = reader.result;

      this.setFormState(value);
    });

    if (validator) {
      this.isValid = validator(file);

      if (!this.isValid) {
        return;
      }
    } else {
      this.isValid = true;
    }

    reader.readAsDataURL(file);
  }

  render() {
    return <span>
      <input type='file' onChange={::this.onChange} />
    </span>;
  }
}
