import React from 'react';
import TextInput from './TextInput';
import RadioInput from './RadioInput';

export default class Input extends React.Component {
  static contextTypes = {
    formName: React.PropTypes.string,
    setFormState: React.PropTypes.func,
    attachValidator: React.PropTypes.func,
    detachValidator: React.PropTypes.func,
  };

  constructor(props, context) {
    super(props, context);

    this.formName = context.formName;

    const { type } = this.props;
    const allowedTypes = [ 'text', 'radio' ];

    if (allowedTypes.indexOf(type) === -1) {
      console.error(`Please supply a valid type. Valid types: ${allowedTypes}`);
      return;
    }

    if (!this.formName) {
      console.error(`This Input component (${type}) is outside of a Form component.`);
      return;
    }

    context.attachValidator(props.name, (value) => {
      return this.validate(value);
    });

    this.state = {
      errorful: false
    };
  }

  componentWillMount() {
    const { attachValidator } = this.context;
    const { name } = this.props;

    attachValidator(name, (value) => {
      return this.validate(value);
    });
  }

  componentWillUnmount() {
    const { detachValidator } = this.context;
    const { name } = this.props;

    detachValidator(name);
  }

  onChange(e) {
    const value = e.target.value;

    console.log(`Changed: ${value}`)

    this.validate(value);
    // We set this regardless so that we prevent the
    // user typing in something like `josh*` and it
    // validating to `josh` when the user submits the form
    this.setFormState(value);
  }

  validate(value) {
    const { validator } = this.props;

    if (!validator) {
      // If there isn't a validator, we just set it to true
      return true;
    }

    // We handle the errorText state population here so that
    // if the validation occurs when the form is submitted,
    // the error text will appear as well
    const result = validator(value);

    if (result === true) {
      this.setState({
        errorful: false
      });
    } else {
      this.setState({
        errorful: true
      });
    }

    return result;
  }

  setFormState(value) {
    const { name } = this.props;

    if (!this.context.setFormState) {
      console.error(`Attempted to set form state on a component with no Form. Name ${name} value ${value}.`);
      return;
    }

    return this.context.setFormState(name, value);
  }

  render() {
    const { type, ...rest } = this.props;
    const { errorful } = this.state;
    // let Component;

    if (type === 'text') {
      const { password, errorText, label, name, placeholder, disabled } = rest;
      return <TextInput errorful={errorful} onChange={::this.onChange} password={password} errorText={errorText} label={label} name={name} placeholder={placeholder} disabled={disabled} />;
    } else if (type === 'radio') {
      const { errorText, label, name, value, disabled } = rest;
      return <RadioInput errorful={errorful} onChange={::this.onChange} errorText={errorText} label={label} name={name} value={value} disabled={disabled} />;
    } else {
      console.error(`Cannot render Input component: invalid type ${type}`);
      return null;
    }

    // return <Component />;
  }
}
