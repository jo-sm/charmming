import React from 'react';
import ReactDOM from 'react-dom';
import pv from 'bio-pv';
import please from 'pleasejs';

import { request } from '../utils/request';

export default class Viewer extends React.Component {
  componentDidMount() {
    const componentNode = ReactDOM.findDOMNode(this);

    this.viewer = new pv.Viewer(componentNode, {
      width: 'auto',
      height: 'auto',
      quality: 'medium',
      antialias: true,
      animateTime: 350
    });

    this.segmentsToDisplay = [];
    this.currentId = null;
  }
  /*
    TODO: Make this more generic, so that it can handle
    click/double click events, not just for viewing structures
    on the Structure page
   */
  componentDidUpdate() {
    const { id } = this.props;

    if (id === this.currentId) {
      return null;
    }

    this.currentId = id;

    request('structure_pdb', {
      id
    }, {}).then(d => {
      // We need the full PDB for the helix data
      const colors = please.make_color({ colors_returned: d.pdbs.length, saturation: 1.0 });

      this.pdb = pv.io.pdb(d.pdb);
      this.pdbs = d.pdbs.map((pdbData, i) => {
        const segmentName = pdbData.segment;
        const pdbText = pdbData.pdb;
        const pdb = pv.io.pdb(pdbText);
        const color = colors[i];

        return {
          pdb,
          segmentName,
          color
        };
      });

      window.pdb = this.pdb;
      window.pdbs = this.pdbs;
      window.viewer = this.viewer;

      this.displayFull();
      this.viewer.fitTo(this.pdb);
    }).catch(err => {
      console.error(err);
    });
  }

  displayFull() {
    this.viewer.clear();

    const water = this.pdb.select('water');

    // This is after CHARMM builds
    const tipWater = this.pdb.select({ rname: 'TIP' });
    const protein = this.pdb.select('protein');
    const ligand = this.pdb.select('ligand');

    this.viewer.cartoon('protein', protein, {
      color: pv.color.rainbow(),
    });
    this.viewer.ballsAndSticks('water', water, {
      color: pv.color.uniform('blue')
    });
    this.viewer.ballsAndSticks('water', tipWater, {
      // color: pv.color.uniform('blue')
    });
    this.viewer.ballsAndSticks('ligand', ligand, {
      color: pv.color.uniform('orange')
    });
  }

  displayNone() {
    this.viewer.clear();

    const water = this.pdb.select('water');

    // This is after CHARMM builds
    const tipWater = this.pdb.select({ rname: 'TIP' });
    const protein = this.pdb.select('protein');
    const ligand = this.pdb.select('ligand');

    this.viewer.cartoon('protein', protein, {
      color: pv.color.uniform('lightgrey')
    });
    this.viewer.ballsAndSticks('water', water, {
      color: pv.color.uniform('lightgrey')
    });
    this.viewer.ballsAndSticks('water', tipWater, {
      // color: pv.color.uniform('lightgrey')
    });
    this.viewer.ballsAndSticks('ligand', ligand, {
      color: pv.color.uniform('lightgrey')
    });
    this.viewer.centerOn(protein);
  }

  displaySegments(displayNone) {
    this.viewer.clear();

    if (this.segmentsToDisplay.length === 0) {
      if (displayNone) {
        return this.displayNone();
      } else {
        return this.displayFull();
      }
    }

    const atomsInfo = {
      atoms: [],
      colors: [],
      chains: []
    };

    for (const i in this.segmentsToDisplay) {
      const segmentName = this.segmentsToDisplay[i];
      const segment = this.pdbs.find(pdb => segmentName === pdb.segmentName);
      const segmentPdb = segment.pdb;

      if (!segmentPdb) {
        continue;
      }

      atomsInfo.atoms[i] = [];

      segmentPdb.eachAtom(atom => atomsInfo.atoms[i].push(atom.qualifiedName()));

      atomsInfo.colors[i] = segment.color;
    }


    for (const i in atomsInfo.atoms) {
      const atomNames = atomsInfo.atoms[i];
      const color = atomsInfo.colors[i];

      const atoms = this.pdb.atomSelect(atom => atomNames.indexOf(atom.qualifiedName()) !== -1);

      const waterYes = atoms.select('water');
      const tipWaterYes = atoms.select({ rname: 'TIP' });
      const proteinYes = atoms.select('protein');
      const ligandYes = atoms.select('ligand');

      this.viewer.tube('proteinYes', proteinYes, {
        color: pv.color.uniform(color)
      });
      this.viewer.ballsAndSticks('waterYes', waterYes, {
        color: pv.color.uniform(color)
      });
      this.viewer.ballsAndSticks('tipWaterYes', tipWaterYes, {
        color: pv.color.uniform(color)
      });
      this.viewer.ballsAndSticks('ligandYes', ligandYes, {
        color: pv.color.uniform(color)
      });
    }

    let atoms = [];

    for(const i in atomsInfo.atoms) {
      atoms = atoms.concat(atomsInfo.atoms[i]);
    }

    const otherAtoms = this.pdb.atomSelect(atom => atoms.indexOf(atom.qualifiedName()) === -1);

    const waterNo = otherAtoms.select('water');
    const tipWaterNo = otherAtoms.select({ rname: 'TIP' });
    const proteinNo = otherAtoms.select('protein');
    const ligandNo = otherAtoms.select('ligand');
    const opacity = 0.7;

    this.viewer.ballsAndSticks('proteinNo', proteinNo, {
      color: pv.color.uniform('lightgrey')
    });
    this.viewer.get('proteinNo').setOpacity(opacity);
    this.viewer.ballsAndSticks('waterNo', waterNo, {
      color: pv.color.uniform('lightgrey')
    });
    this.viewer.get('waterNo').setOpacity(opacity);
    this.viewer.ballsAndSticks('tipWaterNo', tipWaterNo, {
      color: pv.color.uniform('lightgrey')
    });
    this.viewer.get('tipWaterNo').setOpacity(opacity);
    this.viewer.ballsAndSticks('ligandNo', ligandNo, {
      color: pv.color.uniform('lightgrey')
    });
    this.viewer.get('ligandNo').setOpacity(opacity);

    this.viewer.requestRedraw();
  }

  toggleSegment(name, { status, displayNone } = {}) {
    const i = this.segmentsToDisplay.indexOf(name);

    if (!status) {
      if (i === -1) {
        status = true;
      } else {
        status = false;
      }
    }

    if (i !== -1 && status === false) {
      this.segmentsToDisplay.splice(i, 1);
    } else {
      this.segmentsToDisplay.push(name);
    }

    return this.displaySegments(displayNone);
  }

  render() {
    return <div className='Viewer'></div>;
  }
}
