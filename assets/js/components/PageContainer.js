import StatefulComponent from './StatefulComponent';

import { getPageComponent } from '../utils/page';

import get from 'lodash.get';

export default class PageContainer extends StatefulComponent {
  render() {
    const { page, meta, extraProps = {} } = this.props;

    if (!page) {
      return null;
    }

    const PageComponent = getPageComponent(page);

    // We should have the PageComponent at this stage since the
    // navigation would have already completed, but this is just
    // in case.
    if (!PageComponent) {
      return null;
    }

    const componentProps = {
      page,
      meta,
      ...extraProps
    };

    for (const i in PageComponent.watch) {
      const key = PageComponent.watch[i];
      const stateValue = get(this.state, key);

      if (stateValue) {
        componentProps[key] = stateValue;
      }
    }

    return <PageComponent {...componentProps} />;
  }
}
