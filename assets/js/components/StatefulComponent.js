import React from 'react';

export default class StatefulComponent extends React.Component {
  static contextTypes = {
    store: React.PropTypes.object
  }

  constructor(props, context) {
    super(props, context);

    this.store = context.store;
    this.state = {};
    this.__watch__ = [];
    this.uniqueId = Math.random().toString(36).substr(2);
  }

  __watcher__(state) {
    this.setState(state);
  }

  componentWillMount() {
    console.log(`Mounting ${this.constructor.name} (unique id ${this.uniqueId})`);
    this.store.subscribe(this.uniqueId, ::this.__watcher__);
  }

  componentWillUnmount() {
    console.log(`Unmounting ${this.constructor.name}`);
    this.store.unsubscribe(this.uniqueId);
  }
}
