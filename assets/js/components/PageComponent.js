import React from 'react';

export default class PageComponent extends React.Component {
  /*
    Higher order page component that handles retrieving state for
    the actual page requested.
   */
}
