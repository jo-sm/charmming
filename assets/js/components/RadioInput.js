import React from 'react';

import { capitalize } from '../utils/string';

export default class RadioInput extends React.Component {
  render() {
    const { errorText, label, name, value, disabled, errorful, onChange } = this.props;
    const inputLabel = label ? label : capitalize(name);

    return <span>
      <label htmlFor={name}>{inputLabel}</label>
      <input type='radio' id={name} name={name} value={value} disabled={disabled} onChange={onChange} />
      { errorful && errorText ? <span>{errorText}</span> : null }
    </span>;
  }
}
