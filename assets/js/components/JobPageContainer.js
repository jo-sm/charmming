import React from 'react';
import PageContainer from './PageContainer';
import { jobPageExists } from '../utils/page';
import { dispatch } from '../store';

export default class JobPageContainer extends React.Component {
  submit(data) {
    const { program, job, structure } = this.props;
    const jobData = Object.assign({}, data);

    jobData.structure = structure.id;

    dispatch('submitJob', {
      program,
      job,
      jobData
    });
  }

  render() {
    const { program, job, structure } = this.props;

    if (!jobPageExists(program, job)) {
      console.error(`Job page for ${program} ${job} does not exist.`);
      return null;
    }

    const pageName = `jobs.${program}.${job}`;

    return <PageContainer page={pageName} extraProps={{submit: ::this.submit, structure}} />;
  }
}
