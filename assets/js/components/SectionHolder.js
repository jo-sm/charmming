import React from 'react';

export default class SectionHolder extends React.Component {
  render() {
    const { children } = this.props;

    return <div className='SectionHolder'>
      { children }
    </div>;
  }
}
