import React from 'react';

// import StatefulComponent from '../components/StatefulComponent';
import ListMenu from '../components/ListMenu';
import ListMenuItem from '../components/ListMenuItem';
import SecondarySideNavMenu from '../components/SecondarySideNavMenu';
import Separator from '../components/Separator';
import JobStatus from '../components/JobStatus';

import { dispatch } from '../store';

import get from 'lodash.get';

export default class SideNav extends React.Component {
  constructor(...args) {
    super(...args);

    this.state = {};
  }

  goto(page, options = {}) {
    return () => {
      console.log(`Dispatching navigateTo for ${page} with ${options}`);
      dispatch('navigateTo', {
        page,
        options
      });
    };
  }

  gotoAndHide(subMenu, page, options = {}) {
    return () => {
      console.log(`Going to ${page} and hiding ${subMenu}`);
      this.toggleVisible(subMenu, false)();
      this.goto(page, options)();
    };
  }

  toggleVisible(subMenu, forceVisibility) {
    return () => {
      const subMenuVisiblility = this.state["visible-" + subMenu];
      const visibility = forceVisibility != null ? forceVisibility : !subMenuVisiblility;

      this.setState({
        ["visible-" + subMenu]: visibility
      });
    };
  }

  loadStructures() {
    dispatch('loadStructures');
  }

  deleteStructure(id) {
    return (e) => {
      e.stopPropagation();

      dispatch('deleteStructure', {
        id
      });
    };
  }

  render() {
    const { loggedIn, structures, tasks, loading } = this.props;
    const visibility = {
      structures: this.state["visible-structures"]
    };

    if (!loggedIn) {
      return null;
    }

    const tasksLoading = get(loading, 'tasks');
    const task = tasks && tasks[0];

    return <div className='SideNav'>
      <JobStatus loading={tasksLoading} task={task} />
      <ListMenu dark>
        <ListMenuItem action={::this.goto('main')}>Dashboard</ListMenuItem>
        <ListMenuItem {...{selected: visibility.structures}} action={::this.toggleVisible('structures')}>
          Structures
        </ListMenuItem>
        <ListMenuItem>Jobs</ListMenuItem>
        <ListMenuItem>Calculations</ListMenuItem>
        <ListMenuItem>Analyses</ListMenuItem>
      </ListMenu>

      <SecondarySideNavMenu onLoad={::this.loadStructures} {...{visible: visibility.structures}}>
        <ListMenuItem action={::this.gotoAndHide('structures', 'fileupload')}>Upload New Structure</ListMenuItem>
        <Separator />
        { get(loading, 'structures') &&
          <ListMenuItem>Loading structures...</ListMenuItem>
        }
        {
          structures &&
          structures.map(s => {
            return <ListMenuItem key={s.id} action={::this.gotoAndHide('structures', 'structure', { id: s.id })}>
              {s.name}
              <span className='hover-only' onClick={::this.deleteStructure(s.id)}>
                Trash
              </span>
            </ListMenuItem>;
          })
        }
      </SecondarySideNavMenu>
    </div>;
  }
}
