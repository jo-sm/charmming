import React from 'react';

import Button from '../components/Button';

import { dispatch } from '../store';

export default class Nav extends React.Component {
  logout() {
    dispatch('logout');
  }

  render() {
    const { loggedIn } = this.props;

    let inner = null;

    if (loggedIn) {
      inner = <div className='NavSettings'>
        <Button white onClick={::this.logout}>Logout</Button>
      </div>;
    }

    return <div className='Nav'>
      <div className='NavLogo'>
        <strong>CHARMMing</strong>
      </div>
      { inner }
    </div>;
  }
}
