import StatefulComponent from '../components/StatefulComponent';
import Nav from './Nav';
import SideNav from './SideNav';
import PageContainer from '../components/PageContainer';

export default class Container extends StatefulComponent {
  constructor(props, context) {
    super(props, context);

    this.store.dispatch('handleInitialAuth');
  }

  render() {
    const { page, meta, loggedIn, structures, tasks, loading } = this.state;

    if (!page) {
      return null;
    }

    return <div className='Container'>
      <Nav loggedIn={loggedIn} />
      <div className='PageContainer'>
        <SideNav loggedIn={loggedIn} structures={structures} tasks={tasks} loading={loading} />
        <div className='Page'>
          <PageContainer page={page} meta={meta} />
        </div>
      </div>
    </div>;
  }
}

