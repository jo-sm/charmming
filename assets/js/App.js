import ReactDOM from 'react-dom';
import React from 'react';

import { replaceJs, replaceCss } from './dev';
import { store, dispatch } from './store';

import Container from './ui/Container';

import 'whatwg-fetch'; // window.fetch polyfill

const isDev = process.env.NODE_ENV === 'development';
let notificationsWebsocket;

if (isDev) {
  const ws = new WebSocket('ws://localhost:3002');

  ws.onopen = () => {
    console.info('Hot Swap -- WebSocket connected');
  };

  ws.onmessage = (evt) => {
    const data = evt.data;
    let msg;

    try {
      msg = JSON.parse(data);
    } catch (e) {
      console.error(`Cannot Hot Swap: Invalid JSON (${e})`);
    }

    if (msg.reload) {
      if (msg.reload === 'js') {
        ws.close();
        replaceJs().then(renderApp);
      } else if (msg.reload === 'css') {
        replaceCss();
      }

      console.info(`Hot Swap -- Reloaded ${msg.reload}`);
    } else if (msg.error) {
      console.error(msg.message);
    }
  };
}

class Provider extends React.Component {
  static childContextTypes = {
    store: React.PropTypes.object
  }

  getChildContext() {
    return {
      store: this.props.store
    };
  }

  render() {
    return this.props.children;
  }
}

function renderApp() {
  ReactDOM.render(
    <Provider store={store}>
      <Container />
    </Provider>,
    document.getElementById('root')
  );
}

function setupNotificationsWebsocket() {
  const domain = window.location.hostname;
  notificationsWebsocket = new WebSocket(`ws://${domain}:9487`);

  notificationsWebsocket.onopen = () => {
    console.log(`Notification Socket connected`);
  };

  notificationsWebsocket.onmessage = (evt) => {
    const raw = evt.data;
    let data;

    try {
      data = JSON.parse(raw);
    } catch (e) {
      console.error(`Cannot parse notification: Invalid JSON (${e})`);
    }

    console.log(data);

    dispatch('notification', data);
  };
}

function teardownNotificationsWebsocket() {
  if (notificationsWebsocket) {
    notificationsWebsocket.close();
    notificationsWebsocket = null;
  }
}

// Load app initially
document.addEventListener('DOMContentLoaded', renderApp);
document.addEventListener('setupNotificationsWebsocket', setupNotificationsWebsocket);
document.addEventListener('teardownNotificationsWebsocket', teardownNotificationsWebsocket);

// Handle history (back button) changes
// There is not a "popstate" event to add an event
// listener to, so `window.onpopstate` must be used
window.onpopstate = (e) => {
  if (e.state) {
    const options = e.state.meta;
    options.replace = true;

    const state = {
      page: e.state.page,
      options,
    };

    store.dispatch('navigateTo', state);
  }
};
