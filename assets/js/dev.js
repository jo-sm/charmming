import { store } from './store';

export const replaceJs = () => {
  const oldScript = document.getElementById('js');
  oldScript.parentNode.removeChild(oldScript);

  // Retrieve the current state
  const state = store.getState();

  return new Promise(resolve => {
    const newScript = document.createElement('script');
    newScript.src = '/js/bundle.js';
    newScript.id = 'js';
    newScript.onload = () => {
      // Set the old state in the new bundle
      store.triggerUpdate(state);
      resolve();
    };

    document.body.appendChild(newScript);
  });
};

export const replaceCss = () => {
  return new Promise(resolve => {
    const newCss = document.createElement('link');
    newCss.href = '/css/bundle.css';
    newCss.id = 'new_css';
    newCss.rel = 'stylesheet';
    newCss.type = 'text/css';
    newCss.onload = function() {
      // Remove old CSS
      const oldCss = document.getElementById('css');
      oldCss.parentNode.removeChild(oldCss);

      newCss.id = 'css';

      resolve();
    };

    document.head.appendChild(newCss);
  });
};
