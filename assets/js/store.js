import { capitalize } from './utils/string';

import * as dispatchers from './dispatchers';
import * as actions from './actions';

import cloneDeep from 'lodash.cloneDeep';
import get from 'lodash.get';

export function dispatch(name, data) {
  return store.dispatch(name, data);
}

export const store = {
  // Default state
  __state__: {
    loading: {}
  },
  __subscriptions__: {},
  __history__: [],
  subscribe: function(uniqueId, callback) {
    this.__subscriptions__[uniqueId] = (callback);

    // Run callback initially
    callback(this.getState());
  },
  unsubscribe: function(uniqueId) {
    delete this.__subscriptions__[uniqueId];
  },
  fireAction: function(name, data) {
    const currentState = this.__state__;
    const actionParts = name.split('.');
    let actionDir;
    let actionName;

    if (actionParts.length > 1) {
      actionDir = actionParts.slice(0, -1).join('.');
    }

    actionName = capitalize(actionParts.slice(-1)[0]);

    if (actionDir) {
      actionName = `${actionDir}.${actionName}`;
    }

    if (!get(actions, actionName)) {
      console.error(`Action does not exist: ${actionName}`);
    }

    const newState = get(actions, actionName)(currentState, data);
    this.triggerUpdate(newState);
  },
  dispatch: function(name, data) {
    // This function explicitly does not have a return value
    // Dispatching an action should potentially change the data
    // and that data should be observed for changes, not any
    // resulting change from the dispatch completion
    const dispatcherName = capitalize(name);
    const currentState = this.__state__;

    if (!dispatchers[dispatcherName]) {
      console.error(`Dispatcher does not exist: ${dispatcherName}`);
      return;
    }

    dispatchers[dispatcherName]({ state: currentState, data, fire: ::this.fireAction }).catch(e => {
      console.error('An uncaught error occurred during action dispatch.');
      console.error(e);
    });
  },
  triggerUpdate(state) {
    if (state) {
      this.__history__.push(this.__state__);
      this.__state__ = cloneDeep(state);
    }

    for(const key in this.__subscriptions__) {
      const subscription = this.__subscriptions__[key];

      subscription(this.__state__);
    }
  },
  getState: function() {
    return this.__state__;
  }
};
