#!/usr/bin/env bash

docker rm postgres >/dev/null 2>/dev/null

# Since this is local, this password in plain text is fine
postgres_user="charmming"
postgres_pass="9f89f5ec66778f9c468a775f"
postgres_db_data_dir="$(pwd)/.db/db"

test_postgres() {
  while true; do
    output=$(PGPASSWORD=$postgres_pass psql -U $postgres_user -h localhost -d postgres -c "\\h" 2>/dev/null)

    if [ -n "$output" ]; then
      break
    fi
  done
}

end() {
  printf "Stopping charmming container... "
  docker stop charmming_test 2>/dev/null

  printf "Stopping postgres container... "
  docker stop postgres

  exit 0
}

trap end SIGINT SIGTERM

if [[ "${@:1:1}" == "--debug" ]]; then
  maketask="run-tests-debug"
else
  maketask="run-tests"
fi

# Start Postgres
docker run \
  -e "POSTGRES_PASSWORD=$postgres_pass" \
  --name postgres \
  -p 127.0.0.1:5432:5432 \
  -v $postgres_db_data_dir:/var/lib/postgresql/data \
  -t \
  -d \
  postgres:9.6 2>/dev/null >/dev/null

# The above command makes an assumption that "postgres" is the
# container that contains the PostgreSQL database

if [ "0" -eq "$?" ]; then
  test_postgres
  test_postgres
fi

docker run \
  -e "CHARMMING_DB_NAME=charmming" \
  -e "CHARMMING_DB_USER=$postgres_user" \
  -e "CHARMMING_DB_PASS=$postgres_pass" \
  -e "CHARMMING_SECRET_KEY=test" \
  -e "CHARMMING_DB_HOST=db" \
  -v $(pwd):/charmming \
  --link postgres:db \
  --name charmming_test \
  -it \
  --rm \
  scheduler:latest \
  make -C /charmming $maketask

end
