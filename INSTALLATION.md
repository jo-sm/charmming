# Installation

Local development installation of CHARMMing is simple, since CHARMMing uses Docker. Currently, you will need to build the images yourself until they are hosted on a repository.

## Building Yourself

The entire CHARMMing application uses the following Docker images:

1. The CHARMMing base image, which contains dependencies and applications not installed via pip or npm. It can be either the basic (without proprietary executables), or extended (with proprietary executables like `CHARMM` and `Q-chem`).
2. The CHARMMing web app image, which also contains the Scheduler daemon.
3. The Notification daemon image
4. The Scheduler daemon image

Additionally, for development, Redis and PostgreSQL images are used and are pulled automatically during the initial application start.

### Base image

Before building the webapp and notification daemon images, you will need to build the base image. Instructions for building can be found on [LoBoS Gitlab](https://git.lobos.nih.gov/charmming/charmming-base).

### Webapp

Once you have the base image, you can create the web app image using the following command:

```
# In project root folder
docker build -t charmming -f docker/charmming/Dockerfile .
```

If you have an Extended base image available, you can build the image with the following command:

```
# In project root folder
docker build -t charmming -f docker/charmming/Extended.dockerfile .
```

The web app can be run without the Extended base image, but it will only be able to do basic things like uploading and augmenting structures. To submit and run jobs, you will need an Extended base image.

### Notification daemon

You can create the notification daemon image using the following command:

```
# In project root folder
docker build -t notificationd -f docker/notificationd/Dockerfile .
```

### Scheduler daemon

You can create the notification daemon image using the following command:

```
# In project root folder
docker build -t schedulerd -f docker/schedulerd/Dockerfile .
```

# Post Installation

After building the Docker images, you will need to run the migrations for the database, create an initial user, and setup the databases. Run the `post_install.sh` script to set these up.
