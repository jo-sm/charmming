#!/usr/bin/env python3

from getpass import getpass

import os
import django
import bcrypt

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "charmming.settings")

django.setup()

from charmming.models import User # noqa

def ask_for(label):
  value = input("{}: ".format(label))

  if not value:
    ask_for(label)

  return value

def ask_for_password():
  password = getpass()
  password_verify = getpass(prompt='Password (again): ')

  if password != password_verify:
    ask_for_password()

  return password

first_name = ask_for("First name")
last_name = ask_for("Last name")

username = ask_for("Username")
password = ask_for_password()

digest = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())

user = User()
user.username = username
user.digest = digest
user.first_name = first_name
user.last_name = last_name
user.email = 'dev@localhost'
user.save()
