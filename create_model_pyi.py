#!/usr/bin/env python3

import ast
import importlib.util
import importlib
import pkgutil
import os
import django

_dir = "charmming.models"

model_cache = {}
import_cache = {
  'django.contrib.auth.models': [
    'User'
  ],
  'typing': [
    'List',
    'Iterable',
    'Iterator',
    'Sized',
  ]
}
pyi_file = open('charmming/models/__init__.pyi', 'w')

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "charmming.settings")
django.setup()

def determine_type(attr, name, *args):
  types = {
    'int': [
      'AutoField',
      'BigAutoField',
      'IntegerField',
      'SmallIntegerField',
      'PositiveIntegerField',
      'PositiveSmallIntegerField',
    ],
    'str': [
      'CharField',
      'EmailField',
      'FileField',
      'FilePathField',
      'ImageField',
      'GenericIPAddressField',
      'SlugField',
      'TextField',
      'URLField',
    ],
    'bytes': [
      'BinaryField'
    ],
    'bool': [
      'BooleanField',
      'NullBooleanField'
    ],
    'datetime': [
      'DateTimeField'
    ],
    'float': [
      'DecimalField',
      'FloatField',
    ],
    '__foreign__': [
      'ForeignKey',
      'ManyToManyField',
      'OneToOneField',
    ]
  }

  field_type = [ k for k, v in list(types.items()) if attr in v ]

  if len(field_type):
    field_type = field_type[0]

    if field_type == '__foreign__':
      if hasattr(args[0], 's'):
        if args[0].s == 'self':
          return (name, name)
      else:
        return (args[0].id, args[0].id)

    return (field_type, None)
  else:
    return ('str', None)

def is_model(base, **kwargs):
  if hasattr(base, 'attr'):
    if base.attr == 'Model' and base.value.id == 'models':
      return (True, None)
  elif hasattr(base, 'id'):
    loader = kwargs.get('loader')
    _dir = kwargs.get('_dir')
    name = base.id

    if not loader:
      return (False, None)

    if loader.find_spec("{}.{}.{}".format(_dir, name, name)):
      return (True, name)

  return (False, None)

def determine_model_stub(_dir, loader, name, defs, imports):
  source = loader.find_spec("{}.{}.{}".format(_dir, name, name)).loader.get_source("{}.{}.{}".format(_dir, name, name))

  tree = ast.parse(source)

  # Determine if this is a Django db model
  for branch in tree.body:
    if type(branch) == ast.ClassDef:
      klass = branch

      base = klass.bases[0]

      _is_model, base_class = is_model(base, _dir=_dir, loader=loader)

      if not _is_model:
        continue

      defs[klass.name] = {
        'base': None,
        'foreign_keys': [],
        'attributes': [],
        'functions': []
      }

      if base_class:
        defs[klass.name]['base'] = base_class

      for obj in klass.body:
        if type(obj) == ast.Assign:
          if type(obj.value) == ast.Call:
            try:
              _type, foreign_key = determine_type(obj.value.func.attr, klass.name, *obj.value.args)
            except:
              _type = 'str'

            if _type == 'datetime':
              if not imports.get('datetime'):
                imports['datetime'] = []

              imports['datetime'].append('datetime')

            if foreign_key:
              defs[klass.name]['foreign_keys'].append(foreign_key)

            defs[klass.name]['attributes'].append({
              'name': obj.targets[0].id,
              'type': _type
            })
        elif type(obj) == ast.FunctionDef:
          defs[klass.name]['functions'].append({
            'name': obj.name,
            'arguments': []
          })

      return defs, imports

def get_base_attributes(model_name):
  model = model_cache.get(model_name)
  attributes = model.get('attributes').copy()

  print(model_name)
  print(attributes)

  if model.get('base'):
    print('Got base...')
    attributes += get_base_attributes(model.get('base'))

  return attributes

def write_line(str, **kwargs):
  indentation = kwargs.get('indentation', 0)

  pyi_file.write("{}{}\n".format("  " * indentation, str))

def write_import(import_name, imports):
  if len(imports):
    write_line("from {} import {}".format(import_name, ','.join(list(set(imports)))))
  else:
    write_line("import {}".format(import_name))

def write_model(model_name, model):
  if model.get('written'):
    return

  # This is a crude way to handle this (lumping all of the base model attributes into here)
  # but it gets around limitations of mypy
  attributes = get_base_attributes(model_name)

  write_line("class ModelObjectMethods{}(ModelObjectMethods):".format(model_name))
  write_line("def get(self, *args, **kwargs) -> {}: ...".format(model_name), indentation=1)
  write_line("def __getitem__(self, *args) -> {}: ...".format(model_name), indentation=1)
  write_line("def all(self, **kwargs) -> ModelObjectMethods{}: ...".format(model_name), indentation=1)
  write_line("def filter(self, **kwargs) -> ModelObjectMethods{}: ...".format(model_name), indentation=1)
  write_line("def exclude(self, *args, **kwargs) -> ModelObjectMethods{}: ...".format(model_name), indentation=1)
  write_line("def order_by(self, *args) -> ModelObjectMethods{}: ...".format(model_name), indentation=1)

  write_line("class {}:".format(model_name))
  write_line("def __init__(self, {}) -> None: ...".format(', '.join(["{}: {} = ...".format(m['name'], m['type']) for m in attributes])), indentation=1)

  for attr in attributes:
    write_line("{} = ... # type: {}".format(attr['name'], attr['type']), indentation=1)

  write_line("def id(self) -> int: ...", indentation=1)
  write_line("def save(self, **kwargs) -> None: ...", indentation=1)
  write_line("def add(self, *args) -> None: ...", indentation=1)
  write_line("def all(self, *args) -> ModelObjectMethods{}: ...".format(model_name), indentation=1)
  write_line("def delete(self, *args) -> None: ...", indentation=1)
  write_line("objects = ... # type: ModelObjectMethods{}".format(model_name), indentation=1)

  model_cache[model_name]['written'] = True

def write_foreign_key_models(model_name, foreign_keys):
  for key in foreign_keys:
    if key == model_name:
      continue

    foreign_key_model = model_cache.get(key)

    if foreign_key_model:
      # print('foreign_key', foreign_key_model.get('foreign_keys'))

      if len(foreign_key_model.get('foreign_keys')):
        write_foreign_key_models(key, foreign_key_model.get('foreign_keys'))

      if model_cache.get(key):
        write_model(key, model_cache.get(key))

_module_ = importlib.import_module(_dir)

for loader, name, is_pkg in pkgutil.walk_packages(_module_.__path__):
  model_cache, import_cache = determine_model_stub(_dir, loader, name, model_cache, import_cache)

# Print the imports and model definitions
for import_name, imports in list(import_cache.items()):
  if len(imports):
    write_line("from {} import {}".format(import_name, ','.join(list(set(imports)))))
  else:
    write_line("import {}".format(import_name))

# Write Django objects methods
write_line("class ModelObjectMethods(Iterable, Sized):")
write_line("def values(self, *args, **kwargs) -> List[dict]: ...", indentation=1)
write_line("def update(self, **kwargs) -> None: ...", indentation=1)
write_line("def delete(self, **kwargs) -> None: ...", indentation=1)

for model_name, model_def in list(model_cache.items()):
  base_name = model_def.get('base')

  if len(model_def.get('foreign_keys')):
    write_foreign_key_models(model_name, model_def.get('foreign_keys'))

  if base_name and model_cache.get(base_name):
    write_foreign_key_models(model_name, [ base_name ])

  if not model_cache.get(model_name):
    continue

  write_model(model_name, model_def)

pyi_file.close()
