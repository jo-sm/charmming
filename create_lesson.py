
import os
import traceback
from django.template import Context
from django.template.loader import get_template
from charmming.helpers import output
from charmming import charmming_config
from charmming.lesson_maker.models import LessonMakerLesson, Step, Objective
from charmming.solvation.models import solvationTask
from charmming.models import EnergyTask, Structure
from charmming.normalmodes.models import nmodeTask
from charmming.models import LdTask, MdTask, SgldTask, MinimizeTask

from charmming.lesson_config import lesson_title as lesson_conf_lesson_title
from charmming.lesson_config import file_type as lesson_conf_file_type
import shutil
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = 'charmming.settings' # if we don't do this before the rest it freaks out

# the next few should not be necessary, but running straight python creates...problems with the RDBMS
# as used by django

# this is where the magic happens, we turn DB lesson data into lesson code!
# While we could call up lesson-maker_tpl2.py and do stuff using it, it's probably easier
# to just rewrite the files, since this gives us more flexibility. For example, say we are
# creating lesson42. Then we want to custom-build lesson42/models.py to have error conditions
# that are specific to it. To this end, we create a bunch of templates in mytemplates/new_lesson_maker
# (to avoid confusion with the old one) that template the files that we need to
# create from scratch. We also need to create a new directory for the lesson in the root with models,
# views, admin, __init__.py, all of which (except __init__) need to be templated.
# Then we need to create HTML files for this lesson based on the HTML for the previous lessons.
# Once all of this is done, we don't have to create any more files, but rather we need to modify them.
# First thing we need to do is copy over any structure files that were uploaded (though I haven't
# a clue as of yet on how to do that - maybe I should focus on that later)
# then once that is done, we can start modifying existing files that import lesson stuff.
# this means every single task that is called in the lesson's models.py:
#   minimization, solvation, energy, nmode, md, ld, sgld and redox (which isn't always called)
# redox and mutation can be problematic, so we need to be careful with those. I'll leave them
# out for now since they're confusing and we need to get the basics up first; adding the rest is not
# as difficult, it just means adding stuff to the templates as tasks develop.
# we also need to modify structure/models and structure/views to add the extra lessons
# once that is done, we need to modify lesson_config to add the new stuff as well
# and also settings.py to add those extra bits in.
# we also need to modify skeleton.html, but this is a trivial template.

# in order to make future modifications to this code sane (because they will happen),
# we make a dict (rather than a straight object, since I don't know how django's codebase
# treats the keys of its template dictionary, or whether it needs to hash them for whatever
# reason) holding the following things:
#        lesson_num - really a misnomer, this is the suffix, e.g. "6" for lesson6
#        file_type  - file type of the lesson, this can be derived from the LessonMakerLesson object filename
#        lesson_title - title of lesson, this is also defined by the user
#        filename - filename of the structure file uploaded by the user, get from LessonMakerLesson object
#        stepcount - count of step objects. Get these.
#        objectives - objectives, get from HTML form
#        intro_remarks - intro, get from HTML form
# steps get treated separately, which you can see how to treat in mytemplates/new_lesson_maker/lesson_models_tpl.py
# that file has everything you will probably need

# we call this one a bunch to get rid of the lesson folder so we can rebuild
def delete_lesson_files():
    traceback.print_exc()
    shutil.rmtree("%s/lesson%s"%(charmming_config.charmming_root,lesson_name))
    print("Please verify that your lesson was valid, and try running this program again.")
    sys.exit(1)

def create_lesson_tpl(tdict,modify,lesson_name):
    t = get_template('%s/mytemplates/new_lesson_maker/lesson_%s_tpl.py' % (charmming_config.charmming_root,modify))
    template = output.tabTxt(t.render(Context(tdict)))
    templ_out = open("%s/lesson%s/%s.py" %(charmming_config.charmming_root,lesson_name,modify),"w")
    templ_out.write(template)
    templ_out.close()
#    except (django.template.TemplateSyntaxError),e:
#        print(e.django_template_source)
#        sys.exit(1)

print("Starting create_lesson. Scanning for lessons to build...")
try:
    lessons = LessonMakerLesson.objects.all() #fetch all lesson objects
except:
    print("There are no lessons to build, or the lessons are corrupted. Please build lessons before using this program.")
    sys.exit(1)

for lesson in lessons:
    lesson_exists = False #we can save some work if the lesson already exists and you're overwriting it.
    #we attach these to vars because we will call them later and it's better to have them in "hard" storage than constantly callback
    lesson_name = lesson.name
    lesson_title = lesson.title
    lesson_struct = lesson.structure
    lesson_owner = lesson_struct.owner
    struct_name = lesson.filename
    lesson_filetype = lesson.filename.strip().split(".")[1].upper() #turns "1yjp.pdb  " into "PDB"
    user_lesson = ""
    if not lesson.finished:
        print(("lesson%s: %s, created by %s, with structure file %s has not been finalized. Skipping." % (lesson_name, lesson_title, lesson_owner, struct_name)))
        continue
    while (user_lesson.upper() != "Y") and (user_lesson.upper() != "N"):
        user_lesson = input("Do you wish to create lesson%s: %s, created by %s, with structure file %s? (Y/N)" % (lesson_name, lesson_title, lesson_owner, struct_name))
    if user_lesson.upper() == "N":
        continue
    #first we check if the lesson is already there
    try:
        os.stat(charmming_config.charmming_root + "/lesson" + lesson_name)
        lesson_exists = True
        print("LessonMakerLesson already exists. Overwriting...")
    except:
        os.mkdir(charmming_config.charmming_root + "/lesson" + lesson_name)
        try:
            #if this dir is gone, copyfile won't work
            os.mkdir("%s/mytemplates/lessons/lesson%s"%(charmming_config.charmming_root,lesson_name))
        except:
            pass
        print(("Creating directory %s/lesson%s ..." %(charmming_config.charmming_root,lesson_name)))

    steps = Step.objects.filter(lesson=lesson).order_by("step") #ascending order
    tdict = {} #here's where the templating begins
    #we fill in all of the variables we'll ever need, and just pass the single dict around
    #to simplify our logic. After all, django doesn't need to use the whole dict.
    try:
        shutil.copyfile("%s/%s"%(lesson_struct.location,struct_name),"%s/mytemplates/lessons/lesson%s/%s"%(charmming_config.charmming_root,lesson_name,struct_name))
    except:
        print("Failed to copy structure file. Please verify that your structure is valid.")
        traceback.print_exc()
        shutil.rmtree("%s/mytemplates/lessons/lesson%s"%(charmming_config.charmming_root,lesson_name))
        sys.exit(1)
    tdict['lesson'] = lesson
    tdict['steps']=steps
    tdict['objectives'] = Objective.objects.filter(lesson=lesson).order_by("obj_num")
    stepcount = len(steps)
    tdict['stepcount'] = stepcount
    steps_zero = [x for x in range(0,stepcount)]
    tdict['steps_zero'] = steps_zero
    #change task_dict and the loop below to add new tasks
    task_dict = {
        'minimization_tasks':[],
        'solvation_tasks':[],
        'energy_tasks':[],
        'nmode_tasks':[],
        'md_tasks':[],
        'ld_tasks':[],
        'sgld_tasks':[]
    }
    workstruct = None
    for step in steps:
        if step.task and not workstruct:
            workstruct = step.task.workstruct
        if step.type == "minimization":
            newtask = MinimizeTask.objects.get(task_ptr_id=step.task.id)
            task_dict['minimization_tasks'].append((newtask,float(step.step)-1,float(step.step)-0.5))
        elif step.type == "solvation" or step.type == "neutralization":
            newtask = solvationTask.objects.get(task_ptr_id=step.task.id)
            task_dict['solvation_tasks'].append((newtask,float(step.step)-1,float(step.step)-0.5))
        elif step.type == "energy":
            newtask = EnergyTask.objects.get(task_ptr_id=step.task.id)
            task_dict['energy_tasks'].append((newtask,float(step.step)-1,float(step.step)-0.5))
        elif step.type == "nmode":
            newtask = nmodeTask.objects.get(task_ptr_id=step.task.id)
            task_dict['nmode_tasks'].append((newtask,float(step.step)-1,float(step.step)-0.5))
        elif step.type == "md":
            newtask = MdTask.objects.get(task_ptr_id=step.task.id)
            task_dict['md_tasks'].append((newtask,float(step.step)-1,float(step.step)-0.5))
        elif step.type == "ld":
            newtask = LdTask.objects.get(task_ptr_id=step.task.id)
            task_dict['ld_tasks'].append((newtask,float(step.step)-1,float(step.step)-0.5))
        elif step.type == "sgld":
            newtask = SgldTask.objects.get(task_ptr_id=step.task.id)
            task_dict['sgld_tasks'].append((newtask,float(step.step)-1,float(step.step)-0.5))
        else:
            continue #ignore, the other steps we either don't support yet or are buildstruct
    tdict['task_dict'] = task_dict #dict in a dict! totally fine practice.
    tdict['workstruct'] = workstruct #use segments and stuff.
    try:
        print(("Creating models.py file for lesson"+lesson_name))
        create_lesson_tpl(tdict,"models",lesson_name)
    except:
        print("Creating models.py file failed. Please check your uploaded lesson for validity.")
        delete_lesson_files()

    #these templates absolutely need to be rewritten if the lesson already exists, since this logic can change
    if not lesson_exists: #views only take lesson.name - no logic beyond the title here
        #lesson is already defined, so we can just re-use the dict to create views.py
        try:
            print(("Creating views.py file for lesson"+lesson_name))
            create_lesson_tpl(tdict,"views",lesson_name)

            #same thing for admin.py
            print(("Creating admin.py file for lesson"+lesson_name))
            create_lesson_tpl(tdict,"admin",lesson_name)

            #same thing for urls.py
            print(("Creating urls.py file for lesson"+lesson_name))
            create_lesson_tpl(tdict,"urls",lesson_name)

            #create __init__.py, or it won't run
            print(("Creating __init__.py for lesson"+lesson_name))
            shutil.copy("%s/mytemplates/new_lesson_maker/__init__.py"%charmming_config.charmming_root,"%s/lesson%s"%(charmming_config.charmming_root,lesson_name))
        except:
            print("Creating lesson MVC files failed. Deleting all files...")
            delete_lesson_files()

    #now there are still py files to create, but all of these involve opening files and processing
    #so first let's deal with the files we can deal with without having to open anything.
    #these do not conform to our previous format so we ignore them in our big function
    #these will absolutely change on user rework, so let it happen
    try:
        print(("Creating main HTML template file for lesson"+lesson_name))
        t = get_template('%s/mytemplates/new_lesson_maker/lesson_tpl.html' % charmming_config.charmming_root)
        template = output.tabTxt(t.render(Context(tdict)))
        templ_out = open("%s/mytemplates/html/lesson%s.html" %(charmming_config.charmming_root,lesson_name),"w")
        templ_out.write(template)
        templ_out.close()
    except:
        print("Creating HTML template file failed. Please check your lesson and try running this program again.")
        traceback.print_exc()

    #that's about it, since the others require us to know about the other lessons that exist.
    #so let's do lesson_config first, to fetch these lessons, then use them to fill out the
    #remaining templates.
    print("Creating new lesson_config.py file at CHARMMing web root.")
    #lesson titles can change so we don't check for lesson_exists
    #i'd put these above with the rest of the logic but this helps to see the program flow better
    shutil.copyfile("%s/lesson_config.py"%charmming_config.charmming_root,"%s/lesson_config_prelesson%s.py"%(charmming_config.charmming_root,lesson_name))
    lesson_files = lesson_conf_file_type #we can iterate through keys to get the numbers
    lesson_titles = lesson_conf_lesson_title
    #now we set these up into a list
    lessons = []
    for key in lesson_files.keys():
        lessons.append({'name':key,'title':lesson_titles[key],'file_type':lesson_files[key]})
    lessons.append({'name':lesson_name,'title':lesson_title,'file_type':lesson_filetype})
    #we could put the latest lesson last, but it'll get re-sorted when the user adds a new lesson
    #so we may as well get it sorted now
    lessons = sorted(lessons,key=lambda x: x['name']) #name ordering should suffice, however remember these end up not being numbers at some point!
    tdict['lessons']=lessons
    try:
        t = get_template('%s/mytemplates/new_lesson_maker/lesson_config_tpl.py' % charmming_config.charmming_root)
        template = output.tabTxt(t.render(Context(tdict)))
        templ_out = open("%s/lesson_config.py" %charmming_config.charmming_root,"w")
        templ_out.write(template)
        templ_out.close()
    except:
        print("Failed to write lesson_config.py. Reverting to old copy...")
        shutil.copyfile("%s/lesson_config_prelesson%s.py"%(charmming_config.charmming_root,lesson_name),"%s/lesson_config.py"%(charmming_config.charmming_root))
        traceback.print_exc()
        sys.exit(1)

    #we repeat the process once again for skeleton...same reasoning for lesson_exists as before
    shutil.copyfile("%s/mytemplates/html/skeleton.html"%charmming_config.charmming_root,"%s/mytemplates/html/skeleton_prelesson%s.html"%(charmming_config.charmming_root,lesson_name))
    print("Creating new CHARMMing HTML skeleton...")
    try:
        t = get_template('%s/mytemplates/new_lesson_maker/skeleton_tpl.html' % charmming_config.charmming_root)
        template = output.tabTxt(t.render(Context(tdict))) #otherwise this one will just quit for some reason...
        templ_out = open("%s/mytemplates/html/skeleton.html" %charmming_config.charmming_root,"w")
        templ_out.write(template)
        templ_out.close()
    except:
        print("Failed to create new CHARMMing HTML skeleton. Reverting to old copy...")
        shutil.copyfile("%s/mytemplates/html/skeleton_prelesson%s.html"%(charmming_config.charmming_root,lesson_name),"%s/mytemplates/html/skeleton.html"%(charmming_config.charmming_root))

    delete_struct = ""
    while (delete_struct.upper() != "Y") and (delete_struct.upper() != "N"):
        delete_struct = input("Do you wish to delete the structure %s, attached to lesson%s, created by %s? (Y/N)" % (lesson.structure.name, lesson_name, lesson_owner))
    if delete_struct == "Y":
        Structure.objects.filter(id=lesson.structure.id).delete()
    print(("Finished with lesson%s"%lesson_name))
print("Successfully added all finished lessons in database. Please reboot your webserver and run manage.py syncdb before trying your new lessons.")
sys.exit(0)
