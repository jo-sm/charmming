#
#                            PUBLIC DOMAIN NOTICE
#
#  This software/database is a "United States Government Work" under the
#  terms of the United States Copyright Act.  It was written as part of
#  the authors' official duties as a United States Government employee and
#  thus cannot be copyrighted.  This software is freely available
#  to the public for use.  There is no restriction on its use or
#  reproduction.
#
#  Although all reasonable efforts have been taken to ensure the accuracy
#  and reliability of the software and data, NIH and the U.S.
#  Government do not and cannot warrant the performance or results that
#  may be obtained by using this software or data. NIH, NHLBI, and the U.S.
#  Government disclaim all warranties, express or implied, including
#  warranties of performance, merchantability or fitness for any
#  particular purpose.
import os, re, traceback
from django.template import Context, loader
from django.template.loader import get_template
os.environ['DJANGO_SETTINGS_MODULE']='settings' #it'll get weird if we don't set this
from charmming import output
from charmming import structure
from charmming import charmming_config
from charmming.lesson_config import *
import importlib
import shutil
import sys

#This script is the opposite of create_lesson.py.
#It takes the number (name) of the lesson as input, i.e., "6" if dealing with lesson6,
#and wipes all files and all code related to it in the webroot. This way, a user can
#easily remove any and all unwanted lessons from the directory. It is possible to do this
#as part of the installer script, or simply to correct mistakes made during the lesson maker
#procedure. After all, it is ony fair that a user should get to redo the lesson without
#having to modify the code - otherwise, there's no point in the lesson maker, since
#it would mean you have to get it right the first time. That is not a reasonable expectation.
#Let x be the name of the lesson we're wiping, and root be equivalent
#to charmming_config.charmming_root Then:
#Folders we need to wipe:
#
#root/lessonx/
#root/mytemplates/lessons/lessonx (contains PDBs and stuff)
#
#Files we need to wipe:
#root/mytemplates/html/lessonx.html
#
#Files we need to modify (more difficult):
#root/lesson_config.py
#root/mytemplates/html/skeleton.html
#
#In order to carry out the modification, it's probably best to just re-fill the template
#but without the lesson in question. We will do this by accessing lesson_config itself
#and wiping whatever's in it. Same deal for skeleton.html.
#First, let's get the data we need, from lesson_config.py, so as to make sure user input
#is sane
print("Starting Lesson Deleter...")
lesson_files = file_type
lesson_titles = lesson_title
lesson_names = lesson_num_lis
lessons = []
for key in lesson_files.keys():
    lessons.append({'name':key,'title':lesson_titles[key],'file_type':lesson_files[key]})
lessons = sorted(lessons,key=lambda x: x['name']) #name ordering should suffice, however remember these end up not being numbers at some point!

print("The following lessons are currently installed on your system:")
for lesson in lesson_names:
    print(lesson)
lesson_found = False
while not lesson_found:
    lesson_to_delete = input("Which lesson do you wish to delete?\n")
    if lesson_to_delete in lesson_names:
        lesson_found = True
    else:
        print("That lesson is not installed on this system.")

#ok, so now we have a lesson. Onward!
for lesson in lessons:
    if lesson['name'] == lesson_to_delete:
        lessons.remove(lesson)
#rewrite template
tdict = {}
tdict['lessons'] = lessons
#broken links and broken CHARMMing are more important than actually removing the lesson, since having no links is fine
#and not importing the files even though they're still there, is also fine.

try:
    lobj = "lesson{}".format(lesson_to_delete)
    print(("{}.models.{}".format(lobj,lobj.capitalize())))
    lesson_obj = importlib.import_module(lobj).models.__dict__[lobj.capitalize()]()
    lesson_obj_class=lesson_obj.__class__
    #WARNING: the below is a VERY DANGEROUS OPERATION. Only an admin should be allowed to run this!
    lesson_obj_class.objects.raw("DROP TABLE {}_{}".format(lobj,lobj))
except:
    print(("Could not drop table of objects for lesson%s. Please perform this drop manually if needed, or leave the table as-is if you wish to create a lesson with the same name in the future."%charmming_config.charmming_root))
    traceback.print_exc()

print("Modifying CHARMMing HTML skeleton...")
shutil.copyfile("%s/mytemplates/html/skeleton.html"%charmming_config.charmming_root,"%s/mytemplates/html/skeleton_lesson%s.html"%(charmming_config.charmming_root,lesson_to_delete))
try:
    t = get_template('%s/mytemplates/new_lesson_maker/skeleton_tpl.html' % charmming_config.charmming_root)
    template = output.tidyInp(t.render(Context(tdict))).encode('utf-8')
    templ_out = open("%s/mytemplates/html/skeleton.html" %charmming_config.charmming_root,"w")
    templ_out.write(template)
    templ_out.close()
except Exception as exception:
    print("Could not modify the CHARMMing HTML skeleton. Reverting to old version...")
    shutil.copyfile("%s/mytemplates/html/skeleton_lesson%s.html"%(charmming_config.charmming_root,lesson_to_delete),"%s/mytemplates/html/skeleton.html"%charmming_config.charmming_root)
    traceback.print_exc()
    sys.exit(1)

print("Modifying lesson_config module...")
shutil.copyfile("%s/lesson_config.py"%charmming_config.charmming_root,"%s/lesson_config_lesson%s.py"%(charmming_config.charmming_root,lesson_to_delete))
try:
    t = get_template('%s/mytemplates/new_lesson_maker/lesson_config_tpl.py' % charmming_config.charmming_root)
    template = output.tidyInp(t.render(Context(tdict)))
    templ_out = open("%s/lesson_config.py" %charmming_config.charmming_root,"w")
    templ_out.write(template)
    templ_out.close()
except Exception as exception:
    print("Could not modify lesson_config.py. Reverting to old version...")
    shutil.copyfile("%s/lesson_config_lesson%s.py"%(charmming_config.charmming_root,lesson_to_delete),"%s/lesson_config.py"%charmming_config.charmming_root)
    traceback.print_exc()
    sys.exit(1)

print(("Deleting lesson%s module folder..."%lesson_to_delete))
try:
    shutil.rmtree("%s/lesson%s"%(charmming_config.charmming_root,lesson_to_delete))
except Exception as exception:
    print("Could not delete lesson%s module folder.")
    traceback.print_exc()

print(("Deleting lesson%s files folder..."%lesson_to_delete))
try:
    shutil.rmtree("%s/mytemplates/lessons/lesson%s"%(charmming_config.charmming_root,lesson_to_delete))
except Exception as exception:
    print("Could not delete lesson%s files folder.")
    traceback.print_exc()

print(("Deleting lesson%s HTML file..."%lesson_to_delete))
try:
    os.unlink("%s/mytemplates/html/lesson%s.html"%(charmming_config.charmming_root,lesson_to_delete))
except Exception as exception:
    print("Could not delete lesson%s HTML file.")
    traceback.print_exc()


print(("Finished deleting lesson%s. Please restart your server before using the site again."%lesson_to_delete))
sys.exit(0)
