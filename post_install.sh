#!/usr/bin/env bash

set -a

source constants.sh
source utils.sh

end() {
  docker-compose down
}

trapped() {
  end
  exit 0
}

trapped_exit() {
  end
}

get_username() {
  username_raw=$(PGPASSWORD=$postgres_pass psql -t -U $postgres_user --host localhost -d charmming -c 'select username from auth_user limit 1;' 2>/dev/null)

  if [ "$?" -gt "0" ]; then
    echo "Error occurred while retrieving username. Double check that createsuperuser worked properly."
    end
    exit 1
  fi

  echo -ne $(echo -ne "${username_raw}")
}

trap trapped INT TERM
trap trapped_exit EXIT

# Ensure that the script is run inside of the correct directory
ensure_correct_dir

# Create initial db directories
mkdir -p $(pwd)/.db/db 2>/dev/null
mkdir -p $(pwd)/.db/redis 2>/dev/null

docker-compose up -d

# Wait for db to spin up
printf "Waiting for postgres container to fully start... "

test_postgres

# We do this a second time to ensure the database is working
test_postgres

echo "started."

# Detect (and create) charmming database
printf "Detecting charmming database... "

detect_or_create_db "charmming"

echo "done."

printf "Detecting schedulerd database... "

detect_or_create_db "schedulerd"

echo "done."

printf "Waiting for charmming container to start..."

test_charmming

echo " started."

docker-compose exec charmming python manage.py makemigrations
docker-compose exec charmming python manage.py migrate
docker-compose exec charmming python create_user.py

# Find the user's username, and create a folder for them
mkdir -p "files/" 2>/dev/null

echo "Post installation script complete."
echo "Run start.sh to start development."

end
