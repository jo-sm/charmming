#!/usr/bin/env node

const chokidar = require('chokidar');
const build = require('./build');
const webSocket = require('ws');
const exec = require('child_process').exec;

const buildCss = build.buildCss;
const buildJs = build.buildJs;
const createDirs = build.createDirs;

// Create new WebSocketServer for handling browser notifications of
// new bundles
const WebSocketServer = webSocket.Server;
const wss = new WebSocketServer({ port: 3002 });

const watcherOldCss = chokidar.watch('assets/css/old/**/*.css', {
  atomic: true,
  persistent: true
});
const watcherNewCss = chokidar.watch('assets/css/**/*.css', {
  ignored: /old/,
  atomic: true,
  persistent: true
});
const watcherJs = chokidar.watch([ 'assets/js/**/*.js', 'charmming/urls.json' ], {
  atomic: true,
  persistent: true
});
const watcherPy = chokidar.watch([ 'charmming/**/*.py', 'scheduler/**/*.py', 'charmming/urls.json' ], {
  atomic: true,
  persistent: true
});

// Build CSS and JS on changes
watcherOldCss.on('change', buildCss('old', wss));
watcherNewCss.on('change', buildCss('new', wss));
watcherJs.on('change', buildJs(wss));

// Restart Docker gunicorn instance on Python changes
watcherPy.on('change', function() {
  build.log('Restarting Gunicorn instance', { name: 'Python Watcher' });

  const restartCmd = 'docker exec charmming kill -HUP 1';

  exec(restartCmd);
});

process.on('SIGINT', () => {
  // Close watchers
  watcherOldCss.close();
  watcherNewCss.close();
  watcherJs.close();
  wss.close();
  process.exit();
});

// Make sure `public` directory exists
// Build files initially
createDirs();
buildCss('old', wss)();
buildCss('new', wss)();
buildJs(wss)();
