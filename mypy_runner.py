#!/usr/bin/env python3

import os
import subprocess
import inspect

filename = inspect.stack()[0][1]
script_dir = os.path.dirname(filename)

def create_init_files(dir):
  for path, name, filenames in os.walk(dir):
    if path.endswith("__pycache__"):
      continue

    # Only create __init__.pyi files if __init__.py file does not exist
    if not os.path.isfile("{}/__init__.py".format(path)):
      open("{}/__init__.pyi".format(path), 'a').close()

def remove_init_files(dir):
  for path, name, filenames in os.walk(dir):
    init_file = "{}/__init__.pyi".format(path)

    if os.path.isfile(init_file) and os.path.getsize(init_file) == 0:
      os.remove("{}/__init__.pyi".format(path))

base_classes_to_ignore = [ 'charmming.models', 'charmming.views', 'charmming.mixins' ]
imports_to_ignore = [ 'django', 'termcolor' ]

# Create the necessary __init__.pyi files
create_init_files(os.path.join(os.path.dirname(os.path.abspath(__file__)), "charmming"))
create_init_files(os.path.join(os.path.dirname(os.path.abspath(__file__)), "schedd"))

# Run mypy on all files given
env = os.environ.copy()
env['MYPYPATH'] = script_dir

# We don't check /charmming currently
cmd = [
  'mypy',
  '--ignore-missing-imports',
  '--strict-optional',
  '{}/schedd'.format(script_dir)
]
proc = subprocess.run(cmd, env=env, stdout=subprocess.PIPE)
output = bytes.decode(proc.stdout.strip(), 'utf-8')

# Remove all __init__.pyi files
remove_init_files(os.path.join(os.path.dirname(os.path.abspath(__file__)), "charmming"))
remove_init_files(os.path.join(os.path.dirname(os.path.abspath(__file__)), "schedd"))

# Take mypy output and augment it to ignore specific errors
raw_lines = output.split('\n')
mypy_output = []
skip_current_line = False
skip_next_line = False

for i, line in enumerate(raw_lines):
  # Skip line if explicitly told to
  if skip_next_line:
    skip_next_line = False
    continue

  # Don't show the typeshed error
  if line.find("Stub files are from https://github.com/python/typeshed") > -1:
    continue

  # Don't show silent imports error either
  if line.find('Perhaps setting MYPYPATH or using the "--silent-imports" flag would help') > -1:
    continue

  # Skip notes
  if line.find('note: ') > -1:
    continue

  # Ignore if it is complaining about django imports
  for imp in imports_to_ignore:
    if line.find("error: No library stub file for module '{}".format(imp)) > -1:
      skip_current_line = True
      break

  # Ignore this and next line if complaining about specific base class
  # This happens if we import and use a class as a base class from
  # one of our directories that affects the module definition at
  # runtime. Mypy doesn't support this currently, so we ignore it.
  for base_class in base_classes_to_ignore:
    if line.find('error: Invalid type "{}'.format(base_class)) > -1:
      skip_current_line = True

    try:
      if raw_lines[i+1].find('error: Invalid base class') > -1:
        skip_next_line = True
    except IndexError:
      pass

  if skip_current_line:
    skip_current_line = False
    continue

  mypy_output.append(line)

mypy_output = "\n".join(mypy_output)

# Return mypy output
print(mypy_output)

# Maybe, eventually, we will support this, but for now, if there is a type error,
# we don't exit with a non-0 status code
# if len(mypy_output) > 0:
#   exit(1)
