module.exports = {
  rules: {
    // Only allow lowercase hex, e.g. #fff000, not #FFF000
    "color-hex-case": "lower",

    // No invalid hex allowed
    "color-no-invalid-hex": true,

    // Always expect quotes around font names where expected, e.g. Arial is okay, but "Times New Roman" needs quotes
    "font-family-name-quotes": "always-where-recommended",

    // Functions need spaces for operators, e.g. calc(1px + 2px), not calc(1px+2px)
    "function-calc-no-unspaced-operator": true,

    // Never allow multiline function calls with trailing comma, e.g. translate: transform(1,
    // 1)
    "function-comma-newline-after": "never-multi-line",

    // Same as above except with commas coming before, e.g. translate: transform(1
    // ,1)
    "function-comma-newline-before": "never-multi-line",

    // Commas in function calls have to have a space after them
    "function-comma-space-after": "always",

    // Commas in function calls should not have spaces before them
    "function-comma-space-before": "never",

    // Need to use CSS standard directions for gradients
    "function-linear-gradient-no-nonstandard-direction": true,

    // Always use lower case function names, e.g. calc() vs CALC() or Calc()
    "function-name-case": "lower",

    // Never allow newlines inside of function calls
    "function-parentheses-newline-inside": "never-multi-line",

    // URLs must always be quoted
    "function-url-quotes": "always",

    // Functions must always have a space after them
    "function-whitespace-after": "always",

    // Always must have a leading 0 for decimal numbers, e.g. 0.2 vs .2
    "number-leading-zero": "always",

    // No trailing zeros, e.g. 1.000 should be 1
    "number-no-trailing-zeros": true,

    // Strings shouldn't contain newlines
    "string-no-newline": true,

    // Zero length dimensions shouldn't have a unit after them
    // Does not apply to timing e.g. 0s
    "length-zero-no-unit": true,

    // Animation times need to be 100ms or longer
    // "time-no-imperceptible": true,

    // Unit must always be lower case
    "unit-case": "lower",

    // No unknown-to-spec units
    "unit-no-unknown": true,

    // Only allow em, rem, deg, s
    "unit-whitelist": [ "em", "rem", "deg", "s", '%', 'vh' ],

    // Keywords must be lowercase, e.g. block vs Block
    "value-keyword-case": "lower",

    // No vendor prefixes for values. This is handled for us by postcss
    "value-no-vendor-prefix": true,

    // No newlines after commas in value lists
    "value-list-comma-newline-after": "never-multi-line",

    // Same as above except before
    "value-list-comma-newline-before": "never-multi-line",
  }
};
