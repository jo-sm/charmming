def command(data, state):
  return {
    'hello': 'world'
  }, state

def verify(data):
  # The only thing necessary in this datagram is "command: hello"
  return True
