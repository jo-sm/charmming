from schedulerd.protocol import error
from schedulerd.job import start_job

def command(data, state):
  job_id, state = start_job(data, state)

  if not job_id:
    return error(message='Could not start job')

  return {
    'status': 'ok',
    'data': {
      'id': job_id
    }
  }, state

def verify(data):
  if not data.get('scripts'):
    return False

  if type(data['scripts']) != list:
    return False

  if len(data['scripts']) == 0:
    return False

  return True
