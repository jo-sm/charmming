from schedulerd.protocol import error
from schedulerd.job import kill_job

def command(data, state):
  status, state = kill_job(data.get('id'), state)

  if not status:
    result = error(message='Could not kill job')
  else:
    result = {
      'status': 'ok',
      'data': {
        'id': data.get('id')
      }
    }

  return result, state

def verify(data):
  if not data.get('id'):
    return False

  if type(data['id']) != int:
    return False

  return True
