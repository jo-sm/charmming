#!/usr/bin/env python3

from charmming.utils.log import log_wrapper
from charmming.utils.encryption import encrypt

from schedulerd.protocol import bad_protocol
from schedulerd.job import get_state
from schedulerd.utils import states, meta_states, read, write

import aiohttp
import asyncio
import datetime
import importlib
import signal
import sys
import json
import os

log = log_wrapper('schedulerd', 'info')

# asyncio_logger = logging.getLogger('asyncio')
# asyncio_logger.setLevel(logging.DEBUG)

class SchedulerDaemon:
  """
  Scheduler server. Based on simple_tcp_server.py
  """

  def __init__(self, loop):
    self.loop = loop

    self.server_instance = None
    self.clients = {}
    self.state = {}

  def start(self):
    """
    Start the Scheduler Server
    """
    # Update the status for each batch
    self.loop.create_task(self.__update_status())

    self.server_instance = self.loop.run_until_complete(
      asyncio.start_server(self.__accept_client, '127.0.0.1', 9995, loop=self.loop)
    )

    log('Started Scheduler server.')

  def stop(self):
    if self.server_instance:
      self.server_instance.close()
      # self.loop.run_until_complete(self.server_instance.wait_closed())
      self.server_instance = None

  def __accept_client(self, reader, writer):
    task = asyncio.Task(self.__handle_client(reader, writer))
    self.clients[task] = {
      'reader': reader,
      'writer': writer
    }

    peername = writer.get_extra_info('peername')

    log('New client connected from {}'.format(peername))

  async def __update_status(self):
    batches = self.state.get('batches', {})

    for batch_id in batches:
      batch = batches[batch_id]
      current_time = datetime.datetime.now()

      for job_id in batch:
        job = batch[job_id]

        if job.get('ended'):
          # Don't check the status on an ended job
          continue

        job_state = get_state(job)

        if job_state == job['state']:
          continue

        log('Current job state: {}'.format(job['state']))
        log('New job state: {}'.format(job_state))

        if job_state == states['unknown']:
          log('Error fetching current state information for job {}'.format(job.id))
          continue

        if job_state in meta_states['finished']:
          # The job has ended (failure or success)
          job['ended'] = current_time

          for batch_job_id in batch:
            if batch_job_id == job_id:
              # No reason to check the job we are trying to check now
              continue

            batch_job = batch[batch_job_id]
            batch_job_state = get_state(batch_job)

            if batch_job_state in meta_states['failed']:
              job['state'] = states['failed']
              break

        if job_state == states['running']:
          # Running
          job['started'] = current_time

        log('Updating {} state to {}'.format(job['id'], job_state))
        job['state'] = job_state

        if job_id == batch_id:
          batch_job = batch[batch_id]

          # Send http request to front end
          key = os.environ.get('CHARMMING_SECRET_KEY').encode('utf-8')

          data = encrypt(key, json.dumps({'state': batch_job['state']}).encode('utf-8'))

          async with aiohttp.request(
            'put',
            'http://127.0.0.1:8080/task/{}'.format(batch_job.get('task_id')),
            data=data,
            headers={
              'accept': 'application/json',
              'x-charmming-app': 'schedulerd'
            }
          ) as response:
            status = response.status

            if status == 200:
              log('Successfully updated task {} to state {}'.format(batch_id, batch_job['state']))
              continue
            else:
              log('Task update response from CHARMMing web app not 200: {}'.format(response.status))

    # Run in the loop again
    await asyncio.sleep(0.5)

    self.loop.create_task(self.__update_status())

  async def __handle_client(self, reader, writer):
    peername = writer.get_extra_info('peername')

    while True:
      data = await read(reader)

      log('Received data from {}'.format(peername))
      result = self.__parse(data)

      log('Result is: {}'.format(result))

      if result.get('goodbye'):
        # Goodbye command means client disconnection
        log('Client disconnection from {}'.format(peername))
        await write(writer, result)

        break

      await write(writer, result)

  def __handle_client_disconnect(self, task):
    del self.clients[task]

  def __handle_meta(self, command, meta):
    if command == 'submit':
      if meta.get('batch'):
        self.batches[meta.get('id')] = meta.get('batch')

    elif command == 'cancel':
      pass

  def __parse(self, data):
    log(data)

    command_name = data.get('command')
    data = data.get('data')

    try:
      module = importlib.import_module('schedulerd.commands.{}'.format(command_name))
    except ModuleNotFoundError:
      return bad_protocol()

    command = getattr(module, 'command')

    verify = None

    if hasattr(module, 'verify'):
      verify = getattr(module, 'verify')

    if verify and not verify(data):
      log('Data did not verify')
      return bad_protocol()

    result, state = command(data, self.state)

    self.state = state

    log('Returning result {}'.format(result))

    return result

def handle_term(server, loop):
  def result_function(*args) -> None:
    log("Got shut down signal.")
    loop.stop()
    server.stop()
    sys.exit(0)

  return result_function

def handle_hup(server):
  def result_function(*args) -> None:
    log("Received SIGHUP")

  return result_function

def main():
  loop = asyncio.get_event_loop()
  # loop.set_debug(True)
  server = SchedulerDaemon(loop)

  signal.signal(signal.SIGINT, handle_term(server, loop))
  signal.signal(signal.SIGTERM, handle_term(server, loop))
  signal.signal(signal.SIGHUP, handle_hup(server))

  server.start()

  loop.run_forever()

if __name__ == "__main__":
  main()
