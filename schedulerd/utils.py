from charmming.utils.datagram import pack, unpack

import importlib
import struct

states = {
  'queued': 'QUEUED',
  'cancelled': 'CANCELLED',
  'killed': 'KILLED',
  'running': 'RUNNING',
  'completed': 'COMPLETED',
  'failed': 'FAILED',
  'unknown': 'UNKNOWN'
}

meta_states = {
  'finished': [ states['failed'], states['completed'], states['killed'], states['cancelled'] ],
  'failed': [ states['failed'], states['killed'], states['cancelled'] ],
  'running': [ states['running'] ],
  'queued': [ states['queued'] ]
}

def get_interface(name):
  try:
    return importlib.import_module('schedulerd.interfaces.{}'.format(name))
  except ModuleNotFoundError:
    return None

async def read(reader):
  raw_length = await reader.read(4)

  try:
    length = struct.unpack('>I', raw_length)[0]
  except struct.error:
    return None

  raw = await reader.read(length)

  data = unpack(raw)

  return data

async def write(writer, data):
  packed = pack(data)
  length = len(packed)
  packed_length = struct.pack('>I', length)

  writer.write(packed_length)
  writer.write(packed)
  await writer.drain()
