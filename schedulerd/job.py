from charmming.utils.log import log_wrapper

from schedulerd.utils import get_interface, states

from typing import Optional, Tuple

import datetime
import os

log = log_wrapper('schedd')
interface = get_interface('slurm')

apps = {
  'qchem': 'qchem',
  'charmm': 'charmm',
  'charmm-mscale': 'charmm-ompi',
  'propka': 'propka'
}

def start_job(data: dict, state: dict) -> Tuple[Optional[int], Optional[dict]]:
  task_dir: str = data['dir']
  task_id: int = data['task_id']
  scripts: list = data['scripts']
  coordinates: Optional[dict] = data.get('coordinates')

  sched_ids = interface.new(task_dir, scripts, coordinates)

  if not sched_ids:
    log("Job creation failure for scripts {}".format(scripts), level="warning")
    return None, None

  batch_id = sched_ids[-1]
  created_time = datetime.datetime.now()

  batch = {}

  for i in range(len(scripts)):
    sched_id = sched_ids[i]
    exe = scripts[i]['exe']
    script = scripts[i]['path']

    batch[sched_id] = {
      'id': sched_id,
      'task_id': task_id,
      'dir': task_dir,
      'state': states['queued'],
      'exe': exe,
      'script': script,
      'queued': created_time
    }

    log("Created {} OK".format(scripts[i]), level="debug")

  # Update status if necessary
  for i in range(len(scripts)):
    sched_id = sched_ids[i]
    job = batch[sched_id]

    log('Getting state for {}'.format(sched_id), level='debug')

    job_state = get_state(job)

    log('Job state: {}'.format(job_state))

    if job_state != states['queued']:
      batch[sched_id]['state'] = job_state

    log("Exiting start_job with batch id {} for script {} and state at {}.".format(batch_id, scripts[i], job_state), level="debug")

  if not state.get('batches'):
    state['batches'] = {}

  state['batches'][batch_id] = batch

  return batch_id, state

def kill_job(batch_id: int, state: dict) -> Tuple[bool, dict]:
  log("kill_job requested for batch id: {}".format(batch_id), level='debug')

  # Get the batch ID to look up all jobs in this batch with
  if not state.get('batches'):
    return False, state

  if not state['batches'].get(batch_id):
    log('Cannot kill job: Batch id does not exist: {}'.format(batch_id))
    return False, state

  jobs = state['batches'][batch_id]
  current_time = datetime.datetime.now()

  # Kill all jobs in the scheduler first
  for scheduler_id in jobs:
    interface.kill(scheduler_id)

    # Update all the job statuses
    jobs[scheduler_id]['state'] = states['failed']
    jobs[scheduler_id]['ended'] = current_time

  return True, state

def get_state(job):
  job_id = job['id']

  job_state = interface.state(job_id)

  if job_state != job['state']:
    if job_state == states['completed']:
      # we need to figure out if the job is *really* done or failed
      outfile = os.path.join(job['dir'], "{}.out".format(job_id))

      if job['exe'] in list(apps.keys()):
        # TODO: Move this logic into separate files
        # If this is not true, we default to ended
        if job['exe'] == apps['qchem']:
          log("Checking for \"Have a nice day\" in {}".format(outfile))
          phrase = "Thank you very much for using Q-Chem.  Have a nice day."
        else:
          # Default application is CHARMM
          log("Checking for normal termination in {}".format(outfile))
          phrase = "NORMAL TERMINATION BY NORMAL STOP"

        try:
          file_io = open(outfile, 'r')
          file = file_io.read()
          file_io.close()
        except FileNotFoundError:
          log('Could not find outfile {}'.format(outfile), level='warning')
          file = ''

        match = file.find(phrase)

        if match == -1:
          log('Uh oh... could not find phrase {} in {}'.format(phrase, outfile), level='warning')
          return states['failed']
        else:
          return states['completed']
    else:
      return job_state
  else:
    return job_state
