from charmming.utils.log import log

from tempfile import mkstemp

import os
import time
import string
import subprocess
import re

class SchedulerInterface:
  def __init__(self):
    # set up condor environment variables
    self.subcmd = "condor_submit -verbose"
    self.dagcmd = "condor_submit_dag"
    self.killcmd = "condor_rm"
    self.querycmd = "condor_q"
    os.environ['CONDOR_CONFIG'] = "/opt/condor-6.8.4/etc/condor_config"

  def get_state(self, condor_id):
    status, out = subprocess.getstatusoutput("{} {}".format(self.querycmd, condor_id))

    if status != 0:
      return -1

    for line in out.split('\n'):
      line = line.strip()
      if line.startswith(condor_id):
        # job is queued or running, for now return running
        larr = string.splitfields(line)

        if larr[5] == 'R':
          return 2
        else:
          return 1

    # if the job exists and is not running, it must be done
    return 3

  def kill_job(self, condor_id):
    status, out = subprocess.getstatusoutput("{} {}".format(self.killcmd, condor_id))

    if status != 0:
      return -1
    return 0

  def new_job(self, user, dir, exe, scripts):
    try:
      os.chdir(dir)
    except:
      log("Could not change to directory '{}".format(dir), level="debug")
      return None

    log("Trying to start job {} {} {} {}".format(user, dir, exe, scripts), level="debug")

    if len(scripts) > 1:

      # this needs to be done in two steps -- first create the individual script files
      # and then create the DAG

      # create individual script files
      subfiles = []
      for script in scripts:
        tfo = mkstemp(prefix="lcsub-dags-", dir="/tmp")
        tf = os.fdopen(tfo[0], 'w+b')
        tf.write("# Condor submit file for learn CHARMM\n")
        tf.write("# Created {} for charmming user id {}\n\n".format(time.ctime(), user))
        tf.write("Executable      = /usr/local/charmming/gfortran-xxlg.one\n")
        tf.write("Universe        = vanilla\n")
        tf.write("input           = {}\n".format(script))
        tf.write("output          = {}\n".format(script.replace("inp", "out")))
        tf.write("error           = {}\n".format(script.replace("inp", "err")))
        tf.write("log             = {}\n".format(script.replace("inp", "log")))
        tf.write("Queue\n")

        tf.close()
        subfiles.append(tfo[1])

      # create the DAG file
      tfd = mkstemp(prefix="lcsub-dagm-", dir="/tmp")
      tf = os.fdopen(tfd[0], 'w+b')
      tf.write("# DAG job master for learn CHARMM\n\n")

      for i in range(len(scripts)):
        tf.write("Job STEP{}  {}\n".format(i + 1, subfiles[i]))

      tf.write("\n")

      for j in range(len(scripts) - 1):
        tf.write("PARENT STEP{} CHILD STEP{}\n".format(j + 1, j + 2))

      tf.close()
      cmd = "{} {}".format(self.dagcmd, tfd[1])

    else:
      script = scripts[0]
      tfo = mkstemp(prefix="lcsub-", dir="/tmp")
      tf = os.fdopen(tfo[0], 'w+b')
      tf.write("# Condor submit file for learn CHARMM\n")
      tf.write("# Created {} for charmming user id %d\n\n" % (time.ctime(), user))
      tf.write("Executable      = /usr/local/charmming/gfortran-xxlg.one\n")
      tf.write("Universe        = vanilla\n")
      tf.write("input           = {}\n".format(script))
      tf.write("output          = {}\n".format(script.replace("inp", "out")))
      tf.write("error           = {}\n".format(script.replace("inp", "err")))
      tf.write("log             = {}\n".format(script.replace("inp", "log")))
      tf.write("Queue\n")
      tf.close()
      cmd = "{} {}".format(self.subcmd, tfo[1])

    log("Issuing {}".format(cmd), level="debug")
    status, out = subprocess.getstatusoutput(cmd)

    log("Result: {}".format(out), level="debug")

    if status != 0:
      log("Bad status from command.", level="debug")
      return None

    if len(scripts) > 1:
      # DAG submission

      # hack to force rescheduling to avoid the 5 minute wait in DAG jobs...
      s2, o2 = subprocess.getstatusoutput("condor_reschedule -all")
      clmatch = re.compile("submitted to cluster ([0-9]+)")

      for line in out.split("\n"):
        x = clmatch.search(line)
        if x:
          condorID = x.group(1) + ".0"
          log("Got ID {}".format(condorID), level="debug")

          # unlike PBS, we can't determine job IDs in advance so assign all scripts
          # the ID of the DAG manager.
          rlist = []
          for i in range(len(scripts)):
            rlist.append(condorID)
          return rlist
    else:
      # regular condor submission
      for line in out.split("\n"):
        line = line.strip()
        if line.startswith("** Proc"):
          line = line.replace("** Proc ", "")
          line = line.replace(":", "")
          condorID = line

          log("Got ID".format(condorID), level="debug")
          return [ condorID ]

    log("Did not find job number", level="debug")
    return None
