from charmming.utils.log import log_wrapper
from charmming.settings.base import PROJECT_ROOT

from schedulerd.utils import states

from typing import Optional, List

import pystache
import os
import subprocess

log = log_wrapper('schedd')

def state(job_id: str) -> str:
  squeue_cmd = [
    'squeue',
    '-l',
    '-o',
    '%T',
    '-h',
    '-j',
    "{}".format(job_id)
  ]

  process = subprocess.Popen(squeue_cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
  process.wait()

  if process.returncode != 0:
    # It doesn't exist
    log("Did not find {} -- returning unknown state".format(job_id), level="debug")
    return states['unknown']

  stdout = process.stdout.read().decode('utf-8')
  state = stdout.strip()

  if state == '':
    # If the id doesn't show up in squeue, it will show up in sacct
    sacct_cmd = [
      'sacct',
      '-P',
      '-o',
      'State',
      '-n',
      '-j',
      "{}".format(job_id)
    ]

    process = subprocess.Popen(sacct_cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    process.wait()

    stdout = process.stdout.read().decode('utf-8')
    state = stdout.strip()

  log('Received state {} for job {}'.format(state, job_id), level='debug')

  # Queued
  if state == 'PENDING':
    return states['queued']

  # Running
  elif state == 'RUNNING' or state == 'COMPLETING' or state == 'CONFIGURING':
    return states['running']

  # Finished
  elif state == 'COMPLETED':
    return states['completed']

  # Stopped/failed
  elif state == 'SUSPENDED' or state == 'FAILED':
    return states['failed']

  # Manually canceled
  elif state == 'CANCELLED':
    return states['cancelled']

  # Other state
  else:
    return states['unknown']

def kill(job_id: int) -> int:
  scancel_cmd = [
    'scancel',
    "{}".format(job_id)
  ]

  process = subprocess.Popen(scancel_cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
  process.wait()

  if process.returncode != 0:
    log("Failed to kill {}".format(job_id), level="debug")
    return -1

  log("Successfully killed {}".format(job_id), level="debug")
  return 0

def new(task_dir: str, scripts: List, coordinates: Optional[dict]) -> Optional[List[int]]:
  scheduler_path = os.path.join(PROJECT_ROOT, 'schedd')
  template_path = os.path.join(scheduler_path, 'templates', 'slurm.template')
  template = ''

  with open(template_path, 'r', encoding='utf-8') as io:
    template = io.read()

  ids: List[int] = []

  # Submit the initial pre_task script and hold it until all scripts have been submitted
  pre_task_script = '/scripts/pre_task.py'
  arguments = []

  if coordinates:
    files: list = coordinates['files']
    coordinate_paths = ','.join([ files[type] for type in files ])
    arguments.append('--coordinates')
    arguments.append('{}'.format(coordinate_paths))

  pre_task_id = __submit_job(pre_task_script, dir=task_dir, arguments=arguments, hold=True)

  if not pre_task_id:
    return None

  ids.append(pre_task_id)

  for i in range(len(scripts)):
    script = scripts[i]
    log("Trying script {}".format(script), level="debug")

    nproc = script['nproc']
    exe = script['exe']
    path = script['path']

    template_values: dict = {}

    template_values['num_tasks'] = 1
    template_values['num_cpus'] = nproc
    template_values['job_name'] = 'charmming-job'

    if len(ids) > 0:
      template_values['dependency'] = ids[-1]

    if exe.startswith('charmm'):
      body = "{} < {}".format(exe, path)

    elif exe.startswith('qchem'):
      body = "{} {} {}".format(exe, path, path.replace('inp', 'out'))

    elif exe.startswith('propka'):
      body = "{} {}".format(exe, path)

    else:
      body = "{} < {}".format(exe, path)

    template_values['body'] = body

    rendered_template = pystache.render(template, template_values)
    slurm_script_path = os.path.join(task_dir, 'slurm_job_{}.sh').format(i)

    with open(slurm_script_path, 'w') as io:
      io.write(rendered_template)

    job_id = __submit_job(slurm_script_path)

    if not job_id:
      for id in ids:
        kill(id)

      return None

    ids.append(job_id)

  # Release the pre_task script
  log('Releasing {}'.format(pre_task_id))
  released = __release(pre_task_id)

  if not released:
    return None

  # Remove the pre_task id, since it's not necessary for the Scheduler
  ids = ids[1:]

  log('Returning submitted ids {}'.format(ids))
  return ids

def __release(job_id) -> bool:
  release_cmd = [
    'scontrol',
    'release',
    '{}'.format(job_id)
  ]

  process = subprocess.Popen(release_cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
  process.wait()

  if process.returncode != 0:
    return False

  return True

def __submit_job(script_path, **kwargs) -> Optional[int]:
  log('Submitting {}'.format(script_path))
  slurm_cmd = [ 'sbatch' ]

  if kwargs.get('hold'):
    slurm_cmd.append('--hold')

  slurm_cmd.append(script_path)

  if kwargs.get('arguments'):
    slurm_cmd.extend(kwargs['arguments'])

  job_dir = kwargs.get('dir', os.path.dirname(script_path))

  log('Queuing {}'.format(' '.join(slurm_cmd)))

  process = subprocess.Popen(slurm_cmd, cwd=job_dir, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
  process.wait()

  if process.returncode != 0:
    return None

    log('Could not submit script {}. Return code {}'.format(script_path, process.returncode))

    return None

  stdout = process.stdout.read().decode('utf-8')
  job_id = int(stdout.split()[-1])

  return job_id
