from charmming import charmming_config
from charmming.utils.log import log

from tempfile import mkstemp

from typing import Optional, List

import os
import time
import subprocess
import socket

class PBSInterface:
  def __init__(self):
    self.subcmd = "qsub"
    self.querycmd = "qstat"
    self.killcmd = "qdel"

  def get_state(self, pbs_id: str) -> int:
    log("Asked to look up {}".format(pbs_id), level="debug")

    status, out = subprocess.getstatusoutput("%s %s" % (self.querycmd, pbs_id))

    if status != 0:
      # damn thing must not exist
      log("Did not find {} -- returning 3".format(pbs_id), level="debug")
      return 3

    for line in out.split('\n'):
      line = line.strip()
      log("line> {}".format(line), level="debug")

      if line.startswith(pbs_id + "." + socket.gethostname()):
        log("OK found it", level="debug")

        larr = line.split()

        if larr[4] == 'R' or larr[4] == 'E' or larr[4] == 'H':
          log("Running return 2", level="debug")

          return 2

        elif larr[4] == 'Q':
          log("Queued return 1", level="debug")
          return 1
    # if the job exists and is not running, it must be done
    log("Did not find it, returning 3", level="debug")
    return 3

  def kill_job(self, pbs_id: str) -> int:
    # returns 0 on success or an error code:
    # -1: error
    log("Asked to kill {}".format(pbs_id), level="debug")

    cmd = "{} {}".format(self.killcmd, pbs_id)
    status, out = subprocess.getstatusoutput(cmd)

    if status != 0:
      log("Failed to kill {}".format(pbs_id), level="debug")
      return -1

    log("Successfully killed {}".format(pbs_id), level="debug")
    return 0

  def new_job(self, user_id, dir: str, scripts: List[str], exelist: List[str], nprlist: List[str]) -> Optional[List[str]]:
    try:
      os.chdir(dir)
    except:
      return None

    log("Trying to start job {} {} {}".format(user_id, dir, scripts), level="debug")

    # create a temp file and put the PBS junk in it
    ids = [] # type: List[str]
    for i in range(len(scripts)):
      log("Trying script {}".format(scripts[i]), level="debug")
      log("exelist: {}".format(exelist), level="debug")

      if not scripts[i].startswith('customscript'):
        log("Is NOT custom script!", level="debug")

        tfo = mkstemp(prefix="charmming-", dir="/tmp")
        tf = os.fdopen(tfo[0], 'w+b')
        tf.write("#!/bin/bash\n")
        tf.write("#PBS -l nodes=1:ppn={}\n".format(int(nprlist[i])))
        tf.write("#PBS -N charmming-job\n")
        tf.write("#PBS -j oe\n")

        if len(ids) > 0:
          tf.write("#PBS -W depend=afterok:{}\n".format(ids[-1]))

        tf.write("# PBS job file for CHARMMING\n")
        tf.write("# Created {} for charmming user id {}\n".format(time.ctime(), user_id))
        tf.write("# Number of processes requested: {}\n\n".format(nprlist[i]))
        tf.write("export GFORTRAN_UNBUFFERED_ALL=Y\n")
        tf.write("cd {}\n".format(dir))
        tf.write("umask 0022\n")

        # we can't rely on the number of processors being reliable. We make this check BEFOREHAND and use the right exec before passing it into here.
        # TODO: Update logic further.
        if exelist[i] == charmming_config.apps['charmm'] or exelist[i] == charmming_config.apps['charmm-mscale'] or exelist[i] == charmming_config.apps['charmm-apbs']:
          tf.write("{} < {} >& {}\n".format(exelist[i], scripts[i], scripts[i].replace("inp", "out")))

        elif exelist[i] == charmming_config.apps['qchem']:
          tf.write("{} {} > {}\n".format(exelist[i], scripts[i], scripts[i].replace("inp", "out")))

        elif exelist[i] == charmming_config.apps['propka']:
          tf.write("{} {}".format(exelist[i], scripts[i]))  # in this case the "script' is really just a PDB file.

        else:
          tf.write("{} < {} >& {}\n".format(exelist[i], scripts[i], scripts[i].replace("inp", "out")))

        tf.close()
        cmd = "{} {}".format(self.subcmd, tfo[1])

      else:
        # the executable is something we can qsub directly
        log("Is INDEED a custom script!", level="debug")
        cmd = "{} {}".format(self.subcmd, exelist[i])

      log("Issuing {}".format(cmd), level="debug")

      status, out = subprocess.getstatusoutput(cmd)

      if status != 0:
        log("Bad status from command.", level="debug")
        return None

      for line in out.split("\n"):
        try:
          line = line.strip()
          pbsID = line.split('.')[0]
          ids.append(pbsID)
        except:
          log("Bad result from job submission.", level="debug")
          return None

    return ids
