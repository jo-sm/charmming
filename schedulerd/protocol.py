def bad_protocol():
  return error(message='Bad protocol')

def error(*args, **kwargs):
  message = kwargs.get('message', 'Error has occurred')

  return {
    'status': 'error',
    'error': message
  }
