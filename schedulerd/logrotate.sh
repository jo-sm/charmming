#!/bin/bash -x

mv schedulerd.log.4.gz schedulerd.log.5.gz >& /dev/null
mv schedulerd.log.3.gz schedulerd.log.4.gz >& /dev/null
mv schedulerd.log.2.gz schedulerd.log.3.gz >& /dev/null
mv schedulerd.log.1.gz schedulerd.log.2.gz >& /dev/null
mv schedulerd.log schedulerd.log.1
killall -HUP schedulerd.py
gzip schedulerd.log.1
