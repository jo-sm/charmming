#!/usr/bin/env bash
source utils.sh

# Delete previously named containers
docker rm postgres 1>/dev/null 2>/dev/null
docker rm charmming 1>/dev/null 2>/dev/null

end() {
  printf "Stopping postgres container... "
  docker stop postgres

  exit 0
}

trap end SIGINT SIGTERM

# Start Postgres
postgres_id=$(docker run \
  -e "POSTGRES_PASSWORD=$postgres_pass" \
  --name postgres \
  -p 127.0.0.1:5432:5432 \
  -v $postgres_db_data_dir:/var/lib/postgresql/data \
  -t \
  -d \
  postgres:9.6)

# Start Charmming
docker run \
  --name charmming \
  --rm \
  --link postgres:db \
  --label charmming \
  -e "CHARMMING_SECRET_KEY=test" \
  -e "CHARMMING_DB_NAME=charmming" \
  -e "CHARMMING_DB_HOST=db" \
  -e "CHARMMING_DB_USER=$postgres_user" \
  -e "CHARMMING_DB_PASS=$postgres_pass" \
  -v ~/charmming:/charmming \
  -p 127.0.0.1:8080:8080 \
  -it \
  charmming:latest \
  /bin/bash

end
