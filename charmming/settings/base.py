import os

SITE_ID = 1
APPEND_SLASH = False

TIME_ZONE = 'EST5EDT'
LANGUAGE_CODE = 'en-us'

# DISABLE_TRANSACTION_MANAGEMENT = True

# This is the root of the charmming app, e.g. /charmming/charmming
APP_ROOT = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))

# This is the root of the project, e.g. /charmming
# This is one level up from the APP_ROOT
PROJECT_ROOT = os.path.abspath(os.path.join(APP_ROOT, '..'))

DATABASES = {
  'default': {
    'ENGINE': 'django.db.backends.postgresql',
    'NAME': os.environ.get('CHARMMING_DB_NAME'),
    'USER': os.environ.get('CHARMMING_DB_USER'),
    'PASSWORD': os.environ.get('CHARMMING_DB_PASS'),
    'HOST': os.environ.get('CHARMMING_DB_HOST'),
    'PORT': os.environ.get('CHARMMING_DB_PORT') or '5432'
  }
}

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = False

ROOT_URLCONF = 'charmming.urls'

AUTH_PROFILE_MODULE = "UserProfile"

# # List of callables that know how to import templates from various sources.
# TEMPLATE_LOADERS = (
#   'django.template.loaders.filesystem.Loader',
#   'django.template.loaders.app_directories.Loader',
#   # 'django.template.loaders.filesystem.load_template_source',
#   # 'django.template.loaders.app_directories.load_template_source',
#   # 'django.template.loaders.eggs.load_template_source',
# )

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    # 'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # ToDo: add CSRF middleware.
    # 'django.middleware.csrf.CsrfViewMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

TEMPLATES = [
  {
    'BACKEND': 'django.template.backends.django.DjangoTemplates',
    'DIRS': [
      os.path.join(PROJECT_ROOT, 'templates')
    ],
    'APP_DIRS': True,
    'OPTIONS': {
      'context_processors': [
        "django.contrib.auth.context_processors.auth",
        "django.template.context_processors.debug",
        "django.template.context_processors.i18n",
        "django.template.context_processors.media",
        "django.template.context_processors.static",
        "django.template.context_processors.tz",
        "django.contrib.messages.context_processors.messages"
      ],
      'builtins': [
        'charmming.utils.template',
      ]
    }
  }
]

# List of callables that know how to import templates from various sources.
# TEMPLATE_LOADERS = (
#   'django.template.loaders.filesystem.Loader',
#   'django.template.loaders.app_directories.Loader',
#   'django.template.loaders.filesystem.load_template_source',
#   'django.template.loaders.app_directories.load_template_source',
#   'django.template.loaders.eggs.load_template_source',
# )

# MIDDLEWARE_CLASSES = (
#   'django.middleware.common.CommonMiddleware',
#   'django.contrib.sessions.middleware.SessionMiddleware',
#   'django.contrib.auth.middleware.AuthenticationMiddleware',
#   'django.middleware.doc.XViewMiddleware',
# )

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.messages',
    'django.contrib.sessions',
    'django.contrib.admin',
    'django.contrib.sites',
    'django.contrib.staticfiles',

    'charmming',
)

# # all lessons in lesson_num_lis(lesson_config.py) will be put in extra_apps and then added into INSTALLED_APPS
# extra_apps = ()
# for num in lesson_num_lis:
#   extra_apps = extra_apps + ('charmming.lesson' + num,)

# INSTALLED_APPS = INSTALLED_APPS + extra_apps
