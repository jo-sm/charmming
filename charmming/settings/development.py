from charmming.settings.base import PROJECT_ROOT, TEMPLATES

import os

DEBUG = True
# TEMPLATE_DEBUG = True

TEMPLATES[0]['OPTIONS']['debug'] = True

# Absolute path to the directory that holds user uploaded media
MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'uploads')

# URL that handles the media served from MEDIA_ROOT.
# Example: "http://media.lawrence.com"
MEDIA_URL = '/uploads/'

ASSET_URL = '/'

# static URL config
STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static')
STATIC_URL = '/static/'

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = '/static/admin/'

# Manually set latency, for debugging
# In seconds
LATENCY = 0

# Set the log level for the `charmming` log
LOG_LEVEL = 'debug'
