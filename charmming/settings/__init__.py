# This file is separate from base.py because this contains
# logic, whereas base.py does not. Since we dynamically
# import environmental configs, and the environment
# configs rely on variables in base.py, we cannot put
# the base configs in here, otherwise it will result
# in a circular dependency

from charmming.utils.log import log_level_name_to_num

import os
import logging

# This import isn't directly used, but it is required
# when importing settings somewhere else.
from charmming.settings.base import * # noqa: F401, F403

# sys.meta_path = [ ModuleClassImporter() ] + sys.meta_path

ENVIRONMENT = os.environ.get('CHARMMING_ENV')

# Default to development environment
if not ENVIRONMENT:
  ENVIRONMENT = 'development'

try:
  settings_module = __import__('charmming.settings.{}'.format(ENVIRONMENT), fromlist=['*'])

  # dynamically import the specific settings module
  # as global
  for k, v in settings_module.__dict__.items():
    if not k.startswith('_'):
      globals()[k] = v

  if hasattr(settings_module, 'LOG_LEVEL'):
    logger = logging.getLogger('charmming')
    log_level = log_level_name_to_num(getattr(settings_module, 'LOG_LEVEL'))
    logger.setLevel(log_level)

except ImportError:
  pass

SECRET_KEY = os.environ.get('CHARMMING_SECRET_KEY')

if not SECRET_KEY:
  raise Exception('You must set a secret key in the CHARMMING_SECRET_KEY environment variable.')

REDIS_HOST = os.environ.get('CHARMMING_REDIS_HOST')

if not REDIS_HOST:
  raise Exception('You must set the Redis server host in the CHARMMING_REDIS_HOST environment variable.')
