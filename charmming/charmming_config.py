# domain URL
charmming_url = 'http://ctb2.lobos.nih.gov/charmming'

# PDB URL because they keep deciding to change it - only use this for cases not covered by the two below
pdb_base_url = "www.rcsb.org"

# PROTEIN downloads URL because apparently they want to change how you access that, too
pdb_protein_download_url = "files.rcsb.org"

# PROTEIN request string since they change this API on us a lot too
pdb_protein_download_pattern = "/download/{}.pdb" # use .format() on this later

# LIGAND downloads URL because it is not changed in sync with protein stuff
pdb_ligand_download_url = "www.rcsb.org"

# LIGAND request string since they change that part of the API on us too
pdb_ligand_download_pattern = "/pdb/files/ligand/{}_ideal.sdf" # use .format() on this later

# maximum number of atoms
maxatoms = 50000

# set to 1 if you have Q-Chem enabled in your CHARMM exe and want
# to allow your users to use QM/MM
haveqchem = 1

# root of the CHARMMing install
charmming_root = "/charmming"

# place where user directories are...
user_home = "/charmming/uploads"

# place where executables etc. are kept
data_home = "/usr/local/charmming"

# topology file to be used for proteins and nucleic acids ... these
# must live in data_home/toppar.
default_pro_top = "top_all36_prot.rtf"
default_pro_prm = "par_all36_prot.prm"
default_na_top = "top_all36_na.rtf"
default_na_prm = "par_all36_na.prm"
default_cgenff_top = "top_all36_cgenff.rtf"
default_cgenff_prm = "par_all36_cgenff.prm"

# Where AmberTools lives -- used for Antechamber topology/parameter
# determination.
amber_home = "/usr/local/charmming/amber12"

# methods to use to try to generate topology/parameters for BADHET
# atoms. These methods will be tried in order until one succeeds
# (or they all fail).
toppar_generators = 'cgenff,match,antechamber,genrtf'
dd_toppar_generators = 'cgenff,match' # 'match,cgenff'#,antechamber,genrtf'

# CGenFF host & port
cgenff_host = 'dogmans.umaryland.edu'
cgenff_port = 32108

# Path to all libraries needed by the CHARMM executables
lib_path = ''


# path to the MPI-enabled CHARMM executable
charmm_mpi_exe = "/usr/local/charmming/gfortran-xxlg-qc.ompi"

# path to mpirun (not used by default -- testing parallel)
mpirun_exe = "/bin/false"

# path to stride (needed for CG stuff)
stride_bin = "/usr/local/bin/stride"

# Default number of processors for MSCALE jobs. Replace once we have proper multi-proc intefaces.
default_mscale_nprocs = 2

# Default number of processors for CHARMM jobs.
default_charmm_nprocs = 1

# Default number of processors for QCHEM jobs
default_qchem_nprocs = 1

# dd targets
user_dd_targets_home = user_home + "/dd/targets"

# dd ligands
user_dd_ligands_home = user_home + "/dd/ligands"

# dd jobs
user_dd_jobs_home = user_home + "/dd/jobs"

# path to the DAIM executable and properties files
daim_exe = "/usr/local/charmming/drug_design/DAIM/bin/daim"
daim_param = user_home + "/dd/app_files/daim/daim.param"
daim_prop = user_home + "/dd/app_files/daim/daim.prop"
daim_weight = user_home + "/dd/app_files/daim/daim.prop"

# path to the SEED executable and parameter files
seed_exe = "/usr/local/charmming/drug_design/seed_send_3.3.5/seed_3.3.5"
seed_param = user_home + "/dd/app_files/seed/seed.par"

# path to the FFLD executable
ffld_exe = "/usr/local/charmming/drug_design/ffld_send_3.3/ffld_3.3_gcc4.0_static"
ffld_param = user_home + "/dd/app_files/ffld/FFLD_param_cgenff"

# path to VMD executable
vmd_exe = "/usr/local/bin/vmd"

# path to the FLEA exeutable and parameter files
flea_param = user_home + "/dd/app_files/flea/PARM.flea"
flea_exe = "/usr/local/charmming/drug_design/FLEA/flea_1.0_32b_static"

# path to the CHARMM files
# charmm_files = user_home + "/dd/app_files/charmm/"
charmm_files = data_home + "/toppar/"
# charmm_param = user_home + "/dd/app_files/charmm/top_all36_prot.rtf"
charmm_param = data_home + "/toppar/top_all36_prot.rtf"

# dd scripts
dd_scripts_home = user_home + "/dd/scripts"

# dd shell launch command
dd_submit_parallel = "qsub -cwd -l h_rt=00:20:00" # to enable parallel execution of docking commands
dd_submit_serial = "" # for serial execution of docking commands

# Limits on various calculations

# max atoms for all atom normal modes
max_nma_atoms = 1500

# step limits for minimization and dynamics
minimize_steplimit = 1000
dyna_steplimit = 1000

# docking parameters
docking_iterations = 1
ffld_energy_evaluations = 2000
ffld_generations = 100
clustering_energy_cutoff = 10

# qsar
# user_qsar_home = user_home + "/qsar"

# qsar jobs
# user_qsar_jobs_home = user_qsar_home + "/jobs"

# qsar jobs
# user_qsar_models_home = user_qsar_home + "/models"

# lessonmaker - leave this FALSE unless needed
lessonmaker_enabled = False

# apps enabled - these should get set at the installer leve, hopefully
apps = {
  'qchem': '/v/apps/qchem/4.0e/bin/qchem',
  'charmm': '/usr/local/charmming/c37b2-qc-apbs.one',
  'charmm-apbs': '/usr/local/charmming/c37b2-qc-apbs.one',
  'charmm-mscale': '/usr/local/charmming/c37b2-qc-mscale.ompi',
  'propka': 'propka31',
  'genrtf': '%s/genrtf-v3.3' % data_home
}

# I'd make the following a dict but we always iterate anyway to find out how big a box we need
solv_home = data_home + "/solvation"
# first index is rough box radius, second is the path to the box itself.
solvation_boxes = [
  (30, 890, solv_home + "/1water_15a_box.crd"),
  (50, 4057, solv_home + "/1water_25a_box.crd"),
  (70, 11255, solv_home + "/1water_35a_box.crd"),
  (100, 32456, solv_home + "/1water_50a_box.crd"),
  (216, 424277, solv_home + "/1water_108a_box.crd")
]
# path to the single threaded CHARMM executable
# path to the single threaded CHARMM executable
# charmm_exe = "/usr/local/charmming/c37b2-prelease.exe"
# charmm_apbs_exe = "/usr/local/charmming/c37b2-prelease.exe"
qchem_forcefield_path = '/v/apps/qchem/4.0e/aux/force_field/' # for QM/MM with Q-Chem
charmm_exe = "/usr/local/charmming/c37b2-qc-apbs.one"
charmm_apbs_exe = "/usr/local/charmming/c37b2-qc-apbs.one"
charmm_mscale_exe = "/usr/local/charmming/c37b2-qc-mscale.ompi"

register_captcha = False
register_paper = False

# note if register_captch is true, put your recaptcha secret here
recaptcha_secret = 'foo'
