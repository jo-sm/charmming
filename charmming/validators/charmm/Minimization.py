from charmming.validators import Validator

class Minimization(Validator):
  def validate(self):
    sanitized = self.sanitized

    if not self.options.get('sd_steps'):
      self.raise_exception('SD steps parameter (sd_steps) must be given.')

    if not self.options.get('abnr'):
      self.raise_exception('ABNR parameter (abnr) must be given.')

    if not self.options.get('gradient_tolerance'):
      self.raise_exception('Gradient tolerance parameter (gradient_tolerance) must be given.')

    if self.options.get('restain_backbone') and not self.options.get('backbone_force'):
      self.raise_exception('Backbone restraining (restrain_backbone) must also be given backbone_force paramter.')

    sanitized['sd_steps'] = self.options.get('sd_steps')
    sanitized['abnr'] = self.options.get('abnr')
    sanitized['gradient_tolerance'] = self.options.get('gradient_tolerance')

    if self.options.get('restain_backbone'):
      sanitized['restrain_backbone'] = True
      sanitized['backbone_force'] = self.options.get('backbone_force')

    return sanitized
