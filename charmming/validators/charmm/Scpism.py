from charmming.validators import Validator

class Scpism(Validator):
  def validate(self):
    sanitized = self.sanitized

    if self.options.get('qmmm') and self.options.get('oniom'):
      return self.raise_exception('Select either QM/MM or ONIOM model types for multi-scale modeling.')

    if self.options.get('gbmv') and self.options.get('scpism'):
      return self.raise_exception('Select either GBMV or SCPISM for implicit solvation.')

    if (self.options.get('gbmv') or self.options.get('scpism')) and self.options.get('pbc'):
      return self.raise_exception('PBC cannot be used with implicit solvation')

    if self.options.get('scpism'):
      sanitized['scpism'] = True

    if self.options.get('gbmv'):
      sanitized['gbmv'] = True

    if self.options.get('qmmm'):
      sanitized['qmmm'] = True

    if self.options.get('oniom'):
      sanitized['oniom'] = True

    return sanitized
