from charmming.validators import Validator

class Base(Validator):
  def validate(self):
    sanitized = self.sanitized

    sanitized['output_name'] = self.options.get('output_name')

    return sanitized
