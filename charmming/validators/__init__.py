from charmming.exceptions import ValidatorException

class Validator:
  def __init__(self, options, **kwargs):
    if kwargs.get('sanitized'):
      self.sanitized = kwargs.get('sanitized')
    else:
      self.sanitized = {}

    self.options = options

  def validate(self):
    raise NotImplementedError('Must be implemented by Validator child class.')

  def raise_exception(self, message):
    raise ValidatorException(message)
