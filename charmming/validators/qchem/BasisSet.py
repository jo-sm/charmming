from charmming.validators import Validator

class BasisSet(Validator):
  def validate(self):
    valid_basis_sets = [
      'sto-3g',
      '3-21g*',
      '6-31g*'
    ]

    sanitized = self.sanitized

    if not self.options.get('basis_set'):
      self.raise_exception('The basis_set parameter is required for this calculation.')

    if self.options.get('basis_set') not in valid_basis_sets:
      self.raise_exception('Provide a valid basis set for the basis_set parameter.')

    sanitized['basis_set'] = self.options.get('basis_set')

    return sanitized
