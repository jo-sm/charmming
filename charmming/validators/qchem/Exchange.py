from charmming.validators import Validator

class Exchange(Validator):
  def validate(self):
    valid_exchanges = [
      'hf',
      'b',
      'b3'
    ]
    valid_coorelations = [
      'lyp'
    ]

    sanitized = self.sanitized

    if not self.options.get('exchange'):
      self.raise_exception('The exchange parameter is required.')

    if self.options['exchange'] not in valid_exchanges:
      self.raise_exception('Provide a valid exchange for the exchange parameter.')

    sanitized['exchange'] = self.options['exchange']

    if self.options.get('coorelation'):
      if self.options['coorelation'] not in valid_coorelations:
        self.raise_exception('Provide a valid coorelation for the coorelation parameter.')

      sanitized['coorelation'] = self.options['coorelation']

    return sanitized
