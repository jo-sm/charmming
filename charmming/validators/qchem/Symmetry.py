from charmming.validators import Validator

class Symmetry(Validator):
  def validate(self):
    sanitized = self.sanitized

    if self.options.get('symmetry') and type(self.options.get('symmetry')) == bool:
      sanitized['symmetry'] = True

    return sanitized
