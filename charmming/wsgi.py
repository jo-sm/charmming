from django.core.wsgi import get_wsgi_application
import os

# Set the django settings module
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "charmming.settings")

application = get_wsgi_application()
