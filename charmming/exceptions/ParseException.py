class ParseException(Exception):
  reason = "No Reason"

  def __init__(self, reason):
    self.reason = reason
