from django.db import models
from charmming.models import Task, WorkingFile
from charmming.utils.structure.aux import saveQCWorkingFiles

import os

class EnergyTask(Task):
  finale = models.FloatField(null=True)
  usepbc = models.CharField(max_length=1)
  useqmmm = models.CharField(max_length=1, null=True, default="n")
  qmmmsel = models.CharField(max_length=250)  # This is obsolete with our new atom selection system, but we leave it here for posterity.
  modelType = models.CharField(max_length=30, null=True, default=None)  # Holds the modelType of this calculation if it's QM. Null if not.

  def finish(self):
    """test if the job suceeded, create entries for output"""
    loc = self.workstruct.structure.location
    bnm = self.workstruct.identifier
    basepath = loc + '/' + bnm + '-' + self.action  # Let's make this all make sense...
    try:
      os.stat(basepath + '.out')
    except:
      self.status = 'F'
      return

    # There's always an input file, so create a WorkingFile
    # for it.
    wfinp = WorkingFile()
    path = basepath + ".inp"
    try:
      WorkingFile.objects.get(task=self, path=path)
    except:
      wfinp.task = self
      wfinp.path = path
      wfinp.canonPath = wfinp.path  # Change later
      wfinp.type = 'inp'
      wfinp.description = 'Energy input script'
      wfinp.save()

    # Check if an output file was created and if so create
    # a WorkingFile for it.

    wfout = WorkingFile()
    path = basepath + ".out"
    try:
      WorkingFile.objects.get(task=self, path=path)
    except:
      wfout.task = self
      wfout.path = path
      wfout.canonPath = wfout.path
      wfout.type = 'out'
      wfout.description = 'Energy output'
      wfout.save()

    if self.status == 'F':
      return
    saveQCWorkingFiles(self, basepath)
    self.workstruct.save()
    self.status = 'C'
