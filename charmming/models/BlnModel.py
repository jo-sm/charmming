from django.db import models

from charmming.models import CGWorkingStructure

class BlnModel(models.Model):
  selected = models.CharField(max_length=1)
  cgws = models.ForeignKey(CGWorkingStructure)
  nScale = models.DecimalField(max_digits=6, decimal_places=3)
  kBondHelix = models.DecimalField(max_digits=6, decimal_places=3)
  kBondCoil = models.DecimalField(max_digits=6, decimal_places=3)
  kBondSheet = models.DecimalField(max_digits=6, decimal_places=3)
  kAngleHelix = models.DecimalField(max_digits=6, decimal_places=3)
  kAngleCoil = models.DecimalField(max_digits=6, decimal_places=3)
  kAngleSheet = models.DecimalField(max_digits=6, decimal_places=3)
