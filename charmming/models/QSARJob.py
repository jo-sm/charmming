from django.db import models
from django.contrib.auth.models import User

from charmming.models import QSARJobType, SchedulerJob

class QSARJob(models.Model):
  job_owner = models.ForeignKey(User, related_name='job_owner')
  job_scheduler_id = models.ForeignKey(SchedulerJob)
  job_owner_index = models.PositiveIntegerField(default=0)
  job_name = models.CharField(max_length=200, null=True, blank=True)
  description = models.CharField(max_length=250, null=True, blank=True)
  job_start_time = models.DateTimeField(null=True, blank=True)
  job_end_time = models.DateTimeField(null=True, blank=True)
  job_type = models.ForeignKey(QSARJobType, null=True, blank=True)

  def getNewJobOwnerIndex(self, user):
    try:
      last_job = QSARJob.objects.filter(job_owner=user).order_by('-id')[0]
      next_job_owner_index = int(last_job.job_owner_index) + 1

    except:
      next_job_owner_index = 1

    return next_job_owner_index
