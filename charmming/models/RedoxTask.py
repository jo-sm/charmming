from django.db import models

from charmming.models import ApbsParams, Task, WorkingFile

import os

class RedoxTask(Task):
  redoxsite = models.CharField(max_length=250) # segid + 1-/2- or 2-/3-
  apbsparams = models.ForeignKey(ApbsParams, null=True)
  # couple = models.CharField(max_length=250)
  # chargeko = models.CharField(max_length=250)

  def finish(self):
    try:
      # This is a pain, since we need to get all of the input, output, and
      # PDB files from the REDOX calculation.
      """test if the job suceeded, create entries for output"""
      self.modifies_coordinates = False
      loc = self.workstruct.structure.location
      bnm = self.workstruct.identifier
      basepath = loc + "/redox-" + bnm
      # There's always an input file, so create a WorkingFile
      # for it.
      filenamesuffix = ['-build-redall',
                        '-build-redsite',
                        '-build-oxiall',
                        '-build-oxisite',
                        '-combinegrid-site',
                        '-mkgrid-full',
                        '-mkgrid-site',
                        '-modpot',
                        '-modpotref',
                        '-oxipot',
                        '-oxipotref']

      for suffix in filenamesuffix:
        path = basepath + suffix + ".inp"
        wfinp = WorkingFile()
        try:
          WorkingFile.objects.get(task=self, path=path)
        except:
          wfinp.task = self
          wfinp.path = path
          wfinp.canonPath = wfinp.path
          wfinp.type = 'inp'
          wfinp.description = 'a redox input script'
          wfinp.save()

      # Check if an output file was created and if so create
      # a WorkingFile for it.
      for suffix in filenamesuffix:
        path = basepath + suffix + ".out"
        try:
          os.stat(path)
        except:
          # self.status = 'C' ...what?
          self.status = 'F' # This is more like it...why would it be successful if the files aren't there?
          self.finished = 'y'
          return
        wfout = WorkingFile()
        try:
          WorkingFile.objects.get(task=self, path=path)
        except:
          wfout.task = self
          wfout.path = path
          wfout.canonPath = wfout.path
          wfout.type = 'out'
          wfout.description = 'a redox script output'
          wfout.save()
      if self.status == 'F':
        return

      # create Working files for PDB, CRD, and PSF.
      # First fetch all the segments associated to the workstruct associated to this task
      # because we're making files for each of them.
      # try:
      #     segments = self.workstruct.segments
      #     for segment in segments:
      # except:
      # self.finished = 'y'
      # path = basepath + "
    except:
      pass
    self.status = 'C'
