from django.db import models
from charmming.models import Task

class WorkingFile(models.Model):
  path = models.CharField(max_length=160)
  canonPath = models.CharField(max_length=160) # added so we can do file versioning
  version = models.PositiveIntegerField(default=1)
  type = models.CharField(max_length=20)
  description = models.CharField(max_length=500, null=True)
  task = models.ForeignKey(Task, null=True)
  pdbkey = models.CharField(max_length=100, null=True) # if this is a structure file, want to know where to find it

  @property
  def basename(self):
    bn = self.path.split('/')[-1]
    return bn.split('.')[0]

  @property
  def cbasename(self):
    bn = self.canonPath.split('/')[-1]
    return bn.split('.')[0]

  def backup(self):
    # eventually, this will allow us to create new
    # versions of files
    pass
