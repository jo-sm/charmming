from django.db import models
from django.contrib.auth.models import User

class Lesson(models.Model):
  # data for lessons (should not be overridden by subclasses)
  # specifying both user and PDB is redundant (since the PDB references the user),
  # but makes it easier to look up all lessons being done by a particular user.
  user = models.ForeignKey(User)
  nSteps = models.PositiveIntegerField(default=0)
  curStep = models.PositiveIntegerField(default=0)

  # lesson methods (subclasses should override as needed)
  def onFileUpload(self, postdata):
    return True

  def onEditPDBInfo(self, postdata):
    return True

  def onMinimizeSubmit(self, postdata):
    return True

  def onMinimizeDone(self, file):
    return True

  def onSolvationSubmit(self, postdata):
    return True

  def onSolvationDone(self, file):
    return True

  def onEnergySubmit(self, postdata):
    return True

  def onEnergyDone(self, file):
    return True

  def onNMASubmit(self, postdata):
    return True

  def onNMADone(self, file):
    return True

  def onMDSubmit(self, postdata):
    return True

  def onMDDone(self, file):
    return True

  def onLDSubmit(self, postdata):
    return True

  def onLDDone(self, file):
    return True

  def onSGLDSubmit(self, postdata):
    return True

  def onSGLDDone(self, file):
    return True

  def onBuildStructureSubmit(self, postdata):
    return True

  def onBuildStructureDone(self, file):
    return True

  def onRedoxSubmit(self, postdata):
    return True

  def onRedoxDone(self, file):
    return True

  def onRMSDSubmit(self, postdata):
    return True
