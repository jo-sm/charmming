from django.db import models

class DDSource(models.Model):
  source_name = models.CharField(max_length=200)
  description = models.CharField(max_length=250, null=True, blank=True)
  source_object_table_name = models.CharField(max_length=100, null=True, blank=True)
  source_object_id = models.PositiveIntegerField(default=0, null=True, blank=True)
