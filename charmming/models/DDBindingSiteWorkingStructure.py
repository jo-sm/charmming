from django.db import models

from charmming.models import DDBindingSite, WorkingStructure

class DDBindingSiteWorkingStructure(models.Model):
  binding_site = models.ForeignKey(DDBindingSite)
  workstruct = models.ForeignKey(WorkingStructure)
