from django.db import models
from charmming.scheduler.schedInterface import SchedInterface
from charmming.models import APIJob
from charmming import charmming_config

import random
import hashlib
import os
import subprocess
import pychm3

class APIHandle(models.Model):
  jobType = models.CharField(max_length=100)
  APIJob = models.ForeignKey(APIJob, null=True)
  callerHandle = models.CharField(max_length=100, null=False)
  schedulerID = models.PositiveIntegerField(default=0)

  def genHandle(self):
    # random 9 digit number
    myrand = int(random.uniform(100000000, 999999999))
    hasher = hashlib.sha1()
    hasher.update((str(myrand) + str(self.id)).encode('utf8'))
    self.callerHandle = hash.hexdigest()

    return self.callerHandle

  def getStatus(self):
    response = {}

    si = schedInterface()
    sstring = si.checkStatus(self.schedulerID).split()[4]
    del si

    if sstring == 'failed':
      response['errmesg'] = 'The job failed during execution'
      response['errcode'] = -900
      return response
    if sstring == 'running' or sstring == 'submitted' or sstring == 'queued':
      response['errmesg'] = 'The job is still waiting to run or currently running'
      response['errcode'] = -999
      return response

    if sstring != 'complete':
      raise AssertionError('Got unknown job state: %s' % sstring)

    if self.jobType == 'ener':
      out_file = '%s/energy.out' % self.APIJob.directory
    elif self.jobType == 'mini':
      out_file = '%s/minimize.out' % self.APIJob.directory

    # let's save everything for posterity...
    tar_filename = self.APIJob.directory.split('/')[-1] + '.tar.gz'

    os.chdir(self.APIJob.directory + '/..')
    subprocess.call(["tar", "-czf", tar_filename, self.APIJob.directory.split('/')[-1]])

    response['tgz_url'] = '{}/pdbuploads/{}/{}'.format(charmming_config.charmming_url, self.APIJob.user.callerName, tar_filename)
    os.chdir(self.APIJob.directory)

    # parse the outputs for the user...
    fp = open(out_file, 'r')
    ene = self.APIJob.parse_energy(fp)
    if ene < -9000000:
      response['errcode'] = -980
      response['errmesg'] = 'Job was run to completion but failed to calculate energy'
      return response

    response['eneval'] = ene

    fp.seek(0)
    ene = self.APIJob.parse_inte(fp)
    if ene > -9999999:
      response['inte'] = ene

    if self.jobType == 'mini':
      fp = open('mini.pdb', 'r')
      response['minpdb'] = fp.read()
      fp.close()
      response['errcode'] = 0
      response['eneval'] = ene

      pdb = pychm3.io.pdb.PDB('mini.pdb')
      mol = pdb[0]

      for seg in mol.iter_seg():
        seg.write('/tmp/foo.pdb', outformat='pdborg')
        fp = open('/tmp/foo.pdb', 'r')
        response['min_seg_%s' % seg.segid] = fp.read()
        fp.close()

    response['errcode'] = 0
    return response
