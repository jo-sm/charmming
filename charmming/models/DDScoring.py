from django.db import models
from django.contrib.auth.models import User

from charmming.models import QSARUnit, DDScoringCriteria

class DDScoring(models.Model):
  owner = models.ForeignKey(User)
  object_table_name = models.CharField(max_length=100)
  object_id = models.PositiveIntegerField(default=0)
  scoring_criteria = models.ForeignKey(DDScoringCriteria)
  value = models.DecimalField(max_digits=8, decimal_places=5, null=True)
  units = models.ForeignKey(QSARUnit)
