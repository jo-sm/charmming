from django.db import models
from django.template import Context

from charmming import charmming_config
from charmming.models import APIJob
from charmming.helpers import output
from charmming.utils.script import get_input_script

import os

class APIBSiteInte(APIJob):
  implicitSolvent = models.CharField(max_length=8, null=False, default=None)
  FinalEnergyValue = models.FloatField(null=True)
  ligSegID = models.CharField(max_length=8, null=False, default=None)
  bindingSite = models.CharField(max_length=500, null=False, default=None)

  def _parse_inte(self, fp):
    found_inte = False
    for line in fp:
      line = line.strip()
      if line.startswith('INTE>'):
        return float(line.split()[2])

    if not found_inte:
      raise AssertionError('No Inte')

    # this code should be dead
    return 0.0

  def run(self, req, scriptlist):
    response = {}

    template_dict = {
      'rtf_list': self.rtfList.split(),
      'prm_list': self.prmList.split(),
      'str_list': self.strList.split(),
      'lig_segid': self.ligSegID,
      'bsite': self.bindingSite,
      'gbmv': (self.implicitSolvent == 'gbmv')
    }

    t = get_input_script('api_inte.inp')
    charmm_inp = output.tidyInp(t.render(Context(template_dict)))

    inp_file = '{}/inte.inp'.format(self.directory)
    out_file = '{}/inte.out'.format(self.directory)

    fp = open(inp_file, 'w')
    fp.write(charmm_inp)
    fp.close()

    os.chdir(self.directory)
    cmd = '{} < {} > {}' % (charmming_config.charmm_exe, inp_file, out_file)
    os.system(cmd)

    try:
      fp = open(out_file, 'r')
    except:
      response['errcode'] = -999
      response['errmesg'] = 'Job output file could not be opened'
      return response

    if not fp:
      response['errcode'] = -999
      response['errmesg'] = 'Job output file could not be opened'
      return response

    try:
      response['eneval'] = self._parse_inte(fp)
      response['errcode'] = 0
    except:
      response['eneval'] = 0.0
      response['errcode'] = -980
      response['errmesg'] = 'Job completed but did not successfully calculate energy'
    fp.close()

    return response
