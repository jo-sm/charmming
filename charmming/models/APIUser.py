from django.db import models

class APIUser(models.Model):
  callerName = models.CharField(max_length=40, null=False, default=None)
  callerKey = models.CharField(max_length=40, null=False, default=None)
