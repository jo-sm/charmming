from django.db import models
from django.contrib.auth.models import User

from charmming.models import DDProtein

class DDProteinConformation(models.Model):
  owner = models.ForeignKey(User)
  protein = models.ForeignKey(DDProtein)
  conformation_protein_index = models.PositiveIntegerField(default=0)
  conformation_name = models.CharField(max_length=200, null=True, blank=True)
  description = models.CharField(max_length=250, null=True, blank=True)
