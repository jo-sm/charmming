from django.db import models
from charmming.models import Task

class Script(models.Model):
  task = models.ForeignKey(Task)
  path = models.CharField(max_length=255)

  # Shortname for executable within the Scheduler
  # For example, this may be set to 'charmm', but
  # the Scheduler will set this as /usr/bin/charmm
  # within the submitted script.
  executable = models.CharField(max_length=100)

  # By default, it will choose the scripts in the
  # they were created, unless the order is explicitly
  # set.
  order = models.PositiveIntegerField(null=False, default=1)
  processors = models.PositiveIntegerField(null=False, default=1)
