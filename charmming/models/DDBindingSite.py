from django.db import models

class DDBindingSite(models.Model):
  binding_site_name = models.CharField(max_length=200)
  description = models.CharField(max_length=250, null=True, blank=True)
