from django.db import models

class DDFileType(models.Model):
  file_type_name = models.CharField(max_length=200)
  description = models.CharField(max_length=250, null=True, blank=True)
