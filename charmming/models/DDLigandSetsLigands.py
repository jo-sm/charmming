from django.db import models
from django.contrib.auth.models import User

from charmming.models import DDLigandSet, DDLigand

class DDLigandSetsLigands(models.Model):
  owner = models.ForeignKey(User)
  ligands_set = models.ForeignKey(DDLigandSet)
  ligands = models.ForeignKey(DDLigand)
