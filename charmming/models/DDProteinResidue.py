from django.db import models

from charmming.models import DDAminoAcid, DDProtein

class DDProteinResidue(models.Model):
  amino_acid = models.ForeignKey(DDAminoAcid)
  protein = models.ForeignKey(DDProtein)
  chain = models.CharField(max_length=50, null=True, blank=True)
  residue_number = models.PositiveIntegerField(default=0)
