from django.db import models
from django.contrib.auth.models import User

from charmming.models import QSARUnit

class QSARAttribute(models.Model):
  attribute_owner = models.ForeignKey(User, related_name='attribute_owner')
  attribute_short_name = models.CharField(max_length=50, null=True, blank=True)
  attribute_name = models.CharField(max_length=100, null=True, blank=True)
  description = models.CharField(max_length=250, null=True, blank=True)
  units = models.ForeignKey(QSARUnit, null=True, blank=True)
  data_type = models.CharField(max_length=100, null=True)
