from django.db import models

# Statistics are made up of data points...thus the name
class DataPoint(models.Model):  # I don't have a good name for this yet.
  # NONE of these fields must be ForeignKeys!
  # All this data MUST persist in the DB permanently. Otherwise
  # it becomes worthless.
  task_id = models.PositiveIntegerField(default=0)
  task_action = models.CharField(max_length=100)
  user = models.CharField(max_length=30)
  structure_name = models.CharField(max_length=100)
  struct_id = models.CharField(max_length=100)  # This is here because otherwise it is very difficult to avoid multiple-counting structures
  success = models.BooleanField(default=False)
  date_created = models.DateTimeField(auto_now=True)
