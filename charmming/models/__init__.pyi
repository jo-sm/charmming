from django.contrib.auth.models import User
from django.db import models

from datetime import datetime

class SchedulerJob(models.Model):
  sched_id = ... # type: str
  state = ... # type: int
  started = ... # type: datetime
  ended = ... # type: datetime
  queued = ... # type: datetime
  userid = ... # type: User
  batchid = ... # type: str
  exe = ... # type: str
  script = ... # type: str
