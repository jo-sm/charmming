from django.db import models
from charmming.models import TeacherProfile

class Classroom(models.Model):
  name = models.CharField(max_length=200)
  teacher = models.ForeignKey(TeacherProfile)
  num_of_students = models.IntegerField()

  # pre: Needs a valid teacherProfile
  # This will get the list of classrooms based on a teacher
  def getClassList(self, teacher):
    return Classroom.objects.filter(teacher=teacher)
