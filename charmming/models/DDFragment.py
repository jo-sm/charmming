from django.db import models
from django.contrib.auth.models import User

from charmming.models import DDSource

class DDFragment(models.Model):
  owner = models.ForeignKey(User)
  fragment_owner_index = models.PositiveIntegerField(default=0)
  fragment_name = models.CharField(max_length=200)
  description = models.CharField(max_length=250, null=True, blank=True)
  source = models.ForeignKey(DDSource)
