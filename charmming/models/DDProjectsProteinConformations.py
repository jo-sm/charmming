from django.db import models
from django.contrib.auth.models import User

from charmming.models import DDProject, DDProteinConformation

class DDProjectsProteinConformations(models.Model):
  owner = models.ForeignKey(User)
  project = models.ForeignKey(DDProject)
  protein_conformation = models.ForeignKey(DDProteinConformation)
