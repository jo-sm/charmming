from charmming.models import Task
from charmming.scheduler.schedInterface import SchedInterface

class DDTask(Task):
  def start(self, job_folder, scriptlist, exedict):
    st = self.workstruct.structure

    si = schedInterface()
    self.jobID = si.submitJob(st.owner.id, job_folder, scriptlist, exedict)
    if self.jobID > 0:
      self.save()
      self.query()
    else:
      raise AssertionError('Job submission fails')
