from django.db import models
from django.contrib.auth.models import User

from charmming.models import DDFragmentSet, DDFragment

class DDFragmentSetsFragments(models.Model):
  owner = models.ForeignKey(User)
  fragment_set = models.ForeignKey(DDFragmentSet)
  fragment = models.ForeignKey(DDFragment)
