from django.db import models

from charmming.models import LessonMakerLesson

class Objective(models.Model):
  lesson = models.ForeignKey(LessonMakerLesson)
  obj_num = models.IntegerField(null=False) # this gives the objective number because we don't want them to be out of order
  obj_text = models.CharField(max_length=240, null=False) # this should be spawned on creation of an Objective Object
