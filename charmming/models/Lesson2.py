from django.db import models
from django.contrib.auth.models import User
from charmming.models import LessonProblem
from charmming import charmming_config
from charmming.models import DynamicsTask, Structure, Task
from charmming.utils.lesson import diffPDBs

import os
import math

class Lesson2(models.Model):
  # data for lessons (should not be overridden by subclasses)
  # specifying both user and PDB is redundant (since the PDB references the user),
  # but makes it easier to look up all lessons being done by a particular user.
  user = models.ForeignKey(User)
  nSteps = models.PositiveIntegerField(default=6)
  curStep = models.DecimalField(default=0, decimal_places=1, max_digits=3)

  def onFileUpload(self):
    try:
      LessonProblem.objects.get(lesson_type='lesson2', lesson_id=self.id).delete()
    except:
      pass
    file = Structure.objects.get(selected='y', owner=self.user, lesson_id=self.id)
    try:
      filename1 = '%s/mytemplates/lessons/lesson2/1zhy.pdb' % charmming_config.charmming_root
      os.stat(filename1)
      filename2 = file.location + '/1zhy.pdb'
      os.stat(filename2)
    except:
      lessonprob = LessonProblem(lesson_type='lesson2', lesson_id=self.id, errorstep=1, severity=9, description='The PDB you submitted did not upload properly. Go to the <a href="/fileupload">Submit Structure</a> page and check to make sure the PDB.org ID you provided was valid.')
      lessonprob.save()
      return False
    if not diffPDBs(file, filename1, filename2):
      lessonprob = LessonProblem(lesson_type='lesson2', lesson_id=self.id, errorstep=1, severity=9, description='The PDB you uploaded was not the correct PDB. Go to the to the <a href="/fileupload">Submit Structure</a> page and upload the files provided.')
      lessonprob.save()
      return False
    self.curStep = '1'
    self.save()
    return True

  def onEditPDBInfo(self, postdata):
    return True

  def onMinimizeSubmit(self, mp, filename):
    try:
      LessonProblem.objects.filter(lesson_type='lesson2', lesson_id=self.id)[0].delete()
    except:
      pass
    # we can do everything with just mp these days.
    if float(self.curStep) >= 3.0 and float(self.curStep) < 4.0:
      if mp.parent.action != "neutralization":
        lessonprob = LessonProblem(lesson_type='lesson2', lesson_id=self.id, errorstep=4, severity=2, description='Please return to the <a href="/minimize/">Minimize</a> page and check the correct box to use the coordinates from neutralization')
        lessonprob.save()
        return False
      if mp.sdsteps != 100:
        lessonprob = LessonProblem(lesson_type='lesson2', lesson_id=self.id, errorstep=4, severity=2, description='SD steps were set to %s. Please return to the <a href="/minimize/">Minimize</a> page and set SDsteps to 100' % mp.sdsteps)
        lessonprob.save()
        return False
      if mp.abnrsteps != 500:
        lessonprob = LessonProblem(lesson_type='lesson2', lesson_id=self.id, errorstep=4, severity=2, description='ABNR steps were set to %s. Please return to the <a href="/minimize/">Minimize</a> page and set ABNR to 500.' % mp.abnrsteps)
        lessonprob.save()
        return False
      if float(mp.tolg) != .1:
        lessonprob = LessonProblem(lesson_type='lesson2', lesson_id=self.id, errorstep=4, severity=2, description='TOLG was set to %s. Please return to the <a href="/minimize/">Minimize</a> page and set "Gradient tolerance" to 0.1.' % str(mp.tolg))
        lessonprob.save()
        return False
      if mp.usepbc != 't':
        lessonprob = LessonProblem(lesson_type='lesson2', lesson_id=self.id, errorstep=4, severity=2, description='Minimization did not use Periodic Boundary Conditions. Please return to the <a href="/minimize/">Minimize</a> page and check the "Use Periodic Boundary Conditions (PBC) in minimization" box.')
        lessonprob.save()
        return False

    # 3.5 Means it is running
    self.curStep = '3.5'
    self.save()
    return True

  def onMinimizeDone(self, mp):
    try:
      lessonprob = LessonProblem.objects.filter(lesson_type='lesson2', lesson_id=self.id)[0]
    except:
      lessonprob = None
    # mp = minimizeTask.objects.filter(pdb=file,selected='y')[0]

    if lessonprob:
      self.curStep = str(float(self.curStep) - (float(self.curStep) % 1))
      self.save()
      return False

    if float(self.curStep) == 3.5:
      if mp.status == 'F':
        lessonprob = LessonProblem(lesson_type='lesson2', lesson_id=self.id, errorstep=4, severity=9, description='The Job did not complete correctly. Check your job output, then return to the <a href="/minimize/">Minimize</a> page and re-run the job after checking that all of your parameters were correct. If the problem persists, contact the CHARMMing administrator.')
        lessonprob.save()
        self.curStep = '3'
        self.save()
        return False
      else:
        self.curStep = '4'
        self.save()
    return True

  def onSolvationSubmit(self, sp):
    try:
      LessonProblem.objects.get(lesson_type='lesson2', lesson_id=self.id).delete()
    except:
      pass
    # don't bother checking for build parent, I think we're better off if we don't.
    if float(self.curStep) >= 2.0 and float(self.curStep) < 3.0:
      if sp.solvation_structure != 'rhdo':
        lessonprob = LessonProblem(lesson_type='lesson2', lesson_id=self.id, errorstep=3, severity=2, description='You used the wrong solvation structure. Please return to the <a href="/solvate/">Solvate</a> page and set "Choose the shape of the solvation structure" to "Rhombic Dodecahedron (X=Y=Z)".')
        lessonprob.save()
        return False
      # if sp.no_pref_radius != 10:
      if (float(sp.xtl_x) < 90 or float(sp.xtl_x) > 93) or (float(sp.xtl_y) < 90 or float(sp.xtl_y) > 93) or (float(sp.xtl_z) < 90 or float(sp.xtl_z) > 93):
        lessonprob = LessonProblem(lesson_type='lesson2', lesson_id=self.id, errorstep=3, severity=2, description='The wrong radius size was set. Please return to the <a href="/solvate/">Solvate</a> page and set a value of 10.')
        lessonprob.save()
        return False
      if sp.salt != "POT":
        lessonprob = LessonProblem(lesson_type='lesson2', lesson_id=self.id, errorstep=3, severity=2, description='The wrong salt was used. Please return to the <a href="/solvate/">Solvate</a> page and set the salt to potassium chloride.')
        lessonprob.save()
        return False
      if float(sp.concentration) != 0.15:
        lessonprob = LessonProblem(lesson_type='lesson2', lesson_id=self.id, errorstep=3, severity=2, description='The wrong concentration of salt was used. Please return to the <a href="/solvate/">Solvate</a> page and set a concentration of .15.')
        lessonprob.save()
        return False
      if float(sp.ntrials) != 1:
        lessonprob = LessonProblem(lesson_type='lesson2', lesson_id=self.id, errorstep=3, severity=2, description='The wrong number of trials were used. Please return to the <a href="/solvate/">Solvate</a> page and set trials to 1 (the default value is 3).')
        lessonprob.save()
        return False
    self.curStep = '2.5'
    self.save()
    return True

  def onSolvationDone(self, sp):
    try:
      lessonprob = LessonProblem.objects.get(lesson_type='lesson2', lesson_id=self.id)
    except:
      lessonprob = None
    if lessonprob:
      self.curStep = str(float(self.curStep) - (float(self.curStep) % 1))
      self.save()
      return False

    if float(self.curStep) == 2.5:
      if sp.status == 'F':
        lessonprob = LessonProblem(lesson_type='lesson2', lesson_id=self.id, errorstep=3, severity=9, description='The Job did not complete correctly. Check your job output, then return to the <a href="/solvate/">Solvate</a> page and re-run the job after checking that all of your parameters were correct. If the problem persists, contact the CHARMMing administrator.')
        lessonprob.save()
        lessonprob.save()
        self.curStep = '2'
        self.save()
        return False
      else:
        self.curStep = '3'
        self.save()
    return True

  def onNMASubmit(self, postdata):
    return True

  def onNMADone(self, file):
    return True

  def onMDSubmit(self, mdp):
    # Clear any old lessonproblems
    try:
      LessonProblem.objects.get(lesson_type='lesson2', lesson_id=self.id).delete()
    except:
      pass

    if float(self.curStep) >= float(4.0) and float(self.curStep) < 5.0:
      parent_task = Task.objects.get(id=Task.objects.get(id=DynamicsTask.objects.get(id=mdp.dynamicstask_ptr_id).task_ptr_id).parent_id)
      if parent_task.action != 'minimization':
        lessonprob = LessonProblem(lesson_type='lesson2', lesson_id=self.id, errorstep=5, severity=2, description='Heating dynamics were not run on the coordinates from charmming.minimization. Please return to the <a href="/dynamics/md">Molecular Dynamics</a> page and select "Coordinates from minimization".')
        lessonprob.save()
        return False
      if mdp.ensemble != 'heat':
        lessonprob = LessonProblem(lesson_type='lesson2', lesson_id=self.id, errorstep=5, severity=2, description='You used an equilibration calculation instead of a heating one. Please return to the <a href="/dynamics/md">Molecular Dynamics</a> page and select "Heating (adds kinetic energy to the system)".')
        lessonprob.save()
        return False
      if mdp.nstep != 1000:
        lessonprob = LessonProblem(lesson_type='lesson2', lesson_id=self.id, errorstep=5, severity=2, description='You did not set the correct number of steps. Please return to the <a href="/dynamics/md">Molecular Dynamics</a> page and set the number of steps to 1000.')
        lessonprob.save()
        return False
      if float(mdp.firstt) != 210.15:
        lessonprob = LessonProblem(lesson_type='lesson2', lesson_id=self.id, errorstep=5, severity=2, description='You did not set the starting temperature correctly. Please return to the <a href="/dynamics/md">Molecular Dynamics</a> page and set the starting temperature to 100 K to continue.')
        lessonprob.save()
        return False
      if float(mdp.teminc) != 10:
        lessonprob = LessonProblem(lesson_type='lesson2', lesson_id=self.id, errorstep=5, severity=2, description='You did not set the temperature increment correctly. Please return to the <a href="/dynamics/md">Molecular Dynamics</a> page and set the temperature increment to 10K to continue.')
        lessonprob.save()
        return False
      if float(mdp.ihtfrq) != 100:
        lessonprob = LessonProblem(lesson_type='lesson2', lesson_id=self.id, errorstep=5, severity=2, description='You did not set the heating frequency correctly. Please return to the <a href="/dynamics/md">Molecular Dynamics</a> page and set "Steps between temperature increments (IHTFRQ)" to 20 steps.')
        lessonprob.save()
        return False
      if float(mdp.finalt) != 310.15:
        lessonprob = LessonProblem(lesson_type='lesson2', lesson_id=self.id, errorstep=5, severity=2, description='You did not set the final temperature to physiological temperature (310.15 K). Please return to the <a href="/dynamics/md">Molecular Dynamics</a> page and set the final temperature correctly.')
        lessonprob.save()
        return False
      if float(mdp.tbath) != 310.15:
        lessonprob = LessonProblem(lesson_type='lesson2', lesson_id=self.id, errorstep=5, severity=2, description='You did not set the temperature bath to physiological temperature (310.15 K). Please return to the <a href="/dynamics/md">Molecular Dynamics</a> page and set the temperature of the bath correctly.')
        lessonprob.save()
        return False
      if not mdp.usepbc:
        lessonprob = LessonProblem(lesson_type='lesson2', lesson_id=self.id, errorstep=5, severity=2, description='You did not use Periodic Boundary Conditions (PBC) with your dynamics run. Please return to the <a href="/dynamics/md">Molecular Dynamics</a> page and check the box to use PBC.')
        lessonprob.save()
        return False
      self.curStep = '4.5'
      self.save()
      return True
    elif float(self.curStep) >= float(5.0) and float(self.curStep) < 6.0:
      parent_task = Task.objects.get(id=Task.objects.get(id=DynamicsTask.objects.get(id=mdp.dynamicstask_ptr_id).task_ptr_id).parent_id)
      if parent_task.action != 'md':
        lessonprob = LessonProblem(lesson_type='lesson2', lesson_id=self.id, errorstep=6, severity=2, description='Equilibration dynamics were not run on the PDB from the MD heating run. Please return to the <a href="/dynamics/md">Molecular Dynamics</a> page and select "Coordinates from md".')
        lessonprob.save()
        return False
      if mdp.ensemble != 'npt':
        lessonprob = LessonProblem(lesson_type='lesson2', lesson_id=self.id, errorstep=6, severity=2, description='You used an incorrect type of run. Please return to the <a href="/dynamics/md">Molecular Dynamics</a> page and select "NPT dynamics (holds temperature and pressure constant)" to continue.')
        lessonprob.save()
        return False
      if mdp.nstep != 1000:
        lessonprob = LessonProblem(lesson_type='lesson2', lesson_id=self.id, errorstep=6, severity=2, description='You did not set the number of steps correctly. Please return to the <a href="/dynamics/md">Molecular Dynamics</a> page and set the number of steps to 1000 to continue.')
        lessonprob.save()
        return False
      if float(mdp.temp) != 310.15:
        lessonprob = LessonProblem(lesson_type='lesson2', lesson_id=self.id, errorstep=6, severity=2, description='You did not set the simulation temperature correctly. Please return to the <a href="/dynamics/md">Molecular Dynamics</a> page and set the simulation temperature to 310.15 K.')
        lessonprob.save()
        return False
      if not mdp.usepbc:
        lessonprob = LessonProblem(lesson_type='lesson2', lesson_id=self.id, errorstep=5, severity=2, description='You did not use Periodic Boundary Conditions (PBC) with your dynamics run. Please return to the <a href="/dynamics/md">Molecular Dynamics</a> page and check the box to use PBC.')
        lessonprob.save()
        return False
      self.curStep = '5.5'
      self.save()
      return True
    else:
      # to-do, MD done at the wrong time
      return False

  def onMDDone(self, mdp):
    try:
      lessonprob = LessonProblem.objects.filter(lesson_type='lesson2', lesson_id=self.id)[0]
    except:
      lessonprob = None

    if lessonprob:
      self.curStep = str(float(self.curStep) - (float(self.curStep) % 1))
      self.save()
      return False

    if mdp.status == 'F':
      if float(self.curStep) == 4.5:
        lessonprob = LessonProblem(lesson_type='lesson2', lesson_id=self.id, errorstep=5, severity=9, description='The Job did not complete correctly. Check your job output, then return to the <a href="/solvate/">Solvate</a> page and re-run the job after checking that all of your parameters were correct. If the problem persists, contact the CHARMMing administrator.')
      else:
        lessonprob = LessonProblem(lesson_type='lesson2', lesson_id=self.id, errorstep=6, severity=9, description='The Job did not complete correctly. Check your job output, then return to the <a href="/solvate/">Solvate</a> page and re-run the job after checking that all of your parameters were correct. If the problem persists, contact the CHARMMing administrator.')
      lessonprob.save()
      return False
    else:
      if float(self.curStep) == 4.5:
        self.curStep = '5'
      else:
        self.curStep = '6'
      self.save()
    return True

  def onLDSubmit(self, postdata):
    return True

  def onLDDone(self, file):
    return True

  def onSGLDSubmit(self, postdata):
    return True

  def onSGLDDone(self, file):
    return True

  def onRMSDSubmit(self, file):
    return True

  def onNATQSubmit(self, postdata):
    return True

  def onEnergySubmit(self, postdata):
    try:
      LessonProblem.objects.get(lesson_type='lesson2', lesson_id=self.id).delete()
    except:
      pass

    if float(self.curStep) >= 1.0 and float(self.curStep) < 2.0:
      # nothing really to validate here...
      self.curStep = '1.5'
      self.save()

    return True

  def onEnergyDone(self, et):
    if float(self.curStep) == 1.5:
      if float(et.finale) < float(40690) or float(et.finale) > float(40695):
        lessonprob = LessonProblem(lesson_type='lesson2', lesson_id=self.id, errorstep=2, severity=9, description="Your energy was wrong (should be ~ 40692, you got %s). Make sure to use all default settings on the energy page, and to use the custom parameter and topology files provided in this lesson." % et.finale)
        lessonprob.save()
        self.curStep = '1'
        self.save()
        return False
    self.curStep = '2'
    self.save()
    return True

  # generates html for the lesson status page
  def generateStatusHtml(self, file):
    step_status_list = []
    step_status_list.append("<tr class='status'><td class='status'>1. File Uploaded: ")
    step_status_list.append("<tr class='status'><td class='status'>2. Energy: ")
    step_status_list.append("<tr class='status'><td class='status'>3. Solvation: ")
    step_status_list.append("<tr class='status'><td class='status'>4. Minimization: ")
    step_status_list.append("<tr class='status'><td class='status'>5. MD Heating: ")
    step_status_list.append("<tr class='status'><td class='status'>6. MD Equilibration: ")
    # This will store all the status and the steps, clearing the template of logic
    # And only displaying the status
    try:
      lessonprob = LessonProblem.objects.filter(lesson_type='lesson2', lesson_id=self.id)[0]
    except:
      lessonprob = None
    for i in range(self.nSteps):
      if lessonprob and lessonprob.errorstep == math.floor(self.curStep + 1) and math.floor(self.curStep) == i:
        step_status_list[i] += ("<a class='failed' href='javascript:open_failure()'>Failed</span></td></tr>")
        continue
      elif (float(self.curStep) - 0.5) == i and float(self.curStep) % 1 == 0.5:
        step_status_list[i] += ("<span class='running'>Running</span></td></tr>")
        continue
      elif i < float(self.curStep):
        step_status_list[i] += ("<span class='done'>Done</span></td></tr>")
        continue
      elif i + 1 > float(self.curStep):
        step_status_list[i] += ("<span class='inactive'>N/A</span></td></tr>")
        continue
    return step_status_list

  # Returns a list where each index corresponds to lesson progress
  # on the display lesson page
  def getHtmlStepList(self):
    # 2 is running
    # 1 is successfully done
    # 0 is not started
    # -1 is error

    htmlcode_list = []
    for step in range(self.nSteps):
      htmlcode_list.append(0)
    if float(self.curStep) > 0:
      htmlcode_list[0] = 1
    if float(self.curStep) > 1:
      if float(self.curStep) == 1.5:
        htmlcode_list[1] = 2
      else:
        htmlcode_list[1] = 1
    if float(self.curStep) > 2:
      if float(self.curStep) == 2.5:
        htmlcode_list[2] = 2
      else:
        htmlcode_list[2] = 1
    if float(self.curStep) > 3:
      if float(self.curStep) == 3.5:
        htmlcode_list[3] = 2
      else:
        htmlcode_list[3] = 1
    if float(self.curStep) > 4:
      if float(self.curStep) == 4.5:
        htmlcode_list[4] = 2
      else:
        htmlcode_list[4] = 1
    if float(self.curStep) > 5:
      if float(self.curStep) == 5.5:
        htmlcode_list[5] = 2
      else:
        htmlcode_list[5] = 1
    try:
      lessonprob = LessonProblem.objects.filter(lesson_type='lesson2', lesson_id=self.id)[0]
      htmlcode_list[lessonprob.errorstep - 1] = -1
    except:
      lessonprob = None
    return htmlcode_list

  # Exists so that building the structure doesn't explode
  def onBuildStructureDone(self, postdata):
    return True

  def onBuildStructureSubmit(self, postdata):
    return True
