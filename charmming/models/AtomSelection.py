from django.db import models

from charmming.models import WorkingStructure, Task

class AtomSelection(models.Model):
  # The NULLs are all present because MM selections don't have any of those fields. However, they much be attached to a task and a workingstructure and have a selection
  workstruct = models.ForeignKey(WorkingStructure)

  # Holds the parent task for this atom selection. NOT the one that was performed on it, but the one where the original coordinates come from.
  task = models.ForeignKey(Task)

  # This field is for re-populating the qmmm_params page
  # Default "all" to make outer-layer selections easier; i.e. sele all
  selectionstring = models.CharField(max_length=4000, default="all")

  linkatom_num = models.PositiveIntegerField(default=None, null=True)

  # "qmmm" for standard, "oniom" for MSCALE, open to additional fields like semi_emp
  selection_type = models.CharField(max_length=100, default="qmmm")

  # Following are the other parameters so the user doesn't have to fill them out
  exchange = models.CharField(max_length=30, default=None, null=True)
  charge = models.IntegerField(default=None, null=True)
  correlation = models.CharField(max_length=10, default=None, null=True)
  basis_set = models.CharField(max_length=30, default=None, null=True)
  multiplicity = models.PositiveIntegerField(default=None, null=True)
  time_created = models.DateTimeField(auto_now=True)
