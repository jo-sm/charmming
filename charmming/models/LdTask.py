from django.db import models
from charmming.models import DynamicsTask

class LdTask(DynamicsTask):
  fbeta = models.FloatField(default=60.0, null=True)
  sgld = models.BooleanField(default=False)
