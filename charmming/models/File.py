from django.db import models

from charmming.models import Structure

class File(models.Model):
  name = models.CharField(max_length=64)
  path = models.CharField(max_length=100)
  file_type = models.CharField(max_length=10)
  structure = models.ForeignKey(Structure)

  def toJSON(self):
    return {
      'id': self.id,
      'name': self.name
    }
