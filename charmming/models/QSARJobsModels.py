from django.db import models
from django.contrib.auth.models import User

from charmming.models import QSARJob, QSARModel

class QSARJobsModels(models.Model):
  owner = models.ForeignKey(User)
  job = models.ForeignKey(QSARJob)
  qsar_model = models.ForeignKey(QSARModel)
