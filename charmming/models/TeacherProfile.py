from django.db import models
from localflavor.us.forms import USPhoneNumberField
from django.contrib.auth.models import User

class TeacherProfile(models.Model):
  institute = models.CharField(max_length=200)
  phone_number = USPhoneNumberField()
  teacher = models.OneToOneField(User)
