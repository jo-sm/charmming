from django.db import models

from charmming.models import DDSource

class DDPose(models.Model):
  pose_object_table_name = models.CharField(max_length=100)
  pose_object_id = models.PositiveIntegerField(default=0)
  pose_name = models.CharField(max_length=200)
  description = models.CharField(max_length=250, null=True, blank=True)
  source = models.ForeignKey(DDSource)
