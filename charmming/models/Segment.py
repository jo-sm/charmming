from django.db import models
from django.conf import settings
from django.contrib.postgres.fields import ArrayField

from charmming.models import Structure
from charmming.utils.list import unique

from pychm3.io.pdb import PDB

import os
import pickle

class Segment(models.Model):
  structure = models.ForeignKey(Structure)
  name = models.CharField(max_length=6)
  type = models.CharField(max_length=10)
  model = models.CharField(max_length=10)
  chain = models.CharField(max_length=3)
  # default_patch_first = models.CharField(max_length=100)
  # default_patch_last = models.CharField(max_length=100)
  first_patch = models.CharField(max_length=100)
  last_patch = models.CharField(max_length=100)
  built_psf = models.CharField(max_length=100)
  built_crd = models.CharField(max_length=100)
  rtf_list = ArrayField(models.CharField(max_length=200), default=list)
  prm_list = ArrayField(models.CharField(max_length=200), default=list)
  stream_list = ArrayField(models.CharField(max_length=200), default=list)
  is_working = models.CharField(max_length=1, default='n')
  fes4 = models.BooleanField(default=False) # a bit of a hack, but it makes the views code easier
  is_custom = models.BooleanField(default=False) # Bypasses PDB.org SDF file check when building structure
  resName = models.CharField(max_length=4, default='N/A')  # This lists res names for the build structure page and others, allows for faster code and less iteration
  toppar_method = models.CharField(max_length=20, null=True)
  user_supplied_toppar = models.BooleanField(default=False)

  # If the RTF and PRM generation failed

  def toJSON(self):
    return {
      'id': self.id,
      'name': self.name,
      'type': self.type,
      'model': self.model,
      'chain': self.chain,
      'firstPatch': self.first_patch,
      'lastPatch': self.last_patch
    }

  def get_pychm_model(self):
    segment = self.structure.get_file(segment=self)

    models = PDB(segment)

    # Since this is a written out segment, this will
    # only contain one model

    return models[0]

  def get_pychm_segment(self):
    """
    Returns the Segment from the pychm3 Model instance of this Segment
    """
    model = self.get_pychm_model()

    segments = model.segments

    if len(segments) == 1:
      return segments[0]
    elif len(segments) == 0:
      return None
    else:
      for segment in segments:
        if segment.id == self.name:
          return segment

      return None

  def get_file(self, filename, **kwargs):
    return self.structure.get_file(segment=self, filename=filename)

  def save_file(self, filename, contents, **kwargs):
    return self.structure.save_file(filename, contents, segment=self, **kwargs)

  def save_segment_file(self, contents, **kwargs):
    return self.structure.save_structure_file(contents, segment=self)

  def get_filepath(self, **kwargs):
    return self.structure.get_filepath(segment=self, **kwargs)

  def save_pychm_segment(self, pychm_segment):
    """
    Takes a Pychm Mol for this segment and writes
    it out to the Segment file.
    """

    self.save_segment_file(pychm_segment.to_str(format='pdborg'))

    return True

  def save(self, *args, **kwargs):
    base_dir = settings.PROJECT_ROOT
    toppar_dir = os.path.join(base_dir, "toppar")

    initial = kwargs.get('initial', False)

    if initial:
      del kwargs['initial']

      # Set the first and last patches
      if kwargs.get('first_residue_name'):
        first_residue_name = kwargs.get('first_residue_name')
        del kwargs['first_residue_name']
      else:
        first_residue_name = ''

      # Set first and last patches
      if self.type == 'pro':
        if first_residue_name == 'gly':
          self.first_patch = 'GLYP'
        elif first_residue_name == 'pro':
          self.first_patch = 'PROP'
        else:
          self.first_patch = 'NTER'
        self.last_patch = 'CTER'
      elif self.type == 'dna' or self.type == 'rna':
        self.first_patch = '5TER'
        self.last_patch = '3TER'
      else:
        self.first_patch = 'NONE'
        self.last_patch = 'NONE'

      # Set the rtf, prm, and stream lists
      if self.type == 'pro':
        self.rtf_list.append(os.path.join(toppar_dir, "top_all36_prot.rtf"))
        self.prm_list.append(os.path.join(toppar_dir, "par_all36_prot.prm"))
      elif self.type in [ 'dna', 'rna' ]:
        self.rtf_list.append(os.path.join(toppar_dir, "top_all36_na.rtf"))
        self.prm_list.append(os.path.join(toppar_dir, "par_all36_na.prm"))

      if self.type == 'good':
        # Generally this is water, so only need the water ions stream file
        self.stream_list.append(os.path.join(toppar_dir, "toppar_water_ions.str"))

      # Toppar generation defaults
      if self.type == 'bad':
        self.toppar_method = 'autogen'

    # Ensure that in case we ever add the same toppar path into the
    # RTF or PRM list, that we don't have duplicates
    self.rtf_list = unique(self.rtf_list)
    self.prm_list = unique(self.prm_list)

    return super().save(*args, **kwargs)

  def copy(self):
    """
    Returns a non-saved copy of the current segment
    """
    copy = Segment()
    copy.name = self.name
    copy.type = self.type
    copy.model = self.model
    copy.first_patch = self.first_patch
    copy.last_patch = self.last_patch
    # copy.default_patch_first = self.default_patch_first
    # copy.default_patch_last = self.default_patch_last
    copy.rtf_list = self.rtf_list
    copy.prm_list = self.prm_list
    copy.stream_list = self.stream_list
    copy.is_working = self.is_working
    copy.fes4 = self.fes4
    copy.is_custom = self.is_custom
    copy.resName = self.resName

    return copy

  def set_default_patches(self, firstres):
    if self.type == 'pro':
      if firstres == 'gly':
        self.default_patch_first = 'GLYP'
      elif firstres == 'pro':
        self.default_patch_first = 'PROP'
      else:
        self.default_patch_first = 'NTER'
      self.default_patch_last = 'CTER'
    elif self.type == 'dna' or self.type == 'rna':
      self.default_patch_first = '5TER'
      self.default_patch_last = '3TER'
    else:
      self.default_patch_first = 'NONE'
      self.default_patch_last = 'NONE'
    self.save()

  def getProtonizableResidues(self, model=None, pickleFile=None, propka_residues=None):
    """
    Returns tuples of information and whether the user needs to decide on protonation states, in that order.
    Returns "user needs to decide" by default.
    """
    if not pickleFile:
      pfp = open(self.structure.pickle, 'r')
      pdb = pickle.load(pfp)
    else:
      pdb = pickleFile

    if not model:
      mol = next(pdb.iter_models()) # grab first model
    else:
      mol = pdb[model]
    # ToDo: if there is more than one model, the protonizable
    # residues could change. We don't handle this right at
    # the moment, but it may not matter too much since the
    # residue sequence tends not to change between models, just
    # the atom positions.
    # The atoms DO change after a build, however. But at most of the places where
    # this function gets called, this is irrelevant since
    # we haven't built yet
    # We can probably handle multi-model protonizable residues with JavaScript but
    # It will make the buildstruct page bloat quite a lot, and it's pretty
    # bloated as it is now.

    found = False
    for s in mol.iter_seg():
      if self.name == s.segid:
        found = True
        curr_seg = s
        break

    if not found:
      raise AssertionError('Could not find right segment')

    rarr = []

    # At this point, we either use our regular logic, or use the PDB we wrote beforehand in PDBORG format
    # which we then match with the chainid and resid in the list
    asp_list = None
    glup_list = None
    hsp_list = None
    lsn_list = None  # We set to None first to make sure python doesn't freak out
    if propka_residues is not None:  # this means we're using propka
      asp_list = propka_residues[0]
      glup_list = propka_residues[1]
      hsp_list = propka_residues[2]
      lsn_list = propka_residues[3]
    for m in curr_seg.iter_res():
      nm = m.resName.strip()
      if nm in ['hsd', 'hse', 'hsp', 'his']:
        proto_choice = 0
        if hsp_list:
          for sub_list in hsp_list:
            if m.resid == sub_list[0] and m.chainid == sub_list[1]:  # we need to write a good PDB file beforehand...
              proto_choice = sub_list[3]
              break
        rarr.append(
          (
            self.name,
            m.resid,
            [
              ('hsd', 'Neutral histidine with proton on the delta nitrogen'),
              ('hse', 'Neutral histidine with proton on the epsilon nitrogen'),
              ('hsp', 'Positive histidine with protons on both the delta and epsilon nitrogens')
            ],
            m.resid0,
            proto_choice
          )
        )
      if nm in ['asp', 'aspp']:
        proto_choice = 0
        if asp_list:
          for sub_list in asp_list:
            if m.resid == sub_list[0] and m.chainid == sub_list[1]:
              proto_choice = sub_list[3]
              break
        rarr.append((self.name, m.resid, [('asp', '-1 charged aspartic acid'), ('aspp', 'Neutral aspartic acid')], m.resid0, proto_choice))
      if nm in ['glu', 'glup']:
        proto_choice = 0
        if glup_list:
          for sub_list in glup_list:
            if m.resid == sub_list[0] and m.chainid == sub_list[1]:
              proto_choice = sub_list[3]
              break
        rarr.append((self.name, m.resid, [('glu', '-1 charged glutamic acid'), ('glup', 'Neutral glutamic acid')], m.resid0, proto_choice))
      if nm in ['lys', 'lsn']:
        proto_choice = 0
        if lsn_list:
          for sub_list in lsn_list:
            if m.resid == sub_list[0] and m.chainid == sub_list[1]:
              proto_choice = sub_list[3]
              break
        rarr.append((self.name, m.resid, [('lys', '+1 charged Lysine'), ('lsn', 'Neutral Lysine')], m.resid0, proto_choice))
    if not pickleFile:
      pfp.close()
    return rarr

  @property
  def possible_firstpatches(self):
    if self.type == 'pro':
      return ['NTER', 'GLYP', 'PROP', 'ACE', 'ACP', 'NONE']
    elif self.type == 'dna' or self.type == 'rna':
      return ['5TER', '5MET', '5PHO', '5POM', 'CY35']
    else:
      return ['NONE']

  @property
  def possible_lastpatches(self):
    if self.type == 'pro':
      return ['CTER', 'CT1', 'CT2', 'CT3', 'ACP', 'NONE']
    elif self.type == 'dna' or self.type == 'rna':
      return ['3TER', '3PHO', '3POM', '3CO3', 'CY35']
    else:
      return ['NONE']
