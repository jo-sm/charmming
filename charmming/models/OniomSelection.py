from django.db import models
from charmming.models import AtomSelection

class OniomSelection(AtomSelection):  # Holds the MSCALE bits
  total_layers = models.PositiveIntegerField(default=2)  # Holds how many layers this ONIOM model has in total
  layer_num = models.PositiveIntegerField(default=1)  # Holds which layer (1,2,3,etc.) this selection is
  isQM = models.BooleanField(default=False)  # Holds whether this is a QM layer.
