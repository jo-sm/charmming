from django.db import models
from django.template import Context
from django.template.loader import get_template
from charmming.models import APIJob, APIHandle
from charmming.models import EnergyTask
from charmming import charmming_config
from charmming.helpers import output
from charmming.scheduler.schedInterface import SchedInterface

import os
import subprocess

class APIEnergy(APIJob):
  implicitSolvent = models.CharField(max_length=8, null=False, default=None)
  EnergyValue = models.FloatField(null=True)
  task = models.ForeignKey(EnergyTask, null=True)

  def run(self, req, scriptlist):
    response = {}

    if self.implicitSolvent == 'gbmv':
      template_dict = {
        'rtf_list': self.rtfList.split(),
        'prm_list': self.prmList.split(),
        'str_list': self.strList.split(),
        'gbmv': True
      }
      t = get_template('%s/mytemplates/input_scripts/api_energy.inp' % charmming_config.charmming_root)
      charmm_inp = output.tidyInp(t.render(Context(template_dict)))

      os.chdir(self.directory)
      inp_file = '%s/energy.inp' % self.directory
      out_file = '%s/energy.out' % self.directory

      fp = open(inp_file, 'w')
      fp.write(charmm_inp)
      fp.close()

      if 'async' in req:
        si = schedInterface()

        # note - because API users aren't real - API jobs run under CHARMMing user ID 0;
        # this is NOT THE SAME as running as root -- I'm not that dumb :-). There is no
        # real user 0 in the DB (admin = 1), so this is done so as not to fsck with anyone
        # else's jobs.
        scriptlist.append(inp_file)
        jobid = si.submitJob(0, self.directory, scriptlist)

        apih = APIHandle()
        apih.jobType = 'ener'
        apih.APIJob = self
        apih.schedulerID = jobid
        response['handle'] = apih.genHandle()
        apih.save()

        response['errcode'] = 1
        response['errmesg'] = 'Asynchronous job has been submitted for execution'
        return response

      cmd = '{} < {} > {}'.format(charmming_config.charmm_exe, inp_file, out_file)
      os.system(cmd)

      eneout = out_file
    else:
      eneout = '%s/buildstruct.out' % self.directory

    # let's save everything for posterity...
    tar_filename = self.directory.split('/')[-1] + '.tar.gz'

    os.chdir('..')
    subprocess.call(["tar", "-czf", tar_filename, self.directory.split('/')[-1]])

    response['tgz_url'] = '{}/pdbuploads/{}/{}'.format(charmming_config.charmming_url, self.user.callerName, tar_filename)
    os.chdir(self.directory)

    # this is easy, just parse the energy from the file...
    try:
      fp = open(eneout, 'r')
    except:
      response['errcode'] = -999
      response['errmesg'] = 'Job output file could not be opened'
      return response

    if not fp:
      response['errcode'] = -999
      response['errmesg'] = 'Job output file could not be opened'
      return response

    ene = self.parse_energy(fp)
    fp.close()

    if ene < -9000000:
      response['errcode'] = -980
      response['errmesg'] = 'Job finished but failed to calculate energy'
    else:
      response['errcode'] = 0
      response['eneval'] = ene

    return response
