from django.db import models
from django.contrib.auth.models import User

from charmming.models import DDFile

class DDFileObject(models.Model):
  owner = models.ForeignKey(User)
  file = models.ForeignKey(DDFile)
  object_table_name = models.CharField(max_length=100)
  object_id = models.PositiveIntegerField(default=0)
