from django.db import models

# Replica exchange parameters
class RexParams(models.Model):
  firstStep = models.PositiveIntegerField(default=1)
  lastStep = models.PositiveIntegerField(default=2)
  cleanStep = models.PositiveIntegerField(default=50)
  npref = models.DecimalField(default=0, null=False, max_digits=8, decimal_places=3)
  nbath = models.PositiveIntegerField(default=4)
  temperatures = models.CharField(max_length=250)
