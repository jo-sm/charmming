# lesson 1, upload 1YJP, minimize in vacuum, solvate/neutralize, minimize again, and
# run dynamics
from django.db import models
from django.contrib.auth.models import User
from charmming.models import LessonProblem
from charmming import charmming_config
from charmming.models import Structure
from charmming.utils.lesson import diffPDBs

import os
import math

class Lesson1(models.Model):
  # data for lessons (should not be overridden by subclasses)
  # specifying both user and Structure is redundant (since the Structure references the user),
  # but makes it easier to look up all lessons being done by a particular user.
  user = models.ForeignKey(User)
  nSteps = models.PositiveIntegerField(default=4)
  curStep = models.DecimalField(default=0, decimal_places=1, max_digits=3)

  def onFileUpload(self):
    try:
      LessonProblem.objects.get(lesson_type='lesson1', lesson_id=self.id).delete()
    except:
      pass
    file = Structure.objects.filter(selected='y', lesson_id=self.id)[0]

    try:
      filename3 = '%s/mytemplates/lessons/lesson1/1water.crd' % charmming_config.charmming_root
      os.stat(filename3)
      filename4 = file.location + '/1water.crd'
      os.stat(filename4)
    except:
      lessonprob = LessonProblem(lesson_type='lesson1', lesson_id=self.id, errorstep=1, severity=9, description='The CRD you submitted did not upload properly. Go to the <a href="/fileupload/">Submit Structure</a> page and check to make sure the PDB.org ID was valid.')
      lessonprob.save()
      return False
    if not diffPDBs(file, filename3, filename4):
      lessonprob = LessonProblem(lesson_type='lesson1', lesson_id=self.id, errorstep=1, severity=9, description='The structure you uploaded was not the correct structure. Go to the <a href="/fileupload/">Submit Structure</a> page and check to make sure you uploaded the provided files.')
      lessonprob.save()
      return False
    self.curStep = '1'
    self.save()
    return True

  def onBuildStructureDone(self, postdata):
    return True

  def onBuildStructureSubmit(self, postdata):
    return True

  def onMinimizeSubmit(self, mp, filename):
    try:
      LessonProblem.objects.get(lesson_type='lesson1', lesson_id=self.id).delete()
    except:
      pass
#        task=structure.models.Task.objects.get(id=mp.task_ptr_id) #this may not be necessary anymore??
#        parent_task=structure.models.Task.objects.get(id=task.parent_id)
    if float(self.curStep) >= float(2.0) and float(self.curStep) < float(3.0):
      if mp.parent.action is not 'solvation':
        lessonprob = LessonProblem(lesson_type='lesson1', lesson_id=self.id, errorstep=3, severity=2, description='Please minimize the solvated PDB. Go to the <a href="/minimize/">Minimize</a> page and select "Coordinates from solvation".')
        lessonprob.save()
        return False
      if mp.sdsteps != 1000:
        lessonprob = LessonProblem(lesson_type='lesson1', lesson_id=self.id, errorstep=3, severity=2, description='SD steps were not set to 1000. Go to the <a href="/minimize/">Minimize</a> page and set SDsteps to 1000.')
        lessonprob.save()
        return False
      if mp.abnrsteps != 1000:
        lessonprob = LessonProblem(lesson_type='lesson1', lesson_id=self.id, errorstep=3, severity=2, description='ABNR steps were not set to 1000. Go to the <a href="/minimize/">Minimize</a> page and set ABNR to 1000.')
        lessonprob.save()
        return False
      if float(mp.tolg) != 0.01:
        lessonprob = LessonProblem(lesson_type='lesson1', lesson_id=self.id, errorstep=3, severity=2, description='TOLG was set to %s, and not 0.01. Go to the <a href="/minimize/">Minimize</a> page and set tolg to 0.01 instead.' % mp.tolg)
        lessonprob.save()
        return False
      if mp.usepbc != 't':
        lessonprob = LessonProblem(lesson_type='lesson1', lesson_id=self.id, errorstep=3, severity=2, description='Minimization did not use Periodic Boundary Conditions. Go to the <a href="/minimize/">Minimize</a> page and check the "Use Periodic Boundary Conditions (PBC) in minimization" box.')
        lessonprob.save()
        return False

    # 2.5 Means it is running
    self.curStep = '2.5'
    self.save()
    return True

  def onMinimizeDone(self, mp):
    try:
      lessonprob = LessonProblem.objects.get(lesson_type='lesson1', lesson_id=self.id)
    except:
      lessonprob = None
    # mp = minimizeTask.objects.filter(pdb=file,selected='y')[0]
    # fail = re.compile('Failed')
    if lessonprob:
      self.curStep = str(float(self.curStep) - (float(self.curStep) % 1))
      self.save()
      return False

    # let's make sure we don't make the lesson step back if the user deviates a bit
    if float(self.curStep) == float(2.5):
      if mp.status == 'F':
        lessonprob = LessonProblem(lesson_type='lesson1', lesson_id=self.id, errorstep=3, severity=9, description='The Job did not complete correctly. Check your job output, then return to the <a href="/minimize/">Minimize</a> page and re-run your job after checking that all of your parameters were correct. If the issue still persists, contact the CHARMMing administrator.')
        lessonprob.save()
        self.curStep = '2'
        self.save()
        return False
      else:
        self.curStep = '3'
        self.save()
    return True

  def onSolvationSubmit(self, sp):
    try:
      LessonProblem.objects.filter(lesson_type='lesson1', lesson_id=self.id)[0].delete()
    except:
      pass
    # Now check the sp (solvation parameter) object to make sure they used an RHDO structure
    # with a 5 angstrom distance to the edge of the protein
    if float(self.curStep) >= 1.0 and float(self.curStep) < 2.0:
      if sp.solvation_structure != 'rhdo':
        lessonprob = LessonProblem(lesson_type='lesson1', lesson_id=self.id, errorstep=2, severity=2, description='You used the wrong solvation structure. Return to the <a href="/solvate/">Solvate</a> page and select "Rhombic Dodecahedron (X=Y=Z)" under "Choose the shape of the solvation structure".')
        lessonprob.save()
        return False
      if (float(sp.xtl_x) < 30 or float(sp.xtl_x) > 33) or (float(sp.xtl_y) < 30 or float(sp.xtl_y) > 33) or (float(sp.xtl_z) < 30 or float(sp.xtl_z) > 33):
        lessonprob = LessonProblem(lesson_type='lesson1', lesson_id=self.id, errorstep=2, severity=2, description='The wrong radius size was set. Return to the <a href="/solvate/">Solvate</a> page and set the Buffer Size to 15.0 angstroms.')
        lessonprob.save()
        return False
      self.curStep = '1.5'
      self.save()
    return True

  def onSolvationDone(self, sp):
    try:
      lessonprob = LessonProblem.objects.filter(lesson_type='lesson1', lesson_id=self.id)[0]
    except:
      lessonprob = None
    if lessonprob:
      self.curStep = str(float(self.curStep) - (float(self.curStep) % 1))
      self.save()
      return False

    if float(self.curStep) == 1.5:
      if sp.status == 'F':
        lessonprob = LessonProblem(lesson_type='lesson1', lesson_id=self.id, errorstep=2, severity=9, description='The job did not complete correctly. Return to the <a href="/solvate/">Solvate</a> page and re-run the job, checking that all parameters were set correctly. If the problem persists, contact the CHARMMing administrator.')
        lessonprob.save()
        self.curStep = '1'
        self.save()
        return False
      else:
        self.curStep = '2'
        self.save()
    return True

  def onNMASubmit(self, postdata):
    return True

  def onNMADone(self, file):
    return True

  def onMDSubmit(self, mdp):
    # Clear any old lessonproblems
    try:
      LessonProblem.objects.get(lesson_type='lesson1', lesson_id=self.id).delete()
    except:
      pass
    if float(self.curStep) >= 3.0 and float(self.curStep) < 4.0:
      if mdp.parent.action is not 'minimization':
        lessonprob = LessonProblem(lesson_type='lesson1', lesson_id=self.id, errorstep=4, severity=4, description='Please use the minimized and solvated PDB coordinates. Return to the <a href="/dynamics/md">Molecular Dynamics</a> page and select "Coordinates from minimization".')
        lessonprob.save()
        return False
      if mdp.ensemble != 'heat':
        lessonprob = LessonProblem(lesson_type='lesson1', lesson_id=self.id, errorstep=4, severity=2, description='You used an equilibration calculation instead of a heating one. Please return to the <a href="/dynamics/md">Molecular Dynamics</a> page and select "Heating (adds kinetic energy to the system)."')
        lessonprob.save()
        return False
      if mdp.nstep != 1000:
        lessonprob = LessonProblem(lesson_type='lesson1', lesson_id=self.id, errorstep=4, severity=2, description='The number of steps in the simulation was not set correctly. Please return to the <a href="/dynamics/md">Molecular Dynamics</a> page and set "Number of steps (NSTEP)" to 1000.')
        lessonprob.save()
        return False
      if float(mdp.firstt) != 210.15:
        lessonprob = LessonProblem(lesson_type='lesson1', lesson_id=self.id, errorstep=4, severity=2, description='The starting temperature was not set correctly. Please return to the <a href="/dynamics/md">Molecular Dynamics</a> page and set "Starting temperature (FIRSTT)" to 210.15 K to continue.')
        lessonprob.save()
        return False
      if float(mdp.teminc) != 10:
        lessonprob = LessonProblem(lesson_type='lesson1', lesson_id=self.id, errorstep=4, severity=2, description='The temperature increment was not set correctly. Please return to the <a href="/dynamics/md">Molecular Dynamics</a> page and set "Temperature increment (TEMINC)" to 10 K to continue.')
        lessonprob.save()
        return False
      if float(mdp.ihtfrq) != 100:
        lessonprob = LessonProblem(lesson_type='lesson1', lesson_id=self.id, errorstep=4, severity=2, description='The heating frequency was not set correctly. Please return to the <a href="/dynamics/md">Molecular Dynamics</a> page and set "Steps between temperature increments (IHTFRQ)" to 100 steps.')
        lessonprob.save()
        return False
      if float(mdp.finalt) != 310.15:
        lessonprob = LessonProblem(lesson_type='lesson1', lesson_id=self.id, errorstep=4, severity=2, description='The final temperature was not set to physiological temperature. Please return to the <a href="/dynamics/md">Molecular Dynamics</a> page and set "Final temperature (FINALT)" to 310.15 K.')
        lessonprob.save()
        return False
      if float(mdp.tbath) != 310.15:
        lessonprob = LessonProblem(lesson_type='lesson1', lesson_id=self.id, errorstep=4, severity=2, description='The temperature bath was not set to physiological temperature. Please return to the <a href="/dynamics/md">Molecular Dynamics</a> page and set "Temperature bath (TBATH)" to 310.15 K.')
        lessonprob.save()
        return False
    self.curStep = '3.5'
    self.save()
    return True

  def onMDDone(self, mdp):
    try:
      lessonprob = LessonProblem.objects.get(lesson_type='lesson1', lesson_id=self.id)
    except:
      lessonprob = None
    if lessonprob:
      self.curStep = str(float(self.curStep) - (float(self.curStep) % 1))
      self.save()
      return False

    if float(self.curStep) == 3.5:
      if mdp.status == 'F':
        lessonprob = LessonProblem(lesson_type='lesson1', lesson_id=self.id, errorstep=4, severity=9, description='The job did not complete correctly. Please return to the <a href="/dynamics/md">Molecular Dynamics</a> page and re-run the job. Check the output of the job as well, to see if there are any problems. If the problem persists, contact the CHARMMing administrator.')
        lessonprob.save()
        return False
      else:
        self.curStep = '4'
        self.save()
    return True

  def onLDSubmit(self, postdata):
    return True

  def onLDDone(self, file):
    return True

  def onSGLDSubmit(self, postdata):
    return True

  def onSGLDDone(self, file):
    return True

  def onRMSDSubmit(self, file):
    return True

  def onNATQSubmit(self, postdata):
    return True

  def onEnergySubmit(self, postdata):
    return True

  def onEnergyDone(self, finale):
    return True

  # generates html for the lesson status page
  def generateStatusHtml(self, file):
    step_status_list = []
    step_status_list.append("<tr class='status'><td class='status'>1. File Uploaded: ")
    step_status_list.append("<tr class='status'><td class='status'>2. Solvation: ")
    step_status_list.append("<tr class='status'><td class='status'>3. Minimization: ")
    step_status_list.append("<tr class='status'><td class='status'>4. MD: ")
    # This will store all the status and the steps, clearing the template of logic
    # And only displaying the status
    try:
      lessonprob = LessonProblem.objects.filter(lesson_type='lesson1', lesson_id=self.id)[0]
    except:
      lessonprob = None
    for i in range(self.nSteps):
      if lessonprob and lessonprob.errorstep == math.floor(self.curStep + 1) and math.floor(self.curStep) == i:
        step_status_list[i] += ("<a class='failed' href='javascript:open_failure();'>Failed</a></td></tr>")
        continue
      elif (float(self.curStep) - 0.5) == i and float(self.curStep) % 1.0 == 0.5:
        step_status_list[i] += ("<span class='running'>Running</span></td></tr>")
        continue
      elif i < float(self.curStep) or len(step_status_list) + 1 == float(self.curStep):
        step_status_list[i] += ("<span class='done'>Done</span></td></tr>")
        continue
      elif i + 1 > float(self.curStep):
        step_status_list[i] += ("<span class='inactive'>N/A</span></td></tr>")
        continue

    return step_status_list

  # Returns a list where each index corresponds to lesson progress
  # on the display lesson page
  def getHtmlStepList(self):
    # 2 is running
    # 1 is successfully done
    # 0 is not started
    # -1 is error
    htmlcode_list = []
    for step in range(self.nSteps):
      htmlcode_list.append(0)
    if float(self.curStep) > 0:
      htmlcode_list[0] = 1
    if float(self.curStep) > 1:
      if float(self.curStep) == 1.5:
        htmlcode_list[1] = 2
      else:
        htmlcode_list[1] = 1
    if float(self.curStep) > 2:
      if float(self.curStep) == 2.5:
        htmlcode_list[2] = 2
      else:
        htmlcode_list[2] = 1
    if float(self.curStep) > 3:
      if float(self.curStep) == 3.5:
        htmlcode_list[3] = 2
      else:
        htmlcode_list[3] = 1
    try:
      lessonprob = LessonProblem.objects.filter(lesson_type='lesson1', lesson_id=self.id)[0]
      htmlcode_list[lessonprob.errorstep - 1] = -1
    except:
      lessonprob = None
    return htmlcode_list
