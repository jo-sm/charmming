from django.db import models
from django.contrib.auth.models import User
from charmming.models import LessonProblem
from charmming.models import Structure

import os
import math

class Lesson3(models.Model):
  # data for lessons (should not be overridden by subclasses)
  # specifying both user and PDB is redundant (since the PDB references the user),
  # but makes it easier to look up all lessons being done by a particular user.
  user = models.ForeignKey(User)
  nSteps = models.PositiveIntegerField(default=4)
  curStep = models.DecimalField(default=0, decimal_places=1, max_digits=3)

  def onFileUpload(self):
    try:
      LessonProblem.objects.get(lesson_type='lesson3', lesson_id=self.id).delete()
    except:
      pass
    try:
      oldlessons = Lesson3.objects.filter(user=self.user)
      second_upload = 0
      for lessn in oldlessons:
        if lessn.curStep == 3:
          oldlesson = lessn
          file = Structure.objects.get(owner=self.user, lesson_id=oldlesson.id)
          second_upload = 1
          break
        lessonprob = LessonProblem(lesson_type='lesson3', lesson_id=file.id, errorstep=4, severity=9, description='You must perform the other steps first! This is the last step of the lesson!')
    except:
      second_upload = 0
    if second_upload == 1:
      try:
        file2 = Structure.objects.get(selected='y', owner=self.user, lesson_id=self.id)
        filename1 = file2.location + '/seq2pdb.inp'
        os.stat(filename1)
        filename2 = file2.location + '/sequ.pdb'
        os.stat(filename2)
      # filename4 = file2.location + 'new_' + file2.stripDotPDB(file2.filename) + '-a-goodhet.pdb'
      except:
        lessonprob = LessonProblem(lesson_type='lesson3', lesson_id=file.id, errorstep=5, severity=9, description='You did not upload 1YJP sequence properly. Go to the <a href="/fileupload">Submit Structure</a> page and try again.')
        lessonprob.save()
        return False
      if not self.verifyFileByLine(filename1, "GLY ASN ASN GLN GLN ASN TYR"):
        lessonprob = LessonProblem(lesson_type='lesson3', lesson_id=file.id, errorstep=5, severity=9, description='You did not enter the right sequence of 1YJP. Go to the <a href="/fileupload">Submit Structure</a> page and try again.')
        lessonprob.save()
        return False
      oldlesson.curStep += 1
      oldlesson.save()
      file2.lesson_id = file.lesson_id
      file2.save()
      self.delete()
    else:
      file = Structure.objects.filter(selected='y', owner=self.user, lesson_id=self.id)[0]
      try:
        # filename1 = '%s/mytemplates/lessons/lesson3/1yjp-sequ.pdb' % charmming_config.charmming_root
        # filename2 = file.location + 'new_' + file.stripDotPDB(file.filename) + '-sequ-pro.pdb'
        filename1 = file.location + '/seq2pdb.inp'
        os.stat(filename1)
        filename2 = file.location + '/sequ.pdb'
        os.stat(filename2)
      except:
        lessonprob = LessonProblem(lesson_type='lesson3', lesson_id=self.id, errorstep=1, severity=9, description='Please choose the option to upload your own sequence in the <a href="/fileupload">Submit Structure</a> page.')
        lessonprob.save()
        return False
      if not self.verifyFileByLine(filename1, "GLY ASN ASN GLN GLN ASN TYR"):
        lessonprob = LessonProblem(lesson_type='lesson3', lesson_id=self.id, errorstep=1, severity=9, description='You did not enter the right sequence of 1YJP. Go to the <a href="/fileupload">Submit Structure</a> page and try again.')
        lessonprob.save()
        return False
      self.curStep = '1'
    self.save()
    return True

  def onEditPDBInfo(self, postdata):
    return True

  def onMinimizeSubmit(self, mp, filename):
    try:
      LessonProblem.objects.filter(lesson_type='lesson3', lesson_id=self.id)[0].delete()
    except:
      pass
    # check that user has the right number of steps...
    if float(self.curStep) >= 1.0 and float(self.curStep) < 2.0:
      if int(mp.sdsteps) != 1000:
        lessonprob = LessonProblem(lesson_type='lesson3', lesson_id=self.id, errorstep=2, severity=2, description='SD steps were set to %s. Please return to the <a href="/minimize/">Minimize</a> page and set SDsteps to 1000' % mp.sdsteps)
        lessonprob.save()
        return False
      if int(mp.abnrsteps) != 1000:
        lessonprob = LessonProblem(lesson_type='lesson3', lesson_id=self.id, errorstep=2, severity=2, description='ABNR steps were set to %s. Please return to the <a href="/minimize/">Minimize</a> page and set ABNR to 1000.' % mp.abnrsteps)
        lessonprob.save()
        return False
      if float(mp.tolg) != .01:
        lessonprob = LessonProblem(lesson_type='lesson3', lesson_id=self.id, errorstep=2, severity=2, description='TOLG was set to %s. Please return to the <a href="/minimize/">Minimize</a> page and set "Gradient tolerance" to 0.01.' % str(mp.tolg))
        lessonprob.save()
        return False
      if mp.usepbc == 't':
        lessonprob = LessonProblem(lesson_type='lesson3', lesson_id=self.id, errorstep=2, severity=2, description='Minimization used Periodic Boundary Conditions. Please return to the <a href="/minimize/">Minimize</a> page and make sure the "Use Periodic Boundary Conditions (PBC) in minimization" box is left unchecked.')
        lessonprob.save()
        return False
    # 1.5 Means it is running
    self.curStep = '1.5'
    self.save()
    return True

  def onMinimizeDone(self, task):
    try:
      lessonprob = LessonProblem.objects.get(lesson_type='lesson3', lesson_id=self.id)
    except:
      lessonprob = None
    if lessonprob:
      self.curStep = str(float(self.curStep) - (float(self.curStep) % 1))
      self.save()
      return False
    if float(self.curStep) == 1.5:
      if task.status == 'F':
        lessonprob = LessonProblem(lesson_type='lesson3', lesson_id=self.id, errorstep=2, severity=9, description='The Job did not complete correctly. Check your job output, then return to the <a href="/minimize/">Minimize</a> page and re-run the job after checking that all of your parameters were correct. If the problem persists, contact the CHARMMing administrator.')
        lessonprob.save()
        self.curStep = '1'
        self.save()
        return False
      else:
        self.curStep = '2'
        self.save()
    return True

  def onSolvationSubmit(self, postdata):
    return True

  def onSolvationDone(self, file):
    return True

  def onNMASubmit(self, postdata):
    return True

  def onNMADone(self, file):
    return True

  def onMDSubmit(self, postdata):
    return True

  def onMDDone(self, file):
    return True

  def onLDSubmit(self, postdata):
    return True

  def onLDDone(self, file):
    return True

  def onSGLDSubmit(self, sgldp):
    try:
      LessonProblem.objects.filter(lesson_type='lesson3', lesson_id=self.id)[0].delete()
    except:
      pass
    if float(self.curStep) >= 2.0 and float(self.curStep) < 3.0:
      if sgldp.parent.action != "minimization":
        lessonprob = LessonProblem(lesson_type='lesson3', lesson_id=self.id, errorstep=3, severity=2, description='Please return to the <a href="/dynamics/ld/">Langevyn Dynamics</a> page and check the correct box to use the coordinates from minimization')
        lessonprob.save()
        return False
      if float(sgldp.fbeta) != 5.0:
        lessonprob = LessonProblem(lesson_type='lesson3', lesson_id=self.id, errorstep=3, severity=2, description='You used the wrong FBETA. Please return to the <a href="/dynamics/ld/">Langevin Dynamics</a> page and set FBETA to 5.0.')
        lessonprob.save()
        return False
      if int(sgldp.nstep) != 1000:
        lessonprob = LessonProblem(lesson_type='lesson3', lesson_id=self.id, errorstep=3, severity=2, description='You set the number of steps incorrectly. Please return to the <a href="/dynamics/ld/">Langevin Dynamics</a> page and set the number of steps to 1000.')
        lessonprob.save()
        return False
      self.curStep = '2.5'
      self.save()
    return True

  def onSGLDDone(self, sgldp):
    try:
      lessonprob = LessonProblem.objects.filter(lesson_type='lesson3', lesson_id=self.id)[0]
    except:
      lessonprob = None

    if lessonprob:
      self.curStep = str(float(self.curStep) - (float(self.curStep) % 1))
      self.save()
      return False
    if float(self.curStep) == 2.5:
      if sgldp.status == 'F':
        lessonprob = LessonProblem(lesson_type='lesson3', lesson_id=self.id, errorstep=3, severity=9, description='The Job did not complete correctly. Check your job output, then return to the <a href="/minimize/">Minimize</a> page and re-run the job after checking that all of your parameters were correct. If the problem persists, contact the CHARMMing administrator.')
        lessonprob.save()
        self.curStep = '2'
        self.save()
        return False
      else:
        self.curStep = '3'
        self.save()
    return True

  def onRMSDSubmit(self, postdata):
    # no checking
    try:
      LessonProblem.objects.filter(lesson_type='lesson3', lesson_id=self.id)[0].delete()
    except:
      pass
    self.curStep = '4'
    self.save()
    return True

  def onNATQSubmit(self, postdata):
    return True

  def onEnergySubmit(self, postdata):
    return True

  def onEnergyDone(self, finale):
    return True

  def onBuildStructureDone(self, file):
    return True

  def onBuildStructureSubmit(self, postdata):
    return True

  # generates html for the lesson status page
  def generateStatusHtml(self, file):
    step_status_list = []
    step_status_list.append("<tr class='status'><td class='status'>1. File Uploaded: ")
    step_status_list.append("<tr class='status'><td class='status'>2. Minimization: ")
    step_status_list.append("<tr class='status'><td class='status'>3. SGLD: ")
    step_status_list.append("<tr class='status'><td class='status'>4. Compare: ")
    # This will store all the status and the steps, clearing the template of logic
    # And only displaying the status
    try:
      lessonprob = LessonProblem.objects.filter(lesson_type='lesson3', lesson_id=self.id)[0]
    except:
      lessonprob = None
    for i in range(self.nSteps):
      if lessonprob and lessonprob.errorstep == math.floor(self.curStep + 1) and math.floor(self.curStep) == i:
        step_status_list[i] += ("<a class='failed' onclick='javascript:open_failure();'>Failed</a></td></tr>")
        continue
      elif (float(self.curStep) - 0.5) == i and float(self.curStep) % 1 == 0.5:
        step_status_list[i] += ("<a class='running'>Running</a></td></tr>")
        continue
      elif i < float(self.curStep):
        step_status_list[i] += ("<a class='done'>Done</a></td></tr>")
        continue
      elif i >= float(self.curStep):
        step_status_list[i] += ("<a class='inactive'>N/A</a></td></tr>")
        continue
    return step_status_list

  # Returns a list where each index corresponds to lesson progress
  # on the display lesson page
  def getHtmlStepList(self):
    # 2 is running
    # 1 is successfully done
    # 0 is not started
    # -1 is error
    htmlcode_list = []
    for step in range(self.nSteps):
      htmlcode_list.append(0)
    if float(self.curStep) > 0:
      htmlcode_list[0] = 1
    if float(self.curStep) > 1:
      if float(self.curStep) == 1.5:
        htmlcode_list[1] = 2
      else:
        htmlcode_list[1] = 1
    if float(self.curStep) > 2:
      if float(self.curStep) == 2.5:
        htmlcode_list[2] = 2
      else:
        htmlcode_list[2] = 1
    if float(self.curStep) > 3:
      if float(self.curStep) == 3.5:
        htmlcode_list[3] = 2
      else:
        htmlcode_list[3] = 1
    try:
      lessonprob = LessonProblem.objects.filter(lesson_type='lesson3', lesson_id=self.id)[0]
      htmlcode_list[lessonprob.errorstep - 1] = -1
    except:
      lessonprob = None
    return htmlcode_list

  def verifyFileByLine(self, filename, line):
    fread = open(filename, 'r')
    for fline in fread:
      if fline.rstrip() == line.rstrip():
        return True
    return False
