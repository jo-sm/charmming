from django.db import models
from django.contrib.auth.models import User

from charmming import charmming_config
from charmming.models import Structure
from charmming.models import LessonProblem
from charmming.utils.lesson import diffPDBs

import os
import math

class Lesson4(models.Model):
  # data for lessons (should not be overridden by subclasses)
  # specifying both user and Structure is redundant (since the Structure references the user),
  # but makes it easier to look up all lessons being done by a particular user.
  user = models.ForeignKey(User)
  nSteps = models.PositiveIntegerField(default=4)
  curStep = models.DecimalField(default=0, decimal_places=1, max_digits=3)

  def onFileUpload(self):
    try:
      LessonProblem.objects.filter(lesson_type='lesson4', lesson_id=self.id)[0].delete()
    except:
      pass
    file = Structure.objects.get(selected='y', owner=self.user, lesson_id=self.id)
    try:
      filename1 = file.location + '/butane.crd'
      os.stat(filename1)
    except:
      lessonprob = LessonProblem(lesson_type='lesson4', lesson_id=self.id, errorstep=1, severity=9, description='The file you uploaded was not the correct one. Go to the to the <a href="/fileupload">Submit Structure</a> page and upload the files provided.')
      lessonprob.save()
      return False
    try:
      filename1 = '%s/mytemplates/lessons/lesson4/butane.crd' % charmming_config.charmming_root
      os.stat(filename1)
      filename2 = file.location + '/butane.crd'
      os.stat(filename2)
      if not diffPDBs(file, filename1, filename2):
        lessonprob = LessonProblem(lesson_type='lesson4', lesson_id=self.id, errorstep=1, severity=9, description='The CRD you uploaded was not the correct one. Go to the <a href="/fileupload">Submit Structure</a> page and upload the files provided.')
        lessonprob.save()
        return False
    except:
      lessonprob = LessonProblem(lesson_type='lesson4', lesson_id=self.id, errorstep=1, severity=9, description='The CRD/PSF you submitted did not upload properly. Go to the <a href="/fileupload">Submit Structure</a> page and try again.')
      lessonprob.save()
      return False
    self.curStep = '1.0'
    self.save()
    return True

  def onEditPDBInfo(self, postdata):
    return True

  def onMinimizeSubmit(self, mp, filename):
    try:
      LessonProblem.objects.filter(lesson_type='lesson4', lesson_id=self.id)[0].delete()
    except:
      pass
    if float(self.curStep) >= 3.0 and float(self.curStep) < 4.0:
      if mp.sdsteps != 100:
        lessonprob = LessonProblem(lesson_type='lesson4', lesson_id=self.id, errorstep=4, severity=2, description='SD steps were set to %s. Please return to the <a href="/minimize/">Minimize</a> page and set SDsteps to 1000' % mp.sdsteps)
        lessonprob.save()
        return False
      if mp.abnrsteps != 1000:
        lessonprob = LessonProblem(lesson_type='lesson4', lesson_id=self.id, errorstep=4, severity=2, description='ABNR steps were set to %s. Please return to the <a href="/minimize/">Minimize</a> page and set ABNR to 1000.' % mp.abnrsteps)
        lessonprob.save()
        return False
      if float(mp.tolg) != .05:
        lessonprob = LessonProblem(lesson_type='lesson4', lesson_id=self.id, errorstep=4, severity=2, description='TOLG was set to %s. Please return to the <a href="/minimize/">Minimize</a> page and set "Gradient tolerance" to 0.05.' % str(mp.tolg))
        lessonprob.save()
        return False
      # 3.5 Means it is running
      self.curStep = '3.5'
      self.save()
    return True

  def onMinimizeDone(self, mp):
    try:
      lessonprob = LessonProblem.objects.filter(lesson_type='lesson4', lesson_id=self.id)[0]
    except:
      lessonprob = None
    if lessonprob:
      self.curStep = str(float(self.curStep) - (float(self.curStep) % 1))
      self.save()
      return False

    if float(self.curStep) == 3.5:
      if mp.status == 'F':
        lessonprob = LessonProblem(lesson_type='lesson4', lesson_id=self.id, errorstep=3, severity=9, description='The Job did not complete correctly. Check your job output, then return to the <a href="/minimize/">Minimize</a> page and re-run the job after checking that all of your parameters were correct. If the problem persists, contact the CHARMMing administrator.')
        lessonprob.save()
        return False
      else:
        self.curStep = '4'
        self.save()
    return True

  def onSolvationSubmit(self, postdata):
    return True

  def onSolvationDone(self, file):
    return True

  def onNMASubmit(self, postdata):
    return True

  def onNMADone(self, file):
    return True

  def onMDSubmit(self, postdata):
    return True

  def onMDDone(self, file):
    return True

  def onLDSubmit(self, postdata):
    return True

  def onLDDone(self, file):
    return True

  def onSGLDSubmit(self, postdata):
    return True

  def onSGLDDone(self, file):
    return True

  def onEnergyDone(self, et):
    try:
      lessonprob = LessonProblem.objects.get(lesson_type='lesson4', lesson_id=self.id)
    except:
      lessonprob = None
    if float(et.finale) == 0.0:
      return True
    if lessonprob:
      if float(self.curStep) == 1.5:
        self.curStep = '1'
      if float(self.curStep) == 2.5:
        self.curStep = '2'
      self.save()
      return False
    if float(self.curStep) == 1.5:
      # finale should be 6.84842 but we'll give a slight margin for error
      if float(et.finale) < float(6.8) or float(et.finale) > float(6.9):
        lessonprob = LessonProblem(lesson_type='lesson4', lesson_id=self.id, errorstep=2, severity=9, description="Your energy was wrong (should be ~ 6.864, you got %s). Please check that the RTF file you uploaded was correct, then return to the <a href='/charmming/energy'>Energy</a> page and re-run your calculation. If you can't figure out the problem, the correct RTF is <a href=/charmming/lessons/download/lesson4/butane.rtf>here</a>." % str(float(et.finale)))
        lessonprob.save()
        return False
      self.curStep = '2'
      self.save()
      return True
    elif float(self.curStep) == 2.5:
      # finale should be -49122.53689 but we'll give a slight margin for error
      # if float(et.finale) < -49124.5 or float(et.finale) > -49121.5:
      if float(et.finale) < float(-49127.0) or float(et.finale) > float(-49124.0):
        lessonprob = LessonProblem(lesson_type='lesson4', lesson_id=self.id, errorstep=3, severity=9, description='Your energy was wrong (should be ~ -49125.84). Please return to the <a href="/energy">Energy</a> page and check that you set up the QM/MM calculation correctly.')
        lessonprob.save()
        return False
      self.curStep = '3'
      self.save()
      return True
    else:
      return False

  def onRMSDSubmit(self, file):
    return True

  def onNATQSubmit(self, postdata):
    return True

  # generates html for the lesson status page
  def onEnergySubmit(self, et):
    try:
      LessonProblem.objects.get(lesson_type='lesson4', lesson_id=self.id).delete()
    except:
      pass

    if float(self.curStep) == 1.0:
      # nothing really to validate here...
      self.curStep = '1.5'
      self.save()
    elif float(self.curStep) == 2.0:
      # if not postdata.has_key('useqmmm'):
      if et.useqmmm != 'y':
        lessonprob = LessonProblem(lesson_type='lesson4', lesson_id=self.id, errorstep=3, severity=9, description='You did not use QM/MM in your energy calculation. Return to the <a href="/energy">Energy</a> page and select "Use Multi-Scale Modeling", then select "Additive Model (QM/MM)" and set up your calculation.')
        lessonprob.save()
        return False
      # if et.qmmmsel !='type c1 .or. type h11 .or. type h12 .or. type h13 .or. type c2 .or. type h21 .or. type h22':
      # if et.qmmmsel != 'atom buta 1 c1 .or. atom buta 1 c2 .or. atom buta 1 h11 .or. atom buta 1 h12 .or. atom buta 1 h13 .or. atom buta 1 h21 .or. atom buta 1 h22':
      if et.qmmmsel != 'bynum 1:7':
        lessonprob = LessonProblem(lesson_type='lesson4', lesson_id=self.id, errorstep=3, severity=9, description='You did not make the correct QM/MM selection. Please re-read the instructions, then return to the <a href="/energy">Energy</a> page and set up your calculation again.')
        return False
      self.curStep = '2.5'
      self.save()
    return True

  def onBuildStructureDone(self, postdata):
    return True

  def onBuildStructureSubmit(self, postdata):
    return True

  # generates html for the lesson status page
  def generateStatusHtml(self, file):
    step_status_list = []
    step_status_list.append("<tr class='status'><td class='status'>1. File Uploaded: ")
    step_status_list.append("<tr class='status'><td class='status'>2. Classical Energy: ")
    step_status_list.append("<tr class='status'><td class='status'>3. QM/MM Energy: ")
    step_status_list.append("<tr class='status'><td class='status'>4. QM/MM Minimization: ")
    # This will store all the status and the steps, clearing the template of logic
    # And only displaying the status
    try:
      lessonprob = LessonProblem.objects.get(lesson_type='lesson4', lesson_id=self.id)
    except:
      lessonprob = None
    for i in range(self.nSteps):
      if lessonprob and lessonprob.errorstep == math.floor(self.curStep + 1) and math.floor(self.curStep) == i:
        step_status_list[i] += ("<a class='failed' href='javascript:open_failure'>Failed</a></td></tr>")
        continue
      elif (float(self.curStep) - 0.5) == i and float(self.curStep) % 1 == 0.5:
        step_status_list[i] += ("<span class='running'>Running</span></td></tr>")
        continue
      elif i < float(self.curStep):
        step_status_list[i] += ("<span class='done'>Done</span></td></tr>")
        continue
      elif i >= float(self.curStep):
        step_status_list[i] += ("<span class='inactive'>N/A</span></td></tr>")
        continue
    return step_status_list

  # Returns a list where each index corresponds to lesson progress
  # on the display lesson page
  def getHtmlStepList(self):
    # 2 is running
    # 1 is successfully done
    # 0 is not started
    # -1 is error
    htmlcode_list = []
    for step in range(self.nSteps):
      htmlcode_list.append(0)
    if float(self.curStep) > 0:
      htmlcode_list[0] = 1
    if float(self.curStep) > 1:
      if float(self.curStep) == 1.5:
        htmlcode_list[1] = 2
      else:
        htmlcode_list[1] = 1
    if float(self.curStep) > 2:
      if float(self.curStep) == 2.5:
        htmlcode_list[2] = 2
      else:
        htmlcode_list[2] = 1
    if float(self.curStep) > 3:
      if float(self.curStep) == 3.5:
        htmlcode_list[3] = 2
      else:
        htmlcode_list[3] = 1
    try:
      lessonprob = LessonProblem.objects.filter(lesson_type='lesson4', lesson_id=self.id)[0]
      htmlcode_list[lessonprob.errorstep - 1] = -1
    except:
      lessonprob = None
    return htmlcode_list
