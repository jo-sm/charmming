from django.db import models

class DDInteractionType(models.Model):
  interaction_type_name = models.CharField(max_length=200)
  description = models.CharField(max_length=250)
