from django.db import models
from django.contrib.auth.models import User

from charmming.models import DDJobType, SchedulerJob

class DDJob(models.Model):
  owner = models.ForeignKey(User)
  job_scheduler_id = models.ForeignKey(SchedulerJob)
  job_owner_index = models.PositiveIntegerField(default=0)
  job_name = models.CharField(max_length=200, null=True, blank=True)
  description = models.CharField(max_length=250, null=True, blank=True)
  job_start_time = models.DateTimeField(null=True, blank=True)
  job_end_time = models.DateTimeField(null=True, blank=True)
  job_type = models.ForeignKey(DDJobType, null=True, blank=True)
