from django.db import models
from django.contrib.auth.models import User

class DDProject(models.Model):
  owner = models.ForeignKey(User)
  project_name = models.CharField(max_length=200)
  description = models.CharField(max_length=250, null=True, blank=True)
  date_created = models.DateTimeField(null=True, blank=True)
  selected = models.CharField(max_length=1)
