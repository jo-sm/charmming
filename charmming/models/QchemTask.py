from django.db import models

from charmming.models import Task

from pychm3.lib import Atom

import os
import re
import shutil
import pickle
import numpy

class QchemTask(Task):
  jobtype = models.CharField(max_length=5) # force is the longest jobtype string
  exchange = models.CharField(max_length=15) # allows basis set updates
  charge = models.IntegerField(default=0)
  correlation = models.CharField(max_length=10, null=True)
  basis = models.CharField(max_length=10)
  multiplicity = models.PositiveIntegerField(default=0)
  # useqmmm = models.CharField(max_length=1,null=True,default="n")
  # modelType = models.CharField(max_length=30,null=True,default=None)
  # qmmmsel = models.CharField(max_length=250,null=True) #This is obsolete.
  # those last three fields will stay there until something else happens

  def finish(self):
    """test if the job suceeded, create entries for output"""

    loc = self.workstruct.structure.location
    bnm = self.workstruct.identifier
    basepath = loc + '/' + bnm + "-" + self.action
    try:
      os.stat(basepath + '.out')
    except:
      self.status = 'F'
      self.save()
      return
    # path = basepath + ".out"
    if "qchem-opt" == self.action:
      # check the output file for the final optimization
      # extract those coordinates, feed them into pychm and output new PDB
      # now we open up our output file...
      qchem_out = open(basepath + ".out", "r")
      # hold the current coords in an array of lines
      # check for "Optimization Cycle"
      # get the coordinates starting at ATOM after that
      # dumpe ach time you get a new optimization cycle
      coord_lines = []
      opt_cycle = False
      coords_start = False
      for line in qchem_out.readlines():
        if not opt_cycle: # haven't see "Optimization Cycle"
          if "Optimization Cycle" in line:
            opt_cycle = True
            coord_lines = [] # clear after every cycle
            continue
          if "OPTIMIZATION CONVERGED" in line: # at this point we stop caring
            break
        else:
          if not coords_start: # haven't seen 'ATOM' yet
            if "ATOM" in line:
              coords_start = True
              continue
          else:
            if "Point Group" not in line:
              new_line = line.strip()
              m = re.search("\D", new_line)
              if m:
                coord_lines.append(new_line[m.start():].strip())
            else:
              coords_start = False
              opt_cycle = False
      self_act = self.action
      try:
        parent_act = self.parent.action
      except: # no parent found
        parent_act = False
      pdb_atoms = []
      ter_lines = []
      other_lines = []
      if parent_act: # has a CHARMM/PDB-generating parent task
        parent_path = basepath.replace(self_act, parent_act)
        parent_pdb = parent_path + ".pdb"
        parent_file = open(parent_pdb, "r")
        for line in parent_file.readlines():
          if line.startswith("ATOM") or line.startswith("HETATM"):
            curr_atom = Atom(line, format="charmm")
            # curr_atom.set_element() # otherwise element will become W-GO
            pdb_atoms.append(curr_atom)
          elif line.startswith('TER'):
            ter_lines.append(line)
          elif not line.startswith("END"):
            if parent_act.upper() in line:
              other_lines.append(line.replace(parent_act.upper(), self_act.upper()))
            else:
              other_lines.append(line)
          else:
            continue
        parent_file.close()

        if len(pdb_atoms) != len(coord_lines):
          self.status = 'F'
          self.save()
          return

        pdb_write_atoms = []
        crd_write_atoms = []
        for i in range(0, len(coord_lines)):
          if coord_lines[i][0:2].strip() == pdb_atoms[i].element:
            try:
              pdb_atoms[i].set_coordsFromLine(coord_lines[i], informat="xyz")
              pdb_write_atoms.append(pdb_atoms[i].Print("charmm"))
              crd_write_atoms.append(pdb_atoms[i].Print("xcrd"))
            except:
              self.status = 'F'
              self.save()
              return
          else:
            self.save()
            return
        # now copy the PSF over...
        # TODO: Rebuild every time if we have psfgen
        shutil.copyfile((basepath.replace(self_act, parent_act) + ".psf"), (basepath + ".psf"))
        # make CRD...
        old_crd = open(parent_path + ".crd")  # open old crd
        other_crd_lines = []  # let's just take the old lines since the number of atoms won't increase
        i = 0
        for line in old_crd.readlines():
          if i < 4:
            other_crd_lines.append(line)
            i += 1
          else:
            break
        new_crd = open(basepath + ".crd", "w")
        for line in other_crd_lines:
          new_crd.write(line)
        for line in crd_write_atoms:
          new_crd.write(line)
        new_crd.close()
        # now write PDB...
        woof = open(basepath + ".pdb", "w")

        for line in other_lines:  # we assume these can go at the start...usually REMARK
          woof.write(line)
        for atom in pdb_write_atoms:
          woof.write(atom)
        for line in ter_lines:
          woof.write(line)
        woof.write("END\n")
        woof.close()
        # this should be enough
      else:  # if there's no CHARMM parent, load old pickle and modify
        #  pick_path = self.workstruct.localpickle if self.workstruct.localpickle else self.workstruct.structure.pickle
        # IN theory, the above line should work every time, but it never does. Therefore, we do os stat to look for the file.
        pick_path = "%s/%s.dat" % (self.workstruct.structure.location, self.workstruct.identifier)
        try:
          os.stat(pick_path)
        except:
          pick_path = self.workstruct.structure.pickle
        pick_file = open(pick_path, "r")
        inp_file = pickle.load(pick_file)
        pick_file.close()
        seglist = [x.name for x in self.workstruct.segments.all()]
        # workstruct doesn't necessarily have the same atoms as pickle...
        # now that we have a pickle, iterate through atoms and replace coords
        # since order has to be identical, we can just do a naive replace
        # we ASSUME the atoms to be the same since we just used this pickle
        # to perform the task
        try:
          pdb_atoms = ""  # holds atoms for printing PDB
          i = 0  # we need custom loop variable cause pychm uses nothing but iterables
          for seg in inp_file[self.workstruct.modelName].iter_seg():  # put modelname here so we can write whole obj
            if seg.segid in seglist:
              for res in seg.iter_res():
                for atom in res.iter_atom():
                  line = coord_lines[i]
                  coords = numpy.array([float(line[2:17].strip()), float(line[18:29].strip()), float(line[30:41].strip())])
                  atom.cart = coords
                  i += 1
              seg.write(basepath + ".pdb", append=True, outformat="charmm")  # we use this to write to the file immediately, so we can stop caring
              # we can't write the whole file since we don't always have the same segments.
              seg.write(basepath + ".crd", append=True, outformat="xcrd")
          # TODO: Add PSF writing.
        except:  # loop mismatch
          self.status = 'F'
          self.save()
          return
        lpickle = "%s/%s.dat" % (self.workstruct.structure.location, self.workstruct.identifier)
        pick_file = open(lpickle, "w") # make a localpickle
        pickle.dump(inp_file, pick_file)  # dump object, modified
        # now you have a decent pickle file for your build.
        pick_file.close()
        # we don't need to write the PDB here because we don't have a parent task
    if self.status == 'F':
      return
    self.status = 'C'
    self.createStatistics()  # This only fails when the task succeeds, so we shouldn't worry.
    return
