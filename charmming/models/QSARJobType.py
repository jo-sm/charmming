from django.db import models

class QSARJobType(models.Model):
  # LOCK TABLES `qsar_job_types` WRITE;
  # INSERT INTO `qsar_job_types` VALUES (1,'Train',NULL),(2,'Predict',NULL);
  # UNLOCK TABLES;
  job_type_name = models.CharField(max_length=200)
  description = models.CharField(max_length=250, null=True, blank=True)
  # application = models.ForeignKey(applications,null=True, blank=True)
