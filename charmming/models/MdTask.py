from django.db import models
from charmming.models import DynamicsTask

class MdTask(DynamicsTask):
  # temp will represent the temperature in Kelvin if "type" is heat
  temp = models.FloatField(null=True)
  firstt = models.FloatField(null=True)
  finalt = models.FloatField(null=True)
  teminc = models.FloatField(null=True)
  ihtfrq = models.FloatField(null=True)
  tbath = models.FloatField(null=True)
  ensemble = models.CharField(max_length=50)
