from django.db import models

from charmming.models import CGWorkingStructure

class GoModel(models.Model):
  selected = models.CharField(max_length=1)
  cgws = models.ForeignKey(CGWorkingStructure)
  contactType = models.CharField(max_length=10)
  nScale = models.DecimalField(max_digits=6, decimal_places=3)
  kBond = models.DecimalField(max_digits=6, decimal_places=3)
  kAngle = models.DecimalField(max_digits=6, decimal_places=3)
