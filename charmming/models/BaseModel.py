from django.db import models

import datetime

class BaseModel(models.Model):
  class Meta:
    abstract = True

  last_modified_on = models.DateTimeField(default=datetime.datetime.now)
  created_on = models.DateTimeField(default=datetime.datetime.now)

  def save(self, *args, **kwargs):
    self.last_modified_on = datetime.datetime.now()

    return super().save(*args, **kwargs)
