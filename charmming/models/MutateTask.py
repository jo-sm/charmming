from django.db import models
from pychm3.io.pdb import PDB

from charmming.models import Task, WorkingFile

import os
import pickle

class MutateTask(Task):
  MutResi = models.PositiveIntegerField(default=0)
  MutResName = models.CharField(max_length=3, null=True)
  NewResn = models.CharField(max_length=3, null=True) # ALA, SYN, GLY, &c.
  MutSegi = models.CharField(max_length=50, null=True)
  MutFile = models.CharField(max_length=500, null=True)
  lpickle = models.CharField(max_length=500, null=True)

  def finish(self):
    # overrides basic finish, tests if the job succeeded
    loc = self.workstruct.structure.location
    bnm = self.MutFile
    basepath = "%s/%s" % (loc, bnm)
    # create WorkingFile for the input file...
    try:
      path = basepath + ".inp"
      wfinp = WorkingFile()
    except:
      pass
    try:
      WorkingFile.objects.get(task=self, path=path)
    except:
      wfinp.task = self
      wfinp.path = path
      wfinp.canonPath = wfinp.path
      wfinp.type = 'inp'
      wfinp.description = 'mutation script input'
      wfinp.save()

    # Check if an output file was created and if so create
    # a WorkingFile for it.
    path = basepath + ".out"
    try:
      os.stat(path)
    except:
      self.status = 'F'
      return
    wfout = WorkingFile()
    try:
      WorkingFile.objects.get(task=self, path=path)
    except:
      wfout.task = self
      wfout.path = path
      wfout.canonPath = wfout.path
      wfout.type = 'out'
      wfout.description = 'mutation script output'
      wfout.save()

    if self.status == 'F':
      return

    # check and make sure that the output PSF/CRD were
    # created
    path = basepath + ".crd"
    try:
      os.stat(path)
    except:
      self.status = 'F'
      self.save()
      return

    # create Working files for PDB, CRD, and PSF.
    wf = WorkingFile()
    try:
      WorkingFile.objects.get(task=self, path=path)
    except:
      wf.task = self
      wf.path = path
      wf.canonPath = wf.path
      wf.type = 'crd'
      wf.description = 'mutated structure'
      wf.pdbkey = 'mut_' + self.workstruct.identifier
      wf.save()

    path = basepath + ".psf"
    wfpsf = WorkingFile()
    try:
      WorkingFile.objects.get(task=self, path=path)
    except:
      wfpsf.task = self
      wfpsf.path = path
      wfpsf.canonPath = wfpsf.path
      wfpsf.type = 'psf'
      wfpsf.description = 'mutated structure'
      wfpsf.save()

    path = basepath + ".pdb"
    wfpdb = WorkingFile()
    try:
      WorkingFile.objects.get(task=self, path=path)
    except:
      wfpdb.task = self
      wfpdb.path = path
      wfpdb.canonPath = wfpdb.path
      wfpdb.type = 'pdb'
      wfpdb.description = 'mutated structure'
      wfpdb.save()
    # At this point we can assume the script ran successfully so we can wipe tmp.crd
    try:
      os.remove(loc + "/tmp.crd")
    except:
      pass
    # OR at least attempt to.
    # This is where this diverges from normal task finishes
    # We need to reprocess the PDB object, create a new pickle...
    # I know we need this for GLmol, but it may be best to just ignore it for now, since most of our normal
    # PDBs don't have SS records anyway.
    # os.chdir(loc)
    # pdbloc = bnm + ".pdb"
    # try:
    #     f = open("tmp.tmp","w")
    #     p = Popen(["stride","-o",pdbloc],stdout=f) #pdbloc should be something like "1yjp-mt1-mutation.pdb"
    #     f.close()
    #     os.system("stride -o " + pdbloc + " > tmp.tmp")
    #     f = open("tmp.ss","w")
    #     p = Popen([charmming_config.data_home + "/stride2pdb","tmp.tmp"],stdout=f)
    #     f.close()
    #     os.system("stride2pdb tmp.tmp > tmp.ss")
    #     f = open("temp-pdb.pdb","w")
    #     p = Popen(["cat","tmp.ss",pdbloc],stdout=f)
    #     f.close()
    #     os.system("cat tmp.ss " + pdbloc + " > temp-pdb.pdb")
    #     os.remove("tmp.ss")
    #     os.remove("tmp.tmp")
    #     self.save()
    # except Exception as ex:
    #     self.status = "F"
    #     self.save()
    #     return
    try:
      open(self.lpickle, 'r')
    except:
      open(self.workstruct.structure.pickle, 'r')
    try:
      # pdb = cPickle.load(oldpickle)
      # metadata = pdb.get_metaData() #This is where things get fun
      # #Now the PDB file has all the atoms stored in it, what we need now is to use the tmp PDB with the HELIX/SHEET info
      # #take the old metadata and write it in, create a PDB object to store the new metadata, create the new pickle,
      # #then wipe that PDB file since we only use the one that has only atoms.
      # #TODO: Add SEQRES update function
      # #TODO: Add CONECT update function
      # #the metadata's REMARK lines and stuff might remain the same, the issue comes in SEQRES, HELIX, SHEET and a few others.
      # newPDB = open("%s.pdb"%basepath,"a+")    #Write them at the end because PDB doesn't care where they end up.
      # for key in metadata.keys():
      #     if key not in (['helix', 'sheet', 'seqres']):
      #         #Write them to your new PDB file...
      #         linelength = len(metadata[key][0]) #This way we can even out the spacing
      #         for item in metadata[key]:
      #             newPDB.write(key.upper() + (((linelength - len(item)) + 4) * " ") + item + "\n") #4 spaces or less depending on length of the record
      # newPDB.close()
      newPDB = PDB("%s.pdb" % basepath)
      outputstring = "%s.dat" % basepath
      newpickle = open(outputstring, "w") # we're still os.chdir'd to loc
      pickle.dump(newPDB, newpickle)
    except:
      pass
    # try:
    #    os.stat("%s")
    # except:
    # try:
    #    os.remove("temp-pdb.pdb")
    # except:
    #    pass
    self.lpickle = outputstring
    self.workstruct.isBuilt = 't'
    self.workstruct.localpickle = outputstring
    self.workstruct.save()
    """
    randomws = WorkingStructure.objects.get(id=self.workstruct.id)
    randomws.localpickle = outputstring #Make sure localpickle points to the right place...
    randomws.finalTopology = 'alfalfa'
    # self.workstruct.addCRDToPickle(wf.path,'mut_' + self.workstruct.identifier) #THis seems like a bad holdover from -minimization...
    randomws.save() #Make sure localpickle is saved right...
    """
    self.status = 'C'
    self.save()
