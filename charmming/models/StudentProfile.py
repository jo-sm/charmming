from django.db import models
from django.contrib.auth.models import User

class StudentProfile(models.Model):
  institute = models.CharField(max_length=200)
  student = models.OneToOneField(User)
  charmm_license = models.BooleanField()

  # Gets the studentProfile based on the request's user
  def getStudentFromUser(request):
    try:
      student_profile = StudentProfile.objects.filter(student=request.user)[0]
      return student_profile
    except:
      return None
