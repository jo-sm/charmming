from django.db import models
from charmming.models import LdTask

class SgldTask(LdTask):
  tsgavg = models.FloatField(default=0.5, null=True)
  tempsg = models.FloatField(default=1.0, null=True)
  make_sgld_movie = models.BooleanField(default=False)
  sgld_movie_status = models.CharField(max_length=250, null=True)
  sgld_movie_req = models.BooleanField(default=False)
