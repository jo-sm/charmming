from django.db import models
from charmming.models import Task, WorkingFile, CGWorkingStructure
from charmming.utils.structure.aux import saveQCWorkingFiles
from charmming.utils.cg_working_structure import addBondsToPDB

import os

class MinimizeTask(Task):
  sdsteps = models.PositiveIntegerField(default=0)
  abnrsteps = models.PositiveIntegerField(default=0)
  tolg = models.FloatField(null=True)
  usepbc = models.CharField(max_length=1, null=True)
  useqmmm = models.CharField(max_length=1, null=True, default="n")
  modelType = models.CharField(max_length=30, null=True, default=None)
  qmmmsel = models.CharField(max_length=250, null=True) # This is obsolete.

  def finish(self):
    """test if the job suceeded, create entries for output"""

    loc = self.workstruct.structure.location
    bnm = self.workstruct.identifier
    basepath = loc + '/' + bnm + "-" + self.action
    # There's always an input file, so create a WorkingFile
    # for it.

    path = basepath + ".inp"
    wfinp = WorkingFile()
    try:
      WorkingFile.objects.get(task=self, path=path)
    except:
      wfinp.task = self
      wfinp.path = path
      wfinp.canonPath = wfinp.path
      wfinp.type = 'inp'
      wfinp.description = 'minimization script input'
      wfinp.save()

    # Check if an output file was created and if so create
    # a WorkingFile for it.
    try:
      os.stat(basepath + '.out')
    except:
      self.status = 'F'
      return

    path = basepath + ".out"
    wfout = WorkingFile()
    try:
      WorkingFile.objects.get(task=self, path=path)
    except:
      wfout.task = self
      wfout.path = path
      wfout.canonPath = wfout.path
      wfout.type = 'out'
      wfout.description = 'minimization script output'
      wfout.save()

    if self.status == 'F':
      return

    # check and make sure that the output PSF/CRD were
    # created
    try:
      os.stat(basepath + '.crd')
    except:
      self.status = 'F'
      self.save()
      return

    path = basepath + ".crd"
    # create Working files for PDB, CRD, and PSF.
    wf = WorkingFile()
    try:
      WorkingFile.objects.get(task=self, path=path)
    except:
      wf.task = self
      wf.path = loc + '/' + bnm + '-minimization.crd'
      wf.canonPath = wf.path
      wf.type = 'crd'
      wf.description = 'minimized structure'
      wf.pdbkey = 'mini_' + self.workstruct.identifier
      wf.save()
      self.workstruct.addCRDToPickle(wf.path, 'mini_' + self.workstruct.identifier)

    path = basepath + ".psf"
    wfpsf = WorkingFile()
    inp_file = path
    try:
      WorkingFile.objects.get(task=self, path=path)
    except:
      wfpsf.task = self
      wfpsf.path = loc + '/' + bnm + '-minimization.psf'
      wfpsf.canonPath = wfpsf.path
      wfpsf.type = 'psf'
      wfpsf.description = 'minimized structure'
      wfpsf.save()

    path = basepath + ".pdb"
    out_file = path
    try:
      WorkingFile.objects.get(task=self, path=path)
    except:
      wfpdb = WorkingFile()
      wfpdb.task = self
      wfpdb.path = loc + '/' + bnm + '-minimization.pdb'
      wfpdb.canonPath = wfpdb.path
      wfpdb.type = 'pdb'
      wfpdb.description = 'minimized structure'
      wfpdb.save()
    # Generic QChem inp/out code
    saveQCWorkingFiles(self, basepath)
    # Generic coarse-grain code goes here.
    cgws = False
    try:
      cgws = CGWorkingStructure.objects.get(workingstructure_ptr=self.workstruct)
    except CGWorkingStructure.MultipleObjectsReturned: # Uh oh. This MAY be alright if AA/CG...
      self.status = "F"
      return
    except: # Catch everything else
      pass

    if cgws:
      addBondsToPDB(inp_file, out_file) # Sometimes the DB is too slow to catch up and returns blank.
    self.status = 'C'
    # self.createStatistics() #This only fails when the task succeeds, so we shouldn't worry.
    return
