from django.db import models

class QSARUnit(models.Model):
  unit_short_name = models.CharField(max_length=10)
  unit_name = models.CharField(max_length=100)
