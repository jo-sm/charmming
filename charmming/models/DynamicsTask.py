from django.db import models
from charmming.models import RexParams, WorkingFile, Task, CGWorkingStructure
from charmming.utils.cg_working_structure import addBondsToPDB

import re
import os

class DynamicsTask(Task):
  nstep = models.PositiveIntegerField(default=1000)
  make_movie = models.BooleanField(default=False)
  movie_status = models.CharField(max_length=250, null=True)
  replica_exchange = models.ForeignKey(RexParams, null=True)
  scpism = models.BooleanField(default=False)
  usepbc = models.BooleanField(default=False)

  # pre: Requires a Structure object
  # Combines all the smaller PDBs make in the above method into one large PDB that
  # jmol understands
  # type is md,ld,or sgld
  def combinePDBsForMovie(self, inp_file, cgws):
    ter = re.compile('TER')
    remark = re.compile('REMARK')

    # movie_handle will be the final movie created
    # frame movie is the PDBs which each time step sepearated into a new PDB
    movie_handle = open(self.workstruct.structure.location + '/' + self.workstruct.identifier + "-" + self.action + '-movie.pdb', 'a')
    for i in range(1, 11):
      frame_fname = self.workstruct.structure.location + '/' + self.workstruct.identifier + "-" + self.action + "-movie" + str(i) + ".pdb"
      if inp_file is not False:
        frame_status = addBondsToPDB(inp_file, frame_fname)
        if frame_status is not True:
          self.movie_status = 'Failed'
          self.save()
          return 'Failed'
      frame_handle = open(frame_fname, 'r')
      movie_handle.write('MODEL ' + str(i) + "\n")
      for line in frame_handle:
        if not remark.search(line) and not ter.search(line):
          movie_handle.write(line)
      movie_handle.write('ENDMDL\n')
    movie_handle.close()

    self.movie_status = 'Done'
    self.save()
    return "Done"

  def finish(self):
    """test if the job suceeded, create entries for output"""

    loc = self.workstruct.structure.location
    bnm = self.workstruct.identifier

    # There's always an input file, so create a WorkingFile
    # for it.
    basepath = loc + "/" + bnm + "-" + self.action

    path = basepath + ".inp"
    wfinp = WorkingFile()
    try:
      WorkingFile.objects.get(task=self, path=path)
    except:
      wfinp.task = self
      wfinp.path = path
      wfinp.canonPath = wfinp.path
      wfinp.type = 'inp'
      wfinp.description = self.action.upper() + ' input script'
      wfinp.save()

    # Check if an output file was created and if so create
    # a WorkingFile for it.
    path = basepath + ".out"
    try:
      os.stat(path)
    except:
      self.status = 'F'
      self.save()
      return

    wfout = WorkingFile()
    try:
      WorkingFile.objects.get(task=self, path=path)
    except:
      wfout.task = self
      wfout.path = path
      wfout.canonPath = wfout.path
      wfout.type = 'out'
      wfout.description = 'output from ' + self.action.upper()
      wfout.save()

    # check if the final coordinates were created, if so then
    # we can also add the PSF, PDB, trajectory, and restart files
    path = basepath + ".crd"
    try:
      os.stat(path)
    except:
      self.status = 'F'
      self.save()
      return

    wfcrd = WorkingFile()
    try:
      WorkingFile.objects.get(task=self, path=path)
    except:
      wfcrd.task = self
      wfcrd.path = path
      wfcrd.canonPath = wfcrd.path
      wfcrd.type = 'crd'
      wfcrd.description = 'coordinates from ' + self.action.upper()
      wfcrd.save()
      self.workstruct.addCRDToPickle(wfcrd.path, self.action + '_' + self.workstruct.identifier)

    wfpsf = WorkingFile()
    path = basepath + ".psf"
    inp_file = path
    try:
      WorkingFile.objects.get(task=self, path=path)
    except:
      wfpsf.task = self
      wfpsf.path = path
      wfpsf.canonPath = wfpsf.path
      wfpsf.type = 'psf'
      wfpsf.description = 'PSF from ' + self.action.upper()
      wfpsf.save()

    wfpdb = WorkingFile()
    path = basepath + ".pdb"
    out_file = path
    try:
      WorkingFile.objects.get(task=self, path=path)
    except:
      wfpdb.task = self
      wfpdb.path = path
      wfpdb.canonPath = wfpdb.path
      wfpdb.type = 'pdb'
      wfpdb.description = 'PDB coordinates from ' + self.action.upper()
      wfpdb.save()
    # Generic coarse-grain code goes here.
    cgws = False
    try:
      cgws = CGWorkingStructure.objects.get(workingstructure_ptr=self.workstruct.id)
    except CGWorkingStructure.MultipleObjectsReturned: # Uh oh. This MAY be alright if AA/CG...
      self.status = "F"
      return
    except Exception as ex: # Catch everything else
      pass
    if cgws is not False:
      addBondsToPDB(inp_file, out_file)
    if self.make_movie:
      # go to hollywood
      if cgws:
        result = self.combinePDBsForMovie(inp_file, cgws)
      else:
        result = self.combinePDBsForMovie(False, False)
      if result != "Done":
        self.status = "F"
        return
      wfmoviePDB = WorkingFile()
      path = basepath + "-movie.pdb"
      try:
        WorkingFile.objects.get(task=self, path=path)
      except:
        wfmoviePDB.task = self
        wfmoviePDB.path = path
        wfmoviePDB.canonPath = wfmoviePDB.path
        wfmoviePDB.type = 'pdb'
        wfmoviePDB.description = 'Movie PDB for ' + self.action.upper()
        wfmoviePDB.save()
