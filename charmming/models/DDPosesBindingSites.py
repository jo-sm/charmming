from django.db import models
from django.contrib.auth.models import User

from charmming.models import DDPose, DDBindingSite

class DDPosesBindingSites(models.Model):
  owner = models.ForeignKey(User)
  pose = models.ForeignKey(DDPose)
  binding_site = models.ForeignKey(DDBindingSite)
