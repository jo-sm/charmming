from django.db import models
from django.contrib.auth.models import User

from charmming.models import DDSource

class DDLigand(models.Model):
  owner = models.ForeignKey(User)
  ligand_owner_index = models.PositiveIntegerField(default=0)
  ligand_name = models.CharField(max_length=200)
  description = models.CharField(max_length=250, null=True, blank=True)
  source = models.ForeignKey(DDSource)
