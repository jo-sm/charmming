from django.db import models

from charmming.models import DDApplication

class DDJobType(models.Model):
  job_type_name = models.CharField(max_length=200)
  description = models.CharField(max_length=250, null=True, blank=True)
  application = models.ForeignKey(DDApplication, null=True, blank=True)
