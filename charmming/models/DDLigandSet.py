from django.db import models
from django.contrib.auth.models import User

class DDLigandSet(models.Model):
  owner = models.ForeignKey(User)
  ligand_set_name = models.CharField(max_length=200)
  description = models.CharField(max_length=250, null=True, blank=True)
  public = models.CharField(max_length=1)
