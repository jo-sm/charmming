from django.db import models
from django.contrib.auth.models import User

class DDFile(models.Model):
  owner = models.ForeignKey(User)
  file_name = models.CharField(max_length=100)
  file_location = models.CharField(max_length=300, null=True, blank=True)
  description = models.CharField(max_length=250, null=True, blank=True)
