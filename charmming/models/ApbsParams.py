from django.db import models

class ApbsParams(models.Model):
  gdim_x = models.PositiveIntegerField(default=0)
  gdim_y = models.PositiveIntegerField(default=0)
  gdim_z = models.PositiveIntegerField(default=0)
  npoint_x = models.PositiveIntegerField(default=0)
  npoint_y = models.PositiveIntegerField(default=0)
  npoint_z = models.PositiveIntegerField(default=0)
