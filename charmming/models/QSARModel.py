from django.db import models
from django.contrib.auth.models import User

from charmming.models import QSARModelType

class QSARModel(models.Model):
  model_owner = models.ForeignKey(User)
  model_owner_index = models.PositiveIntegerField(default=0)
  model_name = models.CharField(max_length=200, null=True, blank=True)
  model_description = models.CharField(max_length=250, null=True, blank=True)
  model_type = models.ForeignKey(QSARModelType, null=True, blank=True)
  model_file = models.CharField(max_length=250, null=True, blank=True)

  def getNewModelOwnerIndex(self, user):

    try:
      last_model = QSARModel.objects.filter(model_owner=user.id).order_by('-id')[0]
      next_model_owner_index = int(last_model.model_owner_index) + 1

    except:
      next_model_owner_index = 1

    return next_model_owner_index
