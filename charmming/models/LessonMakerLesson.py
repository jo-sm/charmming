from django.db import models

from charmming.models import Structure

class LessonMakerLesson(models.Model):
  # data for lessons (should not be overridden by subclasses)
  # specifying both user and Structure is redundant (since the Structure references the user),
  # but makes it easier to look up all lessons being done by a particular user.
  # presumably you could calculate this out from the count of Steps it currently has, but it doesn't allow for deletio
  finished = models.BooleanField(default=False, null=False)  # update this field at the end
  structure = models.ForeignKey(Structure, null=False)  # set this at the start..
  filename = models.CharField(max_length=25, null=True)  # this keeps track of the filename of the thing the user uploaded
  name = models.CharField(max_length=25, null=True)  # this keeps track of the name, it's for the create script
  intro_text = models.TextField(null=True)  # this tracks the intro text. TextField cause this can get huge.
  title = models.CharField(max_length=75, null=True)  # this tracks the title. We shouldn't need more than 75 chars, and should do input sanitization on entry.
  # we need this to compare with the file later on...
