from django.db import models
from charmming.models import APIUser

class APIJob(models.Model):
  jobType = models.PositiveIntegerField(default=0)
  user = models.ForeignKey(APIUser)
  timestamp = models.DateField(auto_now_add=True)
  directory = models.CharField(max_length=30, null=False)
  rtfList = models.CharField(max_length=1000, null=True)
  prmList = models.CharField(max_length=1000, null=True)
  strList = models.CharField(max_length=1000, null=True)
  segList = models.CharField(max_length=1000, null=True)

  def parse_energy(self, fp):
    foundEne = False
    for line in fp:
      line = line.strip()

      if line.startswith('ENER>'):
        foundEne = True
        try:
          eneval = float(line.split()[2])
        except:
          return -9999999.99

    if not foundEne:
      return -9999999.99
    return eneval

  def parse_inte(self, fp):
    foundEne = False
    for line in fp:
      line = line.strip()

      if line.startswith('INTE>'):
        foundEne = True
        try:
          eneval = float(line.split()[2])
        except:
          return -9999999.99

    if not foundEne:
      return -9999999.99
    return eneval
