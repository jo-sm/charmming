from django.db import models
from charmming.models import Structure, Segment

class Patch(models.Model):
  name = models.CharField(max_length=10)
  segment_residue = models.CharField(max_length=100)
  structure = models.ForeignKey(Structure)

  # Patches can cross multiple segments. If this field is set, the patch
  # only applies to a particular segment (i.e. it is a protonation patch
  # that should be handled at segment build time rather than append time)
  segment = models.ForeignKey(Segment, null=True)
