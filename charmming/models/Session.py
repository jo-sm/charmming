from django.db import models

from charmming.models import User

class Session(models.Model):
  user = models.ForeignKey(User, null=False)
  ip = models.CharField(max_length=64, null=False)
  user_agent = models.CharField(max_length=256, null=False)
  created_on = models.DateTimeField(null=False)
  accessed_on = models.DateTimeField(null=False)
