from django.db import models
from django.contrib.auth.models import User

class SchedulerJob(models.Model):
  sched_id = models.CharField(max_length=64, null=True)
  state = models.PositiveIntegerField(default=0)
  started = models.DateTimeField(null=True)
  ended = models.DateTimeField(null=True)
  queued = models.DateTimeField(null=True)
  userid = models.ForeignKey(User)
  batchid = models.CharField(max_length=64)
  exe = models.TextField()
  script = models.TextField()
