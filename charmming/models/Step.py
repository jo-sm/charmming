from django.db import models

from charmming.models import LessonMakerLesson, Task

class Step(models.Model):
  lesson = models.ForeignKey(LessonMakerLesson)
  step = models.DecimalField(default=3, decimal_places=1, max_digits=3)
  task = models.ForeignKey(Task, null=True)  # Task will be interpreted depending on the type...
  # We will have to update Task stuff for the job sched rebuild
  type = models.CharField(max_length=20, null=False)  # no default value cause this will get modified immediately
  running_text = models.TextField(null=True)  # text to display while it's running
  done_text = models.TextField(null=True)  # text to display when it's done, both of these are longwinded.
