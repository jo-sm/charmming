from django.db import models
from django.contrib.auth.models import User

class DDScoringCriteria(models.Model):
  owner = models.ForeignKey(User)
  scoring_criteria_name = models.CharField(max_length=200)
  description = models.CharField(max_length=250, null=True, blank=True)
