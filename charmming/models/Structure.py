from django.db import models
from django.conf import settings
from django.template import Context
from django.template.loader import get_template
from django.contrib.postgres.fields import ArrayField

from charmming import charmming_config
from charmming.helpers import output
from charmming.models import User
from charmming.utils.pychm import pychm_pdb_to_json

from pychm3.io.pdb import PDB, get_model_from_crd

import datetime
import pickle
import os
import json
import shutil

class Structure(models.Model):
  owner = models.ForeignKey(User)
  name = models.CharField(max_length=100)
  last_modified_on = models.DateTimeField(default=datetime.datetime.now)
  created_on = models.DateTimeField(default=datetime.datetime.now)
  cg_model_type = models.CharField(max_length=10, null=True)
  parent = models.ForeignKey('self', null=True)
  path = models.CharField(max_length=100)
  file_id = models.CharField(max_length=64)
  stream_list = ArrayField(models.CharField(max_length=200), default=list)

  # This is only used when the model is ambiguous (if there are
  # multiple models in the PDB)
  model = models.CharField(max_length=20, default='model00')

  # If the Structure has been built
  # If this is set to 'failed', the Structure will not
  # attempt to build again unless it is set to 'not_built'
  build_status = models.CharField(choices=[
    ('not_built', 'Not Built'),
    ('building', 'Building'),
    ('built', 'Built'),
    ('failed', 'Failed')
  ], max_length=10, default='not_built')
  is_built = models.BooleanField(default=False)

  selected = models.CharField(max_length=1, default='n')
  lesson_type = models.CharField(max_length=50, null=True)
  lesson_id = models.PositiveIntegerField(default=0, null=True)

  natom = models.PositiveIntegerField(default=0)
  original_name = models.CharField(max_length=100, null=True, default=None)
  pickle = models.CharField(max_length=100)

  pdb_disul = models.CharField(max_length=1000)
  location = models.CharField(max_length=200)
  title = models.CharField(max_length=250)
  author = models.CharField(max_length=250)
  journal = models.CharField(max_length=250)
  pub_date = models.DateTimeField(default=datetime.datetime.now)
  domains = models.CharField(max_length=250, default='')

  lessonmaker_active = models.BooleanField(default=False)
  # TODO: THIS IS A NEW FIELD ADD IT IN PRODUCTION DATABASE
  # lessonmaker_active is required for lessonmaker stuff
  # adding more fields isn't always good, but our issue here is that
  # whenever we try to run a job, we do a query on structure to check if we have anything
  # to work with, and then work. Thus, adding lessonmaker_active into here
  # is essentially like getting a "global variable" whenever you run something
  # that you can then check to see if we are currently recording stuff

  def toJSON(self):
    return {
      'id': self.id,
      'owner': self.owner,
      'name': self.name,
      'segments': self.segment_set.all()
    }

  def get_pychm(self, **kwargs):
    model = kwargs.get('model', 0)

    segment = self.get_file()

    models = PDB(segment)

    return models[model]

  def save(self, *args, **kwargs):
    # Set last modified time
    self.last_modified_on = datetime.datetime.now()

    # Create directory if it doesn't exist
    if not os.path.exists(self.dir()):
      os.mkdir(self.dir())

    return super().save(*args, **kwargs)

  def delete(self, *args, **kwargs):
    base_dir = settings.PROJECT_ROOT
    files_dir = os.path.join(base_dir, "files")
    file_dir = os.path.join(files_dir, "{}".format(self.id))

    try:
      shutil.rmtree(file_dir)
    except FileNotFoundError:
      # Log a warning that the structure directory didn't exist
      pass

    return super().delete(*args, **kwargs)

  def tasks(self):
    """
    Returns all tasks, in descending order
    """
    return self.task_set.order_by('-id').all()

  def create_child(self):
    if self.parent:
      return self.parent.create_child()

    child = Structure()
    child.owner = self.owner
    child.name = '{} Child'.format(self.name)
    child.parent = self
    child.save()

    # Copy files from parent to child dir
    base_dir = settings.PROJECT_ROOT
    files_dir = os.path.join(base_dir, "files")
    file_dir = os.path.join(files_dir, "{}".format(self.id))
    child_file_dir = os.path.join(files_dir, "{}".format(child.id))

    # We don't know what the ID of the child is until it's saved, so
    # we have to save twice
    child.path = child_file_dir
    child.save()

    try:
      shutil.copytree(file_dir, child_file_dir)
    except FileExistsError:
      # The directory exists
      shutil.rmtree(child_file_dir)
      shutil.copytree(file_dir, child_file_dir)

    # Rename the base PDB of the child if it exists
    if self.path:
      # Path is only set when there is a PDB saved
      if not os.path.exists(os.path.join(child_file_dir, "{}.pdb".format(self.name))):
        # Log a warning/error because this should not happen
        pass
      else:
        shutil.move(os.path.join(child_file_dir, "{}.pdb".format(self.name)), os.path.join(child_file_dir, "{}.pdb".format(child.file_id)))

    return child

  def dir(self, **kwargs):
    base_dir = settings.PROJECT_ROOT
    files_dir = os.path.join(base_dir, "files")
    file_dir = os.path.join(files_dir, "{}".format(self.id))

    if kwargs.get('internal'):
      file_dir = os.path.join(file_dir, '.charmming')

    return file_dir

  def get_pychm_pdb(self, **kwargs):
    pdb = PDB(self.get_file("{}.pdb".format(self.id)))

    return pdb

  def save_pdb(self, file, **kwargs):
    """
    Saves a PDB (or CRD) and sets the PDB metadata on the Structure.
    """

    if kwargs.get('crd'):
      mol = get_model_from_crd(file)
      pdb_text = mol.to_str(format='pdborg')
    else:
      pdb_text = file

    self.save_structure_file(pdb_text)

    # Save the PDB JSON for faster but incomplete retrieval later
    pdb = PDB(pdb_text)
    # pdb_json = pychm_pdb_to_json(pdb)

    # self.save_file(pdb_json, 'json', internal=True)

    self.name = "{} ({})".format(pdb.code.upper(), "PDB")
    self.save()

    # Save the Segments for this Structure
    # This clears any previously saved Segments that
    # are associated to this Structure

    self.segment_set.all().delete()

    for model in pdb.models:
      for _segment in model.segments:
        # We should write out the segment to disk as well

        first_residue = _segment.residues[0]

        segment = self.segment_set.create()
        segment.name = _segment.id
        segment.type = _segment.type
        segment.chain = _segment.chain_id
        segment.model = model.name
        segment.save(initial=True, first_residue_name=first_residue.name)

        # We don't reorder so that the UI can properly work
        segment_text = _segment.to_str(format='pdborg', reorder=False)

        self.save_structure_file(segment_text, segment=segment)

    return True

  def get_filepath(self, **kwargs):
    base_dir = settings.PROJECT_ROOT
    files_dir = os.path.join(base_dir, "files")
    file_dir = os.path.join(files_dir, "{}".format(self.id))
    is_internal = kwargs.get('internal')

    if is_internal:
      file_dir = os.path.join(file_dir, ".charmming")

    segment = kwargs.get('segment')
    filename = kwargs.get('filename')

    if segment:
      prefix = "{}-{}-{}".format(self.id, segment.model, segment.name)

      if filename:
        filename = "{}-{}".format(prefix, filename)
      else:
        filename = "{}.pdb".format(prefix)
    else:
      # By default, we are getting a file for this Structure
      prefix = "{}".format(self.id)

      if filename:
        filename = "{}-{}".format(prefix, filename)
      else:
        filename = "{}.pdb".format(prefix)

    file_path = os.path.join(file_dir, filename)

    return file_path

  def get_file(self, **kwargs):
    """
    Gets a file associated with this Structure by kind.
    Returns None if not found.
    """

    file_path = self.get_filepath(**kwargs)

    try:
      file_io = open(file_path, 'r')
      file = file_io.read()
      file_io.close()

      return file
    except FileNotFoundError:
      return None

  def save_structure_file(self, contents, **kwargs):
    segment = kwargs.get('segment')

    if segment:
      filename = "{}-{}-{}.pdb".format(self.id, segment.model, segment.name)
    else:
      filename = "{}.pdb".format(self.id)

    return self.save_file(filename, contents, no_mangle=True, **kwargs)

  def save_file(self, filename, contents, **kwargs):
    # Create directory if it doesn't exist
    base_dir = settings.PROJECT_ROOT
    files_dir = os.path.join(base_dir, "files")
    file_dir = os.path.join(files_dir, "{}".format(self.id))

    is_internal = kwargs.get('internal')
    segment = kwargs.get('segment')
    no_mangle = kwargs.get('no_mangle')

    if is_internal:
      file_dir = os.path.join(file_dir, ".charmming")

    # Generally, we save the file with a prefix to the given filename,
    # but in certain cases such as saving the original structure we do
    # not want to "mangle" the filename and want to use the filename
    # given.
    if not no_mangle:
      if segment:
        filename = "{}-{}-{}-{}".format(self.id, segment.model, segment.name, filename)
      else:
        filename = "{}-{}".format(self.id, filename)

    file_path = os.path.join(file_dir, filename)

    try:
      os.mkdir(file_dir)
    except FileExistsError:
      # The directory exists
      pass

    file_io = open(file_path, 'w')
    file_io.write(contents)
    file_io.close()

    # Returns the file directory, filename, and extension
    return file_path

  # Returns a list of files not specifically associated with the structure
  def getNonStructureFiles(self):
    file_list = []
    file_list.append("/solvation/water.crd")
    file_list.append("/scripts/savegv.py")
    file_list.append("/scripts/savechrg.py")
    file_list.append("/scripts/calcewald.pl")
    return file_list

  # CHARMMing GUTS-ified methods go here. Eventually, all of the methods above this point #
  # should be brought below it and cleaned up, or eliminated.                             #

  # GUTS-ified disulfide list builder
  # Note: this returns a list of tuples
  def getDisulfideList(self):
    if not self.pdb_disul:
      return None

    # we need to translate between old and new numbers how
    pfp = open(self.pickle, 'r')
    pdb = pickle.load(pfp)
    pfp.close()
    mdl = next(pdb.iter_models())  # Usually this works out. Watch for multi-model systems.

    dsl = self.pdb_disul.split()
    n = 0
    rarr = []

    while n < len(dsl):
      resn1 = dsl[n + 1]
      segn1 = dsl[n + 2] + '-pro' # protein segments have disulfides
      resi1 = dsl[n + 3]
      resn2 = dsl[n + 4]
      segn2 = dsl[n + 5] + '-pro'
      resi2 = dsl[n + 6]
      n += 7

      # make sure we have the correct residue number -- hack to just
      # use model 0
      for segment in mdl.iter_seg():
        if segment.segid == segn1:
          for atom in segment:
            if atom.resid0 == int(resi1):
              resi1 = str(atom.resid)

        if segment.segid == segn2:
          for atom in segment:
            if atom.resid0 == int(resi2):
              resi2 = str(atom.resid)

      rarr.append((segn1, resn1, resi1, segn2, resn2, resi2))
    return rarr

  # takes the information from the Remark statement of
  # a PDB and determines the title, jrnl, and author
  def getHeader(self, pdbHeader):
    for line in pdbHeader:
      if line.startswith('title'):
        line = line.replace('title', '')
        self.title += line.strip()
      elif line.startswith('author'):
        line = line.replace('author', '')
        self.author += line.strip()
      elif line.startswith('jrnl') or line.startswith('ref') or line.startswith('refn'):
        line = line.replace('jrnl', '')
        line = line.replace('ref', '')
        line = line.replace('refn', '')
        self.journal += line.strip()
      elif line.startswith('ssbond'):
        # process disulfide bridges
        line = line.replace('ssbond', '')
        line = ' '.join(line.split()[:7]) # grab the first seven elements of the line

        self.pdb_disul += ' %s' % line.strip()

    if self.title:
      if len(self.title) > 249:
        self.title = self.title[0:248]
      else:
        self.title = "No information found"

    if self.author:
      if len(self.author) > 249:
        self.author = self.author[0:248]
    else:
      self.author = "No information found"

    self.journal = self.journal.strip()

    if self.journal:
      if len(self.journal) > 249:
        self.journal = self.journal[0:248]
    else:
      self.journal = "No information found"
      if self.pdb_disul:
        if len(self.pdb_disul) > 999:
          raise AssertionError('Too many disulfides in PDB')

    self.save()

  def putSeqOnDisk(self, sequence, path):
    """Special method used with custom sequences; writes out a PDB from a sequence.
    """

    # ToDo: check and make sure that the residues in the sequence are valid
    sequence = sequence.strip()
    x = sequence.split()
    seqrdup = ' '.join(x)

    if len(x) > 500:
      raise AssertionError('Custom sequence is too long!')

    td = {}
    td['nres'] = len(x)
    td['sequence'] = seqrdup
    td['topology'] = '%s/toppar/%s' % (charmming_config.data_home, charmming_config.default_pro_top)
    td['parameter'] = '%s/toppar/%s' % (charmming_config.data_home, charmming_config.default_pro_prm)
    td['outname'] = path
    td['name'] = self.owner.username

    # this needs to be run directly through CHARMM
    t = get_template('%s/mytemplates/input_scripts/seqtopdb.inp' % charmming_config.charmming_root)
    charmm_inp = output.tidyInp(t.render(Context(td)))

    os.chdir(self.location)
    fp = open('seq2pdb.inp', 'w')
    fp.write(charmm_inp)
    fp.close()

    os.system("LD_LIBRARY_PATH=%s %s < seq2pdb.inp > seq2pdb.out" % (charmming_config.lib_path, charmming_config.charmm_exe))
