from django.db import models

class DDAminoAcid(models.Model):
  amino_acid_name = models.CharField(max_length=200)
  three_letter_code = models.CharField(max_length=3, null=True, blank=True)
  one_letter_code = models.CharField(max_length=1, null=True, blank=True)
  description = models.CharField(max_length=250, null=True, blank=True)
