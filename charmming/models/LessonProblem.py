from django.db import models

class LessonProblem(models.Model):
  lesson_type = models.CharField(max_length=50)
  lesson_id = models.PositiveIntegerField(default=0)
  errorstep = models.PositiveIntegerField(default=0)
  severity = models.PositiveIntegerField(default=0)
  description = models.CharField(max_length=500)
