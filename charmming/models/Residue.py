from django.db import models

class Residue(models.Model):

  residue_name = models.CharField(max_length=10, null=True)
  residue_desc = models.CharField(max_length=500, null=True)
