from django.db import models

class QSARModelType(models.Model):
  # DROP TABLE IF EXISTS `qsar_model_types`;
  # CREATE TABLE `qsar_model_types` (
  #   `id` int(11) NOT NULL AUTO_INCREMENT,
  #   `model_type_name` varchar(200) NOT NULL,
  #   `description` varchar(250) DEFAULT NULL,
  #   `type` varchar(200) NOT NULL,
  #   `is_categorical` tinyint(1) NOT NULL,
  #   PRIMARY KEY (`id`)
  # ) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
  # LOCK TABLES `qsar_model_types` WRITE;
  # INSERT INTO `qsar_model_types` VALUES (1,'Random Forest (QSAR) Regression','','ensemble.RandomForestRegressor',0),(2,'Random Forest (SAR) Categorization','','rf',1),(3,'SVM (SAR) Categorization','','svm.SVC',1),(4,'SVM (QSAR) Regression','','svm.SVR',0),(5,'Logit (SAR) Categorization','','linear_model.LogisticRegression',1),(6,'SGD (SAR) Categorization','','linear_model.SGDClassifier',1),(7,'Naive Bayes (SAR) Categorization','','GaussianNB',1),(8,'Gradient Boosting (SAR) Categorization','','ensemble.GradientBoostingClassifier',1),(10,'Decision Tree (SAR) Categorization','','tree.DecisionTreeClassifier',1),(11,'Gradient Boosting (QSAR) Regression','','ensemble.GradientBoostingRegressor',0),(12,'Decision Tree (QSAR) Regression','','tree.DecisionTreeRegressor',0),(14,'Ridge (QSAR) Regression','','linear_model.Ridge',0),(15,'Lasso (QSAR) Regression','','linear_model.Lasso',0),(16,'Elastic Net (QSAR) Regression','','linear_model.ElasticNet',0),(17,'SGD (QSAR) Regression','','linear_model.SGDRegressor',0);
  # UNLOCK TABLES;

  model_type_name = models.CharField(max_length=200)
  description = models.CharField(max_length=250, null=True, blank=True)
  type = models.CharField(max_length=200)
  is_categorical = models.BooleanField()
