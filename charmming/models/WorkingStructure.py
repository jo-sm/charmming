from django.db import models

from charmming.models import Structure, WorkingSegment, Lesson

from pychm3.io.pdb import get_model_from_crd

import os
import pickle

class WorkingStructure(models.Model):
  # The idea is that the WorkingStructure class will hold structures that
  # are ready to be run through minimization, dynamics, etc.
  locked = models.BooleanField(default=False)  # This keeps track of whether this WS' task statuses are currently being updated

  structure = models.ForeignKey(Structure)
  identifier = models.CharField(max_length=25, default='')

  selected = models.CharField(max_length=1, default='n')
  doblncharge = models.CharField(max_length=1, default='f')
  isBuilt = models.CharField(max_length=1, default='f')
  segments = models.ManyToManyField(WorkingSegment)

  modelName = models.CharField(max_length=100, default='model0')
  qmRegion = models.CharField(max_length=250, default='none')

  # final topologies and parameters (built using Frank's TOP/PAR stuff).
  finalTopology = models.CharField(max_length=50, null=True)
  finalParameter = models.CharField(max_length=50, null=True)

  # Points to NULL (default) or to local pickle file (if mutated struct)
  localpickle = models.CharField(max_length=500, null=True)

  # see if we have any toppar stream files
  topparStream = models.CharField(max_length=500, null=True)

  # lesson
  lesson = models.ForeignKey(Lesson, null=True)

  # extra setup stream files that are needed (useful for BLN model
  # and possibly elsewhere)
  extraStreams = models.CharField(max_length=120, null=True)

  @property
  def dimension(self):
    xmax = -9999.
    ymax = -9999.
    zmax = -9999.
    xmin = 9999.
    ymin = 9999.
    zmin = 9999.

    # get the names of all segments that are part of this working structure
    segnamelist = []
    for wseg in self.segments.all():
      segnamelist.append(wseg.name)
    if self.localpickle:
      pickle_file = open(self.localpickle, 'rb')
    else:
      pickle_file = open(self.structure.pickle, 'rb')
    pdb = pickle.load(pickle_file)
    pickle_file.close()

    mol = next(pdb.iter_models()) # potentially dangerous: assume we're dealing with model 0
    for seg in mol.iter_seg():
      if seg.segid in segnamelist:
        for atom in seg:
          x, y, z = atom.cart
          if x > xmax:
            xmax = x
          if y > ymax:
            ymax = y
          if z > zmax:
            zmax = z
          if x < xmin:
            xmin = x
          if y < ymin:
            ymin = y
          if z < zmin:
            zmin = z
    return((xmax - xmin, ymax - ymin, zmax - zmin))

  # Override because topparStream sucks
  def save(self, *args, **kwargs):
    self.locked = False
    super(WorkingStructure, self).save()  # This way it unlocks on save

  def lock(self):
    self.locked = True
    super(WorkingStructure, self).save()

  def unlock(self):
    self.locked = False
    super(WorkingStructure, self).save()

  def getTopparList(self):
    """
    Returns a list of all parameter files used by all the segments
    in this WorkingStructure.

    BTM 20120522 -- build one parameter file to rule them all, using
    Frank's code.

    BTM 20130213 -- building one param file to rule them all is no longer
    needed thanks to some tricks and flexible param stuff. Pulling it out.

    BTM 20141017 -- hack for uploaded PSFs without workingSegments.
    """

    # prm_list = []
    tlist = []
    plist = []

    orderseglist = list(self.segments.filter(type='pro'))
    orderseglist.extend(list(self.segments.filter(type__in=['rna', 'dna'])))
    orderseglist.extend(list(self.segments.filter(type='good')))
    orderseglist.extend(list(self.segments.filter(type='bad')))

    # These are special cases for CG models
    orderseglist.extend(list(self.segments.filter(type='go')))
    orderseglist.extend(list(self.segments.filter(type='bln')))

    if len(orderseglist) == 0:
      # Have to do something here for prebuilt segments... this
      # is a gory hack, but Toshiko needs it working quick.
      infp = open(self.structure.location + '/btopo.txt', 'r')
      for line in infp:
        tlist.append(line.strip())
      infp.close()
      infp = open(self.structure.location + '/bparm.txt', 'r')
      for line in infp:
        plist.append(line.strip())
      infp.close()

      return tlist, plist

    for segobj in orderseglist:
      if segobj.redox:
        continue
      # NB: tlist and plist used to be done as sets, but
      # it turns out that order DOES matter, hence the
      # hack-y ordered set thing here.
      if segobj.prm_list:
        for prm in segobj.prm_list.split(' '):
          if prm not in plist:
            plist.append(prm)
      if segobj.rtf_list:
        for rtf in segobj.rtf_list.split(' '):
          if rtf not in tlist:
            tlist.append(rtf)
    return tlist, plist

  def addCRDToPickle(self, fname, fkey):
    pfile = self.localpickle if self.localpickle else self.structure.pickle
    pickleFile = open(pfile, 'r+')
    pdb = pickle.load(pickleFile)
    pickleFile.close()

    # Create a new Mol object for this
    molobj = get_model_from_crd(fname)
    pdb[fkey] = molobj

    # kill off the old pickle and re-create
    os.unlink(self.structure.pickle)

    pickleFile = open(pfile, 'w')
    pickle.dump(pdb, pickleFile)
    pickleFile.close()

    self.save()

  # Updates the status of in progress operations
