from django.db import models
from django.conf import settings
from django.contrib.postgres.fields import JSONField

from charmming.models import Structure, DataPoint, BaseModel
from charmming.utils.coordinates import job_can_have_coordinates
from charmming.utils.job import get_job_class

from schedulerd.utils import states

import os

class Task(BaseModel):
  STATUS_CHOICES = [ (states[state], state) for state in states ]
  STATUS_CHOICES = [ ('INACTIVE', 'inactive') ] + STATUS_CHOICES

  # Structure this task uses
  structure = models.ForeignKey(Structure)

  # Current job status (may be incorrect)
  state = models.CharField(max_length=12, choices=STATUS_CHOICES, default='INACTIVE')

  # The job id from the scheduler
  job_id = models.PositiveIntegerField(default=0)

  # If it should be hidden from the UI
  hidden = models.BooleanField(default=False)

  # Program name, e.g. charmm, qchem
  program = models.CharField(max_length=50)

  # Job within program, e.g. energy, solvation
  job = models.CharField(max_length=50)

  # The task that contains coordinates that this task uses for its calculations
  # May be null if it's some base task, like building, that doesn't have coords
  # to use
  coordinates_task = models.ForeignKey('self', null=True)

  # Options used during this task
  options = JSONField(default={})

  def toJSON(self):
    return {
      'id': self.id,
      'status': self.state.lower(),
      'hidden': self.hidden,
      'program': self.program,
      'job': self.job,
      'structure': self.structure.id,
      'coordinatesTask': self.coordinates_task.id if self.coordinates_task else None
    }

  def save(self, *args, **kwargs):
    # Verify that the coordinates_task can contain coordinates
    if self.coordinates_task:
      coordinates_program = self.coordinates_task.program
      coordinates_job = self.coordinates_task.job

      if not job_can_have_coordinates(coordinates_program, coordinates_job):
        raise AssertionError('Given coordinates task cannot contain coordinates.')

    result = super().save(*args, **kwargs)

    # Create directory if it doesn't exist
    # This has to happen after the super().save()
    # since the id won't be populated until initial
    # save
    if not os.path.exists(self.dir()):
      os.mkdir(self.dir())

    return result

  def get_coordinates(self):
    job_class = get_job_class(self.program, self.job)
    coordinate_types = job_class.coordinate_types

    task_dir = self.dir()
    paths = {}

    for coord_type in coordinate_types:
      paths[coord_type] = os.path.join(task_dir, "{}.{}".format(self.job, coord_type))

    return {
      'program': self.program,
      'job': self.job,
      'files': paths
    }

  @property
  def active(self):
    return self.status == 'R'

  @property
  def finished(self):
    return self.status == 'C'

  def add_script(self, name, text, program, *args):
    script_path = self.save_file(name, text)

    script = self.script_set.create()
    script.path = script_path
    script.executable = program
    script.save()

    return script

  def save_file(self, filename, contents):
    file_dir = self.dir()

    try:
      os.mkdir(file_dir)
    except FileExistsError:
      # The directory exists
      pass

    file_path = os.path.join(file_dir, filename)

    file_io = open(file_path, 'w')
    file_io.write(contents)
    file_io.close()

    return file_path

  def dir(self):
    base_dir = settings.PROJECT_ROOT
    files_dir = os.path.join(base_dir, "tasks")
    file_dir = os.path.join(files_dir, "{}".format(self.id))

    return file_dir

  def get_scripts(self):
    return self.script_set.order_by('id', 'order')

  # @property
  # def scriptList(self):
  #   if self.scripts.startswith(','):
  #     self.scripts = self.scripts[1:]
  #     self.save()
  #   return self.scripts.split(',')

  def kill(self):
    si = schedInterface()

    if self.jobID > 0:
      si.doJobKill(self.workstruct.structure.owner.id, self.jobID)
      self.status = 'K'

  def query(self):
    si = schedInterface()
    if self.jobID > 0:
      sstring = si.checkStatus(self.jobID).split()[4]
      if sstring == 'submitted' or sstring == 'queued':
        self.status = 'Q'
      elif sstring == 'running':
        self.status = 'R'
      elif sstring == 'complete':
        self.status = 'C'
      elif sstring == 'failed':
        self.status = 'F'
      else:
        raise AssertionError('Unknown status ' + sstring)
      self.save()
      return sstring
    else:
      return 'unknown'

  def createStatistics(self):
    """
    Creates a statistics.models.DataPoint for the sake of keeping
    track of statistics for the admin panel
    """
    datapoint = DataPoint()
    datapoint.task_id = int(self.id)
    datapoint.task_action = str(self.action)  # Make these into primitives so it doesn't try to foreign key them, just in case
    datapoint.user = str(self.workstruct.structure.owner.username)
    datapoint.structure_name = str(self.workstruct.structure.name)
    datapoint.success = True if self.status == 'C' else False
    datapoint.struct_id = self.workstruct.structure.id
    datapoint.save()

  def finish(self):
    """
    This is a pure virtual function. I expect it to be overridden in
    the subclasses of Task. In particular, it needs to decide whether
    the action succeeded or failed.
    """
    pass

  # def setup(self, ws):
  #   models.Model.__init__(self) # call base class init

  #   self.status = 'I'
  #   self.jobID = 0
  #   self.finished = 'n'
  #   # self.scripts = ''
  #   self.workstruct = ws
