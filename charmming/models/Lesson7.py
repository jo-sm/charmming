# lesson 7
# Docking

from django.db import models
from django.contrib.auth.models import User

from charmming import charmming_config
from charmming.models import Structure
from charmming.models import LessonProblem
from charmming.utils.lesson import diffPDBs

import os
import math

class Lesson7(models.Model):
  user = models.ForeignKey(User)
  nSteps = models.PositiveIntegerField(default=5)
  curStep = models.DecimalField(default=0, decimal_places=1, max_digits=3)

  def onFileUpload(self):
    try:
      LessonProblem.objects.get(lesson_type='lesson7', lesson_id=self.id).delete()
    except:
      pass

    file = Structure.objects.get(selected='y', owner=self.user, lesson_id=self.id)
    # this should never return more than 1...if it does we've got some serious DB trouble
    try:
      filename1 = '%s/mytemplates/lessons/lesson7/1mvc.pdb' % charmming_config.charmming_root
      os.stat(filename1)
      filename2 = file.location + '/1mvc.pdb'
      os.stat(filename2)
    except:
      lessonprob = LessonProblem(lesson_type='lesson7', lesson_id=self.id, errorstep=1, severity=9, description='The PDB you submitted did not upload properly, or the wrong structure was uploaded. Go to the <a href="/fileupload/">Submit Structure</a> page and check to make sure the PDB.org ID was valid.')
      lessonprob.save()
      return False

    if not diffPDBs(file, filename1, filename2):
      lessonprob = LessonProblem(lesson_type='lesson7', lesson_id=self.id, errorstep=1, severity=9, description='The PDB you uploaded was not the correct PDB. Go to the <a href="/fileupload/">Submit Structure</a> page and make sure the PDB.org ID was "4DFR".')
      lessonprob.save()
      return False
    self.curStep = '1'
    self.save()
    return True

  def onBuildStructureSubmit(self, postdata):
    try:
      LessonProblem.objects.get(lesson_type='lesson7', lesson_id=self.id).delete()
    except:
      pass
    badsegs = ['select_b-pro', 'select_b-good']
    goodsegs = ['select_a-good', 'select_a-bad', 'select_a-pro']
    correct_string = 'Please return to the <a href="/buildstruct/">"Build/Select Working Structure"</a> page and create a new one taking this into account.'
    for seg in badsegs:
      if seg in postdata and postdata[seg] == "y":
        lessonprob = LessonProblem(lesson_type='lesson7', lesson_id=self.id, errorstep=2, severity=9, description='You selected segment %s, which should not be part of your working structure. %s' % (seg.replace("select_", ""), correct_string))
        lessonprob.save()
        return False
    for seg in goodsegs:
      if not(seg in postdata) or postdata[seg] == 'n':
        lessonprob = LessonProblem(lesson_type='lesson7', lesson_id=self.id, errorstep=2, severity=9, description='You did not select segment %s, which should be part of your working structure. %s' % (seg.replace("select_", ""), correct_string))
        lessonprob.save()
        return False
    self.curStep = '2'
    self.save()
    return True

  def onBuildStructureDone(self, task):
    return True

  def onMinimizeSubmit(self, mp, filename):
    try:
      LessonProblem.objects.filter(lesson_type='lesson7', lesson_id=self.id).delete()
    except:
      pass

    if float(self.curStep) >= float(3.0) and float(self.curStep) < float(3.1): # float casting troubles...
      parent_task_action = mp.parent.action
      correct_string = 'Please return to the <a href="/minimize/">minimization</a> page and change this parameter to the correct value.'
      if parent_task_action != 'build':
        lessonprob = LessonProblem(lesson_type='lesson7', lesson_id=self.id, errorstep=4.0, severity=2, description='Please run minimization on the coordinates from build.')
        lessonprob.save()
        return False
      if mp.sdsteps != 100:
        lessonprob = LessonProblem(lesson_type='lesson7', lesson_id=self.id, errorstep=4.0, severity=2, description='SD steps were not set to 100. %s' % correct_string)
        lessonprob.save()
        return False
      if mp.abnrsteps != 1000:
        lessonprob = LessonProblem(lesson_type='lesson7', lesson_id=self.id, errorstep=4.0, severity=2, description='ABNR steps were not set to 1000. %s' % correct_string)
        lessonprob.save()
        return False
      if float(mp.tolg) != float(0.05):
        lessonprob = LessonProblem(lesson_type='lesson7', lesson_id=self.id, errorstep=4.0, severity=2, description='TOLG was not set to 0.05. %s' % correct_string)
        lessonprob.save()
        return False
      if mp.usepbc != 'f':
        lessonprob = LessonProblem(lesson_type='lesson7', lesson_id=self.id, errorstep=4.0, severity=2, description='PBC use was not set to f. %s' % correct_string)
        lessonprob.save()
        return False
      if mp.useqmmm != 'n':
        lessonprob = LessonProblem(lesson_type='lesson7', lesson_id=self.id, errorstep=4.0, severity=2, description='QM/MM use was not set to n. %s' % correct_string)
        lessonprob.save()
        return False

      self.curStep = '3.5'
      self.save()
      return True
    return True

  def onMinimizeDone(self, mdp):
    try:
      lessonprob = LessonProblem.objects.filter(lesson_type='lesson7', lesson_id=self.id)[0]
    except:
      lessonprob = None
    if lessonprob:
      self.curStep = str(float(self.curStep - self.curStep % 1))
      self.save()
      return False

    if mdp.status == 'F':
      if float(self.curStep) == float('3.5'):
        lessonprob = LessonProblem(lesson_type='lesson7', lesson_id=self.id, errorstep=4.0, severity=9, description='Minimization did not complete correctly. You can click "minimization" in the status report pane on the lower left corner of this page to view the output from CHARMM and troubleshoot your job. Otherwise, try checking over your parameters for minimization, and run the job from the minimization page again.')
        lessonprob.save()
        self.curStep = '3.0'
        self.save()
        return False
    else:
      if float(self.curStep) == float('3.5'):
        self.curStep = '4'
        self.save()
        return True
    return True

  def onSolvationSubmit(self, sp):
    return True

  def onSolvationDone(self, sp):
    return True

  def onEnergySubmit(self, et):
    try:
      LessonProblem.objects.get(lesson_type='lesson7', lesson_id=self.id).delete()
    except:
      pass

    # there's a couple things we can verify here
    if float(self.curStep) == float(2.0):
      if et.useqmmm != 'n':
        lessonprob = LessonProblem(lesson_type='lesson7', lesson_id=self.id, errorstep=3.0, severity=2, description='You used QM/MM methods in your energy calculation. Please run a pure MM energy calculation,i.e., uncheck the "Use Multi-Scale modeling" box.')
        lessonprob.save()
        return False
      if et.usepbc != 'n':
        lessonprob = LessonProblem(lesson_type='lesson7', lesson_id=self.id, errorstep=3.0, severity=2, description='You used Periodic Boundary Conditions (PBC) in your energy calculation. Please leave PBC off when running this calculation.')
        lessonprob.save()
        return False
      self.curStep = '2.5'
      self.save()
      return True
    return True

  def onEnergyDone(self, et):
    if float(et.finale) == 0.0:
      return True
    try:
      lessonprob = LessonProblem.objects.filter(lesson_type='lesson7', lesson_id=self.id)[0]
    except:
      lessonprob = None
    if lessonprob:
      self.curStep = str(float(self.curStep - self.curStep % 1))
      self.save()
      return False
    create_new_ws = 'Try <a href="/buildstruct/">creating a new working structure</a> and running this step again. Make sure you set the "Topology File and Parameter File" setting for segment a-bad to "Attempt to automatically generate paramteters".'
    if et.status == 'F':
      if float(self.curStep) == float('2.5'):
        lessonprob = LessonProblem(lesson_type='lesson7', lesson_id=self.id, errorstep=3.0, severity=9, description='Energy did not complete correctly. %s Make sure that you do NOT check the "Use Periodic Boundary Conditions (PBC)" box in the energy page.' % create_new_ws)
        lessonprob.save()
        self.curStep = '2.0'
        self.save()
        return False
    else:
      if float(self.curStep) >= 2.0 and float(self.curStep) <= 3.0:
        energy_fails = float(et.finale) < (float(189765.12059) - 5) or float(et.finale) > (float(189765.12059) + 5) # energy is too small or too big
        if energy_fails:
          lessonprob = LessonProblem(lesson_type='lesson7', lesson_id=self.id, errorstep=3.0, severity=2, description='Your calculated energy did not match. The correct energy should be 189765.12059. Make sure you are using the correct parameter and topology files for this lesson. %s' % create_new_ws)
          lessonprob.save()
          self.curStep = '2.0'
          self.save()
          return False
        self.curStep = '3.0'
        self.save()
        return True
    return True
    return False

  def onDockingSubmit(self, filename, dt):
    corr_string = ', then return to the <a href="/dd_infrastructure/dsfform/">"Launch Docking Job"</a> page.'
    try:
      LessonProblem.objects.get(lesson_type='lesson7', lesson_id=self.id).delete()
    except:
      pass
    if float(self.curStep) == float(4.0):
      parent_task_action = dt.parent.action
      if parent_task_action != 'minimization':
        lessonprob = LessonProblem(lesson_type='lesson7', lesson_id=self.id, errorstep=5.0, severity=2, description='Please <a href="/dd_infrastructure/dsfform/">run</a> the docking job on the coordinates from charmming.minimization.')
        lessonprob.save()
        self.curStep = "4"
        self.save()
        return False
      if filename == "":
        lessonprob = LessonProblem(lesson_type='lesson7', lesson_id=self.id, errorstep=5.0, severity=2, description='Please <a href="/dd_infrastructure/dsfform/">run</a> the docking job using only one ligand. You have uploaded either no ligands, or more than one, for docking.')
        lessonprob.save()
        self.curStep = "4"
        self.save()
        return False
      else:
        try:
          filename1 = filename
          os.stat(filename1)
          filename2 = "%s/mytemplates/lessons/lesson7/1mvc-lig.mol2" % charmming_config.charmming_root
          os.stat(filename2)
        except:
          lessonprob = LessonProblem(lesson_type='lesson7', lesson_id=self.id, errorstep=5, severity=9, description='The ligand you submitted did not upload properly. Check to make sure the ligand was the correct one?')
          lessonprob.save()
          return False

        file1list = []
        file2list = []

        file1 = open(filename1, 'r')
        linecount = 1
        in_tripos = False # misnomer, actually checks if you're in ATOM lines. A hack to resolve different MOL2 formats that sometimes put numbers in angle brackets as resnames, and sometimes don't incldue charges
        in_bonds = False
        # file1bond_pairs = []
        try:
          for line in file1:
            if linecount not in [2, 4, 5]: # skip filename, "SMALL" and "GASTEIGER" lines. The line with the number of atoms and bonds should stay.
              if in_tripos:
                if "<TRIPOS>BOND" in line:
                  in_tripos = False
                  file1list.append(line.strip())
                  in_bonds = True
                else:
                  check_line = line.split()
                  if len(check_line) > 1:
                    for i in range(2, 5):
                      check_line[i] = check_line[i][:-1]
                    file1list.append(check_line[0:6]) # skip what looks like an imove array, resname and charges
              else:
                if in_bonds:
                  # check_line = line.split()
                  # if len(check_line) == 4:
                  #    file1bond_pairs.append(set([check_line[1],check_line[2]]))
                  break
                else:
                  file1list.append(line.strip())
                  if "<TRIPOS>ATOM" in line:
                    in_tripos = True
            linecount += 1
          file1.close()
          file2 = open(filename2, 'r')
          linecount = 1
          in_tripos = False
          in_bonds = False
          # file2bond_pairs = []
          for line in file2:
            if linecount not in [2, 4, 5]: # skip filename, "SMALL" and "GASTEIGER" lines. The line with the number of atoms and bonds should stay.
              if in_tripos:
                if "<TRIPOS>BOND" in line:
                  in_tripos = False
                  file2list.append(line.strip())
                  in_bonds = True
                else:
                  check_line = line.split()
                  if len(check_line) > 1:
                    for i in range(2, 5):
                      check_line[i] = check_line[i][:-1]
                    file2list.append(check_line[0:6])
              else:
                if in_bonds:
                  # check_line = line.split()
                  # if len(check_line) == 4:
                  #     file2bond_pairs.append(set([check_line[1],check_line[2]]))
                  break
                else:
                  file2list.append(line.strip())
                  if "<TRIPOS>ATOM" in line:
                    in_tripos = True
            linecount += 1
          file2.close()
        except:
          pass
        # same_bonds = True #check bonds...
        # for bond in file1bond_pairs:
        #     if bond not in file2bond_pairs:
        #         same_bonds = False
        #         brea
        if file1list != file2list:
          lessonprob = LessonProblem(lesson_type='lesson7', lesson_id=self.id, errorstep=5, severity=9, description='The ligand you uploaded was not the correct ligand. Make sure you are uploading the ligand created by CHARMM, converted to MOL2 format by OpenBabel%s' % corr_string)
          lessonprob.save()
          self.curStep = "4"
          self.save()
          return False
        self.curStep = '4.5'
        self.save()
        return True

  def onDockingDone(self, task):
    try:
      lessonprob = LessonProblem.objects.filter(lesson_type='lesson7', lesson_id=self.id)[0]
    except:
      lessonprob = None
    if lessonprob:
      self.curStep = str(float(self.curStep - self.curStep % 1))
      self.save()
      return False

    if task.status == 'F':
      if float(self.curStep) == float('4.5'):
        lessonprob = LessonProblem(lesson_type='lesson7', lesson_id=self.id, errorstep=5.0, severity=9, description='Docking did not complete correctly.  Check the "View Jobs/Results" page to verify your results, and try running the job again.')
        lessonprob.save()
        self.curStep = '4.0'
        self.save()
        return False
    else:
      if float(self.curStep) == float('4.5'):
        self.curStep = '5'
        self.save()
        return True
    return True

  def onNMASubmit(self, nmt):
    return True

  def onNMADone(self, nmt):
    return True

  def onMDSubmit(self, mdt):
    return True

  def onMDDone(self, mdt):
    return True

  def onLDSubmit(self, mdt):
    return True

  def onLDDone(self, ldt):
    return True

  def onSGLDSubmit(self, mdt):
    return True

  def onSGLDDone(self, sgldt):
    return True

  # An integer means the job is complete , for example 1 means step 1 was complete, 2 means step 2 was complete
  # A 0.5 means step 1 is running, a 1.5 means step 2 is running
  # If there is a lesson problem that has the same step number as the current step, that means there was a
  # user error in following the lessons and so the lesson step has failed
  # The below code can be used as an example
  def generateStatusHtml(self, file):
    # a template dictionary passing information needed by the template 'generateStatus_Html'
    step_status_list = []
    step_status_list.append("<tr class='status'><td class='status'>" + str(int(1.0)) + " File Uploaded:" + " ")
    step_status_list.append("<tr class='status'><td class='status'>" + str(int(2.0)) + " Working Structure Built:" + " ")
    step_status_list.append("<tr class='status'><td class='status'>" + str(int(3.0)) + " " + "energy".capitalize() + ": ")
    step_status_list.append("<tr class='status'><td class='status'>" + str(int(4.0)) + " " + "minimization".capitalize() + ": ")
    step_status_list.append("<tr class='status'><td class='status'>" + str(int(5.0)) + " Docking: ")
    template_dict = {}
    template_dict['status_list'] = []

    # Check to see if a lessonproblem exists, and if so store it into lessonprob
    try:
      lessonprob = LessonProblem.objects.filter(lesson_type='lesson7', lesson_id=self.id)[0]
    except:
      lessonprob = None
    for i in range(self.nSteps):
      if lessonprob and lessonprob.errorstep == math.floor(self.curStep + 1) and math.floor(self.curStep) == i:
        step_status_list[i] += ("<a class='failed' href='javascript:open_failure()'>Failed</a></td></tr>")
        # TODO: Make this less dependent on javascript hrefs...try javascript:void(0) or whatever
        continue
      elif (float(self.curStep) - 0.5) == i and float(self.curStep) % 1 == 0.5:
        step_status_list[i] += ("<a class='running'>Running</a></td></tr>")
        continue
      elif i < float(self.curStep):
        step_status_list[i] += ("<a class='done'>Done</a></td></tr>")
        continue
      elif i >= float(self.curStep):
        step_status_list[i] += ("<a class='inactive'>N/A</a></td></tr>")
        continue
    return step_status_list
  # This is used to display the lesson page. It tells the template to display which set of directions
  # for the next step
  # Each step is represented as an index in an array. The corresponding value tells the template
  # what is currently happening in regards to the lesson.
  # If the corresponding value is 2, the job is running
  # If it is 1 then the job is finished
  # 0 means the job has not stared
  # -1 means there was an error

  def getHtmlStepList(self):
    htmlcode_list = []
    for step in range(self.nSteps):
      htmlcode_list.append(0)
    if float(self.curStep) > 0:
      htmlcode_list[0] = 1
    if float(self.curStep) > 1:
      if float(self.curStep) == 1.5:
        htmlcode_list[1] = 2
      else:
        htmlcode_list[1] = 1
    if float(self.curStep) > 2:
      if float(self.curStep) == 2.5:
        htmlcode_list[2] = 2
      else:
        htmlcode_list[2] = 1
    if float(self.curStep) > 3:
      if float(self.curStep) == 3.5:
        htmlcode_list[3] = 2
      else:
        htmlcode_list[3] = 1
    if float(self.curStep) > 4:
      if float(self.curStep) == 4.5:
        htmlcode_list[4] = 2
      else:
        htmlcode_list[4] = 1
    try:
      lessonprob = LessonProblem.objects.filter(lesson_type='lesson7', lesson_id=self.id)[0]
      htmlcode_list[lessonprob.errorstep - 1] = -1
    except:
      lessonprob = None
    return htmlcode_list
