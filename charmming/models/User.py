from django.db import models

class User(models.Model):
  username = models.CharField(max_length=64, null=False, unique=True)
  digest = models.CharField(max_length=128, null=False)
  first_name = models.CharField(max_length=100, null=False)
  last_name = models.CharField(max_length=100, null=False)
  email = models.CharField(max_length=100, null=False)
  admin = models.BooleanField(default=False)

  def toJSON(self):
    return {
      'username': self.username,
      'firstName': self.first_name,
      'lastName': self.last_name,
      'email': self.email
    }
