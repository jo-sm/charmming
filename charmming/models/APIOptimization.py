from django.db import models
from django.template import Context
from django.template.loader import get_template

from charmming import charmming_config
from charmming.models import APIJob, APIHandle, MinimizeTask
from charmming.helpers import output
from charmming.scheduler.schedInterface import SchedInterface

import os
import subprocess
import pychm3

class APIOptimization(APIJob):
  implicitSolvent = models.CharField(max_length=8, null=False, default=None)
  nSDStep = models.PositiveIntegerField(default=0)
  nABNRStep = models.PositiveIntegerField(default=0)
  FinalEnergyValue = models.FloatField(null=True)
  task = models.ForeignKey(MinimizeTask, null=True)

  def run(self, req, scriptlist):
    response = {}

    # NB: for now we just hard-code in number of steps
    template_dict = {
      'rtf_list': self.rtfList.split(),
      'prm_list': self.prmList.split(),
      'str_list': self.strList.split(),
      'gbmv': (self.implicitSolvent == 'gbmv'),
      'inte': 'do_inte' in req
    }

    if template_dict['inte']:
      try:
        template_dict['inte_seg1'] = self.segList.split()[0]
        template_dict['inte_seg2'] = self.segList.split()[1]
      except:
        response['errcode'] = -970
        response['errmesg'] = 'Interaction energy requested on structure with only one segment'
        return response

    if 'sdstep' in req:
      try:
        sdsteps = min(1000, int(req['sdstep']))
      except:
        sdsteps = 100
    else:
      sdsteps = 100
    if 'abnrstep' in req:
      try:
        abnrsteps = min(25000, int(req['abnrstep']))
      except:
        abnrsteps = 500
    else:
      abnrsteps = 500
    if 'tolg' in req:
      try:
        tolg = max(0.0, min(0.10, float(req['tolg'])))
      except:
        tolg = 0.10
    else:
      tolg = 0.10
    template_dict['sdsteps'] = sdsteps
    template_dict['abnrsteps'] = abnrsteps
    template_dict['tolg'] = tolg
    t = get_template('%s/mytemplates/input_scripts/api_minimization.inp' % charmming_config.charmming_root)
    charmm_inp = output.tidyInp(t.render(Context(template_dict)))

    inp_file = '%s/minimize.inp' % self.directory
    out_file = '%s/minimize.out' % self.directory

    fp = open(inp_file, 'w')
    fp.write(charmm_inp)
    fp.close()

    if 'async' in req:
      si = schedInterface()

      # note - because API users aren't real - API jobs run under CHARMMing user ID 0;
      # this is NOT THE SAME as running as root -- I'm not that dumb :-). There is no
      # real user 0 in the DB (admin = 1), so this is done so as not to fsck with anyone
      # else's jobs.
      scriptlist.append(inp_file)
      jobid = si.submitJob(0, self.directory, scriptlist)

      apih = APIHandle()
      apih.jobType = 'mini'
      apih.APIJob = self
      apih.schedulerID = jobid
      response['handle'] = apih.genHandle()
      apih.save()

      response['errcode'] = 1
      response['errmesg'] = 'Asynchronous job submitted for execution'
      return response

    os.chdir(self.directory)
    cmd = '{} < {} > {}'.format(charmming_config.charmm_exe, inp_file, out_file)
    os.system(cmd)

    # let's save everything for posterity...
    tar_filename = self.directory.split('/')[-1] + '.tar.gz'

    os.chdir('..')
    subprocess.call(["tar", "-czf", tar_filename, self.directory.split('/')[-1]])

    response['tgz_url'] = '{}/pdbuploads/{}/{}'.format(charmming_config.charmming_url, self.user.callerName, tar_filename)
    os.chdir(self.directory)

    try:
      fp = open(out_file, 'r')
    except:
      response['errcode'] = -999
      response['errmesg'] = 'Job output file could not be opened'
      return response

    if not fp:
      response['errcode'] = -999
      response['errmesg'] = 'Job output file could not be opened'
      return response

    ene = self.parse_energy(fp)
    if 'do_inte' in req:
      fp.seek(0)
      response['inte'] = self.parse_inte(fp)
    fp.close()

    if ene < -9000000:
      response['errcode'] = -980
      response['errmesg'] = 'Job completed but energy was not calculated successfully'
    else:
      fp = open('mini.pdb', 'r')
      response['minpdb'] = fp.read()
      fp.close()
      response['errcode'] = 0
      response['eneval'] = ene

      pdb = pychm.io.pdb.PDB('mini.pdb')
      mol = pdb[0]

      for seg in mol.iter_seg():
        seg.write('/tmp/foo.pdb', outformat='pdborg')
        fp = open('/tmp/foo.pdb', 'r')
        response['min_seg_%s' % seg.segid] = fp.read()
        fp.close()

    return response
