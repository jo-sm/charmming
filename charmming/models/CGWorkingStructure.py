from django.db import models

from charmming.models import WorkingStructure

class CGWorkingStructure(WorkingStructure):
  """
  This is a special WorkingStructure type that is designed for coarse
  grained models. The main difference is how the associate routine
  works (since CG models are simple, we don't worry about patching).
  We also implement a CONECT line generator such that bonding between
  beads is accurately rendered by GLmol and JSmol.
  Bead size will remain inaccurate.
  """
  pdbname = models.CharField(max_length=120) # holds the CG-ified PDB
  cg_type = models.CharField(max_length=10)

  def getAppendPatches():
    """
    This method is overwritten because we don't do any post-appending
    patches on the Go or BLN models, so we want to make sure that we
    return an empty string
    """
    return ''
