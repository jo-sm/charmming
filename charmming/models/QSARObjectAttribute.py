from django.db import models
from django.contrib.auth.models import User

from charmming.models import QSARAttribute

class QSARObjectAttribute(models.Model):
  object_attribute_owner = models.ForeignKey(User, related_name='object_attribute_owner')
  attribute = models.ForeignKey(QSARAttribute)
  object_table_name = models.CharField(max_length=100)
  object_id = models.PositiveIntegerField(default=0)
  value = models.CharField(max_length=250, null=True, blank=True)
