from django.db import models

# Trajectory analysis parameters
class TrajanalParams(models.Model):
  selected = models.CharField(max_length=1)
  trajFileName = models.CharField(max_length=250)
  trajStartStep = models.PositiveIntegerField(default=1000)
  trajStopStep = models.PositiveIntegerField(default=10000)
  trajSkip = models.PositiveIntegerField(default=100)
  atomSelect = models.CharField(max_length=50)
