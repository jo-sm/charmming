from django.db import models
from django.contrib.auth.models import User

from charmming.models import DDSource

class DDProtein(models.Model):
  owner = models.ForeignKey(User)
  protein_owner_index = models.PositiveIntegerField(default=0)
  pdb_code = models.CharField(max_length=5, null=True, blank=True)
  protein_name = models.CharField(max_length=200, null=True, blank=True)
  description = models.CharField(max_length=250, null=True, blank=True)
  source = models.ForeignKey(DDSource, null=True, blank=True)
