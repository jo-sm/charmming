from django.db import models
from http.client import HTTPConnection
from tempfile import NamedTemporaryFile

from charmming import charmming_config
from charmming.models import Segment

from pychm3.lib.toppar import Toppar
from pychm3.io.charmm import open_rtf, open_prm
from pychm3.lib.model import Model
# from pychm3.io.mol2 import MOL2File
from pychm3.io.pdb import get_model_from_pdb

import re
import os
import openbabel
import socket
import shutil
import subprocess

class WorkingSegment(Segment):
  isBuilt = models.CharField(max_length=1)
  patch_first = models.CharField(max_length=100)
  patch_last = models.CharField(max_length=100)
  builtPSF = models.CharField(max_length=100)
  builtCRD = models.CharField(max_length=100)
  tpMethod = models.CharField(max_length=20, default="standard")
  redox = models.BooleanField(default=False)

  def set_terminal_patches(self, postdata):
    if 'first_patch' + self.name in postdata:
      self.patch_first = postdata['first_patch' + self.name]
    if 'last_patch' + self.name in postdata:
      self.patch_last = postdata['first_patch' + self.name]
    self.save()

  def getUniqueResidues(self, mol):
    """
    This routine splits up the badhet segment into each of its unique residues, so
    they can be run through CGenFF/MATCH one at a time. It also write out a MOL2 file
    with added hydrogens for each residue.
    """

    letters = 'abcdefghijklmnopqrstuvwxyz'
    badResList = []
    found = False
    for seg in mol.iter_seg():
      if seg.segid == self.name:
        found = True
        break
    if not found:
      raise AssertionError('Asked to operate on a nonexistent segment!')

    btmcount = 0
    very_bad_res = False  # declare at the top just in case...
    # master_mol = pychm.lib.mol.Mol()
    for residue in seg.iter_res():
      btmcount += 1

      # if autogenerating, check if we know about this residue and add it to the stream
      if self.tpMethod == 'autogen':
        if residue.resName == 'heme':
          if self.stream_list:
            self.stream_list += ' %s/toppar/stream/toppar_all36_prot_heme.str' % charmming_config.data_home
          else:
            self.stream_list = '%s/toppar/stream/toppar_all36_prot_heme.str' % charmming_config.data_home

          rtf_name = '%s/toppar/%s' % (charmming_config.data_home, charmming_config.default_pro_top)
          prm_name = '%s/toppar/%s' % (charmming_config.data_home, charmming_config.default_pro_prm)
          if self.rtf_list:
            self.rtf_list += ' %s' % rtf_name
          else:
            self.rtf_list = '%s' % rtf_name
          if self.prm_list:
            self.prm_list += ' %s' % prm_name
          else:
            self.prm_list = '%s' % prm_name

          self.save()
          continue
        # add more residues that we have stream files for here #

      doitnow = True
      very_bad_res = False
      if residue.resName not in badResList:
        for stuff in badResList:
          for letter in letters:
            mytest = letter + stuff
            if mytest == residue.resName:
              pass
              # BTM, have to be careful about whether we want to nuke these.
              # doitnow = False

        if not doitnow:
          continue

        badResList.append(residue.resName)
        filename_noh = self.structure.location + '/' + self.name + '-badres-' + residue.resName + ".pdb"
        filename_sdf = self.structure.location + '/' + self.name + '-badres-h-' + residue.resName + ".sdf"
        # filename_pdb = self.structure.location + '/' + self.name + '-badres-h-' + residue.resName + ".pdb"
        # filename_h = self.structure.location + '/' + self.name + '-badres-h-' + residue.resName + ".mol2"
        residue.write(filename_noh, format='pdborg')
        # shiv to try to get an SDF file for the residue
        # this is the same path as ligands, so use ligand_download_url
        conn = HTTPConnection(charmming_config.pdb_ligand_download_url)
        reqstring = charmming_config.pdb_ligand_download_pattern.format(residue.resName.upper())
        conn.request("GET", reqstring)
        resp = conn.getresponse()
        if resp.status == 200 and not(self.is_custom):
          sdf_file = resp.read()
          outfp = open(filename_sdf, 'w')
          outfp.write(sdf_file)
          outfp.close()

          sdf_re = re.compile(" +[0-9]+\.[0-9]+ +[0-9]+\.[0-9]+ +[0-9]+\.[0-9]+ +(?!H )[A-Z]+ +[0-9]+ +[0-9]+ +[0-9]+ +[0-9]+ +[0-9]+")
          sdf_result = open(filename_sdf, "r")
          atom_count_sdf = 0
          atom_count_pdb = 0
          for line in sdf_result.readlines():
            if re.match(sdf_re, line):
              atom_count_sdf += 1
              # sdf_atom = line.split()
          for atom in residue.iter_atom():
            atom_count_pdb += 1
          if atom_count_pdb != atom_count_sdf:  # We ignore hydrogens so these should match
            very_bad_res = True
        else:
          # os.system("babel -h --title %s -ipdb %s -omol2 %s" % (residue.resName,filename_noh,filename_h))
          obconv = openbabel.OBConversion()
          mol = openbabel.OBMol()
          obconv.SetInAndOutFormats("pdb", "sdf")
          obconv.ReadFile(mol, filename_noh.encode("utf-8"))
          if not(self.is_custom):
            mol.AddHydrogens()
          mol.SetTitle(residue.resName)
          obconv.WriteFile(mol, filename_sdf.encode("utf-8"))
    arf = very_bad_res
    return badResList, arf

  def makeCGenFF(self, badResList, very_bad_badres):
    """
    Connects to dogmans.umaryland.edu to build topology and
    parameter files using CGenFF
    """
    if self.rtf_list:
      self.rtf_list += ' %s/toppar/top_all36_cgenff.rtf' % charmming_config.data_home
    else:
      self.rtf_list = '%s/toppar/top_all36_cgenff.rtf' % charmming_config.data_home
    if self.prm_list:
      self.prm_list += ' %s/toppar/par_all36_cgenff.prm' % charmming_config.data_home
    else:
      self.prm_list = '%s/toppar/par_all36_cgenff.prm' % charmming_config.data_home

    header = '# USER_IP 165.112.184.52 USER_LOGIN %s\n\n' % self.structure.owner.username
    for badRes in badResList:
      # make a mol2 file out of the SDF.
      # Note: if custom residue we have to use different methodology or risk getting wrong ligands
      filebase = '%s/%s-badres-h-%s' % (self.structure.location, self.name, badRes)
      filename_sdf = filebase + '.sdf'
      filename_mol2 = filebase + '.mol2'
    #  obconv = openbabel.OBConversion()
    #  mol = openbabel.OBMol()
    #  obconv.SetInAndOutFormats("sdf", "mol2")
    #  obconv.ReadFile(mol, filename_sdf.encode("UTF-8"))
    #  mol.SetTitle(badRes)
    #  obconv.WriteFile(mol, filename_mol2.encode("UTF-8"))
      exec_string = "babel -isdf %s -omol2 %s" % (filename_sdf, filename_mol2)
      os.system(exec_string)

      # The following nasty crap attempts to make the names in the MOL2 file the same as those
      # in the PDB.
      pdbmol = get_model_from_pdb(self.structure.location + '/segment-' + self.name + '.pdb')

      # BTM: 20130701 -- deal with the fact that there might be duplicate copies
      # of the ligand
      firstres = pdbmol[0].resid
      pdbmol = [atom for atom in pdbmol if atom.resid == firstres]
      mymol2 = MOL2File(filename_mol2)
      molmol = mymol2.mol

      if not very_bad_badres:
        j = 0
        for i, pdbatom in enumerate(pdbmol):
          if pdbatom.resName != badRes.lower():
            continue
          if pdbatom.atomType.startswith('H'):
            break
          molatom = molmol[j]
          j = j + 1

          # This is for debug purposes only
          # if pdbatom.atomType.strip()[0] != molatom.atomType.strip()[0]:
          #    raise(Exception('Mismatched atom types'))
          molatom.atomType = pdbatom.atomType.strip()

      # VS 2015-2-2: Due to a bug in CHARMM, we need to change the names of protonizable residues right here, right now
      # otherwise CHARMM won't protonate them right.

      molmol.write(filename_mol2, outformat='mol2', header=mymol2.header, bonds=mymol2.bonds)

      fp = open(filename_mol2, 'r')
      payload = ""
      for line in fp.readlines():
        m = re.match('^( +[^ ]+ +[^ ]+ +[^ ]+ +[^ ]+ +[^ ]+ +)([^ ]+)( .*)$', line)
        if m is not None:
          line = m.group(1) + m.group(2).title() + m.group(3)
        payload += line + "\n"
      content = header + payload
      fp.close()

      # send off to dogmans for processing
      recvbuff = ''
      try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((charmming_config.cgenff_host, charmming_config.cgenff_port))
        s.sendall(content)
        s.shutdown(socket.SHUT_WR)
        while True:
          data = s.recv(1024)
          if not data:
            break
          recvbuff += data

      except Exception as e:
        return -1

      fp = open('%s/%s-%s-dogmans.txt' % (self.structure.location, self.name, badRes), 'w')
      fp.write(recvbuff)
      fp.close()

      # parse file returned from dogmans, and make sure that it has both topology and
      # parameters.
      rtffound = False
      prmfound = False
      inrtf = False
      inprm = False
      rtffp = open('%s/%s-%s-dogmans.rtf' % (self.structure.location, self.name, badRes), 'w')
      prmfp = open('%s/%s-%s-dogmans.prm' % (self.structure.location, self.name, badRes), 'w')
      for line in recvbuff.split('\n'):
        if inrtf:
          rtffp.write(line + '\n')
        if inprm:
          prmfp.write(line + '\n')

        line = line.strip()
        if line == 'end' or line == 'END':
          inrtf = False
          inprm = False
        if line.startswith('read rtf'):
          rtffound = True
          inrtf = True
        if line.startswith('read param'):
          prmfound = True
          inprm = True

      if not (rtffound and prmfound):
        return -1

      rtffp.close()
      prmfp.close()

      # we need to adjust the naming conventions in the PDB
      # with open_rtf('%s/%s-%s-dogmans.rtf' % (self.structure.location,self.name,badRes)) as rtffp:
      #    new_tp = pychm_toppar.Toppar()
      #    rtffp.export_to_toppar(new_tp)
      #
      #    name_list = []
      #    if new_tp.residue is not None:
      #        name_list += [ resi.name for resi in new_tp.residue ]
      #    if new_tp.patch is not None:
      #        name_list += [ resi.name for resi in new_tp.patch ]
      #
      #    pdbname = self.structure.location + '/segment-' + self.name + '.pdb'
      #    molobj = pychm.io.pdb.get_model_from_pdb(pdbname)
      #    for res in molobj.iter_res():
      #        if res.resName in name_list:
      #             res._dogmans_rename()
      #
      #    molobj.write(pdbname)

      self.rtf_list += ' %s-%s-dogmans.rtf' % (self.name, badRes)
      self.prm_list += ' %s-%s-dogmans.prm' % (self.name, badRes)
    return 0

  def makeMatch(self, badResList):
    """
    Uses the match program from the Charlie Brooks group to
    try to build topology and parameter files.
    """
    os.putenv("PerlChemistry", "%s/MATCH_RELEASE/PerlChemistry" % charmming_config.data_home)
    os.putenv("MATCH", "%s/MATCH_RELEASE/MATCH" % charmming_config.data_home)
    os.chdir(self.structure.location)

    self.rtf_list = '%s/toppar/top_all36_cgenff.rtf' % (charmming_config.data_home)
    self.prm_list = '%s/toppar/par_all36_cgenff.prm' % (charmming_config.data_home)
    os.chdir(self.structure.location)
    magic_mol = Model()
    magic_anum = 0
    for myresnum, badRes in enumerate(badResList):
      filebase = '%s-badres-h-%s' % (self.name, badRes)
      exe_line = '%s/MATCH_RELEASE/MATCH/scripts/MATCH.pl -CreatePdb %s.pdb %s.sdf' % (charmming_config.data_home, filebase, filebase)
      status, output = subprocess.getstatusoutput(exe_line)
      if status != 0:
        return -1

      # YP call script to restore coordinates in badres-h to the ones in badres
      awk_script = "awk 'FNR==NR{x[NR]=$7; y[NR]=$8; z[NR]=$9; next}{$6=x[FNR]; $7=y[FNR]; $8=z[FNR]; if ($6==\"\" && $2!=\"\") {$6=\"9999.000\"} if ($7==\"\" && $2!=\"\") {$7=\"9999.000\"} if ($8==\"\" && $2!=\"\") {$8=\"9999.000\"}printf \"%%-6s %%4s  %%-4s %%-4s %%3s %%11s%%8s%%8s %%5s %%5s %%9s\\n\", $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11}' %s.pdb %s_tmp.pdb > %s.pdb" % (filebase.replace("-h-", "-"), filebase, filebase)
      os.system("mv %s.pdb %s_tmp.pdb" % (filebase, filebase))
      os.system(awk_script)
      ####
      # add atoms from our PDB into magic_mol and renumber them.
      myown_mol = get_model_from_pdb('%s.pdb' % filebase)
      for atom in myown_mol:
        magic_anum += 1
        atom.atomNum = magic_anum
        atom.resName = badRes
        atom.resid = myresnum + 1
        atom.chainid = self.name[0]
        atom.segType = 'bad'
        magic_mol.append(atom)

      # nuke MASS lines from the RTF, or else they will conflict with CGENFF
      inpfp = open('%s.rtf' % filebase, 'r')
      outfp = NamedTemporaryFile(mode='w', delete=True)
      for line in inpfp:
        if line.startswith('MASS'):
          continue
        if line.startswith('RESI'):
          line = line.replace('UNK', badRes)
        outfp.write(line)
      outfp.flush()
      inpfp.close()
      shutil.copy(outfp.name, '%s.rtf' % filebase)
      outfp.close()

      # nuke NONBonded lines from the PRM, same reason
      inpfp = open('%s.prm' % filebase, 'r')
      outfp = NamedTemporaryFile(mode='w', delete=True)
      for line in inpfp:
        if line.startswith('NONB'):
          break
        outfp.write(line)
      outfp.flush()
      inpfp.close()
      shutil.copy(outfp.name, '%s.prm' % filebase)
      outfp.close()

      os.chmod('%s.rtf' % filebase, 0o644)
      os.chmod('%s.prm' % filebase, 0o644)

      # ToDo: add the rtf/param files that have been generated to a master topology
      # and parameter files for the entire segment.
      self.rtf_list += ' %s-badres-h-%s.rtf' % (self.name, badRes)
      self.prm_list += ' %s-badres-h-%s.prm' % (self.name, badRes)

    magic_mol.write('segment-%s.pdb' % self.name, outformat='charmm')
    return 0

  # Tim Miller, make hetid RTF/PRM using antechamber
  # Now updated to use openbabel Python bindings
  def makeAntechamber(self, badResList):
    os.putenv("AMBERHOME", charmming_config.amber_home)
    os.chdir(self.structure.location)

    # as with makeMATCH, we have to cdollect all of the atoms
    # in the BadHet segment together into a single Mol object,
    # so we can write out a unified PDB for the segment with all
    # of the correct names.
    magic_mol = Model()
    magic_anum = 0

    for myresnum, badRes in enumerate(badResList):

      basefile = '%s-badres-h-%s' % (self.name, badRes)
      sdffile = basefile + '.sdf'
      pdbfile = basefile + '.pdb'
      rtffile = basefile + '.rtf'
      prmfile = basefile + '.prm'

      obconv = openbabel.OBConversion()
      mol = openbabel.OBMol()
      obconv.SetInAndOutFormats("sdf", "pdb")
      obconv.ReadFile(mol, sdffile.encode("utf-8"))
      obconv.WriteFile(mol, pdbfile.encode("utf-8"))

     # cmd = "babel -isdf %s -opdb %s" % (sdffile,pdbfile)
     # status = os.system(cmd)
     # if status != 0:
     #     return -1
      cmd = "sed -i.bak -e 's/LIG/MOL/' %s" % pdbfile
      # TODO: Verify that this isn't a syntax error...
      status = os.system(cmd)
      if status != 0:
        return -1

      cmd = charmming_config.amber_home + "/bin/antechamber -i " + pdbfile + " -fi pdb -fo pdb -pf yes -o charmm-input.pdb"
      status = os.system(cmd)
      if status != 0:
        return -1

      inpfp = open('charmm-input.pdb', 'r')
      outfp = open(pdbfile, 'w')
      for line in inpfp:
        outfp.write(line)
      outfp.write('END\n')
      inpfp.close()
      outfp.close()

      cmd = charmming_config.amber_home + "/bin/antechamber -i " + pdbfile + " -fi pdb -at gaff -rn LIG -c 7 -fo charmm -pf yes -o charmm-input"
      status = os.system(cmd)
      if status != 0:
        return -1

      cmd = "sed -i.bak -e 's/MOL/%s/' charmm-input.pdb" % badRes
      status = os.system(cmd)
      if status != 0:
        return -1

      # call the rename_dupes script to rename atoms that might conflict with those in the
      # standard topology and parameter files.
      cmd = '%s/rename_dupes.py -n charmm-input.rtf %s/toppar/%s %s/toppar/%s %s/toppar/top_all36_water_ions.rtf' % \
            (charmming_config.data_home, charmming_config.data_home, charmming_config.default_pro_top, charmming_config.data_home, charmming_config.default_na_top, charmming_config.data_home)
      status = os.system(cmd)
      if status != 0:
        return -1

      inpfp = open('charmm-input.rtf', 'r')
      outfp = open(rtffile, 'w')
      masslines = []
      masscount = 0
      for line in inpfp:
        line = line.lower()
        newnum = 0
        if line.startswith('mass'):
          masscount += 1
          larr = line.split()
          newnum = masscount + 450
          line = line.replace(larr[1], str(newnum))
          masslines.append(line)
        if line.startswith('resi'):
          line = line.replace('lig', badRes)
        outfp.write(line)
      inpfp.close()
      outfp.close()

      # make a flex-compatible PRM file
      inpfp = open('charmm-input.prm', 'r')
      outfp = open(prmfile, 'w')
      for line in inpfp:
        line = line.lower()
        if line.startswith('bond'):
          outfp.write('atoms\n')
          for ml in masslines:
            outfp.write(ml)
          outfp.write('\n\n\n')
        outfp.write(line)
      inpfp.close()
      outfp.close()

      os.rename('charmm-input.pdb', pdbfile)

      # set the newly generated RTF/PRM to be used.
      if self.rtf_list:
        self.rtf_list += " " + rtffile
      else:
        self.rtf_list = rtffile
      if self.prm_list:
        self.prm_list += " " + prmfile
      else:
        self.prm_list = prmfile

      # append atoms to the magic_mol
      myown_mol = get_model_from_pdb(pdbfile)
      for atom in myown_mol:
        magic_anum += 1
        atom.atomNum = magic_anum
        atom.resName = badRes
        atom.resid = myresnum + 1
        atom.chainid = self.name[0]
        atom.segType = 'bad'
        magic_mol.append(atom)

    # end of loop, dump out the new PDB for this segment
    magic_mol.write('segment-%s.pdb' % self.name, outformat='charmm')

    self.save()
    return 0

  # This will run genRTF through the non-compliant hetatms
  # and make topology files for them
  def makeGenRTF(self, badResList):
    os.chdir(self.structure.location)

    my_gentop = []
    my_genpar = []
    for badRes in badResList:
      filebase = self.name + '-badres-h-' + badRes
      sdffname = filebase + '.sdf'
      pdbfname = 'segment-' + self.name + '.pdb'

      # BABEL gets called to put Hydrogen positions in for the bonds
      obconv = openbabel.OBConversion()
      mol = openbabel.OBMol()
      obconv.SetInAndOutFormats("sdf", "xyz")
      obconv.ReadFile(mol, sdffname.encode("utf-8"))
      obconv.WriteFile(mol, filebase.encode("utf-8"))

     # os.system('/usr/bin/babel -isdf ' + sdffname + ' -oxyz ' + filebase + '.xyz')
      os.system(charmming_config.apps['genrtf'] + ' -s ' + self.name + ' -r ' + badRes + ' -x ' + filebase + '.xyz')
      # Can we replace this with subprocess? Probably.

      # Run the GENRTF generated inp script through CHARMMing b/c the residue names/ids have changed.
      # so we can't trust the original PDB any more.
      shutil.copy(pdbfname, '%s.bak' % pdbfname)
      os.system(charmming_config.charmm_exe + ' < ' + filebase + '.inp > ' + filebase + '.out')

      # The new rtf filename will look like genrtf-filename.rtf
      my_gentop.append(filebase + ".rtf")
      genrtf_handle = open(filebase + ".inp", 'r')
      rtf_handle = open(self.structure.location + '/' + filebase + '.rtf', 'w')

      # Now the input file will be parsed for the rtf lines
      # The RTF files start with read rtf card append and end with End
      rtf_handle.write('* Genrtf \n*\n')
      rtf_handle.write('   28 1\n\n') # need the CHARMM version under which this was generated, c28 is OK.
      mass_start = False

      for line in genrtf_handle:
        if "MASS" in line:
          mass_start = True
          rtf_handle.write(line)
        elif "End" in line or "END" in line:
          rtf_handle.write(line)
          break
        elif mass_start:
          rtf_handle.write(line)
          rtf_handle.close()

      # now for the Paramter files
      # The new prm filename will look like genrtf-filename.prm
      if self.prm_list:
        self.prm_list += ' '
      self.prm_list += filebase + ".prm"
      self.save()
      my_genpar.append(filebase + ".prm")
      prm_handle = open(self.structure.location + '/' + filebase + '.prm', 'w')

      # Now the input file will be parsed for the prm lines
      # The PRM files start with read prm card append and end with End
      prm_handle.write('* Genrtf \n*\n\n')
      continue_to_write = False

      for line in genrtf_handle:
        if line.upper().startswith("BOND"):
          prm_handle.write(line)
          continue_to_write = True
        elif line.upper().startswith("END"):
          prm_handle.write(line)
          continue_to_write = False
          break
        elif continue_to_write:
          prm_handle.write(line)

      prm_handle.close()
      genrtf_handle.close()
    # end large for loop over all badres

    # hack alert, recombine all of the bad RTF and PRMs because GENRTF
    # does not generate flexible stuff.
    if len(my_gentop) != len(my_genpar):
      raise("Funny list lengths from GENRTF!")

    if len(badResList) > 1:
      final_toppar = Toppar()
      final_tfile = open_rtf('genrtf-%s.rtf' % self.name, 'w')
      final_pfile = open_prm('genrtf-%s.prm' % self.name, 'w')

      for i in range(len(my_gentop)):
        tmp_rtf = open_rtf(my_gentop[i], 'r')
        tmp_prm = open_prm(my_genpar[i], 'r')
        tmp_rtf.export_to_toppar(final_toppar)
        tmp_prm.export_to_toppar(final_toppar)
        tmp_rtf.close()
        tmp_prm.close()

      final_tfile.import_from_toppar(final_toppar)
      final_pfile.import_from_toppar(final_toppar)
      final_tfile.write_all()
      final_pfile.write_all()
      if self.rtf_list:
        self.rtf_list += ' genrtf-%s.rtf' % self.name
      else:
        self.rtf_list = 'genrtf-%s.rtf' % self.name
      if self.prm_list:
        self.prm_list += ' genrtf-%s.prm' % self.name
      else:
        self.prm_list = 'genrtf-%s.prm' % self.name
    else:
      if self.rtf_list:
        self.rtf_list += ' %s' % my_gentop[0]
      else:
        self.rtf_list = my_gentop[0]
      if self.prm_list:
        self.prm_list += ' %s' % my_genpar[0]
      else:
        self.prm_list = my_genpar[0]
    return 0
