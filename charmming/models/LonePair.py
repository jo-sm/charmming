from django.db import models
from charmming.models import AtomSelection

class LonePair(models.Model):
  divid = models.CharField(max_length=50, default="0")  # This is the same for both - it's linkqm + (divid) or linkmm + (divid), since they're pairs, and same for qmatomtype/mmatomtype
  qmresid = models.PositiveIntegerField(default=0)  # These two can be very different.
  mmresid = models.PositiveIntegerField(default=0)
  qmatomname = models.CharField(max_length=10, default=None)
  mmatomname = models.CharField(max_length=10, default=None)
  qmsegid = models.CharField(max_length=40, default=None)
  mmsegid = models.CharField(max_length=40, default=None)  # These will usually be the same, but I'm going for the super-generic case
  selection = models.ForeignKey(AtomSelection)
  qqh = models.PositiveIntegerField(default=1)  # Simplifies accounting for these things in MSCALE scripts
