# lesson 8
# Simulating Protein-Ligand Complexes with QM/MM Method

from django.db import models
from django.contrib.auth.models import User

from charmming import charmming_config
from charmming.models import Structure
from charmming.models import LessonProblem
from charmming.utils.atom_selection import getAtomSelections
from charmming.utils.lesson import diffPDBs

import os
import math

class Lesson8(models.Model):
  user = models.ForeignKey(User)
  nSteps = models.PositiveIntegerField(default=8)
  curStep = models.DecimalField(default=0, decimal_places=1, max_digits=3)

  def onFileUpload(self):
    try:
      LessonProblem.objects.get(lesson_type='lesson8', lesson_id=self.id).delete()
    except:
      pass

    file = Structure.objects.get(selected='y', owner=self.user, lesson_id=self.id)
    # this should never return more than 1...if it does we've got some serious DB trouble
    try:
      filename1 = '%s/mytemplates/lessons/lesson8/1mvc.pdb' % charmming_config.charmming_root
      os.stat(filename1)
      filename2 = file.location + '/1mvc.pdb'
      os.stat(filename2)
    except:
      lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=1, severity=9, description='The PDB you submitted did not upload properly. Return to the <a href="/fileupload/">Submit Structure</a> page and check to make sure the PDB.org ID was valid.')
      lessonprob.save()
      return False

    if not diffPDBs(file, filename1, filename2):
      lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=1, severity=9, description='The PDB you uploaded was not the correct PDB. Please return to the <a href="/fileupload/">Submit Structure</a> page and check to make sure you upload the correct file.')
      lessonprob.save()
      return False
    self.curStep = '1'
    self.save()
    return True

  def onBuildStructureSubmit(self, postdata):
    try:
      LessonProblem.objects.get(lesson_type='lesson8', lesson_id=self.id).delete()
    except:
      pass
    badsegs = ['select_b-pro', 'select_b-good']
    goodsegs = ['select_a-good', 'select_a-bad', 'select_a-pro']
    correct_string = 'Please return to the <a href="/buildstruct/">"Build/Select Working Structure"</a> page and create a new one taking this into account.'
    for seg in badsegs:
      if seg in postdata and postdata[seg] == "y":
        lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=2, severity=9, description='You selected segment %s, which should not be part of your working structure. %s' % (seg.replace("select_", ""), correct_string))
        lessonprob.save()
        return False
    for seg in goodsegs:
      if not(seg in postdata) or postdata[seg] == 'n':
        lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=2, severity=9, description='You did not select segment %s, which should be part of your working structure. %s' % (seg.replace("select_", ""), correct_string))
        lessonprob.save()
        return False
    self.curStep = '2'
    self.save()
    return True

  def onBuildStructureDone(self, task):
    return True

  def onMinimizeSubmit(self, mp, filename):

    try:
      LessonProblem.objects.get(lesson_type='lesson8', lesson_id=self.id).delete()
    except:
      pass

    if float(self.curStep) == float(4.0):
      parent_task_action = mp.parent.action
      if parent_task_action != 'neutralization':
        lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=5, severity=2, description='Please return to the <a href="/minimize/">Minimization</a> page and check the correct box to run minimization on the coordinates from neutralization.')
        lessonprob.save()
        return False
      if mp.sdsteps != 100:
        lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=5, severity=2, description='SD steps were not set to 100. Please return to the <a href="/minimize/">Minimization</a> page and set this to the correct value.')
        lessonprob.save()
        return False
      if mp.abnrsteps != 1000:
        lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=5, severity=2, description='ABNR steps were not set to 1000. Please return to the <a href="/minimize/">Minimization</a> page and set this to the correct value.')
        lessonprob.save()
        return False
      if float(mp.tolg) != float(0.01):
        lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=5, severity=2, description='TOLG was not set to 0.01. Please return to the <a href="/minimize/">Minimization</a> page and set this to the correct value.')
        lessonprob.save()
        return False
      if mp.usepbc != 't':
        lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=5, severity=2, description='You used Periodic Boundary Conditions (PBC) with your minimization run. Please return to the <a href="/minimize/">Minimization</a> page and check the box to use PBC.')
        lessonprob.save()
        return False
      if mp.useqmmm != 'n':
        lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=5, severity=2, description='You used multi-scale models in your minimization. Please return to the <a href="/minimize">Minimization</a> page and disable Multi-Scale Modeling.')
        lessonprob.save()
        return False

      self.curStep = '4.5'
      self.save()
      return True
    return True

  def onMinimizeDone(self, mdp):
    try:
      lessonprob = LessonProblem.objects.filter(lesson_type='lesson8', lesson_id=self.id)[0]
    except:
      lessonprob = None
    if lessonprob:
      self.curStep = str(float(self.curStep) - (float(self.curStep) % 1))
      self.save()
      return False

    if mdp.status == 'F':
      if float(self.curStep) == float('4.5'):
        lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=5, severity=9, description='The job did not complete correctly. Check your outputs, then return to the <a href="/minimize/">Minimization</a> page and run it again. If the problem persists, please contact the CHARMMing administrator.')
        lessonprob.save()
        self.curStep = '4.0'
        self.save()
        return False
    else:
      if float(self.curStep) == float('4.5'):
        self.curStep = '5'
        self.save()
        return True
    return True

  def onSolvationSubmit(self, sp):
    try:
      LessonProblem.objects.get(lesson_type='lesson8', lesson_id=self.id).delete()
    except:
      pass

    if float(self.curStep) == float(3.0):
      parent_task_action = sp.parent.action
      if parent_task_action != 'build':
        lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=4, severity=2, description='Please return to the <a href="/solvate/">Solvation</a> page and check the correct box to run neutralization on the coordinates from build.')
        lessonprob.save()
        return False
      if sp.solvation_structure != 'rhdo':
        lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=4, severity=2, description='Solvation Structure was not set to rhdo. Please return to the <a href="/solvate/">Solvation</a> page and set this to the correct value.')
        lessonprob.save()
        return False
      if (float(sp.xtl_x) < float(71.582 - 2) or float(sp.xtl_x) > float(71.582 + 2)) or (float(sp.xtl_y) < float(71.582 - 2) or float(sp.xtl_y) > float(71.582 + 2)) or (float(sp.xtl_z) < float(71.582 - 2) or float(sp.xtl_z) > float(71.582 + 2)):
        lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=4, severity=2, description='The wrong radius size was set. Please return to the <a href="/solvate/">Solvation</a> page and use a value of 10.0 instead.')
        lessonprob.save()
        return False
      if sp.salt != 'POT':
        lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=4, severity=2, description='The wrong salt was used. Please return to the <a href="/solvate/">Solvation</a> page and set this to the correct value.') # Modify this line for your salt...
        lessonprob.save()
        return False
      if float(sp.concentration) != 0.15:
        lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=4, severity=2, description='You did not set salt concentration to 0.15. Please return to the <a href="/solvate/">Solvation</a> page and set this to the correct value.')
        lessonprob.save()
        return False
      if float(sp.ntrials) != 3:
        lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=4, severity=2, description='You did not set the number of trials to 3. Please return to the <a href="/solvate/">Solvation</a> page and set this to the correct value.')
        lessonprob.save()
        return False

      self.curStep = '3.5'
      self.save()
      return True
    return True

  def onSolvationDone(self, sp):
    try:
      lessonprob = LessonProblem.objects.filter(lesson_type='lesson8', lesson_id=self.id)[0]
    except:
      lessonprob = None
    if lessonprob:
      self.curStep = str(float(self.curStep - self.curStep % 1))
      self.save()
      return False

    if sp.status == 'F':
      if float(self.curStep) == float('3.5'):
        lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=4, severity=9, description='The job did not complete correctly. Check your outputs, then return to the <a href="/solvate/">Solvation</a> page and run it again. If the problem persists, please contact the CHARMMing administrator.')
        lessonprob.save()
        self.curStep = '3.0'
        self.save()
        return False
    else:
      if float(self.curStep) == float('3.5'):
        self.curStep = '4'
        self.save()
        return True
    return True

  def onEnergySubmit(self, et):
    try:
      try:
        LessonProblem.objects.filter(lesson_type='lesson8', lesson_id=self.id).delete()
      except:
        pass
      # there's a couple things we can verify here
      parent_task_action = False
      if et.parent:
        parent_task_action = et.parent.action
      if float(self.curStep) == float(2.0):
        # you shouldn't be here without task parental supervision
        # if you are, something has fundamentally changed about how we build structures
        if parent_task_action != "build":
          lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=3, severity=2, description='You did not run energy on the coordinates from build. Return to the <a href="/energy/">Energy</a> page and click the correct box to use these coordinates.')
          lessonprob.save()
          return False
        if et.useqmmm != 'n':
          lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=3, severity=2, description='You used multi-scale methods with your energy run. Please return to the <a href="/energy/">Energy</a> page and check the box to use Multi-scale Modeling.')
          lessonprob.save()
          return False
        if et.usepbc != 'n':
          lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=3, severity=2, description='You used use Periodic Boundary Conditions (PBC) with your energy run. Please return to the <a href="/energy/">Energy</a> page and check the box to use PBC.')
          lessonprob.save()
          return False
        self.curStep = '2.5'
        self.save()
        return True
      if float(self.curStep) == float(6.0):
        if parent_task_action != "md":
          lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=7, severity=2, description='You did not run energy on the coordinates from the dynamics run (md). Return to the <a href="/energy/">Energy</a> page and click the correct box to use these coordinates.')
          lessonprob.save()
          return False
        if et.useqmmm != 'y':
          lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=7, severity=2, description='You did not use multi-scale methods with your energy run. Please return to the <a href="/energy/">Energy</a> page and check the box to use multi-scale methods.')
          lessonprob.save()
          return False
        if et.modelType != "qmmm":
          # at this point we verified that they did use multi-scale stuff so we check
          lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=7, severity=2, description='You did not use QM/MM with your energy run. Please return to the <a href="/energy/">Energy</a> page and check the box to use QM/MM (additive model) methods.')
          lessonprob.save()
          return False
        if et.usepbc != 'n':
          lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=7, severity=2, description='You used Periodic Boundary Conditions (PBC) with your energy run. QM/MM does not support PBC. Please return to the <a href="/energy/">Energy</a> page and check the box to use PBC.')
          lessonprob.save()
          return False
        # ok, so far so good. Now let's make sure they selected using the GUI.
        sel_dict = getAtomSelections({}, et.workstruct) # get the selections
        if len(sel_dict) < 1: # no selection
          lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=7, severity=2, description='You did not submit a region selection with your energy run. Please return to the <a href="/energy/">Energy</a> page and click "Select atoms graphically" to select the ligand for use in the energy calculation.')
          lessonprob.save()
          return False
        if 'oniom_selections' in sel_dict:
          # you shouldn't be here, but we check to make sure in case of weird behavior.
          lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=7, severity=2, description='You submitted an ONIOM selection, which implies that you did not use QM/MM with your energy run. Please return to the <a href="/energy/">Energy</a> page and check the box to use QM/MM (additive model) methods.')
          lessonprob.save()
          return False
        # ok, now that we're clear, we have a selection. There's no LPs, so first let's check if we do
        if "lonepairs" in sel_dict and len(sel_dict["lonepairs"]) > 0:
          lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=7, severity=2, description='You submitted a selection with link atoms. For the purposes of this lesson, no link atoms are necessary. Please return to the <a href="/energy/">Energy</a> page, click "Select Atoms Graphically", and make sure to select the entire ligand, and only the ligand.')
          lessonprob.save()
          return False
        if sel_dict['atomselection'].selectionstring.strip() != "bynum 3387:3441": # strip() just in case so we don't run into weird issues with spaces
          lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=7, severity=2, description='You did not make the correct selection. Please return to the <a href="/energy/">Energy</a> page, click "Select Atoms Graphically", and make sure to select the entire ligand, and only the ligand. Note that you need to use the graphical interface for this step.')
          lessonprob.save()
          return False

        self.curStep = '6.5'
        self.save()
        return True
      if float(self.curStep) == float(7.0):
        if parent_task_action != "md":
          lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=8, severity=2, description='You did not run energy on the coordinates from the dynamics run (md). Return to the <a href="/energy/">Energy</a> page and click the correct box to use these coordinates.')
          lessonprob.save()
          return False
        if et.useqmmm != 'y':
          lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=8, severity=2, description='You did not use multi-scale methods with your energy run. Please return to the <a href="/energy/">Energy</a> page and check the box to use multi-scale methods.')
          lessonprob.save()
          return False
        if et.modelType != "oniom":
          # at this point we verified that they did use multi-scale stuff so we check
          lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=8, severity=2, description='You did not use MSCALE with your energy run. Please return to the <a href="/energy/">Energy</a> page and check the box to use MSCALE (subtractive model) methods.')
          lessonprob.save()
          return False
        if et.usepbc != 'n':
          lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=8, severity=2, description='You used Periodic Boundary Conditions (PBC) with your energy run. MSCALE does not support PBC under the current version of CHARMM. Please return to the <a href="/energy/">Energy</a> page and check the box to use PBC.')
          lessonprob.save()
          return False
        sel_dict = getAtomSelections({}, et.workstruct) # get the selections
        if len(sel_dict) < 1: # no selection
          lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=7, severity=2, description='You did not submit a region selection with your energy run. Please return to the <a href="/energy/">Energy</a> page and click "Select atoms graphically" to select the ligand for use in the energy calculation.')
          lessonprob.save()
          return False
        if 'atomselection' in sel_dict:
          # you shouldn't be here, but we check to make sure in case of weird behavior.
          lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=7, severity=2, description='You submitted a QM/MM selection, which implies that you did not use MSCALE with your energy run. Please return to the <a href="/energy/">Energy</a> page and check the box to use MSCALE (subtractive model) methods.')
          lessonprob.save()
          return False
        # ok, now that we're clear, we have a selection. There's no LPs, so first let's check if we do
        if "lonepairs" in sel_dict and len(sel_dict["lonepairs"]) > 0:
          lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=7, severity=2, description='You submitted a selection with link atoms. For the purposes of this lesson, no link atoms are necessary. Please return to the <a href="/energy/">Energy</a> page, click "Select Atoms Graphically", and make sure to select the entire ligand, and only the ligand.')
          lessonprob.save()
          return False
        if len(sel_dict['oniom_selections']) > 2: # too many laters
          lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=7, severity=2, description='You submitted a selection with the wrong number of layers. Please return to the <a href="/energy/">Energy</a> page, click "Select Atoms Graphically", and make sure to select the option to only have two layers.')
          lessonprob.save()
          return False
        if sel_dict['oniom_selections'][1].selectionstring.strip() != "all": # if layer 2 isn't MM, we've got trouble
          lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=7, severity=2, description='Your lowest level of theory (Layer 2) is not MM. Please return to the <a href="/energy/">Energy</a> page, click "Select Atoms Graphically", and check the "Make this layer MM" box for Layer 2.')
          lessonprob.save()
          return False
        if sel_dict['oniom_selections'][0].selectionstring.strip() != "bynum 3387:3441": # if layer 1 doesn't match, throw an error
          lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=7, severity=2, description='You did not make the correct selection. Please return to the <a href="/energy/">Energy</a> page, click "Select Atoms Graphically", and make sure to select the entire ligand, and only the ligand. Note that you need to use the graphical interface for this step.')
          lessonprob.save()
          return False
        self.curStep = '7.5'
        self.save()
        return True
    except:
      pass
    return True

  def onEnergyDone(self, et):
    try:
      lessonprob = LessonProblem.objects.filter(lesson_type='lesson8', lesson_id=self.id)[0]
    except:
      lessonprob = None
    if lessonprob:
      self.curStep = str(float(self.curStep - self.curStep % 1))
      self.save()
      return False
    if et.status == 'F':
      if float(self.curStep) == float('2.5'):
        lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=3, severity=9, description='The job did not complete correctly. Check your outputs, then return to the <a href="/energy/">Energy</a> page and run it again. If the problem persists, please contact the CHARMMing administrator.')
        lessonprob.save()
        self.curStep = '2.0'
        self.save()
        return False
      if float(self.curStep) == float('6.5'):
        lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=7, severity=9, description='The job did not complete correctly. Check your outputs, then return to the <a href="/energy/">Energy</a> page and run it again. If the problem persists, please contact the CHARMMing administrator.')
        lessonprob.save()
        self.curStep = '6.0'
        self.save()
        return False
      if float(self.curStep) == float('7.5'):
        lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=8, severity=9, description='The job did not complete correctly. Check your outputs, then return to the <a href="/energy/">Energy</a> page and run it again. If the problem persists, please contact the CHARMMing administrator.')
        lessonprob.save()
        self.curStep = '7.0'
        self.save()
        return False
    else:
      if float(self.curStep) == float('2.5'):
        # get atom selections
        # sele_dict = getAtomSelections({}, et.workstruct)
        if float(et.finale) < (float(189760) - 5.0) or float(et.finale) > (float(189760) + 5.0):
          lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=3, severity=2, description='Your calculated energy of ' + et.finale + ' did not match. Make sure you are using the correct parameter and topology files for this lesson, then return to the <a href="/energy/">Energy</a> page and run the calculation again.')
          lessonprob.save()
          self.curStep = '2'
          self.save()
          return False
        self.curStep = '3'
        self.save()
        return True
      # we can't get to the following two cases unless we had the right selection. Because MD positioning is variable, we can't check energy values.
      if float(self.curStep) == float('6.5'):
        self.curStep = '7'
        self.save()
        return True
      if float(self.curStep) == float('7.5'):
        self.curStep = '8'
        self.save()
        return True
    return True

  def onNMASubmit(self, nmt):
    return True

  def onNMADone(self, nmt):
    return True

  def onMDSubmit(self, mdt):
    try:
      LessonProblem.objects.filter(lesson_type='lesson8', lesson_id=self.id).delete()
    except:
      pass
    # there's a couple things we can verify here
    if float(self.curStep) == float(5.0):
      parent_task_action = mdt.parent.action
      if parent_task_action != 'minimization':
        lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=6, severity=2, description='Please return to the <a href="/dynamics/md/">Molecular Dynamics</a> page and check the correct box to run md on the coordinates from charmming.minimization.')
        lessonprob.save()
        return False
      if mdt.nstep != 1000:
        lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=6, severity=2, description='NSTEP was not set to 1000')
        lessonprob.save()
        return False
      if mdt.scpism is not False:
        lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=6, severity=2, description='SCPISM Implicit Solvent was not set to False')
        lessonprob.save()
        return False
      if not mdt.usepbc:
        lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=6, severity=2, description='You did not use Periodic Boundary Conditions (PBC) with your dynamics run.')
        lessonprob.save()
        return False
      if mdt.ensemble != 'heat':
        lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=6, severity=2, description='heat dynamics were not selected. Please return to the <a href="/dynamics/md/">Molecular Dynamics</a> page and select this type of dynamics.')
        lessonprob.save()
        return False
      if float(mdt.firstt) != float(210.15):
        lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=6, severity=2, description='FIRSTT was not set to 210.15. Please return to the <a href="/dynamics/md/">Molecular Dynamics</a> page and, with Heating selected, set FIRSTT to the correct value.')
        lessonprob.save()
        return False
      if float(mdt.finalt) != float(310.15):
        lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=6, severity=2, description='FINALT was not set to 310.15. Please return to the <a href="/dynamics/md/">Molecular Dynamics</a> page and, with Heating selected, set FINALT to the correct value.')
        lessonprob.save()
        return False
      if float(mdt.teminc) != float(10.0):
        lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=6, severity=2, description='TEMINC was not set to 10.0. Please return to the <a href="/dynamics/md/">Molecular Dynamics</a> page and, with Heating selected, set TEMINC to the correct value.')
        lessonprob.save()
        return False
      if float(mdt.ihtfrq) != float(100.0):
        lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=6, severity=2, description='IHTFRQ was not set to 100.0. Please return to the <a href="/dynamics/md/">Molecular Dynamics</a> page and, with Heating selected, set IHTFRQ to the correct value.')
        lessonprob.save()
        return False
      if float(mdt.tbath) != float(310.15):
        lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=6, severity=2, description='tbath was not set to 310.15. Please return to the <a href="/dynamics/md/">Molecular Dynamics</a> page and, with Heating selected, set TBATH to the correct value.')
        lessonprob.save()
        return False
    self.curStep = '5.5'
    self.save()
    return True

  def onMDDone(self, mdt):
    try:
      lessonprob = LessonProblem.objects.filter(lesson_type='lesson8', lesson_id=self.id)[0]
    except:
      lessonprob = None
    if lessonprob:
      self.curStep = str(float(self.curStep - self.curStep % 1))
      self.save()
      return False

    if mdt.status == 'F':
      if float(self.curStep) == float('5.5'):
        lessonprob = LessonProblem(lesson_type='lesson8', lesson_id=self.id, errorstep=6, severity=9, description='The job did not complete correctly. Check your outputs, then return to the <a href="/dynamics/md/">Molecular Dynamics</a> page and run it again. If the problem persists, please contact the CHARMMing administrator.')
        lessonprob.save()
        self.curStep = '5.0'
        self.save()
        return False
    else:
      if float(self.curStep) == float('5.5'):
        self.curStep = '6'
        self.save()
        return True
    return True

  def onLDSubmit(self, mdt):
    return True

  def onLDDone(self, ldt):
    return True

  def onSGLDSubmit(self, mdt):
    return True

  def onSGLDDone(self, sgldt):
    return True

  # An integer means the job is complete , for example 1 means step 1 was complete, 2 means step 2 was complete
  # A 0.5 means step 1 is running, a 1.5 means step 2 is running
  # If there is a lesson problem that has the same step number as the current step, that means there was a
  # user error in following the lessons and so the lesson step has failed
  # The below code can be used as an example
  def generateStatusHtml(self, file):
    # a template dictionary passing information needed by the template 'generateStatus_Html'
    step_status_list = []
    step_status_list.append("<tr class='status'><td class='status'>" + str(int(1.0)) + " File Uploaded" + ": ")
    step_status_list.append("<tr class='status'><td class='status'>" + str(int(2.0)) + " Working Structure Built" + ": ")
    step_status_list.append("<tr class='status'><td class='status'>" + str(int(3.0)) + " " + "energy".capitalize() + ": ")
    step_status_list.append("<tr class='status'><td class='status'>" + str(int(4.0)) + " Solvation: ")
    step_status_list.append("<tr class='status'><td class='status'>" + str(int(5.0)) + " " + "minimization".capitalize() + ": ")
    step_status_list.append("<tr class='status'><td class='status'>" + str(int(6.0)) + " Heating Dynamics: ")
    step_status_list.append("<tr class='status'><td class='status'>" + str(int(7.0)) + " QM/MM energy: ")
    step_status_list.append("<tr class='status'><td class='status'>" + str(int(8.0)) + " MSCALE energy: ")
    template_dict = {}
    template_dict['status_list'] = []
    # Check to see if a lessonproblem exists, and if so store it into lessonprob
    try:
      lessonprob = LessonProblem.objects.filter(lesson_type='lesson8', lesson_id=self.id)[0]
    except:
      lessonprob = None
    for i in range(self.nSteps):
      if lessonprob and lessonprob.errorstep == math.floor(self.curStep + 1) and math.floor(self.curStep) == i:
        step_status_list[i] += ("<a class='failed' href='javascript:open_failure();'>Failed</a></td></tr>")
        continue
      elif (float(self.curStep) - 0.5) == i and float(self.curStep) % 1 == 0.5:
        step_status_list[i] += ("<a class='running'>Running</a></td></tr>")
        continue
      elif i < float(self.curStep):
        step_status_list[i] += ("<a class='done'>Done</a></td></tr>")
        continue
      elif i >= float(self.curStep):
        step_status_list[i] += ("<a class='inactive'>N/A</a></td></tr>")
        continue
    return step_status_list
  # This is used to display the lesson page. It tells the template to display which set of directions
  # for the next step
  # Each step is represented as an index in an array. The corresponding value tells the template
  # what is currently happening in regards to the lesson.
  # If the corresponding value is 2, the job is running
  # If it is 1 then the job is finished
  # 0 means the job has not stared
  # -1 means there was an error

  def getHtmlStepList(self):
    htmlcode_list = []
    for step in range(self.nSteps):
      htmlcode_list.append(0)
    if self.curStep > 0:
      htmlcode_list[0] = 1
    if self.curStep > 1:
      if self.curStep == 1.5:
        htmlcode_list[1] = 2
      else:
        htmlcode_list[1] = 1
    if self.curStep > 2:
      if self.curStep == 2.5:
        htmlcode_list[2] = 2
      else:
        htmlcode_list[2] = 1
    if self.curStep > 3:
      if self.curStep == 3.5:
        htmlcode_list[3] = 2
      else:
        htmlcode_list[3] = 1
    if self.curStep > 4:
      if self.curStep == 4.5:
        htmlcode_list[4] = 2
      else:
        htmlcode_list[4] = 1
    if self.curStep > 5:
      if self.curStep == 5.5:
        htmlcode_list[5] = 2
      else:
        htmlcode_list[5] = 1
    if self.curStep > 6:
      if self.curStep == 6.5:
        htmlcode_list[6] = 2
      else:
        htmlcode_list[6] = 1
    if self.curStep > 7:
      if self.curStep == 7.5:
        htmlcode_list[7] = 2
      else:
        htmlcode_list[7] = 1
    try:
      lessonprob = LessonProblem.objects.filter(lesson_type='lesson8', lesson_id=self.id)[0]
      htmlcode_list[lessonprob.errorstep - 1] = -1
    except:
      lessonprob = None
    return htmlcode_list
