# This program will be rewritten every time create_lesson.py is run
# Typically it adds new lesson just created by running create_lesson.py
# Lesson number list
lesson_num_lis = [
  '1',
  '2',
  '3',
  '4',
  '5',
  '6',
  '7',
  '8'
]

# The file type lessons need to upload
file_type = {
  '1': 'CRD',
  '2': 'PDB',
  '3': 'CUSTOM/SEQ',
  '4': 'CRD',
  '5': 'CRD',
  '6': 'PDB',
  '7': 'PDB',
  '8': 'PDB'
}


# Lesson title - used to simplify skeleton templating
lesson_title = {
  '1': 'Intro to simulations',
  '2': 'Simulating proteins',
  '3': 'SGLD, sequences',
  '4': 'Custom RTF, QM/MM',
  '5': 'Coarse-grain models',
  '6': 'Redox calculation',
  '7': 'Docking',
  '8': 'Simulating Protein-Ligand Complexes with QM/MM Methods'
}
