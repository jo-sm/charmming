from charmming.utils.string import capitalize_classcase

import importlib

def get_base_validator(program):
  return get_validator(program, 'base')

def get_validator(program, name):
  capitalized_name = capitalize_classcase(name)

  try:
    module = importlib.import_module('charmming.validators.{}'.format(program))
  except ModuleNotFoundError:
    return None

  if hasattr(module, capitalized_name):
    return getattr(module, capitalized_name)
  else:
    return None
