from charmming import charmming_config
from charmming.models import Task
from charmming.helpers import output
from charmming.utils.task import start, add_script
from charmming.utils.script import get_input_script

from pychm3.lib import Atom

import os
import pickle

def qchem_tpl(request, qt, pTaskID):
  # template dictionary passes the needed variables to the template
  template_dict = {}

  if pTaskID != -1:  # skip checking for pTask cause this is the first one
    pTask = Task.objects.filter(id=pTaskID)[0]
    qt.parent = pTask
    qt.save()
  else:
    qt.parent = None
    qt.save()
  # workstruct has to be defined.
  loc = qt.workstruct.structure.location
  iden = qt.workstruct.identifier
# in order to simplify our lives, we will use the pickle, which is always created
  atoms = ""
  if pTaskID == -1:
    pick_path = "%s/%s.dat" % (qt.workstruct.structure.location, qt.workstruct.identifier)
    try:
      os.stat(pick_path)
      if not qt.workstruct.localpickle:
        qt.workstruct.localpickle = pick_path
        qt.workstruct.save()
    except:
      pick_path = qt.workstruct.structure.pickle
#        pick_path = qt.workstruct.localpickle if qt.workstruct.localpickle else qt.workstruct.structure.pickle
#   This test is unreliable because our database fails.
    pick_file = open(pick_path, "rb")
    inp_file = pickle.load(pick_file)
    pick_file.close()
    # now write xyz
    # this code is slower but more generic
    curr_model = inp_file[qt.workstruct.modelName]
    seglist = [x.name for x in qt.workstruct.segments.all()]

    for seg in curr_model.iter_seg():
      if seg.segid in seglist:
        for res in seg.iter_res():
          one_letter = res.is_nuc or res.is_pro or (res.name.upper() in ['HOH', 'TIP3'])
          # one_letter = Etc.isNuc(res.resName) or Etc.isPro(res.resName) or (res.resName.upper() in ['HOH', 'TIP3'])  # check if you only get the first letter of the element
          for atom in res.iter_atom():
            x, y, z = atom.cart
            if one_letter:
              elem = atom.atomType.strip()[:1]
            else:
              elem = atom.atomType[:2].strip()
            elem = elem.upper()
            atoms += "%-2s  %14.5f %14.5f %14.5f\n" % (elem, x, y, z)
  else:
    inp_file = open("%s/%s-%s.pdb" % (loc, iden, pTask.action), "r")
    for line in inp_file.readlines():
      if "ATOM" in line or "HETATM" in line:
        atoms += Atom(line, format="charmm").to_str(format="xyz") + "\n"
  template_dict['molecule_lines'] = atoms
#    template_dict['useqmmm'] = qt.useqmmm == "y"
  template_dict['jobtype'] = qt.jobtype
  template_dict['charge'] = qt.charge
  template_dict['exchange'] = qt.exchange
  if qt.correlation:
    template_dict['correlation'] = qt.correlation
  template_dict['basis'] = qt.basis
  template_dict['multiplicity'] = qt.multiplicity

  # modelType = ""
  qt.save()
  t = get_input_script('qchem_job.inp')
  qchem_inp = output.tidyInp(t.render(template_dict))

  qchem_filename = "%s/%s-qchem-%s.inp" % (qt.workstruct.structure.location, qt.workstruct.identifier, qt.jobtype)
  inp_out = open(qchem_filename, 'w')
  inp_out.write(qchem_inp)
  inp_out.close()
  add_script(qt, "qchem", qchem_filename, charmming_config.default_qchem_nprocs)
#    qt.scripts += ',%s' % qchem_filename
  # TODO: change these to use new task infrastructure
  start(qt)
  qt.save()

#    #YP lessons status update
#    try:
#        lnum=qt.workstruct.structure.lesson_type
#        lesson_obj = eval(lnum+'.models.'+lnum.capitalize()+'()')
#    except:
#        lesson_obj = None
#
#    if lesson_obj:
#        lessonaux.doLessonAct(qt.workstruct.structure,"onQchemSubmit",mp,"")
#    #YP

  qt.workstruct.save()
  return output.returnSubmission('Q-Chem')
