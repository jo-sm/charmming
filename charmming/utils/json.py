from django.db.models.query import QuerySet
from django.db.models import Model

import json

class CHARMMingJSONEncoder(json.JSONEncoder):
  """
  Handles serializing objects not natively supported
  for serialization.

  If the object cannot be serialized, it will return
  None as the object.
  """
  def default(self, obj):
    try:
      result = super().default(obj)
    except TypeError:
      if isinstance(obj, QuerySet):
        result = []

        for i in obj:
          result.append(_serializeDjangoObj(i))
          # Get values and append them to an object
          # Doesn't use django.core.serializers.serialize since
          # it doesn't handle JSON serialization well
      elif isinstance(obj, Model):
        result = _serializeDjangoObj(obj)
      else:
        result = None

    return result

def dumps(obj):
  return json.dumps(obj, cls=CHARMMingJSONEncoder)

def loads(obj):
  return json.loads(obj)

def _serializeDjangoObj(obj):
  result = None

  # Look for the toJSON method first
  if hasattr(obj, 'toJSON'):
    result = obj.toJSON()

  for key, value in list(result.items()):
    if isinstance(value, QuerySet):
      _temp = []

      for inst in value:
        _temp.append(_serializeDjangoObj(inst))

      result[key] = _temp

    elif isinstance(value, Model):
      result[key] = _serializeDjangoObj(value)

  return result
