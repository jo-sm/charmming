from django.contrib import messages
from django.template.loader import get_template
from django.template import Context

from charmming.helpers import output
from charmming import charmming_config
from charmming.utils.script import process_postdata, setup_scripts
from charmming.utils.task import start_task, add_script, start
from charmming.utils.lesson import get_lesson_object, doLessonAct
from charmming.utils.list import get
from charmming.utils.toppar import get_toppar_path

import os
import pickle
import copy

water_boxes = [
  {
    'diameter': 30,
    'waters_count': 890
  },
  {
    'diameter': 50,
    'waters_count': 4057
  },
  {
    'diameter': 70,
    'waters_count': 11255
  },
  {
    'diameter': 100,
    'waters_count': 32456
  },
  {
    'diameter': 216,
    'waters_count': 424277
  }
]

def get_water_box(diameter):
  water_box_desc = get(water_boxes, lambda water_box: water_box['diameter'] == diameter)

  if not water_box_desc:
    return None

  return get_toppar_path("water_boxes/{}_{}_box.crd".format(water_box_desc['diameter'], water_box_desc['waters_count']))

# This function constructs an input script with template
def neutralize_tpl(solvTask, postdata):
  workingstruct = solvTask.workstruct

  cation = "POT"
  if postdata['salt'] == "nacl":
    cation = "SOD"
  elif postdata['salt'] == "cacl2":
    cation = "CAL"
  elif postdata['salt'] == "mgcl2":
    cation = "MG"
  elif postdata['salt'] == "cscl":
    cation = "CES"
  solvTask.salt = cation

  try:
    concentration = float(postdata['concentration'])
  except:
    concentration = 0.15
  solvTask.concentration = str(concentration)
  try:
    ntrials = int(postdata['ntrials'])
  except:
    ntrials = 3
  solvTask.ntrials = ntrials
  solvTask.save()

  os.chdir(workingstruct.structure.location)

  # template dictionary passes the needed variables to the template
  template_dict = {}
  template_dict['topology_list'], template_dict['parameter_list'] = workingstruct.getTopparList()
  template_dict['infile'] = workingstruct.identifier + '-solvation'
  template_dict['cation'] = cation
  template_dict['concentration'] = concentration
  template_dict['ntrials'] = ntrials
  template_dict['solvation_structure'] = solvTask.solvation_structure
  template_dict['fname'] = workingstruct.identifier
  if workingstruct.topparStream:
    template_dict['tpstream'] = list(set(workingstruct.topparStream.split()))
  else:
    template_dict['tpstream'] = []

  t = get_template('%s/mytemplates/input_scripts/neutralize_template.inp' % charmming_config.charmming_root)
  charmm_inp = output.tidyInp(t.render(Context(template_dict)))

  neut_filename = workingstruct.structure.location + "/" + workingstruct.identifier + "-neutralization.inp"
  inp_out = open(neut_filename, 'w')
  inp_out.write(charmm_inp)
  inp_out.close()

  if solvTask.solvation_structure != 'sphere':
    # set up the crystal file
    t2_dict = {}
    t2_dict['shape'] = solvTask.solvation_structure
    t2_dict['dim_x'] = solvTask.xtl_x
    t2_dict['dim_y'] = solvTask.xtl_y
    t2_dict['dim_z'] = solvTask.xtl_z

    if solvTask.solvation_structure == 'rhdo':
      t2_dict['angles'] = "60.0 90.0 60.0"
    elif solvTask.solvation_structure == 'hexa':
      t2_dict['angles'] = "90.0 90.0 120.0"
    elif solvTask.solvation_structure in ['cube', 'cubic', 'tetr']:
      t2_dict['angles'] = "90.0 90.0 90.0"
    else:
      raise AssertionError("Unknown XTL type %s" % solvTask.solvation_structure)

    t = get_template('%s/mytemplates/input_scripts/cryst_template.inp' % charmming_config.charmming_root)
    cryst_inp = output.tidyInp(t.render(Context(t2_dict)))

    cry_filename = workingstruct.structure.location + '/' + workingstruct.identifier + "-crystl.str"
    str_out = open(cry_filename, 'w+')
    str_out.write(cryst_inp)
    str_out.close()

  # copy the addions stream file
  copy("%s/addions.str" % charmming_config.data_home, "%s/%s-addions.str" % (workingstruct.structure.location, workingstruct.identifier))

  # start job
  add_script(solvTask, "charmm", neut_filename, charmming_config.default_charmm_nprocs)
#    solvTask.scripts += ',%s' % neut_filename
  solvTask.workstruct.save() # make sure we don't lose extra information like localpickle and topparStream
  solvTask.save()

  # lessons are still borked
  # if file.lesson_type:
  #    lessonaux.doLessonAct(file,"onSolvationSubmit",postdata,None)

  start_task(solvTask)
  solvTask.save()
  workingstruct.save()

"""
Contains utilities related to setting up solvation boxen.
"""

def get_closest_solvbox(diam):
  """
  Gets the solvation box whose size is closest to a particular diameter.
  Raises an Exception if no box can be used, or if the structure is too big.
  """
  right_box = False
  solv_box_copy = copy.deepcopy(charmming_config.solvation_boxes) # let's not risk modifying globals
  if diam > solv_box_copy[-1][0]:
    raise ValueError("Your structure is too big to be solvated by CHARMMing.")
  while not right_box:
    # check the middle element
    middle = int(len(solv_box_copy) / 2)
    current_compare = solv_box_copy[middle]
    if middle == 0 or middle == len(solv_box_copy):  # deal with extreme cases
      right_box = current_compare
      break
    if diam > current_compare[0]:
      # first check if we're less than the next box
      if diam < solv_box_copy[middle + 1][0]:
        # if it's bigger than the current, but smaller than the one following it, use
        right_box = solv_box_copy[middle + 1]
        break
      # otherwise binary search the second half and keep going
      solv_box_copy = solv_box_copy[middle:]
    elif diam < current_compare[0]:
      # we're smaller, so check to make sure we aren't picking too big a box
      if diam > solv_box_copy[middle - 1][0]:
        right_box = current_compare  # if it's bigger than the box preceding, the one you're checking is the minimal box
        break
      # otherwise break it down more
      solv_box_copy = solv_box_copy[:middle]
    else:  # whoa, we're equal
      right_box = solv_box_copy[middle + 1]
      break
  return right_box

def get_diameter_from_pickle(workingstruct, buffer):
  """
  Gets the diameter of a structure from its pickle file, and returns it.
  If there is an issue with the structure that prevents this, an Exception is raised.
  """
  load_pickle = workingstruct.localpickle if workingstruct.localpickle else workingstruct.structure.pickle
  pickpick = open(load_pickle, 'rb')
  pickfile = pickle.load(pickpick)  # load the base structure - maybe optimize to load from build??
  pickpick.close()
  buff_used = buffer if buffer else 5.0  # so that our buffer gets properly used
  # now get the segments
  segnamelist = [wseg.name for wseg in workingstruct.segments.all()]  # get the name of the segments we care about
  if len(segnamelist) < 1:  # ruh roh
    raise ValueError("Your structure should have at least one segment. Please re-build your structure to include at least one segment.")
  allsegs = False
  model = next(pickfile.iter_models())
  for seg in model.iter_seg():
    if seg.segid in segnamelist:
      if not allsegs:
        allsegs = seg
      else:
        allsegs.extend(seg)
  diam = allsegs.diameter + buff_used
  return diam

def setup_solvation_dimensions(tdict, solvTask, postdata):
  """
  Sets up the dimensions of the solvation box based on the user's POST
  data from the solvation page. Also fills in the template dictionary
  with values related to the solvation structure.
  Returns the buffer used, if any.
  """
  buffer = False
  solvTask.solvation_structure = postdata['solvation_structure']
  tdict['solvation_structure'] = postdata["solvation_structure"]
  workingstruct = solvTask.workstruct
  if postdata['solvtype'] == 'sphere_solvation':
    solvTask.solvation_structure = "sphere"
    solvTask.spradius = postdata["spradius"]
    tdict["spradius"] = postdata["spradius"]
  elif postdata['solvtype'] == 'set_dimensions':
    solvTask.xtl_x = postdata['set_x']
    solvTask.xtl_y = postdata['set_y']
    solvTask.xtl_z = postdata['set_z']
  elif postdata['solvtype'] == 'determine_dimensions':
    dimensions = workingstruct.dimension
    ldim = list(dimensions)
    ldim.sort()
    buffer = float(postdata['buffer'])

    if solvTask.solvation_structure in ['cubic', 'rhdo']:
      solvTask.xtl_x = ldim[2] + (2 * buffer)
      solvTask.xtl_y = ldim[2] + (2 * buffer)
      solvTask.xtl_z = ldim[2] + (2 * buffer)
    elif solvTask.solvation_structure in ['tetra', 'hexa']:
      # the solvation input template does a coor orient, so it's safe to
      # assume that the X edge is the longest and the Y edge is the second
      # longest.
      solvTask.xtl_x = ldim[2] + (2 * buffer)
      solvTask.xtl_y = ldim[2] + (2 * buffer)
      solvTask.xtl_z = ldim[0] + (2 * buffer)
    else:
      raise ValueError("Unknown crystal structure {}. Please report this error to the CHARMMing administrators.".format(postdata["solvation_structure"]))
  else:
    raise AssertionError('Unknown type')
  if postdata["solvtype"] != "sphere_solvation":
    # there's a nicer way to do this if statement, I know it
    tdict['xtl_x'] = float(solvTask.xtl_x)
    tdict['xtl_y'] = float(solvTask.xtl_y)
    tdict['xtl_z'] = float(solvTask.xtl_z)
    # use spradius as the minimum safe sphere to cut around (replaces the old
    # stuff in calc_delete.inp.
    maxl = max(tdict['xtl_x'], tdict['xtl_y'], tdict['xtl_z'])
    tdict["spradius"] = maxl
  solvTask.save()
  return buffer

def solvate_tpl(request, solvTask, pTask):
  postdata = request.POST

  workingstruct = solvTask.workstruct
  os.chdir(workingstruct.structure.location)

  template_dict = {}
  # solvation-specific stuff
  solvTask.concentration = '0.0' # needs to be set here, will be set to proper value in ionization.py
  solvTask.structure = workingstruct
  # try:
  buffer = setup_solvation_dimensions(template_dict, solvTask, postdata)
  # except Exception as e:
  #   messages.error(request, str(e))
  #   return HttpResponseRedirect("/solvate")
  solvTask.save()

  if not solvTask.check_valid():
    return output.returnSubmission('Solvation', error='Invalid crystal structure definition')

  if workingstruct.topparStream:
    if 'toppar_water_ions.str' not in workingstruct.topparStream:
      workingstruct.topparStream += ' %s/toppar/toppar_water_ions.str' % charmming_config.data_home
  else:
    workingstruct.topparStream = '%s/toppar/toppar_water_ions.str' % charmming_config.data_home
  template_dict['angles'] = "%5.2f %5.2f %5.2f" % (solvTask.angles[0], solvTask.angles[1], solvTask.angles[2])

  if postdata['solvtype'] == 'determine_dimensions':
    # try:
    diam = get_diameter_from_pickle(workingstruct, buffer)
    # except Exception as e:
    #   messages.error(request, str(e))
    #   return HttpResponseRedirect("/buildstruct/")
  elif postdata['solvtype'] == 'sphere_solvation' or postdata["solvtype"] == "set_dimensions":
    diam = float(postdata["spradius"]) * 2
  try:
    right_box = get_closest_solvbox(diam)
  except Exception as e:
    messages.error(request, str(e))
  template_dict['num_waters'] = right_box[1]
  template_dict['box_name'] = right_box[2]

  # non-solvation-specific stuff
  template_dict = process_postdata(template_dict, solvTask, pTask, request)

  workingstruct.save() # probably isn't necessary -- being safe
  solvate_input_filename = setup_scripts(template_dict, solvTask, "solvation_template.inp")
  add_script(solvTask, "charmm", solvate_input_filename, charmming_config.default_charmm_nprocs)
  # change the status of the file regarding solvation
  solvTask.save()

  # this is solvation-specific
  # don't template further because neutralization is strange
  doneut = 'salt' in postdata and postdata['salt'] != 'none'
  if doneut:
    solvTask.action = 'neutralization'
    neutralize_tpl(solvTask, postdata)
    ctask = 'Solvation'
  else:
    start(solvTask)
    solvTask.save()
    workingstruct.save()
    ctask = 'Neutralization'

  # YP lessons stuff
  lesson_obj = get_lesson_object(workingstruct.structure.lesson_type)
  if lesson_obj:
    doLessonAct(workingstruct.structure, "onSolvationSubmit", solvTask, "")
  # end YP lesson stuff
  return output.returnSubmission(ctask)
