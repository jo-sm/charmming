from django.template.loader import get_template
from django.template import Context

from charmming import charmming_config
from charmming.models import Task
from charmming.utils.working_structure import build
from charmming.helpers import output
from charmming.utils.task import start, add_script

import os

def mutate_task_process(request, mt, oldws):
  """
  Handles templating based on the POST data from the Mutation form.
  """
  os.chdir(mt.workstruct.structure.location)
  template_dict = {}
  template_dict['topology_list'], template_dict['parameter_list'] = mt.workstruct.getTopparList()
  if mt.workstruct.topparStream:
    template_dict['tpstream'] = list(set(mt.workstruct.topparStream.split()))
  else:
    template_dict['tpstream'] = []
  template_dict['output_name'] = mt.workstruct.identifier + '-mutate'
  template_dict['MutResi'] = mt.MutResi
  template_dict['NewResn'] = mt.NewResn
  template_dict['MutSegi'] = mt.MutSegi
  template_dict['MutFile'] = mt.MutFile
  template_dict['BaseStruct'] = mt.workstruct.structure.name
  pTaskID = int(request.POST['ptask'])
  pTask = Task.objects.filter(id=pTaskID)[0]
  action = ""
  ws = mt.workstruct
  # struct = ws.structure
  if ws.isBuilt != 't':  # In theory this should work, but it doesn't. Not sure why.
    # isBuilt = False  # ??? It serves no purpose in minimization either...
    pTask = build(oldws, mt)
    pTaskID = pTask.id
  else:
    # isBuilt = True
    pTaskID = int(request.POST['ptask'])  # THis comes from the previous form...
  pTask = Task.objects.filter(id=pTaskID)[0]
  action = pTask.action
  template_dict['input_file'] = oldws.identifier + "-" + action
  # So now there's a reference to the parent task which makes the rest of charmming not break

  mt.parent = None
  mt.save()

  t = get_template('%s/mytemplates/input_scripts/mutation_template.inp' % (charmming_config.charmming_root))
  charmm_inp = output.tidyInp(t.render(Context(template_dict)))

  # user_id = ws.structure.owner.id
  mutate_filename = ws.structure.location + "/" + ws.identifier + "-mutation.inp"
  inp_out = open(mutate_filename, 'w')
  inp_out.write(charmm_inp)
  inp_out.close()
  add_script(mt, "charmm", mutate_filename, charmming_config.default_charmm_nprocs)
  start(mt)
  mt.save()
  mt.workstruct.save()
  return output.returnSubmission('Mutation')  # Will this break everything?
