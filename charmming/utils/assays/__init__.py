from charmming import charmming_config

import os

def get_dir(request):
  # username = request.user.username
  # u = User.objects.get(username=username)
  location = charmming_config.user_home + '/' + request.user.username + '/qsar/'
  if not os.path.exists(location):
    os.makedirs(location)
  return str(location)
