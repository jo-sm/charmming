import sys
import urllib.request
import urllib.parse
import urllib.error
import json
import xml.etree.ElementTree as ET
from concurrent.futures import ThreadPoolExecutor
from concurrent import futures
# get ZSI and wsdl2py --complexType 'http://pubchem.ncbi.nlm.nih.gov/pug_soap/pug_soap.cgi?wsdl'
# from .PUG_client import *

def get_sd_file(aids, sdname, remove_inconclusive):
  return 0
  # seen = set()
  # count = 0
  # writer = Chem.SDWriter(str(sdname))
  # for aid in aids:
  #     cid_to_act = dict()
  #     aid = aid.strip()
  #     if not aid:
  #         return count

  #     loc = PUGLocator()
  #     port = loc.getPUGSoap()

  #     req = InputAssaySoapIn()
  #     req.set_element_AID(int(aid))
  #     req.set_element_Columns('eAssayColumns_Complete')
  #     listKey = port.InputAssay(req).get_element_AssayKey()
  #     req = AssayDownloadSoapIn();
  #     req.set_element_AssayKey(listKey)
  #     req.set_element_AssayFormat('eAssayFormat_CSV')
  #     req.set_element_eCompress('eCompress_GZip')
  #     downloadKey = port.AssayDownload(req).get_element_DownloadKey()
  #     req = GetOperationStatusSoapIn()
  #     req.set_element_AnyKey(downloadKey)
  #     status = port.GetOperationStatus(req).get_element_status()
  #     while (status == 'eStatus_Queued' or status == 'eStatus_Running'):
  #         sleep(10)
  #         status = port.GetOperationStatus(req).get_element_status()
  #     if (status != 'eStatus_Success'):
  #         return count
  #     req = GetDownloadUrlSoapIn()
  #     req.set_element_DownloadKey(downloadKey)
  #     url = port.GetDownloadUrl(req).get_element_url()
  #     tmpcsv = tempfile.NamedTemporaryFile()
  #     (csvname, headers) = urllib.request.urlretrieve(url,tmpcsv.name)
  #     csvfh = gzip.open(csvname)

  #     header_line = csvfh.readline()
  #     #if header_line.startswith("PUBCHEM_SID,PUBCHEM_CID,PUBCHEM_ACTIVITY_OUTCOME"):
  #     cid_pos = 2
  #     if header_line.startswith("#,SID,CID,BioAssay_Source,RankScore,Outcome"):
  #         header_line = header_line.rstrip()
  #         header = header_line.split(",")
  #         for line in csvfh:
  #             line = line.rstrip();
  #             data = line.split(",")
  #             if len(data) != len(header):
  #                 continue
  #             cid_to_act[data[cid_pos]] = dict()
  #             for i,d in enumerate(data):
  #                 if i > 1:
  #                     cid_to_act[data[cid_pos]][header[i]] = d
  #     csvfh.close()

  #     listkeyfh = urllib.request.urlopen("http://pubchem.ncbi.nlm.nih.gov/rest/pug/assay/aid/"+aid+"/cids/JSON?list_return=listkey")
  #     jsonlistkey = listkeyfh.read()
  #     listkeyfh.close()
  #     listkey = json.loads(jsonlistkey)
  #     if "IdentifierList" not in listkey:
  #         return count
  #     if "ListKey" not in listkey["IdentifierList"]:
  #         return count
  #     key = listkey["IdentifierList"]["ListKey"]
  #     if not key:
  #         return count

  #     req = DownloadSoapIn();
  #     req.set_element_ListKey(key)
  #     req.set_element_eFormat('eFormat_SDF')
  #     req.set_element_eCompress('eCompress_GZip')
  #     downloadKey = port.Download(req).get_element_DownloadKey()
  #     req = GetOperationStatusSoapIn()
  #     req.set_element_AnyKey(downloadKey)
  #     status = port.GetOperationStatus(req).get_element_status()
  #     while (status == 'eStatus_Queued' or status == 'eStatus_Running'):
  #         sleep(10)
  #         status = port.GetOperationStatus(req).get_element_status()
  #     if (status != 'eStatus_Success'):
  #         return count
  #     req = GetDownloadUrlSoapIn()
  #     req.set_element_DownloadKey(downloadKey)
  #     url = port.GetDownloadUrl(req).get_element_url()

  #     tmpsdf = tempfile.NamedTemporaryFile()
  #     (filename, headers) = urllib.request.urlretrieve(url,tmpsdf.name)
  #     inf = gzip.open(filename)
  #     gzsuppl = Chem.ForwardSDMolSupplier(inf)
  #     for m in gzsuppl:
  #         if m is not None:
  #             if not m.HasProp("PUBCHEM_COMPOUND_CID"):
  #               continue
  #             cid =  m.GetProp("PUBCHEM_COMPOUND_CID")
  #             if cid in seen:
  #                 continue
  #             seen.add(cid)
  #             if remove_inconclusive and cid_to_act[cid]["Outcome"] != "Active" and cid_to_act[cid]["Outcome"] != "Inactive":
  #                 continue
  #             m.SetProp("PUBCHEM_AID",str(aid))
  #             for k,v in list(cid_to_act[cid].items()):
  #                 m.SetProp(str(k),str(v))
  #             writer.write(m)
  #             count += 1
  #     tmpsdf.close()
  # return count


def get_aids(term, start=0):
  eufh = urllib.request.urlopen("http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pcassay&term=" + term + "&RetStart=" + str(start))
  xmlstr = eufh.read()
  root = ET.fromstring(xmlstr)
  aids = [aid.text for aid in root.findall("./IdList/Id")]
  step = root.find("./RetMax").text
  total = root.find("./Count").text
  return step, total, aids

def get_description(aid):
  try:
    descfh = urllib.request.urlopen("http://pubchem.ncbi.nlm.nih.gov/rest/pug/assay/aid/" + aid + "/summary/JSON")
    jsonstr = descfh.read()
    jsonobj = json.loads(jsonstr)
    name = jsonobj["AssaySummaries"]["AssaySummary"][0]["Name"]
    num = jsonobj["AssaySummaries"]["AssaySummary"][0]["CIDCountAll"]
  except KeyError:
    return None
  return (name, num, aid)

def get_list_aids(query, start=0):
  if query is None:
    choices = []
    return choices
  (step, total, aids) = get_aids(query, start)
  choices = []
  with ThreadPoolExecutor(len(aids)) as executor:
    future_choices = set(executor.submit(get_description, aid) for aid in aids)
  for future in futures.as_completed(future_choices):
    if future.result() is not None:
      (name, num, aid) = future.result()
      name += " (" + str(num) + ")"
      choices.append((aid, name))
  choices.sort()
  return (step, total, choices)

def get_url_base():
  return "http://pubchem.ncbi.nlm.nih.gov/assay/assay.cgi?aid="

def print_aid(aid):
  print(aid, get_description(aid))

if __name__ == '__main__':
  # query = sys.argv[1]
  sdname = sys.argv[2]
  remove_inconclusive = False

  # (step,total,choices) = get_list_aids(query)

  # aids = [c[0] for c in choices]
  aids = [sys.argv[1]]
  num_mols = get_sd_file(aids, sdname, remove_inconclusive)
