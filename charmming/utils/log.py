from termcolor import colored

import datetime
import inspect
import logging

def log_wrapper(name, level='warning'):
  logger = logging.getLogger(name)
  log_level = log_level_name_to_num(level)
  logger.setLevel(log_level)

  def inner(*args, **kwargs):
    kwargs['logger'] = name

    return log(*args, **kwargs)

  return inner

def log(*args, **kwargs) -> None:
    frame = inspect.currentframe()
    frame_info = inspect.getouterframes(frame, context=2)
    caller = frame_info[1][3]

    logger_name = kwargs.get('logger') or 'charmming'
    logger = logging.getLogger(logger_name)
    handler = logging.StreamHandler()

    if not logger.hasHandlers():
      logger.addHandler(handler)

    log_arr = [str(arg) for arg in args]
    level_name = kwargs.get('level') or 'info'
    level = log_level_name_to_num(level_name)

    if level_name == 'debug':
        level_color = 'yellow'
    elif level_name == 'info':
        level_color = 'blue'
    elif level_name == 'warning':
        level_color = 'magenta'
    elif level_name == 'critical':
        level_color = 'red'

    time = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f +0000')

    time_str = colored(time, 'grey')
    level_str = colored(level_name, level_color)
    caller_str = colored(caller, 'green')

    logger.log(level, "{} {} ({}) {}".format(time_str, level_str, caller_str, " ".join(log_arr)))

def log_level_name_to_num(name: str) -> int:
    allowed_levels = {
        'debug': 10,
        'info': 20,
        'warning': 30,
        'critical': 40
    }

    if name not in list(allowed_levels.keys()):
        name = 'info'

    return allowed_levels[name]
