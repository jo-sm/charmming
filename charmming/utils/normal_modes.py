from django.template.loader import get_template
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.template import Context

from charmming.models import NmodeTask
from charmming.models import Task
from charmming.helpers import output
from charmming import charmming_config
from charmming.utils.task import start, add_script
from charmming.utils.lesson import get_lesson_object, doLessonAct
from charmming.utils.script import process_postdata, setup_scripts
from charmming.utils.structure.properties import get_atom_count

import re
import os

def applynma_tpl(request, workstruct, pTaskID, nmTask):
  """
  Generates the Normal modes script and runs it
  """

  # get the parent task
  pTask = Task.objects.get(id=pTaskID)

  # template dictionary passes the needed variables to the template
  template_dict = {}
  # nmode-specific stuff
  template_dict['nma'] = request.POST['nma']
  nmTask.nmodes = int(request.POST["num_normalmodes"])
  template_dict['numnormalmodes'] = nmTask.nmodes
  # save model
  if template_dict['nma'] == 'useenm':
    nmTask.type = 2
    nmTask.rcut = float(request.POST['rcut'])
    nmTask.kshort = float(request.POST['kshort'])
    nmTask.klong = float(request.POST['klong'])
    template_dict['rcut'] = nmTask.rcut
    template_dict['kshort'] = nmTask.kshort
    template_dict['klong'] = nmTask.klong
  else:
    nmTask.type = 1
  template_dict['identifier'] = workstruct.identifier
  # If need be, print out trajectories for the modes the user requested.
  template_dict['gen_trj'] = "gen_trj" in request.POST
  template_dict['num_trjs'] = "num_trjs" in request.POST
  if "gen_trj" in request.POST and "num_trjs" in request.POST:
    # movies are liable to be broken for the time being
    nmTask.nma_movie_req = True
    try:
      ntrjs = int(request.POST['num_trjs'])
    except:
      ntrjs = 5
    if ntrjs > 20:
      ntrjs = 20
    if ntrjs > nmTask.nmodes:
      ntrjs = nmTask.nmodes
    template_dict['ntrjs'] = str(ntrjs)
    nmTask.num_trjs = ntrjs
  else:
    nmTask.nma_movie_req = False
  if template_dict["nma"] == "usevibran":
    if get_atom_count(workstruct) > charmming_config.max_nma_atoms:
      messages.error(request, "You may only do NMA on structures with less than %d atoms" % charmming_config.max_nma_atoms)
      return HttpResponseRedirect("/normalmodes/")
  nmTask.nmodes = int(request.POST['num_normalmodes'])
  if template_dict["nma"] != "usevibran" and "useqmmm" in request.POST:
    messages.error(request, "Cannot use QM/MM with ENM.")
    return HttpResponseRedirect("/normalmodes/")

  # non-nmode-specific stuff
  try:
    template_dict = process_postdata(template_dict, nmTask, pTask, request)
  except Exception as e:
    messages.error(request, str(e))
    return HttpResponseRedirect("/normalmodes/")

  nma_filename = setup_scripts(template_dict, nmTask, "applynma_template.inp")
  # if nmTask.useqmmm == 'y' and modelType == "oniom":
  if nmTask.useqmmm == 'y':
    add_script(nmTask, 'charmm-mscale', nma_filename, charmming_config.default_mscale_nprocs)
  else:
    add_script(nmTask, 'charmm', nma_filename, charmming_config.default_charmm_nprocs)
  nmTask.save()

  # change the status of the file regarding minimization
  if "gen_trj" in request.POST and "num_trjs" in request.POST:
    nmTask.make_nma_movie = True
    nmTask.save()
    return makeNmaMovie_tpl(workstruct, request.POST, int(request.POST['num_trjs']), template_dict['nma'], nmTask, pTask)  # I have no idea if pTask is stored in nmTask, so let's be safe
  else:
    start(nmTask)

  lesson_obj = get_lesson_object(workstruct.structure.lesson_type)
  if lesson_obj:
    doLessonAct(workstruct.structure, "onNMASubmit", nmTask, None)

  workstruct.save()
  return output.returnSubmission('Normal modes')

# makes NMA Movie
def makeNmaMovie_tpl(workstruct, postdata, num_trjs, typeoption, nmm, ptask):
  # template dictionary passes the needed variables to the template
  template_dict = {}
  template_dict['topology_list'], template_dict['parameter_list'] = workstruct.getTopparList()
  template_dict['orig_coords'] = workstruct.identifier + "-" + ptask.action  # Only inp/out files don't match the task.action scheme. Careful with dsfdocking though.
  template_dict['filebase'] = workstruct.identifier
  template_dict['typeoption'] = typeoption
  template_dict['num_trjs'] = str(num_trjs)
  if workstruct.topparStream:
    template_dict['tpstream'] = workstruct.topparStream.split()
  else:
    template_dict['tpstream'] = []

  # Run the script through charmm, this is not done under job queue at the moment
  # because the PDBs must be appended together after the above script has been run.
  # Once the DAG and query stuff has been implemented, this is how the following code should
  # be changed to
  # 1. Create a new field in the Structure object called movie_status
  # 2. In the status method, check to see if movie_status is done
  # 3. If it is done, call the method combinePDBsForMovie(...): right below the following code.

  t = get_template('%s/mytemplates/input_scripts/makeNmaMovie_template.inp' % charmming_config.charmming_root)
  charmm_inp = output.tidyInp(t.render(Context(template_dict)))

  movie_filename = workstruct.structure.location + "/" + workstruct.identifier + '-nmamovie.inp'
  movie_handle = open(movie_filename, 'w')
  movie_handle.write(charmm_inp)
  movie_handle.close()
  if nmm.useqmmm == 'y' and nmm.modelType == "oniom":
    add_script(nmm, "charmm-mscale", movie_filename, charmming_config.default_mscale_nprocs)
  else:
    add_script(nmm, 'charmm', movie_filename, charmming_config.default_charmm_nprocs)
#    nmm.scripts += ',' + movie_filename

  # nmm.nma_movie_status = "<span style='color:33CC00'>Done</span>"
  nmm.workstruct.save()
  start(nmm)
  nmm.save()
  return output.returnSubmission('Normal modes')

# pre: Requires a PDB object
# Combines all the smaller PDBs make in the above method into one large PDB that
# jmol understands
# type is nma
# Ignore this for now...there's a better one attached to the nmTask
def combineNmaPDBsForMovie(file):
  nmm = NmodeTask.objects.filter(pdb=file, selected='y')[0]
  ter = re.compile('TER')
  remark = re.compile('REMARK')
  # movie_handle will be the final movie created
  # mini movie is the PDBs which each time step sepearated into a new PDB
  trj_num = 0
  continueit = True
  while(continueit):
    try:
      pdbno = trj_num + 1
      os.stat(file.location + 'new_' + file.stripDotPDB(file.filename) + '-mtraj_' + str(pdbno) + '.trj')
      trj_num += 1
    except:
      continueit = False
  currtrjnum = 1
  while currtrjnum < (trj_num + 1):
    movie_handle = open(file.location + 'new_' + file.stripDotPDB(file.filename) + "-nma-mainmovie-" + str(currtrjnum) + ".pdb", 'a')
    for i in range(12):
      i = i + 1
      minimovie_handle = open(file.location + "new_" + file.stripDotPDB(file.filename) + "-nmapremovie" + repr(i) + "-" + str(currtrjnum) + ".pdb", 'r')
      movie_handle.write('MODEL ' + repr(i) + "\n")
      for line in minimovie_handle:
        if(not remark.search(line) and not ter.search(line)):
          movie_handle.write(line)
      movie_handle.write('ENDMDL\n')
      minimovie_handle.close()
      os.remove(file.location + "new_" + file.stripDotPDB(file.filename) + "-nmapremovie" + repr(i) + "-" + str(currtrjnum) + ".pdb")
    currtrjnum += 1

  nmm.nma_movie_status = "<span class='done'>Done</span>"  # where do we even use this...?
  nmm.make_nma_movie = False
  nmm.save()
  file.save()
