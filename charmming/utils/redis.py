from django.conf import settings

import redis

__all__ = [
  'get_instance'
]

pool = redis.ConnectionPool(host=settings.REDIS_HOST, port=6379, db=0)

def get_instance():
  return redis.Redis(connection_pool=pool)
