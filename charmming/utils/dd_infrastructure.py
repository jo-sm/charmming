from charmming.models import DDJob, DDProject

import datetime

def GetEnergyFromSeedEval(path, posename):
  energies_file = open(path + "/seed_energies.out", 'r')
  ligand_energies = []
  for line in energies_file:
    ligand_energies = line.split()
    if ligand_energies[0] == posename + "_min.mol2":
      return ligand_energies[1], ligand_energies[2], ligand_energies[3]

  return 0

def GetEnergyFromFFLDEval(path, posename):
  energies_file = open(path + "/ffld_energies.out", 'r')
  ligand_energies = []
  for line in energies_file:
    ligand_energies = line.split()
    if ligand_energies[0] == posename + "_min.mol2":
      return ligand_energies[1], ligand_energies[2], ligand_energies[3]

  return 0

def GetEnergyFromCHARMMMini(path, posename):
  energies_file = open(path + "/charmm_mini_energies.out", 'r')
  ligand_energies = []
  for line in energies_file:
    ligand_energies = line.split()
    if ligand_energies[0] == posename:
      return ligand_energies[1], ligand_energies[2], ligand_energies[3]

  return 0

def GetEnergyFromFLEAMol(filename):
  mol2file = open(filename, 'r')
  energy = "9999"
  for line in mol2file:
    if line.find(":") > 0:
      energy = line[line.find(":") + 1:].strip()
      return energy

  return energy

def ProjectExists(projectname, user):
  try:
    projects = DDProject.objects.filter(name=projectname, owner=user)
    if projects:
      return True
    else:
      return False
  except:
    return False

def getNewJobOwnerIndex(user):

  try:
    last_job = DDJob.objects.filter(owner=user).order_by('-id')[0]
    next_job_owner_index = int(last_job.job_owner_index) + 1

  except:
    next_job_owner_index = 1

  return next_job_owner_index

class objProjectProteinConformation():
  def __init__(self, **kwargs):
    self.conformation_id = kwargs.get('conformation_id') or 0
    self.project_conformation_id = kwargs.get('project_conformation_id') or 0
    self.conformation_name = kwargs.get('conformation_name') or ""
    self.protein_pdb_code = kwargs.get('protein_pdb_code') or ""

class objDDJob():
  def __init__(self, **kwargs):
    self.id = kwargs.get('id') or 0
    self.scheduler_id = kwargs.get('scheduler_id') or 0
    self.owner_index = kwargs.get('owner_index') or 0
    self.description = kwargs.get('description') or ""
    self.status = kwargs.get('status') or ""
    self.queued = kwargs.get('queued') or datetime.datetime.now()
    self.waited = kwargs.get('waited') or datetime.datetime.now()
    self.runtime = kwargs.get('runtime') or datetime.datetime.now()
    self.totaltime = kwargs.get('totaltime') or datetime.datetime.now()
    self.output_file = kwargs.get('output_file') or ""
    self.target_conformations_file_list = kwargs.get('target_conformations_file_list') or []
    self.ligands_file_list = kwargs.get('ligands_file_list') or []
    self.results_file_list = kwargs.get('results_file_list') or []

def mergeLists(list1, list2):

  for item in list2:
    list1.append(item)

  return list1

class objFile():
  id = ""
  fullpath = ""
  name = ""
  tag = ""
