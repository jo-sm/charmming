from django.shortcuts import render_to_response

from charmming.models import PropkaTask
from charmming.utils.task import start, add_script
from charmming.helpers import output

import os
import subprocess

"""
This file contains utility functions for running PROPKA on structures, in order
to abstract some stuff out of dossier.views, and improve propka.views functionality.
"""

def run_propka_base_structure(struct):
  """
  Runs PROPKA on the file denoted by struct.original_name,
  previously converted by convert_base_pdb_to_propka.
  """
  base_path = os.path.join(struct.location, struct.original_name)
  path_to_orig_pdb = base_path + ".pdb"
  path_to_propka_pdb = base_path + "-propka.pdb"
  if not os.path.exists(path_to_propka_pdb):
    if not os.path.exists(path_to_orig_pdb):
      raise ValueError("Could not find original PDB for structure with title {}. Maybe it was deleted?".format(struct.title))
    convert_pdb_to_propka(path_to_orig_pdb) # this will convert and output the file we need
  os.chdir(struct.location) # Important - PROPKA only puts its output in cwd!
  subprocess.call(["propka31", path_to_propka_pdb])

def convert_pdb_to_propka(filepath):
  """
  Converts a PDB file to PROPKA format by opening it, replacing HSD,HSE and HSP with HIS,
  then writes a "-propka.pdb" file for reading by propka.
  """
  # Main fix we need to do for PROPKA is change HSD/HSE/HSP to HIS; it can only understand HIS
  bad_his_file = open(filepath, "r")
  bad_his = bad_his_file.read()
  propka_out = open(filepath.replace(".pdb", "-propka.pdb"), "w")
  propka_out.write(bad_his.replace("HSE", "HIS").replace("HSD", "HIS").replace("HSP", "HIS"))
  bad_his_file.close()
  propka_out.close()

def calcpropka(request, ws, pTask):
  if not request.user.is_authenticated():
    return render_to_response('html/loggedout.html')
  # tdict = {}
  # set up files for PROPKA calculation, store file for later display
  os.chdir(ws.structure.location)  # else PROPKA can't get permissions right
  inp_file_location = "%s-%s.pdb" % (ws.identifier, pTask.action)
  # convert to PROPKA format so we get report on HIS protonation states
  convert_pdb_to_propka(inp_file_location)
  inp_file_location = inp_file_location.replace(".pdb", "-propka.pdb")
  # out_file_location = inp_file_location.replace(".pdb", ".pka")
  protask = PropkaTask()
  protask.setup(ws)
  protask.active = 'y'
  protask.action = 'propka'
  protask.parent = pTask
  protask.modifies_coordinates = False
  protask.save()

  arf = open("/tmp/parent-test.txt", "w")
  arf.write(protask.parent.action)
  arf.close()
  add_script(protask, "propka", inp_file_location, 1)
  start(protask)
  protask.save()
  protask.workstruct.save()
  return output.returnSubmission("PROPKA")
