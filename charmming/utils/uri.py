def data_uri(str):
  pieces = str.split(';')
  encoded = pieces[-1].split(',')[-1]
  mimetype = pieces[0].split(':')[1]

  return mimetype, encoded
