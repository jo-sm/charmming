from charmming import charmming_config
from charmming.models import Segment

from charmming.utils.uri import data_uri
from charmming.utils.segment import valid_first_patch, valid_last_patch, get_default_first_patch, get_default_last_patch
from charmming.utils.number import make_float

from pychm3.io.pdb import PDB

import base64

def create_child(structure, options):
  """
  Creates a child structure of given `structure` using
  given `options`.
  """

  valid_types = [ 'aa', 'bln', 'go' ]

  if not options.get('segments'):
    # At least one segment is required
    return None

  if len(options['segments']) == 0:
    return None

  if not options.get('model'):
    return None

  # Ensure that the model exists
  pychm_structure = structure.get_pychm()

  try:
    pychm_structure[options['model']]
  except KeyError:
    return None

  model = options['model']
  segments_data = options['segments']
  segment_names = list(segments_data.keys())
  segments = structure.segment_set.all(name__in=segment_names, model=model)

  child_type = options.get('type', 'aa')

  if child_type not in valid_types:
    # Must be a valid structure type
    return None

  child_structure = structure
  child_structure.pk = None
  child_structure.id = None
  child_structure.model = model
  child_structure.name = '{} Child'.format(structure.name)
  child_structure.save()

  child_structure.save_structure_file(pychm_structure[model].to_str(format='pdborg'))

  child_structure.segment_set.clear()

  if child_type == 'aa':
    child_structure = create_aa_child(child_structure, segments, segments_data)
  elif child_type == 'bln':
    pass
  elif child_type == 'go':
    pass
  else:
    return None

  if not child_structure:
    return None

  return child_structure

def create_aa_child(child_structure, segments, data):
  segments_data = data.get('segments')
  new_segments = []
  rtfs = {}
  prms = {}

  for segment in segments:
    # Creating a "copy"
    pychm_segment = segment.get_pychm_segment()

    segment.pk = None
    segment.id = None

    name = segment.name
    segment_data = segments_data[name]

    if segment_data.get('rtf') and segment_data.get('prm'):
      segment.user_supplied_toppar = True

      rtf_raw = segment_data.get('rtf')
      mimetype, file_encoded = data_uri(rtf_raw)
      rtf_encoded = base64.b64decode(file_encoded)
      rtf = rtf_encoded.decode('utf-8')
      rtfs[segment.name] = rtf

      prm_raw = segment_data.get('prm')
      mimetype, file_encoded = data_uri(prm_raw)
      prm_encoded = base64.b64decode(file_encoded)
      prm = prm_encoded.decode('utf-8')
      prms[segment.name] = prm

    if segment_data.get('first_patch'):
      if not valid_first_patch(segment_data['first_patch']):
        segment_data['first_patch'] = get_default_first_patch(pychm_segment)

      segment.first_patch = segment_data['first_patch']

    if segment_data.get('last_patch'):
      if not valid_last_patch(segment_data['last_patch']):
        segment_data['last_patch'] = get_default_last_patch(pychm_segment)

      segment.last_patch = segment_data['last_patch']

    new_segments.append(segment)

  # Create the child structure
  child_structure.segment_set.add(*new_segments)

  for segment in new_segments:
    # Handle protonation and disulfide patching
    # This happens here, since we need to add a patch to the "patch set"

    # Handle protonation patching
    if segment_data.get('protonation'):
      for residue, name in list(segment_data['protonation'].items()):
        if name in ['hsd', 'lys', 'glu', 'asp']:
          # These are default patches, so they can be skipped
          continue

        patch = segment.patch_set.create()
        patch.structure = child_structure
        patch.name = name
        patch.segment_residue = "{}".format(residue)
        patch.save()

    # Handle disulfide patching
    if segment_data.get('disulfide'):
      for patch_data in list(segment_data.get('disulfide')):
        segment_1 = patch_data.get('segment_1')
        segment_1_residue = patch_data.get('segment_1_residue')
        segment_2 = patch_data.get('segment_2')
        segment_2_residue = patch_data.get('segment_2_residue')

        patch = child_structure.patch_set.create()
        patch.structure = child_structure
        patch.name = 'disu'
        patch.segment_residue = '{} {} {} {}'.format(segment_1, segment_1_residue, segment_2, segment_2_residue)
        patch.save()

    # Add any rtfs and prms that the user supplied
    if rtfs.get(segment.name):
      path = segment.add_file('uploaded.rtf', rtfs[segment.name])
      segment.rtf_list = [ path ]

      path = segment.add_file('uploaded.prm', prms[segment.name])
      segment.prm_list = [ path ]

      segment.save()

  return child_structure

def create_bln_child(child_structure, segments, data):
  bln_options = {
    'helix_bond_force': make_float(data.get('helix_bond_force', 3.5), 3.5),
    'helix_angle_force': make_float(data.get('helix_angle_force', 8.37), 8.37),
    'b_sheet_bond_force': make_float(data.get('b_sheet_bond_force', 3.5), 3.5),
    'b_sheet_angle_force': make_float(data.get('b_sheet_angle_force', 8.37), 8.37),
    'coil_bond_force': make_float(data.get('coil_bond_force', 2.5), 2.5),
    'coil_angle_force': make_float(data.get('coil_angle_force', 5.98), 5.98),
  }

  child_structure.cg_model_type = 'bln'
  child_structure.save()

  associate_segments_with_cg_structure(child_structure, segments)
  handle_cg(child_structure, 'bln', **bln_options)

  return child_structure

def create_go_child(child_structure, segments, data):
  valid_contact_types = [ 'mj', 'kgs', 'bt' ]
  contact_type = data.get('contact_type', 'mj')

  if contact_type not in valid_contact_types:
    return None

  go_options = {
    'contact_type': contact_type,
    'calculate_nscale': data.get('calculate_nscale', False),
    'nscale': make_float(data.get('nscale', 1.00), 1.00),
    'nscale_temp': data.get('nscale_temp', 340),
    'contact_radius': make_float(data.get('contact_radius', 4.5), 4.5),
    'bond_force_const': make_float(data.get('bond_force_const', 50.0), 50.0),
    'angle_force_const': make_float(data.get('angle_force_const', 30.0), 30.0),
    'dihedral_alpha_1_helix': make_float(data.get('dihedral_alpha_1_helix', 0.30), 0.30),
    'dihedral_310_1_helix': make_float(data.get('dihedral_310_helix', 0.30), 0.30),
    'dihedral_helical_1': make_float(data.get('dihedral_helical_1', 0.55), 0.55),
    'dihedral_alpha_3_helix': make_float(data.get('dihedral_alpha_3_helix', 0.15), 0.15),
    'dihedral_310_3_helix': make_float(data.get('dihedral_310_3_helix', 0.15), 0.15),
    'dihedral_unhelical_3': make_float(data.get('dihedral_unhelical_3', 0.275), 0.275),
    'h_bond_alpha_helix': make_float(data.get('h_bond_alpha_helix', -0.25), -0.25),
    'h_bond_three_10_helix': make_float(data.get('h_bond_three_10_helix', -0.25), -0.25),
    'h_bond_no_helix': make_float(data.get('h_bond_no_helix', -0.50), -0.50),
    'non_attractive_lj': make_float(data.get('non_attractive_lj', 1e-12), 1e-12),
    'backbone_sidechain_lj': make_float(data.get('backbone_sidechain_lj', -0.37), -0.37),
  }

  child_structure.cg_model_type = 'go'
  child_structure.save()

  # Associate segments with new structure
  associate_segments_with_cg_structure(child_structure, segments)
  handle_cg(child_structure, 'go', **go_options)

  if go_options.get('calculate_nscale'):
    pass
    # TODO: Add this as a job instead of a separate command
    # cmd = '%s/find_nscale_async.py %s %s %f %f' % (charmming_config.data_home, working_structure.structure.location, basefname, cg_model.nScale, kwargs.get('nscale_temp'))
    # os.system(cmd)

  return child_structure

def associate_segments_with_cg_structure(structure, segments):
  """
  Adds segments to the CG structure.

  If there is a "pro" segment, the segment is copied and some segment properties are changed
  to work with a CG structure.
  """

  segments_list = []
  model_type = structure.cg_model_type

  for segment in segments:
    if not segment.type == 'pro':
      continue

    copy = segment
    copy.pk = None
    copy.id = None

    copy.name = copy.name.replace('-pro', '-{}'.format(model_type))
    copy.type = model_type

    copy.save()

    segments_list.append(copy)

  structure.segment_set.clear()
  structure.segment_set.add(*segments)

  return True

def handle_cg(structure, segments, model_type, **kwargs):
  pdb = structure.get_pychm_pdb()
  model = pdb[structure.model]

  segment_ids = [segment.name for segment in segments]

  structure_parts = []

  for segment in model.segments:
    if segment.id in segment_ids:
      structure_parts.append(segment.to_str(format='charmm', ter=True, end=False))

  structure_parts = structure_parts + [ '', 'END', '' ]
  cg_structure = '\n'.join(structure_parts)

  model = PDB(cg_structure)[0]

  if model_type == 'go':
    cg_model = model.become_cg('go')
    cg_model.nScale = kwargs.get('nscale')
    cg_model.contactSet = kwargs.get('contact_type')
    cg_model.kBond = kwargs.get('bond_force_const')
    cg_model.kAngle = kwargs.get('angle_force_const')
    cg_model.contactrad = kwargs.get('contact_radius')

  elif model_type == 'bln':
    cg_model = model.become_cg('bln')
    cg_model.kBondHelix = kwargs.get('helix_bond_force')
    cg_model.kBondSheet = kwargs.get('b_sheet_bond_force')
    cg_model.kBondCoil = kwargs.get('coil_bond_force')
    cg_model.kAngleHelix = kwargs.get('helix_angle_force')
    cg_model.kAngleSheet = kwargs.get('b_sheet_angle_force')
    cg_model.kAngleCoil = kwargs.get('coil_angle_force')

  cg_model.run_stride()

  pdb = cg_model.get_pdb()
  rtf = cg_model.get_rtf()
  prm = cg_model.get_prm()

  rtf_path = structure.save_file("{}.rtf".format(model_type), rtf)
  prm_path = structure.save_file("{}.prm".format(model_type), prm)

  for segment in structure.segment_set.all():
    segment.rtf_list = [ rtf_path ]
    segment.prm_list = [ prm_path ]
    segment.save()

  structure.save_structure_file(pdb)

  return True

"""
BEGIN OLD UTILS
"""

def attach_seg_to_struct(new_molecule, struct, not_custom):
  """
  Attaches a segment new_molecule to a Structure object.
  The custom flag is whether the new thing that is being attached exists on PDB.org;
  how this is relevant is uncertain.
  """
  if (not_custom):
    getSegs(new_molecule, struct, auto_append=True)
  else:
    getSegs(new_molecule, struct, auto_append=False)

def getSegs(Molecule, Struct, auto_append):
  # auto_append is never used so I will use it for the custom ligand build...
  # True means normal/PDB.org build, False means custom build
  Struct.save()
  # Why do we save before doing anything?
  sl = []
  sl.extend(Segment.objects.filter(structure=Struct))
  for seg in Molecule.iter_seg():
    reslist = seg.iter_res()
    firstres = next(reslist).resName
    try:
      # secondres = next(reslist)
      resName = "N/A"
    except StopIteration:  # End of iterator, returns exception instead of null
      resName = firstres

    newSeg = Segment()
    newSeg.structure = Struct
    newSeg.is_working = 'n'
    newSeg.name = seg.segid
    newSeg.type = seg.segType
    newSeg.is_custom = not(auto_append)  # False if auto_append is True, and vice-versa.
    newSeg.resName = resName  # This is for buildstruct page and other misc uses

    # foo = [x.name for x in sl]
    # bar = [x.structure for x in sl]
    # the following checks for whether the segment you're "getting" is already in the structure, or whether it's not. THis is used for custom ligands
    # such that you can add ligands to an already-existing structure without duplicating its segments
    # If the if test fails, the save is skipped - newSeg gets stuff associated to it but doesn't get committed to the database.
    if newSeg.name in [x.name for x in sl] and newSeg.structure in [x.structure for x in sl]:
      # If a segment with that name already exists in that structure, skip it and keep going.
      continue

    if seg.segType in ['pro']:
      newSeg.rtf_list = charmming_config.data_home + '/toppar/' + charmming_config.default_pro_top
      newSeg.prm_list = charmming_config.data_home + '/toppar/' + charmming_config.default_pro_prm
    elif seg.segType in ['rna', 'dna']:
      newSeg.rtf_list = charmming_config.data_home + '/toppar/' + charmming_config.default_na_top
      newSeg.prm_list = charmming_config.data_home + '/toppar/' + charmming_config.default_na_prm
    elif seg.segType == 'good':
      newSeg.rtf_list = ''
      newSeg.prm_list = ''
      newSeg.stream_list = charmming_config.data_home + '/toppar/toppar_water_ions.str'

    # get list of potential REDOX only segments. These segments will only
    # be used for REDOX calculations and otherwise aren't part of the final
    # constructed model. NB, we're going to assume that the first model is
    # canonical for these.
    if seg.segType == 'bad':
      ok = True
      for res in seg.iter_res():
        if res.resName not in ['fs4', 'sf4']:
          ok = False
          break
      if ok:
        newSeg.fes4 = True

    # set default patching type
    newSeg.set_default_patches(firstres)
    newSeg.save()
    # Should we save Struct again? We don't change anything here.
