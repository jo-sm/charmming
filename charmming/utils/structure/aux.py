import re
from django.shortcuts import render_to_response
from charmming.models import WorkingFile

def checkNterPatch(file, segment):

  # custom sequences must be handled differently
  if segment == "sequ-pro":
    sequ_filename = "new_" + file.stripDotPDB(file.filename) + "-sequ-pro.pdb"
    sequ_handle = open(file.location + sequ_filename, 'r')
    sequ_line = sequ_handle.read()
    sequ_line.strip()
    resid = sequ_line.split(' ')[0]
  else:
    # we can't count on the -final files to have been created yet, but the parsed PDB
    # files will do OK
    try:
      segpdb = open(file.location + "new_" + file.stripDotPDB(file.filename) + "-" + segment + ".pdb", "r")
    except:
      # NTER is gonna be the default if something goes wrong -- lame, huh?
      return "nter"
    if not segpdb:
      return "nter"
    # read until the first line beginning with atom (this should be the very first line of the
    # the file, but this is defensive programming, kiddos.
    for line in segpdb.readlines():
      if line.startswith('ATOM'):
        break
    segpdb.close()
    try:
      resid = line[18:21]
    except:
      # should probably throw an error instead
      return "nter"

  # figured out the first resid in this segment, now let's decide what patch it needs
  resid = resid.upper()
  if resid == "GLY":
    return "glyp"
  elif resid == "PRO":
    return "prop"
  else:
    return "nter"

# Why does this show up twice? This is in structure.views as well...
def parseEnergy(file, output_filename):
  outfp = open(file.location + output_filename, 'r')
  ener = re.compile('ENER ENR')
  extern = re.compile('EXTERN')
  initiate = 0
  # the second extern marks the end of the energy output
  extern_occurance = 0
  energy_lines = ''
  for line in outfp:
    if(extern_occurance == 2 or (initiate > 0 and line.strip().startswith('CHARMM>'))):
      if(line.strip().startswith('CHARMM>')):
        break
      energy_lines += line.lstrip()
      break
    if(ener.search(line) or initiate > 0):
      if(initiate == 0):
        initiate = 1
      energy_lines += line.lstrip()
    if(extern.search(line)):
      extern_occurance += 1
  outfp.close()
  writefp = open(file.location + 'energy-' + file.stripDotPDB(file.filename) + '.txt', 'w')
  writefp.write(energy_lines)
  writefp.close()
  return render_to_response('html/displayenergy.html', {'linelist': energy_lines})

# TODO: Add os.stat checks for failed QC tasks. However, the scheduler should respond with "Failed" before then.
# I wanted to put this into atomselection_aux but it failed.
# Layers is there as input because I can't hit the DB from here because it would cause a circular import (since selection.models imports from here)
def saveQCWorkingFiles(task, basepath):  # Takes a task as input and saves its QC input/output files, if it is in fact a QC task. This avoids rewriting the same code in 3 places.
  if task.useqmmm == 'y':
    if task.modelType == "qmmm":  # This one works. Don't wipe it.
      wfqcin = WorkingFile()
      qcbasepath = basepath.replace(task.workstruct.identifier + "-" + task.action, "qchem-" + task.workstruct.identifier + task.action)
      path = qcbasepath + ".inp"
      try:
        WorkingFile.objects.get(task=task, path=path)
      except:
        wfqcin.task = task
        wfqcin.path = path
        wfqcin.canonPath = wfqcin.path
        wfqcin.type = "inp"
        wfqcin.description = "QChem input"
        wfqcin.save()

      wfqcout = WorkingFile()
      path = qcbasepath + ".out"
      try:
        WorkingFile.objects.get(task=task, path=path)
      except:
        wfqcout.task = task
        wfqcout.path = path
        wfqcout.canonPath = wfqcout.path
        wfqcout.type = "out"
        wfqcout.description = "QChem output"
        wfqcout.save()
    else:
      # Make workingfiles for inputs, then make the output files by extrapolating from them.
       # allcurrentwfs = WorkingFile.objects.filter(task=task) #Since we attach them to this task to begin with, it should work fine
       # subsysbasepath = basepath.replace(task.workstruct.identifier+"-"+task.action,"") + "subsys/"
       # for wf in allcurrentwfs: #Now do checks, and create new files
       #     if wf.description.startswith("QChem") or wf.description.startswith("Subsystem"): #Thankfully we hardcode these.
       #         new_wf = WorkingFile()
       #         new_path = wf.path.replace("inp","out")
       #         try:
       #             WorkingFile.objects.get(task=task,path=new_path)
       #         except:
       #             new_wf.task = task
       #             new_wf.path = path
       #             new_wf.canonPath = new_wf.path
       #             new_wf.type = "out"
       #             new_wf.description = wf.description.replace("input","output")
       #             new_wf.save()
       # #Now get the whole system PSF/CRDs and make files out of them...
       # path = subsysbasepath + "system_with_linkatoms.crd"
       # wfsubsyscrd = WorkingFile()
       # try:
       #     WorkingFile.objects.get(task=task,path=path)
       # except:
       #     wfsubsyscrd.task = task
       #     wfsubsyscrd.path = path
       #     wfsubsyscrd.canonPath = wfsubsyscrd.path #Fix later?
       #     wfsubsyscrd.type = "inp"
       #     wfsubsyscrd.description = "CRD for whole system (incl. linkatoms)"
       #     wfsubsyscrd.save()

       # path = path.replace("crd","psf")
       # wfsubsyspsf = WorkingFile()
       # try:
       #     WorkingFile.objects.get(task=task,path=path)
       # except:
       #     wfsubsyspsf.task = task
       #     wfsubsyspsf.path = path
       #     wfsubsyspsf.canonPath = wfsubsyspsf.path #Fix later?
       #     wfsubsyspsf.type = "inp"
       #     wfsubsyspsf.description = "PSF for whole system (incl. linkatoms)"
       #     wfsubsyspsf.save()
       # We're done. Save task and get out.
      pass  # I am not going to do this. Too much trouble. ~VS
  task.save()
