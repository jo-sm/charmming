from http.client import HTTPConnection
import os
import django.core.exceptions
import pickle

from charmming import charmming_config
from charmming.models import Structure
"""
Miscellaneous functions used for building and modifying Structure/WorkingStructure objects
and their associated classes.
Primarily serves as an abstraction tool for common operations in structure.views
and the Tasks that require structure modification/creation.
"""

def create_structure(owner, title, location, dname, original_name=False, automatically_generate_name=False, create_location=True, pickle=False):
  """
  Creates a Structure object with the owner, title, location and name properties filled in.
  If original_name is passed in, the original_name property of the Structure object will be set to that value.
  Otherwise, it will be set to be the same as dname.
  If automatically_generate_name is passed in, dname will be used as a base to generate a name,
  which will have a numerical suffix (e.g. sequ-1) until an identifier is generated that
  is not currently in use by the particular user.
  If create_location is True, the location argument will be used to create a directory with 0775 permissions
  for the structure.
  If a pickle file is provided for the pickle argument, it will be attached to the structure as well.
  """
  struct = Structure()
  struct.owner = owner
  struct.title = title
  struct.location = location
  if not automatically_generate_name:
    struct.name = dname
  else:
    # this is the standard CHARMMing way of doing it, and it's sort of boilerplate code...
    struct.name = generate_name(dname, location)
  struct.location = os.path.join(location, struct.name)
  if original_name:
    struct.original_name = original_name
  else:
    struct.original_name = struct.name
  if create_location:
    os.mkdir(struct.location, 0o775)
    os.chmod(struct.location, 0o775)
  if pickle:
    struct.pickle = pickle
  struct.save()  # make SURE it is committed to the DB before anything else uses it!
  return struct

def generate_name(dname, location):
  """
  Automatically generates a name for a structure by appending numerical suffixes
  to the end of the structure name until one is found that has not in use.
  Included as a convenience function to simplify the flow of dossier.views.
  """
  chkname = dname
  version = -1
  while os.path.exists(os.path.join(location, chkname)):
    version += 1
    chkname = "{}-{}".format(dname, str(version))
  return chkname

def unselect_current_structure(user):
  """
  If there is a currently-selected structure by a user, unselects it.
  Used to set up new Structure objects.
  """
  # NOTE: Only ONE Structure should be selected at any given time!
  # the except below attempts to handle that case and unselects everything,
  try:
    oldfile = Structure.objects.get(owner=user, selected='y')
    oldfile.selected = "n"
    oldfile.save()
  except django.core.exceptions.MultipleObjectsReturned:
    oldfiles = Structure.objects.filter(owner=user, selected='y')
    for file in oldfiles:
      file.selected = 'n'
      file.save()
  except:  # other exceptions are fine
    pass

def create_pickle_file(struct, pdb):
  """
  Creates the pickle file for a Structure based on the structure and its pychm PDB object.
  If the input file is a CRD, use .get_model_from_crd and add it to an empty PDB object.
  """
  # NOTE: The PDB object is separate because sometimes we want to make the it first to verify
  # that it is parseable, or potentially convert something else to create it.
  pfname = os.path.join(struct.location, "pdbpickle.dat")
  pickleFile = open(pfname, "wb")
  pickle.dump(pdb, pickleFile)
  pickleFile.close()
  struct.pickle = pfname
  struct.save()

def put_pdb_on_disk(pdbid, fullpath):
  """
  Takes as input a PDB identifier and a path, and deposits the PDB file at that path.
  Raises a ValueError if problems are encountered fetching the structure or processing the file.
  """
  # try:
  conn = HTTPConnection(charmming_config.pdb_protein_download_url)
  conn.request("GET", charmming_config.pdb_protein_download_pattern.format(pdbid))
  resp = conn.getresponse()
  # if resp.status != 200:
  #     raise ValueError("The PDB server returned error code {}.".format(resp.status))
  pdb_file = resp.read()

  # if "does not exist" in pdb_file:
  #     raise ValueError("The PDB servers report that this structure does not exist.")

  outfp = open(fullpath, 'w')
  outfp.write(bytes.decode(pdb_file, 'utf-8'))
  outfp.close()
  conn.close()
  # except ValueError as e:
  #   raise e # re-raise, catch anything else weird below
  # except:
  #   raise ValueError("There was an error processing your file. Please contact the server administrator for more details. Error: {}".format(sys.exc_info()[0]))
