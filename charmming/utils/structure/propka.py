import re
import os

# This file exists because of PROPKA for the Build Structure page ONLY!
# All functions in here are to do with using PROPKA for that page
# for the PROPKA page, see charmming/propka/views.py
def check_pka(inp_pka, inp_ph):
  # compares pKa to user-provided pH and returns value depending on whether it's protonated, neutral, or requires a decision
  # thankfully we don't have residues that go to +2 states
  # if we ever do, we'll need to modify this function
  if inp_pka < inp_ph:
    return 1  # neutral if ASP or GLU, protonated if HIS or LYS
  elif inp_pka > inp_ph:
    return 0  # -1 if ASP or GLU, neutral if HIS or LYS
  else:
    return 2  # user must decide

def calculate_propka_residues(struct, user_ph=7):
  # Outline goes as follows:
  # run PROPKA on the PDB from the protein (if any...if there isn't one, i.e. just PSF/CRD, we can't do anything since
  # babel used to have CHARMM format in 2002, but 12 years later they still haven't re-added support for it for openbabel 2.0)
  # we can probably convert since pychm now takes CRD inputs, however this is unlikely to be an issue since we always write out a PDB of some sort.
  # Anyway!
  # we run PROPKA, and use data garnished/stolen from organic chem textbooks on the pKa of these things
  # (we can probably cite him if need be)
  # and use that to predict the proto state. If < the pKa of a certain proto state, it's protonated, else, it's deprotonated
  # if pKa = the turning point, tell the user that there's a problem
  # basically we're going to append one last thing to the tuple containing the resname to mark what species it should be
  # now we check for whether we have a PDB file
  # user_ph is set by default to 7, can be modified otherwise
  glup_list = []  # allocate these just in case python decides to keep them inside the if statement
  hsp_list = []
  lsn_list = []
  asp_list = []
  list_append = {'ASP': asp_list, 'GLU': glup_list, 'LSN': lsn_list, 'HIS': hsp_list}
  os.chdir(struct.location)  # have to change to here because PROPKA only outputs to the dir it's run in
  propka_output = open(struct.original_name + "-propka.pka")
  # we make a list holding data on each of the four residues we care about
  # and then we make a regex for parsing them
  space_regex = re.compile(r' +')
  found_summary = False  # We use the "SUMMARY OF THIS PREDICTION" lines because they're formatted better
  user_decision = False
  for line in propka_output:
    if line.startswith("       Group"):  # We use this so that we're on the right line...using SUMMARY puts us one line above the useless table headers
      found_summary = True
      continue
    if found_summary:
      if line.startswith("------------"):  # close enough to the terminator line
        break
      else:
        line_pieces = space_regex.split(line)
        # Now we dump the resno pKa value and chain and make lists
        if line_pieces[1] in list_append:
          info_list = []
          info_list.append(int(line_pieces[2]))
          info_list.append(line_pieces[3].lower())
          info_list.append(line_pieces[4])
          species_pka = float(line_pieces[4])  # make our lives easier here
          check_state = check_pka(species_pka, user_ph)
          if check_state == 2:  # user must decide
            user_decision = True
          info_list.append(check_state)
          list_append[line_pieces[1]].append(info_list)  # avoids a pile of if statements
        else:
          continue  # skip over it
  propka_output.close()
  return [asp_list, glup_list, hsp_list, lsn_list], user_decision
  # These are all sequentially ordered, so there should be no problems later
