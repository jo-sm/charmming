from charmming.helpers import output
from charmming.models import Structure, WorkingStructure
from charmming.utils.lesson import get_lesson_object, doLessonAct

from pychm3.tools import flatten
from pychm3.lib.model import Model
from pychm3.io.charmm.dcd import open_dcd

import pickle
import numpy

def doRMSD(user, postdata, location, id, picklefile, stlist):
  # pfp = open(picklefile, 'r')
  # pdb = pickle.load(pfp)
  # pfp.close()

  # get all of the Mol objects corresponding to the structures that we want RMSDs of
  mols = {}
  for t in stlist:
    if t.action == 'build':
      mols[t.action] = pdb['append_' + id]
    elif t.action == 'minimization':
      mols[t.action] = pdb['mini_' + id]
    elif t.action == 'solvation':
      mols[t.action] = pdb['neut_' + id]
    elif t.action == 'neutralization':
      mols[t.action] = pdb['neut_' + id]
    elif t.action == 'md':
      mols[t.action] = pdb['md_' + id]
    elif t.action == 'ld':
      mols[t.action] = pdb['ld_' + id]
    elif t.action == 'sgld':
      mols[t.action] = pdb['sgld_' + id]
    else:
      return output.returnSubmission('RMS Calculation', error='RMSD does not handle %s' % t.action)

  # Now, strip out crap like solvent and orient them
  # we do this by only grabbing pro, dna, and rna residues...
  smols = {}
  for k in list(mols.keys()):
    tmp1 = mols[k].find(segtype='pro')
    tmp2 = mols[k].find(segtype='dna')
    tmp3 = mols[k].find(segtype='rna')
    tmp4 = mols[k].find(segtype='go')
    tmp5 = mols[k].find(segtype='bln')
    tmp6 = mols[k].find(segtype='bad')
    smols[k] = Model(sorted(tmp1 + tmp2 + tmp3 + tmp4 + tmp5 + tmp6))

  # Last step: if we want only the backbone, pull those atoms out of the first struct,
  # reorient it, and get the transform matrix to apply to all of the rest of the structs.
  if postdata['comp_allatom'] == '0':
    fmols = {}
    for k in list(smols.keys()):
      finmol = Model()
      if 'bb_n' in postdata:
        tmp = smols[k].find(atomtype=' n  ')
        finmol = Model(sorted(finmol + tmp))
      if 'bb_ca' in postdata:
        tmp = smols[k].find(atomtype=' ca ')
        finmol = Model(sorted(finmol + tmp))
      if 'bb_c' in postdata:
        tmp = smols[k].find(atomtype=' c  ')
        finmol = Model(sorted(finmol + tmp))
      if 'bb_o' in postdata:
        tmp = smols[k].find(atomtype=' o  ')
        finmol = Model(sorted(finmol + tmp))
      if 'bb_hn' in postdata:
        tmp = smols[k].find(atomtype=' hn ')
        finmol = Model(sorted(finmol + tmp))
      if 'bb_ha' in postdata:
        tmp = smols[k].find(atomtype=' ha ')
        finmol = Model(sorted(finmol + tmp))
      fmols[k] = Model(sorted(finmol))
  else:
    fmols = smols

  # now go ahead and calculate the RMSDs:

  rmsdlines = ['<table class="table_center" border="1">\n']

  fp = open(location + '/rmsd-' + id + '.html', 'w')
  structlist = list(fmols.keys())
  structlist.sort()

  tabwidth = float(100.0 / (len(structlist) + 1))
  headline = '<tr><td style="width:%f%%"></td>' % tabwidth
  for k in structlist:
    headline += '<td style="width:%f%%">%s</td>' % (tabwidth, k)
  headline += '</tr>\n'
  rmsdlines.append(headline)

  for i in range(len(structlist)):
    thisline = '<tr><td>%s</td>' % structlist[i]
    for j in range(len(structlist)):
      if j < i:
        thisline += '<td></td>'
      elif j == i:
        thisline += '<td>0.00</td>'
      else:
        key1 = structlist[i]
        key2 = structlist[j]
        rmsd = fmols[key1].get_rmsd(fmols[key2], orient=True)
        thisline += '<td>%.2f</td>' % rmsd
    thisline += '</tr>\n'
    rmsdlines.append(thisline)
  rmsdlines.append('</table>\n')

  for line in rmsdlines:
    fp.write(line)
  fp.close()

  # lesson time...
  try:
    struct = Structure.objects.filter(owner=user, selected='y')[0]
  except:
    return {
      'redirect': '/fileupload'
    }
  try:
    ws = WorkingStructure.objects.filter(structure=struct, selected='y')[0]
  except:
    return {
      'redirect': '/buildstruct'
    }

  lesson_obj = get_lesson_object(ws.structure.lesson_type)
  if lesson_obj:
    doLessonAct(ws.structure, "onRMSDSubmit", postdata)
  # end lesson time

  return {
    'context': {
      'rmsdlines': rmsdlines
    }
  }

def get_native_contacts(mymol, AACUT=4.5):
  # aux routines for NatQ
  # build AA.resid -> CG.atomid map
  mymap = [ None, ]
  ngly = 0
  for i, res in ( (res.resid, res) for res in mymol.iter_res()):
    if res.resName == 'gly':
      ngly += 1
      mymap.append(None)
    cur_index = i * 2 + ngly
    mymap.append(cur_index)
  # nuke backbone atoms
  atoms = ( atom for atom in mymol if not atom.is_backbone() )
  # nuke hydrogens
  atoms = ( atom for atom in atoms if atom.element != 'h' )
  # rebuild Mol obj
  mymol = Model(atoms)
  # build list of residue pairs in native contact
  native_contacts = []
  iterres = [ (res.resid, res) for res in mymol.iter_res() ]
  for i, res_i in iterres:
    for j, res_j in iterres:
      if i >= j - 1:
        continue # dont consider adjacent residues
      try:
        for atom_i in res_i:
          for atom_j in res_j:
            r_ij = atom_i.calc_length(atom_j)
            if r_ij <= AACUT:
              native_contacts.append((i, j))
              raise AssertionError # break a double loop
      except AssertionError:
        pass

  # apply AA.resid -> CG.atomid map to native_contacts
  def fun(inp):
    return (mymap[inp[0]], mymap[inp[1]])
  #
  return list(map(fun, native_contacts))

def extract_xyz(mydcd, native_contacts):
  # dump all data
  with open_dcd(mydcd) as fp:
    data = fp.get_massive_dump()
  # figure out which cg atoms we need
  cg_ids = sorted(set(flatten(native_contacts)))
  # extract the xyz data from the dump, for our coordinates of interest
  x = {}
  y = {}
  z = {}
  for id in cg_ids:
    try:
      x[id] = data['x'][:, id - 1]
      y[id] = data['y'][:, id - 1]
      z[id] = data['z'][:, id - 1]
    except:
      pass
  return (x, y, z)

def calc_q(native_contacts, x, y, z, CGCUT=8.0):
  # look at cut**2, instead of cut, sqrt's are expensive
  CUT = CGCUT * CGCUT
  # build scratch space for r**2
  dim0 = len(native_contacts)
  dim1 = x[native_contacts[0][0]].shape[0]
  data = numpy.zeros((dim0, dim1))
  # populate scratch with r**2 values for all native contacts
  for n, (i, j) in enumerate(native_contacts):
    dx = x[i] - x[j]
    dy = y[i] - y[j]
    dz = z[i] - z[j]
    rr = (dx * dx + dy * dy + dz * dz)
    data[n, :] = rr
  # build scratch for Q
  tmp = numpy.zeros(data.shape)
  # use fancy numpy mapping to check if R**2 is .gt. or .lt. CUT**2
  tmp[data <= CUT] = 1
  tmp[data > CUT] = 0
  # return Q(T)
  return tmp.mean(axis=0)
