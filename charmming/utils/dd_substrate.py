from django.contrib.auth.models import User

from charmming import charmming_config
from charmming.models import DDLigand, DDPose, DDFileObject, DDFile, DDLigandSet

import subprocess
import os
import socket

def getNewLigandOwnerIndex(user):
  next_ligand_owner_index = 1

  try:

    last_ligand = DDLigand.objects.filter(owner=user).order_by('-id')[0]
    next_ligand_owner_index = int(last_ligand.ligand_owner_index) + 1

  except:
    next_ligand_owner_index = 1

  return next_ligand_owner_index

def getChemicalName(file):
  chem_name = ""
  while 1:
    line = file.readline()
    if not line:
      break
    if line.strip() == "USER_CHARGES":
      chem_line = file.readline()
      chem_name = chem_line.strip()
      break
  return chem_name

def getNewLigandFilename(user):
  return "ligand_%s" % (str(getNewLigandOwnerIndex(user)))

def getMoleculeTitle(file):
  while 1:
    line = file.readline()
    if not line:
      break
    if line.strip() == "@<TRIPOS>MOLECULE":
      titleline = file.readline()
      title = titleline.strip()
      break
  return title


class LigandInfo():

  id = 0
  ligandsetid = 0
  name = ""
  description = ""
  file_objects_list = []
  filecount = 0
  files = []
  file = ""
  file_id = 0

class LigandSetObj():
  def __init__(self, **kwargs):
    self.id = kwargs.get('id') or 0
    self.name = kwargs.get('name') or ""
    self.description = kwargs.get('description') or ""
    self.ligand_count = kwargs.get('ligand_count') or 0
    self.datecreated = kwargs.get('datecreated') or ""

def LigandSetExists(setname, user):
  try:
    sets = DDLigandSet.objects.filter(name=setname, owner=user)
    if sets:
      return True
    else:
      return False
  except:
    return False

# TODO: This used to use manual transactions, and exception handlers
# They were removed and this should not cause an issue. Investigate!
def DeletePose(user, delete_id):
  public_user = User.objects.get(username='public')
  # make sure request data isn't malicious
  # input.checkRequestData(request)
  if delete_id != 0:
    delete_pose = DDPose.objects.get(id=delete_id)
    pose_files_objects = DDFileObject.objects.filter(owner__in=[user, public_user], object_table_name='dd_substrate_poses', object_id=delete_pose.id)
    for pose_file_object in pose_files_objects:
      pose_files = DDFile.objects.filter(owner__in=[user, public_user], id=pose_file_object.file_id)
      for pose_file in pose_files:
        DDFileObject.objects.filter(owner__in=[user, public_user], file=pose_file).exclude(id=pose_file_object.id).delete()
      pose_files.delete()
    pose_files_objects.delete()
    delete_pose.delete()
  else:
    return False
  return True


def makeMatch(tempname):
  """
  Uses the match program from the Charlie Brooks group to
  try to build topology and parameter files.
  """

  os.putenv("PerlChemistry", "%s/MATCH_RELEASE/PerlChemistry" % charmming_config.data_home)
  os.putenv("MATCH", "%s/MATCH_RELEASE/MATCH" % charmming_config.data_home)
  os.chdir("/tmp")

  # self.rtf_list = '%s/toppar/top_all27_prot_na.rtf' % charmming_config.data_home
  # self.prm_list = '%s/toppar/par_all27_prot_na.prm' % charmming_config.data_home
  # for badRes in badResList:
  exe_line = '%s/MATCH_RELEASE/MATCH/scripts/MATCH.pl %s.mol2 > /tmp/%s.txt' % (charmming_config.data_home, tempname, tempname)
  status, output = subprocess.getstatusoutput(exe_line)
  if status != 0:
    return -1
  else:
    os.chdir("/tmp")
    os.system("awk '/RESI/{$2=\"ULIG\"; print;next}{print}' %s.rtf > %s_temp.rtf" % (tempname, tempname))
    os.system("mv %s.prm %s_temp.prm" % (tempname, tempname))
    os.system("grep -v \"MASS\" %s_temp.rtf > %s.rtf" % (tempname, tempname))
    os.system("awk '/NONBONDED/{exit;}{print;}' %s_temp.prm > %s.prm" % (tempname, tempname))
    os.system("echo 'read rtf card append' > %s.str" % (tempname))
    os.system("cat %s.rtf >> %s.str" % (tempname, tempname))
    os.system("echo 'read para card flex append' >> %s.str" % (tempname))
    os.system("cat %s.prm >> %s.str" % (tempname, tempname))
    os.system("printf 'END\nRETURN' >> %s.str" % (tempname))

    # ToDo: add the rtf/param files that have been generated to a master topology
    # and parameter files for the entire segment.
    # self.rtf_list += ' %s-badres-h-%s.rtf' % (self.name,badRes)
    # self.prm_list += ' %s-badres-h-%s.prm' % (self.name,badRes)
  return 0

def makeCGenFF(ws, tempname):
  """
  Connects to dogmans.umaryland.edu to build topology and
  parameter files using CGenFF
  """
  # self.rtf_list = '%s/toppar/top_all36_cgenff.rtf' % charmming_config.data_home
  # self.prm_list = '%s/toppar/par_all36_cgenff.prm' % charmming_config.data_home

  header = '# USER_IP 165.112.184.52 USER_LOGIN %s\n\n' % ws.structure.owner.username
  # for badRes in badResList:

  fp = open('/tmp/%s.mol2' % (tempname), 'r')
  payload = fp.read()
  content = header + payload
  fp.close()

  recvbuff = ''
  try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((charmming_config.cgenff_host, charmming_config.cgenff_port))
    s.sendall(content)
    s.shutdown(socket.SHUT_WR)
    while True:
      data = s.recv(1024)
      if not data:
        break
      recvbuff += data

  except Exception as e:
    return -1

  fp = open('/tmp/%s-dogmans.txt' % (tempname), 'w')
  fp.write(recvbuff)
  fp.close()
  # lines = open('/tmp/%s-dogmans.txt' % (tempname)).readlines()
  # open('/tmp/%s.str' % (tempname),'w').writelines(lines[1:])

  # parse file returned from dogmans, and make sure that it has both topology and
  # parameters.
  rtffound = False
  prmfound = False
  inrtf = False
  inprm = False
  rtffp = open('/tmp/%s-dogmans.rtf' % (tempname), 'w')
  prmfp = open('/tmp/%s.prm' % (tempname), 'w')
  for line in recvbuff.split('\n'):
    if inrtf:
      rtffp.write(line + '\n')
    if inprm:
      prmfp.write(line + '\n')

    line = line.strip()
    if line == 'end' or line == 'END':
      inrtf = False
      inprm = False
    if line.startswith('read rtf'):
      rtffound = True
      inrtf = True
    if line.startswith('read param'):
      prmfound = True
      inprm = True

  rtffp.close()
  prmfp.close()

  if not (rtffound and prmfound):
    return -1
  else:
    os.chdir("/tmp")
    os.system("awk '/RESI/{$2=\"ULIG\"; print;next}{print}' dd_user_ligand-dogmans.rtf > dd_user_ligand.rtf")
    os.system("awk '/Toppar/,0' dd_user_ligand-dogmans.txt > dd_user_ligand.str")

  # self.rtf_list += ' %s-%s-dogmans.rtf' % (self.name,badRes)
  # self.prm_list += ' %s-%s-dogmans.prm' % (self.name,badRes)

  return 0

def checkDAIM(tempname):
  os.chdir("/tmp")

  # self.rtf_list = '%s/toppar/top_all27_prot_na.rtf' % charmming_config.data_home
  # self.prm_list = '%s/toppar/par_all27_prot_na.prm' % charmming_config.data_home
  # for badRes in badResList:
  os.system("cp %s ." % (charmming_config.daim_param))
  os.system("cp %s ." % (charmming_config.daim_prop))
  os.system("cp %s ." % (charmming_config.daim_weight))

  exe_line = '%s --paramfile %s  %s.mol2 > /tmp/%s_daim.txt' % (charmming_config.daim_exe, charmming_config.daim_param, tempname, tempname)
  status, output = subprocess.getstatusoutput(exe_line)
  if status != 0:
    return -1
  else:
    os.chdir("/tmp")
    if "WARNING" in open("/tmp/daim.log").read():
      return -1
  return 0
