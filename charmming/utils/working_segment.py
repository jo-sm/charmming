from charmming.models import Patch
from charmming import charmming_config
from charmming.helpers import output
from charmming.utils.script import get_input_script

import pickle
import os

# This method will handle the building of the segment, including
# any terminal patching
def build(working_segment, mdlname, workstruct):
  template_dict = {}
  # protonation patching for this segment. We do this here so we can make sure pro segs are written correctly
  # especially since we don't need any data from the pickle or anything else to make this
  # Hopefully it doesn't break. ~VS 2/2/15
  patch_lines = ''
  patches = Patch.objects.filter(structure=workstruct)
  patchresd = {}  # list of residues that we need to patch in the pickle file before we can write anything
  for patch in patches:
    pseg = patch.patch_segid
    if pseg and pseg.name == working_segment.name:
      # this is a patch that we want to apply
      if patch.patch_name.startswith('hs'):
        # these seem to be the only ones where it's a problem since it's not a real patch.
        # patch_lines += 'rename resn %s sele resid %s end\n' % (patch.patch_name,patch.patch_segres.split()[1])
        patchresd[int(patch.patch_segres.split()[1])] = patch.patch_name  # since we're only looking at this seg, we don't need any more info
        # we need the split because patch_Segres is actually a_pro 34, not just a-pro, because we need the full thing for getAppendPatches.
      else:
        patch_lines += 'patch %s %s\n' % (patch.patch_name, patch.patch_segres)

  if patch_lines:
    template_dict['patch_lines'] = patch_lines

  # write out the PDB file in case it doesn't exist
  pfile = workstruct.localpickle if workstruct.localpickle else workstruct.structure.pickle
  fp = open(pfile, 'rb')
  mol = (pickle.load(fp))[mdlname]
  fname = ""
  use_other_buildscript = False
  for s in mol.iter_seg():
    if s.segid == working_segment.name:
      if len(patchresd) > 0:  # we got some HIS we need to rename
        for res in s.iter_res():
          if res.resid in patchresd:  # if this is the right one - this might be possible to make more efficient??
            # resid is an int so no need to cast
            res.resName = patchresd[res.resid]  # change the name
      # after this we hopefully write the right res to the file, and CHARMM can figure out how to HBUILD correctly.
      fname = working_segment.structure.location + "/" + "segment-" + working_segment.name + ".pdb"
      s.write(fname, outformat="charmm")
      template_dict['pdb_in'] = fname
      break
  os.system('cat %s >> /tmp/debug1.txt' % fname)

  # see if we need to build any topology or param files
  use_other_buildscript = False
  if working_segment.type == 'bad':
    bhResList, use_other_buildscript = working_segment.getUniqueResidues(mol)

    if len(bhResList) > 0:
      if working_segment.tpMethod == 'autogen':

        success = False
        for tpmeth in charmming_config.toppar_generators.split(','):
          if tpmeth == 'genrtf':
            rval = working_segment.makeGenRTF(bhResList)
          elif tpmeth == 'antechamber':
            rval = working_segment.makeAntechamber(bhResList)
          elif tpmeth == 'cgenff':
            rval = working_segment.makeCGenFF(bhResList, use_other_buildscript)
          elif tpmeth == 'match':
            rval = working_segment.makeMatch(bhResList)
          if rval == 0:
            success = True
            break
        if not success:
          raise AssertionError('Unable to build topology/parameters')

      elif working_segment.tpMethod == 'dogmans':
        rval = working_segment.makeCGenFF(bhResList, use_other_buildscript)
      elif working_segment.tpMethod == 'match':
        rval = working_segment.makeMatch(bhResList)
      elif working_segment.tpMethod == 'antechamber':
        rval = working_segment.makeAntechamber(bhResList)
      elif working_segment.tpMethod == 'genrtf':
        rval = working_segment.makeGenRTF(bhResList)
  # ennd if working_segment.type == 'bad'
  # done top/par file building
  fp.close()

  # template dictionary passes the needed variables to the template
  if working_segment.rtf_list:
    template_dict['topology_list'] = working_segment.rtf_list.split(' ')
  else:
    template_dict['topology_list'] = []
  if working_segment.prm_list:
    template_dict['parameter_list'] = working_segment.prm_list.split(' ')
  else:
    template_dict['parameter_list'] = []

  template_dict['patch_first'] = working_segment.patch_first
  template_dict['patch_last'] = working_segment.patch_last
  template_dict['segname'] = working_segment.name
  template_dict['outname'] = working_segment.name + '-' + str(working_segment.id)
  if working_segment.stream_list:
    template_dict['tpstream'] = [x for x in working_segment.stream_list.split()]
  else:
    template_dict['tpstream'] = []
  # somewhere around here we need to inject the new build script
  if working_segment.type == 'good':
    template_dict['doic'] = False
    template_dict['noangdihe'] = 'noangle nodihedral'
  else:
    template_dict['doic'] = True
    template_dict['noangdihe'] = ''

  # Custom user sequences are treated differently
  # NB: this has not been gutsified at all...
  if(working_segment.name == 'sequ-pro'):
    # The sequ filename stores in the PDBInfo filename
    # differs from the actual filename due to comaptibility
    # problems otherwise so sequ_filename is the actual filename
    sequ_filename = "new_" + working_segment.stripDotPDB(working_segment.filename) + "-sequ-pro.pdb"
    sequ_handle = open(working_segment.location + sequ_filename, 'r')
    sequ_line = sequ_handle.read()
    sequ_line.strip()
    sequ_list = sequ_line.split(' ')
    number_of_sequences = len(sequ_list)
    template_dict['sequ_line'] = sequ_line
    template_dict['number_of_sequences'] = repr(number_of_sequences)

  # seg patching used to be done here but we can't rely on CHARMM renaming to actually c

  # write out the job script
  if use_other_buildscript:
    t = get_input_script('buildBadSeg.inp')
  else:
    t = get_input_script('buildSeg.inp')
  charmm_inp = output.tidyInp(t.render(template_dict))
  charmm_inp_filename = working_segment.structure.location + "/build-" + template_dict['outname'] + ".inp"
  charmm_inp_file = open(charmm_inp_filename, 'w')
  charmm_inp_file.write(charmm_inp)
  charmm_inp_file.close()

  # user_id = working_segment.structure.owner.id
  os.chdir(working_segment.structure.location)

  working_segment.builtPSF = template_dict['outname'] + '.psf'
  working_segment.builtCRD = template_dict['outname'] + '.crd'
  working_segment.isBuilt = 'y'
  working_segment.save()
  workstruct.save()

  return charmm_inp_filename
