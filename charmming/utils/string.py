def capitalize_classcase(value):
  parts = value.split('_')

  parts = [ part.capitalize() for part in parts ]

  return ''.join(parts)
