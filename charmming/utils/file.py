from django.conf import settings

from charmming.models import File

import os

def save_file(file, file_type, structure):
  # Create directory in /files
  base_dir = settings.PROJECT_ROOT
  files_dir = os.path.join(base_dir, "files")
  file_dir = os.path.join(files_dir, "{}".format(structure.id))

  # Create the file and attempt to save it
  file_name = "{}.{}".format(os.urandom(8).hex(), file_type)
  file_path = os.path.join(file_dir, file_name)

  try:
    os.mkdir(file_dir)
  except FileExistsError:
    # The directory exists somehow...
    return False

  file_io = open(file_path, 'w')
  file_io.write(file)
  file_io.close()

  # Save the file information to the database
  file_model_instance = File()
  file_model_instance.name = file_name
  file_model_instance.path = file_path
  file_model_instance.file_type = file_type
  file_model_instance.structure = structure
  file_model_instance.save()

  return True
