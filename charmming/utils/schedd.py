from django.http import HttpResponse

from charmming import charmming_config

from socket import socket

import string

class schedInterface:
  def checkStatus(self, jobID):
    if jobID < 1:
      return ''
    self.sock.send("STATUS\n")
    buf = self.sock.recv(512)
    if not buf.startswith("OK"):
      return -1
    self.sock.send("%d\n" % jobID)
    buf = self.sock.recv(512)
    if not buf.startswith("OK"):
      return -2
    return buf[3:]

  def submitJob(self, user, dir, scriptlist, exedict={}, nprocdict={}, mscale=False):

    # error out if the length of the executable list or the
    # length of the nproc list is different from the length of
    # the scriptlist (assuming they exist).
    le = len(list(exedict.keys()))
    lp = len(list(nprocdict.keys()))
    if le > len(scriptlist):
      return -3
    if lp > len(scriptlist):
      return -3

    # build exelist and nproclist from exedict and nprocdict
    exelist = []
    nproclist = []
    for i in range(len(scriptlist)):
      if scriptlist[i] in list(nprocdict.keys()):
        nproclist.append(nprocdict[scriptlist[i]])
      else:
        nproclist.append(1)

      if scriptlist[i] in list(exedict.keys()):
        exelist.append(exedict[scriptlist[i]])
        # mscale is an alt_exe so we can probably comment out the next two lines
      elif nproclist[-1] > 1 and not mscale:
        exelist.append(charmming_config.apps['charmm-mscale'])
      else:
        if 'charmm' in list(charmming_config.apps.keys()):
          exelist.append(charmming_config.apps['charmm'])
        else:
          return -3
    self.sock.send("NEW\n")
    buf = self.sock.recv(512)
    if not buf.startswith("OK"):
      return -1
    scriptstr = string.join(scriptlist)
    self.sock.send("%d %s %s\n" % (user, dir, scriptstr))
    buf = self.sock.recv(512)
    if not buf.startswith("OK"):
      logfp = open("/tmp/scheda_debug.txt", "w")
      logfp.write('test 1\n')
      logfp.write(buf + '\n')
      logfp.close()
      return -2
    logfp = open("/tmp/schedint_debug.txt", "w")
    logfp.write("exelist: " + str(exelist) + "\n")
    nprbuf = ""
    npl2 = []
    for i in range(len(nproclist)):
      nprbuf += "%s " % nproclist[i]

    exebuf = ""
    for i in range(len(scriptlist)):
      if exelist:
        exebuf += "%s " % exelist[i]
      else:
        if npl2[i] == 1:
          exebuf += "%s " % charmming_config.charmm_exe
        elif npl2[i] > 1:
          exebuf += "%s " % charmming_config.charmm_mpi_exe
    logfp.write("exebuf: " + str(exebuf) + "\n")
    logfp.flush()

    self.sock.send("%s\n" % exebuf)
    logfp.write("exebuf: " + str(exebuf) + "\n")
    logfp.flush()
    buf = self.sock.recv(512)
    logfp.write(str(buf))
    logfp.close()
    if not buf.startswith("OK"):
      logfp = open("/tmp/scheda_debug.txt", "w")
      logfp.write('test 2\n')
      logfp.write(buf + '\n')
      logfp.close()
      return -2

    self.sock.send("%s\n" % nprbuf)
    buf = self.sock.recv(512)
    if not buf.startswith("OK"):
      logfp = open("/tmp/scheda_debug.txt", "w")
      logfp.write('test 3\n')
      logfp.write('nprbuf = %s\n' % nprbuf)
      logfp.write(buf + '\n')
      logfp.close()
      return -2

    return int(buf.split()[2])

  def doJobKill(self, user, jobid):
    self.sock.send("ABORT\n")
    buf = self.sock.recv(512)
    if not buf.startswith("OK"):
      return -1
    self.sock.send("%d %s\n" % (user, jobid))
    buf = self.sock.recv(512)
    if not buf.startswith("OK"):
      return -2
    return 0

  def submitOrderedList(self, user, dir, nscript, slist):
    pass

  def __del__(self):
    self.sock.send("END\n")
    self.sock.close()

  def __init__(self):
    self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    self.sock.connect(('scheduler', 9995))
    self.sock.send("HELLO SCHEDD\n")
    buf = self.sock.recv(1024)
    if buf != "HELLO CLIENT\n":
      self.sock.close()
      raise Exception("Protocol error from server.")

# kills a job based on user request...
def killJob(request, jobid):
  uid = request.user.id
  jobid = jobid.replace('/', '')

  si = schedInterface()
  r = si.doJobKill(uid, jobid)
  return HttpResponse("code %s" % r)

def statsDisplay(statline, jobid):
    try:
       statarr = statline.split()
    except:
       return "Unknown"
    status = statarr[4]
    if status == 'submitted':
        return "<span style='color:FFCC33'>Submitted</span>"
    elif status == 'queued':
        return "<span style='color:FFCC33'>Queued</span> <a href=\"javascript:killJob(%d);\">X</a>" % jobid
    elif status == 'running':
        return "<span style='color:3333FF'>Running</span> <a href=\"javascript:killJob(%d);\">X</a>" % jobid
    elif status == 'complete':
        return "<span style='color:33CC00'>Done</span>"
    elif status == 'failed':
        return "<font color=red>Failed</span>"

    print("Achtung! Couldn't figure out this status (%s)!!!!" % status)
    return "<font color=\"red\">Unknown</span>"
