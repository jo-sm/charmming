from charmming.utils.toppar import handle_toppar_generation

def handle_pre_build(segment):
  handle_false_patches(segment)

  if segment.type == 'bad':
    generated = handle_toppar_generation(segment)

    return generated

  return True

def handle_false_patches(segment):
  # Handle false patches
  patch_renames = {}
  patches = segment.structure.patch_set.filter(segment=segment)

  for patch in patches:
    if patch.name.startswith('hs'):
      # We only want the true patch lines.
      # These seem to be the only ones where it's a problem since it's not a real patch.
      residue = int(patch.segment_residue.split()[1])

      patch_renames[residue] = patch.name

  # If there are any patch rewrites, we write those out
  # to the PDB before building.
  # TODO: Can this be handled in another way?
  if len(patch_renames) > 0:
    segment_pdb = segment.get_pychm_segment()

    for res in segment_pdb.residues:
      if res.id in patch_renames:
        res.name = patch_renames[res.id]

    segment.save_pychm_segment(segment_pdb)
