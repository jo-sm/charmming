import importlib

def basic_validation_coordinates(coordinates):
  """
  Makes sure that the coordinate dict is, at least, formatted
  properly and contains valid data
  """
  if not type(coordinates) == dict:
    return False

  program = coordinates.get('program')
  job = coordinates.get('job')
  index = coordinates.get('index')

  if not program:
    return False

  if not job:
    return False

  if index is None:
    return False

  try:
    # Attempt to find the module that the coordinates references
    importlib.import_module('charmming.jobs.{}.{}'.format(program, job))
  except ModuleNotFoundError:
    print('Could not find module...')
    return False

  return True

def job_can_have_coordinates(program, job):
  try:
    # Attempt to find the module that the coordinates references
    module = importlib.import_module('charmming.jobs.{}.{}'.format(program, job))
  except ModuleNotFoundError:
    return False

  class_name = job.capitalize()

  if not hasattr(module, class_name):
    return False

  job_class = getattr(module, class_name)

  if not job_class.has_coordinates:
    return False

  return True
