from charmming.models import LessonMakerLesson, Step, Lesson

"""
This module handles boilerplate code related to the lesson_maker being triggered for particular steps.
"""

def create_lesson_base_structure(struct, filename):
  """
  Creates a new LessonMakerLesson object and attaches it to a Structure for further development of a new lesson,
  then calls onFileUpload for additional processing.
  """
  new_lesson = LessonMakerLesson()
  new_lesson.structure = struct
  new_lesson.finished = False
  new_lesson.filename = filename
  new_lesson.save()
  onFileUpload(new_lesson)

def onTaskDone(lesson_maker_obj, inp_task):
  # we call the exact same code over and over for every task, so we're making it generic
  task = inp_task
  # we can pass the task directly in structure_models...we don't have to care
  if task.status == "F":
    return False
  else:
    file_step = Step()
    file_step.lesson = Lesson.objects.get(structure=task.workstruct.structure)
    # now we do have to care, so we fetch the object since we are calling this from
    # an empty Lesson()
    file_step.step = len(Step.objects.filter(lesson=lesson_maker_obj)) + 1  # make this generic, so we start at 0
    file_step.task = task
    file_step.type = task.action
    file_step.structure = None  # avoid data redundancy!
    file_step.workingstructure = None
    file_step.save()
    lesson_maker_obj.save()  # just in case...
    return True

def onFileUpload(lesson_maker_obj):
  file_step = Step()
  file_step.lesson = lesson_maker_obj
  file_step.step = len(Step.objects.filter(lesson=lesson_maker_obj)) + 1  # make this generic, so we start at 0
  # we only need whole number steps, but we will leave them in the format that is compatible with lessons currently available
  file_step.task = None
  file_step.type = "fileupload"
  file_step.save()
  lesson_maker_obj.save()  # structure is defined by this point so we don't have to care
  lesson_maker_obj.structure.lessonmaker_active = True
  lesson_maker_obj.structure.save()
  return True
  # we'll pass this step the structure from the Lesson object

def onBuildStructureDone(lesson_maker_obj, postdata):
  return True

def onBuildStructureSubmit(lesson_maker_obj):
  # this one actually gets the structure built before it gets called, so we don't have to worry about it
  # we sadly don't actually have the task object otherwise I'd save that
  file_step = Step()
  file_step.lesson = lesson_maker_obj
  # we also do this on the page directly so we don't have to care just yet
  file_step.step = len(Step.objects.filter(lesson=lesson_maker_obj)) + 1  # make this generic, so we start at 0
  file_step.task = None
  file_step.type = "buildstruct"
  # we'll pass this step a WorkingStructure later.
  file_step.save()  # highly doubt this does anything but why not
  lesson_maker_obj.save()
  return True
