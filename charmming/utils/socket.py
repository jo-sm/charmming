import msgpack
import struct

def send(sock, raw):
  """
  Sends a msgpack encoded message to `sock`
  """
  packed = msgpack.packb(raw, use_bin_type=True)
  length = len(packed)

  msg = struct.pack('>I', length) + packed

  sock.send(msg)

def recv(sock, **kwargs):
  with_addr = False
  empty = None

  if kwargs.get('with_addr'):
    with_addr = True
    empty = None, None

  raw_length, addr = sock.recvfrom(4)

  if not raw_length:
    return empty

  length = struct.unpack('>I', raw_length)[0]
  data = b''

  while len(data) < length:
    packet, addr = sock.recvfrom(length - len(data))

    if not packet:
      # Somehow we didn't get anything here
      return empty

    data += packet

  message = msgpack.unpackb(data, encoding='utf-8')

  if with_addr:
    return message, addr
  else:
    return message
