from django.template.loader import get_template
from django.template import Context

from charmming import charmming_config
from charmming.helpers import output
from charmming.utils import redox_mod
from charmming.utils.task import start, add_script
from charmming.utils.lesson import get_lesson_object, doLessonAct

from pychm3.lib.model import Model
from pychm3.io.charmm.rtf import RTFFile

import pickle
import copy
import os
import re

def createFinalPDB(request, workstruct):
  if 'picksite' not in request.POST:
    raise AssertionError('Variable picksite must be specified')

  '''
     Below is the code Scott removed to add in the mutation-friendly code.
  '''
  #    try:
  #         pfp = open(workstruct.structure.location + '/' + workstruct.identifier + '-pickle.dat', 'r')
  #         mut = True
  #    except:
  #         pfp = open(workstruct.structure.pickle, 'r')
  # #   pfp = open(workstruct.structure.pickle, 'r')
  #    pdb = cPickle.load(pfp)
  #    pfp.close()
  #    ormdl = pdb[0] # first model has all of the segments
  # #   ormdl.write('/tmp/Redox_test3.pdb',outformat='charmm') # WRITE
  #    try:
  #        if '-mt1' in workstruct.identifier:
  #            mymdl = pdb['append_%s' % workstruct.identifier[:-4]]
  #        else:
  #            mymdl = pdb['append_%s' % workstruct.identifier]
  #    except:
  #        mymdl = pdb['model00']
  #    numdl = copy.deepcopy(mymdl)

  #    for seg in workstruct.segments.all():
  #        if seg.redox:
  #            for mdlseg in ormdl.iter_seg():
  #                if mdlseg.segid == seg.name:
  #                    # add this segment to the model
  #                    numdl = numdl + Mol(mdlseg)

  try:
    '''
        We'll attempt to read in two different PDB files; a mutant (pdb) and the original (wt_pdb).
    '''
    pfp = open(workstruct.structure.location + '/' + workstruct.identifier + '-mutation.dat', 'r')
    pdb = pickle.load(pfp)
    pfp.close()

    pfp = open(workstruct.structure.pickle, 'r')
    wt_pdb = pickle.load(pfp)
    pfp.close()

    # m_ormdl = pdb[0] # first model has all of the segments
    w_ormdl = wt_pdb[0] # first model has all of the segments
    m_mymdl = pdb['model00']
    # w_mymdl = wt_pdb['append_%s' % workstruct.identifier[:-4]]

    numdl = copy.deepcopy(m_mymdl)

    for seg in workstruct.segments.all():
      if seg.redox:
        for mdlseg in w_ormdl.iter_seg():
          if mdlseg.segid == seg.name:
            # add this segment to the model
            numdl = numdl + Model(mdlseg)
  except:
    '''
        If something went wrong, we'll go back to the single PDB code.
    '''
    try:
      pfp = open(workstruct.localpickle, 'r')
    except:
      pfp = open(workstruct.structure.pickle, 'r')
    pdb = pickle.load(pfp)
    pfp.close()
    ormdl = pdb[0] # first model has all of the segments

    pfp = open(workstruct.structure.pickle, 'r')
    wt_pdb = pickle.load(pfp)
    pfp.close()

    try:
      if '-mt1' in workstruct.identifier:
        mymdl = wt_pdb['append_%s' % workstruct.identifier[:-4]]
      else:
        mymdl = pdb['append_%s' % workstruct.identifier]
    except:
      mymdl = pdb['model00']
    numdl = copy.deepcopy(mymdl)

    for seg in workstruct.segments.all():
      if seg.redox:
        for mdlseg in ormdl.iter_seg():
          if mdlseg.segid == seg.name:
            # add this segment to the model
            numdl = numdl + Model(mdlseg)

  numdl = Model(numdl)
  numdl.write('/tmp/Redox_test.pdb', outformat='charmm')

  pdb['redox_%s' % workstruct.identifier] = numdl

  os.unlink(workstruct.structure.pickle) # to be safe
  pfp = open(workstruct.structure.pickle, 'w')
  pickle.dump(pdb, pfp)
  pfp.close()

  return numdl

def redox_tpl(request, redoxTask, workstruct, pdb, pdb_metadata):

  # creatMutantPDB(request,workstruct)

  if 'couple' not in request.POST:
    raise AssertionError('No coupling found')

  if 'picksite' not in request.POST:
    raise AssertionError('Got to redox_tpl without picksite. How did that happen??!')

  doKnockout = ''
  if 'chrgknockout' in request.POST:
    # input.checkForMaliciousCode(request.POST['chrgknockout'])
    if len(request.POST['chrgknockout']) > 0:
      doKnockout = request.POST['chrgknockout']

  m = re.search('site_([A-Z])_([1-9])', request.POST['picksite'])
  if not m:
    raise AssertionError('picksite is not in the correct format')

  if request.POST['couple'] == 'oxi':
    clusnameo = '4fso'
    redoxTask.redoxsite = 'couple_oxi'
  else:
    # assume we want a reduction
    clusnameo = '4fsr'
    redoxTask.redoxsite = 'couple_red'

  rtf = RTFFile('%s/toppar/top_all22_4fe4s_esp_090209.inp' % charmming_config.data_home)

  pdb.write('/tmp/Redox_test2.pdb', outformat='charmm')

  # Step 1: generate the patched structures of the redox site ... a call to Scott's
  # code replaces the old genstruct_tpl call.
  cysDict = {}
  redox_mod.fesSetup(pdb, clusnameo, rtf, int(m.group(2)), 0, workstruct.structure.location, workstruct.identifier, pdb_metadata, cysDict)

  dictkey = '%s-%s' % (m.group(1).lower(), m.group(2))
  try:
    cysDict[dictkey]
  except:
    raise AssertionError('key %s not found in cysDict -- bad Scott!' % dictkey)

  # step 2: make final PSF and CRD of the oxidized and reduced structures
  # genstruct_tpl(workstruct,redoxTask,m.group(1),rsite_resid,cysResList)
  genstruct_tpl(workstruct, redoxTask, m.group(1), int(m.group(2)), cysDict)

  # step 3: make dielectric grids (1. protein + SF4 2. just SF4 + hanging -CH2)
  # This script will include a system call to dxmath to make the grids and combine
  # them.
  # NB: This functionality has been moved into gendelg_tpl -- schedule for removal
  # gengrid_tpl(request,workstruct,redoxTask)

  # step 4: Get the four free energy values for redpot, redpotref, modpot, modpotref where mod = oxi or sr
  getdelg_tpl(request, workstruct, redoxTask, doKnockout, int(m.group(2)))

  # all scripts generated, submit to the scheduler
  start(redoxTask, altexe=charmming_config.charmm_apbs_exe)
  redoxTask.save()

  # YP lessons status update
  lesson_obj = get_lesson_object(workstruct.structure.lesson_type)
  if lesson_obj:
    doLessonAct(workstruct.structure, "onRedoxSubmit", redoxTask, "")
  # YP
  workstruct.save()

  return output.returnSubmission('Oxidation/reduction')

def genstruct_tpl(workstruct, redoxTask, rsite_chain, rsite_resid, cysDict):
  # Major changes by Scott on 09/28/12:
  #     cysDict is carried over rather than just cysResList - need to patch all clusters
  #     rsite_resid was added - indicates the residue being reduced
  td = {}
  td['rtf'] = '%s/toppar/top_all22_4fe4s_esp_090209.inp' % charmming_config.data_home
  td['prm'] = '%s/toppar/par_all22_4fe4s_esp_090209.inp' % charmming_config.data_home
  td['id'] = workstruct.identifier

  NumberOfRedoxSites = len(cysDict)
  reds = []
  for i in range(0, NumberOfRedoxSites):
    red = {}
    red['segid'] = 'FS%s' % i
    red['presname'] = 'PFSR'
    tmpsegresid = ''
    cysResList = cysDict['a-%i' % (i + 1)]
    resnums = cysResList.split(',')
    if len(resnums) != 4:
      raise AssertionError('wrong number of elements is cysResList')
    for rnum in resnums:
      tmpsegresid += '%s-pro %s ' % (rsite_chain, rnum)
    tmpsegresid += '%s-bad %i' % (rsite_chain, i + 1)
    red['segresid'] = tmpsegresid
    reds.append(red)
  td['reds'] = reds

  # td['reds'] = ['FS%s'%i for i in range(1,NumberOfRedoxSites+1)]
  # td['segresid'] = ''
  # resnums = cysResList.split(',')
  # if len(resnums) != 4:
  #    raise AssertionError('wrong number of elements is cysResList')
  # for rnum in resnums:
  #    td['segresid'] += '%s-pro %s ' % (rsite_chain,rnum)
  # td['segresid'] += '%s 1' % td['reds'][0]
  # td['segresid'] += '%s-bad 1' % rsite_chain # FixMe: we hope for now that the REDOX site is the only residue in the bad chain

  if redoxTask.redoxsite == 'couple_oxi':
    oxipatch = 'PFSO'
    redpatch = 'PFSR'
  else:
    oxipatch = 'PFSR'
    redpatch = 'PFSS'

  # step 1: oxidized all
  td['segs'] = [ seg.name for seg in workstruct.segments.all()]
  td['suffix'] = '_o'
  td['outname'] = 'redox-%s-oxiall' % workstruct.identifier
  td['needpatch'] = True
  td['reds'][rsite_resid - 1]['presname'] = oxipatch
  t = get_template('%s/mytemplates/input_scripts/redox_makestruct.inp' % charmming_config.charmming_root)
  charmm_inp = output.tidyInp(t.render(Context(td)))
  inp_filename = workstruct.structure.location + "/" + 'redox-%s-build-oxiall.inp' % workstruct.identifier
  fp = open(inp_filename, 'w')
  fp.write(charmm_inp)
  fp.close()
  add_script(redoxTask, "charmm", inp_filename, charmming_config.default_charmm_nprocs)

  # step 2: oxidized redox site only
  td['segs'] = [ '%s-bad' % rsite_chain.lower() ]
  td['outname'] = 'redox-%s-oxisite' % workstruct.identifier
  td['needpatch'] = False
  t = get_template('%s/mytemplates/input_scripts/redox_makestruct.inp' % charmming_config.charmming_root)
  charmm_inp = output.tidyInp(t.render(Context(td)))
  inp_filename = workstruct.structure.location + '/' + 'redox-%s-build-oxisite.inp' % workstruct.identifier
  fp = open(inp_filename, 'w')
  fp.write(charmm_inp)
  fp.close()
  add_script(redoxTask, "charmm", inp_filename, charmming_config.default_charmm_nprocs)

  # step 3: reduced all
  td['segs'] = [seg.name for seg in workstruct.segments.all()]
  td['suffix'] = '_r'
  td['outname'] = 'redox-%s-redall' % workstruct.identifier
  td['needpatch'] = True
  td['reds'][rsite_resid - 1]['presname'] = redpatch
  t = get_template('%s/mytemplates/input_scripts/redox_makestruct.inp' % charmming_config.charmming_root)
  inp_filename = workstruct.structure.location + '/' + 'redox-%s-build-redall.inp' % workstruct.identifier
  charmm_inp = output.tidyInp(t.render(Context(td)))
  fp = open(inp_filename, 'w')
  fp.write(charmm_inp)
  fp.close()
  add_script(redoxTask, "charmm", inp_filename, charmming_config.default_charmm_nprocs)

  # step 4: reduced redox site only
  td['segs'] = [ '%s-bad' % rsite_chain.lower() ]
  td['outname'] = 'redox-%s-redsite' % workstruct.identifier
  td['needpatch'] = False
  t = get_template('%s/mytemplates/input_scripts/redox_makestruct.inp' % charmming_config.charmming_root)
  charmm_inp = output.tidyInp(t.render(Context(td)))
  inp_filename = workstruct.structure.location + '/' + 'redox-%s-build-redsite.inp' % workstruct.identifier
  fp = open(inp_filename, 'w')
  fp.write(charmm_inp)
  fp.close()
  add_script(redoxTask, "charmm", inp_filename, charmming_config.default_charmm_nprocs)

def getdelg_tpl(request, workstruct, redoxTask, knockout, redoxSite):
  td = {}
  try:
    td['srad'] = float(request.POST['srad'])
  except:
    td['srad'] = 1.4

  td['rtf'] = '%s/toppar/top_all22_4fe4s_esp_090209.inp' % charmming_config.data_home
  td['prm'] = '%s/toppar/par_all22_4fe4s_esp_090209.inp' % charmming_config.data_home
  td['id'] = workstruct.identifier
  td['data_home'] = charmming_config.data_home
  td['filebase'] = 'redox-%s' % workstruct.identifier
  td['knockout'] = knockout
  td['siteid'] = redoxSite

  # step 1: generate final grids for full system and redox site
  # a. full sys
  td['psf'] = 'redox-%s-oxiall.psf' % workstruct.identifier
  td['crd'] = 'redox-%s-oxiall.crd' % workstruct.identifier
  td['sizegrid'] = True
  td['grid_name'] = 'pro'
  try:
    td['pdie'] = float(request.POST['prot_diel'])
    td['sdie'] = float(request.POST['solv_diel'])
  except:
    td['pdie'] = 4
    td['sdie'] = 78
  t = get_template('%s/mytemplates/input_scripts/redox_mkgrid.inp' % charmming_config.charmming_root)
  charmm_inp = output.tidyInp(t.render(Context(td)))
  inp_name = '%s/redox-%s-mkgrid-full.inp' % (workstruct.structure.location, workstruct.identifier)
  fp = open(inp_name, 'w')
  fp.write(charmm_inp)
  fp.close()
  add_script(redoxTask, "charmm", inp_name, charmming_config.default_charmm_nprocs)  # change this for APBS? It's parallelizable, I think ~VS

  # b. redox site
  td['psf'] = 'redox-%s-oxisite.psf' % workstruct.identifier
  td['crd'] = 'redox-%s-oxisite.crd' % workstruct.identifier
  td['grid_name'] = 'ref'
  td['sizegrid'] = False
  try:
    # THE FOLLOWING TWO LINES WERE IN CORRECT - BSP
    # td['pdie'] = float(request.POST['prot_diel'])
    # td['sdie'] = float(request.POST['solv_diel'])
    td['pdie'] = float(request.POST['redx_diel'])
    td['sdie'] = float(request.POST['prot_diel'])
  except:
    td['pdie'] = 1
    td['sdie'] = 4
  t = get_template('%s/mytemplates/input_scripts/redox_mkgrid.inp' % charmming_config.charmming_root)
  charmm_inp = output.tidyInp(t.render(Context(td)))
  inp_name = '%s/redox-%s-mkgrid-site.inp' % (workstruct.structure.location, workstruct.identifier)
  fp = open(inp_name, 'w')
  fp.write(charmm_inp)
  fp.close()
  add_script(redoxTask, "charmm", inp_name, charmming_config.default_charmm_nprocs)

  # step 2: use dxmath to generate the final grids
  td['dxmath_inp'] = []
  td['dxmath'] = '%s/apbs-1.1.0/share/tools/mesh/dxmath' % charmming_config.data_home
  for axis in ["x", "y", "z"]:
    dxmath_scr = "combo.%s" % axis
    fp = open(workstruct.structure.location + '/' + dxmath_scr, 'w')
    fp.write("# combine mesh for %s\n" % axis)
    fp.write("%s/ref.%s 4.0 - %s/pro.%s + %s/iapbs-diel%s.dx =\n" % (workstruct.structure.location, axis, workstruct.structure.location, axis, workstruct.structure.location, axis))
    fp.close()
    td['dxmath_inp'].append(dxmath_scr)
  t = get_template('%s/mytemplates/input_scripts/redox_combinegrid.inp' % charmming_config.charmming_root)
  charmm_inp = output.tidyInp(t.render(Context(td)))
  inp_name = '%s/redox-%s-combinegrid-site.inp' % (workstruct.structure.location, workstruct.identifier)
  fp = open(inp_name, 'w')
  fp.write(charmm_inp)
  fp.close()
  add_script(redoxTask, "charmm", inp_name, charmming_config.default_charmm_nprocs)

  td['sizegrid'] = True

  # step 3: for each of redpot, redpotref, modpot, and modpotref generate the
  # delta G via APBS

  td['data_home'] = charmming_config.data_home

  # oxipot
  td['input_file'] = 'redox-%s-oxiall' % workstruct.identifier
  td['rdiel'] = 'rdiel'
  td['pdie'] = 4
  td['sdie'] = 78
  td['site_only'] = False
  td['enefile'] = 'redox-%s-oxipot.txt' % workstruct.identifier
  inp_filename = '%s/redox-%s-oxipot.inp' % (workstruct.structure.location, workstruct.identifier)

  t = get_template('%s/mytemplates/input_scripts/redox_delg.inp' % charmming_config.charmming_root)
  charmm_inp = output.tidyInp(t.render(Context(td)))
  fp = open(inp_filename, 'w')
  fp.write(charmm_inp)
  fp.close()
  add_script(redoxTask, "charmm", inp_filename, charmming_config.default_charmm_nprocs)

  # oxipotref
  td['input_file'] = 'redox-%s-oxisite' % workstruct.identifier
  td['rdiel'] = ''
  td['pdie'] = 1
  td['sdie'] = 1
  td['site_only'] = True
  td['enefile'] = 'redox-%s-oxipotref.txt' % workstruct.identifier
  inp_filename = '%s/redox-%s-oxipotref.inp' % (workstruct.structure.location, workstruct.identifier)

  t = get_template('%s/mytemplates/input_scripts/redox_delg.inp' % charmming_config.charmming_root)
  charmm_inp = output.tidyInp(t.render(Context(td)))
  fp = open(inp_filename, 'w')
  fp.write(charmm_inp)
  fp.close()
  add_script(redoxTask, "charmm", inp_filename, charmming_config.default_charmm_nprocs)

  # modpot
  td['input_file'] = 'redox-%s-redall' % workstruct.identifier
  td['rdiel'] = 'rdiel'
  td['pdie'] = 4
  td['sdie'] = 78
  td['site_only'] = False
  td['enefile'] = 'redox-%s-modpot.txt' % workstruct.identifier
  inp_filename = '%s/redox-%s-modpot.inp' % (workstruct.structure.location, workstruct.identifier)

  t = get_template('%s/mytemplates/input_scripts/redox_delg.inp' % charmming_config.charmming_root)
  charmm_inp = output.tidyInp(t.render(Context(td)))
  fp = open(inp_filename, 'w')
  fp.write(charmm_inp)
  fp.close()
  add_script(redoxTask, "charmm", inp_filename, charmming_config.default_charmm_nprocs)

  # modpotref
  td['input_file'] = 'redox-%s-redsite' % workstruct.identifier
  td['rdiel'] = ''
  td['pdie'] = 1
  td['sdie'] = 1
  td['site_only'] = True
  td['enefile'] = 'redox-%s-modpotref.txt' % workstruct.identifier
  inp_filename = '%s/redox-%s-modpotref.inp' % (workstruct.structure.location, workstruct.identifier)

  t = get_template('%s/mytemplates/input_scripts/redox_delg.inp' % charmming_config.charmming_root)
  charmm_inp = output.tidyInp(t.render(Context(td)))
  fp = open(inp_filename, 'w')
  fp.write(charmm_inp)
  fp.close()
  add_script(redoxTask, "charmm", inp_filename, charmming_config.default_charmm_nprocs)
