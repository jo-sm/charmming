from charmming.lesson_config import lesson_num_lis

import importlib

def validate_lesson_string(lesson_string):
  """
  Validates an incoming lesson string by checking it against the list of lessons
  within lesson_config. This is done because it is absolutely crucial
  that no stray imports find their way into importlib.import_module() calls,
  for maximum security.
  Returns True if valid, and raises a ValueError otherwise.
  """
  try:
    lsplit = lesson_string.split("lesson")
  except:
    # if it fails it either isn't a string, or something else is weird about it.
    raise ValueError("The passed-in value {} is not splittable.".format(lesson_string))
  if len(lsplit) != 2 or lsplit[1] == "" or lsplit[1] not in lesson_num_lis:
    # if it is either XXlesson, contains lesson multiple times, not at all, or the lesson ID is not in lesson_num_lis, discard it
    raise ValueError("Someone is trying to hack our lessons module! Value passed in for lessontype: {}".format(lesson_string))
  return True

def doLessonAct(file, function, task=None, filename=None):
  """
  Fires an event for a particular action in a lesson.
  file is the Structure object that generated the event,
  and function is a string describing the event that was fired.
  Since for most functions, the event firing affects a Task object,
  task is a required argument unless using the getObject function.
  Filename is passed in specifically for docking jobs, to check
  whether the ligand file used for docking was correct.
  """
  lessontype = file.lesson_type
  lessonid = file.lesson_id
  validate_lesson_string(lessontype)
  lesson_num_obj = importlib.import_module('charmming.models.{}'.format(lessontype))()
  lesson_num_class = lesson_num_obj.__class__
  lesson_obj = lesson_num_class.objects.get(id=lessonid)

  if function == 'onFileUpload':
    lesson_obj.onFileUpload()
    return True
  elif function == 'onBuildStructureSubmit':
    lesson_obj.onBuildStructureSubmit(task)
    return True
  elif function == "onDockingSubmit":
    lesson_obj.onDockingSubmit(filename, task)
    return True
  elif function == "onDockingDone":
    lesson_obj.onDockingDone(task)
    return True
  elif function == 'onBuildStructureDone':
    lesson_obj.onBuildStructureDone(task)
    return True
  elif function == 'onMinimizeSubmit':
    lesson_obj.onMinimizeSubmit(task, filename)
    return True
  elif function == 'onMinimizeDone':
    lesson_obj.onMinimizeDone(task)
    return True
  elif function == 'onSolvationSubmit':
    lesson_obj.onSolvationSubmit(task)
    return True
  elif function == 'onSolvationDone':
    lesson_obj.onSolvationDone(task)
    return True
  elif function == 'onMDSubmit':
    lesson_obj.onMDSubmit(task)
    return True
  elif function == 'onMDDone':
    lesson_obj.onMDDone(task)
    return True
  elif function == 'onLDSubmit':
    lesson_obj.onLDSubmit(task)
    return True
  elif function == 'onLDDone':
    lesson_obj.onLDDone(task)
    return True
  elif function == 'onSGLDSubmit':
    lesson_obj.onSGLDSubmit(task)
    return True
  elif function == 'onSGLDDone':
    lesson_obj.onSGLDDone(task)
    return True
  elif function == 'onEnergySubmit':
    lesson_obj.onEnergySubmit(task)
    return True
  elif function == 'onEnergyDone':
    lesson_obj.onEnergyDone(task)
    return True
  elif function == 'onRMSDSubmit':
    lesson_obj.onRMSDSubmit(task)
    return True
  elif function == 'onNATQSubmit':
    lesson_obj.onNATQSubmit(task)
    return True
  elif function == 'onRedoxSubmit':
    lesson_obj.onRedoxSubmit(task)
    return True
  elif function == 'onRedoxDone':
    lesson_obj.onRedoxDone(task)
    return True
  elif function == 'getObject':
    return lesson_obj

def getPDBListFromPostdata(file, postdata):
  filename_list = file.getLimitedFileList('blank')
  pdblist = []
  for i in range(len(filename_list)):
    if filename_list[i] in postdata:
      pdblist.append(filename_list[i])
    elif 'min' in postdata:
      pdblist.append(postdata['min'])
      break
    elif 'solv_or_min' in postdata:
      pdblist.append(postdata['solv_or_min'])
      break
    else:
      continue
  return pdblist


def diffPDBs(file, filename1, filename2):
  file1list = []
  file2list = []

  file1 = open(filename1, 'r')
  for line in file1:
    file1list.append(line.strip())
  file1.close()
  file2 = open(filename2, 'r')
  for line in file2:
    file2list.append(line.strip())
  file2.close()
  if file1list != file2list:
    return False
  return True

def attach_lesson_to_structure(struct, postdata, lesson_obj):
  """
  Attaches a new Lesson to a Structure that is being created and
  fires an onFileUpload event.
  Assumes that the POST data has the lesson type. We validate
  again just in case - we don't want any weirdness.
  """
  lnum = postdata["lesson"]
  validate_lesson_string(lnum)
  struct.lesson_type = lnum
  struct.lesson_id = lesson_obj.id
  struct.save()
  doLessonAct(struct, "onFileUpload", postdata, "")

def get_lesson_object(lnum):
  """
  Takes as input a lesson type, validates it, and either returns an instance of that
  particular lesson's LessonX() object, or None if the validation fails.
  """
  try:
    validate_lesson_string(lnum)
    # example result for the below line: import lesson3.models.Lesson3()
    lesson_obj = importlib.import_module(lnum).models.__dict__[lnum.capitalize()]()
  except ValueError:
    lesson_obj = None
  return lesson_obj
