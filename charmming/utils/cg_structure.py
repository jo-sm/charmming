from pychm3.io.pdb import PDB
from pychm3.cg.sansombln import SansomBLN
from pychm3.cg.ktgo import KTGo

import os

def associate_segments_with_cg_structure(structure, segments):
  """
  Adds segments to the CG structure. If there is a "pro" segment
  the segment is copied and some segment properties are changed
  to work with a CG structure.
  """

  segments_list = []

  for i, segment in enumerate(segments):
    if not segment.name.find('-pro') > -1:
      segments_list.append(segment)

    copy = segment.copy()
    copy.name = copy.name.replace('-pro', '-{}'.format(structure.cg_model_type))
    copy.type = structure.cg_model_type
    copy.default_patch_first = 'None'
    copy.default_patch_last = 'None'

    copy.save()

    segments_list.append(copy)

  structure.segments.clear()
  structure.segments.add(*segments)

  return True

def handle_cg(structure, type, **kwargs):
  pdb_filename = 'cg-base.pdb'.format(structure.id)

  full_path = structure.save_file(pdb_filename, "")

  pdb = structure.get_pdb(full=True)
  model = pdb[structure.model]

  segment_ids = [segment.name for segment in structure.segment_set.all()]

  for segment in model.segments:
    if segment.id in segment_ids:
      segment.write(full_path, outformat='charmm', append=True, ter=True, end=False)

  file_io = open(full_path, 'a')
  file_io.write('\nEND\n')
  file_io.close()

  model = PDB(full_path)[0]

  if type == 'go':
    cg_model = KTGo(model)
    cg_model.nScale = kwargs.get('nscale')
    cg_model.contactSet = kwargs.get('contact_type')
    cg_model.kBond = kwargs.get('bond_force_const')
    cg_model.kAngle = kwargs.get('angle_force_const')
    cg_model.contactrad = kwargs.get('contact_radius')

  elif type == 'bln':
    cg_model = SansomBLN(model)
    cg_model.kBondHelix = kwargs.get('helix_bond_force')
    cg_model.kBondSheet = kwargs.get('b_sheet_bond_force')
    cg_model.kBondCoil = kwargs.get('coil_bond_force')
    cg_model.kAngleHelix = kwargs.get('helix_angle_force')
    cg_model.kAngleSheet = kwargs.get('b_sheet_angle_force')
    cg_model.kAngleCoil = kwargs.get('coil_angle_force')

  # Write CG model files
  cg_model.write_pdb(os.path.join(path, "{}.pdb".format(type)))
  cg_model.write_rtf(os.path.join(path, "{}.rtf".format(type)))
  cg_model.write_prm(os.path.join(path, "{}.prm".format(type)))

  for segment in list(structure.segments.all()):
    segment.rtf_list = os.path.join(path, "{}.rtf".format(type))
    segment.prm_list = os.path.join(path, "{}.prm".format(type))
    segment.save()

  # TODO: Update pychm to return value instead of writing to file
  # to skip this file_io nonsense

  file_io = open(os.path.join(path, "{}.pdb".format(type)), 'r')
  file = file_io.read()
  file_io.close()

  structure.save_pdb(file)

  return True
