from django.shortcuts import render_to_response

from charmming import charmming_config
from charmming.utils.lesson import get_lesson_object, doLessonAct
from charmming.models import Task
from charmming.utils.script import process_postdata, setup_scripts
from charmming.utils.task import start_task, add_script

import re

def calcEnergy_tpl(request, workstruct, pTaskID, eobj):
  pTask = Task.objects.get(id=pTaskID)
  template_dict = {}
  eobj.finale = 0.0

  # non-energy-specific stuff
  try:
    template_dict = process_postdata(template_dict, eobj, pTask, request)
  except Exception as e:
    return {
      'error': {
        'message': 'There was an error calculating the energy. Please consult the CHARMMing administrators.'
      }
    }

  energy_input = setup_scripts(template_dict, eobj, "calcEnergy_template.inp")
  if eobj.useqmmm == "y" and template_dict["modelType"] == "oniom":
    add_script(eobj, "charmm-mscale", energy_input, charmming_config.default_mscale_nprocs)
  else:
    add_script(eobj, "charmm", energy_input, charmming_config.default_charmm_nprocs)
  eobj.modifies_coordinates = False  # We set it here, because it doesn't stick in the finish method, and it doesn't stick in the method that calls this one
  # For some very strange and unknown reason, the field is modified when this method (the one we are in) is called.
  # We force-set it to false just in case.
  eobj.save()

  # YP lessons status update
  lesson_obj = get_lesson_object(workstruct.structure.lesson_type)
  if lesson_obj:
    doLessonAct(eobj.workstruct.structure, "onEnergySubmit", eobj, "")
  start_task(eobj, template_dict)

  # loop until the energy calculation is finished
  # TODO: Eliminate this? It seems a bit silly since we do .query() with updateActionStatus anyway...
  while True:
    sstring = eobj.query()
    if "complete" in sstring:
      break
    if "failed" in sstring:
      return {
        'error': {
          'message': 'Energy calculation failed. Please check output from {}.'.format(pTask.action)
        }
      }

  # for now, I am not going to create a workstructure for the energy since it's effectively a
  # no-op; talk to Lee about whether this should be done. It probably should be, at one point
  # VS note: We should do this UNTIL we make build an actual part of the build working structure page. At that point
  # there's no reason to carry it around for any reason other than having it as a pTask, which shouldn't matter.
  workstruct.save()

  return parseEnergy(workstruct, energy_input, eobj)

def parseEnergy(workstruct, input_filename, enerobj=None):
  # This I believe is used for the Energy render AJAX request. It's weird and a holdover from oldcharmming.
  """
  Takes as input a working structure, the path to the energy input script, and
  the energy Task object, and produces an HTML templated with
  the output from ENERGY job.
  """

  outfp = open(input_filename.replace(".inp", ".out"), "r")
  ener = re.compile('ENER ENR')
  dash = re.compile('----------')
  # charmm = re.compile('CHARMM>')
  initiate = 0
  # the second extern marks the end of the energy output
  dash_occurance = 0
  energy_lines = ''

  for line in outfp:
    line = line.lstrip()
    if line.startswith('ENER>'):
      enersplit = line.split()
      # ok so USUALLY split[2] is the right answer.
      # but it's not always, because remember FORTRAN is centered on character positions
      # therefore if we want to make this work we have to make sure that splitfields[1] isn't just
      # something really long or with a negative sign
      # that just happens to be a real number. Therefore, we set it up
      enernum = 0.0
      if str(enersplit[1]) == "0":  # if it is just 0 by itself, we good
        enernum = float(enersplit[2])
      else:
        # otherwise split it up
        enernum = float(enersplit[1][1:])  # get the first character onward
      enerobj.finale = enernum  # it's already a float so let's not re-convert
      enerobj.save()
    if(dash_occurance == 2 or (initiate > 0 and line.strip().startswith('CHARMM>'))):
      if(line.startswith('CHARMM>')):
        break
      energy_lines += line
      break
    if(ener.search(line) or initiate > 0):
      if(initiate == 0):
        initiate = 1
      energy_lines += line
    if(dash.search(line) and initiate > 0):
      dash_occurance += 1
  outfp.close()
  writefp = open(workstruct.structure.location + '/' + workstruct.identifier + '-energy.txt', 'w')
  writefp.write(energy_lines)
  writefp.close()

  return render_to_response('html/displayenergy.html', {'linelist': energy_lines})
