from charmming.models import WorkingFile, CGWorkingStructure, Task, DataPoint, MinimizeTask, SolvationTask, MdTask, LdTask, SgldTask, EnergyTask, NmodeTask, RedoxTask, MutateTask, QchemTask, Patch, Segment, WorkingSegment, Script, LessonMakerLesson
from charmming.utils.lesson import doLessonAct, get_lesson_object
from charmming.utils.cg_working_structure import addBondsToPDB
from charmming.exceptions import NoNscaleFound
from charmming import charmming_config
from charmming.helpers import output
from charmming.utils.working_segment import build as build_working_segment
from charmming.utils.lesson_maker import onTaskDone
from charmming.utils.script import get_input_script

import os
import shutil

def updateActionStatus(working_structure):
  # appending is a special case, since it doesn't exist as a task unto
  # working_structure. So if the structure is not built, we should check and see
  # whether or not that happened.
  working_structure.lock()  # this way we skip EVERYTHING and make sure no multi-threads exist
  if working_structure.isBuilt != 't':
    # check if the PSF and CRD files for this structure exist
    # If build did not complete, we need to report as failed
    try:
      os.stat(working_structure.structure.location + '/' + working_structure.identifier + '-build.psf')
      os.stat(working_structure.structure.location + '/' + working_structure.identifier + '-build.crd')
      os.stat(working_structure.structure.location + '/' + working_structure.identifier + '-build.pdb')
    except:
      pass
    else:
      working_structure.isBuilt = 't'
      working_structure.addCRDToPickle(working_structure.structure.location + '/' + working_structure.identifier + '-build.crd', 'append_' + working_structure.identifier)
      loc = working_structure.structure.location
      bnm = working_structure.identifier

      # add the working files to the quasi appending Task
      buildtask = Task.objects.get(workstruct=working_structure, finished='n', action='build')

      wfinp = WorkingFile()
      path = loc + '/' + bnm + '-build.inp'
      try:
        WorkingFile.objects.get(task=buildtask, path=path)
        # If there is NOT a WorkingFile at this path associated to THIS task, keep going, otherwise don't create another one in the DB
        # Since the ID is unique you'll end up with several copies of the same file
      except:
        wfinp.task = buildtask
        wfinp.path = path
        wfinp.canonPath = wfinp.path
        wfinp.type = 'inp'
        wfinp.description = 'Build script input'
        wfinp.save()

      wfout = WorkingFile()
      path = loc + '/' + bnm + '-build.out'
      try:
        WorkingFile.objects.get(task=buildtask, path=path)
      except:
        wfout.task = buildtask
        wfout.path = path
        wfout.canonPath = wfout.path
        wfout.type = 'out'
        wfout.description = 'Build script output'
        wfout.save()

      wfpsf = WorkingFile()
      path = loc + '/' + bnm + '-build.psf'
      try:
        WorkingFile.objects.get(task=buildtask, path=path)
      except:
        wfpsf.task = buildtask
        wfpsf.path = loc + '/' + bnm + '-build.psf'
        wfpsf.canonPath = wfpsf.path
        wfpsf.type = 'psf'
        wfpsf.description = 'Build script PSF'
        wfpsf.save()

      wfpdb = WorkingFile()
      path = loc + '/' + bnm + '-build.pdb'
      try:
        WorkingFile.objects.get(task=buildtask, path=path)
      except:
        wfpdb.task = buildtask
        wfpdb.path = loc + '/' + bnm + '-build.pdb'
        wfpdb.canonPath = wfpdb.path
        wfpdb.type = 'pdb'
        wfpdb.description = 'Build script PDB'
        wfpdb.save()

      wfcrd = WorkingFile()
      path = loc + '/' + bnm + '-build.crd'
      try:
        WorkingFile.objects.get(task=buildtask, path=path)
      except:
        wfcrd.task = buildtask
        wfcrd.path = loc + '/' + bnm + '-build.crd'
        wfcrd.canonPath = wfcrd.path
        wfcrd.type = 'crd'
        wfcrd.pdbkey = 'append_' + working_structure.identifier
        wfcrd.description = 'Build script CRD'
        wfcrd.save()
      inp_file = loc + "/" + bnm + "-build.psf"
      out_file = inp_file.replace("psf", "pdb")
      cgws = False
      try:
        cgws = CGWorkingStructure.objects.get(workingstructure_ptr=buildtask.workstruct.id)
      except:
        pass
      PDB_bond_built = 42  # arbitrary non-Boolean value
      if cgws is not False:
        try:
          PDB_bond_built = addBondsToPDB(inp_file, out_file)  # This is highly unlikely to fail, but just in case...
        except Exception as ex:
          pass

      if not (PDB_bond_built == 42 or PDB_bond_built is True):  # Bad coordinates were generated for a CG model.
        buildtask.status = 'F'
      else:
        buildtask.status = 'C'
      buildtask.finished = 'y'
      buildtask.save()
     # buildtask.workstruct.unlock()
      buildtask.workstruct.save()
      datapoint = DataPoint()
      datapoint.task_id = int(buildtask.id)
      datapoint.task_action = str(buildtask.action)  # Make these into primitives so it doesn't try to foreign key them, just in case
      datapoint.user = str(buildtask.workstruct.structure.owner.username)
      datapoint.structure_name = str(buildtask.workstruct.structure.name)
      datapoint.success = True if buildtask.status == 'C' else False
      datapoint.struct_id = buildtask.workstruct.structure.id
      datapoint.save()

  tasks = Task.objects.filter(workstruct=working_structure, active='y', finished='n')
  # YP lesson stuff
  lesson_obj = get_lesson_object(working_structure.structure.lesson_type)
  # YP
  if working_structure.structure.lessonmaker_active:
    lessonmaker_obj = LessonMakerLesson.objects.filter(structure=working_structure.structure)[0]  # there's no reason why this should fail
  for t in tasks:
    t.query()

    # TODO: DRY this
    if t.status == 'C' or t.status == 'F':
      if t.action == 'minimization':
        t2 = MinimizeTask.objects.get(id=t.id)
        if lesson_obj:
          doLessonAct(working_structure.structure, "onMinimizeDone", t)
      elif t.action == 'solvation' or t.action == 'neutralization':
        t2 = SolvationTask.objects.get(id=t.id)
        if lesson_obj:
          doLessonAct(working_structure.structure, "onSolvationDone", t)
      elif t.action == 'md':
        if lesson_obj:
          doLessonAct(working_structure.structure, "onMDDone", t)
        t2 = MdTask.objects.get(id=t.id)
      elif t.action == 'ld':
        if lesson_obj:
          doLessonAct(working_structure.structure, "onLDDone", t)
        t2 = LdTask.objects.get(id=t.id)
      elif t.action == 'sgld':
        if lesson_obj:
          doLessonAct(working_structure.structure, "onSGLDDone", t)
        t2 = SgldTask.objects.get(id=t.id)
      elif t.action == 'energy':
        t2 = EnergyTask.objects.get(id=t.id)
        if lesson_obj:
          doLessonAct(working_structure.structure, "onEnergyDone", t2)
      elif t.action == 'nmode':
        t2 = NmodeTask.objects.get(id=t.id)
        if lesson_obj:
          doLessonAct(working_structure.structure, "onNMADone", t2)
      elif t.action == 'redox':
        t2 = RedoxTask.objects.get(id=t.id)
        if lesson_obj:
          doLessonAct(working_structure.structure, "onRedoxDone", t)
      elif t.action == 'mutation':
        t2 = MutateTask.objects.get(id=t.id)
      elif "qchem" in t.action:
        t2 = QchemTask.objects.get(id=t.id)
      elif t.action == "dsfdocking":
        # ddtask objects have no special properties, just special methods
        # t2 = legacy_models.dd_infrastructure.ddtask.objects.get(id=t.id)
        t2 = t
        try:
          if lesson_obj:
            doLessonAct(working_structure.structure, "onDockingDone", t)
        except:
          pass
      else:
        t2 = t
      try:
        if lessonmaker_obj:
          onTaskDone(lessonmaker_obj, t2)  # t2 means less queries to render the templates
      except:
        pass
      # lessonmaker only needs Task, not the specific subclass you're using at the moment
      # maybe we should save lessonmaker_obj?? it saves in onTaskDone...
      t.finished = 'y'
      t.save()
      t2.finished = 'y'
      try:
        t2.finish()
      except:
        pass
      try:
        DataPoint.objects.get(task_id=t.id)  # Avoids mysterious duplicates
      except:
        t.createStatistics()  # In theory this is generic
      t2.save()
  if working_structure.isBuilt == "t":
    working_structure.unlock()  # get this out so that the task manager can go from running to done
    # fcntl.lockf(lockfp,fcntl.LOCK_UN)
    # lockfp.close()
    #    Commented out regions above are old locking system. Will attempt new one.

  working_structure.save()

# The following code is (as of Oct, 2014) duplicated in way too many places (basically once
# for each task). Let's move it here.
def getParent(working_structure, request, mytask):
  if working_structure.isBuilt != 't':
    pTask = build(working_structure, mytask)
  elif 'ptask' in request.POST:
    pTaskID = int(request.POST['ptask'])
    pTask = Task.objects.get(id=pTaskID)
    ptask_path = "%s/%s-%s.psf" % (working_structure.structure.location, working_structure.identifier, pTask.action)
    try:
      os.stat(ptask_path)
    except:  # probably a qchem thing, so copy the build PSF
      # shutil.copyfile("%s/%s-build.psf"%(struct.location,ws.identifier),ptask_path) #TODO: warn user?
      # shutil.copyfile("%s/%s-build.crd"%(struct.location,ws.identifier),ptask_path.replace(".psf",".crd")) #TODO: warn user?
      shutil.copyfile("%s/%s-build.psf" % (working_structure.structure.location, working_structure.identifier), ptask_path)  # TODO: warn user?
      shutil.copyfile("%s/%s-build.crd" % (working_structure.structure.location, working_structure.identifier), ptask_path.replace(".psf", ".crd"))  # TODO: warn user
      # crd doesn't change for qchem stuff...
  else:
    # OK, we have a structure that is built, supposedly, but no parent task. Let's reference the fake
    # build task we created. If this fails, we're out of luck...
    try:
      pTask = Task.objects.get(active='y', workstruct=working_structure, action='build')
    except:
      pTask = None

  return pTask

def getAppendPatches(working_structure):
  """
  Get a list of all the patches that need to be applied at append
  time. For now, this means disulfide patches, but it can be used
  for other types of patching such as for FeS4 clusters in the
  future.
  """
  plist = Patch.objects.filter(structure=working_structure)
  plines = ''
  for patch in plist.all():
    if patch.patch_segid:
      continue # not a structure-wide patch
    plines += 'patch %s %s\n' % (patch.patch_name, patch.patch_segres)

  return plines

def associate(working_structure, structref, segids, tpdict):
  """
  use @transaction.commit_manually
  and see Django docs
  """

  for sid in segids:
    working_structure.structure = structref
    working_structure.save()
    segobj = Segment.objects.filter(name=sid, structure=structref)[0]

    # right now I can't think of any better way to copy all the data
    # from the segment object to the working segment object, so we just
    # do it by hand.
    wseg = WorkingSegment()
    wseg.is_working = 'y'
    wseg.structure = segobj.structure
    wseg.name = segobj.name
    wseg.type = segobj.type
    wseg.is_custom = segobj.is_custom
    wseg.default_patch_first = segobj.default_patch_first
    wseg.default_patch_last = segobj.default_patch_last
    wseg.stream_list = segobj.stream_list
    wseg.tpMethod = tpdict[sid]

    if wseg.tpMethod == 'standard':
      wseg.rtf_list = segobj.rtf_list
      wseg.prm_list = segobj.prm_list
    elif wseg.tpMethod == 'upload':
      wseg.rtf_list = working_structure.structure.location + '/' + working_structure.identifier + '-' + wseg.name + '.rtf'
      wseg.prm_list = working_structure.structure.location + '/' + working_structure.identifier + '-' + wseg.name + '.prm'
    elif wseg.tpMethod == 'redox':
      # structure will be used ONLY for oxi/reduce calculations so doesn't need
      # top/par (redox script provides these when needed).
      wseg.rtf_list = ''
      wseg.prm_list = ''
      wseg.redox = True
    else:
      # custom built topology/parameter files will be handled at build time
      wseg.rtf_list = ''
      wseg.prm_list = ''

    wseg.save()

    working_structure.segments.add(wseg)
    working_structure.save()

def build(working_structure, inTask):
  """
  This method replaces minimization.append_tpl() -- it is the explicit
  step that builds a new structure and appends it to the PDB object
  in charmminglib.
  """

  tdict = {}
  # step 1: check if all segments are built
  tdict['seg_list'] = []
  tdict['nonhet_seg_list'] = []
  tdict['het_seg_list'] = []
  tdict['output_name'] = working_structure.identifier + '-build'
  tdict['blncharge'] = False # we're not handling BLN models for now

  # check if we're a CG model.
  try:
    cgws = CGWorkingStructure.objects.get(workingstructure_ptr=working_structure.id)
  except:
    cgws = None

  if cgws:

    havenscale = False
    try:
      os.stat('%s/lock-%s-go.txt' % (working_structure.structure.location, working_structure.identifier))
    except:
      havenscale = True

    if not havenscale:
      raise NoNscaleFound()

    tdict['input_pdb'] = cgws.pdbname
    tdict['finalname'] = cgws.cg_type
  else:
    # We are not a CG structure... go ahead and build up the seg_list
    # as normal.
    try:
      pfile = "%s/%s.dat" % (working_structure.structure.location, working_structure.identifier)
      os.stat(pfile)  # try and find the local pickle
      # have_pickle = True
      working_structure.localpickle = pfile
      working_structure.save()
    except:
      pass
    # all_segs = ""  # let's make it so we only save the WS once
    working_structure.topparStream = ''
    for segobj in working_structure.segments.all():
      # segment to be used solely for redox calculations get
      # handled differently
      if segobj.isBuilt != 't' and not segobj.redox:
        newScript = build_working_segment(segobj, working_structure.modelName, working_structure)
        all_scripts = Script.objects.filter(task=inTask).order_by("-order")
        for script in all_scripts:
          script.order = script.order + 1
        # have to do this custom because it MUST come before the other tasks
        # and then need to make sure segment stuff also executes in the right order
        new_bscript = Script()
        new_bscript.task = inTask
        new_bscript.scriptname = newScript
        new_bscript.order = 1
        new_bscript.executable = charmming_config.apps['charmm']
        new_bscript.processors = charmming_config.default_charmm_nprocs
        new_bscript.save()
      if not segobj.redox:
        if segobj.type in ['pro', 'dna', 'rna']:
          tdict['nonhet_seg_list'].append(segobj)
        else:
          tdict['het_seg_list'].append(segobj)

      if segobj.stream_list:
        for item in segobj.stream_list.split():
          if item not in working_structure.topparStream:
            # hackish, do we want to be more thorough?
            working_structure.topparStream += ' %s' % item
    # end for loop over all segs
    # working_structure.topparStream = all_segs
    working_structure.save()

  tdict['seg_list'] = tdict['nonhet_seg_list'] + tdict['het_seg_list']
  tdict['topology_list'], tdict['parameter_list'] = working_structure.getTopparList()
  if working_structure.topparStream:
    tdict['tpstream'] = list(set(working_structure.topparStream.split()))
  else:
    tdict['tpstream'] = []
  tdict['patch_lines'] = getAppendPatches(working_structure)

  # if qrebuild:
  #    tdict['rebuild'] = True
  #    segtuplist = []
  # for segobj in working_structure.segments.all():
  # if segobj.type == 'good':
  #            special = 'noangle nodihedral'
  # else:
  #            special = ''
  # segtuplist.append((segobj.name,segobj.builtCRD.replace('.crd','.pdb'),segobj.patch_first,segobj.patch_last,special))
  #    tdict['segbuild'] = segtuplist
  # else:
  #    tdict['rebuild'] = False

  t = get_input_script('append.inp')
  charmm_inp = output.tidyInp(t.render(tdict))

  os.chdir(working_structure.structure.location)
  charmm_inp_filename = working_structure.structure.location + "/" + working_structure.identifier + "-build.inp"
  charmm_inp_file = open(charmm_inp_filename, 'w')
  charmm_inp_file.write(charmm_inp)
  charmm_inp_file.close()
  all_scripts = Script.objects.filter(task=inTask).order_by("-order")
  for script in all_scripts:
    script.order = script.order + 1
  build__script = Script()
  build__script.task = inTask
  build__script.scriptname = charmm_inp_filename
  build__script.executable = charmming_config.apps['charmm']
  build__script.processors = charmming_config.default_charmm_nprocs
  build__script.order = 1
  build__script.save()
 # inTask.scripts += ',%s' % charmm_inp_filename
  inTask.save()

  # create a Task object for appending; this has no parent
  task = Task()
  task.setup(working_structure)
  task.parent = None
  task.action = 'build'
  task.active = 'y'
  task.save()

  working_structure.save()

  return task
