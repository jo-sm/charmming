def make_float(value, default):
  try:
    return float(value)
  except ValueError:
    return default
