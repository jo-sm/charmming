from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import Group
from django.core.mail import mail_admins

from charmming import charmming_config
from charmming.models import StudentProfile

import os

def registerStudent(request, user):
  # added temporarily to get rid of teacher/student stuff
  # This may be completely messed up - the indentation was horrible according to Vim
  # So I took actuon and added error messages. If something is broken, that's probably why!
  # So restore the old copy of this file from production. ~VS
  request.user = user
  if user.is_authenticated():
    if(request.POST):
      # the explanation for why the teacher will use the interface
      # is saved in the directory ~/teachers/teacher_user_name/
      new_student = StudentProfile(student=request.user)
      new_student.institute = request.POST['institute']
      if new_student.institute.strip() == '':
        user.delete()
        messages.error(request, 'Institute name is required.')
        return HttpResponseRedirect("/accounts/register/student/")

      if request.POST['charmmlicense'] == "No":
        new_student.charmm_license = 0
      else:
        new_student.charmm_license = 1
      # Stores user object of the student's teacher
      # students_teacher_user = User.objects.filter(username=request.POST['teacher_name'])[0]
      # students_teacher_user = User.objects.filter(id=1)[0]
      # Gets the teacher Profile of the teacher
      # students_teacher = teacherProfile.objects.filter(teacher = students_teacher_user)[0]
      # new_student.teacher = students_teacher
      # Now to set the students classroom
      # teacher.teacher.username is the teacher's username to log into the site
      # commented out because getting rid of teacher/student thing
      # classname = request.POST[new_student.teacher.teacher.username]
      # new_student.classroom = classroom.objects.filter(name=classname)[0]
      # classname = "General Public"
      new_student.student.username = new_student.student.username.lower()
      new_student.save()
      # New student needs a folder
      os.mkdir("%s/%s" % (charmming_config.user_home, new_student.student.username))
      os.chmod("%s/%s" % (charmming_config.user_home, new_student.student.username), 0o775)

      # preapprove = Group.objects.get(name='preapprove')
      student = Group.objects.get(name='student')
      # request.user.groups.add(preapprove)
      request.user.groups.add(student)

      mail_message = """
      New student registered.
      Username:""" + new_student.student.username + """
      Name:""" + new_student.student.first_name + " " + new_student.student.last_name + """
      E-mail: """ + new_student.student.email + """
      Institute:""" + new_student.institute + """
      CHARMM License?:""" + request.POST['charmmlicense']
      mail_admins('new student', mail_message, fail_silently=False)
      return HttpResponse('Your account has been configured. You may now access CHARMMing.')
    # return render_to_response('registration/registerstudent.html')
  return HttpResponse('Please go through the registration page first.')
