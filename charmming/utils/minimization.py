from django.http import HttpResponseRedirect
from django.contrib import messages

from charmming import charmming_config
from charmming.models import Task
from charmming.helpers import output
from charmming.utils.script import process_postdata, setup_scripts
from charmming.utils.task import start_task, add_script
from charmming.utils.lesson import get_lesson_object, doLessonAct

import os

def minimize_tpl(request, mp, pTaskID):
  postdata = request.POST

  # change the status of the file regarding minimization
  sdsteps = postdata['sdsteps']
  abnr = postdata['abnrsteps']
  tolg = postdata['tolg']
  os.chdir(mp.workstruct.structure.location)
  pTask = Task.objects.filter(id=pTaskID)[0]
  # fill in the model for the minimization
  try:
    mp.sdsteps = int(sdsteps)
    mp.abnrsteps = int(abnr)
    mp.tolg = tolg
  except:
    return output.returnSubmission('Minimization', error='Form not filled out.')

  if mp.sdsteps > charmming_config.minimize_steplimit or mp.abnrsteps > charmming_config.minimize_steplimit:
    return output.returnSubmission('Minimization', error='Minimization has a limit of %d steps.' % charmming_config.minimize_steplimit)

  # template dictionary passes the needed variables to the template
  template_dict = {}

  # Minimization-specific section
  template_dict['sdsteps'] = sdsteps
  template_dict['abnr'] = abnr
  template_dict['tolg'] = tolg

  template_dict['restraints'] = ''
  try:
    postdata['apply_restraints']
    # FIXME: this doesn't exist anywhere
    # template_dict['restraints'] = file.handleRestraints(request)
  except:
    pass
  if 'fixnonh' in postdata:
    template_dict['fixnonh'] = 1
  else:
    template_dict['fixnonh'] = 0
  # handles backbone restraints
  if 'restrainbb' in request.POST:
    template_dict['restrainbb'] = True
    template_dict["bbforce"] = float(postdata['bbforce']) if 'bbforce' in postdata else 100.0
  else:
    template_dict['restrainbb'] = False

  # non-mini-specific section
  try:
    template_dict = process_postdata(template_dict, mp, pTask, request)
  except Exception as e:
    # If we got here, an Exception got raised which probably tells us of a parameter mismatch.
    messages.error(request, str(e))
    return HttpResponseRedirect("/minimize/")
  minimize_filename = setup_scripts(template_dict, mp, "minimization_template.inp")
  if mp.useqmmm == 'y' and template_dict["modelType"] == 'oniom':  # tdict gets updated by process_postdata
    add_script(mp, "charmm-mscale", minimize_filename, charmming_config.default_mscale_nprocs)
  else:
    add_script(mp, "charmm", minimize_filename, 1)
  start_task(mp, template_dict)
  # NOTE: This cannot be abstracted out as the parameters change depending on task type.
  lesson_obj = get_lesson_object(mp.workstruct.structure.lesson_type)
  if lesson_obj:
    doLessonAct(mp.workstruct.structure, "onMinimizeSubmit", mp, "")
  mp.workstruct.save()
  return output.returnSubmission('Minimization')
