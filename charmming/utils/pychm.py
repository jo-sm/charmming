import json

def pychm_pdb_to_json(pdb):
  """
  Transforms a PDB model into JSON.
  """

  pdb_json = {}

  pdb_json['code'] = pdb.code

  for model in pdb.models:
    pdb_json[model.name] = {
      # 'mass': model.mass,
      # 'centerOfMass': [str(i) for i in model.com],
      'segments': [{
        'id': segment.id,
        'diameter': segment.diameter,
        # 'mass': segment.mass,
        'type': segment.type,
        'residues': [{
          'name': residue.name,
          # 'mass': residue.mass,
          'id': residue.id,
          'atoms': [{
            'element': atom.element,
            'bFactor': atom.b_factor,
            'is_backbone': atom.is_backbone(),
            # 'mass': atom.mass,
            'tag': atom.tag,
            'x': atom.cart[0],
            'y': atom.cart[1],
            'z': atom.cart[2]
          } for atom in residue.atoms]
        } for residue in segment.residues]
      } for segment in model.segments]
    }

  return json.dumps(pdb_json)
