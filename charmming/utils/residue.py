from http.client import HTTPConnection

import openbabel
import re

def get_sdf(residue):
  conn = HTTPConnection("www.rcsb.org")
  reqstring = "/pdb/files/ligand/{}_ideal.sdf".format(residue.name.upper())
  conn.request("GET", reqstring)
  resp = conn.getresponse()
  if resp.status == 200:
    raw_resp = resp.read()

    return raw_resp.decode('utf-8')
  else:
    residue_text = residue.to_str(format='pdborg')
    obconv = openbabel.OBConversion()
    mol = openbabel.OBMol()
    mol.SetTitle(residue.name)

    obconv.SetInAndOutFormats("pdb", "sdf")
    obconv.ReadString(mol, residue_text)
    return obconv.WriteString(mol)

def is_very_bad(residue):
  sdf_re = re.compile(" +[0-9]+\.[0-9]+ +[0-9]+\.[0-9]+ +[0-9]+\.[0-9]+ +(?!H )[A-Z]+ +[0-9]+ +[0-9]+ +[0-9]+ +[0-9]+ +[0-9]+")

  sdf = get_sdf(residue)

  sdf_atoms = len(list(filter(lambda line: sdf_re.match(line), sdf.split("\n"))))
  residue_atoms = len(residue.atoms)

  if sdf_atoms != residue_atoms:
    return True

  return False
