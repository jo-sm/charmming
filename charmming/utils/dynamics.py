from django.http import HttpResponseRedirect
from django.contrib import messages

from charmming import charmming_config
from charmming.models import SolvationTask
from charmming.helpers import output
from charmming.utils.task import start, add_script
from charmming.utils.lesson import get_lesson_object, doLessonAct
from charmming.utils.script import get_input_script

import os
import shutil

def applyld_tpl(request, ldt, pTask):
  postdata = request.POST

  # template dictionary passes the needed variables to the template
  template_dict = {}
  template_dict['topology_list'], template_dict['parameter_list'] = ldt.workstruct.getTopparList()
  template_dict['fbeta'] = postdata['fbeta']
  template_dict['nstep'] = postdata['nstep']
  template_dict['usesgld'] = 'usesgld' in postdata
  template_dict['identifier'] = ldt.workstruct.identifier
  if ldt.workstruct.topparStream:
    template_dict['tpstream'] = list(set(ldt.workstruct.topparStream.split()))
  else:
    template_dict['tpstream'] = []

  if int(template_dict['nstep']) > charmming_config.dyna_steplimit:
    return output.returnSubmission('LD', error='Dynamics has a limit of %d steps.' % charmming_config.dyna_steplimit)

  ldt.fbeta = template_dict['fbeta']
  ldt.nstep = template_dict['nstep']

  # pTask = Task.objects.get(id=pTaskID)
  ldt.parent = pTask
  template_dict['input_file'] = ldt.workstruct.identifier + '-' + pTask.action
  template_dict['useqmmm'] = ''
  template_dict['qmmmsel'] = ''
  ldt.save()

  # deal with whether or not we want to go to Hollywood (i.e. make a movie)
  # NB: this is not working at present; flesh this code in!!!
  # try:
  #   make_movie = postdata['make_movie']
  # except:
  #   make_movie = None

  # If the user wants to solvate implicitly the scpism line is needed
  # 84 will be the scpism number in this program
  impsolv = ''
  try:
    impsolv = postdata['solvate_implicitly']
    if impsolv == 'scpism':
      ldt.scpism = True
  except:
    impsolv = 'none'
  template_dict['impsolv'] = impsolv
  template_dict['impsolv'] = impsolv

  template_dict['shake'] = 'apply_shake' in request.POST
  if 'apply_shake' in request.POST:
    template_dict['which_shake'] = postdata['which_shake']
    # WTF is this here? --> template_dict['qmmmsel'] = mp.qmmmsel

    if postdata['which_shake'] == 'define_shake':
      template_dict['shake_line'] = postdata['shake_line']
      if postdata['shake_line'] != '':
        input.checkForMaliciousCode(postdata['shake_line'], postdata)

  if ldt.action == 'sgld':
    tsgavg = '0.0'
    tempsg = '0.0'

    tsgavg = postdata['tsgavg']
    ldt.tsgavg = str(tsgavg)
    tempsg = postdata['tempsg']
    ldt.tempsg = str(tempsg)

    template_dict['tsgavg'] = tsgavg
    template_dict['tempsg'] = tempsg

  # determine whether periodic boundary conditions should be applied, and if
  # so pass the necessary crystal and image parameters
  dopbc = False
  if 'usepbc' in request.POST:
    if impsolv != 'none':
      return output.returnSubmission('Langevin dynamics', error='Conflicting solvation options chosen')

    # decide if the structure we're dealing with has
    # been solvated.
    solvated = False
    wfc = pTask
    while True:
      if wfc.action == 'solvation' or wfc.action == 'neutralization':
        solvated = True
        break
      if wfc.parent:
        wfc = wfc.parent
      else:
        break
    if not solvated:
      return output.returnSubmission('Langevin dynamics', error='Requested PBC on unsolvated structure')

    dopbc = True
    try:
      sp = SolvationTask.objects.filter(workstruct=ldt.workstruct, active='y')[0]
    except:
      return output.returnSubmission('Langevin dynamics', error="Could not find solvation parameters")
    ldt.usepbc = True
    ldt.save()
    template_dict['xtl_x'] = sp.xtl_x
    template_dict['xtl_y'] = sp.xtl_y
    template_dict['xtl_z'] = sp.xtl_z
    template_dict['xtl_angles'] = "%10.6f %10.6f %10.6f" % (sp.angles[0], sp.angles[1], sp.angles[2])
    template_dict['xtl_ucell'] = sp.solvation_structure
    template_dict['ewalddim'] = sp.calcEwaldDim()

  template_dict['dopbc'] = dopbc
  template_dict['output_name'] = ldt.workstruct.identifier + '-' + ldt.action
  # user_id = ldt.workstruct.structure.owner.id
  ld_filename = ldt.workstruct.structure.location + '/' + ldt.workstruct.identifier + "-" + ldt.action + ".inp"
  t = get_input_script('applyld_template.inp')
  charmm_inp = output.tidyInp(t.render(template_dict))

  inp_out = open(ld_filename, 'w')
  inp_out.write(charmm_inp)
  inp_out.close()
  add_script(ldt, "charmm", ld_filename, charmming_config.default_charmm_nprocs)
#    if ldt.useqmmm == 'y' and modelType == 'oniom': #TODO: implement QM/MM MD
#        new_script.executable = charmming_config.apps['charmm-mscale']
#        new_script.processors = charmming_config.default_mscale_nprocs
#    else:
  ldt.save()
#    ldt.scripts += ',%s' % ld_filename

  # YP lessons status update
  lesson_obj = get_lesson_object(ldt.workstruct.structure.lesson_type)

  if lesson_obj:
    if 'usesgld' in postdata:
      doLessonAct(ldt.workstruct.structure, "onSGLDSubmit", ldt, "")
    else:
      doLessonAct(ldt.workstruct.structure, "onLDSubmit", ldt, "")
  # YP

  if 'make_movie' in postdata:
    ldt.make_movie = 't'
    return makeJmolMovie(ldt, postdata, ldt.action)
  else:
    start(ldt)
    ldt.save()

  ldt.workstruct.save()

  return output.returnSubmission('Langevin dynamics')

# Generates MD script and runs it
def applymd_tpl(request, mdt, pTask):
  postdata = request.POST

  template_dict = {}
  impsolv = ''
  try:
    impsolv = postdata['solvate_implicitly']
  except:
    impsolv = 'none'
  template_dict['impsolv'] = impsolv

  mdt.make_movie = 'make_movie' in postdata
  mdt.scpism = (impsolv == 'scpism')

  # template dictionary passes the needed variables to the templat
  template_dict['topology_list'], template_dict['parameter_list'] = mdt.workstruct.getTopparList()
  template_dict['output_name'] = mdt.workstruct.identifier + '-md'
  if mdt.workstruct.topparStream:
    template_dict['tpstream'] = list(set(mdt.workstruct.topparStream.split()))
  else:
    template_dict['tpstream'] = []

  template_dict['input_file'] = mdt.workstruct.identifier + '-' + pTask.action
  template_dict['impsolv'] = impsolv
  mdt.parent = pTask

  template_dict['shake'] = 'apply_shake' in request.POST
  if 'apply_shake' in request.POST:
    template_dict['which_shake'] = postdata['which_shake']
    # WTF is this here? --> template_dict['qmmmsel'] = mp.qmmmsel

    if postdata['which_shake'] == 'define_shake':
      template_dict['shake_line'] = postdata['shake_line']
      if postdata['shake_line'] != '':
        input.checkForMaliciousCode(postdata['shake_line'], postdata)

  orig_rst = mdt.workstruct.structure.location + "/" + pTask.action + "-md.res"
  new_rst = mdt.workstruct.structure.location + "/" + pTask.action + "-md-old.res"
  if 'dorestart' in postdata:
    template_dict['restartname'] = new_rst
    template_dict['strtword'] = "restart"
    try:
      shutil.copy(orig_rst, new_rst)
    except:
      pass
  else:
    template_dict['strtword'] = "start"

  # determine whether periodic boundary conditions should be applied, and if
  # so pass the necessary crystal and image parameters
  dopbc = False
  if 'usepbc' in request.POST:
    if impsolv != 'none':
      messages.error(request, "Conflicting solvation options.")
      return HttpResponseRedirect("/dynamics/md/")

    # decide if the structure we're dealing with has
    # been solvated.
    solvated = False
    wfc = pTask
    while True:
      if wfc.action == 'solvation' or wfc.action == 'neutralization':
        solvated = True
        break
      if wfc.parent:
        wfc = wfc.parent
      else:
        break
    if not solvated:
      return output.returnSubmission('Molecular dynamics', error='Requested PBC on unsolvated structure')

    dopbc = True
    try:
      sp = SolvationTask.objects.filter(workstruct=mdt.workstruct, active='y')[0]
    except:
      return output.returnSubmission('Molecular dynamics', error="Could not find solvation parameters")
    mdt.usepbc = True
    mdt.save()
    template_dict['xtl_x'] = sp.xtl_x
    template_dict['xtl_y'] = sp.xtl_y
    template_dict['xtl_z'] = sp.xtl_z
    template_dict['xtl_angles'] = "%10.6f %10.6f %10.6f" % (sp.angles[0], sp.angles[1], sp.angles[2])
    template_dict['xtl_ucell'] = sp.solvation_structure
    template_dict['ewalddim'] = sp.calcEwaldDim()

  template_dict['dopbc'] = dopbc

  # The MD type can have three values: useheat, usenve, or
  # usenvt (todo add NPT)
  template_dict['mdtype'] = postdata['mdtype']
  template_dict['genavg_structure'] = 'gen_avgstruct' in postdata

  if template_dict['mdtype'] == 'useheat':
    mdt.ensemble = 'heat'
    mdt.firstt = postdata['firstt']
    mdt.finalt = postdata['finalt']
    mdt.teminc = postdata['teminc']
    mdt.ihtfrq = postdata['ihtfrq']
    mdt.tbath = postdata['tbath']
    template_dict['nstep'] = postdata['nstepht']
    template_dict['firstt'] = postdata['firstt']
    template_dict['finalt'] = postdata['finalt']
    template_dict['tbath'] = postdata['tbath']
    template_dict['teminc'] = postdata['teminc']
    template_dict['ihtfrq'] = postdata['ihtfrq']
  elif template_dict['mdtype'] == 'usenve':
    mdt.ensemble = 'nve'
    template_dict['nstep'] = postdata['nstepnve']
    template_dict['nvetemp'] = postdata['tstruct']
    template_dict['ieqfrq'] = postdata['ieqfrq']
  elif template_dict['mdtype'] == 'usenvt':
    mdt.ensemble = 'nvt'
    mdt.temp = postdata['hoovertemp']
    template_dict['nstep'] = postdata['nstepnvt']
    template_dict['hoovertemp'] = postdata['hoovertemp']
  elif template_dict['mdtype'] == 'usenpt':
    mdt.ensemble = 'npt'
    mdt.temp = postdata['hoovertemp']
    template_dict['nstep'] = postdata['nstepnpt']
    template_dict['hoovertemp'] = postdata['hoovertemp']
    template_dict['pgamma'] = postdata['pgamma']

  if int(template_dict['nstep']) > charmming_config.dyna_steplimit:
    return output.returnSubmission('LD', error='Dynamics has a limit of %d steps.' % charmming_config.dyna_steplimit)

  if dopbc or mdt.ensemble == 'nvt' or mdt.ensemble == 'npt':
    template_dict['cpt'] = 'cpt'
  else:
    template_dict['cpt'] = ''

  # user_id = mdt.workstruct.structure.owner.id
  os.chdir(mdt.workstruct.structure.location)
  t = get_input_script('applymd_template.inp')

  # write out the file and let it go...
  md_filename = mdt.workstruct.structure.location + "/" + mdt.workstruct.identifier + "-md.inp"
  charmm_inp = output.tidyInp(t.render(template_dict))
  inp_out = open(md_filename, 'w')
  inp_out.write(charmm_inp)
  inp_out.close()

  add_script(mdt, "charmm", md_filename, charmming_config.default_charmm_nprocs)
#    if mdt.useqmmm == 'y' and modelType == 'oniom': #TODO: implement QM/MM MD
#        new_script.executable = charmming_config.apps['charmm-mscale']
#        new_script.processors = charmming_config.default_mscale_nprocs
#    else:
  mdt.save()
#    mdt.scripts += ',%s' % md_filename

  # YP lessons status update
  lesson_obj = get_lesson_object(mdt.workstruct.structure.lesson_type)

  if lesson_obj:
    doLessonAct(mdt.workstruct.structure, "onMDSubmit", mdt)
  # YP

  if 'make_movie' in postdata:
    mdt.make_movie = 't'
    return makeJmolMovie(mdt, postdata, 'md')
  else:
    start(mdt)

  mdt.save()
  mdt.workstruct.save()

  return output.returnSubmission("Molecular dynamics")

# pre: Requires a file object, and the name of the psf/crd file to read in
# Jmol requires the PDB to be outputted a certain a certain way for a movie to be displayed
# it does not take DCD files
def makeJmolMovie(task, postdata, type):
  task.movie_status = ""
  charmm_inp = """* Movie making
*

read psf card name """ + task.workstruct.identifier + """-""" + type + """.psf

open read unit 10 file name """ + task.workstruct.identifier + """-""" + type + """.dcd
traj firstu 10 skip 10

set i 1
label loop
traj read
open unit 1 card write name """ + task.workstruct.identifier + """-""" + type + """-movie@i.pdb
write coor pdb unit 1
**Cords
*
incr i by 1
if i lt 11 goto loop

stop"""
  # Run the script through charmm, this is not done under job queue at the moment
  # because the PDBs must be appended together after the above script has been run.
  # Once the DAG and query stuff has been implemented, this is how the following code should
  # be changed to
  # 1. Create a new field in the Structure object called movie_status
  # 2. In the status method, check to see if movie_status is done
  # 3. If it is done, call the method combinePDBsForMovie(...): right below the following code.
  if(type == 'md'):
    movie_filename = task.workstruct.identifier + '-mdmovie.inp'
  elif(type == 'ld'):
    movie_filename = task.workstruct.identifier + '-ldmovie.inp'
  elif(type == 'sgld'):
    movie_filename = task.workstruct.identifier + '-sgldmovie.inp'
  mov_path = "%s/%s" % (task.workstruct.structure.location, movie_filename)
  movie_handle = open(mov_path, 'w')
  movie_handle.write(charmm_inp)
  movie_handle.close()
  add_script(task, "charmm", mov_path, charmming_config.default_charmm_nprocs)
#    if task.useqmmm == 'y' and modelType == 'oniom': #TODO: implement QM/MM MD
#        new_script.executable = charmming_config.apps['charmm-mscale']
#        new_script.processors = charmming_config.default_mscale_nprocs
#    else:
  task.save()
#    task.scripts += ",%s/%s" % (task.workstruct.structure.location,movie_filename)
  task.workstruct.save()  # make sure we don't lose extra fields...
  start(task)
  task.save()
  return output.returnSubmission("Molecular dynamics")
