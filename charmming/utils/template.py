from ast import literal_eval
from typing import Optional
from django import template
from django.conf import settings
from django.utils.safestring import mark_safe

register = template.Library()

@register.simple_tag()
def asset(type: str, name: str, user_attrs: Optional[str]=None) -> str:
  # Set default attribute or parse as dict
  if not user_attrs:
    user_attrs = "{}"

  user_attrs = literal_eval(user_attrs)

  asset_type_info = {
    'js': {
      'tag': 'script',
      'url_attr': 'src',
      'attrs': [
        ('type', 'text/javascript')
      ],
      'close': True
    },
    'css': {
      'tag': 'link',
      'url_attr': 'href',
      'attrs': [
        ('rel', 'stylesheet'),
        ('type', 'text/css')
      ],
    }
  }

  if not asset_type_info.get(type):
    return ''

  # Return ASSET_URL and ASSET_DIR if it exists, used for prod
  asset_prefix = settings.ASSET_URL + (settings.ASSET_DIR if hasattr(settings, 'ASSET_DIR') else '')

  # Returns the asset_prefix (already has / appended), followed by
  # type directory, asset name, and the asset extension (same as type)
  tag_name = asset_type_info[type]['tag']
  url_attr = asset_type_info[type]['url_attr']

  attrs = asset_type_info[type]['attrs'].copy()
  attrs += user_attrs
  attrs.append((url_attr, "{}{}/{}.{}".format(asset_prefix, type, name, type)))

  attrs = ' '.join([ "{}=\"{}\"".format(t[0], t[1]) for t in attrs ])

  tag = "<{} {}>".format(tag_name, attrs)

  if asset_type_info[type].get('close'):
    tag += "</{}>".format(tag_name)

  return mark_safe(tag)
