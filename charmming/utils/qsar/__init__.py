from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User

from charmming.forms import QsarSelectPropertyForm
from charmming import charmming_config
from charmming.utils.qsar.train import active_inactive, get_sd_properties, check_regression_property
from charmming.utils.qsar.common import AssignObjectAttribute
from charmming.scheduler.schedInterface import SchedInterface
from charmming.models import QSARJob, QSARJobType, QSARModel, QSARModelType, QSARJobsModels

from rdkit import Chem

import os

def handle_uploaded_file(f, filename):
  path = filename
  with open(path, 'wb+') as destination:
    for chunk in f.chunks():
      destination.write(chunk)

def get_dir(request):
  location = charmming_config.user_home + '/' + request.user.username + '/qsar/'
  if not os.path.exists(location):
    os.makedirs(location)
  return location

def qsar_property(request, filename=None, qsar_model_id=None, message="", query="", num_mols=0):
  if request.method == 'POST':
    if 'filename' in request.POST:
      filename = request.POST['filename']
      qsar_model_id = request.POST['qsar_model_id']
    work_dir = str(get_dir(request))
    fullfilename = work_dir + '/' + filename
    if 'query' in request.POST:
      query = request.POST['query']
    if 'num_mols' in request.POST:
      num_mols = request.POST['num_mols']
    if 'back' in request.POST:
      if query != "":
        return HttpResponseRedirect(reverse('assays-cont', kwargs={
          'filename': filename,
          'query': query,
          'num_mols': num_mols
        }))
      return HttpResponseRedirect(reverse('newModel', args=()))
    elif 'next' in request.POST:
      activity_property = request.POST["activity_property"]
      return train(request, filename, activity_property, qsar_model_id, query, num_mols)
  if filename is not None and qsar_model_id is not None:
    work_dir = str(get_dir(request))
    fullfilename = work_dir + '/' + filename
    qsar_model = QSARModel.objects.get(id=qsar_model_id)
    form = QsarSelectPropertyForm(filename=filename, qsar_model_id=qsar_model_id, fullfilename=fullfilename, query=query, num_mols=num_mols)
    model_type = QSARModelType.objects.get(id=qsar_model.model_type_id)
    activity_property_choice_length = len(get_sd_properties(fullfilename))
    return render_to_response('qsar/property.html', {
      'form': form,
      'filename': filename,
      'activity_property_choice_length': activity_property_choice_length,
      'qsar_model': qsar_model,
      'model_type': model_type,
      'query': query
    })
  else:
    return HttpResponse('<h4> Internal Error <h4>')


def train(request, filename=None, activity_property=None, qsar_model_id=None, query="", num_mols=0):
  if not request.user.is_authenticated():
    return render_to_response('html/loggedout.html')

  err_message = ""
  if filename is not None and activity_property is not None:
    work_dir = str(get_dir(request))
    fullfilename = work_dir + '/' + filename
    count = 0
    ms = []
    for x in Chem.SDMolSupplier(str(fullfilename)):
      if x is not None:
        ms.append(x)
        count += 1
        if count > 10000:
          break

    if count < 100:
      err_message = "The training file should contain at least 100 structures"
    if count > 10000:
      err_message = "Training file is too big"

    qsar_model = QSARModel.objects.get(id=qsar_model_id)

    subtype = qsar_model.model_type.type
    categorization = qsar_model.model_type.is_categorical
    if categorization:
      active, inactive, not_two = active_inactive(ms, activity_property)
      if not_two:
        err_message = "Property %s should take exactly 2 values" % activity_property

    if not categorization:
      if not check_regression_property(ms, activity_property):
        err_message = "Property %s is not numerical or takes fewer than 10 values" % activity_property

    if err_message != "":
      model_type = QSARModelType.objects.get(id=qsar_model.model_type_id)
      form = QsarSelectPropertyForm(filename=filename, qsar_model_id=qsar_model.id, fullfilename=fullfilename, query=query, num_mols=num_mols)
      activity_property_choice_length = 0
      activity_property_choice_length = len(get_sd_properties(fullfilename))
      return render_to_response('qsar/property.html', {
        'form': form,
        'filename': filename,
        'activity_property_choice_length': activity_property_choice_length,
        'qsar_model': qsar_model,
        'model_type': model_type,
        'message': err_message,
        'query': query
      })
    AssignObjectAttribute(request.user.id, qsar_model_id, "qsar_qsar_models", "Activity Property", activity_property)

    # Iwona
    # work_dir = get_dir(request)
    # (fh,model_name) = mkstemp(dir=work_dir)
    # os.close(fh);
    # (fh,output_txt) = mkstemp(dir=work_dir)
    # os.close(fh);

    # subprocess.call(["python","/var/www/charmming/qsar/create_model.py", filename, model_name, activity_property, active, output_txt])
    # f = open(output_txt,"r")
    # self_auc = float(f.readline())
    # recall = float(f.readline())
    # precision = float(f.readline())
    # threshold = float(f.readline())
    # auc_rand = float(f.readline())
    # cross_auc = float(f.readline())
    # f.close()
    # end Iwona

    username = request.user.username
    u = User.objects.get(username=request.user.username)
    # job_owner_id=jobs.getNewJobOwnerIndex(u)
    # job_folder = charmming_config.user_home + '/' + username
    NewJob = QSARJob()
    qsar_folder = charmming_config.user_home + "/" + username + "/qsar"
    qsar_jobs_folder = qsar_folder + "/jobs"
    job_folder = qsar_folder + "/jobs/qsar_job_" + str(NewJob.getNewJobOwnerIndex(u))
    qsar_models_folder = qsar_folder + "/models"
    model_folder = qsar_folder + "/models/qsar_model_" + str(qsar_model.model_owner_index)  # common.getNewModelOwnerIndex(u)
    os.system("mkdir %s" % (qsar_folder))
    os.system("mkdir %s" % (qsar_jobs_folder))
    os.system("mkdir %s" % (job_folder))
    os.system("chmod -R g+w %s" % (job_folder))
    os.system("mkdir %s" % (qsar_models_folder))
    os.system("mkdir %s" % (model_folder))
    os.system("chmod -R g+w %s" % (model_folder))
    model_file = model_folder + "/model"
    training_output = model_folder + "/training_output"
    os.system("chmod g+rw %s" % (fullfilename))
    os.system("chmod g+rw %s" % (model_file))
    os.system("chmod g+rw %s" % (training_output))
    train_submitscript = open(job_folder + "/train_submitscript.inp", 'w')
    train_submitscript.write("#! /bin/bash\n")
    train_submitscript.write("cd %s\n" % (job_folder))
    train_submitscript.write("export PYTHONPATH=$PYTHONPATH:%s/\n" % ("/var/www/charmming"))  # job_folder))
    train_submitscript.write("export DJANGO_SETTINGS_MODULE=settings\n")
    subtype = qsar_model.model_type.type
    categorization = qsar_model.model_type.is_categorical
    if categorization:
      train_submitscript.write("python /var/www/charmming/qsar/create_model.py %s %s %s %s %s %s %s %s\n" % (fullfilename, model_file, activity_property, active, training_output, str(qsar_model.id), str(u.id), subtype))
    else:
      train_submitscript.write("python /var/www/charmming/qsar/create_model_regression.py %s %s %s %s %s %s %s\n" % (fullfilename, model_file, activity_property, training_output, str(qsar_model.id), str(u.id), subtype))

    train_submitscript.write("echo 'NORMAL TERMINATION'\n")
    train_submitscript.close()

    scriptlist = []
    scriptlist.append(job_folder + "/train_submitscript.inp")
    exedict = {job_folder + "/train_submitscript.inp": 'sh'}
    si = schedInterface()
    newSchedulerJobID = si.submitJob(request.user.id, job_folder, scriptlist, exedict)

    NewJob.job_owner = request.user
    NewJob.job_scheduler_id = newSchedulerJobID
    NewJob.job_owner_index = NewJob.getNewJobOwnerIndex(u)
    NewJob.description = "Building a model " + qsar_model.model_name  # "QSAR Training Job"
    try:
      jobtype = QSARJobType.objects.get(job_type_name='Train')
    except:
      jobtype = QSARJobType()
      jobtype.job_type_name = "Train"
      jobtype.save()

    NewJob.job_type = jobtype
    NewJob.save()

    qsar_model.model_file = model_file
    subtype = qsar_model.model_type.type
    categorization = qsar_model.model_type.is_categorical
    if categorization:
      AssignObjectAttribute(request.user.id, qsar_model.id, "qsar_qsar_models", "Active", active)
      AssignObjectAttribute(request.user.id, qsar_model.id, "qsar_qsar_models", "Inactive", inactive)

    qsar_model.save()

    NewJobModel = QSARJobsModels()
    NewJobModel.owner = request.user
    NewJobModel.job_id = NewJob.id
    NewJobModel.qsar_model_id = qsar_model.id
    NewJobModel.save()

    # self_auc = 0
    # recall = 0
    # precision = 0
    # threshold = 0
    # auc_rand = 0
    # cross_auc = 0
    # end YP

  # form = QsarTestUploadForm(active=active,inactive=inactive,saved_model=model_file,threshold=threshold,activity_property=activity_property,filename=filename)
  # return render_to_response('qsar/train.html', {'job_owner_index':NewJob.job_owner_index, 'self_auc': self_auc, 'auc_rand': auc_rand, 'cross_auc':cross_auc,
  #                                               'threshold':threshold,'recall':recall,'precision':precision, 'form':form}, context_instance=RequestContext(request))
  return HttpResponseRedirect('/qsar/viewjobs')
