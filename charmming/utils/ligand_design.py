from charmming import charmming_config
from charmming.models import Structure, Segment

from http.client import HTTPConnection

def clear_struct(request):  # To write less repetitive code...
  return (None, charmming_config.user_home + "/" + request.user.username + "/")

def get_structure_to_attach(request):
  """
  If a user has selected to attach the ligand to their current structure,
  this function checks whether there is a structure to attach to in the first place,
  and then works out a name for the new segment by incrementing the ASCII value of
  the first letter of the last segment on the structure.
  This is hack-ish, but since CHARMM doesn't support Unicode, it's about as
  good as anything done there.
  This function returns the Structure object that is currently selected by the user,
  the path to that Structure's files, and the letter that will be used to define
  the label of the segment that will consist of the ligand attached to the Structure,
  in a tuple:
      (struct,path_to_struct,segment_letter)
  This function raises a ValueError if the Structure already has segments up to the letter
  "z", and standard Django exceptions for the Structure not being found.
  """
  segletter = "a"
  # seggoodletter = "a"
  struct = Structure.objects.get(owner=request.user, selected='y')
  filepath = struct.location
  sl = []
  sl.extend(Segment.objects.filter(structure=struct))
  maxletter = ""
  maxgoodletter = ""  # For GOOD segments
  for seg in sl:  # This way we limit ourselves to the "bad" hetatms
    if seg.type == "bad" and seg.name[0] > maxletter:
      maxletter = seg.name[0]
    if seg.type == "good" and seg.name[0] > maxgoodletter:
      maxgoodletter = seg.name[0]
  if maxletter == "z" or maxgoodletter == "z":
    raise ValueError("Too many segments for this structure.")
  else:
    if maxletter:  # if there's an issue, default to 'a'
      segletter = chr(ord(maxletter) + 1)  # Can't handle anything but standard letters, but that's fine, neither can CHARMM.
    if maxgoodletter:
      pass
      # seggoodletter = chr(ord(maxgoodletter) + 1)
  return (struct, filepath, segletter)

def check_if_ligand_in_pdb(ligname):
  """
  Checks if a ligand is present in the PDB, first by checking its name
  against the ligand database, and then against the protein database.
  The URL in charmming_config that represents each of these databases
  *must* be kept up to date in order for this method to return
  anything sensible.
  If the ligand is present in either database, this function returns
  the link to its SDF file in the PDB. Otherwise, it returns False.
  """
  conn = HTTPConnection(charmming_config.pdb_ligand_download_url)
  reqstring = charmming_config.pdb_ligand_download_pattern.format(ligname)
  conn.request("GET", reqstring)
  resp = conn.getresponse()
  if resp.status == 200:
    return charmming_config.pdb_ligand_download_url + reqstring
  else:
    conn = HTTPConnection(charmming_config.pdb_protein_download_url)
    reqstring = charmming_config.pdb_protein_download_pattern.format(ligname)
    conn.request("GET", reqstring)
    resp = conn.getresponse()
    if resp.status == 200:
      return charmming_config.pdb_protein_download_url + reqstring
  return False

def create_ligand_file(molname, resname, segletter, jmol_data):
  """
  Takes as input the name of the ligand as defined by the user,
  the ligand name truncated to 4-characters and uppercased to fit
  in CHARMM conventions, the letter chosen for the segment,
  and the PDB data passed in by Jmol, and creates the string
  representing the ligand PDB file with the correct name for the
  ligand instead of UNK. This file will be used to create the structure for the ligand.
  """
  end_data = ""
  for line in jmol_data:
    if line.startswith("COMPND"):
      line = "COMPND    LIGAND: " + molname + "\n"
    end_data += line + "\n"
  # usable_resname = resname.upper() + (" " * (5 - len(resname))) + segletter.upper()
  end_data = end_data.replace(" UNK ", resname.upper())  # Jmol doesn't know our residue name, just UNK.
  return end_data
