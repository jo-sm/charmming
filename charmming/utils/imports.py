import importlib
import importlib.util
import inspect
import pkgutil
import ast
import sys

def import_classes(_dir, _globals):
  module_cache = []
  imported_cache = []

  def find_outside_references(_dir, module, loader):
    """
    Finds references that are not on the initial import level.

    For example, if a db model imports a utility, and the utility
    imports a db model, that isn't on the first level and needs to
    be found and imported to prevent potential import issues.
    """

    # We keep a cache of all modules we've already looked in, to
    # prevent recursive lookups
    if module in module_cache:
      return []

    try:
      source = importlib.util.find_spec("{}".format(module)).loader.get_source("{}".format(module))
    except:
      return []

    # Only accept string or bytes to parse into the tree
    # Prevents looking at builtins
    if type(source) is not str and type(source) is not bytes:
      return []

    tree = ast.parse(source)

    names = []

    for branch in tree.body:
      if type(branch) == ast.ImportFrom:
        if branch.module == _dir:
          names = branch.names + names
        else:
          module_cache.append(module)
          names = find_outside_references(_dir, branch.module, loader) + names

    module_cache.append(module)

    return names

  """
  This method takes a module name, and its globals dict, and
  adds all classes with the same name as its submodule to the
  globals.

  Essentially, this is run in __init__.py and makes any class
  with the same name as the file the import, rather than the
  file as a module. So, if you have LoginView.py with the class
  LoginView in it, it will allow this:

  from charmming.views import LoginView

  Instead of:

  from charmming.views.LoginView import LoginView

  This does not override __all__, so other modules can be imported
  as normal.
  """
  def import_class(_dir, name, _globals, loader, level=0):
    # If the class is already loaded in globals, ignore it
    if name in _globals and inspect.isclass(_globals[name]):
      return

    # We load the source and parse out imports so that any dependencies within this same module
    # are loaded before the original module, to prevent import order issues
    # e.g. if A imports B which imports C, C won't be loaded yet and will cause an import error
    try:
      source = loader.find_spec("{}.{}".format(_dir, name)).loader.get_source("{}.{}".format(_dir, name))
    except:
      raise ImportError("No module named {}.{}".format(_dir, name))

    tree = ast.parse(source)

    for branch in tree.body:
      if type(branch) == ast.ImportFrom:
        if branch.module == _dir:
          # Gather names and attempt to import them
          names = branch.names
        else:
          # This module needs to be inspected first, to see if it has something to be imported
          names = find_outside_references(_dir, branch.module, loader)

        for _name in names:
          if name not in imported_cache:
            import_class(_dir, _name.name, _globals, loader, level + 1)

    # Load the module
    try:
      module = importlib.import_module("{}.{}".format(_dir, name))
    except AttributeError:
      # This module did not exist, e.g. charmming.views.dsfargeg
      return

    # Attempt to get class with the same name as module
    try:
      module_class = getattr(module, name)
      if inspect.isclass(module_class):
        _globals[name] = module_class
        sys.modules["{}.{}".format(_dir, name)] = module_class
        imported_cache.append(name)

    except AttributeError:
      # The module does exist, but the property does not
      print('This module does exist, but the property does not: {}.{}'.format(_dir, name))
      return

  # Walk the package and, if the package module is a class with the
  # same name as the file, import the class rather than the module
  _module_ = importlib.import_module(_dir)
  for loader, name, is_pkg in pkgutil.walk_packages(_module_.__path__):
    import_class(_dir, name, _globals, loader)
