from charmming.models import DDProtein, DDProteinConformation

def getNewProteinOwnerIndex(user):
  next_target_owner_index = 1

  try:

    last_target = DDProtein.objects.filter(owner=user).order_by('-id')[0]
    next_target_owner_index = int(last_target.protein_owner_index) + 1

  except:
    next_target_owner_index = 1

  return next_target_owner_index


def getNewProteinFilename(user):

  return "target_%s" % (str(getNewProteinOwnerIndex(user)))

def getNewConformationProteinIndex(protein):
  try:

    last_conformation = DDProteinConformation.objects.filter(protein=protein).order_by('-id')[0]
    next_conformation_protein_index = int(last_conformation.conformation_protein_index) + 1

  except:
    next_conformation_protein_index = 1

  return next_conformation_protein_index


class objProteinConformation():
  def __init__(self, **kwargs):
    self.conformation_id = kwargs.get('conformation_id') or 0
    self.conformation_name = kwargs.get('conformation_name') or ""
    self.protein_pdb_code = kwargs.get('protein_pdb_code') or ""
    self.files = kwargs.get('files') or []

class TargetConformationInfo():
  def __init__(self, **kwargs):
    self.id = kwargs.get('id') or 0
    self.name = kwargs.get('name') or ""
    self.description = kwargs.get('description') or ""
    self.file_objects_list = kwargs.get('file_objects_list') or []
    self.filecount = kwargs.get('filecount') or 0
    self.target_pdb_code = kwargs.get('target_pdb_code') or ""
    self.conformations = kwargs.get('conformations') or []
