from charmming.utils.redis import get_instance
from charmming.utils.datagram import pack

def send_notification(name, data, **kwargs):
  instance = get_instance()
  user = kwargs.get('user')

  notification = {
    'data': {
      'type': name,
      'data': data
    }
  }

  if user:
    notification['username'] = user.username

  packed = pack(notification)

  return instance.publish('charmming.notifications', packed)
