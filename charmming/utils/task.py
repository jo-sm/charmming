from charmming import charmming_config
from charmming.scheduler.schedInterface import SchedInterface
from charmming.models import Script

def start_task(task, tdict):
  """
  Takes as input a task and the template dictionary intended to run it,
  and decides if to run using QM/MM.
  """
  if task.useqmmm == "y" and "modelType" in tdict and tdict["modelType"] == "oniom":
    start(task, mscale_job=True)
  else:
    start(task)
  task.save()

def start(self, **kwargs):
  st = self.workstruct.structure

  si = schedInterface()
  # now, fetch all script objects attached to this Task
  # since it's in a function it won't matter that we do this on something that we declare below
  all_scripts = None
  try:
    all_scripts = Script.objects.filter(task=self).order_by('order')  # order is IMPORTANT. Otherwise they execute wrong.
  except:
    all_scripts = None
  exedict = {}
  nprocdict = {}
  if all_scripts:
    for script in all_scripts:
      exedict[script.scriptname] = script.executable
   #          exedict MUST ALWAYS have the same length as scripts!!
      nprocdict[script.scriptname] = script.processors
   # This should be set to default_mscale_procs for MSCALE stuff!
      scriptList = [x.scriptname for x in all_scripts]
    self.jobID = si.submitJob(st.owner.id, st.location, scriptList, exedict=exedict, nprocdict=nprocdict)
  else:
    if "mscale_job" in kwargs:
      for inpscript in self.scriptList:
        exedict[inpscript] = charmming_config.charmm_mscale_exe
        nprocdict[inpscript] = charmming_config.default_mscale_nprocs
      # TODO: Update this to interface with variable processor numbers!
      self.jobID = si.submitJob(st.owner.id, st.location, self.scriptList, exedict=exedict, nprocdict=nprocdict, mscale=True)
    elif 'altexe' in kwargs:
      for inpscript in self.scriptList:
        exedict[inpscript] = kwargs['altexe']
      self.jobID = si.submitJob(st.owner.id, st.location, self.scriptList, exedict=exedict)
    else:
      self.jobID = si.submitJob(st.owner.id, st.location, self.scriptList)
  if self.jobID > 0:
    self.save()
    self.query()
  else:
    # test = self.jobID
    raise AssertionError('Job submission fails')

# This should be refactored to pass script directly
def add_script(self, app_name, script_name, processors):
  all_scripts = Script.objects.filter(task=self).order_by('-order')
  new_script = Script()
  new_script.task = self
  new_script.scriptname = script_name
  new_script.executable = charmming_config.apps[app_name]
  new_script.processors = processors
  if len(all_scripts) > 0:
    new_script.order = all_scripts[0].order + 1
  new_script.save()
