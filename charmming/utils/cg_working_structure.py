from django.http import HttpResponse

from charmming import charmming_config
from charmming.models import Segment, WorkingSegment, GoModel

from pychm3.cg.sansombln import SansomBLN
from pychm3.io.pdb import PDB
from pychm3.cg.ktgo import KTGo

import pickle
import os

def associate(working_structure, structref, segids, **kwargs):
  """
  This method buildg the CG model from the AA coordinates. Note
  that the kwargs is designed to hold CG-model specific parameters,
  e.g. nscale etc. These get passed directly to the pychm
  constructor of the CG model.
  """

  working_structure.structure = structref
  working_structure.save()

  for sid in segids:
    segobj = Segment.objects.filter(name=sid, structure=structref)[0]

    # here is where there are some changes from the parent associate
    # routine ... WorkingSegments need to have their type set to
    # modelType and no patch_first or patch_last.
    wseg = WorkingSegment()
    wseg.is_working = 'y'
    wseg.structure = segobj.structure
    wseg.name = segobj.name.replace('-pro', '-%s' % working_structure.cg_type)
    wseg.type = working_structure.cg_type
    wseg.default_patch_first = 'None'
    wseg.default_patch_last = 'None'
    wseg.tpMethod = 'standard'

    wseg.save()

    working_structure.segments.add(wseg)
    working_structure.save()

    # We could actually go ahead and call wseg.build() here, but I
    # don't think that it is necessary. XXX Right now the segment
    # holds the RTF and PRM lists, which aren't set right for a CG
    # model segment. Fortunately, we can just build in a per cgWorkStruct,
    # and not a per segment basis, but this will need to be re-done
    # at some point.

  # because of the way the Go model works, we need to actually build
  # the AA structure first, then build the CG model.
  if working_structure.cg_type == 'go' or working_structure.cg_type == 'bln':
    pdbfname = structref.location + '/cgbase-' + str(working_structure.id) + '.pdb'
    pfile = working_structure.localpickle if working_structure.localpickle else structref.pickle
    fp = open(pfile, 'r')
    mol = (pickle.load(fp))[working_structure.modelName]
    sct = 0
    for s in mol.iter_seg():
      if s.segid in segids:
        sct += 1
        s.write(pdbfname, outformat='charmm', append=True, ter=True, end=False)
    fp.close()

    if sct == 0:
      return HttpResponse("No segments selected")

    fp = open(pdbfname, 'a')
    fp.write('\nEND\n')
    fp.close()

    # AA PDB file is built, now turn it into a CG model
    pdbfile = PDB(pdbfname)[0]

    # decide how this model is going to be stored on disk.
    basefname = working_structure.identifier + '-' + working_structure.cg_type

    if working_structure.cg_type == 'go':
      # need to set strideBin via kwargs
      intgo = GoModel()  # This used to be structure.models.goModel(). We're in structure.models right now.
      intgo.cgws = working_structure

      cgm = KTGo(pdbfile, strideBin=charmming_config.stride_bin)
      if 'nScale' in kwargs:
        cgm.nScale = float(kwargs['nScale'])
        intgo.nScale = kwargs['nScale']
      if 'contactSet' in kwargs:
        cgm.contactSet = kwargs['contactSet']
        intgo.contactType = kwargs['contactSet']
      if 'kBond' in kwargs:
        cgm.kBond = float(kwargs['kBond'])
        intgo.kBond = kwargs['kBond']
      if 'kAngle' in kwargs:
        cgm.kAngle = float(kwargs['kAngle'])
        intgo.kAngle = kwargs['kAngle']
      if 'contactrad' in kwargs:
        cgm.contactrad = kwargs['contactrad']

      intgo.save()

    elif working_structure.cg_type == 'bln':
      # kwargs can be hbondstream
      cgm = SansomBLN(pdbfile, **kwargs)
      if 'kBondHelix' in kwargs:
        cgm.kBondHelix = float(kwargs['kBondHelix'])
      if 'kBondSheet' in kwargs:
        cgm.kBondHelix = float(kwargs['kBondSheet'])
      if 'kBondCoil' in kwargs:
        cgm.kBondHelix = float(kwargs['kBondCoil'])
      if 'kAngleHelix' in kwargs:
        cgm.kAngleHelix = float(kwargs['kAngleHelix'])
      if 'kAngleSheet' in kwargs:
        cgm.kAngleSheet = float(kwargs['kAngleSheet'])
      if 'kAngleCoil' in kwargs:
        cgm.kAngleCoil = float(kwargs['kAngleCoil'])

    # write out
    cgm.write_pdb(working_structure.structure.location + '/' + basefname + '.pdb')
    cgm.write_rtf(working_structure.structure.location + '/' + basefname + '.rtf')
    cgm.write_prm(working_structure.structure.location + '/' + basefname + '.prm')
    if working_structure.cg_type == 'go':
      if 'findnscale' in kwargs and kwargs['findnscale']:
        findnscale = True
      else:
        findnscale = False

      if findnscale:
        lockfp = open(working_structure.structure.location + '/lock-' + basefname + '.txt', 'w')
        lockfp.write('%s\n' % cgm.nScale)
        lockfp.close()
        cmd = '%s/find_nscale_async.py %s %s %f %f' % (charmming_config.data_home, working_structure.structure.location, basefname, cgm.nScale, kwargs['gm_nscale_temp'])
        os.system(cmd)

    # add the topology and parameter to each segment ... since we
    # never want to build CG model segments individually, we just
    # set these all to built
    for wseg in working_structure.segments.all():
      wseg.isBuilt = 't'
      wseg.rtf_list = working_structure.structure.location + '/' + basefname + '.rtf'
      wseg.prm_list = working_structure.structure.location + '/' + basefname + '.prm'
      wseg.save()

    working_structure.pdbname = working_structure.structure.location + '/' + basefname + '.pdb'
    working_structure.save()
  else:
    raise AssertionError('Unknown CG model')

def addBondsToPDB(inp_file, out_file):
  """
  This method adds CONECT lines to the PDB file located at out_file
  by looking at the bond information in the PSF file located at
  inp_file.
  This is done in a generic fashion such that it can be called
  on the result of any task that generates a PDB that is done
  on a CGWorkingStructure (which should be put into the
  updateActionStatus() method in this file, as well as the build
  routine.)
  This procedure is derived from psf2connect.py, which is available
  on the CHARMMing webroot.
  This method returns True if successful, and an error message
  otherwise.
  This method should ONLY be called internally. Do not trust
  user input with this method!
  """
  if not (out_file.endswith("pdb")):
    return "out_file not a PDB file."
  if not (inp_file.endswith("psf")):
    return "inp_file not a PSF file."
  # An alternate method for the subsequent is to read the whole file
  # Then split at the indexes of "!NBOND" and "!NTHETA" and chop on newlines then.
  bondlist = []
  psf_file = open(inp_file, "r")
  curr_line = psf_file.readline()
  if not(curr_line.startswith('PSF')):
    return "Malformed PSF."

  psf_file.readline()  # second line is always blank, CHARMM std format
  curr_line = psf_file.readline()
  ntitle = int(curr_line.split()[0])
  # format is 2 !NTITLE; 2 is the number of title lines.

  for x in range(ntitle + 1):
    psf_file.readline()
  # Now we're at natom.
  latom = psf_file.readline()
  natom = int(latom.split()[0])

  for x in range(natom + 1):
    psf_file.readline()
  lbond = psf_file.readline()
  nbond = int(lbond.split()[0])
  if nbond <= 0:
    return "No bonds."

  nbread = 0
  while nbread < nbond:
    line = psf_file.readline()
    bond_array = line.split()
    if len(bond_array) % 2 != 0:
      return "Malformed bond at line " + nbread + " of NBOND."

    for i in range(0, len(bond_array), 2):
      atom1, atom2 = int(bond_array[i]), int(bond_array[i + 1])
      if atom1 > natom or atom1 < 0:
        return "Bad atom number in NBOND, line " + nbread + "."
      if atom2 > natom or atom2 < 0:
        return "Bad atom number in NBOND, line " + nbread + "."

      if atom2 < atom1:
        atom1, atom2 = atom2, atom1
      bondlist.append((atom1, atom2))

    nbread += len(bond_array) / 2
  psf_file.close()
  con_dict = {}

  for bond in bondlist:
    if bond[0] in list(con_dict.keys()):
      con_dict[bond[0]].append(bond[1])
    else:
      con_dict[bond[0]] = [bond[1], ]
  out_string = ""

  for k in list(con_dict.keys()):
    out_string = out_string + "CONECT" + ' ' * (5 - len(str(k))) + str(k)
    for x in con_dict[k]:
      out_string = out_string + ' ' * (5 - len(str(x))) + str(x)  # PDB format supports up to 4 digits.
    out_string = out_string + "\n"
  try:
    pdb_file = open(out_file, "r")
  except:
    return "Could not find file."
  old_pdb = pdb_file.read()
  pdb_file.close()
  old_pdb = old_pdb.replace("END\n", "")  # Remove last line
  old_pdb = old_pdb + out_string + "END\n"
  os.unlink(out_file)  # This is done because otherwise django goes crazy about permissions
  try:
    pdb_file = open(out_file.replace("pdb", "tmp"), "w")
    pdb_file.write(old_pdb)  # Write the result...
  except Exception as ex:
    return str(ex)
  pdb_file.close()  # Done.
  os.rename(out_file.replace("pdb", "tmp"), out_file)
  os.chmod(out_file, 0o664)
  return True
