from django.http import HttpResponseRedirect
from django.contrib import messages

from charmming import charmming_config
from charmming.utils.structure import structure_builder
from charmming.utils.propka import run_propka_base_structure
from charmming.utils.structure import getSegs
from charmming.utils.lesson import attach_lesson_to_structure
from charmming.utils.lesson_maker import create_lesson_base_structure

import pychm3.io
import os

def make_sequence_structure(request, lesson_obj):
  """
  Creates a Structure object and PDB file from an amino acid sequence passed in by a user,
  and then redirects to the "Build Working Structure" page.
  lesson_obj is passed in for lesson status checks.
  """
  location = os.path.join(charmming_config.user_home, request.user.username)
  struct = structure_builder.create_structure(request.user, "Custom amino acid sequence", location, "sequ", original_name="sequ", automatically_generate_name=True)
  fullpath = struct.location + '/sequ.pdb'
  struct.putSeqOnDisk(request.POST['sequ'], fullpath)
  try:
    pdb = pychm3.io.pdb.PDB(fullpath)
    mol = next(pdb.iter_models())
    mol.parse()
    getSegs(mol, struct, auto_append=True)
  except ValueError:
    messages.error(request, "Error parsing PDB file. Please report this error to the CHARMMing administrators, along with details of your sequence.")
    return HttpResponseRedirect("/fileupload/")
  # store the pickle file containing the structure
  # TODO: Add dname-pdpickle.dat as filename. Do once the rest of the infrastructure is complete...
  structure_builder.create_pickle_file(struct, pdb)
  structure_builder.unselect_current_structure(request.user)
  struct.selected = 'y'
  if lesson_obj:
    attach_lesson_to_structure(struct, request.POST, lesson_obj)
  struct.save()
  if 'use_propka' in request.POST:
    run_propka_base_structure(struct)  # handles the conversion to PROPKA format implicitly
  if 'start_lessonmaker' in request.POST and charmming_config.lessonmaker_enabled:
    create_lesson_base_structure(struct, fullpath.split("/")[-1])
    # fullpath is passed in in that way because we need the full filename with extension to check for file upload issues.
  return HttpResponseRedirect('/buildstruct/')

def get_uploaded_filename(request):
  """
  Gets the filename for a file a user uploaded, and verifies that it has no special characters.
  Returns the filename if it has no special characters, and False otherwise.
  """
  filename = request.FILES["pdbupload"].name if "pdbupload" in request.FILES else request.FILES["crdupload"].name
  if input.contains_special_characters(filename):
    return False
  return filename.lower()

def write_uploaded_file(request, fullpath, filetype):
  """
  Writes a file from the request FILES data to our disk, by writing the file chunks.
  filetype is which of the request.FILES data we actually want to access.
  """
  outfile = open(fullpath, "w")
  for chunk in request.FILES[filetype].chunks():
    outfile.write(chunk)
  outfile.close()
