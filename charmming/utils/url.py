def get_pattern(pattern_proto):
  if isinstance(pattern_proto, str):
    pattern = pattern_proto
  elif isinstance(pattern_proto, list):
    pattern = ""

    for i in pattern_proto:
      if isinstance(i, str):
        pattern += i
      elif isinstance(i, dict):
        name = i.get('name')
        regex = i.get('regex')

        pattern += "(?P<{}>{})".format(name, regex)

  return pattern
