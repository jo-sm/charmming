from functools import reduce

def unique(list):
  """
  Creates a order-preserved list that contains each value
  from the list just once. The first instance from the
  original list is added to the resulting list.

  This is useful when the order of the list is important,
  as list(set(...)) does not preserve original ordering.
  """
  def reducer(memo, i):
    if i not in memo:
      memo.append(i)

    return memo

  return reduce(reducer, list, [])

def flatten(list):
  """
  Flattens a list that may contains lists
  as values.
  """
  def reducer(memo, i):
    if type(i) == list:
      i = flatten(i)

    memo.append(i)

    return memo

  return reduce(reducer, list, [])

def get(list, search_func):
  result = [i for i in list if search_func(i)]

  if len(result) == 0:
    return None

  return result[0]
