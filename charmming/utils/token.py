from django.conf import settings

from charmming.utils.encryption import encrypt, decrypt

from typing import Optional

def generate_instance_token(instance) -> Optional[str]:
  if not instance:
    return None

  try:
    instance.id
  except AttributeError:
    return None

  return encrypt(settings.SECRET_KEY.encode('utf-8'), "{}".format(instance.id).encode('utf-8'))

def determine_instance_id_from_token(token):
  if not token:
    return None

  if len(token) != 64:
    return None

  return decrypt(settings.SECRET_KEY.encode('utf-8'), token.encode('utf-8'))
