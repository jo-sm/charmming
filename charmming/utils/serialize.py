# serialize_aux.py
# This file includes any random stuff we may need in order to make
# objects JSON-serializable. It is included for code reuse.

def django_to_dict(qset):
  """
  Takes a django QuerySet as input, then turns it into a dictionary
  for easy JSON serialization.
  """
  if len(qset) > 0:
    return list(map(remove_django, qset))
  else:
    return {}

def remove_django(obj):
  """
  Takes a Django database object as input, and returns a dictionary that is
  JSON-serializable by removing any offending django-specific objects
  which cannot be serialized
  """
  new_dict = obj.__dict__
  for key in new_dict:
    if key.startswith("_"):
      new_dict[key] = ""  # all django vars start with _. Make sure none of our DB fields do!
  if 'time_created' in new_dict:  # this is SPECIFICALLY for atom selections.
    new_dict['time_created'] = new_dict['time_created'].strftime("%s")
  return new_dict
