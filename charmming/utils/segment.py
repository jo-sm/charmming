from charmming.utils.residue import is_very_bad

def has_very_bad_residue(segment):
  for residue in get_bad_residues(segment):
    if is_very_bad(residue):
      return True

  return False

def has_bad_residues(segment):
  if segment.type != 'bad':
    return False

  pychm_segment = segment.get_pychm_segment()

  if len(list(pychm_segment.iter_res())) > 0:
    return True

  return False

def get_bad_residues(segment):
  bad_residues = []

  if segment.type != 'bad':
    return bad_residues

  pychm_segment = segment.get_pychm_segment()

  for res in pychm_segment.residues:
    bad_residues.append(res)

  # TODO: This logic used to exist but doesn't seem
  # to do anything. Investigate.
  # if residue.resName not in badResList:
  #   for stuff in badResList:
  #     for letter in letters:
  #       mytest = letter + stuff
  #       if mytest == residue.resName:
  #         pass
  #         # BTM, have to be careful about whether we want to nuke these.
  #         # doitnow = False

  return bad_residues

def get_default_first_patch(pychm_segment):
  first_residue = pychm_segment.residues[0]

  if pychm_segment.type == 'pro':
    if first_residue.name == 'gly':
      return 'GLYP'
    elif first_residue.name == 'pro':
      return 'PROP'
    else:
      return 'NTER'
  elif pychm_segment.type == 'dna' or pychm_segment.type == 'rna':
    return '5TER'
  else:
    return 'NONE'

def get_default_last_patch(pychm_segment):
  if pychm_segment.type == 'pro':
    return 'CTER'
  elif pychm_segment.type == 'dna' or pychm_segment.type == 'rna':
    return '3TER'
  else:
    return 'NONE'

def valid_first_patch(segment, patch_name):
  if segment.type == 'pro':
    if patch_name not in ['NTER', 'GLYP', 'PROP', 'ACE', 'ACP', 'NONE']:
      return False
  elif segment.type == 'dna' or segment.type == 'rna':
    if patch_name not in ['5TER', '5MET', '5PHO', '5POM', 'CY35']:
      return False
  else:
    if patch_name != 'NONE':
      return False

  return True

def valid_last_patch(segment, patch_name):
  if segment.type == 'pro':
    if patch_name not in ['CTER', 'CT1', 'CT2', 'CT3', 'ACP', 'NONE']:
      return False
  elif segment.type == 'dna' or segment.type == 'rna':
    if patch_name not in ['3TER', '3PHO', '3POM', '3CO3', 'CY35']:
      return False
  else:
    if patch_name != 'NONE':
      return False

  return True
