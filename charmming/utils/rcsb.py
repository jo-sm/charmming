from http.client import HTTPConnection

def retrieve_rcsb_pdb(id):
  """
  Takes as input a PDB identifier and returns the response text.
  Returns None if could not retrieve PDB file
  """
  # try:
  conn = HTTPConnection("files.rcsb.org")
  conn.request("GET", "/download/{}.pdb".format(id))
  resp = conn.getresponse()

  if resp.status != 200:
    return None
    # raise ValueError("The PDB server returned error code {}.".format(resp.status))

  pdb_file = resp.read()
  pdb_file = pdb_file.decode('utf-8')

  return pdb_file
