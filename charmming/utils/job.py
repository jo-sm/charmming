from charmming.jobs import generate_input_script, generate_build_script, generate_segment_build_script, get_program_module
from charmming.scheduler.schedInterface import SchedInterface
from charmming.utils.build import handle_pre_build
from charmming.utils.coordinates import basic_validation_coordinates
from charmming.utils.string import capitalize_classcase

import shutil
import importlib
import os

def job_exists(program, job_name):
  try:
    module = importlib.import_module("charmming.jobs.{}.{}".format(program, job_name))
  except ModuleNotFoundError:
    return False

  class_name = capitalize_classcase(job_name)

  return hasattr(module, class_name)

def get_job_class(program, job_name):
  if job_exists(program, job_name):
    module = importlib.import_module("charmming.jobs.{}.{}".format(program, job_name))
    class_name = capitalize_classcase(job_name)

    return getattr(module, class_name)
  else:
    return None

def all_coordinates(structure):
  """
  Returns all UI data about coordinates
  """
  coordinates = {}

  for task in structure.tasks():
    if not coordinates.get(task.program):
      coordinates[task.program] = {}

    if not coordinates[task.program].get(task.job):
      coordinates[task.program][task.job] = []

    coordinate = {
      'job': task.job,
      'program': task.program
    }

    coordinates[task.program][task.job].append(coordinate)

  return coordinates

def get_coordinates_task_for(structure, program, job, index):
  """
  Returns more detailed info about a specific coordinate,
  including its location in the filesystem
  """

  job_class = get_job_class(program, job)

  if not job_class:
    return None

  if not job_class.has_coordinates:
    return None

  possible_tasks = []

  for task in structure.tasks():
    if task.program == program and task.job == job:
      possible_tasks.append(task)

  if len(possible_tasks) <= index:
    print('Possible tasks length {} less than index {}'.format(len(possible_tasks), index))
    return None

  actual_task = possible_tasks[index]

  return actual_task

def cancel_build(task, structure):
  stop(task, kill=True)

  # Also hide the task from the UI
  task.hidden = True
  task.save()

  structure.is_building = False
  structure.save()

def submit(structure, program, job_name, options, **kwargs):
  """
  Creates a new `Task` object for given `structure`,
  `program`, and `job_name`, running on the specified `coordinates`,
  and given `options`.

  `options` is expected to be a valid DataValidator instance,
  and will raise SubmissionError if not.

  Will attempt to build if the structure is not yet
  built, and will raise BuildError (and save the associated
  error message to the Task) if the build process fails in
  some way.

  Will raise SubmissionError if submitting the job
  fails in some way.
  """

  # By default, we use the build coordinates
  coordinates = options.get('coordinates', {
    'program': 'charmm',
    'job': 'build',
    'index': 0
  })

  if not basic_validation_coordinates(coordinates):
    print('Could not validate coordinates')
    return False

  building_task = None

  if not structure.is_built or structure.is_building:
    building_task = build(structure)

    if not building_task:
      return False

  coordinates_task = get_coordinates_task_for(structure, coordinates.get('program'), coordinates.get('job'), coordinates.get('index'))

  if not coordinates_task:
    print('Could not get coordinates info for {} {} {}'.format(coordinates.get('program'), coordinates.get('job'), coordinates.get('index')))
    return False

  input_script, files_used = generate_input_script(structure, program, job_name, coordinates_task, options)

  if not input_script:
    print('Could not generate input script')
    if building_task:
      cancel_build(building_task, structure)

    return False

  task = structure.task_set.create()
  task.coordinates_task = coordinates_task
  task.program = program
  task.job = job_name
  task.options = options
  task.save()

  task_dir = task.dir()

  for file in files_used:
    original = file.get('path')
    new_name = file.get('name')

    shutil.copy(original, task_dir)

    if os.path.basename(original) != new_name:
      os.rename(os.path.join(task_dir, os.path.basename(original)), os.path.join(task_dir, new_name))

  # There may be a modifier for the program depending on the
  # options given during submission. For example, we may need
  # to use CHARMM MSCALE instead of normal CHARMM.
  program = apply_modifiers(options, program)

  task.add_script("{}.inp".format(job_name), input_script, program)

  started = start(task)

  if not started:
    if building_task:
      cancel_build(building_task, structure)

    return False

  return task

def apply_modifiers(options, program):
  module = get_program_module(program)

  if not hasattr(module, 'apply_modifiers'):
    return program

  return getattr(module, 'apply_modifiers')(options)

def start(task, **kwargs):
  interface = SchedInterface()

  # now, fetch all script objects attached to this Task
  # since it's in a function it won't matter that we do this on something that we declare below
  job_id = interface.submit(task)

  if not job_id:
    return False

  return True

def stop(task, **kwargs):
  do_kill = kwargs.get('kill', False)
  do_cancel = kwargs.get('cancel', False)

  interface = SchedInterface()
  is_killed = interface.kill(task)

  # TODO: Fix this...
  if not is_killed:
    return False

  if do_kill:
    # Killed means that something internally
    # went wrong, so the job failed
    # TODO: Provide some meaningful error message
    # to the user in these cases
    task.status = 'F'
  elif do_cancel:
    # Canceled means the user manually killed the job
    task.status = 'K'

  task.save()

  return True

def build(structure, **kwargs):
  """
  Handles special logic for creating the build task
  Creates Scripts for building the segments, and the
  final Script for building the Structure.

  Will automatically start the task unless told otherwise.
  """
  task = structure.task_set.create()
  task.program = 'charmm'
  task.job = 'build'
  task.save()

  task_dir = task.dir()

  # Add the Segment scripts first
  segments = structure.segment_set.all()

  files_used = []
  segment_crd_list = [[], []]
  segment_nonhet_psf_list = []
  segment_het_psf_list = []

  for segment in segments:
    # Reset the initial generated toppar
    toppar_generated = None

    # TODO: Ordering segment scripts
    toppar_generated = handle_pre_build(segment)

    if not toppar_generated:
      break

    segment_script, segment_files_used, segment_data = generate_segment_build_script(structure, segment)
    task.add_script('segment-{}.inp'.format(segment.name), segment_script, 'charmm')

    files_used.extend(segment_files_used)

    if segment.type in ['pro', 'dna', 'rna']:
      segment_nonhet_psf_list.append(segment_data['psf_file'])
      segment_crd_list[0].append(segment_data['crd_file'])
    else:
      segment_het_psf_list.append(segment_data['psf_file'])
      segment_crd_list[1].append(segment_data['crd_file'])

  if not toppar_generated:
    # TODO: Return some error
    return None

  # This kind of logic (and the logic with double lists above) is due
  # to CHARMM requiring CRDs and PSFs be read in the same order. This
  # flattens the segment crd list
  segment_crd_list = [crd for sublist in segment_crd_list for crd in sublist]

  # Add the Structure script
  build_script, build_files_used = generate_build_script(structure, {
    'segment_crd_list': segment_crd_list,
    'segment_nonhet_psf_list': segment_nonhet_psf_list,
    'segment_het_psf_list': segment_het_psf_list,
  })

  if not build_script:
    return None

  task.add_script('structure.inp', build_script, 'charmm')

  task.save()

  files_used.extend(build_files_used)

  for file in files_used:
    shutil.copy(file, task_dir)

  # Set the structure as "building"

  start(task)

  return task
