from django.template.loader import get_template
from django.template import Context

from charmming.helpers import input, output # for checkForMaliciousCode
from charmming.models import SolvationTask # for PBC checks
from charmming.utils.atom_selection import template_gen

import os

"""
This module abstracts out the general functions used for CHARMMing's input script templating,
such as setting up input and output coordinate files, and making checks for QM/MM and PBC.
"""

valid_qmmm_models = ["oniom", "qmmm"] # this is the list of all valid QM/MM models. update it if we ever get more.

def get_input_script(name):
  template = get_template(os.path.join('input_scripts', name))

  return template

def process_qmmm(tdict, useqmmm, request, task):
  """
  Processes the variables related to QM/MM and sets up the template dict accordingly.
  Returns the updated dictionary.
  Raises a ValueError if incompatible parameters are given.
  """
  if "model_selected" not in request.POST:
    return tdict  # nothing to see here if there's no QM/MM
  model_selected = request.POST["model_selected"]
  if task.useqmmm == "y":
    if model_selected not in valid_qmmm_models:
      raise ValueError("Invalid Multi-Scale modeling type.")
    tdict = template_gen(model_selected, tdict, request, task)
  return tdict

def process_postdata(tdict, setup_task, pTask, request):
  """
  Processes the POST data passed in by the user and returns an updated templating dictionary
  that includes flags for various user-defined parameters such as using PBC or QM/MM.
  Calls setup_inputs to do more thorough checks and actual modifications
  of the template dictionary.
  This function should be called from external modules.
  setup_task is the new Task that is being set up, and pTask is the parent task to the current one,
  if any, or None.
  """
  postdata = request.POST  # TODO: change atomselection_aux.template_gen so that it takes as input postdata
  # at the top here, let's get all the variables we need
  shake = 'apply_shake' in postdata
  usepbc = 'usepbc' in postdata
  useqmmm = "useqmmm" in postdata
  setup_task.useqmmm = "y" if useqmmm else "n"
  if useqmmm:  # then we have to have selected a model somehow
    tdict = process_qmmm(tdict, useqmmm, request, setup_task)
  which_shake = False
  shake_line = False
  if shake:
    which_shake = postdata['which_shake'] if 'which_shake' in postdata else False
    if which_shake == 'define_shake':
      shake_line = postdata['shake_line']
      if shake_line != '':
        check_shake = input.checkForMaliciousCode(postdata['shake_line'], postdata)
        if check_shake != shake_line:  # malicious code has been logged
          raise ValueError(check_shake)
  return setup_inputs(tdict, setup_task, pTask, usepbc=usepbc, useqmmm=useqmmm, shake=shake, which_shake=which_shake, shake_line=shake_line)

def pbc_check(useqmmm, impsolv, pTask, setup_task):
  """
  Checks if PBC should be possible for a structure given whether QM/MM and implicit solvent
  are used, as well as checking whether pTask (the task chosen as a parent for the
  next calculation step) or any of its parent tasks are a solvation.
  PBC cannot be performed if QM/MM or implicit solvent are enabled, and cannot be
  performed if the structure has not been solvated.
  Returns True if the structure can have PBC used on it, or raises a ValueError
  with the reason otherwise.
  """
  if useqmmm:
    raise ValueError("You cannot use PBC with QM/MM")
  if impsolv:
    raise ValueError("Conflicting solvation options given - implicit solvation cannot be used with PBC.")
  solvated = False
  taskTest = pTask
  while True:  # we break eventually
    if taskTest.action == "solvation" or taskTest.action == "neutralization":
      solvated = True
      break
    if taskTest.parent:
      taskTest = taskTest.parent
    else:
      break
  if not solvated:
    raise ValueError("PBC was requested, but your structure has not been solvated.")
  try:
    sp = SolvationTask.objects.get(workstruct=setup_task, active='y')
  except:
    raise ValueError("PBC was requested, but your structure has not been solvated.")
  if sp.solvation_structure == "sphere":
    raise ValueError("You cannot use PBC with a spherical water shell.")
  return sp

def setup_inputs(tdict, setup_task, pTask, **kwargs):
  """
  Takes as input a templating dictionary, a Task to set up and its parent, and a set of keyword
  arguments (see process_postdata in this module) to run checks on the validity of the
  arguments passed in by the user, and adds arguments to the template dictionary to aid
  in the creation of scripts for a particular Task.
  Returns the updated template dictionary.
  Raises ValueErrors if any of the parameters the user chose conflict with each other,
  in particular in the case of PBC.
  """
  impsolv = kwargs.get('impsolv', False)
  usepbc = kwargs.get('usepbc', False)
  shake = kwargs.get('shake', False)
  which_shake = kwargs.get('which_shake', False)
  shake_line = kwargs.get('shake_line', False)
  useqmmm = kwargs.get('useqmmm', False)
  workstruct = setup_task.workstruct
  tdict['topology_list'], tdict['parameter_list'] = workstruct.getTopparList()

  if workstruct.topparStream:
    tdict['tpstream'] = list(set(workstruct.topparStream.split()))
  else:
    tdict['tpstream'] = []

  tdict['output_name'] = "%s-%s" % (workstruct.identifier, setup_task.action)  # this way we can ensure custom filenames for users in the future
  tdict['input_file'] = "%s-%s" % (workstruct.identifier, pTask.action)
  tdict['useqmmm'] = useqmmm

  if impsolv:
    tdict['impsolv'] = impsolv
  else:
    tdict["impsolv"] = "none"

  if shake:
    tdict['shake'] = shake
    tdict['which_shake'] = which_shake
    if which_shake == "define_shake":
      tdict['shake_line'] = shake_line

  if usepbc:
    sp = pbc_check(useqmmm, impsolv, pTask, setup_task)
    tdict['xtl_x'] = sp.xtl_x
    tdict['xtl_y'] = sp.xtl_y
    tdict['xtl_z'] = sp.xtl_z
    tdict['xtl_angles'] = "%10.6f %10.6f %10.6f" % (sp.angles[0], sp.angles[1], sp.angles[2])
    tdict['xtl_ucell'] = sp.solvation_structure
    tdict['ewalddim'] = sp.calcEwaldDim()
    tdict['dopbc'] = True
    setup_task.usepbc = 't'
  else:
    setup_task.usepbc = "f"

  setup_task.parent = pTask
  setup_task.save()
  setup_task.workstruct.save()  # sanity check for the topparStreams
  return tdict

def setup_scripts(tdict, task, template_name):
  """
  Writes the template file based on the template dictionary.
  template_name is passed in because our input scripts have inconsistent
  naming conventions.
  Returns the full path of the file written, if successful.
  """
  t = get_input_script(template_name)
  charmm_inp = output.tidyInp(t.render(Context(tdict)))
  out_filename = os.path.join(task.workstruct.structure.location, tdict["output_name"] + ".inp")
  out_file = open(out_filename, "w")
  out_file.write(charmm_inp)
  out_file.close()
  return out_filename
