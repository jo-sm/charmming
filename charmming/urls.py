# This file contains all the URLs for the Django app
# Media URLs used for development are loaded here, as well as
# URLs from `urls.json`.
#
# The imports that are not used here are defined in `urls.json`, and
# will be refactored away to class based views and imported automatically.

from django.conf import settings
from django.conf.urls import url
from django.views.static import serve
from django.views.generic import View

from charmming.utils.url import get_pattern
from charmming.utils.log import log

import json
import os
import importlib
import inspect

urlpatterns = []

urls_raw = open(os.path.join(os.path.dirname(__file__), 'urls.json'), 'r')
urls = json.load(urls_raw)
views = importlib.import_module('charmming.views')

for u in urls:
  url_method = None
  view_name = u.get('view')

  if hasattr(views, view_name):
    view = getattr(views, view_name)

    if inspect.isclass(view):
      if issubclass(view, View):
        url_method = view.as_view()
    elif view_name in globals():
      url_method = globals()[view_name]
  elif view_name in globals():
    url_method = globals()[view_name]

  if url_method:
    pattern = get_pattern(u.get('pattern'))
    urlpatterns.append(url('^{}$'.format(pattern), url_method, u.get('context') or {}, name=u.get('name')))
  else:
    log('{} ({}) ({}) did not have a valid view'.format(view_name, u.get('name'), u.get('regex')), level='critical')

if settings.ENVIRONMENT == 'development':
  urlpatterns.extend([
    url(r'^js/(?P<path>.*)$', serve, { 'document_root': '{}/public/js/'.format(settings.PROJECT_ROOT)}),
    url(r'^css/(?P<path>.*)$', serve, { 'document_root': '{}/public/css/'.format(settings.PROJECT_ROOT)}),
    url(r'^pdbuploads/(?P<path>.*)$', serve, { 'document_root': '{}/uploads/'.format(settings.PROJECT_ROOT)}),
    url(r'^images/(?P<path>.*)$', serve, { 'document_root': '{}/static/images/'.format(settings.PROJECT_ROOT)}),
  ])
