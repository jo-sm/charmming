from django.conf import settings

from charmming.jobs.renderer import render
from charmming.utils.segment import has_very_bad_residue
from charmming.utils.string import capitalize_classcase
from charmming.utils.list import unique

from copy import deepcopy

import importlib
import os

def get_program_module(program):
  base_path = 'charmming.jobs.'
  full_path = "{}{}".format(base_path, program)

  # Determine if the job_name exists for program
  try:
    module = importlib.import_module(full_path)
  except ImportError:
    print('Could not find module {}...'.format(full_path))
    return None

  return module

def get_template(program, name):
  app_root = settings.APP_ROOT
  templates_path = os.path.join(app_root, 'jobs', program, 'templates')
  template_name = "{}.inp".format(name)

  full_template_path = os.path.join(os.path.join(templates_path, template_name))

  try:
    template = open(full_template_path, 'r').read()
  except FileNotFoundError:
    template = None

  return template

def get_base_template(program):
  # Get the generic template for `program`
  return get_template(program, 'base')

def get_job_module(program, job):
  program_module = get_program_module(program)

  if not program_module:
    return None

  name = program_module.__name__

  full_path = "{}.{}".format(name, job)

  try:
    module = importlib.import_module(full_path)
  except ImportError as e:
    print('Could not find module {}...'.format(full_path))
    return None

  return module

def generate_input_script(structure, program, job_name, coordinates_task, options):
  """
  Creates an input file for a specific `program` and the `job_name`
  of that program.
  """

  program_module = get_program_module(program)
  module = get_job_module(program, job_name)

  # Set the output name, if needed
  options['output_name'] = job_name

  # Get the title
  job_class = getattr(module, capitalize_classcase(job_name))()

  if not hasattr(job_class, 'title'):
    # Log some error
    title = 'Generic {} job'.format(program)
  else:
    title = job_class.title

  if not hasattr(job_class, 'render_template'):
    body = ""
    files_used = []
    sanitized_options = {}
  else:
    job_class.render_template(structure, options)

    sanitized_options = job_class.options
    body = job_class.body
    files_used = job_class.original_files

  coordinates = coordinates_task.get_coordinates()

  # We may need an option from here to be in the base template
  program_options = deepcopy(sanitized_options)
  program_options['title'] = title
  program_options['body'] = body

  return program_module.render_template(structure, coordinates, program_options, files_used=files_used)

def get_list_for_all_segments(type, structure):
  raw_list = []

  for segment in structure.segment_set.all():
    if type == 'prm':
      raw_list.extend(segment.prm_list)
    elif type == 'rtf':
      raw_list.extend(segment.rtf_list)
    elif type == 'str':
      raw_list.extend(segment.stream_list)

  file_list = unique(raw_list)

  if not type == 'str':
    file_list = [ { 'name': item, 'append': True } for item in file_list ]

    if len(file_list) > 0:
      file_list[0]['append'] = False

  return file_list

def generate_build_script(structure, options):
  base_template = get_base_template('charmm')
  template = get_template('charmm', 'build')

  segment_crd_list = options.get('segment_crd_list', [])
  segment_het_psf_list = options.get('segment_het_psf_list', [])
  segment_nonhet_psf_list = options.get('segment_nonhet_psf_list', [])

  if len(segment_crd_list) != (len(segment_het_psf_list) + len(segment_nonhet_psf_list)):
    return None, None

  files = {
    'segment_crd_list': [ { 'name': file, 'append': True } for file in segment_crd_list ],
    'segment_het_psf_list': [ { 'name': file, 'append': True } for file in segment_het_psf_list ],
    'segment_nonhet_psf_list': [ { 'name': file, 'append': True } for file in segment_nonhet_psf_list ]
  }

  if len(files['segment_crd_list']) > 0:
    files['segment_crd_list'][0]['append'] = False

  # Nonhet segments appear first in the input file
  if len(files['segment_nonhet_psf_list']) == 0 and len(files['segment_het_psf_list']) > 0:
    files['segment_het_psf_list'][0]['append'] = False

  if len(files['segment_nonhet_psf_list']) > 0:
    files['segment_nonhet_psf_list'][0]['append'] = False

  files['topology_list'] = get_list_for_all_segments('rtf', structure)
  files['parameter_list'] = get_list_for_all_segments('prm', structure)
  files['stream_list'] = get_list_for_all_segments('str', structure)

  body = render('charmm', template, {
    'output_name': 'build'
  })

  rendered_template = render('charmm', base_template, {
    'title': 'Append multiple segments into a final structure',
    'body': body,
    'files': files,
    'building': True
  })

  return rendered_template, []

def generate_segment_build_script(structure, segment):
  base_template = get_base_template('charmm')
  template = get_template('charmm', 'build_segment')

  # We want to create the CHARMM version of this segment
  print('Writing {} CHARMM segment'.format(segment.name))
  segment_pychm_model = segment.get_pychm_model()
  segment_pychm_model.make_charmm_compliant()
  charmm_segment = segment_pychm_model.to_str(format='charmm')

  # We update "HETATM" to "ATOM  " since CHARMM sometimes has issues with it
  charmm_segment = '\n'.join([ line.replace('HETATM', 'ATOM  ') for line in charmm_segment.split('\n') ])
  segment_full_filename = segment.save_file("charmm.pdb".format(segment.name), charmm_segment, internal=True)

  segment_filename = os.path.basename(segment_full_filename)

  files_used = [
    segment_full_filename
  ]

  # The filename of the segment, with .pdb replaced with -built.
  # For example, `94-model00-a-bad.pdb` becomes 194-model00-a-bad-built`.
  # This is a prefix since it will be used to generate full filenames
  # in the template.
  build_segment_file_prefix = segment_filename.replace(".pdb", "-built")

  title = 'Run Segment {} Through CHARMM'.format(segment.name)
  no_angle_dihe = ""

  if segment.type == 'good':
    no_angle_dihe = 'noangle nodihedral'

  # Handle any patching (including false patches)
  patch_lines = []
  patches = structure.patch_set.filter(segment=segment)

  for patch in patches:
    if not patch.name.startswith('hs'):
      # We only want the true patch lines.
      # These seem to be the only ones where it's a problem since it's not a true patch.
      patch_lines.append("{} {}".format(patch.name, patch.segment_residue))

  # Patch lines is a string, not an array, so that
  # it's invoked once in the template rather than
  # per line
  patch_lines = "\n".join(patch_lines)

  very_bad = has_very_bad_residue(segment)

  body = render('charmm', template, {
    'segment_name': segment.name,
    'first_patch': segment.first_patch,
    'last_patch': segment.last_patch,
    'very_bad': very_bad,
    'do_ic': segment.type != 'good',
    'no_angle_dihe': no_angle_dihe,
    "patch_lines": patch_lines,
    'segment_filename': segment_filename,
    'build_segment_file_prefix': build_segment_file_prefix
  })

  template_files = {
    'topology_list': [ { 'name': os.path.basename(path), 'append': True } for path in segment.rtf_list ],
    'parameter_list': [ { 'name': os.path.basename(path), 'append': True } for path in segment.prm_list ],
    'stream_list': [ os.path.basename(path) for path in segment.stream_list]
  }

  if len(template_files['topology_list']) > 0:
    template_files['topology_list'][0]['append'] = False

  if len(template_files['parameter_list']) > 0:
    template_files['parameter_list'][0]['append'] = False

  # This is a special case where we do not run it through
  # the charmm_module.render_template and invoke render
  # directly.
  # TODO: Possibly put this functionality into the
  # render_template method?
  template = render('charmm', base_template, {
    'title': title,
    'body': body,
    'files': template_files
  })

  files_used.extend(segment.rtf_list)
  files_used.extend(segment.prm_list)
  files_used.extend(segment.stream_list)

  print('Files used in generate_build_segment_script')
  print(files_used)

  return template, files_used, {
    'psf_file': "{}.psf".format(build_segment_file_prefix),
    'crd_file': "{}.crd".format(build_segment_file_prefix)
  }

def generate_build_data(structure):
  build_values = {
    'is_monolithic': False
  }

  if structure.cg_model_type:
    # This is a CG structure, so it's "monolithic"
    build_values['is_monolithic'] = True
    build_values['cg_model_type'] = structure.cg_model_type
  else:
    pass

  return build_values
