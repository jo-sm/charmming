# from charmming.utils.job import get_template
from charmming.jobs.renderer import render
from charmming.jobs import get_template
from charmming.utils.validator import get_base_validator

import os

class Job:
  has_coordinates = False
  coordinate_types = []
  validators = []

  def __init__(self, *args, **kwargs):
    program_module = self.__class__.__module__
    self.program = program_module.split('.')[2] # [ 'charmming', 'jobs', 'charmm', 'energy' ]
    self.job = self.__class__.__name__.lower()

    # Attempt to find and add the base validator for this program
    # If there isn't one, it's fine
    base_validator = get_base_validator(self.program)

    if base_validator:
      self.validators = [ base_validator ] + self.validators

    self.template = get_template(self.program, self.job)
    self.body = ''

    # Template files are the filenames that are used in the template,
    # which are essentially the file basename of the fullpath
    # of the original files.
    self.template_files = []

    # The full "original" filepaths used for this job
    self.original_files = []

    # The sanitized options for this job
    self.options = {}

  def render_template(self, structure, options):
    self.options = self.validate(options)

    if hasattr(self, 'post_validate'):
      self.post_validate(self.options)

    self.body = render(self.program, self.template, self.options)

  def add_file(self, *args):
    """
    Adds a file to be rendered in the template.

    Does not check if the file is valid.
    """
    for path in args:
      if not path:
        continue

      if not os.path.exists(path):
        continue

      basename = os.path.basename(path)

      self.template_files.append({ 'name': basename, 'path': basename })
      self.original_files.append({ 'name': basename, 'path': path })

  def validate(self, options):
    """
    Validates an options dictionary. Expects the
    options given in the API request, and returns
    a sanitized and validated dictionary.
    """
    sanitized = {}

    for i, validator in enumerate(self.validators):
      print(validator)
      if i == 0:
        sanitized = validator(options).validate()
      else:
        sanitized = validator(options, sanitized=sanitized).validate()

    return sanitized
