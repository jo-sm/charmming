from charmming.jobs.Job import Job
from charmming.utils.validator import get_validator
from charmming.utils.toppar import get_toppar_path

class Energy(Job):
  title = 'Simple Energy Calculation'
  validators = [
    get_validator('charmm', 'scpism')
  ]

  def post_validate(self, options):
    # Copy scpism if it's set
    if options.get('scpism'):
      self.add_file(get_toppar_path('scpism.inp'))
