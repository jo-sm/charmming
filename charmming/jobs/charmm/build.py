from charmming.jobs.Job import Job

class Build(Job):
  title = 'Append multiple segments into a final structure'
  has_coordinates = True
  coordinate_types = [ 'psf', 'pdb', 'crd' ]
