from charmming.jobs import get_base_template
from charmming.jobs.renderer import render
from charmming.utils.list import unique

import os

def apply_modifiers(options):
  if not options:
    return 'charmm'

  if 'mscale' in options:
    return 'charmm-mscale'

  return 'charmm'

def render_template(structure, coordinates, options, **kwargs):
  template = get_base_template('charmm')
  title = options.get('title', 'CHARMM Task')
  body = options.get('body', '')
  files_used = kwargs.get('files_used', [])

  # Generate the parameter and topology list
  segments = structure.segment_set.all()
  parameter_list = []
  topology_list = []
  stream_list = []

  # Get each prm, rtf, str for structure segments
  for segment in segments:
    parameter_list.extend(segment.prm_list)
    topology_list.extend(segment.rtf_list)
    stream_list.extend(segment.stream_list)

  # Only gather the unique files
  parameter_list = unique(parameter_list)
  topology_list = unique(topology_list)
  stream_list = unique(stream_list)

  # Copy all of the files we used
  files_used.extend([ { 'name': os.path.basename(file), 'path': file } for file in parameter_list])
  files_used.extend([ { 'name': os.path.basename(file), 'path': file } for file in topology_list])
  files_used.extend([ { 'name': os.path.basename(file), 'path': file } for file in stream_list])

  # CHARMM templates use PSF and CRD coordinate files
  # Copy them
  if not coordinates.get('files'):
    return None, None

  if not coordinates['files'].get('psf'):
    return None, None

  if not coordinates['files'].get('crd'):
    return None, None

  # The prefix for psf, crd files in the base template (see read_structure.inp)
  prefix = os.path.splitext(os.path.basename(coordinates['files'].get('psf')))[0]
  prefix = "{}-coordinates".format(prefix)

  # Coordinate files are copied with the task runner, since it may depend on a currently running job
  # (like building)

  # Create the files list for use in the template
  files = {
    'topology_list': [ { 'name': os.path.basename(file), 'append': True } for file in topology_list ],
    'parameter_list': [ { 'name': os.path.basename(file), 'append': True } for file in parameter_list ],
    'stream_list': [ os.path.basename(file) for file in stream_list ]
  }

  # The first prm and rtf file is not appended
  if len(files['topology_list']) > 0:
    files['topology_list'][0]['append'] = False
  if len(files['parameter_list']) > 0:
    files['parameter_list'][0]['append'] = False

  input_script_values = {
    'title': title,
    'body': body,
    'files': files,
    'use_qmmm': options.get('use_qmmm'),
    'prefix': prefix
  }

  return render('charmm', template, input_script_values), files_used
