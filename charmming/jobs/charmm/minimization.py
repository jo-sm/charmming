from charmming.jobs.Job import Job
from charmming.utils.validator import get_validator

class Minimization(Job):
  title = 'Minimize structure'
  has_coordinates = True
  coordinate_types = [ 'psf', 'pdb', 'crd' ]
  validators = [
    get_validator('charmm', 'minimization')
  ]
