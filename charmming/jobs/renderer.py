import pystache
import re
import os

class TemplateRenderer(pystache.Renderer):
  def __init__(self, options, program, *args, **kwargs):
    self.options = options

    search_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), program, 'templates')

    return super().__init__(file_extension='inp', search_dirs=search_dir, *args, **kwargs)

  def _make_load_template(self):
    loader = self._make_loader()

    def load_template(template_name):
      has_dynamic_template = re.search(r"\((?P<name>[a-zA-Z_]*)\)", template_name)

      if not has_dynamic_template:
        return loader.load_name(template_name)
      else:
        dynamic_template_var_name = has_dynamic_template.group('name')

        if not dynamic_template_var_name:
          # Somehow, there wasn't a given name inside of the parentheses
          # Default to the original functionality
          return loader.load_name(template_name)

        key = self.options.get(dynamic_template_var_name)

        if not key:
          return loader.load_name(template_name)

        dynamic_template_name = self.options.get(key)

        if not dynamic_template_name:
          return loader.load_name(template_name)

        return loader.load_name(dynamic_template_name)

    return load_template

def render(program, template, options):
  renderer = TemplateRenderer(options, program)
  return renderer.render(template, options)
