from charmming.jobs.Job import Job
from charmming.utils.validator import get_validator

class Sp(Job):
  title = 'Energy calculation'
  validators = [
    get_validator('qchem', 'symmetry'),
    get_validator('qchem', 'basis_set'),
    get_validator('qchem', 'exchange'),
  ]
