from charmming.jobs import get_base_template
from charmming.jobs.renderer import render

def apply_modifiers(options):
  return 'qchem'

def render_template(structure, coordinates, options, **kwargs):
  template = get_base_template('qchem')
  title = options.get('title', 'Q-chem Task')
  body = options.get('body', '')
  files_used = kwargs.get('files_used', [])

  model = structure.get_pychm()
  xyz = model.to_str(format='xyz-qchem')

  input_script_values = {
    'title': title,
    'body': body,
    'molecule_lines': xyz,
    'charge': 0,
    'multiplicity': 1
  }

  return render('qchem', template, input_script_values), files_used
