# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2017-01-17 08:58
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('charmming', '0011_auto_20170117_0821'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='segment',
            name='default_patch_first',
        ),
        migrations.RemoveField(
            model_name='segment',
            name='default_patch_last',
        ),
    ]
