# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2017-01-14 15:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('charmming', '0008_auto_20170114_1247'),
    ]

    operations = [
        migrations.RenameField(
            model_name='structure',
            old_name='cg_model',
            new_name='cg_model_type',
        ),
        migrations.AddField(
            model_name='segment',
            name='toppar_gen_method',
            field=models.CharField(max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='structure',
            name='model',
            field=models.CharField(default='model0', max_length=20),
        ),
        migrations.AddField(
            model_name='user',
            name='admin',
            field=models.BooleanField(default=False),
        ),
    ]
