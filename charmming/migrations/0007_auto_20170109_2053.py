# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2017-01-09 20:53
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('charmming', '0006_structure_file'),
    ]

    operations = [
        migrations.CreateModel(
            name='File',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64)),
                ('path', models.CharField(max_length=100)),
                ('file_type', models.CharField(max_length=10)),
            ],
        ),
        migrations.RemoveField(
            model_name='structure',
            name='file',
        ),
        migrations.AddField(
            model_name='file',
            name='structure',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='charmming.Structure'),
        ),
    ]
