# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-09-21 05:10
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

  dependencies = [
      ('charmming', '0001_initial'),
  ]

  operations = [
      migrations.AlterField(
          model_name='qsarattribute',
          name='data_type',
          field=models.CharField(max_length=100, null=True),
      ),
  ]
