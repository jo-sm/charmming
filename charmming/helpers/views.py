"""
This module is intended as a way to consolidate the boilerplate code that is
pervasive throughout CHARMMing by the use of decorators or similar abstractions.
It is the hope that in doing this, it will be easier to write more Task
classes in the future.
"""
from charmming.helpers import input
from charmming.models import Structure, WorkingStructure, CGWorkingStructure

class Accountinator(object):
  """
  Performs all of the checks needed to determine whether a user can actually visit a page.
  In sequence, these are:
      1. logged in?
      2. check for malicious POST data
      3. Structure submitted?
      4. WorkingStructure built?
      5. Is current WorkingStructure coarse-grained?
  Basically every single page that runs jobs does these checks in exactly the same fashion,
  so abstracting them out is favorable.

  If the task that this decorator is bound to does not support coarse-grained working structures,
  the task should pass it a "cg_fail" keyword argument with the name of the task,
  which then allows for templating a message to display to the user stating that
  the task does not support coarse-grained models. This path means that the Accountinator
  should return an HttpResponseRedirect at step 5.

  If the task or view only requires authentication, pass the "auth_only" kwarg as True
  and the struct/ws stuff will be skipped.

  If the task or view only need to check up to the Structure level (e.g. download files page),
  pass the "struct_only" kwarg as True and the ws/cgws stuff will be skipped.

  Some tasks require a further check for whether the WorkingStructure has been built. To
  make this check, pass the check_ws_built kwarg as True.
  """

  def __init__(self, **kwargs):
    self.cg_fail = kwargs.get("cg_fail", False)
    self.auth_only = kwargs.get("auth_only", False)
    self.struct_only = kwargs.get("struct_only", False)
    self.check_ws_built = kwargs.get("check_ws_built", False)

  def __call__(self, func):
    def decorated_func(*args, **kwargs):
      # TODO: handle this more gracefully, since this will cause errors if there are args at the end
      request = args[-1]  # assume this is true...

      if not request.user.is_authenticated():
        return {
          'redirect': '/'
        }

      input.checkRequestData(request)

      if self.auth_only:
        # exit early so as to only do auth stuff. If we ever do a more complex auth procedure or checks,
        # this part can be updated and it will perpetuate itself through all tasks/view pages.
        return func(*args, **kwargs)

      try:
        struct = Structure.objects.filter(owner=request.user, selected='y')[0]
      except:
        return {
          'error': {
            'message': 'Please submit a structure first.'
          },
          'redirect': '/fileupload'
        }

      kwargs["struct"] = struct

      if self.struct_only:
        # exit before checking further so we can still save some boilerplate.
        return func(*args, **kwargs)

      try:
        ws = WorkingStructure.objects.filter(structure=struct, selected='y')[0]
      except:
        return {
          'error': {
            'message': 'Please build a working structure before performing any calculations or visualizing your structure.'
          },
          'redirect': '/buildstruct'
        }

      if self.check_ws_built:
        if ws.isBuilt != "t":
          return {
            'error': {
              'message': 'Please perform a calculation on this structure first.'
            },
            'redirect': '/energy'
          }

      kwargs["ws"] = ws
      cg_model = False
      cgws = None

      try:
        cgws = CGWorkingStructure.objects.get(workingstructure_ptr=ws.id)
        cg_model = True
        if self.cg_fail:
          # If a task does not support CG models, display an error.
          return {
            'error': {
              'message': "The {0} task is not supported for coarse-grained models. If you wish to perform {0} on this structure, please build a working structure with the CHARMM all-atom model.".format(self.cg_fail)
            },
            'redirect': '/buildstruct'
          }

      except:
        pass

      kwargs["cgws"] = cgws
      kwargs["cg_model"] = cg_model

      return func(*args, **kwargs)
    return decorated_func
