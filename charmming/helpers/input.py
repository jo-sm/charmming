from django.core.mail import mail_admins

import re
"""
Input validation routines.
"""

# checks data for possible malicious code
def checkForMaliciousCode(text, request):
  # syst is how charmm executes system commands
  syst = re.compile('syst')
  semicolon = re.compile(';')
  osdot = re.compile('os\.')
  if(syst.search(text) or semicolon.search(text) or osdot.search(text)):
    msg = "User :" + request.user.username + "\n tried to execute malicous code with:\n " + text + "\n"
    mail_admins('Malcious Code Warning', msg, fail_silently=False)
    # sys.exit()
    return "Dangerous Data! Attempt has been logged."
  return text


# check request data for malicious code
def checkRequestData(request):
  try:
    for parameter in request.POST:
      checkForMaliciousCode(request.POST[parameter], request)
  except AttributeError as e:
    pass
  try:
    for parameter in request.GET:
      checkForMaliciousCode(request.GET[parameter], request)
  except AttributeError as e:
    pass

def contains_special_characters(filename):
  """
  Checks for special characters that we do not allow in a filename,
  as they may lead to security holes in either CHARMM or MySQL.
  Returns True if it does, or False if no foul play is seen.
  """
  specialchars_string = "#$/;\n\\+=[]{}()&^%"
  specialchars = set(specialchars_string)
  return len(specialchars.intersection(filename)) > 0
