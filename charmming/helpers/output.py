# tidyInp takes an input script from a template a neatens it up (removes
# leading spaces, excessive blank lines, and any line with just "blankme").

from django.shortcuts import render_to_response

def returnSubmission(jobtitle, error=None):
  td = {}
  if error:
    td['joberror'] = True
    td['errortext'] = error
  else:
    td['joberror'] = False

  return render_to_response('html/jobsubmit.html', td)

def tidyInp(intxt):
  blcnt = 0
  larr = intxt.split('\n')
  outp = ''

  for line in larr:
    line = line.strip()
    if not line:
      blcnt += 1
    else:
      blcnt = 0
    if blcnt > 1 or line == 'blankme':
      continue
    if line.startswith("CHARMM-scripts"):
      line = line[14:]
    outp += "%s\n" % line

  return outp

def tidyTxt(intxt):
  larr = intxt.split('\n')
  outp = ''

  for line in larr:
    if line.strip() == 'blankme':
      continue
    if line:
      outp += "%s\n" % line
    else:
      outp += "\n"
  return outp

# preserves tabs, also encodes to utf-8 to prevent issues
def tabTxt(intxt):
  larr = intxt.split('\n')
  outp = ''

  for line in larr:
    if 'blankme' in line:
      continue
    outp += '%s\n' % line

  return outp.encode('utf-8')
