from django import forms
from django.utils.safestring import mark_safe
from django.utils.encoding import force_text
from charmming.utils.assays.rest import get_url_base

from itertools import chain

# see /usr/lib/python2.7/dist-packages/django/forms/widgets.py
class CheckboxSelectMultipleWithUrl(forms.CheckboxSelectMultiple):
  def render(self, name, value, attrs=None, choices=()):
    if value is None:
      value = []
    has_id = attrs and 'id' in attrs
    final_attrs = self.build_attrs(attrs, name=name)
    output = ['<ul style="list-style-type:none">']
    # Normalize to strings
    str_values = set([force_text(v) for v in value])
    for i, (option_value, option_label) in enumerate(chain(self.choices, choices)):
      # If an ID attribute was given, add a numeric index as a suffix,
      # so that the checkboxes don't all have the same ID attribute.
      if has_id:
        final_attrs = dict(final_attrs, id='%s_%s' % (attrs['id'], i))
        label_for = ' for="%s"' % final_attrs['id']
      else:
        label_for = ''

      cb = forms.CheckboxInput(final_attrs, check_test=lambda value: value in str_values)
      option_value = force_text(option_value)
      rendered_cb = cb.render(name, option_value)
      option_label = force_text(option_label)
      output.append('<li style="margin-bottom:0.5em"><label%s>%s <a href="%s">%s</a> %s</label></li>' % (label_for, rendered_cb, get_url_base() + option_value, option_value, option_label))
    output.append('</ul>')
    return mark_safe('\n'.join(output))
