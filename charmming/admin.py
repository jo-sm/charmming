from django.contrib import admin
from charmming.models import TeacherProfile, StudentProfile, Classroom
from charmming.models import ApbsParams, RedoxTask
from charmming.models import RexParams, MdTask, LdTask, SgldTask
from charmming.models import Lesson1, Lesson2, Lesson3, Lesson4, Lesson5, Lesson6, Lesson7, Lesson8
from charmming.models import LessonMakerLesson, Step
from charmming.models import MinimizeTask, MutateTask, NmodeTask, QchemTask, SolvationTask
from charmming.models import Structure, EnergyTask, GoModel, BlnModel
from charmming.models import TrajanalParams

# Account
admin.site.register(TeacherProfile)
admin.site.register(StudentProfile)
admin.site.register(Classroom)

# APBS
admin.site.register(RedoxTask)
admin.site.register(ApbsParams)

# Dynamics
admin.site.register(RexParams)
admin.site.register(MdTask)
admin.site.register(LdTask)
admin.site.register(SgldTask)

# Lessons
admin.site.register(Lesson1)
admin.site.register(Lesson2)
admin.site.register(Lesson3)
admin.site.register(Lesson4)
admin.site.register(Lesson5)
admin.site.register(Lesson6)
admin.site.register(Lesson7)
admin.site.register(Lesson8)

# Lesson Maker
admin.site.register(LessonMakerLesson)
admin.site.register(Step)

# Misc Tasks
admin.site.register(MinimizeTask)
admin.site.register(MutateTask)
admin.site.register(NmodeTask)
admin.site.register(QchemTask)
admin.site.register(SolvationTask)

# Structure
admin.site.register(Structure)
admin.site.register(EnergyTask)
admin.site.register(GoModel)
admin.site.register(BlnModel)

# Trajanal
admin.site.register(TrajanalParams)
