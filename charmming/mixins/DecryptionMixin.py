from django.conf import settings

from charmming.utils.encryption import decrypt

import json

class DecryptionMixin(object):
  """
  Handles decrypting a request, if it is an encrypted request.

  Currently, if the HTTP_X_CHARMMING_APP (x-charmming-app) header is set,
  the data is expected to be encrypted.
  """
  def dispatch(self, request, *args, **kwargs):
    """
    Attempt to decrypt a given request, if it's from a charmming app.

    If the request is a get request, the params are expected to have the
    `data` parameter with an encrypted JSON string in it.

    If the request is a different method, the body is expected to be
    encrypted.
    """
    error = {
      'error': {
        'message': 'Invalid encryption.'
      }
    }

    if request.META.get('HTTP_X_CHARMMING_APP'):
      if request.method == 'GET':
        if not request.GET.get('data'):
          return error

        raw = request.GET['data']
        data = decrypt(settings.SECRET_KEY.encode('utf-8'), raw.encode('utf-8'))

        if not data:
          return error

        params = json.loads(data)

        setattr(request, 'GET', params)

      else:
        raw = request.body
        data = decrypt(settings.SECRET_KEY.encode('utf-8'), raw)

        if not data:
          return error

        setattr(request, '_body', data)

    return super().dispatch(request, *args, **kwargs)
