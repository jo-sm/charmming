import json

class JSONMixin(object):
  """
  Parses the JSON request object and puts it in place
  of the request[METHOD]. For example, if logging in,
  it will do something like:

  request.POST = {
    'username': 'test',
    'password': 'test'
  }
  """
  def dispatch(self, request, *args, **kwargs):
    try:
      body_str = request.body.decode('utf-8')
    except UnicodeDecodeError:
      # Could not decode the object, just continue
      return super().dispatch(request, *args, **kwargs)

    try:
      json_body = json.loads(body_str)
    except json.decoder.JSONDecodeError:
      return super().dispatch(request, *args, **kwargs)

    setattr(request, request.method, json_body)

    return super().dispatch(request, *args, **kwargs)
