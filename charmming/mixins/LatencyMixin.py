from django.conf import settings

from charmming.utils.log import log

import time

class LatencyMixin(object):
  def dispatch(self, request, *args, **kwargs):
    if settings.LATENCY > 0:
      log('Waiting {} seconds...'.format(settings.LATENCY), level='warning')
      time.sleep(settings.LATENCY)

    return super().dispatch(request, *args, **kwargs)
