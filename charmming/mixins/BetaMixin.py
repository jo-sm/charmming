from django.shortcuts import render

class BetaMixin(object):
  def dispatch(self, request, *args, **kwargs):
    # TODO: Put logic to determine if user is in beta
    # in an ACL model
    return render(request, 'html/app.html', {})
