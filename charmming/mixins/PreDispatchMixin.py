class PreDispatchMixin(object):
  def dispatch(self, request, *args, **kwargs):
    if hasattr(self, 'pre'):
      # Generally this is used to set up additional
      # parameters for the actual request handler
      self.pre(request, *args, **kwargs)

    if hasattr(self, 'pre_{}'.format(request.method.lower())):
      getattr(self, 'pre_{}'.format(request.method.lower()))(request, *args, **kwargs)

    return super().dispatch(request, *args, **kwargs)
