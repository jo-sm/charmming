from django.http import HttpResponse
from django.shortcuts import render

from charmming.utils.json import dumps
from charmming.utils.log import log

import datetime
import re

class APIMixin(object):
  """
  Mixin that handles returning API requests for views.

  Each view is expected to return a dict structured like:

  {
    # Redirect to given path
    'redirect': '/path/to/redirect',

    # Error with message and code
    'error': {
      'message': 'Error message',
      'code': 422,
    },

    # Status messages
    'statuses': {
      'success': 'Success message',
      'info': 'Informational message',
      'warning': 'Warning message',
    },

    # Additional context for route
    'context': {
      Your context here...
    }
  }

  If the request is JSON, this will be formatted into a JSON
  object. If the request is HTML, the dict will be formatted
  and passed to the template defined in the view.
  """
  def dispatch(self, request, *args, **kwargs):
    start = datetime.datetime.now()
    result = super().dispatch(request, *args, **kwargs)

    if hasattr(request, request.method):
      resp_format = getattr(request, request.method).get('format')
    else:
      resp_format = ""

    # If we encounter a non-dict, return it without processing further.
    # This shouldn't happen in normal views
    if type(result) is not dict:
      log('View returned non dict as result: {}'.format(result), level='warning')

      return result

    status = result.get('status') or {}
    error = result.get('error')

    if error:
      # Build the response dict
      # If we have an error, we only include that error without any context
      if type(error) is not dict:
        error = {}

      status = {
        'type': 'error',
        'message': error.get('message', 'An error occurred'),
        'code': error.get('code', 500)
      }
    else:
      status = {
        'type': 'success',
        'code': 200
      }

    # Certain error codes, like 404, are treated differently.
    # 404 returns only the code, and the type in the status
    # key.
    if status['code'] == 404:
      response_obj = {
        'status': {
          'code': 404,
          'type': 'error'
        }
      }
    else:
      response_obj = result.get('context') or {}
      response_obj['status'] = status

    if re.search('application/json', request.META.get('HTTP_ACCEPT')) or resp_format == 'json':
      response = HttpResponse(dumps(response_obj), content_type='application/json', status=response_obj['status']['code'])
    else:
      response = render(request, "html/app.html")

    if result.get('cookies'):
      cookies = result.get('cookies')
      for name, val in list(cookies.items()):
        # If the value is explicitly none, we unset the cookie
        if val is None:
          response.delete_cookie(name)
        else:
          response.set_cookie(name, val)

    now = datetime.datetime.now()
    response_time = (now - start).total_seconds()
    log('Response for {} took {} ms'.format(request.path, response_time * 1000), level='debug')

    return response
