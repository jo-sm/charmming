from charmming.models import Session
from charmming.utils.token import determine_instance_id_from_token

class AuthMixin(object):
  """
  Handles authenticating and retrieving the session for a
  request.
  """
  def dispatch(self, request, *args, **kwargs):
    # TODO: Do some double checking on the IP and useragent
    # The IP might change, but the useragent is unlikely to change, but
    # the IP should be in the same general area. If it's not, force a
    # session expiration and have the user login again.
    if not hasattr(self, 'no_auth'):

      if hasattr(self, 'internal'):
        internal = self.internal
        internal_route = False

        if type(internal) == bool and self.internal:
          internal_route = True
        elif type(internal) == list:
          if request.method.lower() in self.internal:
            internal_route = True

        if internal_route:
          # This route is only for internal use, e.g. for schedd and notificationd

          # Check to see which app is requesting this
          if not request.META.get('HTTP_X_CHARMMING_APP'):
            return {
              'error': {
                'code': 404
              }
            }

          # We know that the app is okay at this point, because
          # its body was properly decrypted.

          # Internal use routes do not use a session, so
          # no session is set on the request.
          return super().dispatch(request, *args, **kwargs)

      token = request.COOKIES.get('charmming-token')

      if not token:
        return {
          'cookies': {
            'charmming-token': None
          },
          'error': {
            'message': 'Login required.'
          }
        }

      session_id = determine_instance_id_from_token(token)

      if not session_id:
        # The token wasn't valid and didn't contain
        # a valid session id
        return {
          'cookies': {
            'charmming-token': None
          },
          'error': {
            'message': 'Please login again.'
          }
        }

      sessions = Session.objects.filter(id=session_id)

      # This session was likely deleted by the user
      # TODO: handle inactivating sessions without deleting
      if len(sessions) != 1:
        return {
          'cookies': {
            'charmming-token': None
          },
          'error': {
            'message': 'Please login again.'
          }
        }

      session = sessions[0]
      setattr(request, 'session', session)

    # TODO: handle ACL here
    # if hasattr(self, 'admin') and not request.user.is_superuser:
    #   return {
    #     'redirect': '/'
    #   }

    return super().dispatch(request, *args, **kwargs)
