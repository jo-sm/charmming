class BaseDataValidator(dict):
  """
  A dict that provides special
  validation specific methods.
  """
  def __init__(self, data):
    if type(data) != dict:
      raise TypeError('Data must be of type dict. Type given: {}'.format(type(data)))

    self.__valid = False
    self.__validated = False
    self.__validators = {}

    for key, value in data.items():
      self[key] = value

  @property
  def valid(self):
    if self.__validated:
      return self.__valid

    valid = True

    for key, validator in self.__validators.items():
      value = self.get(key)

      valid = validator(value, self)

      if not valid:
        break

    self.__validated = True
    self.__valid = valid

    return valid

  def set_validator(self, key, validator):
    self.__validators[key] = validator

  def __reset_validation(self):
    self.__valid = False
    self.__validated = False

  def __setitem__(self, *args):
    super().__setitem__(*args)
    self.__reset_validation()

  def __delitem__(self, *args, **kwargs):
    super().__delitem__(*args)
    self.__reset_validation()
