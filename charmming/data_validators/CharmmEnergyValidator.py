from charmming.data_validators import BaseDataValidator

class CharmmEnergyValidator(BaseDataValidator):
  def __init__(self, data):
    # Sanitize the data

    if data.get('qmmm') and data.get('oniom'):
      return {
        'error': {
          'message': 'Select either QM/MM or ONIOM model types for multi scale modeling.'
        }
      }

    if data.get('gbmv') and data.get('scpism'):
      return {
        'error': {
          'message': 'Select either GBMV or SCPISM for implicit solvation.'
        }
      }
