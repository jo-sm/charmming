from charmming.utils.socket import send, recv

import socket
import os

class SchedInterface:
  # Based on:
  # http://stackoverflow.com/questions/17667903/python-socket-receive-large-amount-of-data

  def submit(self, task, **kwargs):
    coordinates_task = task.coordinates_task

    if coordinates_task:
      coordinates = coordinates_task.get_coordinates()
    else:
      coordinates = None

    script_set = task.get_scripts()

    scripts = [ {
      'path': script.path,
      'exe': script.executable,
      'nproc': script.processors
    } for script in script_set ]

    message = {
      'command': 'submit',
      'data': {
        'dir': task.dir(),
        'task_id': task.id,
        'scripts': scripts,
        'coordinates': coordinates
      }
    }

    send(self.sock, message)
    response = recv(self.sock)

    if response.get('status') == 'error':
      return None

    data = response.get('data')
    if not data.get('id'):
      return None

    task.job_id = data.get('id')
    task.save()

    return task.job_id

  def kill(self, task):
    job_id = task.job_id

    message = {
      'command': 'cancel',
      'data': {
        'id': job_id
      }
    }

    send(self.sock, message)
    response = recv(self.sock)

    if response.get('status') == 'error':
      return False

    data = response.get('data')
    if not data.get('id'):
      return False

    return True

  def __del__(self):
    message = {
      'command': 'end'
    }
    send(self.sock, message)
    self.sock.close()

  def __init__(self):
    self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    self.sock.connect((os.environ.get('CHARMMING_SCHEDULER_HOST'), 9995))

    message = {
      'command': 'hello'
    }

    send(self.sock, message)
    response = recv(self.sock)

    if not response.get('hello') or response.get('hello') != 'world':
      self.sock.close()
      raise Exception('Protocol error from server.')
