from django import forms

class QsarTestUploadForm(forms.Form):
  def __init__(self, *args, **kwargs):
    active = kwargs['active']
    inactive = kwargs['inactive']
    saved_model = kwargs['saved_model']
    threshold = kwargs['threshold']
    activity_property = kwargs['activity_property']
    filename = kwargs['filename']
    del kwargs['active']
    del kwargs['inactive']
    del kwargs['saved_model']
    del kwargs['threshold']
    del kwargs['activity_property']
    del kwargs['filename']
    super(QsarTestUploadForm, self).__init__(*args, **kwargs)
    activewidget = forms.HiddenInput(attrs={'value': active})
    inactivewidget = forms.HiddenInput(attrs={'value': inactive})
    saved_modelwidget = forms.HiddenInput(attrs={'value': saved_model})
    thresholdwidget = forms.HiddenInput(attrs={'value': threshold})
    activity_propertywidget = forms.HiddenInput(attrs={'value': activity_property})
    filenamewidget = forms.HiddenInput(attrs={'value': filename})
    self.fields['active'] = forms.CharField(widget=activewidget, required=True)
    self.fields['inactive'] = forms.CharField(widget=inactivewidget, required=True)
    self.fields['saved_model'] = forms.CharField(widget=saved_modelwidget, required=True)
    self.fields['threshold'] = forms.CharField(widget=thresholdwidget, required=True)
    self.fields['activity_property'] = forms.CharField(widget=activity_propertywidget, required=True)
    self.fields['filename'] = forms.CharField(widget=filenamewidget, required=True)
    self.fields['predictfile'] = forms.FileField(label='Select a file', help_text='SDF with structures')
