from django import forms

from charmming.models import QSARModelType

class QsarTrainingUploadForm(forms.Form):
  model_type1 = forms.ModelChoiceField(queryset=QSARModelType.objects.filter(is_categorical=True), required=False)
  model_type2 = forms.ModelChoiceField(queryset=QSARModelType.objects.filter(is_categorical=False), required=False)
  model_name = forms.CharField(max_length=100, required=True)
  trainfile = forms.FileField(
      label='Select a training file',
      help_text='SDF with structures and activities'
  )
