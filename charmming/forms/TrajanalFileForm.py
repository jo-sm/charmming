from django import forms

class TrajanalFileForm(forms.Form):
  startstep = forms.CharField(max_length=5)
  stopstep = forms.CharField(max_length=5)
  skip = forms.CharField(max_length=5)
  atomselection = forms.CharField(max_length=50)
  trjfile = forms.Field(widget=forms.FileInput())
