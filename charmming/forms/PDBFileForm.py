from django import forms

class PDBFileForm(forms.Form):
  pdbid = forms.CharField(max_length=5)
  sequ = forms.CharField(widget=forms.widgets.Textarea())
  pdbupload = forms.FileField()
  psfupload = forms.FileField()
  crdupload = forms.FileField()
  rtf_file = forms.FileField()
  prm_file = forms.FileField()

  # Go model stuffs
  gm_dm_file = forms.FileField()
  gm_dm_string = forms.CharField(max_length=25)
  gm_nScale = forms.CharField(max_length=10, initial="0.05")
  gm_kBond = forms.CharField(max_length=10, initial="50.0")
  gm_kAngle = forms.CharField(max_length=10, initial="30.0")

  # BLN model stuffs
  bln_dm_file = forms.FileField()
  bln_dm_string = forms.CharField(max_length=25)
  bln_nScale = forms.CharField(max_length=10, initial="1.0")
  bln_kBondHelix = forms.CharField(max_length=8, initial="3.5")
  bln_kBondSheet = forms.CharField(max_length=8, initial="3.5")
  bln_kBondCoil = forms.CharField(max_length=8, initial="2.5")
  bln_kAngleHelix = forms.CharField(max_length=8, initial="8.37")
  bln_kAngleSheet = forms.CharField(max_length=8, initial="8.37")
  bln_kAngleCoil = forms.CharField(max_length=8, initial="5.98")
