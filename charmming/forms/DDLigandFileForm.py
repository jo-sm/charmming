from django import forms

class DDLigandFileForm(forms.Form):
  ligand_file = forms.FileField()
