from django import forms

from charmming.models import QSARModelType

class AssayTrainingSubmitForm(forms.Form):
  def __init__(self, *args, **kwargs):
    filename = ""
    if 'filename' in kwargs:
      filename = kwargs['filename']
      del kwargs['filename']
    query = ""
    if 'query' in kwargs:
      query = kwargs['query']
      del kwargs['query']
    num_mols = 0
    if 'num_mols' in kwargs:
      num_mols = kwargs['num_mols']
      del kwargs['num_mols']
    super(AssayTrainingSubmitForm, self).__init__(*args, **kwargs)
    model_choices1 = [(obj.id, obj.model_type_name) for obj in QSARModelType.objects.all() if obj.is_categorical]
    self.fields['model_type1'] = forms.ChoiceField(choices=model_choices1, required=False)
    model_choices2 = [(obj.id, obj.model_type_name) for obj in QSARModelType.objects.all() if not obj.is_categorical]
    self.fields['model_type2'] = forms.ChoiceField(choices=model_choices2, required=False)
    self.fields['model_name'] = forms.CharField(max_length=100, required=True)
    filename_widget = forms.HiddenInput(attrs={'value': filename})
    self.fields['filename'] = forms.CharField(widget=filename_widget, required=True)
    query_widget = forms.HiddenInput(attrs={'value': query})
    self.fields['query'] = forms.CharField(widget=query_widget, required=True)
    num_mols_widget = forms.HiddenInput(attrs={'value': num_mols})
    self.fields['num_mols'] = forms.CharField(widget=num_mols_widget, required=True)
