from django import forms

class AssaySearchForm(forms.Form):
  query = forms.CharField(max_length=200, required=True)
