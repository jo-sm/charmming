from django import forms

class DDTargetFileForm(forms.Form):
  target_file = forms.FileField()
  psf_target_file = forms.FileField()
