from django.contrib.auth.models import User

from charmming.views import BaseView
from charmming.models import DDLigand, DDFileObject, DDLigandSetsLigands
from charmming.utils.dd_substrate import LigandInfo

class DDUpdateDSFLigandsView(BaseView):
  template_name = 'html/dddsfligands.html'

  def post(self, request, *args, **kwargs):
    selected_set_id = kwargs.get('selected_set_id')
    ligand_info_list = []
    user_ligands = DDLigand.objects.filter(owner=request.user).select_related()
    public_user = User.objects.get(username='public')
    public_ligands = DDLigand.objects.filter(owner=public_user).select_related()

    if (selected_set_id == '0'):  # show all ligands
      for ligand in user_ligands:
        ligand_info = LigandInfo()
        ligand_info.id = ligand.id
        ligand_info.name = ligand.ligand_name
        ligand_info.description = ligand.description
        ligand_info.file_objects_list = DDFileObject.objects.filter(owner=request.user, object_table_name="dd_substrate_ligands", object_id=ligand.id).select_related()
        for object in ligand_info.file_objects_list:
          object.file.file_location = object.file.file_location.replace("/home/schedd/", "/charmming/pdbuploads/")
        ligand_info_list.append(ligand_info)
      for ligand in public_ligands:
        ligand_info = LigandInfo()
        ligand_info.id = ligand.id
        ligand_info.name = ligand.ligand_name
        ligand_info.description = ligand.description
        ligand_info.file_objects_list = DDFileObject.objects.filter(owner=public_user, object_table_name="dd_substrate_ligands", object_id=ligand.id).select_related()
        for object in ligand_info.file_objects_list:
          object.file.file_location = object.file.file_location.replace("/home/schedd/", "/charmming/pdbuploads/")
        ligand_info_list.append(ligand_info)

    elif (selected_set_id == '00'):  # user ligands
      for ligand in user_ligands:
        ligand_info = LigandInfo()
        ligand_info.id = ligand.id
        ligand_info.name = ligand.ligand_name
        ligand_info.description = ligand.description
        ligand_info.file_objects_list = DDFileObject.objects.filter(owner=request.user, object_table_name="dd_substrate_ligands", object_id=ligand.id).select_related()
        for object in ligand_info.file_objects_list:
          object.file.file_location = object.file.file_location.replace("/home/schedd/", "/charmming/pdbuploads/")
        ligand_info_list.append(ligand_info)
    else:
      selected_set_ligands = DDLigandSetsLigands.objects.filters(owner__id__in=[request.user.id, public_user.id], ligands_set__id=selected_set_id).select_related()
      selected_set_ligands = [ l.ligand for l in selected_set_ligands ]
      for ligand in selected_set_ligands:
        ligand_info = LigandInfo()
        ligand_info.id = ligand.id
        ligand_info.name = ligand.ligand_name
        ligand_info.description = ligand.description
        ligand_info.file_objects_list = DDFileObject.objects.filter(object_table_name="dd_substrate_ligands", object_id=ligand.id).select_related()
        for object in ligand_info.file_objects_list:
          object.file.file_location = object.file.file_location.replace("/home/schedd/", "/charmming/pdbuploads/")
        ligand_info_list.append(ligand_info)

    return {
      'context': {
        'ligands': ligand_info_list
      }
    }
