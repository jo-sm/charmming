from charmming.views import BaseView
from charmming.models import Task
from charmming.utils.notifications import send_notification

from schedulerd.utils import states

class TaskView(BaseView):
  internal = [ 'put' ]

  def get(self, request, *args, **kwargs):
    task_id = kwargs.get('id')
    tasks = Task.objects.filter(id=task_id)

    if len(tasks) == 0:
      return {
        'error': {
          'message': 'Task {} not found'.format(task_id)
        }
      }

    task = tasks[0]

    return {
      'context': {
        'task': task
      }
    }

  def put(self, request, *args, **kwargs):
    data = request.PUT
    task_id = kwargs.get('id')
    new_task_state = data.get('state')

    if not new_task_state:
      return {
        'error': {
          'message': 'Provide a state for task {}'.format(task_id)
        }
      }

    if new_task_state not in list(states.values()):
      return {
        'error': {
          'message': 'Invalid state {} provided for task {}'.format(new_task_state, task_id)
        }
      }

    tasks = Task.objects.filter(id=task_id)

    if len(tasks) == 0:
      return {
        'error': {
          'message': 'Task {} does not exist.'.format(task_id)
        }
      }

    task = tasks[0]
    task.state = new_task_state
    task.save()

    task_owner = task.structure.owner
    send_notification('updateTask', {
      'id': task.id,
      'status': task.state.lower()
    }, user=task_owner)

    return {
      'context': {
        'task': task_id
      }
    }
