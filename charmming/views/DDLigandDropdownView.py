from charmming.views import BaseView
from charmming.models import DDLigand

class DDLigandDropdownView(BaseView):
  template_name = 'html/ddliganddropdown.html'

  def get(self, request, *args, **kwargs):
    ligandlist = DDLigand.objects.filter(owner=request.user).order_by("ligand_name")

    return {
      'context': {
        'ligandlist': ligandlist
      }
    }
