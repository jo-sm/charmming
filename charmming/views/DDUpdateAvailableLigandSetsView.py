from django.contrib.auth.models import User

from charmming.views import BaseView
from charmming.models import DDLigand, DDLigandSetsLigands, DDFileObject
from charmming.utils.dd_substrate import LigandInfo

class DDUpdateAvailableLigandSetsView(BaseView):
  template_name = 'html/ddavailablesetligands.html'

  def get(self, request, *args, **kwargs):
    selected_set_id = kwargs.get('selected_set_id')
    ligand_info_list = []
    public_user = User.objects.get(username='public')
    if (str(selected_set_id) == '0'):  # show all ligands

      user_ligands = DDLigand.objects.filter(owner=request.user).select_related()

      public_ligands = DDLigand.objects.filter(owner=public_user)

      for user_ligand in user_ligands:
        ligand_info = LigandInfo()
        ligand_info.id = user_ligand.id
        ligand_info.name = user_ligand.ligand_name
        ligand_info.description = user_ligand.description
        ligand_info_list.append(ligand_info)

      for ligand in public_ligands:
        ligand_info = LigandInfo()
        ligand_info.id = ligand.id
        ligand_info.name = ligand.ligand_name
        ligand_info.description = ligand.description
        ligand_info_list.append(ligand_info)

    else:  # show selected set ligands
      selected_set_ligands = DDLigandSetsLigands.objects.filter(ligand__owner__id__in=[request.user.id, public_user.id], ligands_set__id=selected_set_id).select_related()
      selected_set_ligands = [ s.ligand for s in selected_set_ligands ]
      for ligand in selected_set_ligands:
        ligand_info = LigandInfo()
        ligand_info.id = ligand.id
        ligand_info.name = ligand.ligand_name
        ligand_info.description = ligand.description
        ligand_info_list.append(ligand_info)

    for ligand_info in ligand_info_list:
      ligand_file = DDFileObject.objects.filter(object_table_name='dd_substrate_ligands', object_id=ligand_info.id).values('file__id', 'file__file_name', 'file__file_location')

      if len(ligand_file) == 0:
        ligand_info.file_id = 0
      else:
        ligand_file = ligand_file[0]
        ligand_info.file_id = ligand_file.file__id
        ligand_info.file_location = ligand_file.file__file_location.replace("/home/schedd/", "/charmming/pdbuploads/") + ligand_file.file__file_name

    return {
      'context': {
        'available_ligands': ligand_info_list
      }
    }
