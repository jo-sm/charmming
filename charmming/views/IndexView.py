from charmming.views import BaseView

class IndexView(BaseView):
  template_name = 'html/frontpage.html'
  no_auth = True
