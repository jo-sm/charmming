from django.views.generic import View
from charmming.mixins import APIMixin, AuthMixin, JSONMixin, PreDispatchMixin, LatencyMixin, DecryptionMixin

class BaseView(APIMixin, DecryptionMixin, AuthMixin, JSONMixin, PreDispatchMixin, LatencyMixin, View):
  def dispatch(self, request, *args, **kwargs):
    """
    If a template name is defined, allow get implicitly
    """
    if request.method.lower() == 'get':
      if not hasattr(self, 'no_get') and not hasattr(self, 'get'):
        self.get = self._get

    return super().dispatch(request, *args, **kwargs)

  def _get(self, request, *args, **kwargs):
    return {}
