from charmming.models import Task
from charmming.views import BaseView
from charmming.helpers.views import Accountinator
from charmming.utils.propka import calcpropka

import os

class PropkaFormView(BaseView):
  template_name = 'html/propka_task.html'

  @Accountinator(cg_fail="PROPKA")
  def get(self, request, *args, **kwargs):
    # NOTE: The below two kwargs come in from Accountinator.
    # struct = kwargs.get("struct")
    ws = kwargs.get("ws")
    if ws.isBuilt != 't':
      # can't let you do that
      return {
        'error': {
          'message': 'You need to build your working structure before using PROPKA. Please perform a calculation first.'
        },
        'redirect': '/energy'
      }

    tdict = {}
    tdict['ws_identifier'] = ws.identifier
    tasks = Task.objects.filter(workstruct=ws, status='C', active='y', modifies_coordinates=True)
    # we need to get the tasks to find out the files we need to put up for download
    propka_files = []
    file_loc = ws.structure.location  # to make the loop a little faster
    ident = ws.identifier
    for task in tasks:
      suffix = "{}-{}-propka.pka".format(ident, task.action)
      try:
        os.stat(os.path.join(file_loc, suffix))
        propka_files.append((task.action, suffix))
      except:
        continue
    tdict['propka_files'] = propka_files
    # we don't actually need the following logic except for the display page.
  #    propka_lines = []
  #    if len(propka_files) > 0:
  #        for file in propka_files:
  #            try:
  #                propkafp = open('%s-%s-propka.pka' % (ws.identifier,task.action),'r')
  #                propka_lines = propkafp.readlines()
  #                propkafp.close()
  #            except: #it's unlikely anything will ever happen here, so
  #                continue
  #
  #    tdict['propka_lines'] = propka_lines

    if 'form_submit' in request.POST:
      # we're using task stuff for this. The job scheduler has been modified.
      pTaskID = int(request.POST['ptask'])
      pTask = Task.objects.get(id=pTaskID)
      return calcpropka(request, ws, pTask)
    else:
      tdict['tasks'] = tasks  # tasks was assigned before
      # we do need these tasks so we can get the right coordinates
      return {}
