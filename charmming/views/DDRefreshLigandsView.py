from django.contrib.auth.models import User

from charmming import charmming_config
from charmming.views import BaseView
from charmming.models import DDLigand, DDFile, DDFileObject, DDSource, DDFileType
from charmming.utils.dd_substrate import getMoleculeTitle, getNewLigandOwnerIndex

import os
import glob
import time

class DDRefreshLigandsView(BaseView):
  def post(self, request, *args, **kwargs):
    location = charmming_config.user_dd_ligands_home + '/public/zinctest3/'
    os.system("cd %s\n" % (location))
    os.environ["MATCH"] = "/usr/local/charmming/MATCH_RELEASE/MATCH"
    os.environ["PerlChemistry"] = "/usr/local/charmming/MATCH_RELEASE/PerlChemistry"
    u = User.objects.get(username='public')
    count = 0
    newligands = []
    for file in glob.glob(os.path.join(location, '*.mol2')):
      os.system("/usr/local/charmming/MATCH_RELEASE/MATCH/scripts/MATCH.pl %s > %smatchresult\n" % (file, location))
      while os.path.exists(location + "matchresult") is False:
        time.sleep(3)
      if "Success!" in open(location + 'matchresult').read():
        file_obj = open(file, 'r')
        mol_title = getMoleculeTitle(file_obj)
        newligand = DDLigand()
        newfile = DDFile()
        newfileobject = DDFileObject()
        try:
          newligands = DDLigand.objects.filter(owner=u, ligand_name=mol_title)
        except:
          pass
        if len(newligands) == 0:
          newligand.owner = u
          newligand.ligand_name = mol_title
          newligand.ligand_owner_index = str(getNewLigandOwnerIndex(u))
          newligand.description = "System preloaded drug-like compound"
          source = DDSource.objects.get(source_name='Zinc Database')
          newligand.source = source
          newligand.save()

          new_location = charmming_config.user_dd_ligands_home + '/' + 'public' + '/' + 'ligand_' + str(newligand.ligand_owner_index) + '/'
          filename = 'ligand_' + str(newligand.ligand_owner_index) + '_1.mol2'
          ligand_file_type = DDFileType.objects.get(file_type_name="Ligand Structure File")
          newfile.owner = u
          newfile.file_name = filename
          newfile.file_location = new_location
          newfile.file_type = ligand_file_type
          newfile.description = "Structure File for Ligand %s, %s" % (newligand.ligand_name, newligand.description)
          newfile.save()

          newfileobject.owner = u
          newfileobject.file = newfile
          newfileobject.object_table_name = 'dd_substrate_ligands'
          newfileobject.object_id = newligand.id
          newfileobject.save()

          os.system("mkdir " + new_location)
          os.system("cp %s %s%s" % (file, new_location, filename))

      count = count + 1
      if count > 1000:
        break

    return {
      'statuses': {
        'success': 'Done.'
      }
    }
