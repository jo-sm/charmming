from charmming.views import BaseView
from charmming.models import Structure, WorkingStructure, DDJob
from charmming.utils.dd_infrastructure import objDDJob

from datetime import datetime

class DDJobsView(BaseView):
  template_name = 'html/ddviewjobs.html'

  def get(self, request, *args, **kwargs):
    try:
      struct = Structure.objects.get(owner=request.user, selected='y')
    except:
      return {
        'error': {
          'message': 'Please submit a structure first.'
        },
        'redirect': '/fileupload'
      }

    try:
      WorkingStructure.objects.get(structure=struct, selected='y')
    except:
      return {
        'error': {
          'message': 'Please build a structure first.'
        },
        'redirect': '/buildstruct'
      }

    jobs = []

    rows = DDJob.objects.filter(job_scheduler_id__userid=request.user.id).values(
      'id',
      'job_scheduler_id',
      'job_owner_index',
      'description',
      'job_scheduler_id__queued',
      'job_scheduler_id__started',
      'job_scheduler_id__ended',
      'job_scheduler_id__state'
    )

    for row in rows:
      if row.job_scheduler_id__state == 1:
        status = 'Queued'
      elif row.job_scheduler_id__state == 2:
        status = 'Running'
      elif row.job_scheduler_id__state == 3:
        status = 'Done'
      elif row.job_scheduler_id__state == 4:
        status = 'Failed'

      if status == 'Queued':
        waited_time = (datetime.now() - row.job_scheduler_id__queued).total_seconds()
      else:
        waited_time = (row.job_scheduler_id__ended - row.job_scheduler_id__queued).total_seconds()

      if status == 'Running':
        runtime_time = (datetime.now() - row.job_scheduler_id__started).total_seconds()
        total_time = (datetime.now() - row.job_scheduler_id__queued).total_seconds
      else:
        runtime_time = (row.job_scheduler_id__ended - row.job_scheduler_id__started).total_seconds()
        total_time = (row.job_scheduler_id__ended - row.job_scheduler_id__queued).total_seconds()

      job = objDDJob()
      job.id = row.id,
      job.scheduler_id = row.job_scheduler_id
      job.owner_index = row.job_owner_index
      job.description = row.description
      job.status = status
      job.queued = row.job_scheduler_id__queued
      job.waited = waited_time
      job.runtime = runtime_time
      job.totaltime = total_time
      jobs.append(job)

    return {
      'context': {
        'jobs': jobs
      }
    }
