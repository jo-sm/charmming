from charmming.views import BaseView
from charmming.models import DDLigand, DDLigandSet, DDLigandSetsLigands, DDFileObject
from charmming.utils.dd_substrate import LigandInfo

class DDSetLigandsView(BaseView):
  template_name = 'html/ddsetligands.html'

  def get(self, request, *args, **kwargs):
    ligandset_id = kwargs.get('ligandset_id')
    addedids = kwargs.get('addedids')
    removedids = kwargs.get('removedids')

    if (ligandset_id != '0') and (ligandset_id != ''):
      if (addedids != "") and (addedids != '0'):
        addedids_list = [int(n) for n in addedids.split(',')]
        added_ligands = DDLigand.objects.filter(id__in=addedids_list)

        for ligand in added_ligands:
          set_ligand = DDLigandSetsLigands(owner_id=request.user.id, ligands_set_id=ligandset_id, ligands_id=ligand.id)
          set_ligand.save()

      if (removedids != "") and (removedids != '0'):
        removedids_list = [int(n) for n in removedids.split(',')]
        DDLigandSetsLigands.objects.filter(id__in=removedids_list).delete()

      lig_set = DDLigandSet.objects.get(id=ligandset_id)
      setligands = DDLigandSetsLigands.objects.filter(owner=request.user, ligands_set=lig_set).select_related()

      ligand_info_list = []

      for setligand in setligands:
        ligand_info = LigandInfo()
        ligand_info.id = setligand.ligands.id
        ligand_info.ligandsetid = setligand.id
        ligand_info.name = setligand.ligands.ligand_name
        ligand_info.description = setligand.ligands.description
        ligand_info_list.append(ligand_info)

      for ligand_info in ligand_info_list:
        ligand_files = DDFileObject.objects.filter(object_table_name='dd_substrate_ligands', object_id=ligand_info.id).values('file__id', 'file__file_location', 'file__file_name')

        if len(ligand_files) == 0:
          ligand_info.file_id = 0
        else:
          ligand_file = ligand_files[0]
          ligand_info.file_id = ligand_file.file__id
          ligand_info.file_location = ligand_file.file__file_location.replace("/home/schedd/", "/charmming/pdbuploads/") + ligand_file.file__file_name

    return {
      'context': {
        'setligands': ligand_info_list,
        'ligandcount': len(setligands),
      }
    }
