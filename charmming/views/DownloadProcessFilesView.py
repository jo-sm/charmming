from django.http import HttpResponse

from charmming import charmming_config
from charmming.views import BaseView
from charmming.helpers.views import Accountinator

import os
import mimetypes

class DownloadProcessFilesView(BaseView):
  @Accountinator(struct_only=True)
  def downloadProcessFiles(request, filename, mimetype=None, **kwargs):
    # Does this work?
    # downloadGenericFiles doesn't exist anywhere
    """
    Actual file downloads happen here, along with MIME-type guessing.
    """
    # NOTE: the below kwarg comes in from Accountinator
    struct = kwargs.get("struct")
    username = request.user.username

    # if the filename contains a slash, it is probably not located in the pdb_uploads/ folder
    # an example would be filename == /solvation/water.crd which is located in /usr/local/charmming
    # slash = re.compile("/")
    # downloadGenericFiles doesn't exist anywhere
    # if (slash.search(filename)):
    #   return downloadGenericFiles(request, filename)
    if mimetype is None:
      mimetype, encoding = mimetypes.guess_type("%s/%s/%s" % (charmming_config.user_home, username, filename))

    try:
      sval = os.stat("%s/%s" % (struct.location, filename))
    except:
      return {
        'error': {
          'message': "{} does not exist.".format(filename)
        },
        'redirect': '/viewprocessfiles'
      }

    response = HttpResponse(mimetype=mimetype)
    response['Content-Disposition'] = 'attachment; filename=%s' % filename
    response['Content-length'] = sval.st_size
    response.write(open("%s/%s" % (struct.location, filename), "rb").read())

    return response
