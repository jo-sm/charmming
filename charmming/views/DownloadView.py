from charmming.views import BaseView
from charmming.helpers.views import Accountinator

class DownloadView(BaseView):
  template_name = 'html/newdownload.html'

  @Accountinator()
  def get(self, request, *args, **kwargs):
    # This one just makes the template for the page. No big deal here.
    # The main "meat" of this file should be in fetch_path, because it creates
    # the data structure for the rest.
    # NOTE: this kwarg comes in from Accountinator
    ws = kwargs.get("ws")
    tdict = {}
    tdict['ws'] = ws

    return {
      'context': tdict
    }
