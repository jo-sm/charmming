from django.contrib.auth.models import User

from charmming.views import BaseView
from charmming.models import DDFile

import openbabel

class DDLigandGLMolView(BaseView):
  template_name = 'html/ddviewligandglmol.html'

  def viewLigandGLmol(request, ligand_file_id):
    username = request.user.username
    u = User.objects.get(username=username)
    public_user = User.objects.get(username='public')

    ligandfile = DDFile.objects.get(owner__in=[u, public_user], id=ligand_file_id.replace("/", ""))
    filename = ligandfile.file_location + "/" + ligandfile.file_name

    # ligandfile=filename.replace("/home/schedd/","/charmming/pdbuploads/")
    # Add logic here for getting the PDB file somehow. Let's do an OBConv.

    obconv = openbabel.OBConversion()
    obconv.SetInAndOutFormats("mol2", "sdf")
    mol = openbabel.OBMol()
    obconv.ReadFile(mol, filename.encode("utf-8"))
    ligand_data = obconv.WriteString(mol)

    return {
      'context': {
        'ligand_data': ligand_data
      }
    }
