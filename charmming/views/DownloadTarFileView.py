from django.http import HttpResponse

from charmming import charmming_config
from charmming.views import BaseView
from charmming.helpers.views import Accountinator
from charmming.models import WorkingStructure, AtomSelection

import os
import subprocess
import mimetypes

class DownloadTarFileView(BaseView):
  @Accountinator(struct_only=True)
  def get(self, request, *args, **kwargs):
    """
    Puts all files associated with a particular file upload into a single TAR file.
    """
    # NOTE: the below kwarg comes in from Accountinator
    struct = kwargs.get("struct")
    tar_filename = struct.name + '.tar.gz'

    username = request.user.username
    os.chdir(charmming_config.user_home + '/' + username)

    try:
      os.unlink(tar_filename)
    except:
      pass

    # remove error and log files that might be lying around
    subprocess.call(["rm", "-f", (struct.name + '/*.{err,log}')])

    # subprocess.call(["cp", (charmming_config.data_home + '/solvation/water.crd'), struct.name])
    # TODO: Copy the actual waterbox used.

    subprocess.call(["cp", (charmming_config.data_home + '/calcewald.pl'), struct.name])
    subprocess.call(["cp", (charmming_config.data_home + '/savegv.py'), struct.name])
    subprocess.call(["cp", (charmming_config.data_home + '/savechrg.py'), struct.name])

    # WS being optional is not a standard path, therefore this will NOT be added to Accountinator...
    # get all of the topology and parameter files that we need.,, if there's a workingstructure
    try:
      ws = WorkingStructure.objects.filter(structure=struct, selected='y')[0]
    except:
      ws = None

    if ws:

      subprocess.call(["rm", "-rf", (struct.name + '/toppar/')])
      os.mkdir(struct.name + '/toppar')
      tplist, prlist = ws.getTopparList()

      if tplist and prlist:
        for tpf in tplist + prlist:
          subprocess.call(['cp', tpf, struct.name + '/toppar/'])

      if ws.topparStream:
        for stf in ws.topparStream.split():
          subprocess.call(['cp', stf, struct.name + '/toppar/'])

      sellst = AtomSelection.objects.filter(workstruct=ws)
      if sellst:
        # hey - we maybe did a QM/MM calculation here. We need renumber.str!
        subprocess.call(["cp", (charmming_config.data_home + '/renumber.str'), struct.name])

    subprocess.call(["tar", "-czf", tar_filename, struct.name])

    statinfo = os.stat(tar_filename)
    mimetype, encoding = mimetypes.guess_type("%s/%s/%s" % (charmming_config.user_home, username, tar_filename))

    response = HttpResponse(mimetype=mimetype)
    response['Content-Disposition'] = 'attachment; filename=%s' % tar_filename
    response['Content-Length'] = statinfo.st_size
    response.write(open(charmming_config.user_home + "/" + username + "/" + tar_filename, "rb").read())
    return response
