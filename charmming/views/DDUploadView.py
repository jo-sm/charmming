from django.contrib.auth.models import User

from charmming import charmming_config
from charmming.utils.dd_target import getNewProteinOwnerIndex, getNewConformationProteinIndex
from charmming.views import BaseView
from charmming.models import DDProtein, DDFile, DDFileObject, DDProteinConformation
from charmming.forms import DDTargetFileForm

import os

class DDUploadView(BaseView):
  template_name = 'html/ddtargetfileupload.html'

  def post(self, request, *args, **kwargs):
    username = request.user.username
    u = User.objects.get(username=username)
    newtarget = DDProtein()
    newfile = DDFile()
    newfileobject = DDFileObject()
    newconformation = DDProteinConformation()

    try:
      request.FILES['target_file'].name
      target_uploaded_by_user = 1
    except:
      target_uploaded_by_user = 0

    if target_uploaded_by_user == 1:
      try:
        newtarget = DDProtein.objects.filter(owner=u, id=request.POST['existingtarget'])[0]
      except:
        newtarget.owner = u
        newtarget.protein_name = request.POST['targetname']
        newtarget.pdb_code = request.POST['pdbcode']
        newtarget.protein_owner_index = str(getNewProteinOwnerIndex(u))
        newtarget.description = request.POST['targetdescription']
        newtarget.save()

      try:
        newconformation = DDProteinConformation.objects.filter(owner=u, id=request.POST['existingconformation'])[0]
      except:
        newconformation.owner = u
        newconformation.protein = newtarget
        newconformation.conformation_name = 'Default Conformation'  # request.POST['conformationname']
        newconformation.conformation_protein_index = str(getNewConformationProteinIndex(newtarget))
        newconformation.description = request.POST['targetdescription']

        newconformation.save()

      location = charmming_config.user_dd_targets_home + '/' + username + '/' + 'target_' + str(newtarget.protein_owner_index) + '/'
      filename = 'target_' + str(newtarget.protein_owner_index) + '_' + str(newconformation.conformation_protein_index) + '.pdb'
      psf_filename = 'target_' + str(newtarget.protein_owner_index) + '_' + str(newconformation.conformation_protein_index) + '.psf'
      os.system("mkdir " + location)
      destination = open(location + filename, 'w')
      psf_destination = open(location + psf_filename, 'w')
      for fchunk in request.FILES['target_file'].chunks():
        destination.write(fchunk)
      # destination.close()
      # destination.write(request.FILES['target_file']['content'])
      destination.close()
      for fchunk in request.FILES['psf_target_file'].chunks():
        psf_destination.write(fchunk)
      psf_destination.close()
      # target_file_type = file_types.objects.filter(file_type_name="Target Structure File")[0]
      newfile.owner = u
      newfile.file_name = filename
      newfile.file_location = location
      # newfile.file_type=target_file_type
      newfile.description = request.POST['filedesc']
      newfile.save()

      newfileobject.file = newfile
      newfileobject.owner = u
      newfileobject.object_table_name = 'dd_target_protein_conformations'
      newfileobject.object_id = newconformation.id
      newfileobject.save()

    form = DDTargetFileForm()
    return {
      'context': {
        'form': form
      }
    }
