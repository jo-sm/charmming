from charmming.views import BaseView
from charmming.models import DDLigand, DDFile, DDFileObject, DDPose
from charmming.utils.dd_substrate import DeletePose

class DDDeleteLigandView(BaseView):
  def post(self, request, *args, **kwargs):
    if 'id' in request.POST:
      delete_id = request.POST['id']
    else:
      delete_id = "1"

    if(delete_id == "all_ligands"):
      pass
    else:
      ligand = DDLigand.objects.get(owner__in=[request.user], id=delete_id)
      ligand_files_objects = DDFileObject.objects.filter(owner__in=[request.user], object_table_name='dd_substrate_ligands', object_id=delete_id)
      for ligand_file_object in ligand_files_objects:
        ligand_files = DDFile.objects.filter(owner__in=[request.user], id=ligand_file_object.file_id)
        for ligand_file in ligand_files:
          ligand_associations = DDFileObject.objects.filter(owner__in=[request.user], file=ligand_file).exclude(id=ligand_file_object.id)
          ligand_associations.delete()
          ligand_files.delete()
      ligand_poses = DDPose.objects.filter(pose_object_table_name="dd_substrate_ligands", pose_object_id=ligand.id)
      for ligand_pose in ligand_poses:
        if DeletePose(request.user, ligand_pose.id) is False:
          return {
            'error': {
              'message': 'Could not delete pose.'
            }
          }
      ligand_files_objects.delete()
      ligand.delete()
    return {
      'statuses': {
        'success': 'Done.'
      }
    }
