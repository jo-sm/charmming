from charmming.views import BaseView

class QMMMTemplateView(BaseView):
  template_name = 'html/qmmm_atomselection_new.html'
