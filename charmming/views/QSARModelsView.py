from django.contrib.auth.models import User

from charmming import charmming_config
from charmming.views import BaseView
from charmming.models import QSARModel, QSARJob, QSARJobType
from charmming.utils.qsar.common import ObjectWithAttributes, GetObjectAttributeValue
from charmming.utils.qsar.train import is_valid_sd
from charmming.scheduler.schedInterface import SchedInterface

import os

class QSARModelsView(BaseView):
  template_name = 'qsar/viewmodels.html'

  def get(self, request, *args, **kwargs):
    message = kwargs.get('message')
    username = request.user.username
    models_list = QSARModel.objects.select_related().filter(model_owner_id=request.user.id).order_by("-model_owner_index")
    models_with_attributes_list = []

    for qsar_model in models_list:
      model_with_attributes = ObjectWithAttributes()
      model_with_attributes.object_id = qsar_model.id
      model_with_attributes.object_owner_id = qsar_model.model_owner_id
      model_with_attributes.object_owner_index = qsar_model.model_owner_index
      model_with_attributes.object_name = qsar_model.model_name
      model_with_attributes.object_type = qsar_model.model_type.model_type_name
      model_with_attributes.FillObjectAttributeList("qsar_models")

      qsar_folder = charmming_config.user_home + "/" + username + "/qsar"
      # qsar_models_folder = qsar_folder + "/models"
      model_folder = qsar_folder + "/models/qsar_model_" + str(qsar_model.model_owner_index)

      training_output = model_folder + "/training_output"
      model_with_attributes.object_status = "<font color='Red'>Incomplete</font>"
      if os.path.exists(training_output):
        if os.path.getsize(training_output) > 0:
          model_with_attributes.object_status = "<font color='Green'>Ready</font>"

      models_with_attributes_list.append(model_with_attributes)

    return {
      'context': {
        'models': models_with_attributes_list,
        'message': message
      }
    }

  def post(self, request, *args, **kwargs):
    message = kwargs.get('message')
    orig_name = ""
    username = request.user.username
    if message == "":
      models_list = QSARModel.objects.select_related().filter(model_owner_id=request.user.id)
      for qsar_model in models_list:
        try:
          if request.FILES['predict_file_' + str(qsar_model.id)]:
            orig_name = str(request.FILES['predict_file_' + str(qsar_model.id)].name)
        except:
          continue

        name, ext = fileName, fileExtension = os.path.splitext(orig_name)
        new_name = name + "-predicted" + ext
        u = User.objects.get(username=request.user.username)
        # job_owner_id=jobs.getNewJobOwnerIndex(u)
        # job_folder = charmming_config.user_home + '/' + username
        NewJob = QSARJob()
        qsar_folder = charmming_config.user_home + "/" + username + "/qsar"
        qsar_jobs_folder = qsar_folder + "/jobs"
        job_folder = qsar_folder + "/jobs/qsar_job_" + str(NewJob.getNewJobOwnerIndex(u))
        os.system("mkdir %s" % (qsar_folder))
        os.system("mkdir %s" % (qsar_jobs_folder))
        os.system("mkdir %s" % (job_folder))
        os.system("chmod -R g+w %s" % (job_folder))
        predict_output_file = job_folder + "/predict_output"
        predict_results_file = job_folder + "/" + new_name  # "/predict_results.sdf"
        # predict_input_file=request.POST['predict_file_' + str(qsar_model.id)]
        predict_input_file = open("/%s/predict_input.sdf" % (job_folder), 'w')
        for fchunk in request.FILES['predict_file_' + str(qsar_model.id)].chunks():
          predict_input_file.write(fchunk)
        predict_input_file.close()
        predict_input_file = "/%s/predict_input.sdf" % (job_folder)
        if not is_valid_sd(predict_input_file):
          return {
            'error': {
              'message': 'No structures found in prediction file. Please upload an SD file with at least one structure.'
            }
          }

        # os.system("chmod g+rw %s" % (predict_file))
        os.system("chmod g+rw %s" % (predict_results_file))
        os.system("chmod g+rw %s" % (predict_output_file))
        predict_submitscript = open(job_folder + "/predict_submitscript.inp", 'w')
        predict_submitscript.write("#! /bin/bash\n")
        predict_submitscript.write("cd %s\n" % (job_folder))
        predict_submitscript.write("export PYTHONPATH=$PYTHONPATH:%s/\n" % ("/var/www/charmming"))  # job_folder))
        predict_submitscript.write("export DJANGO_SETTINGS_MODULE=settings\n")
        # threshold=GetObjectAttributeValue(request.user.id, qsar_model.id, "qsar_qsar_models", "Recommended threshold")
        # activity_property=GetObjectAttributeValue(request.user.id, qsar_model.id, "qsar_qsar_models", "Activity Property")
        # active=GetObjectAttributeValue(request.user.id, qsar_model.id, "qsar_qsar_models", "Active")
        # inactive=GetObjectAttributeValue(request.user.id, qsar_model.id, "qsar_qsar_models", "Inactive")
        # subprocess.call(["python","/var/www/charmming/qsar/run_model.py",saved_model,str(name),str(threshold),active,inactive,activity_property,str(out),output_txt])
        subtype = qsar_model.model_type.type
        categorization = qsar_model.model_type.is_categorical
        if categorization:
          threshold = GetObjectAttributeValue(request.user.id, qsar_model.id, "qsar_qsar_models", "Recommended threshold")
          activity_property = GetObjectAttributeValue(request.user.id, qsar_model.id, "qsar_qsar_models", "Activity Property")
          active = GetObjectAttributeValue(request.user.id, qsar_model.id, "qsar_qsar_models", "Active")
          inactive = GetObjectAttributeValue(request.user.id, qsar_model.id, "qsar_qsar_models", "Inactive")

          predict_submitscript.write("python /var/www/charmming/qsar/run_model.py %s %s %s %s %s %s %s %s %s\n" % (qsar_model.model_file, predict_input_file, threshold, active, inactive, activity_property, predict_results_file, predict_output_file, subtype))
        else:
          activity_property = GetObjectAttributeValue(request.user.id, qsar_model.id, "qsar_qsar_models", "Activity Property")
          # AssignObjectAttribute(user_id, model_id, "qsar_qsar_models", "Self R2", str(self_r2))
          # AssignObjectAttribute(user_id, model_id, "qsar_qsar_models", "Y-randomization", str(r2_rand))
          # AssignObjectAttribute(user_id, model_id, "qsar_qsar_models", "5-fold cross-validation", str(cross_r2))

          predict_submitscript.write("python /var/www/charmming/qsar/run_model_regression.py %s %s %s %s %s\n" % (qsar_model.model_file, predict_input_file, activity_property, predict_results_file, predict_output_file))

        predict_submitscript.write("echo 'NORMAL TERMINATION'\n")
        predict_submitscript.close()

        scriptlist = []
        scriptlist.append(job_folder + "/predict_submitscript.inp")
        exedict = {job_folder + "/predict_submitscript.inp": 'sh'}
        si = schedInterface()
        newSchedulerJobID = si.submitJob(request.user.id, job_folder, scriptlist, exedict)

        NewJob.job_owner = request.user
        NewJob.job_scheduler_id = newSchedulerJobID
        NewJob.job_owner_index = NewJob.getNewJobOwnerIndex(u)
        NewJob.description = "Based on " + qsar_model.model_name  # "QSAR Prediction Job"
        try:
          jobtype = QSARJobType.objects.get(job_type_name='Predict')
        except:
          jobtype = QSARJobType()
          jobtype.job_type_name = "Predict"
          jobtype.save()

        NewJob.job_type = jobtype
        NewJob.save()

        return {
          'redirect': '/qsar/viewjobs'
        }
      if orig_name == "":
        return {
          'error': {
            'message': 'No prediction file specified. Please upload an SD file with at least one structure'
          }
        }
