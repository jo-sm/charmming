from charmming.views import BaseView
from charmming.models import DDProteinConformation
from charmming.utils.dd_target import objProteinConformation

class DDAvailableProjectConformationsView(BaseView):
  template_name = 'html/ddavailableconformations.html'

  def get(self, request, *args, **kwargs):
    protein_id = kwargs.get('protein_id')

    if protein_id != '0' and protein_id != '':
      rows = DDProteinConformation.objects.filter(
        protein__id=protein_id,
        protein__owner__id=request.user.id,
        owner__id=request.user.id
      ).values('id', 'conformation_name', 'protein__pdb_code')

      conformations = [ objProteinConformation(
        conformation_id=row.id,
        conformation_name=row.conformation_name,
        protein_pdb_code=row.protein__pdb_code
      ) for row in rows]

    return {
      'context': {
        'available_conformations': conformations,
      }
    }
