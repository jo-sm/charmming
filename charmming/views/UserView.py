from charmming.views import BaseView

class UserView(BaseView):
  def get(self, request, *args, **kwargs):
    user = request.session.user

    return {
      'context': {
        'user': user
      }
    }
