from django.db.models import Count

from charmming.views import BaseView
from charmming.models import DDLigandSetsLigands
from charmming.utils.dd_substrate import LigandSetObj

class DDLigandSetsView(BaseView):
  template_name = 'html/ddviewligandsets.html'

  def get(self, request, *args, **kwargs):
    rows = DDLigandSetsLigands.objects.filter(owner__id=request.user.id).exclude(
      ligands_set__ligand_set_name='All Available'
    ).annotate(ligandcount=Count('ligands__id', distinct=True)).values(
      'ligands_set__id',
      'ligands_set__ligand_set_name',
      'ligands_set__description',
      'ligandcount'
    )

    sets = [LigandSetObj(
      id=row.ligand_sets__id,
      name=row.ligand_sets__ligand_set_name,
      description=row.ligand_sets__description,
      grid_count=row.ligandcount
    ) for row in rows]

    return {
      'context': {
        'ligandsets': sets
      }
    }
