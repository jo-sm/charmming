from charmming import charmming_config
from charmming.views import BaseView
from charmming.helpers.views import Accountinator
from charmming.models import WorkingStructure, Task, MutateTask
from charmming.utils.mutation import mutate_task_process

from pychm3.const.bio import aaAlphabet

import re
import os
import pickle
import json
import copy

class StructureMutationView(BaseView):
  template_name = 'html/mutationselect.html'

  @Accountinator(cg_fail="mutation")
  def get(self, request, *args, **kwargs):
    """
    Loads the mutation form.
    """
    # NOTE: The following kwarg comes in from Accountinator
    struct = kwargs.get("struct")
    ws = kwargs.get("ws")

    if ws.isBuilt != "t":
      return {
        'error': {
          'message': 'Please perform a calculation on this structure before performing a mutation.'
        },
        'redirect': '/energy'
      }

    testname = struct.name
    proposedname = testname
    # the below just exists to get a name, the query should not be significantly delaying otherwise
    # we can probably optimize Accountinator to return ALL workstructs and save a query, but this seems a little excessive.
    existingWorkStructs = WorkingStructure.objects.filter(structure=struct)
    if existingWorkStructs:
      usedNameList = [ews.identifier for ews in existingWorkStructs]

      while proposedname in usedNameList:
        m = re.search("([^-\s]+)-mt([0-9]+)$", proposedname)
        if m:
          numbah = int(m.group(2))
          proposedname = testname + "-mt" + str(numbah + 1)
        else:
          proposedname += "-mt1"
    else:
      # No longer necessary because the WS check in Accountinator takes it in, but leaving it in in case the database gets messed with somehow.
      return {
        'error': {
          'message': 'Please build a working structure before performing a mutation.'
        },
        'redirect': '/buildstruct'
      }

    segments = ws.segments.all()
    proCheck = False
    for segment in segments:
      if "good" in segment.name:
        return {
          'error': {
            'message': 'GOOD hets are not currently supported by the mutation procedure. Please build a working structure with only PRO-type segments.'
          },
          'redirect': '/buildstruct'
        }
      if "pro" in segment.name:
        proCheck = True
    # If there's no "pro" segment, we shouldn't be mutating at all.
    if not proCheck:
      return {
        'error': {
          'message': 'Your molecule does not have "PRO" segments. CHARMMing only support mutating on "PRO" segments at this time.'
        },
        'redirect': '/buildstruct'
      }
    # Do BADhets cause issues? Do we even know why this fails for GOODhets in the first place?
    # This way we need less JS black magic
    tdict = {}
    tdict['proposedname'] = proposedname
    tdict['structname'] = struct.name
    filepath = struct.location.replace(charmming_config.user_home, '') + '/'
    filename = ws.identifier  # This makes it so we can change the coordinates on-the-fly
    # Do we need the model? This seems like something we might actually want...
    amino_list = sorted(list(set(aaAlphabet.keys())))
    tdict['amino_list'] = amino_list
    segnames = []
    seglist = []
    if ws.localpickle:
      pdb = open(ws.localpickle)
    else:
      pdb = open(struct.pickle)
    # This way you can do multi-level mutations without issue
    pdbfile = pickle.load(pdb)
    taco = next(pdbfile.iter_models())  # we don't care about which model we load, so long as we load one
    pdb.close()
    tasks = Task.objects.filter(workstruct=ws, status='C', active='y', modifies_coordinates=True).exclude(action="solvation")  # Things that were solvated on, you run at your own risk
    isMutated = False
    for task in tasks:
      if task.action == 'mutation':
        isMutated = True
        break
    tdict['filename'] = filename
    tdict['isMutated'] = isMutated
    # This way it gets replaced if there's mutation, or otherwise it'll just stay the same
    try:
      segments = ws.segments.all()
    except Exception as ex:
      return {
        'error': {
          'message': "{}".format(ex)
        }
      }
    segmentnames = [x.name for x in segments]
    # Pickle loading! Time to make this code even slower. We will use model00 since I honestly don't think the connectivity/segmentation of the molecule itself changes
    # with minimization and the like, it just changes conformation, and we don't need coords to do mutation (we can fetch those from elsewhere)
    # segnames holds the segment names, seglist holds a list of dictionaries matching
    # the amino acids in each segment to their resIDs.
    # However we need to make SURE that only the segments from the current ws are loaded, or things can get quite bad.
    segnames = []  # We leave this in or otherwise the webpage gets the indices wrong
    chain_terminators = []  # Stores when each chain terminates (atom number); this way we don't have problems
    for seg in taco.iter_seg():
      if seg.segid in segmentnames and "pro" in seg.segid:  # Careful...all proteins or non-purely-hetatm segments MUST have PRO in them!
        segdict = dict(list(zip(amino_list, [ [] for i in range(len(amino_list)) ])))
        atom = seg.iter_res().next().iter_atom().next().atomNum0  # Gets the (original) atom number to figure out the chain termination
        chain_terminators.append(atom)
        for res in seg.iter_res():
          try:
            segdict[res.resName].append(res.resid)
          except KeyError:
            pass
        segdict = dict(( (k, v) for k, v in segdict.items() if v ))  # Generator function
        seglist.append(segdict)
        segnames.append(seg.segid)
    dictlist = json.dumps(seglist)  # Holds JSON strings of each dictionary so that JS can use them for shiny stuff
    tdict['segnames'] = segnames
    tdict['dictlist'] = dictlist
    tdict['chain_terminators'] = json.dumps(chain_terminators)
    tdict['tasks'] = tasks
    tdict['ws_identifier'] = ws.identifier
    tdict['filepath'] = filepath
    if (ws.isBuilt != 't'):
      return {
        'error': {
          'message': 'Please perform a calculation on the whole atom set before performing a mutation.'
        },
        'redirect': '/energy'
      }

    return {
      'context': tdict
    }

  @Accountinator()
  def post(self, request, *args, **kwargs):
    """
    Processes the result of the Mutation form. NOTE: This is NOT the display!
    """
    # NOTE: This kwarg comes in from Accountinator
    ws = kwargs.get("ws")
    postdata = request.POST
    try:
      os.stat(ws.structure.location + "/" + postdata['MutFile'] + "-mutation.pdb")

      # This is unlikely to happen. I dont' want to restructure the whole page because of this one error,
      # an error which would only happen if the user REFUSES to use our automatic generator and picks a name that already exists, on their own.
      return {
        'error': {
          'message': 'There already exists a mutation under that name. Please delete this mutation file or choose a different name.'
        },
        'redirect': '/mutation'
      }
    except:  # Basically if the file DOESN'T exist, keep going
      pass

    MutResi = postdata['MutResi']
    MutResName = postdata['MutResName']  # The script doesn't actually care about this one, it's just for user reference
    NewResn = postdata['NewResn']
    MutSegi = postdata['MutSegi']
    MutFile = postdata['MutFile']

    if len(MutFile) > 20:
      return {
        'error': {
          'message': 'Working structure identifier is too long. Please make sure your identifier is under 20 characters.'
        },
        'redirect': '/mutation'
      }

    modstruct = WorkingStructure()
    modstruct.structure = ws.structure
    modstruct.doblncharge = copy.deepcopy(ws.doblncharge)  # This may be a primitive but I don't care, let's be safe
    modstruct.isBuilt = copy.deepcopy(ws.isBuilt)
    modstruct.modelName = copy.deepcopy(ws.modelName)
    modstruct.qmRegion = copy.deepcopy(ws.qmRegion)  # What is this?
    modstruct.finalTopology = copy.deepcopy(ws.finalTopology)
    modstruct.finalParameter = copy.deepcopy(ws.finalParameter)
    modstruct.topparStream = copy.deepcopy(ws.topparStream)
    if not modstruct.topparStream:
      modstruct.topparStream = '%s/toppar/toppar_water_ions.str' % charmming_config.data_home
    else:
      modstruct.topparStream += '%s/toppar/toppar_water_ions.str' % charmming_config.data_home
    modstruct.localpickle = "%s/%s-mutation.dat" % (ws.structure.location, MutFile)
    modstruct.lesson = None  # No association - I don't think we should keep track of this unless there's a lesson for mutation?
    modstruct.extraStreams = copy.deepcopy(ws.extraStreams)
    modstruct.identifier = MutFile  # Can we do this? I need to do it to make sure the filename percolates up correctly
    modstruct.save()

    # horrific hack alert --
    # Copy all of the working segment identifiers from the parent workingstructure into this one. The reason that
    # this works is because the mutate task does this actually mutation AND builds a new PSF, so the PSFs of the
    # working segments are never again used in a CHARMM script. This is definitely not a clean way of doing things,
    # but it ought to work...
    for seg in ws.segments.all():
      modstruct.segments.add(seg)

    modstruct.save()

    ws.selected = 'n'
    ws.save()
    modstruct.selected = 'y'
    modstruct.save()

    if modstruct.localpickle:
      woof = modstruct.localpickle
    else:
      woof = modstruct.pickle # No specific pickle file, possibly not mutated before

    mt = MutateTask()
    mt.setup(modstruct)
    mt.lpickle = woof
    mt.MutResi = MutResi
    mt.MutResName = MutResName
    mt.NewResn = NewResn
    mt.MutSegi = MutSegi
    mt.MutFile = MutFile + "-mutation"  # THis way all other tasks don't break...
    mt.active = 'y'
    mt.action = 'mutation'  # I assume these are inherited from Task...
    mt.parent = None  # Important; mutate Tasks have no parent; equivalent to build Tasks
    mt.save()
    # Since there's no parent task we can skip over it...
    return mutate_task_process(request, mt, ws)  # PDB is passed so we can work with it...
