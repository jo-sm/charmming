from charmming.views import BaseView
from charmming.models import Structure

class StructurePDBView(BaseView):
  def get(self, request, *args, **kwargs):
    user = request.session.user
    structure_id = kwargs.get('id')
    structures = Structure.objects.filter(id=structure_id, owner=user)

    if len(structures) == 0:
      return {
        'error': {
          'message': 'Structure not found.'
        }
      }

    if len(structures) > 1:
      return {
        'error': {
          'message': 'An error occurred. Try again.'
        }
      }

    structure = structures[0]
    segments = structure.segment_set.all()

    pdb_texts = [{
      'segment': segment.name,
      'pdb': structure.get_file(segment=segment)
    } for segment in segments]

    pdb_text = structure.get_file(structure=True)

    return {
      'context': {
        'pdbs': pdb_texts,
        'pdb': pdb_text
      }
    }
