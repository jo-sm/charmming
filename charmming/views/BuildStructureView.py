from charmming import charmming_config
from charmming.views import BaseView
from charmming.models import Structure, WorkingStructure, Segment
from charmming.utils.structure.propka import calculate_propka_residues

import re
import os
import pickle

class BuildStructureView(BaseView):
  template_name = 'html/buildstruct.html'

  def get(self, request, *args, **kwargs):
    try:
      struct = Structure.objects.filter(owner=request.user, selected='y')[0]
    except:
      return {
        'error': {
          'Please submit a structure first.'
        },
        'redirect': '/fileupload'
      }

    # pick out a proposed name for this working structure
    proposedname = struct.name
    existingWorkStructs = WorkingStructure.objects.filter(structure=struct)

    if existingWorkStructs:
      usedNameList = [ews.identifier for ews in existingWorkStructs]

      while proposedname in usedNameList:
        m = re.search("([^\s]+)-ws([0-9]+)$", proposedname)
        if m:
          basename = m.group(1)
          numbah = int(m.group(2))
          proposedname = basename + "-ws" + str(numbah + 1)
        else:
          proposedname += "-ws1"

    tdict = {}
    tdict['proposedname'] = proposedname
    ws = []
    if len(existingWorkStructs) > 0:
      tdict['haveworkingstruct'] = True
      ws.extend(existingWorkStructs)
      ws.sort()
    else:
      tdict['haveworkingstruct'] = False
    tdict['built_list'] = ws

    # get list of all of the models that we can use
    # st_models = []
    fp = open(struct.pickle, 'rb')
    pdb = pickle.load(fp)
    fp.close()
    tdict['model_list'] = list(pdb.keys())
    sl = []
    sl.extend(Segment.objects.filter(structure=struct, is_working='n'))
    sl = sorted(sl, key=lambda x: x.name)
    customCount = 0  # Checks each seg for "is_custom", and if customCount > 1, make a fuss
    for seg in sl:
      if seg.is_custom:
        customCount += 1
    # By having the database field resName in each Segment object we can save a lot of grief
    # Somehow this doesn't work that well in normalmodes...
    tdict['customCount'] = customCount
    tdict['seg_list'] = sl
    tdict['disulfide_list'] = struct.getDisulfideList()
    tdict['proto_list'] = []
    tdict['super_user'] = request.user.is_superuser
    try:
      tdict['filepath'] = struct.location.replace(charmming_config.user_home, "/pdbuploads/") + "/" + struct.original_name + ".pdb"  # This assumes we have a PDB file! Please be careful with this.
    except:
      tdict['filepath'] = struct.location.replace(charmming_config.user_home, "/pdbuploads/") + "/" + struct.name + ".pdb"  # This assumes we have a PDB file! Please be careful with this.
    try:
      os.stat(struct.location + "/" + struct.original_name + "-propka.pka")
      try:
        user_ph = float(request.POST["user_ph"])
      except:
        user_ph = 7
      propka_residues, user_decision = calculate_propka_residues(struct, user_ph)
    except:
      propka_residues = None
      user_decision = True
    for seg in sl:
      if seg.type == "pro":
        tdict['proto_list'].extend(seg.getProtonizableResidues(pickleFile=pdb, propka_residues=propka_residues))
    tdict['user_decision'] = user_decision  # we need this to indicate to the user whether they need to make a decision
    tdict['structname'] = struct.name
    tdict['no_propka'] = True if propka_residues is not None else False
    return {
      'context': tdict
    }
