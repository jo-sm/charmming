from charmming.views import BaseView
from charmming.models import Structure, WorkingStructure, CGWorkingStructure, DDBindingSite, DDFile, DDFileObject, DDBindingSiteWorkingStructure

import re

class DDBindingFormView(BaseView):
  template_name = 'html/ddbindingsite.html'

  def get(self, request, *args, **kwargs):
    try:
      struct = Structure.objects.filter(owner=request.user, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'Please submit a structure first.'
        },
        'redirect': '/fileupload'
      }
    try:
      ws = WorkingStructure.objects.filter(structure=struct, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'Please build a working structure before performing any calculations.'
        },
        'redirect': '/buildstruct'
      }

    if ws.isBuilt != 't':
      return {
        'error': {
          'message': 'Please perform a calculation on your working structure before defining a new binding site.'
        },
        'redirect': '/energy'
      }

    try:
      CGWorkingStructure.objects.get(workingstructure_ptr=ws.id)
      return {
        'error': {
          'message': 'You cannot define docking sites on coarse-grained structures. Please build a non-coarse-grained working structure first.'
        },
        'redirect': '/buildstruct'
      }
    except:
      # if there's more than one this could get problematic, too, so we just kick them out and have some other part of the site deal with it
      # either way I think we shouldn't allow these?
      pass

    try:
      found_pro = False
      for seg in ws.segments.all():  # this might fail if you have no segments...not sure though, QuerySets don't except when they're empty.
        if seg.type == "pro":
          found_pro = True
          break
      if not found_pro:
        return {
          'error': {
            'message': 'You cannot create binding sites for a structure that has no "pro"-type segments. Please build a working structure with "pro"-type segments first.'
          },
          'redirect': '/buildstruct'
        }
    except:
      return {
        'error': {
          'message': 'Your working structure has either no segments, or a corrupted segment list. Please rebuild your working structure.'
        },
        'redirect': '/buildstruct'
      }

    # this should not be made into a task, since it is just saving a DB object based on certain properties of the structure
    # we might need to do some sgment parsing for the chain terminators, but for now let's just render the page
    tdict = {}
    tdict['filepath'] = "%s/%s/%s-build.pdb" % (request.user.username, struct.name, ws.identifier)  # TODO: THIS IS TEMPORARY; REPLACE IT

    return {
      'context': tdict
    }

  def post(self, request, *args, **kwargs):
    if len(request.POST['binding_name']) > 120:
      return {
        'error': {
          'message': 'Binding site names are limited to 120 characters.'
        }
      }

    # first get the binding sites from the request
    res_select = request.POST['residue_selection']
    test_numeric = re.match("^([0-9]*( ){0,1})+$", res_select)
    if not(test_numeric):  # whoops, letters in the selection...
      return {
        'error': {
          'message': 'You cannot use non-numeric characters (other than spaces) for defining your binding site selection. Please use numeric residue IDs.'
        }
      }
    residues = res_select.split()  # space-separated
    res_print = "\n".join(residues)  # residues are strings so it's ok
    # TODO: Take all this boilerplate code out somewhere so we always have these vars...
    try:
      struct = Structure.objects.filter(owner=request.user, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'Please submit a structure first.'
        },
        'redirect': '/fileupload'
      }
    try:
      ws = WorkingStructure.objects.filter(structure=struct, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'Please build a working structure before performing any calculations.'
        },
        'redirect': '/buildstruct'
      }

    bs_filename = "%s/%s-%s.bs" % (struct.location, ws.identifier, request.POST['binding_name'])
    bs_file = open(bs_filename, "w")
    bs_file.write(res_print)
    bs_file.close()

    bs_data = DDBindingSite()  # this one holds actual data - name and description
    bs_data.binding_site_name = request.POST['binding_name']
    bs_data.description = "User uploaded binding site %s" % request.POST['binding_name']  # TODO: Replace this with a proper desc field
    bs_data.save()

    bs_filedata = DDFile()  # this one holds the file itself
    bs_filedata.owner = request.user
    bs_filedata.file_name = bs_filename.split("/")[-1]
    bs_filedata.file_location = "".join(bs_filename.split("/")[:-1])
    bs_filedata.description = "User uploaded binding site %s" % bs_data.description  # hopefully django's object does the job here
    bs_filedata.save()

    bs_fileobj = DDFileObject()  # this one associates file with other DD stuff
    bs_fileobj.owner = request.user
    bs_fileobj.file = bs_filedata
    bs_fileobj.object_table_name = "binding_sites"
    bs_fileobj.object_id = bs_data.id
    bs_fileobj.save()

    bs_associate = DDBindingSiteWorkingStructure()  # this one links bs file with ws to avoid
    # display of extraneous bs on the docking page
    bs_associate.binding_site = bs_data
    bs_associate.workstruct = ws
    bs_associate.save()

    return {
      'redirect': '/dd_infrastructure/dsfform'
    }
