from charmming.views import BaseView
from charmming.utils. dd_infrastructure import objDDJob
from charmming.models import DDJob

from datetime import datetime

class DDJobInfoView(BaseView):
  template_name = 'html/ddviewjobinfo.html'

  def get(self, request, *args, **kwargs):
    job_id = kwargs.get('job_id')

    rows = []
    rows = DDJob.objects.filter(
      job_scheduler_id__userid=request.user.id,
      job_scheduler_id__id=job_id.replace('/', '')
    ).values(
      'id',
      'job_scheduler_id',
      'job_owner_index',
      'description',
      'job_scheduler_id__state',
      'job_scheduler_id__queued',
      'job_scheduler_id__ended',
      'job_scheduler_id__started'
    )

    if len(rows) == 0:
      return {
        'error': {
          'message': 'No job found.'
        }
      }

    row = rows[0]

    if row.job_scheduler_id__state == 1:
      status = 'Queued'
    elif row.job_scheduler_id__state == 2:
      status = 'Running'
    elif row.job_scheduler_id__state == 3:
      status = 'Done'
    elif row.job_scheduler_id__state == 4:
      status = 'Failed'

    if status == 'Queued':
      waited_time = (datetime.now() - row.job_scheduler_id__queued).total_seconds()
    else:
      waited_time = (row.job_scheduler_id__ended - row.job_scheduler_id__queued).total_seconds()

    if status == 'Running':
      runtime_time = (datetime.now() - row.job_scheduler_id__started).total_seconds()
      total_time = (datetime.now() - row.job_scheduler_id__queued).total_seconds
    else:
      runtime_time = (row.job_scheduler_id__ended - row.job_scheduler_id__started).total_seconds()
      total_time = (row.job_scheduler_id__ended - row.job_scheduler_id__queued).total_seconds()

    job = objDDJob()
    job.id = row.id
    job.scheduler_id = row.job_scheduler_id
    job.owner_index = row.job_owner_index
    job.description = row.description
    job.status = status
    job.queued = row.job_scheduler_id__queued
    job.waited = waited_time
    job.runtime = runtime_time
    job.totaltime = total_time

    results = 0

    return {
      'context': {
        'job': job,
        'results': results
      }
    }
