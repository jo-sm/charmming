from django.http import HttpResponse

from charmming import charmming_config
from charmming.views import BaseView

import mimetypes

class DownloadLessonFilesView(BaseView):
  def get(self, request, lessonfolder, filename, mimetype=None, *args, **kwargs):
    fnarr = filename.split("/")
    filename = fnarr[-1]

    path = "%s/mytemplates/lessons/%s" % (charmming_config.charmming_root, lessonfolder)
    if mimetype is None:
      mimetype, encoding = mimetypes.guess_type(path + "/" + filename)
    response = HttpResponse(mimetype=mimetype)
    response['Content-Disposition'] = 'attachment; filename=%s' % filename
    response.write(open(path + "/" + filename, "rb").read())

    return response
