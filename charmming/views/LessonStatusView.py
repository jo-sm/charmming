from django.http import HttpResponse

from charmming.views import BaseView
from charmming.models import Structure, LessonProblem
from charmming.utils.lesson import doLessonAct

class LessonStatusView(BaseView):
  template_name = 'html/lesson1report.html'

  def get(self, request, *args, **kwargs):
    try:
      file = Structure.objects.filter(owner=request.user, selected='y')[0]
    except:
      return HttpResponse('')

    try:
      lesson_obj = doLessonAct(file, "getObject")
    except:
      # TODO: Refactor code depending on this
      # The lesson status code on the front end depends on this returning ''
      # if no lesson is happening. Ideally, it can just return something with
      # no context and be fine.
      return HttpResponse('')

    try:
      lessonprob = LessonProblem.objects.get(lesson_type=file.lesson_type, lesson_id=file.lesson_id)
    except:
      lessonprob = None

    step_status_list = lesson_obj.generateStatusHtml(file)

    # Gets a list of numbers that corresponds to whether a lesson step was successfully done, failed,
    # or has not been performed yet

    return {
      'context': {
        'file': file,
        'lesson': lesson_obj,
        'lessonprob': lessonprob,
        'status_list': step_status_list
      }
    }
