from charmming.views import BaseView
from django.http import HttpResponse

import mimetypes
import os

class QSARDownloadView(BaseView):
  def post(self, request, *args, **kwargs):
    if 'download' in request.POST:
      filename = request.POST['output']
      orig_name = request.POST['orig_name']
      name, ext = fileName, fileExtension = os.path.splitext(orig_name)
      new_name = name + "-predicted" + ext
      mimetype, encoding = mimetypes.guess_type(filename)
      response = HttpResponse(mimetype=mimetype)
      response['Content-Disposition'] = 'attachment; filename=%s' % new_name
      response.write(open(filename, "rb").read())

      return response
