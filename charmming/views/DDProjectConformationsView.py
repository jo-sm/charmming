from django.db.models import F

from charmming.views import BaseView
from charmming.models import DDProjectsProteinConformations, DDProteinConformation
from charmming.utils.dd_infrastructure import objProjectProteinConformation

class DDProjectConformationsView(BaseView):
  template_name = 'html/ddprojectconformations.html'

  def get(self, request, *args, **kwargs):
    project_id = kwargs.get('project_id')
    addedids = kwargs.get('addedids')
    removedids = kwargs.get('removedids')

    if (project_id != '0') and (project_id != ''):
      if (addedids != "") and (addedids != '0'):
        addedids_list = [int(n) for n in addedids.split(',')]
        added_conformations = DDProteinConformation.objects.filter(id__in=addedids_list)

        for conformation in added_conformations:
          project_conformation = DDProjectsProteinConformations(owner_id=request.user.id, project_id=project_id, protein_conformation_id=conformation.id)
          project_conformation.save()

      if (removedids != "") and (removedids != '0'):
        removedids_list = [int(n) for n in removedids.split(',')]
        DDProjectsProteinConformations.objects.filter(id__in=removedids_list).delete()

      rows = DDProjectsProteinConformations.objects.filter(
        project__id=project_id,
        owner__id=request.user.id
      ).annotate(
        project_conformation_id=F('id'),
        conformation_id=F('protein_conformation__id'),
        conformation_name=F('protein_conformation__name'),
        protein_pdb_code=F('protein_conformation__protein__pdb_code')
      ).values('project_conformation_id', 'conformation_id', 'conformation_name', 'protein_pdb_code')

      conformations = [objProjectProteinConformation(
        conformation_id=row.conformation_id,
        project_conformation_id=row.project_conformation_id,
        conformation_name=row.conformation_name,
        protein_pdb_code=row.protein_pdb_code
      ) for row in rows]

    return {
      'context': {
        'conformations': conformations,
      }
    }
