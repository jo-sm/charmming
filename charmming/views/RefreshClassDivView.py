from django.contrib.auth.models import User

from charmming.views import BaseView
from charmming.models import TeacherProfile, Classroom

import re

class RefreshClassDivView(BaseView):
  template_name = 'html/classlistdiv.html'

  def get(self, request, *args, **kwargs):
    slash = re.compile('/')
    teacher_name = kwargs.get('teacher_name')
    teacher_name = slash.sub('', teacher_name)
    try:
      # Stores user object of the student's teacher
      students_teacher_user = User.objects.filter(username=teacher_name)[0]
      # Gets the teacher Profile of the teacher
      students_teacher = TeacherProfile.objects.filter(teacher=students_teacher_user)[0]
      class_list = Classroom.objects.filter(teacher=students_teacher)
      return {
        'context': {
          'class_list': class_list
        }
      }
    except:
      return {
        'error': {
          'message': 'Sorry, no such teacher exists. Name is case-sensitive.'
        }
      }
