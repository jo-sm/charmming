from charmming.views import BaseView
from charmming.forms import AssayForm
from charmming.utils.assays import get_dir
from charmming.utils.assays.rest import get_sd_file

class AssaysView(BaseView):
  template_name = 'assays/assays.html'

  def get(self, request, *args, **kwargs):
    query = request.GET.get('query')
    start = 0
    # total = 0
    selected = dict()
    form = AssayForm(query=query, start=start, selected=selected)
    if form.len_aids > 0:
      return {
        'context': {
          'form': form
        }
      }
    else:
      return {
        'redirect': '/assays/search'
      }

  def post(self, request, *args, **kwargs):
    selected = dict()
    aids = []
    if 'start_form' in request.POST:
      start = int(request.POST['start_form'])
    if 'assays' in request.POST:
      aids = request.POST.getlist('assays')
    if 'selected' in request.POST:
      selected_packed = request.POST['selected']
      for x in selected_packed.split("|"):
        p = x.split(":", 1)
        if len(p) == 2:
          selected[p[0]] = p[1].split(",")
    selected[str(start)] = aids
    if 'step' in request.POST:
      step = int(request.POST['step'])
    # if 'query' in request.POST:
      # query = request.POST['query']

    if 'back' in request.POST:
      return {
        'redirect': '/assays/search'
      }
    elif 'next' in request.POST:
      start += step
    elif 'prev' in request.POST:
      start -= step
    elif 'filename_request' in request.POST:
      work_dir = get_dir(request)
      filename_request = request.POST['filename_request']
      filename = work_dir + filename_request
      remove_inconclusive = False
      if 'remove' in request.POST:
        remove_inconclusive = True
      all_aids = []
      for s in selected:
        all_aids += selected[s]
      num_records = get_sd_file(all_aids, filename, remove_inconclusive)
      touch = open(filename + ".done", "w")
      touch.write("%d" % num_records)
      touch.close()

      return {
        'statuses': {
          'success': 'Searched'
        }
      }
