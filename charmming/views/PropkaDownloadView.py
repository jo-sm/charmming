from django.http import HttpResponse

from charmming.views import BaseView
from charmming.models import Structure, WorkingStructure

import mimetypes
import os

class PropkaDownloadView(BaseView):
  def get(self, request, filename, *args, **kwargs):
    try:
      struct = Structure.objects.filter(owner=request.user, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'Please upload a structure first.'
        },
        'redirect': '/fileupload'
      }
    try:
      WorkingStructure.objects.filter(structure=struct, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'A working structure needs to be uploaded before PROPKA analysis can be performed on your system.'
        },
        'redirect': '/buildstruct'
      }

    mimetype, encoding = mimetypes.guess_type("%s/%s" % (struct.location, filename))

    try:
      sval = os.stat("%s/%s" % (struct.location, filename))
    except:
      return {
        'error': {
          'message': "The file {} does not appear to exist anymore.".format(filename)
        },
        'redirect': '/fileupload'
      }

    response = HttpResponse(mimetype=mimetype)
    response['Content-Disposition'] = 'attachment; filename=%s' % filename
    response['Content-length'] = sval.st_size
    response.write(open("%s/%s" % (struct.location, filename), "rb").read())
    return response
