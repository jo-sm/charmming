from django.contrib.auth.models import User
from django.core.mail import send_mail

from charmming.views import BaseView

import random
import string

class ResetPasswordView(BaseView):
  http_method_names = [ 'get', 'post' ]
  template_name = 'html/resetpassword.html'
  no_auth = True

  def post(self, request):
    # email-given and user_id are the the variables the user has submitted
    email_given = request.POST['email']
    user_id = request.POST['username']
    password = ''

    # User should not be able to reset the password of the demo account
    if(user_id == 'demo'):
      return {
        'error': {
          'message': "Did you really think we wouldn't test for that?" # I leave this one as is - it's better that way. ~VS
        }
      }

    try:
      # Attempt to get the user_profile based on the user supplied info
      user_profile = User.objects.get(email__iexact=email_given, username=user_id)
    except:
      return {
        'error': {
          'message': 'Sorry, no user could be located with the given information.'
        },
      }

    if(user_profile.is_superuser or user_profile.is_staff):
      return {
        'error': {
          'message': 'Staff and Admins cannot get their password reset. Whoops!'
        }
      }

    chars = string.ascii_letters + string.digits

    for x in range(8):
      password += random.choice(chars)

    user_profile.set_password(password)
    user_profile.save()

    mail_message = "Your password for CHARMMing.org has been reset. Your new password is: " + password
    mail_message += ". You can change the password once you log in. If you did not "
    mail_message += "request this password reset or there are any other "
    mail_message += "problems please E-mail btamiller@gmail.com. Please do not reply to this E-mail.\n\n"
    mail_message += "Thanks for using CHARMMing,\n CHARMMing Development Team"

    send_mail('CHARMMing Password Reset', mail_message, 'PasswordReset@charmming.org', [user_profile.email], fail_silently=False)
    return {
      'messages': {
        'success': 'Password changed! You should receive an e-mail shortly.'
      }
    }
