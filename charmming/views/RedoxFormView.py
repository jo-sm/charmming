from charmming.views import BaseView
from charmming.models import RedoxTask
# from charmming.utils.task import deactivate_current_task
from charmming.utils.apbs import createFinalPDB, redox_tpl
from charmming.helpers.views import Accountinator

import pickle
import os

class RedoxFormView(BaseView):
  template_name = 'html/redox.html'

  @Accountinator(cg_fail="redox")
  def get(self, request, *args, **kwargs):
    # NOTE: The below two kwargs come in from Accountinator.
    struct = kwargs.get("struct")
    ws = kwargs.get("ws")
    tmpfp = open(struct.pickle, 'rb')
    pdb = pickle.load(tmpfp)
    tmpfp.close()

    # This code path is taken if the form IS NOT filled in. We need
    # to decide which REDOX sites are present in our working structure.

    # use charmminglib to decide if there is a valid FeS compound in
    # this structure.
    try:
      myMol = pdb['append_%s' % ws.identifier]
    except:
      try:
        myMol = pdb['model00']  # Some things only have model00
      except:
        return {
          'error': {
            'message': 'Your working structure must be built before you perform a redox calculation.'
          },
          'redirect': '/energy'
        }

    pfp = open(struct.pickle, 'rb')
    pdb = pickle.load(pfp)
    pfp.close()
    # ToDo: fix so that the user does not have to build their WorkingStructure
    # before they can use redox.
    redox_segs = []
    redox_nums = {}

    # myMol.write('/tmp/scotttmp.pdb',outformat='charmm')
    '''
        THIS CAUSES ERRORS WITH MUTATION
    '''
    if 'mt1' not in ws.identifier:
      het = list(myMol.iter_res(segtypes=['bad'], resName=['fs4', 'sf4']))
      for res in het:
        if res.chainid.upper() in redox_nums:
          redox_nums[res.chainid.upper()] += 1
        else:
          redox_segs.append(res.chainid.upper())
          redox_nums[res.chainid.upper()] = 1

    # if we have any segments that are marked as redox only, we need to take
    # them into consideration ... this is a bit of a hack.
    parsedMdl = pdb[0]
    for seg in ws.segments.all():
      if seg.redox:
        for res in parsedMdl.iter_res():
          if res.segid == seg.name:
            if res.chainid.upper() in redox_nums:
              redox_nums[res.chainid.upper()] += 1
            else:
              redox_segs.append(res.chainid.upper())
              redox_nums[res.chainid.upper()] = 1

    if len(redox_segs) < 1:
      noredox = True
    else:
      noredox = False

    # if 'mt1' in ws.identifier: noredox = False

    # Check and see if we have any results to print out
    print_result = False
    calc_final = True
    oxipot = 'N/A'
    oxipotref = 'N/A'
    modpot = 'N/A'
    modpotref = 'N/A'
    delg = "N/A"
    ade = "N/A"
    she = "N/A"
    delgnf = "N/A"
    finres = "N/A"

    try:
      os.stat(struct.location + '/redox-' + ws.identifier + '-modpot.txt')
    except OSError as oe:
      calc_final = False
    else:
      noredox = False
      print_result = True
      fp = open(struct.location + '/redox-' + ws.identifier + '-modpot.txt', 'r')
      try:
        modpot = float(fp.readline())
      except:
        print_result = False
      fp.close()
    try:
      os.stat(struct.location + '/redox-' + ws.identifier + '-modpotref.txt')
    except OSError as oe:
      calc_final = False
    else:
      print_result = True
      fp = open(struct.location + '/redox-' + ws.identifier + '-modpotref.txt', 'r')
      try:
        modpotref = float(fp.readline())
      except:
        print_result = False
      fp.close()
    try:
      os.stat(struct.location + '/redox-' + ws.identifier + '-oxipot.txt')
    except OSError as oe:
      calc_final = False
    else:
      print_result = True
      fp = open(struct.location + '/redox-' + ws.identifier + '-oxipot.txt', 'r')
      try:
        oxipot = float(fp.readline())
      except:
        print_result = False
      fp.close()
    try:
      os.stat(struct.location + '/redox-' + ws.identifier + '-oxipotref.txt')
    except:
      calc_final = False
    else:
      print_result = True
      fp = open(struct.location + '/redox-' + ws.identifier + '-oxipotref.txt', 'r')
      try:
        oxipotref = float(fp.readline())
      except:
        print_result = False
      fp.close()

    if calc_final:
      # figure out the correct ade value
      # rp = apbs.models.redoxTask.objects.filter(workstruct=ws, action='redox', active='y', finished='y')[0]
      ade = 0.0
      delg = 0.0
      she = 4.43

      delg = modpot - modpotref - oxipot + oxipotref
      ade = -0.232
      '''
      if rp.redoxsite == 'couple_oxi':
          delg = modpot - modpotref - oxipot + oxipotref
          ade = -0.232
      elif rp.redoxsite == 'couple_red':
          delg = modpot - modpotref - oxipot + oxipotref #oxipot - oxipotref - modpot + modpotref
          ade = 3.453
      '''
      delgnf = delg * (-4.184 / 96.485)
      finres = delgnf - she - ade

    # Django's template language doesn't handle ranges well, so this is a hack to display
    # the correct number of redox sites for each segment.
    rn = {}
    for k in list(redox_nums.keys()):
      rn[k] = list(range(1, redox_nums[k] + 1))

    return {
      'context': {
        'redox_segs': redox_segs,
        'noredox': noredox,
        'print_result': print_result,
        'oxipot': oxipot,
        'oxipotref': oxipotref,
        'modpot': modpot,
        'modpotref': modpotref,
        'delg': delg,
        'delgnf': delgnf,
        'ade': ade,
        'finres': finres,
        'she': she,
        'redox_nums': rn,
        'ws_identifier': ws.identifier
      }
    }

  @Accountinator(cg_fail="redox")
  def post(self, request, *args, **kwargs):
    # NOTE: The below two kwargs come in from Accountinator.
    struct = kwargs.get("struct")
    ws = kwargs.get("ws")
    tmpfp = open(struct.pickle, 'rb')
    pdb = pickle.load(tmpfp)
    pdb_metadata = pdb.get_metaData()
    tmpfp.close()

    '''
       SCOTT HACK: trying main pickle, b/c mutation pickle is missing link data
    '''
    try:
      tmpfp = open(ws.structure.pickle, 'rb')
      tmpdb = pickle.load(tmpfp)
      pdb_metadata = tmpdb.get_metaData()
      tmpfp.close()
    except:
      pass

    deactivate_current_task("RedoxTask", ws)
    rdxtsk = RedoxTask()
    rdxtsk.setup(ws)
    rdxtsk.action = 'redox'
    rdxtsk.active = 'y'
    rdxtsk.save()

    if ws.isBuilt != 't':
      return {
        'error': {
          'message': 'Your working structure must be built before you perform a redox calculation.'
        },
        'redirect': '/energy'
      }

    # isBuilt = True
    # try:
    #   pTaskID = Task.objects.get(workstruct=ws, action='build').id
    # except:
    #   pTaskID = 0.
    pdb = createFinalPDB(request, ws)
    return redox_tpl(request, rdxtsk, ws, pdb, pdb_metadata)
