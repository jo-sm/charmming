from charmming.views import BaseView
from charmming.helpers.views import Accountinator
from charmming.utils.structure import attach_seg_to_struct
from charmming.utils.structure.properties import is_res_in_structure, get_mol_from_struct, is_spin_mult_singlet, has_nucleotide_suffixes, residue_is_reserved
from charmming.utils.structure.structure_builder import create_structure, unselect_current_structure
from charmming.utils.ligand_design import clear_struct, get_structure_to_attach, check_if_ligand_in_pdb, create_ligand_file
from charmming.utils.getTopparFiles import check_if_residue_database_present

from pychm3.io.pdb import PDB

import os
import pickle

class LigandDesignView(BaseView):
  template_name = 'html/ligand_design.html'

  @Accountinator(auth_only=True)
  def post(self, request, *args, **kwargs):
    """
    Actually performs the ligand building portion by taking in POST data
    about the molecule from the Ligand Design page.
    On successful build, it will redirect to the Build Working Structure page.
    """

    post_data = request.POST
    tdict = {}
    segletter = "a"
    struct, filepath = clear_struct(request)

    if "attach_check" in post_data:
      # if you want to attach a ligand to a structure, make sure
      # there's a structure to attach to
      # put these here since they're related to the attach_check being present
      tdict['attach_check'] = True
      try:
        struct, filepath, segletter = get_structure_to_attach(request)
      except ValueError:
        return {
          'error': {
            'message': 'Too many segments for this structure.'
          },
          'redirect': '/ligand_design'
        }
      except Exception:
        pass  # pretty sure we don't need to re-assign struct and filepath
    else:
      tdict['attach_check'] = False

    os.chdir(filepath)
    molname = post_data['LigName']
    moldata = post_data['molinfo']
    tdict['moldata'] = moldata  # TODO: check if this is really necessary
    tdict['molname'] = molname
    tdict['mol_short'] = molname[0:4].upper()  # truncated since CHARMM only takes 4-char identifiers for residues
    tdict['filepath'] = '/charmming/uploads/' + request.user.username + "/"
    # from here on out "if struct" means "if attach_check is True and we have a structure to attach to"
    if struct:
      tdict['filepath'] = tdict['filepath'] + struct.name + "/"
    check_if_residue_database_present(tdict)  # TODO: make this part of installer maybe?
    resname = tdict["mol_short"]
    residue_description = residue_is_reserved(resname)
    if residue_description:
      return {
        'error': {
          'message': "The residue name {0}: {1} is a reserved word in CHARMM. Please choose another name for your molecule.".format(resname, residue_description)
        }
      }

    jmol_data = moldata[moldata.index("HETATM"):moldata.index("\\n\\n")].split("\\n")
    end_data = create_ligand_file(molname, resname, segletter, jmol_data)
    ligname = resname  # ??? why do I need this?
    tmpfile = open("moldata.pdb", "w")  # write so that the JSmol display can be accurate
    tmpfile.write(end_data)
    tmpfile.close()
    if has_nucleotide_suffixes(resname):
      return {
        'error': {
          'message': 'CHARMMing does not support ligand names containing nucleotide suffixes (THY, URA, GUA, ADE, CYT).'
        }
      }

    if not is_spin_mult_singlet(end_data) and post_data['force_charge'] == "false":
      tdict['mult_alert'] = True
      return {
        'context': tdict
      }
    tdict['mult_alert'] = False
    force_custom = post_data['force_custom'] == "true"
    # If set to false, we check whether the molecule exists in PDB.org,
    # and if it does, warn the user.
    # If set to true, the user has approved of the molecule, so we skip the if check.
    if not(force_custom):  # i.e. if the user has not said he is sure this structure is correct
      sdf_link = check_if_ligand_in_pdb(ligname)
      if sdf_link:
        tdict["sdf_link"] = sdf_link

        return {
          'context': tdict
        }
    if struct:
      struct_mol = get_mol_from_struct(struct)
      # first a sanity check so that the user doesn't build more than one residue with the same name
      res_in_struct = is_res_in_structure(struct_mol, ligname)
      if res_in_struct:
        tdict['same_residue'] = True

        return {
          'context': tdict
        }

    not_custom = post_data['not_custom'] == "true"  # Whether it exists on PDB.org
    ligpdb = PDB("moldata.pdb")  # we wrote this earlier
    ligmol = next(ligpdb.iter_models())
    for seg in ligmol.iter_seg():
      if seg.segType == "pro":  # If the builder caught it as GOOD/BAD, leave it, else change
        seg.segType = "good"  # If it became PRO, then we have params for it, so make it good
    if struct:
      # if the structure is present, attach our ligand do the structure
      struct_mol.extend(ligmol)
      # Now we need to save segments and dump the pickle.
      attach_seg_to_struct(struct_mol, struct, not_custom)
      # rewrite pickle file now that you've added in the segment.
      woof = open(struct.pickle, "w")
      pickle.dump(ligpdb, woof)  # we took the object, extended it, now we save
      woof.close()
      # we don't delete moldata.pdb for the sake of debugging. May change this later.
      struct.save()
    else:  # No structure, so we build one
      dname = resname.lower()  # resname.lower() because CHARMMing names are by convention lowercase
      # we don't let you make multipel ligands with the same name - that may be a bad choice later on.
      # make new pickle
      # attempt to create structure
      try:
        struct = create_structure(request.user, "Custom Ligand {}".format(resname), filepath, dname)
        # this contains an os.mkdir call so make sure we can catch any problems
        # put in the pickle after because we don't have the dir until after we create the structure
        pfname = os.path.join(filepath, dname, "pdbpickle.dat")
        picklefile = open(pfname, "wb")
        pickle.dump(ligpdb, picklefile)
        picklefile.close()
        struct.pickle = pfname
      except:
        tdict['struct_error'] = True
        return {
          'context': tdict
        }

      attach_seg_to_struct(ligmol, struct, not_custom)

      unselect_current_structure(request.user)
      struct.selected = "y"
      struct.save()  # save again to make sure selected status is updated

      return {
        'redirect': '/buildstruct'
      }
