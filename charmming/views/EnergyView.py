from charmming.views import JobBaseView
from charmming.models import Structure
from charmming.utils.job import submit

class EnergyView(JobBaseView):
  def post(self, request, *args, **kwargs):
    # Do some data sanity checks to ensure fake data hasn't been submitted
    data = request.POST
    structure = request.structure

    submitted_task = submit(structure, 'charmm', 'energy', data)

    if not submitted_task:
      return {
        'error': {
          'message': 'Could not submit Energy job.'
        }
      }
    else:
      return {
        'context': {
          'task': submitted_task.id
        }
      }

    # # Create the template
    # # Submit the job
    # ws = kwargs.get("ws")  # from Accountinator
    # # tdict = {}
    # deactivate_current_task("EnergyTask", ws)  # I'm not moving EnergyTask out of charmming.structure.models because we'd need a DB wipe.

    # et = EnergyTask()
    # et.setup(ws)
    # et.active = 'y'
    # et.action = 'energy'
    # et.save()

    # ptid = -1
    # try:
    #   pTask = getParent(ws, request, et)
    #   ptid = pTask.id
    # except NoNscaleFound as e:
    #   return output.returnSubmission('Energy', error='The nScale parameterization process has not yet completed. It may take 1-2 hours.')
    # except:
    #   # PROBABLY what happened was that build failed somehow. Make note of this - better to have an error message that is right 90% of the time,
    #   # than no error message at all.
    #   return {
    #     'error': {
    #       'message': 'There was an error building your structure. Please check over your coordinate build files. If the problem persists, contact the CHARMMing system administrator.'
    #     },
    #     'redirect': '/buildstruct'
    #   }

    # if 'useqmmm' in request.POST:
    #   saveAtomSelections(request, ws, pTask)

    # return calcEnergy_tpl(request, ws, ptid, et)
