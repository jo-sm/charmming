from charmming.views import BaseView

class LogoutView(BaseView):
  def get(self, request, *args, **kwargs):
    session = request.session
    session.delete()

    return {
      'cookies': {
        'charmming-token': None
      }
    }
