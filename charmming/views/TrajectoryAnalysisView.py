from charmming.views import BaseView
from charmming.models import Structure
from charmming.forms import TrajanalFileForm
from charmming.utils.structure.aux import checkNterPatch

class TrajectoryAnalysisView(BaseView):
  template_name = 'html/trajanalform.html'

  def get(self, request, *args, **kwargs):
    if kwargs.get('updating') == 'true':
      return {
        'status': {
          'info': 'Trajectory analysis running...'
        }
      }

    try:
      file = Structure.objects.filter(owner=request.user, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'Please submit a structure first'
        },
        'redirect': '/fileupload'
      }

    # do the usual lame copy-paste job...
    solv_pdb = "new_" + file.stripDotPDB(file.filename) + "-solv.pdb"
    neu_pdb = "new_" + file.stripDotPDB(file.filename) + "-neutralized.pdb"
    min_pdb = "new_" + file.stripDotPDB(file.filename) + "-min.pdb"
    md_pdb = "new_" + file.stripDotPDB(file.filename) + "-md.pdb"
    ld_pdb = "new_" + file.stripDotPDB(file.filename) + "-ld.pdb"
    sgld_pdb = "new_" + file.stripDotPDB(file.filename) + "-sgld.pdb"
    filename_list = file.getLimitedFileList('blank')
    seg_list = file.segids.split(' ')
    het_list = file.getNonGoodHetPDBList()
    tip_list = file.getGoodHetPDBList()
    seg2_list = file.getProteinSegPDBList()
    protein_list = [x for x in seg2_list if x.endswith("-pro.pdb") or x.endswith("-pro-final.pdb")]
    go_list = [x for x in seg2_list if x.endswith("-go.pdb") or x.endswith("-go-final.pdb")]
    nucleic_list = [x for x in seg2_list if x.endswith("-dna.pdb") or x.endswith("-dna-final.pdb") or x.endswith("-rna.pdb") or x.endswith("-rna-final.pdb")]
    userup_list = [
      x for x in seg2_list if not ( x.endswith("-pro.pdb") or x.endswith("-pro-final.pdb") or x.endswith("-dna.pdb") or x.endswith("-dna-final.pdb") or x.endswith("-rna.pdb") or x.endswith("-rna-final.pdb") or x.endswith("het.pdb") or x.endswith("het-final.pdb") or x.endswith("-go.pdb") or x.endswith("-go-final.pdb")) ]
    disulfide_list = file.getPDBDisulfides()
    # scriptlist = []

    # TODO: Handle this in POST
    # # This handles the case where the user submits the form
    # for i in range(len(filename_list)):
    #   try:
    #     tempid = request.POST[filename_list[i]]
    #     # filename = request.POST[filename_list[i]]
    #   except:
    #     try:
    #       tempid = request.POST['solv']
    #       # filename = request.POST['solv']
    #     except:
    #       tempid = "null"
    #   if tempid != "null":
    #     try:
    #       if(request.POST['usepatch']):
    #         file.handlePatching(request.POST)
    #     except:
    #       # If there is no patch, make sure patch_name is zero
    #       file.patch_name = ""
    #       file.save()
    #     # if filename != solv_pdb and filename != min_pdb and filename != md_pdb and filename != ld_pdb and filename != sgld_pdb and filename != neu_pdb:
    #     #   TODO: this is apparently utils.working_structure.build now
    #     #   append_tpl(request.POST, filename_list, file, scriptlist)
    #       # html = trajanal_tpl(request, file, scriptlist)
    #     # treat an appended PDB differently as it will have different PSF/CRD files
    #     # else:
    #     #   html = trajanal_tpl(request, file, scriptlist)

    #     return django.http.HttpResponseRedirect("/analysis/trajanal?updating=true")

    # this handles the part where the user has not uploaded anything (just display the form)
    propatch_list = []
    nucpatch_list = []
    for seg in seg_list:
      if seg.endswith("-pro"):
        defpatch = checkNterPatch(file, seg)
        propatch_list.append((seg, defpatch))
      elif seg.endswith("-dna") or seg.endswith("-rna"):
        # Right now, all nucleic acids get the 5TER and 3TER patches by default
        nucpatch_list.append((seg, "5TER", "3TER"))
    form = TrajanalFileForm(initial={'atomselection': 'all'})

    return {
      'context': {
        'form': form,
        'filename_list': filename_list,
        'seg_list': seg_list,
        'file': file,
        'min_pdb': min_pdb,
        'solv_pdb': solv_pdb,
        'neu_pdb': neu_pdb,
        'md_pdb': md_pdb,
        'ld_pdb': ld_pdb,
        'sgld_pdb': sgld_pdb,
        'het_list': het_list,
        'tip_list': tip_list,
        'protein_list': protein_list,
        'trusted': True, # TODO: Handle ACL in a Mixin
        'disulfide_list': disulfide_list,
        'propatch_list': propatch_list,
        'nucleic_list': nucleic_list,
        'nucpatch_list': nucpatch_list,
        'seg_list': seg_list,
        'userup_list': userup_list,
        'go_list': go_list,
      }
    }
