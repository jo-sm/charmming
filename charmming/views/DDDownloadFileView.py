from django.http import HttpResponse

from charmming.views import BaseView
from charmming.models import DDFile

import mimetypes
import os

class DDDownloadFileView(BaseView):
  def get(self, request, *args, **kwargs):
    file_id = kwargs.get('file_id')
    file_id = file_id.replace("/", "")
    dfile = DDFile.objects.get(id=file_id)
    filename = dfile.file_location + "/" + dfile.file_name

    mimetype, encoding = mimetypes.guess_type("%s" % (filename))
    try:
      os.stat("%s" % (filename))
    except:
      return HttpResponse('Oops ... that file no longer exists.')
    response = HttpResponse(mimetype=mimetype)
    response['Content-Disposition'] = 'attachment; filename=%s' % dfile.file_name
    response.write(open(filename, "rb").read())
    return response
