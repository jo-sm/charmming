from charmming.views import BaseView

class AwaitingApprovalView(BaseView):
  template_name = 'registration/waitingapproval.html'
