from charmming.views import BaseView
from charmming.models import DDProtein

class DDProjectDetailsView(BaseView):
  template_name = 'html/ddprojectdetails.html'

  def get(self, request, *args, **kwargs):
    user_proteins = DDProtein.objects.filter(owner=request.user).order_by('pdb_code')
    project_id = kwargs.get('project_id')

    return {
      'context': {
        'targets': user_proteins,
        'project_id': project_id
      }
    }
