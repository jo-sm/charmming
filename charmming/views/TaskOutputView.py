from charmming.views import BaseView
from charmming.models import Task

import os

class TaskOutputView(BaseView):
  template_name = 'html/view_task_output.html'

  def get(self, request, taskid, *args, **kwargs):
    tdict = {}
    try:
      tid = int(taskid)
      task = Task.objects.get(id=tid)  # This is unique. Hopefully.
      # This model does not exist
      # if task.action == "dsfdocking":
      #     dockjob = dd_infrajobs.objects.get(job_scheduler_id=task.jobID)
      output_location = task.workstruct.structure.location
      os.chdir(output_location)
      out_action = task.action
    except Exception as ex:
      tdict['intro_string'] = "Task not found. Please report this issue." + str(ex)  # Since we have a jQuery frame already, let's just not worry about making one
      return {
        'context': tdict
      }
    # if task.action == "dsfdocking":
    #     #This is where things get weird since the file paths are completely different
    #     fp = open(charmming_config.user_home + "/dd/jobs/" + request.user.username + "/dd_job_" + str(dockjob.job_owner_index) + "/run.out")
    if task.action == "neutralization":  # We need some extra checks here because this is a two-part task
      try:
        path_to_solv = task.workstruct.structure.location + "/" + task.workstruct.identifier + "-neutralization.out"
        os.stat(path_to_solv)  # TODO: MOdify this for multi-names!
        # If the file exists, we read that, if not, we read solvation.out.
        fp = open(path_to_solv)
      except:
        fp = open(path_to_solv.replace("neutraliz", "solv"))  # Minimal form.
    elif task.action == "redox":  # There's a lot of scripts going on and no real "main" one doing execution.
      # If you can figure out a decent system for this stuff, please tell me. ~VS
      tdict['intro_string'] = "Redox information cannot be displayed. Please use the progress tracker instead."
      return {
        'context': tdict
      }
    else:
      fp = open(task.workstruct.identifier + "-" + out_action + ".out")  # e.g. 1yjp-energy.out
      # actions are standardized now. Whoever reads this in the future, make SURE your task ACTION matches the file name.
      # TODO: Adapt this code for mutli-file task recognition.
    fcontents = fp.read()
    fp.close()
    top_string = "Structure: " + task.workstruct.structure.name + ", Working Structure: " + task.workstruct.identifier + ", Action: " + task.action.capitalize()
    tdict['file_contents'] = fcontents
    tdict['intro_string'] = top_string
    return {
      'context': tdict
    }
