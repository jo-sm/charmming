from django.contrib.auth.models import User
from django.db.models import Q, F

from charmming import charmming_config
from charmming.views import BaseView
from charmming.utils.dd_infrastructure import objDDJob
from charmming.models import DDJob, DDFileObject, DDProteinConformation, DDLigand

from datetime import datetime
import os

class DDJobDetailsView(BaseView):
  template_name = 'html/ddviewjobdetails.html'

  def get(self, request, *args, **kwargs):
    job_id = kwargs.get('job_id')
    result = {}

    public_user = User.objects.get(username='public')

    rows = DDJob.objects.filter(
      job_scheduler_id__userid=request.user.id,
      job_scheduler_id__id=job_id.replace('/', '')
    ).annotate(
      dj_id=F('id'),
      id=F('job_scheduler_id__id')
    ).values(
      'dj_id',
      'id',
      'job_owner_index',
      'description',
      'queued',
      'job_scheduler_id__started',
      'job_scheduler_id__ended'
    ).order_by('-job_owner_index')

    if len(rows) == 0:
      return {
        'error': {
          'message': 'Job does not exist'
        }
      }

    job = rows[0]

    if job.state == 1:
      status = 'Queued'
    elif job.state == 2:
      status = 'Running'
    elif job.state == 3:
      status = 'Done'
    elif job.state == 4:
      status = 'Failed'

    if status == 'Queued':
      waited_time = (datetime.now() - job.queued).total_seconds()
    else:
      waited_time = (job.ended - job.queued).total_seconds()

    if status == 'Running':
      runtime_time = (datetime.now() - job.started).total_seconds()
    else:
      runtime_time = (job.ended - job.started).total_seconds()

    for row in rows:
      job = objDDJob()
      job.dj_id = row.dj_id
      job.id = row.id
      job.owner_index = row.job_owner_index
      job.description = row.description
      job.status = status
      job.queued = row.queued
      job.waited = waited_time
      job.runtime = runtime_time

      if not os.path.exists(charmming_config.user_dd_jobs_home + '/' + request.user.username + '/' + 'dd_job_' + str(job.owner_index)):
        result['statuses'] = {
          'warning': 'This is an old job whose files have been deleted from the server. The information shown on this page is incomplete.'
        }

    f2 = DDFileObject.objects.filter(owner__id=request.user.id, object_table_name__in=['dd_infrastructure_jobs', 'dd_target_protein_conformations']).select_related().values('file__id', 'object_id')

    file = [f for f in f2 if f.object_id == job.dj_id]

    if len(file) == 0:
      return {
        'error': {
          'message': 'Could not find file.'
        }
      }

    file = file[0]
    f1 = DDFileObject.objects.filter(file_id=file.file__id, object_table_name='dd_target_protein_conformations').values('object_id', 'file_id')

    file_ids = [f.file_id for f in f1]
    _conformations = DDProteinConformation.objects.filter(id=file.file__id).values('conformation_name', 'protein__pdb_code', 'protein__protein_name').distinct('protein__pdb_code')

    conformations = [{
      'conformation_name': c.conformation_name,
      'protein_pdb_code': c.protein__pdb_code,
      'protein_name': c.protein__protein_name,
      'file_id': f
    } for f in file_ids for c in _conformations]

    file_ids = DDFileObject.objects.filter(
      Q(
        owner__id__in=[request.user.id, public_user.id],
        object_table_name='dd_infrastructure_jobs',
        object_id=job.dj_id
      ) | Q(
        owner__id__in=[request.user.id, public_user.id],
        object_table_name='dd_substrate_ligands'
      )
    ).annotate(file_id=F('file__id')).values('file__id')

    file_ids = [f.file__id for f in file_ids]

    ids = DDFileObject.objects.filter(object_table_name='dd_substrate_ligands', file__id__in=file_ids)
    _job_ligands = DDLigand.objects.filter(id__in=ids, ).values('ligand_name', 'description').distinct('ligand_name')
    job_ligands = [{
      'ligand_name': j.ligand_name,
      'description': j.description,
      'file_id': fid
    } for fid in file_ids for j in _job_ligands]

    job_files = []

    job_files = DDFileObject.objects.filter(
      Q(file__file_name__iendswith='.out') | Q(file__file_name__iendswith='.tar.gz'),
      owner__id=request.user.id,
      object_table_name='dd_infrastructure_jobs',
      object_id=job.dj_id
    ).annotate(description='file__description', file_id='file__id').values('description', 'file_id')

    result.update({
     'context': {
        'job': job,
        'job_conformations': conformations,
        'job_ligands': job_ligands,
        'job_files': job_files,
      }
    })

    return result
