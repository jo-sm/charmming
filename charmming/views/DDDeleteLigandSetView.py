from charmming.views import BaseView
from charmming.models import DDLigandSetsLigands, DDLigandSet

class DDDeleteLigandSetView(BaseView):
  def post(self, request, *args, **kwargs):
    set_id = kwargs.get('set_id')
    setligands = DDLigandSetsLigands.objects.filter(ligands_set=DDLigandSet.objects.get(id=set_id))
    setligands.delete()

    deleteset = DDLigandSet.objects.get(id=set_id)
    deleteset.delete()

    return {
      'statuses': {
        'success': 'Done.'
      }
    }
