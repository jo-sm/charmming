from charmming import charmming_config
from charmming.views import BaseView
from charmming.helpers.views import Accountinator

import pickle

class GLMolView(BaseView):
  template_name = 'html/glmol.html'

  @Accountinator()
  def get(self, request, filename, *args, **kwargs):
    # Lets user view PDB through GLmol
    # NOTE; below kwargs come in from Accountinator
    struct = kwargs.get("struct")
    ws = kwargs.get("ws")
    cg_model = kwargs.get("cg_model")

    if len(filename) < 4:
      return {
        'error': {
          'message': 'Bad filename. Please verify that the file you are trying to open exists before accessing this page.'
        },
        'redirect': '/visualize'
      }

    filepath = struct.location.replace(charmming_config.user_home, '')  # ws is always in same dir as struct...
    filename = filepath + "/" + filename
    filename_end = filename.rsplit('/', 1)[1]

    helix_info = ""  # Holds helix/sheet info for GLmol to read off the textbox
    # We make a check for localpickle so we don't load the wrong thing...
    if ws.localpickle:  # ws is only called so that we can get its localpickle object.
      datafile = open(ws.localpickle)
    else:
      datafile = open(struct.pickle)
    pdbfile = pickle.load(datafile)  # Get the PDB file...
    datafile.close()
    try:
      for line in pdbfile.get_metaData()['helix']:  # Print out the helices/sheets
        helix_info = helix_info + "HELIX" + (" " * ((67 - len(line)) + 4)) + line.upper() + "\\n"
    except KeyError:
      pass
    try:
      for line in pdbfile.get_metaData()['sheet']:
        if len(line) < 60:  # i.e. if it's a "starting" record
          helix_info = helix_info + "SHEET" + (" " * ((31 - len(line)) + 4)) + line.upper() + "\\n"
        else:
          helix_info = helix_info + "SHEET" + (" " * ((60 - len(line)) + 4)) + line.upper() + "\\n"
    except KeyError:
      pass
    # There really isn't anything to be done if there's no helix/sheet info present because while it might mean that we need to run STRIDE it also might just mean there isn't any (like in 1yjp). Running STRIDE is cheap but it would probably be best to do it at the pychm/PDB I/O level rather than up here to guarantee everything is stored in the pickle's metadata
    # Since our PDBPickle doesn't know about the spacing and just stores the numbers, we need to "shift" the spaces to the left such that a 1-character sheet/helix number takes up 4 spaces after the HELIX/SHEET identifier, a 2-character number takes up 3 spaces, etc.
    # The length of a "basic" HELIX line (1-character number) is 67, so we subtract from 67, then add 4 to get the correct number of spaces
    # Similarly the length of a basic SHEET line is 60.
    # Now we have the helix information for GLmol stored.
    # WARNING! We always take first model. Does this ever matter? I have no idea!
    # TODO: Maybe do automated secondary structure guessing with STRIDE if no HELIX/SHEET.
    try:
      isProtein = not("-good-" in filename_end or "-bad-" in filename_end or filename_end[3] == "-")
      # TODO: Modify this! It's not a good way to tell and we really need a better way. Maybe attach an isProtein to the file objects...?
    except:
      return {
        'error': {
          'message': 'Bad filename. Please verify that the file you are trying to open exists before accessing this page.'
        },
        'redirect': '/visualize'
      }
    return {
      'context': {
        'cg_model': cg_model,
        'filepath': filename,
        'segid': 'NA',
        'resid': 'NA',
        'isProtein': isProtein,
        'helix_info': helix_info
      }
    }
