from charmming.views import BaseView
from charmming.models import DDProtein, DDProteinConformation, DDFileObject
from charmming.utils.dd_target import TargetConformationInfo, objProteinConformation

class DDTargetsView(BaseView):
  template_name = 'html/ddviewtargets.html'

  def get(self, request, *args, **kwargs):
    target_info_list = []
    conf_files = []
    targets = DDProtein.objects.filter(owner=request.user)

    for target in targets:
      new_target_info = TargetConformationInfo()
      new_target_info.id = target.id
      new_target_info.name = target.protein_name
      new_target_info.target_pdb_code = target.pdb_code
      new_target_info.description = target.description
      new_target_info.conformations = []
      conformations = DDProteinConformation.objects.filter(owner=request.user, protein=target)
      for conformation in conformations:
        conformation_info = objProteinConformation()
        conformation_info.conformation_id = conformation.id
        conformation_info.conformation_name = conformation.conformation_name
        conf_files = DDFileObject.objects.filter(owner__id=request.user.id, object_table_name='dd_target_protein_conformations', object_id=conformation.id)
        conf_files = [ f.file for f in conf_files ]
        conformation_info.files = conf_files
        new_target_info.conformations.append(conformation_info)

      target_info_list.append(new_target_info)

    return {
      'context': {
        'targets': target_info_list
      }
    }
