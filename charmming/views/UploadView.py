from charmming.views import BaseView
from charmming.utils.uri import data_uri
from charmming.utils.rcsb import retrieve_rcsb_pdb
from charmming.models import Structure

import base64

class UploadView(BaseView):
  def post(self, request, *args, **kwargs):
    """
    Handle a new file being uploaded.
    """

    # We attempt to figure out the file type first from the base64 encoding
    # If we can't, we will prompt the user later for information

    # TODO: Handle custom sequences
    pdb_id = request.POST.get('pdb_id')
    file_raw = request.POST.get('file')
    sequence = request.POST.get('sequence')

    if not pdb_id and not file_raw and not sequence:
      return {
        'error': {
          'message': 'Upload a file, select a PDB id, or input a custom sequence.'
        }
      }

    if pdb_id:
      file_type = 'pdb'
      file = retrieve_rcsb_pdb(pdb_id)

      if not file:
        return {
          'error': {
            'message': 'Could not retrieve {} from RCSB.'.format(pdb_id)
          }
        }

    elif file_raw:
      mimetype, file_encoded = data_uri(file_raw)
      file = base64.b64decode(file_encoded)
      file = file.decode('utf-8') # Need to send str to save_file

      if mimetype == 'chemical/x-pdb':
        file_type = 'pdb'
      elif mimetype == 'chemical/x-crd':
        file_type = 'crd'
      else:
        file_type = 'unknown'

      if file_type == 'unknown':
        return {
          'error': {
            'message': 'Unknown file type. Upload a PDB or CRD file.'
          }
        }

    elif sequence:
      pass
      # Need to pass the custom sequence to CHARMM to create the PDB

    # TODO: Check the file type
    # Initially check if it's text, and if so,
    # test to see if it's PDB, CRD, PSF
    # If it's binary, try to parse as DCD.
    # If we can't understand it, reject it

    user = request.session.user
    structure = Structure()
    structure.owner = user

    # This is temporary in case the `save_pdb` doesn't populate correctly
    structure.name = '{} File'.format(file_type.upper())
    structure.save()

    saved = structure.save_pdb(file, crd=(file_type == 'crd'))

    if not saved:
      # The file couldn't save. Maybe disk full?
      structure.delete()

      return {
        'error': {
          'message': "Your structure could not be saved. Try again."
        }
      }

    return {
      'context': {
        'structure': structure.id
      }
    }
