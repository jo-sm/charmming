from charmming.views import BaseView
from charmming.models import Structure, Segment, WorkingStructure, Task

class AddStructTopparView(BaseView):
  template_name = 'html/addstructtoppar.html'

  def get(self, request, *args, **kwargs):
    # input.checkRequestData(request)

    try:
      struct = Structure.objects.filter(owner=request.user, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'Please submit a structure first.'
        },
        'redirect': '/fileupload'
      }

    seglist = []
    segs = Segment.objects.filter(structure=struct, is_working='n')
    for s in segs:
      seglist.append(s.name)

    return {
      'context': {
        'seglist': seglist
      }
    }

  def post(self, request, *args, **kwargs):
    # input.checkRequestData(request)

    try:
      struct = Structure.objects.filter(owner=request.user, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'Please submit a structure first.'
        },
        'redirect': '/fileupload'
      }

    # We have to create a working structure here, but it's already been built so yay
    # Note that this workingstructure is going to be weird, since it has NO
    # WorkingSegments associated with it (which is why we never call associate here).

    new_ws = WorkingStructure()
    new_ws.identifier = struct.name
    new_ws.modelName = 'model00'
    new_ws.structure = struct
    new_ws.selected = 'y'
    new_ws.isBuilt = 't'

    # The seglist is only needed in case we hock up an error and need to reload
    # the form.
    seglist = []
    segs = Segment.objects.filter(structure=struct, is_working='n')
    for s in segs:
      seglist.append(s.name)

    try:
      ntoppar = int(request.POST['ntoppar'])
      ntpstr = int(request.POST['ntpstr'])
    except:
      return {
        'error': {
          'message': 'At least one set of topologies and parameters is needed.'
        },
        'context': {
          'seglist': seglist
        }
      }

    if ntoppar + ntpstr < 1:
      return {
        'error': {
          'message': 'At least one set of topologies and parameters is needed.'
        },
        'context': {
          'seglist': seglist
        }
      }

    tpfp = open(struct.location + '/btopo.txt', 'w')
    prfp = open(struct.location + '/bparm.txt', 'w')
    for i in range(1, ntoppar + 1):
      try:
        topfile = request.FILES['top_%d' % i]
        parfile = request.FILES['par_%d' % i]
      except:
        return {
          'error': {
            'message': "Failed to upload top/par pair {}.".format(i)
          },
          'context': {
            'seglist': seglist
          }
        }

      try:
        fp = open('%s/%s' % (struct.location, topfile.name), 'w+')
        for fchunk in topfile.chunks():
          fp.write(fchunk)
        fp.close()
        fp = open('%s/%s' % (struct.location, parfile.name), 'w+')
        for fchunk in parfile.chunks():
          fp.write(fchunk)
        fp.close()
      except:
        return {
          'error': {
            'message': "Failed to place top/par file {} on disk.".format(i)
          },
          'context': {
            'seglist': seglist
          }
        }

      tpfp.write('%s\n' % topfile.name)
      prfp.write('%s\n' % parfile.name)

    tpfp.close()
    prfp.close()

    new_ws.topparStream = ''
    for i in range(1, ntpstr + 1):
      try:
        tpstream = request.FILES['tpstream_%d' % i]
      except:
        return {
          'error': {
            'message': "Failed to upload stream file pair {}.".format(i)
          },
          'context': {
            'seglist': seglist
          }
        }

      try:
        fp = open('%s/%s' % (struct.location, tpstream.name), 'w+')
        for fchunk in tpstream.chunks():
          fp.write(fchunk)
        fp.close()
      except:
        return {
          'error': {
            'message': "Failed to place stream file {} on disk.".format(i)
          },
          'context': {
            'seglist': seglist
          }
        }

      new_ws.topparStream += '%s ' % tpstream.name
    new_ws.save()

    # create a fake build task that we can reference -- if we don't do this, we're
    # going to have problems later on.
    task = Task()
    task.setup(new_ws)
    task.parent = None
    task.action = 'build'
    task.active = 'y'
    task.status = 'C' # yeah - user did this for us!
    task.save()

    return {
      'template': 'html/built.html',
      'context': {
        'nscale_msg': ''
      }
    }
