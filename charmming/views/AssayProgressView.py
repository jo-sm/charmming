from charmming.views import BaseView
from charmming.utils.assays import get_dir

import os

class AssayProgressView(BaseView):
  def post(self, request, *args, **kwargs):
    if 'filename' in request.POST:
      filename = request.POST['filename']
      work_dir = get_dir(request)
      name = work_dir + filename + ".done"
      if os.path.isfile(name):
        os.remove(name)
      return {
        'context': {
          'num: num'
        }
      }
    return {
      'context': {
        'num': None
      }
    }
