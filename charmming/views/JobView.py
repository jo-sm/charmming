from charmming.views import JobBaseView
from charmming.utils.job import submit

class JobView(JobBaseView):
  def post(self, request, *args, **kwargs):
    data = request.POST
    structure = request.structure
    program = kwargs.get('program')
    job = kwargs.get('job')

    submitted_task = submit(structure, program, job, data)

    if not submitted_task:
      return {
        'error': {
          'message': 'Could not submit {} {} job.'.format(program, job)
        }
      }
    else:
      return {
        'context': {
          'task': submitted_task.id
        }
      }
