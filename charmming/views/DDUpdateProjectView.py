from charmming.models import DDProject
from charmming.views import BaseView
from charmming.utils.dd_infrastructure import ProjectExists

import datetime

class DDUpdateProjectView(BaseView):
  template_name = 'html/ddprojectinfo.html'

  def post(self, request, *args, **kwargs):
    owner = request.user
    action = kwargs.get('action')
    projectid = kwargs.get('projectid')
    if ((action == 'update') or (action == 'addnew')):

      if (len(request.POST['name'].strip()) == 0):
        message = "Operation Failed!<br>Project Name Cannot be Blank"
        if (action == 'update'):
          project = DDProject.objects.get(owner=request.user, id=projectid)
          return {
            'context': {
              'project': project,
              'message': message,
              'messagecolor': 'Red'
            }
          }
        else:
          return {
            'template': 'html/ddnewprojectinfo.html',
            'context': {
              'description': request.POST['description'],
              'name': request.POST['name'],
              'message': message,
              'messagecolor': 'Red'
            }
          }
      else:
        if (action == 'update'):
          project = DDProject.objects.get(owner=request.user, id=projectid)
          project.project_name = request.POST['name'].strip()
          project.description = request.POST['description'].strip()
          project.save()
          project_id = projectid
          message = "Project Info was successfully updated"

        elif (action == 'addnew'):
          if ProjectExists(request.POST['name'], request.user) is True:
            return {
              'template': 'html/ddnewprojectinfo.html',
              'context': {
                'description': request.POST['description'],
                'name': request.POST['name'],
                'message': 'Operation Failed!<br>Duplicate Project Found.',
                'messagecolor': 'Red'
              }
            }

          project = DDProject()
          project.owner = owner
          project.date_created = datetime.datetime.now().isoformat(' ')
          project.project_name = request.POST['name'].strip()
          project.description = request.POST['description'].strip()
          project.save()

          added_project = DDProject.objects.filter(owner=request.user).order_by('-id')[0]  # filter[0] is intended
          project_id = added_project.id
          message = "Project was successfully created"

      project = DDProject.objects.get(owner=request.user, id=project_id)
      return {
        'context': {
          'project': project,
          'message': message,
          'messagecolor': 'Green'
        }
      }
    # no action, just display blank or populated form
    else:
      if (projectid != '0'):
        project = DDProject.objects.get(owner=request.user, id=projectid)
        return {
          'context': {
            'project': project,
            'message': '',
            'messagecolor': 'Red'
          }
        }
      else:
        return {
          'template': 'html/ddnewprojectinfo.html',
          'context': {
            'description': '',
            'message': '',
            'messagecolor': 'Red'
          }
        }
