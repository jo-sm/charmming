from charmming.views import BaseView
from charmming.models import Structure

class PDBsView(BaseView):
  template_name = 'html/viewpdbs.html'

  def get(self, request, *args, **kwargs):
    user_pdbs = Structure.objects.filter(owner=request.user)

    return {
      'context': {
        'user_pdbs': user_pdbs
      }
    }
