from django.db.models import Count

from charmming.views import BaseView
from charmming.models import DataPoint

class StatisticsView(BaseView):
  template_name = 'admin/statistics.html'

  def get(self, request, *args, **kwargs):
    tdict = {}
    datapoints = DataPoint.objects.all()  # Get everything since we don't care about any filtering criteria
    task_actions = {}
    task_action_list = []
    total_tasks = len(datapoints)
    successful_tasks = 0
    failed_tasks = 0
    users = set([])
    structure_dict = {}
    struct_ids = set([])
    users_by_structure = DataPoint.objects.values('user').annotate(scount=Count('struct_id', distinct=True)).order_by('-scount')
    for point in datapoints:
      if point.success:
        successful_tasks += 1
      else:
        failed_tasks += 1
      if point.user not in users:
        users = users.union(set([point.user]))
      if point.task_action not in list(task_actions.keys()):
        task_actions[point.task_action] = 1
      else:
        task_actions[point.task_action] += 1
      if point.struct_id not in struct_ids:  # If the struct id is already in there, you shouldn't double count it.
        struct_ids.add(point.struct_id)
        if point.structure_name not in list(structure_dict.keys()):
          structure_dict[point.structure_name] = 1
        else:
          structure_dict[point.structure_name] += 1
    top_structures = reversed(sorted(list(structure_dict.items())[0:50], key=lambda x: x[1]))  # It's ok to over-reach with range because Python automagically chops only the pieces that exist
    tdict['users_by_structure'] = users_by_structure
    tdict['top_structures'] = top_structures
    total_users = len(users)
    # This is silly but it's also the easiest way to get it right. I tried to handle it with a list and the lookups get silly and mess my code up, at least this works
    for key, val in task_actions.items():
      task_action_list.append((key, val))
    task_action_list = sorted(task_action_list, key=lambda x: x[0])  # Make it into a sorted list for easy display later
    tdict['task_list'] = task_action_list
    tdict['successful_tasks'] = successful_tasks
    tdict['failed_tasks'] = failed_tasks
    tdict['total_users'] = total_users
    tdict['total_tasks'] = total_tasks

    return {
      'context': tdict
    }
