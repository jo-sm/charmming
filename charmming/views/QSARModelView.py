from django.contrib.auth.models import User

from charmming.views import BaseView
from charmming.forms import QsarTrainingUploadForm
from charmming.utils.qsar import handle_uploaded_file, get_dir, qsar_property
from charmming.models import QSARModel, QSARModelType
from charmming.utils.qsar.train import get_sd_properties

from tempfile import mkstemp

import os

class QSARModelView(BaseView):
  template_name = 'qsar/newmodel.html'

  def newModel(request):
    form = QsarTrainingUploadForm() # A empty, unbound form

    return {
      'context': {
        'form': form
      }
    }

  def post(self, request, *args, **kwargs):
    form = QsarTrainingUploadForm(request.POST, request.FILES)
    if form.is_valid():
      (fh, name) = mkstemp(dir=get_dir(request))
      os.close(fh)
      handle_uploaded_file(request.FILES['trainfile'], name)
      activity_property_choice_length = len(get_sd_properties(name))
      if not activity_property_choice_length:
        return {
          'error': {
            'message': 'Not an SD file or no SD properties found. Please upload an SD file with Activity Property'
          },
          'context': {
            'form': form,
          }
        }
      u = User.objects.get(username=request.user.username)
      newmodel = QSARModel()
      newmodel.model_owner = request.user
      newmodel.model_owner_index = newmodel.getNewModelOwnerIndex(u)
      newmodel.model_name = request.POST['model_name']
      newmodel.model_type = QSARModelType.objects.get(id=request.POST['model_type'])
      newmodel.save()
      filename = os.path.basename(name)
      return qsar_property(request, filename, newmodel.id)
    else:
      return {
        'context': {
          'form': form
        }
      }
