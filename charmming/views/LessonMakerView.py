from charmming import charmming_config
from charmming.views import BaseView
from charmming.models import Structure, LessonMakerLesson, Step, Objective

class LessonMakerView(BaseView):
  template_name = 'html/lesson_maker_create.html'
  admin = True

  def get(self, request):
    tdict = {}
    try:
      struct = Structure.objects.filter(owner=request.user, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'No structure has been uploaded. Please upload a structure to begin recording lessons for the LessonMakerLesson Maker.'
        },
        'redirect': '/fileupload'
      }

    if not struct.lessonmaker_active:
      return {
        'error': {
          'message': 'Your structure is not currently recording your actions for the LessonMakerLesson Maker. Please upload a structure that does so before using the LessonMakerLesson Maker.'
        },
        'redirect': '/fileupload'
      }

    # now that we've verified that things are doing fine we can keep going
    # we should not assume that we have a workstruct or ANYTHING. LessonMakerLessons can be just an upload
    # in the end. They're not very useful lessons, clearly, but we have to leave the
    # possibility open.

    recorded_lesson = LessonMakerLesson.objects.filter(structure=struct)[0]
    # everything else is attached to Structure - if it's selected, then we don't have to check again, etc.

    steps = Step.objects.filter(lesson=recorded_lesson)
    # now we have all the steps that are in that lesson, so we can make a list of steps

    step_list = []

    for step in steps:
      step_list.append((step.step, step.type))
    if len(step_list) < 1:
      return {
        'error': {
          'message': 'Your lesson has less than one step. This should not happen. Please report this bug to your system administrator, and try to build your structure for recording lessons again.'
        },
        'redirect': '/fileupload'
      }
    elif len(step_list) < 3:
      return {
        'error': {
          'message': 'Your lesson does not perform any calculations. Please perform some calculation on your structure for a proper lesson.'
        },
        'redirect': '/'
      }
    new_step_list = [] # we need to hold references to previous and next, because the template can't read them
    for i in range(0, len(step_list)):
      if i < len(step_list) - 2:
        new_step_list.append((step_list[i], step_list[i + 1])) # this, next, we don't need prev
      else:
        new_step_list.append((step_list[i], None))

    tdict['step_list'] = new_step_list
    # now that we have a list we can make a bunch of text boxes for the user to put stuff in to
    # in theory we can display the parameters there too but that's a lot more work and
    # involves iterating through all of the class properties
    # it would be useful to display pdb results? FOr now let's make simple boxes like these
    # since we don't need much more.
    return {
      'context': tdict
    }

  def post(self, request, *args, **kwargs):
    # if the user puts in a lesson that already exists, we overwrite, because deleting
    # is a pain and they should deal with that, not us
    objectives = []
    steps = []
    for key, value in request.POST.items():
      # since we need to iterate through to get the objectives stuff anyway
      # we might as well fill in the rest of our variables here
      if "lesson_name" in key:
        lesson_name = request.POST['lesson_name']
      elif "intro_text" in key:
        intro_text = request.POST['intro_text']
      elif "objective" in key:
        objectives.append((key.split(" ")[1], value))
      elif "title" in key:
        lesson_title = request.POST['title']
      elif "done" in key or "running" in key:
        new_key = key.strip().split(" ")
        steps.append((value, float(new_key[1]), new_key[2])) # this is text, step number, status (running vs done)

    try:
      curr_struct = Structure.objects.filter(owner=request.user, selected='y')[0]
      lesson_obj = LessonMakerLesson.objects.filter(structure=curr_struct)[0]
      step_objects = Step.objects.filter(lesson=lesson_obj) # don't take 0, we need all of them
    except:
      return {
        'error': {
          'message': 'Something has gone terribly wrong when trying to create database objects for the LessonMakerLesson Maker. Please contact your system administrator.'
        },
        'redirect': '/fileupload'
      }

    if lesson_obj.finished: # get out - you shouldn't do this twice. This object should have been wiped.
      return {
        'error': {
          'message': 'Your current lesson recording has already been turned into a file. If you wish to create a different lesson using this same structure, please upload it again.'
        },
        'redirect': '/fileupload'
      }

    lesson_obj.name = lesson_name
    lesson_obj.intro_text = intro_text
    lesson_obj.title = lesson_title
    lesson_obj.save() # we don't mark it as saved UNTIL it's actually done.

    for objective in objectives:
      new_obj = Objective()
      new_obj.lesson = lesson_obj
      new_obj.obj_num = int(objective[0])
      new_obj.obj_text = objective[1]
      new_obj.save()

    for step in steps:
      try:
        new_step = step_objects.get(step=str(step[1]))
      except:
        return {
          'error': {
            'message': 'There is more than one step with the same number. Please report this error to your system administrator.'
          },
          'redirect': '/fileupload'
        }
      if "running" in step[2]:
        new_step.running_text = step[0]
      elif "done" in step[2]:
        new_step.done_text = step[0]
      else: # uh oh. This is likely that someone messed with the values of the name attributes on the page.
        return {
          'error': {
            'message': 'Invalid step. Please contact your system administrator about this message.'
          },
          'redirect': '/fileupload;'
        }
      new_step.save()

    # At this point we're done.
    lesson_obj.finished = True
    lesson_obj.save()

    return {
      'statuses': {
        'success': "LessonMakerLessons Data successfully saved to the database. Please run " + charmming_config.charmming_root + "/create_lesson.py to complete the creation process."
      },
      'redirect': '/'
    }
