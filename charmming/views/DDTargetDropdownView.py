from charmming.views import BaseView
from charmming.models import DDProtein

class DDTargetDropdownView(BaseView):
  template_name = 'html/ddtargetdropdown.html'

  def get(self, request, *args, **kwargs):
    targetlist = DDProtein.objects.filter(owner=request.user).order_by("protein_name")

    return {
      'context': {
        'targetlist': targetlist
      }
    }
