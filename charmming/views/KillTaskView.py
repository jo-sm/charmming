from charmming.views import BaseView
from charmming.models import Task

class KillTaskView(BaseView):
  def post(self, request, taskid, *args, **kwargs):
    # input.checkRequestData(request)

    try:
      tid = int(taskid)
      t = Task.objects.get(id=tid)
    except:
      return {
        'error': {
          'message': 'Task does not exist.'
        }
      }

    # TODO: handle this in the SQL
    if t.workstruct.structure.owner != request.user:
      return {
        'error': {
          'message': 'Task does not exist.'
        }
      }

    if t.status != 'R':
      return {
        'error': {
          'message': 'Job is not currently running.'
        }
      }

    t.kill()
    t.workstruct.unlock()

    return {
      'statuses': {
        'success': 'Task successfully killed.'
      }
    }
