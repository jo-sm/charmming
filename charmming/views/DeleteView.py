from charmming.views import BaseView

import os
import subprocess

from charmming.helpers.views import Accountinator
from charmming import charmming_config
from charmming.models import Structure

class DeleteView(BaseView):
  """
  TODO: Move into FileView action "delete"
  """
  @Accountinator(auth_only=True)
  def post(self, request):
    """
    Deletes the specified file both from the database (if a WorkingFile), as well as on disk.
    """
    if 'filename' in request.POST:
      delete_filename = request.POST['filename']
    else:
      return {
        'error': {
          'message': 'Invalid request'
        },
        'redirect': '/buildstruct'
      }

    deleteAll = 0
    # remove_extension = re.compile('\.\S*')
    # If user wants to remove all files, visit /deletefile/all_files/

    if(delete_filename == "all_files"):
      deleteAll = 1
      total_files = Structure.objects.filter(owner=request.user)
    else:
      file = Structure.objects.filter(owner=request.user, name=delete_filename)[0]
      total_files = [file]

    for s in total_files[::-1]:
      os.chdir(charmming_config.user_home + '/' + request.user.username)
      subprocess.call(["rm", "-rf", s.name])  # TODO: Replace with some sort of OS call...?

      if s.selected == 'y' and deleteAll == 0:
        s.selected = ''
        allFileList = Structure.objects.filter(owner=request.user)
        if len(allFileList) >= 2:
          # We need to change the selection to another file, arbitrarilty we choose
          # the first one.
          for candidateSwitch in allFileList:
            if candidateSwitch.name != delete_filename:
              candidateSwitch.selected = 'y'
              candidateSwitch.save()
              break
      try:
        s.delete()
      except:
        pass
    return {
      'statuses': {
        'success': 'File deleted'
      }
    }
