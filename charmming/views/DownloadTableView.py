from charmming import charmming_config
from charmming.views import BaseView

import os
import re

class DownloadTableView(BaseView):
  def fetch_path(request, path):
    # this one takes in a path from the website (which is a path based on the workstruct's current location)
    # and then returns the folders and files inside that path, in a datastructure as follows:
    # CONTENT object
    # CONTENT.type = "file" or "folder". This identifies whether something is a file or a folder.
    # CONTENT.name - this splits the path to only have the name, simplifying display later on
    # CONTENT.path - the path to this file, or to this folder. Used to access the file or the folder.
    # CONTENT.root - "one level up" from the current folder
    # CONTENT.contents - the meat of the problem; this one holds all files and folders inside a folder
    # FOR FILES:
    # CONTENT.is_displayable - applies only to files that are INP/OUT
    # CONTENT.view_link - INP/OUT viewer link. We have this technology already.
    # CONTENT.download_link - actual link to the file via webserver.
    allowed_filetypes = {
      "crd": 0,
      "psf": 0,
      "sdf": 0,
      "pdb": 0,
      "mol2": 0,
      "txt": 0,
      "in": 0,
      "inp": 0,
      "out": 0,
      "pka": 0,
      "rtf": 0,
      "prm": 0
    }  # add to this list for more acceptable files
    base_path = charmming_config.charmming_root + "/pdbuploads/" + request.user.username  # this path is used for actual filesystem reasons
    new_files = {}
    if len(path.split("/")) < 2:  # big mistake
      return {
        'error': {
          'message': 'Invalid path for this structure. Please contact your system administrator with this message.'
        },
        'redirect': '/fileupload'
      }
    try:
      os.stat("%s/%s" % (base_path, path ))  # extra slash just in case. weird things can happen if there isn't one.
    except:
      # oh dear - return an object with type NULL
      new_files['type'] = None

      return {
        'context': new_files
      }
    # well, now we're sure the path exists! Good show. Let's make an object.
    new_files['type'] = "folder"  # it has to be, it's the path

    contents = []
    filenam = []
    dirnam = []
    is_admin = request.user.is_superuser
    os.chdir(base_path)

    # let's make sure we don't walk anywhere we're not supposed to by putting
    # the user's base path in front.
    path_to_walk = path.strip()
    path_to_walk = base_path + '/' + path

    for dirname, dirnames, filenames in os.walk(path_to_walk):
      dirnam.extend(dirnames)
      filenam.extend(filenames)
      break

    for x in dirnam:
      dir = {}
      dir['path'] = os.path.join(path, x)
      # maybe modify this string based on whatever format we may want
      dir['contents'] = None  # specify this so the JS doesn't go mad about undefined
      dir['type'] = "folder"
      dir['name'] = x.split("/")[-1]
      dir['root'] = path
      contents.append(dir)
      # now do files. Since these are different loops, we have to copy the code.
    for y in filenames:
      filetype = y.split(".")[-1]
      if (filetype in allowed_filetypes or is_admin) and not re.match("o[0-9]+", filetype):
        # if the file extension is in our list of allowed filetypes...admins see all
        # however no one can see o files from the job scheduler, since you don't get permission
        # to download them. If you really need to see those...you probably have access to the real disk.
        file = {}
        file['path'] = os.path.join(path, y)
        file['contents'] = None  # specify this so the JS doesn't go mad about undefined
        file['type'] = "file"
        file['name'] = y.split("/")[-1]
        file['root'] = path
        file['download_link'] = os.path.join(base_path.replace(charmming_config.charmming_root), file['path'])
        if y.split(".")[-1] in allowed_filetypes:  # displayable files
          file['view_link'] = "/viewprocessfiles/viewfile/" + y  # use the old infrastructure cause it ain't broke
          file['is_displayable'] = True
        else:
          file['is_displayable'] = False
        contents.append(file)
    contents = sorted(contents, key=lambda x: (x['type'] + " " + x['name']))
    new_files['contents'] = contents
    new_files['root'] = path.split("/")[-1]

    return {
      'context': new_files
    }
