from charmming.views import BaseView
from charmming.models import Session

class SessionsView(BaseView):
  def get(self, request, *args, **kwargs):
    sessions = Session.objects.filter(user=request.user)

    if len(sessions) == 0:
      # This shouldn't happen, since this is an auth-only view...
      return {
        'error': {
          'message': 'An error occurred.'
        }
      }

    # TODO: Handle toJSON properly
    return {
      'context': {
        'sessions': sessions
      }
    }
