from charmming.views import BaseView
from charmming.forms import DDTargetFileForm

class DDFileUploadFormView(BaseView):
  template_name = 'html/ddtargetfileuploadform.html'

  def get(self, request, *args, **kwargs):
    form = DDTargetFileForm()
    return {
      'context': {
        'form': form
      }
    }
