from charmming.views import BaseView
from charmming.models import Task, Structure, WorkingStructure, QchemTask
from charmming.helpers.views import Accountinator
from charmming.utils.atom_selection import getAtomSelections
from charmming.utils.qchem import qchem_tpl
# from charmming.utils.task import deactivate_current_task

class QchemView(BaseView):
  template_name = 'html/qchemform.html'

  @Accountinator(cg_fail="q-chem")
  def get(self, request, *args, **kwargs):
    # processes form data for Q-chem jobs
    # NOTE: The below two kwargs come in from Accountinator. The cg_model kwarg is ignored because this task does not support CG models.
    # struct = kwargs.get("struct")
    ws = kwargs.get("ws")
    tdict = {}
    # this is the stuff for the Q-Chem page display
    tdict['ws_identifier'] = ws.identifier  # you don't need WS built for it to have an identifier
    # get all workingFiles associated with this struct
    tasks = Task.objects.filter(workstruct=ws, status='C', active='y', modifies_coordinates=True)

    tdict['tasks'] = tasks
    # segments are also needed for QM/MM
    seg_list = []
    for seg in ws.segments.all():
      seg_list.append(seg.name)

    tdict['seg_list'] = sorted(seg_list)

    tdict = getAtomSelections(tdict, ws)  # it's possible that we should not call this yet, depending on how we change buildstruct

    return {
      'context': tdict
    }

  def post(self, request, *args, **kwargs):
    # hopefully we don't have issues with doing this...if not, make a hidden form field to submit maybe?
    try:
      struct = Structure.objects.filter(owner=request.user, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'Please submit a structure first.'
        },
        'redirect': '/fileupload'
      }
    try:
      ws = WorkingStructure.objects.filter(structure=struct, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'Please submit a working structure.'
        },
        'redirect': '/buildstruct'
      }
    deactivate_current_task("QchemTask", ws)

    qt = QchemTask()
    qt.setup(ws)
    qt.save()
    qt.symmetry = "symmetry" in request.POST
    qt.jobtype = request.POST['jobtype']
    if request.POST['jobtype'] == "force" or request.POST['jobtype'] == "sp":
      qt.modifies_coordinates = False  # hack until we get PSFGEN up.
    qt.exchange = request.POST['exchange']
    qt.charge = request.POST['charge']
    qt.correlation = request.POST['correlation']
    qt.basis = request.POST['basis']
    qt.multiplicity = request.POST['multiplicity']
    qt.save()
    qt.active = 'y'
    qt.action = 'qchem-' + request.POST['jobtype']
#        qt.useqmmm = request.POST['qm_mm']
    # we're only going to want two layers...we can use the qmmm_params one.
    qt.save()
    # we don't care about ws being built specifically
    # however we need to get the pTask....
    # BELOW THIS LINE IS MINIMIZATION.VIEWS!!
    if ws.isBuilt != 't':
      # isBuilt = False
      # try:
      #     pTask = ws.build(qt)
      # except NoNscaleFound, e:
      #     return output.returnSubmission('Minimization', error='The nScale parameterization process has not yet completed. It may take 1-2 hours.')
      # pTaskID = pTask.id
      #   we ignore everything and just set pTaskID to -1 so as to keep track of a sentinel
      pTaskID = -1
    else:
      # isBuilt = True
      pTaskID = int(request.POST['ptask'])  # presumably you'd fetch other coords...
      # pTask = Task.objects.get(id=pTaskID)
    return qchem_tpl(request, qt, pTaskID)
