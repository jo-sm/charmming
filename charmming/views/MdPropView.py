from charmming.models import Structure, WorkingStructure, MdTask, WorkingFile
from charmming.views import BaseView

# from pychm3.scripts.getprop import getProp

import os

class MdPropView(BaseView):
  template_name = 'html/mdanalysis.html'

  def get(self, request, *args, **kwargs):
    # TODO: Turn this into middleware
    # input.checkRequestData(request)

    # chooses the file based on if it is selected or not
    try:
      struct = Structure.objects.filter(owner=request.user, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'Please submit a structure first.'
        },
        'redirect': '/fileupload'
      }

    try:
      ws = WorkingStructure.objects.filter(structure=struct, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'Please build your structure before minimizing.'
        },
        'redirect': '/buildstruct'
      }

    try:
      MdTask.objects.filter(workstruct=ws, status='C', active='y')[0]
    except:
      return {
        'error': {
          'message': 'You must have completed molecular dynamics to get properties.'
        },
        'redirect': '/dynamics/md'
      }

    return {}

  def post(self, request, *args, **kwargs):
    # input.checkRequestData(request)
    # chooses the file based on if it is selected or not
    try:
      struct = Structure.objects.filter(owner=request.user, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'Please submit a structure first.'
        },
        'redirect': '/fileupload'
      }

    try:
      ws = WorkingStructure.objects.filter(structure=struct, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'Please build your structure before minimizing.'
        },
        'redirect': '/buildstruct'
      }

    try:
      mdp = MdTask.objects.filter(workstruct=ws, status='C', active='y')[0]
    except:
      return {
        'error': {
          'message': 'You must have completed molecular dynamics to get properties.'
        },
        'redirect': '/dynamics/md'
      }

    return {}

    fname = "%s/%s-md.out" % (ws.structure.location, ws.identifier)
  #    cmdline = "%s/prop.py %s %s-mdproperties.dat" % (charmming_config.data_home,fname,ws.identifier)
    os.chdir(ws.structure.location)
    # nprop = 0
    props = []
    if 'getener' in request.POST:
      props.append("averener")
  #        cmdline += " averener"
    if 'getvolu' in request.POST:
      props.append("avervolu")
  #        cmdline += " avervolu"
    if 'getpressi' in request.POST:
      props.append("averpressi")
  #        cmdline += " averpressi"
    if 'gettemp' in request.POST:
      props.append("avertemp")
  #        cmdline += " avertemp"
    if 'gettote' in request.POST:
      props.append("avertote")
  #        cmdline += " avertote"
    if 'gettotk' in request.POST:
      props.append("avertotk")
  #        cmdline += " avertotk"
    props.append("avertime")
    inpfile = open(fname)
    taco = getProp(inpfile, *props)
    inpfile.close()

    outLabels = []
    outData = []
    outLabels.append('avertime')
    outData.append(['%10.5f' % x for x in taco['avertime']])
    for key in list(taco.keys()):
      if key.endswith("time"):
        continue
      outLabels.append(key)
      outData.append(['%10.5f' % x for x in taco[key]])
    outData = map(*outData)

    rawOutput = []
    rawOutput.append('    '.join(outLabels))
    for line in outData:
      rawOutput.append('    '.join(line))
    rawOutput = '\n'.join(rawOutput)

    outFile = open(ws.identifier + "-mdproperties.dat", "w")
    outFile.write(rawOutput)
    outFile.close()

    # create a working file, so that this thing appears on the download page
    wf = WorkingFile()
    wf.task = mdp
    wf.path = '%s/%s-mdproperties.dat' % (ws.structure.location, ws.identifier)
    wf.canonPath = wf.path
    wf.type = 'prp'
    wf.description = 'MD property analysis data'
    wf.save()
    tdict = {}
    tdict['outLabels'] = outLabels
    tdict['outData'] = outData

    # TODO: update template to handle this
    return {
      'template': 'html/didprop.html',
      'context': tdict
    }
