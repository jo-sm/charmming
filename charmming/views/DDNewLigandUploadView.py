from django.contrib.auth.models import User

from charmming import charmming_config
from charmming.views import BaseView
from charmming.models import Structure, WorkingStructure, DDLigand, DDPose, DDFileObject, DDFileType, DDSource, DDProtein, DDFile
from charmming.forms import DDLigandFileForm
from charmming.utils.dd_substrate import makeCGenFF, makeMatch, getNewLigandOwnerIndex

from shutil import copyfile

import os

class DDNewLigandUploadView(BaseView):
  template_name = 'html/ddligandfileupload.html'

  def get(self, request, *args, **kwargs):
    try:
      struct = Structure.objects.get(owner=request.user, selected='y')
    except:
      return {
        'error': {
          'message': 'Please submit a structure first.'
        },
        'redirect': '/fileupload'
      }

    try:
      WorkingStructure.objects.get(structure=struct, selected='y')
    except:
      return {
        'error': {
          'message': 'Please build a structure first.'
        },
        'redirect': '/buildstruct'
      }

    form = DDLigandFileForm()
    return {
      'context': {
        'form': form
      }
    }

  def post(self, request, *args, **kwargs):
    try:
      struct = Structure.objects.get(owner=request.user, selected='y')
    except:
      return {
        'error': {
          'message': 'Please submit a structure first.'
        },
        'redirect': '/fileupload'
      }

    try:
      ws = WorkingStructure.objects.get(structure=struct, selected='y')
    except:
      return {
        'error': {
          'message': 'Please build a structure first.'
        },
        'redirect': '/buildstruct'
      }

    username = request.user.username
    u = User.objects.get(username=username)
    newligand = DDLigand()
    # newfile = files()
    # newfileobject= files_objects()
    newligandpose = DDPose()
    newfileposeobject = DDFileObject()
    ligand_file_type = DDFileType.objects.filter(file_type_name="Ligand Structure File")[0]
    psf_file_type = DDFileType.objects.get(file_type_name="CHARMM Structure File")
    # str_file_type = file_types.objects.get(file_type_name="CHARMM Param Stream File")
    user_source = DDSource.objects.filter(source_name='User')[0]
    form = DDLigandFileForm()
    try:
      request.FILES['ligand_file'].name
      ligand_uploaded_by_user = 1
    except:
      ligand_uploaded_by_user = 0

    if ligand_uploaded_by_user == 1:
      if request.POST['pdbcode'] != "":
        sourceproteins = DDProtein.objects.filter(owner=u, pdb_code=request.POST['pdbcode'])
      # this commented block of code is needed when checking for existing ligand is coded in
      # if 1==1:
      # try:
      #    newligand=ligands.objects.filter(owner=u,id=request.POST[''])[0]
      # Atomtyping check
      tempname = 'dd_user_ligand'
      os.system("rm /tmp/%s.mol2" % (tempname))
      destination = open("/tmp/%s.mol2" % (tempname), 'w')
      for fchunk in request.FILES['ligand_file'].chunks():
        destination.write(fchunk)
      destination.close()

      if request.POST['tpmeth'] == 'autogen':
        # before we try any build methods, first make sure we have top/par in the right dir
        top_dest = "/tmp/%s" % charmming_config.default_cgenff_top
        par_dest = "/tmp/%s" % charmming_config.default_cgenff_prm
        try:
          os.stat(top_dest)
          os.stat(par_dest)
        except:
          copyfile("%s/toppar/%s" % (charmming_config.data_home, charmming_config.default_pro_top), top_dest)
          copyfile("%s/toppar/%s" % (charmming_config.data_home, charmming_config.default_pro_prm), par_dest)
        success = False
        for tpmeth in charmming_config.dd_toppar_generators.split(','):
          if tpmeth == 'genrtf':
            rval = -1  # self.makeGenRTF(bhResList)
          elif tpmeth == 'antechamber':
            rval = -1  # common.makeAntechamber(tempname)
          elif tpmeth == 'cgenff':
            rval = makeCGenFF(ws, tempname)
            if rval == 0:
              os.system("cp %s/build_%s.inp /tmp" % (charmming_config.dd_scripts_home, tempname))
              os.chdir("/tmp")
              os.system("%s < build_%s.inp > build_%s.out" % (charmming_config.charmm_exe, tempname, tempname))
              if "NORMAL TERMINATION BY NORMAL STOP" in open("/tmp/build_%s.out" % (tempname)).read():
                rval = 0
              else:
                rval = -1

              # make a back-up of the CGENFF file
              copyfile("/tmp/build_%s.out" % tempname, "/tmp/build_%s_cgenff.out" % tempname)
          elif tpmeth == 'match':
            rval = makeMatch(tempname)
            if rval == 0:
              os.system("cp %s/build_%s.inp /tmp" % (charmming_config.dd_scripts_home, tempname))
              os.chdir("/tmp")
              os.system("%s < build_%s.inp > build_%s.out" % (charmming_config.charmm_exe, tempname, tempname))
              if "NORMAL TERMINATION BY NORMAL STOP" in open("/tmp/build_%s.out" % (tempname)).read():
                rval = 0
              else:
                rval = -1

          if rval == 0:
            success = True
            break

        if not success:
          raise AssertionError('Unable to build topology/parameters/structure file')

      # Atomtyping check end
      if success:
        newligand.owner = u
        newligand.ligand_name = request.POST['ligandname']
        newligand.ligand_owner_index = str(getNewLigandOwnerIndex(u))
        newligand.description = request.POST['liganddescription']
        newligand.source = user_source

        newligand.save()
      # if 1==1:
      # try:
        try:
          usr_base = charmming_config.user_dd_ligands_home + "/" + username + "/"  # check if we have the dir
          os.stat(usr_base)  # if not, create it
        except:
          os.mkdir(usr_base)
        location = usr_base + 'ligand_' + str(newligand.ligand_owner_index) + '/'
        filename = 'ligand_' + str(newligand.ligand_owner_index) + '_1.mol2'
        os.system("mkdir " + location)
        destination = open(location + filename, 'w')
        for fchunk in request.FILES['ligand_file'].chunks():
          destination.write(fchunk)
        destination.close()
        newfile = DDFile()
        newfile.owner = u
        newfile.file_name = filename
        newfile.file_location = location
        newfile.file_type = ligand_file_type
        newfile.description = "Structure File for Ligand %s, %s" % (newligand.ligand_name, newligand.description)
        newfile.save()

        newfileobject = DDFileObject()
        newfileobject.owner = u
        newfileobject.file = newfile
        newfileobject.object_table_name = 'dd_substrate_ligands'
        newfileobject.object_id = newligand.id
        newfileobject.save()

        psffilename = 'ligand_' + str(newligand.ligand_owner_index) + '_1.psf'
        os.system("cp /tmp/dd_user_ligand.psf %s%s" % (location, psffilename))
        newpsffile = DDFile()
        newpsffile.owner = u
        newpsffile.file_name = psffilename
        newpsffile.file_location = location
        newpsffile.file_type = psf_file_type
        newpsffile.description = "CHARMM Structure File for Ligand %s, %s" % (newligand.ligand_name, newligand.description)
        newpsffile.save()

        newpsffileobject = DDFileObject()
        newpsffileobject.owner = u
        newpsffileobject.file = newpsffile
        newpsffileobject.object_table_name = 'dd_substrate_ligands'
        newpsffileobject.object_id = newligand.id
        newpsffileobject.save()

        strfilename = 'ligand_' + str(newligand.ligand_owner_index) + '_1.str'
        os.system("cp /tmp/dd_user_ligand.str %s%s" % (location, strfilename))
        os.system("cp /tmp/dd_user_ligand.rtf %s%s" % (location, strfilename.replace("str", "rtf")))

        newstrfile = DDFile()
        newstrfile.owner = u
        newstrfile.file_name = strfilename
        newstrfile.file_location = location
        newstrfile.file_type = ligand_file_type
        newstrfile.description = "CHARMM Param Stream File for Ligand %s, %s" % (newligand.ligand_name, newligand.description)
        newstrfile.save()

        newstrfileobject = DDFileObject()
        newstrfileobject.owner = u
        newstrfileobject.file = newstrfile
        newstrfileobject.object_table_name = 'dd_substrate_ligands'
        newstrfileobject.object_id = newligand.id
        newstrfileobject.save()

        if request.POST['pdbcode'] != "":
          pose_source = DDSource()

          if len(sourceproteins) != 0:
            pose_source.source_object_table_name = "dd_target_proteins"
            pose_source.source_object_id = sourceproteins[0].id

          pose_source.description = "Protein Crystal Structure " + request.POST['pdbcode']
          pose_source.source_name = "PDB " + request.POST['pdbcode']
          # pose_source.id=str(997)
          pose_source.save()
        else:
          pose_source = user_source

        newligandpose.owner = u
        newligandpose.pose_object_table_name = 'dd_substrate_ligands'
        newligandpose.pose_object_id = newligand.id
        newligandpose.name = 'Default Pose'
        newligandpose.description = 'Initially uploaded pose'
        newligandpose.source_id = pose_source.id
        # newligandpose.id=str(998)
        newligandpose.save()

        newfileposeobject.owner = u
        newfileposeobject.file = newfile
        newfileposeobject.object_table_name = 'dd_substrate_poses'
        newfileposeobject.object_id = newligandpose.id
        newfileposeobject.save()

        return {
          'redirect': '/dd_infrastructure/dsfform'
        }
      else:
        return {
          'error': {
            'message': 'There was an error uploading your ligand. Make sure your ligand is in correct MOL2 format.'
          },
          'context': {
            'form': form
          }
        }
