from charmming.views import BaseView
from charmming.forms import QsarTestUploadForm
from charmming.utils.qsar import get_dir, handle_uploaded_file, qsar_property

from tempfile import mkstemp

import os
import subprocess

class QSARPredictView(BaseView):
  template_name = 'qsar/predict.html'

  def post(self, request, *args, **kwargs):
    active = request.POST['active']
    inactive = request.POST['inactive']
    saved_model = request.POST['saved_model']
    threshold = float(request.POST['threshold'])
    activity_property = request.POST['activity_property']
    filename = request.POST['filename']
    if 'back' in request.POST:
      return qsar_property(request, filename)
    if 'predict' in request.POST:
      form = QsarTestUploadForm(request.POST, request.FILES, active=active, inactive=inactive, saved_model=saved_model, threshold=threshold, activity_property=activity_property, filename=filename)
      if form.is_valid():
        orig_name = str(request.FILES['predictfile'].name)
        work_dir = get_dir(request)
        (fh, name) = mkstemp(dir=work_dir)
        os.close(fh)
        handle_uploaded_file(request.FILES['predictfile'], name)
        (fh, out) = mkstemp(dir=work_dir)
        os.close(fh)
        (fh, output_txt) = mkstemp(dir=work_dir)
        os.close(fh)

        # TODO: Handle this somewhere else
        subprocess.call(["python", "/var/www/charmming/qsar/run_model.py", saved_model, str(name), str(threshold), active, inactive, activity_property, str(out), output_txt])
        f = open(output_txt, "r")
        recall = float(f.readline())
        precision = float(f.readline())
        f.close()

        output_exists = os.path.isfile(out)
        return {
          'context': {
            'orig_name': orig_name,
            'output_exists': output_exists,
            'output': out,
            'recall': recall,
            'precision': precision,
            'activity_property': activity_property,
            'active': active
          }
        }
