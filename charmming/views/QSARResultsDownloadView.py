from django.http import HttpResponse

from charmming import charmming_config
from charmming.views import BaseView

import subprocess
import mimetypes
import os

class QSARResultsDownloadView(BaseView):
  def get(self, request, *args, **kwargs):
    username = request.user.username
    qsar_job_id = kwargs.get('qsar_job_id')

    # u = User.objects.get(username=username)
    qsar_folder = charmming_config.user_home + "/" + username + "/qsar"
    # qsar_folder ="/schedd/" + username + "/qsar"
    qsar_jobs_folder = qsar_folder + "/jobs"
    job_folder = qsar_jobs_folder + "/qsar_job_" + qsar_job_id

    p = subprocess.Popen("ls -1 " + job_folder + "/*-predicted.*", shell=True, stdout=subprocess.PIPE)
    pout, perr = p.communicate()
    filename = pout.rstrip()

    # if mimetype is None:
    mimetype, encoding = mimetypes.guess_type("%s" % (filename))
    try:
      os.stat("%s" % (filename))
    except:
      return HttpResponse('File does not exist.')

    response = HttpResponse(mimetype=mimetype)
    response['Content-Disposition'] = 'attachment; filename=%s' % (os.path.basename(filename))
    response.write(open(filename, "rb").read())
    return response
