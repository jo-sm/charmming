# # will allow a user to change their password
# def changeUserPassword(request):
#   User = request.user
#   # check to see if user has submitted any data
#   if(request.POST):
#   # if not, see if they are alteast logged in. If true, direct them to
#   # the password change form
#   elif(User.is_authenticated()):
#     return render_to_response('html/changepassword.html')
#   # if they are not logged in, return them to the front page
#   else:
#     return render_to_response('html/frontpage.html')

from charmming.views import BaseView
from django.contrib.auth.models import User

class ChangePasswordView(BaseView):
  template_name = 'html/changepassword.html'

  def post(self, request):
    old_user_password = request.POST.get('oldpassword')
    new_user_password = request.POST.get('newpassword')
    new_user_password_confirm = request.POST.get('newpasswordconfirm')

    if not old_user_password:
      return {
        'error': {
          'message': 'Your old password was not correct. Please go back and re-enter your password.',
          'status': 422
        }
      }
    # make sure the user supplied the correct old password first
    if(User.check_password(old_user_password)):
      # ensure the new password and the new confirmation password match
      if(new_user_password == new_user_password_confirm):
        User.set_password(new_user_password)
        User.save()
        return {
          'statuses': {
            'success': 'Password successfully changed!'
          }
        }
      else:
        return {
          'error': {
            'message': 'The two new passwords you entered did not match! Please go back and re-enter your password.',
            'status': 422
          }
        }
    else:
      return {
        'error': {
          'message': 'Your old password was not correct. Please go back and re-enter your password.',
          'status': 422
        }
      }
