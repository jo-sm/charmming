from charmming.views import BaseView
from charmming.models import DDLigand

class DDLigandsView(BaseView):
  template_name = 'html/ddviewligands.html'

  def get(self, request, *args, **kwargs):
    ligands = DDLigand.objects.filter(owner__id=request.user.id).values(
      'id',
      'ligand_owner_index',
      'ligand_name',
      'description',
      'source__source_name'
    ).order_by('ligand_name')
    return {
      'context': {
        'ligands': ligands
      }
    }
