from charmming.views import BaseView
from charmming.models import Structure

class StructuresView(BaseView):
  def get(self, request, *args, **kwargs):
    user = request.session.user
    structures = Structure.objects.filter(owner=user)

    return {
      'context': {
        'structures': structures
      }
    }
