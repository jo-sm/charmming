from charmming.views import BaseView
from charmming.utils.token import determine_instance_id_from_token
from charmming.models import User

class VerifyNotificationCookieView(BaseView):
  """
  Verifies a notification cookie, and returns account information
  if a valid cookie.
  """
  internal = True

  def get(self, request, *args, **kwargs):
    params = request.GET
    token = params.get('cookie')

    if not token:
      return {
        'error': {
          'message': 'Provide a cookie to verify.'
        }
      }

    user_id = determine_instance_id_from_token(token)

    if not user_id:
      return {
        'error': {
          'message': 'Invalid token.'
        }
      }

    try:
      user = User.objects.get(id=user_id)
    except User.DoesNotExist:
      print('user does not exist with id: {}'.format(user_id))
      return {
        'error': {
          'message': 'Invalid token.'
        }
      }

    return {
      'context': {
        'id': user.id,
        'username': user.username
      }
    }
