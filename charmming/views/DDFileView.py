from django.http import HttpResponse

from charmming.views import BaseView
from charmming.models import DDFile

import os

class DDFileView(BaseView):
  def get(self, request, *args, **kwargs):
    file_id = kwargs.get('file_id')
    file_id = file_id.replace("/", "")
    dfile = DDFile.objects.get(id=file_id)
    filename = dfile.file_location + "/" + dfile.file_name

    try:
      os.stat("%s" % (filename))
    except:
      return HttpResponse("That file doesn't seem to exist. Maybe you deleted it?")

    mimetype = "Content-Type: text/richtext"
    response = HttpResponse(mimetype=mimetype)
    response.write(open("%s" % (filename), "rb").read())
    return response
