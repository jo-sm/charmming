from django.core.mail import mail_admins

from charmming.views import BaseView

class ReportErrorView(BaseView):
  def post(self, request, *args, **kwargs):
    # input.checkRequestData(request)
    try:
      if(request.POST['errordescription'] != ''):
        emailmsg = 'Bug Reported!\n'
        emailmsg += 'User was: ' + request.user.username + '\n'
        emailmsg += 'Subject of error is: ' + request.POST['subject'] + '\n'
        emailmsg += 'Error details given by user are: \n' + request.POST['errordescription']
        mail_admins('Bug Reported', emailmsg, fail_silently=False)

        return {
          'statuses': {
            'success': 'Bug was successfully reported. Thank you for reporting this bug.'
          }
        }
      else:
        return {
          'error': {
            'message': 'You must type something into the form...'
          },
          'redirect': '/about'
        }
    except:
      return {
        'error': {
          'message': 'Bug failed to be sent properly. Please check all fields or E-mail btamiller@gmail.com.'
        },
        'redirect': '/about'
      }
