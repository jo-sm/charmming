from charmming.views import BaseView
from charmming.models import Structure, WorkingStructure, Task
from charmming.utils.working_structure import updateActionStatus

class StatusView(BaseView):
  template_name = 'html/statusreport.html'

  def get(self, request, *args, **kwargs):
    try:
      file = Structure.objects.filter(owner=request.user, selected='y')[0]
    except:
      return {
        'context': {
          'structure': None
        }
      }

    try:
      workingStruct = WorkingStructure.objects.filter(structure=file, selected='y')[0]
      # haveWorkingStruct = True
    except:
      # haveWorkingStruct = False
      workingStruct = None

    statuses = []
    if workingStruct:
      if not workingStruct.locked:
        updateActionStatus(workingStruct)
      t = Task.objects.filter(workstruct=workingStruct, active='y')
      # TODO: Modify this for multi-task support. However we don't care until tasks don't just overwrite themselves.
      foo_tasks = set()  # holds task actions so we don't repeat tasks
      for task in t:
        cankill = 0
        if task.status == 'R':
          cankill = 1
        if task.action not in foo_tasks:
          foo_tasks = foo_tasks.union(set([task.action]))
          statuses.append((task.action, task.get_status_display(), cankill, task.id))

    return {
      'context': {
        'structure': file,
        'ws': workingStruct,
        'haveWorkingStruct': True,
        'statuses': statuses
      }
    }
