from charmming import charmming_config
from charmming.views import BaseView
from charmming.helpers.views import Accountinator

class JmolView(BaseView):
  template_name = 'html/jmol.html'

  @Accountinator()
  def get(self, request, filename, *args, **kwargs):
    # Lets user view PDB through jsmol - same functions and scripting
    # NOTE: the below kwargs come in from Accountinator
    cg_model = kwargs.get("cg_model")
    struct = kwargs.get("struct")

    if len(filename) < 4:
      return {
        'error': {
          'message': "Bad filename. Please verify that the file you are trying to open exists before accessing this page."
        },
        'redirect': '/visualize'
      }

    filename = struct.location.replace(charmming_config.user_home, '') + '/' + filename
    filename_end = filename.rsplit('/', 1)[1]  # i.e., "a-pro-5.pdb", etc.
    super_user = request.user.is_superuser
    structname = filename_end.split('.', 1)[0]  # a-pro-5
    try:
      isProtein = not("-good-" in filename_end or "-bad-" in filename_end or filename_end[3] == "-")
    except:
      return {
        'error': {
          'message': 'Bad filename. Please verify that the file you are trying to open exists before accessing this page.'
        },
        'redirect': '/visualize'
      }
    # Assume everything that isn't a good/bad hetatm segment to be a protein for the purposes of rendering, that way we deal with mutations
    # Also assume anything that has 3 letters followed by a dash at the start to be a ligand file.
    return {
      'context': {
        'structname': structname,
        'filepath': filename,
        'segid': 'NA',
        'resid': 'NA',
        'isProtein': isProtein,
        'super_user': super_user,
        'cg_model': cg_model
      }
    }
