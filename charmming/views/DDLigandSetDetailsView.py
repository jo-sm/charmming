from django.db.models import Count
from django.contrib.auth.models import User

from charmming.views import BaseView
from charmming.models import DDLigandSetsLigands, DDLigand
from charmming.utils.dd_substrate import LigandSetObj

class DDLigandSetDetailsView(BaseView):
  template_name = 'html/ddligandsetdetails.html'

  def get(self, request, *args, **kwargs):
    ligandset_id = kwargs.get('ligandset_id')

    public_user = User.objects.get(username='public')

    rows = DDLigandSetsLigands.objects.filter(
      ligand_sets__id__in=[request.user.id, public_user.id]
    ).exclude(
      ligand_sets__ligand_set_name='All Available',
      ligand_sets__id=ligandset_id
    ).annotate(ligandcount=Count('ligands__id', distinct=True)).values('ligand_sets__id', 'ligand_sets__ligand_set_name', 'ligandcount')

    all_count = DDLigand.objects.filter(owner__id__in=[request.user.id, public_user.id]).annotate(ligandcount=Count('id', distinct=True)).values('ligandcount')
    sets = []

    sets = [LigandSetObj(
      id=row.ligand_sets__id,
      name=row.ligand_sets__ligand_set_name,
      ligand_count=row.ligandcount
    ) for row in rows]

    all_set = LigandSetObj(id=0, name='All Available', ligand_count=all_count.ligandcount)

    sets = [ all_set ] + sets
    set_id = ligandset_id.replace("/", "")

    return {
      'context': {
        'existingsets': sets,
        'set_id': set_id
      }
    }
