from charmming.views import BaseView
from charmming.models import Structure, LessonProblem, Lesson3

class Lesson3View(BaseView):
  template_name = 'html/lesson3.html'

  def get(self, request, *args, **kwargs):
    try:
      file = Structure.objects.filter(owner=request.user, selected='y')[0]
    except:
      return {}

    if file.lesson_type == 'lesson3':
      lesson_obj = Lesson3.objects.filter(user=request.user, id=file.lesson_id)[0]
      html_step_list = lesson_obj.getHtmlStepList()
    else:
      lesson_obj = None
      html_step_list = None

    try:
      lessonprob_obj = LessonProblem.objects.filter(lesson_type='lesson3', lesson_id=lesson_obj.id)[0]
    except:
      lessonprob_obj = None

    return {
      'context': {
        'lesson3': lesson_obj,
        'lessonproblem': lessonprob_obj,
        'html_step_list': html_step_list
      }
    }
