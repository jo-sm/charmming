from django.db.models import Q, Count
from django.db.models.query import QuerySet
from django.contrib.auth.models import User

from charmming import charmming_config
from charmming.views import BaseView
from charmming.models import Structure, WorkingStructure, Segment, Task, DDBindingSiteWorkingStructure, DDProject, DDProjectsProteinConformations, DDFileObject, DDJob, DDJobType, DDLigand, DDFile, DDDSFDockingTask, DDLigandSet
from charmming.utils.dd_infrastructure import objFile, getNewJobOwnerIndex
from charmming.utils.dd_target import TargetConformationInfo
from charmming.utils.dd_substrate import LigandSetObj
from charmming.utils.working_structure import build
from charmming.utils.task import start
from charmming.utils.lesson import get_lesson_object, doLessonAct

from functools import reduce

import os
import glob
import shutil
import datetime

class DDDSFFormView(BaseView):
  template_name = 'html/dddsfform.html'

  def get(self, request, *args, **kwargs):
    try:
      struct = Structure.objects.get(owner=request.user, selected='y')
    except:
      return {
        'error': {
          'message': 'Please submit a structure first.'
        },
        'redirect': '/fileupload'
      }

    try:
      ws = WorkingStructure.objects.get(structure=struct, selected='y')
    except:
      return {
        'error': {
          'message': "Please build a working structure first."
        },
        'redirect': '/buildstruct'
      }

    # try:
    abadfiles = []
    os.chdir(struct.location)
    # start = ""
    # end = ""
    nativefilename = ""
    os.system("rm *-native-*.pdb")
    for file in glob.glob(struct.location + '/*-bad*.pdb'):
      if "badres" not in file and "segment" not in file:
        os.system("awk '{if($4!=$res && $1==\"ATOM\") {res=$4; print >> \"%s-native-\"res\".pdb\";}}' %s" % (os.path.basename(file)[0:1], os.path.basename(file)))
    # first let's try and get native binding sites.
    for native_lig_file in glob.glob(struct.location + '/*-native-*.pdb'):
      nativefilename = os.path.basename(native_lig_file)
      abadfile = objFile()
      abadfile.fullpath = native_lig_file  # This is not used ever after, so we add a field for our purposes (display, JSmol)
      abadfile.jsmolpath = abadfile.fullpath.replace("/home/schedd/", "/charmming/pdbuploads/")
      abadfile.name = nativefilename
      abadfile.tag = nativefilename[0 + len("a-native-"):nativefilename.index(".pdb", 0 + len("a-native-"))]
      # abadfile.tag=nativefilename[nativefilename.index("a-native-")+len("a-native-"):nativefilename.index(".pdb",nativefilename.index("a-native-")+len("a-native-"))]
      abadfiles.append(abadfile)
    # ok, now check DB for non-native binding sites
    bsites = DDBindingSiteWorkingStructure.objects.filter(workstruct=ws)
    bs_files = []
    if len(bsites) > 0:
      for site in bsites:
        bs_files.append("%s-%s.bs" % (ws.identifier, site.binding_site.binding_site_name))

    try:
      project = DDProject.objects.filter(owner=request.user, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'Please create a drug design project first.'
        }
      }
    project_conformations = DDProjectsProteinConformations.objects.filter(owner=request.user, project=project).select_related()

    conformation_info_list = [TargetConformationInfo(
      id=p.protein_conformation.id,
      name=p.protein_conformation.confomation_name,
      description=p.protein_conformation.description,
      target_pdb_code=p.protein_conformation.protein.pdb_code,
      file_objects_list=DDFileObject.objects.filter(owner=request.user, object_table_name="dd_target_protein_conformations", object_id=p.protein_conformation.id).select_related()
    ) for p in project_conformations]

    public_user = User.objects.get(username='public')
    ligand_sets = DDLigandSet.objects.filter(
      owner__id__in=[request.user.id, public_user.id]
    ).exclude(ligand_set_name='All Available').values('id', 'ligand_set_name')
    ligands_count_q = DDLigand.objects.filter(owner__id__in=[request.user.id, public_user.id]).annotate(count=Count('id')).values('count', 'owner_id').query
    ligands_count_q.group_by = ['owner_id'] # Group by isn't supported in Django ORM natively
    ligands_count = QuerySet(query=ligands_count_q, model=DDLigand)

    user_uploaded_set = LigandSetObj(
      id='00',
      name='All User Uploaded',
      ligand_count=next(o for o in ligands_count if o['owner_id'] == request.user.id)
    )

    all_available_set = LigandSetObj(
      id='0',
      name='All Available',
      ligand_count=reduce(lambda memo, i: memo + i['count'], 0)
    )

    sets = [ user_uploaded_set, all_available_set] + [LigandSetObj(
      id=l.id,
      name=l.ligand_set_name,

      # This used to be the following SQL:
      # select count(distinct ligands_id) from dd_substrate_ligand_sets_ligands where ligands_set_id=ls.id and owner_id in (%s,%s)
      # But this SQL isn't currently used as the tables are empty, so the ligand count in all cases except below
      # are 0
      ligand_count=0
    ) for l in ligand_sets]

    tasks = Task.objects.filter(workstruct=ws, status='C', active='y', modifies_coordinates=True)
    return{
      'context': {
        'abadfiles': abadfiles,
        'ws_identifier': ws.identifier,
        'tasks': tasks,
        'conformations': conformation_info_list,
        'existingsets': sets,
        'binding_sites': bs_files
      }
    }

  def post(self, request, *args, **kwargs):
    message = ""
    username = request.user.username
    u = User.objects.get(username=username)

    if "native_ligand" in request.POST:
      nligand = request.POST['native_ligand']
    else:
      return {
        'error': {
          'message': 'This molecule has no binding sites associated to it, or you did not select one. Please define a binding site before performing a docking job.'
        },
        'redirect': '/dd_infrastructure/bindingsite'
      }

    # this block contains the actions taking place when the form gets submitted
    # i.e. the job gets launched
    tar_files = []
    job_owner_id = getNewJobOwnerIndex(u)
    job_basename = 'dd_job_' + str(job_owner_id)
    job_folder = charmming_config.user_dd_jobs_home + '/' + username + '/' + job_basename
    os.system("mkdir %s" % (charmming_config.user_dd_jobs_home + '/' + username + '/'))
    os.system("mkdir %s" % (job_folder))
    os.system("chmod -R g+w %s" % (job_folder))
    os.chdir(job_folder)
    os.system("touch %s/ligands" % (job_folder))
    os.system("mkdir %s/Inputs" % (job_folder))
    os.system("chmod -R g+w %s/Inputs" % (job_folder))
    os.system("chown schedd %s/Inputs" % (job_folder))
    os.system("mkdir %s/Results" % (job_folder))
    os.system("chmod -R g+w %s/Results" % (job_folder))
    os.system("chown schedd %s/Results" % (job_folder))
    os.system("mkdir %s/ligands_to_dock" % (job_folder))
    os.system("chmod -R g+w %s/ligands_to_dock" % (job_folder))
    os.system("chown schedd %s/ligands_to_dock" % (job_folder))
    os.system("cp %s/charmm_prot_convert.sh %s\n" % (charmming_config.dd_scripts_home, job_folder))
    os.system('cp %s/cgenff_convert.sh %s\n' % (charmming_config.dd_scripts_home, job_folder))
    os.system('cp %s/targetprep.sh %s\n' % (charmming_config.dd_scripts_home, job_folder))
    os.system('cp %s/typeassign.py %s\n' % (charmming_config.dd_scripts_home, job_folder))
    os.system('cp %s/ffld_paramconvert.pl %s\n' % (charmming_config.dd_scripts_home, job_folder))
    os.system('cp %s/cgenff.sh %s\n' % (charmming_config.dd_scripts_home, job_folder))
    os.system('cp %s/cgenff_lig.sh %s\n' % (charmming_config.dd_scripts_home, job_folder))
    os.system('cp %s/add_substructure.sh %s\n' % (charmming_config.dd_scripts_home, job_folder))
    os.system('cp %s/add_lig_substructure.sh %s\n' % (charmming_config.dd_scripts_home, job_folder))
    os.system('cp %s/Run_FFLD_FLEA_yp.sh %s\n' % (charmming_config.dd_scripts_home, job_folder))
    os.system('cp %s/num_substructures.pl %s\n' % (charmming_config.dd_scripts_home, job_folder))
    os.system('cp %s/pdb2mol.in %s\n' % (charmming_config.dd_scripts_home, job_folder))
    os.system('cp %s/run.sh %s\n' % (charmming_config.dd_scripts_home, job_folder))
    # os.system("cp %s/dodsfc.sh %s" % (charmming_config.dd_scripts_home,job_folder))
    os.system('cp %s/split_seed_input.sh %s\n' % (charmming_config.dd_scripts_home, job_folder))
    os.system('cp %s/run_seed.sh %s\n' % (charmming_config.dd_scripts_home, job_folder))
    os.system('cp %s/dock_and_score.sh %s\n' % (charmming_config.dd_scripts_home, job_folder))
    os.system('cp %s/submit_dock_and_score.sh %s\n' % (charmming_config.dd_scripts_home, job_folder))
    os.system('cp %s/submit_dock_iteration.sh %s\n' % (charmming_config.dd_scripts_home, job_folder))

    # charmm stuff
    os.system("mkdir %s/charmm_mini" % (job_folder))
    os.system("mkdir %s/charmm_mini/lig" % (job_folder))
    os.system("mkdir %s/charmm_mini/combined" % (job_folder))
    os.system("mkdir %s/charmm_mini/target" % (job_folder))
    os.system("mkdir %s/charmm_mini/poses" % (job_folder))
    os.system("mkdir %s/charmm_mini/minimized" % (job_folder))
    os.system("cp %s* %s/charmm_mini/" % (charmming_config.charmm_files, job_folder))
    os.system("cp %s/*.inp %s/charmm_mini/" % (charmming_config.dd_scripts_home, job_folder))
    os.system("cp %s/mol2crd %s/charmm_mini/" % (charmming_config.dd_scripts_home, job_folder))
    os.system("cp %s/run_mini_ligand.sh %s/charmm_mini/" % (charmming_config.dd_scripts_home, job_folder))
    os.system("cp %s/pdb2mol.sh  %s/charmm_mini/" % (charmming_config.dd_scripts_home, job_folder))

    # end charmm stuff

    # ffld scoring stuff
    os.system("mkdir %s/ffld_eval" % (job_folder))
    os.system("mkdir %s/ffld_eval/poses" % (job_folder))
    os.system("mkdir %s/ffld_eval/ligands" % (job_folder))
    os.system("cp %s/run_ffld_eval_ligand.sh  %s/ffld_eval/\n" % (charmming_config.dd_scripts_home, job_folder))
    ####

    # seed scoring stuff
    os.system("mkdir %s/seed_eval" % (job_folder))
    os.system("mkdir %s/seed_eval/poses" % (job_folder))
    os.system("cp %s/run_seed_eval_ligand.sh  %s/seed_eval/\n" % (charmming_config.dd_scripts_home, job_folder))
    os.system("cp %s/make_seed_inp_ligand.sh  %s/seed_eval/\n" % (charmming_config.dd_scripts_home, job_folder))
    ####

    os.system("chmod -R g+w %s" % (job_folder))
    os.system("chmod -R g+x %s" % (job_folder))
    # copy native ligand into the job folder
    # native_ligand_destination = open(job_folder + "/native_ligand.pdb", 'w')
    try:
      struct = Structure.objects.get(owner=request.user, selected='y')
    except:
      return {
        'error': {
          'message': 'Please submit a structure first.'
        },
        'redirect': '/fileupload'
      }
    if nligand.endswith(".bs"):
      os.system('cp %s/%s %s/' % (struct.location, nligand, job_folder))
      sed_cmd = "sed -i 's/--native native_ligand\.mol2/--bsfile %s/' %s/run.sh" % (nligand, job_folder)
      os.system(sed_cmd)
    filename = struct.location + '/' + nligand
    shutil.copy2(filename, job_folder + "/native_ligand.pdb")

    # for fchunk in request.FILES['native_ligand_file'].chunks():
    # for fchunk in request.POST['native_ligand'].chunks():
    # native_ligand_destination.write(fchunk)

    daim_ligands_list_file = open(job_folder + "/ligands", 'w')

    public_user = User.objects.get(username='public')
    user_ligands = DDLigand.objects.filter(Q(owner=request.user) | Q(owner=public_user))
    user_ligand_ids = []
    # wnp_ligprep_string = ""
    for user_ligand in user_ligands:
      user_ligand_ids.append(user_ligand.id)

    ligandcount = 0
    ligandfile_list = []
    try:
      user_ligand_file_objects = DDFileObject.objects.filter(Q(owner=request.user) | Q(owner=public_user), object_table_name='dd_substrate_ligands', object_id__in=user_ligand_ids)
      job_ligand_file_ids = []
      lesson_lig_filename = ""
      for user_ligand_file_object in user_ligand_file_objects:
        try:
          if (request.POST["ligand_" + str(user_ligand_file_object.file_id)] == 'on'):
            ligand_file = DDFile.objects.get(id=user_ligand_file_object.file_id)
            job_ligand_file_ids.append(ligand_file.id)
            os.system("cp %s%s %s/%s_%s" % (ligand_file.file_location, ligand_file.file_name, job_folder, ligand_file.owner_id, ligand_file.file_name))
            strfile = ("%s%s" % (ligand_file.file_location, ligand_file.file_name.replace(".mol2", ".str")))
            psffile = ("%s%s" % (ligand_file.file_location, ligand_file.file_name.replace(".mol2", ".psf")))
            rtffile = ("%s%s" % (ligand_file.file_location, ligand_file.file_name.replace(".mol2", ".rtf")))
            if (os.path.isfile(strfile)):
              os.system("cp %s %s/%s_%s" % (strfile, job_folder, ligand_file.owner_id, ligand_file.file_name.replace(".mol2", ".str")))
              os.system("cp %s %s/%s_%s" % (rtffile, job_folder, ligand_file.owner_id, ligand_file.file_name.replace(".mol2", ".rtf")))
              os.system("cp %s %s/charmm_mini/lig/%s_%s" % (strfile, job_folder, ligand_file.owner_id, ligand_file.file_name.replace(".mol2", ".str")))
              os.system("cp %s %s/charmm_mini/lig/%s_%s" % (psffile, job_folder, ligand_file.owner_id, ligand_file.file_name.replace(".mol2", ".psf")))

            ligandcount = ligandcount + 1
            if ligandcount > 1:
              lesson_lig_filename = ""
            else:
              lesson_lig_filename = "%s/%s_%s" % (job_folder, ligand_file.owner_id, ligand_file.file_name)
            ligandfile_list.append(str(ligand_file.owner_id) + "_" + ligand_file.file_name)
            daim_ligands_list_file.write(str(ligand_file.owner_id) + "_" + ligand_file.file_name + '\n')
        except:
          pass
    except:
      pass

    daim_ligands_list_file.close()
    settings_file = open(job_folder + "/settings.cfg", 'w')
    settings_file.write("#! /bin/bash\n")

    ##################
    # CHARMMING integrated target

    try:
      struct = Structure.objects.filter(owner=request.user, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'Please submit a structure first'
        },
        'redirect': '/fileupload'
      }
    try:
      ws = WorkingStructure.objects.filter(structure=struct, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'Please build a working structure first'
        },
        'redirect': '/buildstruct'
      }

    try:
      oldtsk = DDDSFDockingTask.objects.filter(workstruct=ws, active='y')[0]
      oldtsk.active = 'n'
      oldtsk.save()
    except:
      pass

    dsftask = DDDSFDockingTask()
    dsftask.setup(ws)
    dsftask.active = 'y'
    dsftask.action = 'dsfdocking'
    dsftask.modifies_coordinates = False
    dsftask.save()

    if ws.isBuilt != 't':
      # isBuilt = False
      pTask = build(ws, dsftask)
      pTaskID = pTask.id
    else:
      # isBuilt = True
      pTaskID = int(request.POST['ptask'])

    pTask = Task.objects.get(id=pTaskID)
    # struct=Structure.objects.get(id=t.id)

    pro_segment = Segment.objects.filter(structure=dsftask.workstruct.structure, type="pro", is_working="y")[0]  # filter[0] is on purpose
    pro_name = pro_segment.name + '-' + str(pro_segment.id)
    # pro_name=dsftask.workstruct.identifier + '-' + pTask.action

    settings_file.write('protein="%s"\n' % (pro_name))
    settings_file.write('vmd_exe="%s"\n' % (charmming_config.vmd_exe))
    settings_file.write('daim_exe="%s"\n' % (charmming_config.daim_exe))
    settings_file.write('seed_exe="%s"\n' % (charmming_config.seed_exe))
    settings_file.write('ffld_exe="%s"\n' % (charmming_config.ffld_exe))
    settings_file.write('flea_exe="%s"\n' % (charmming_config.flea_exe))
    settings_file.write('charmm_exe="%s"\n' % (charmming_config.charmm_exe))
    settings_file.write('submit="%s" #serial execution of docking commands\n' % (charmming_config.dd_submit_serial))
    settings_file.write('#submit="%s" #parallel execution of docking commands\n' % (charmming_config.dd_submit_parallel))
    settings_file.write('docking_iterations=%s\n' % (charmming_config.docking_iterations))
    settings_file.write('clustering_energy_cutoff=%s\n' % (charmming_config.clustering_energy_cutoff))
    settings_file.write('num_energy_evaluations=%s\n' % (charmming_config.ffld_energy_evaluations))
    settings_file.write('num_generations=%s\n' % (charmming_config.ffld_generations))
    settings_file.close()

    os.system("cp %s/%s.pdb %s" % (dsftask.workstruct.structure.location, pro_name, job_folder))
    os.system("cp %s/%s.psf %s" % (dsftask.workstruct.structure.location, pro_name, job_folder))
    os.system("cp %s/%s.crd %s" % (dsftask.workstruct.structure.location, pro_name, job_folder))
    os.system("cp %s/%s.psf %s/charmm_mini/target/" % (dsftask.workstruct.structure.location, pro_name, job_folder))
    os.system("cp %s/%s.crd %s/charmm_mini/target" % (dsftask.workstruct.structure.location, pro_name, job_folder))

    # end Charmming integrated target
    #################

    os.system("cp %s %s\n" % (charmming_config.daim_param, job_folder))
    os.system("cp %s %s\n" % (charmming_config.daim_prop, job_folder))
    os.system("cp %s %s/FFLD_param_cgenff_base" % (charmming_config.ffld_param, job_folder))
    os.system("cp %s %s/Inputs/seed.par\n" % (charmming_config.seed_param, job_folder))
    os.system("cp %s %s\n" % (charmming_config.flea_param, job_folder))
    os.system("cp %s %s/\n" % (charmming_config.charmm_param, job_folder))

    os.system("chmod g+w -R %s" % (job_folder))

    scriptlist = []
    os.system("touch %s/run.inp" % (job_folder))
    scriptlist.append(job_folder + "/run.inp")
    # exedict={job_folder + "/run.inp": 'bash'}
    exedict = {job_folder + "/run.inp": job_folder + '/run.sh'}
    # exedict={"": "bash " + job_folder + "/run.inp >& " + job_folder + "/run.out"}
    # uncomment this block to turn on job launching

    # Job launching code
    # si = schedInterface()
    # newSchedulerJobID = si.submitJob(request.user.id,job_folder,scriptlist,exedict)

    NewJob = DDJob()
    NewJob.owner = request.user
    NewJob.job_owner_index = job_owner_id
    NewJob.description = request.POST['job_description']
    NewJob.job_start_time = datetime.datetime.now()
    jobtype = DDJobType.objects.get(job_type_name='DAIM/SEED/FFLD Docking')
    NewJob.job_type = jobtype
    NewJob.save()

    # Create downloadable job folder
    os.chdir(charmming_config.user_dd_jobs_home + '/' + username)
    os.system("tar -zcvf %s.tar.gz %s" % (job_folder, job_basename))
    # create associate tar file with the job
    tar_file = DDFile()
    tar_file.owner = u
    tar_file.file_name = job_basename + ".tar.gz"
    tar_file.file_location = charmming_config.user_dd_jobs_home + '/' + username
    tar_file.description = "Archived Downloadable Drug Design Job " + str(job_owner_id)
    tar_file.save()

    tarfile = objFile()
    tarfile.fullpath = tar_file.file_location
    tarfile.name = tar_file.file_name
    tarfile.id = tar_file.id
    # tarfile.tag=nativefilename[0+len("a-native-"):nativefilename.index(".pdb",0+len("a-native-"))]
    tar_files.append(tarfile)

    new_job_tar_file_object = DDFileObject()
    new_job_tar_file_object.owner = u
    new_job_tar_file_object.file_id = tar_file.id
    new_job_tar_file_object.object_table_name = 'dd_infrastructure_jobs'
    new_job_tar_file_object.object_id = NewJob.id
    new_job_tar_file_object.save()
    # End of downloadable job folder creation

    # CHARMMING integrated association of job with task of the target structure
    dsftask.parent = pTask
    start(dsftask, job_folder, scriptlist, exedict)
    dsftask.save()
    dsftask.workstruct.save()

    NewJob.job_scheduler_id = dsftask.jobID
    NewJob.save()

    new_job = DDJob.objects.filter(owner=u).order_by("-id")[0]

    ######

    # associate ligand files with this job in the database
    for job_ligand_file_id in job_ligand_file_ids:
      new_job_ligand_file_object = DDFileObject()
      new_job_ligand_file_object.owner = u
      new_job_ligand_file_object.file_id = job_ligand_file_id
      new_job_ligand_file_object.object_table_name = 'dd_infrastructure_jobs'
      new_job_ligand_file_object.object_id = new_job.id
      new_job_ligand_file_object.save()

    # create associate submitscript.out file with the job
    output_file = DDFile()
    output_file.owner = u
    output_file.file_name = "run.out"
    output_file.file_location = job_folder
    output_file.description = "Job execution output for job dd_job_" + str(job_owner_id)
    output_file.save()

    new_output_file = DDFile.objects.filter(owner=u).order_by("-id")[0]

    new_job_output_file_object = DDFileObject()
    new_job_output_file_object.owner = u
    new_job_output_file_object.file_id = new_output_file.id
    new_job_output_file_object.object_table_name = 'dd_infrastructure_jobs'
    new_job_output_file_object.object_id = new_job.id
    new_job_output_file_object.save()
    # End of job launching code

    message = "New DAIM/SEED/FFLD Docking job (Job Code: dd_job_%s)\n was successuflly submitted! Use the link below to download the job files\n" % (job_owner_id)
    message_color = 'green'

    lesson_obj = get_lesson_object(struct.lesson_type)
    if lesson_obj:
      try:
        doLessonAct(struct, "onDockingSubmit", filename=lesson_lig_filename, task=dsftask)
      except:
        pass

    conformation_info_list = []
    project = DDProject.objects.filter(owner=request.user, selected='y')[0]
    project_conformations = DDProjectsProteinConformations.objects.filter(owner=request.user, project=project).select_related()
    for project_conformation in project_conformations:
      conformation_info = TargetConformationInfo()
      conformation_info.id = project_conformation.protein_conformation.id
      conformation_info.name = project_conformation.protein_conformation.conformation_name
      conformation_info.description = project_conformation.protein_conformation.description
      conformation_info.target_pdb_code = project_conformation.protein_conformation.protein.pdb_code
      conformation_info.file_objects_list = DDFileObject.objects.filter(owner=request.user, object_table_name="dd_target_protein_conformations", object_id=project_conformation.protein_conformation.id).select_related()
      conformation_info_list.append(conformation_info)

    return {
      'context': {
        'conformations': conformation_info_list,
        'message': message,
        'message_color': message_color,
        'tarfiles': tar_files
       }
    }
