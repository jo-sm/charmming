from charmming.views import BaseView
from charmming.models import Structure, WorkingStructure
from charmming.utils.serialize import remove_django, django_to_dict
from charmming.utils.atom_selection import getAtomSelections

class QMMMVarsView(BaseView):
  def get(self, request, *args, **kwargs):
    """
    This function returns variables for the page as JSON object, so we don't have to be
    templating variables and the like.
    Note that this function SHOULD NOT USE Accountinator - this function is intended
    as a JSON API, and should not perform the types of redirects that Accountinator does.
    """
    try:
      struct = Structure.objects.filter(owner=request.user, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'Could not find structure.'
        }
      }

    try:
      ws = WorkingStructure.objects.filter(structure=struct, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'Could not find working structure.'
        }
      }

    sl = []
    sl.extend(ws.segments.all())
    new_segl = sorted([x.name for x in sl])
    tdict = {}
    tdict = getAtomSelections(tdict, ws)
    if len(tdict['lonepairs']) > 0:
      tdict['lonepairs'] = django_to_dict(tdict['lonepairs'])
    else:
      tdict['lonepairs'] = []
    if 'atomselection' in tdict and tdict['atomselection'] is not None:
      tdict['old_task'] = tdict['atomselection'].task.action  # get task to set coordinates stuff
      tdict['atomselection'] = [remove_django(tdict['atomselection'])]
    elif 'oniom_selections' in tdict:
      tdict['old_task'] = tdict['oniom_selections'][0].task.action  # get task to set coordinates stuff
      tdict['total_layers'] = tdict['oniom_selections'][0].total_layers
      tdict['oniom_selections'] = django_to_dict(tdict['oniom_selections'])
    tdict['seg_list'] = new_segl

    return {
      'context': tdict
    }
