from charmming import charmming_config
from charmming.views import BaseView
from charmming.models import Structure, WorkingStructure, Task
from charmming.utils.atom_selection import saveAtomSelections, validateInputs

from pychm3.io.pdb import PDB

import json
import pickle

class SelectStructureView(BaseView):
  template_name = 'html/selection.html'
  no_get = True

  def post(self, request, *args, **kwargs):
    postdata = request.POST
    source = postdata['source']
    highest_qm_layer = 1  # TODO: This should be changed for MM/MM.
    if 'model_selected' in postdata:  # If it doesn't have it going in from the energy/minim/whatever page...
      if postdata['model_selected'] not in ['qmmm', 'oniom', 'janus']:
        return {
          'error': {
            'message': 'Please use our graphical interface to perform atom selections. If you are seeing this message in error, please report it.'
          },
          'redirect': '/main'
        }
      else:
        modelType = postdata['model_selected']
      try:
        layers = int(postdata['oniom_layers'])
      except:
        return {
          'error': {
            'message': 'Invalid number of layers.'
          },
          'redirect': '/'
        }
      if modelType == "qmmm" or source == "qchem":  # modify source for further modifications
        layers = 1
      else:
        highest_qm_layer = layers - 1
        for i in range(2, layers):
          if 'mm_box_layer_' + str(i) in postdata:
            highest_qm_layer = i - 1
    elif 'modelType' in postdata:  # Then we're in the selection page going back.
      if postdata['modelType'] not in ['qmmm', 'oniom', 'janus']:
        return {
          'error': {
            'message': 'Invalid model type. Please use our graphical interface to perform atom selections. If you are seeing this message in error, please report it.'
          },
          'redirect': '/'
        }
      else:
        modelType = postdata['modelType']
      try:
        layers = int(postdata['layers'])
      except:
        return {
          'error': {
            'message': 'Invalid number of layers.'
          },
          'redirect': '/'
        }
    # If your modelType isn't oniom, don't worry about the layers.
    dest = source + "/"  # Yes this is a bad naming convention. However, it works.
    try:
      struct = Structure.objects.filter(owner=request.user, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'No structure selected. Please upload a structure.'
        },
        'redirect': '/fileupload'
      }

    try:
      ws = WorkingStructure.objects.filter(structure=struct, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'Please build a working structure.'
        },
        'redirect': '/buildstruct'
      }

    try:
      task_id = postdata['task_id']
    except:
      return {
        'error': {
          'message': 'No tasks present. Please perform a calculation on your working structure before doing QM/MM selection.'
        },
        'redirect': '/energy'
      }

    if ws.isBuilt == 'f':
      return {
        'error': {
          'message': 'Your working structure has not been built. Please perform a calculation on it before doing QM/MM selection.'
        },
        'redirect': '/energy'
      }
    try:
      task = Task.objects.filter(workstruct=ws, id=task_id)[0]  # This might break with build tasks?
    except:
      # This means something went REALLY wrong in the database
      return {
        'error': {
          'message': "Task {} does not exist. Please select a task from which to draw coordinates.".format(task_id)
        },
        'redirect': '/energy'
      }

    tdict = {}
    # Remember to validate input BEFORE going in to the rest.
    filepath = struct.location.replace(charmming_config.user_home, '') + "/" + ws.identifier + "-" + task.action + ".pdb"
    # for example 1yjp-ws1-build.pdb
    tdict['modelType'] = modelType
    tdict['layers'] = layers
    tdict['highest_qm_layer'] = highest_qm_layer
    tdict['filepath'] = filepath
    tdict['source'] = source
    tdict['task_id'] = task_id
    tdict['starting_layer'] = layers - 1  # This is not an issue with QM/MM since we override.
    # The following will all be stored in hidden fields and passed back and forth. We need to put checks on the receiving end (i.e. either the Python or the JS for qmmm_params)
    # such that no script injection can occur
    # It's safe to assume all these fields exist since they must by default
    if not ('atomselection' in postdata or 'atomselection_layer_1' in postdata):  # Make sure we're coming in from the right page.
      if layers > 1:  # layers >1 => ONIOM model. This MAY require changing later if implementing more models.
        layer_values = []  # Hack because django doesn't take variables within variables, like {{exchange_layer{{layer}}}}
        for i in range(1, layers):  # Since you don't care about the final layer...
          current_layer = []
          current_layer.append(i)
          current_layer.append(postdata['qmmm_exchange_layer_' + str(i)])
          current_layer.append(postdata['qmmm_charge_layer_' + str(i)])
          current_layer.append(postdata['qmmm_correlation_layer_' + str(i)])
          current_layer.append(postdata['qmmm_basisset_layer_' + str(i)])
          current_layer.append(postdata['qmmm_multiplicity_layer_' + str(i)])
          layer_values.append(current_layer)  # Therefore layer 1 is stored in 0, but contains the number, so we can template easily.
          # Assume user does not input a text selection, but also make sure these values don't change.
          # TODO: Make sure that when the form is submitted for any QM/MM calc that these values are checked and updated on the AtomSelection/OniomSelection.
        tdict['layer_values'] = layer_values
      else:
        add_string = "qmmm_" if source != "qchem" else ""
        tdict['exchange'] = postdata[add_string + 'exchange']
        tdict['charge'] = postdata[add_string + 'charge']
        tdict['correlation'] = postdata[add_string + 'correlation']
        tdict['basis_set'] = postdata[add_string + 'basisset']
        tdict['multiplicity'] = postdata[add_string + 'multiplicity']
        if add_string + "forcefield" in postdata:
          tdict['forcefield'] = postdata[add_string + "forcefield"]
    else:
      validate_inputs_result = validateInputs(tdict, postdata, layers)
      if validate_inputs_result != "Passed":
        return {
          'error': {
            'message': validate_inputs_result
          },
          'redirect': '/{}'.format(dest)
        }
      error_message = saveAtomSelections(request, ws, task)
      try:
        int(error_message)
      except:  # If it's not an int, we have a problem
        return {
          'error': {
            'message': error_message
          },
          'redirect': '/{}'.format(dest)
        }
      return {
        'redirect': '/{}'.format(dest)
      }
      # Do stuff with the database and return redirect to source
    # TODO: Consider rewriting the above behemoth into a generic method like saveAtomSelections in etc.atomselection_aux.py
    # This code is applied when there is NOT an atomselection active.
    if ws.localpickle:
      pdb = open(ws.localpickle)
      pdbfile = pickle.load(pdb)
      pdb.close()
    else:
      pdbfile = PDB(struct.location + "/" + ws.identifier + "-" + task.action + ".pdb")
    taco = pdbfile['model00']
    # Important note: this does NOT work the same as in mutation. Since the segment iteration
    # Goes by alphabet rather than by atom number order, this becomes a mess. So we
    # Get the segment objects in the list, then sort by their "first atom" number. HOpefully
    # The fact that we make a new generator object means we don't repeat ourselves and thus
    # have issues with single-residue things.
    segmentlist = []  # Holds the segment names
    chain_terminators = []  # Holds the "chain terminator". We pair them up in different lists because we need to preserve order
    for seg in taco.iter_seg():  # segments are in alphabetical order...
      atom = seg.iter_res().next().iter_atom().next().atomNum0  # Gets the original atom number of the first atom in the first residue of this segment
      segmentlist.append(seg)
      chain_terminators.append(atom)
    # The next line makes it so that the segments are sorted by the atom number of their first residue.
    # While we do get this same thing while iterating, the segments are in alphabetical order rather than in order by atomic number.
    # Therefore, we need to sort them by the number of that atom once they're done, but we don't really "know" the order until the end anyway.
    # Thus, the sort below.
    segmentlist = sorted(segmentlist, key=lambda x: x.iter_res().next().iter_atom().next().atomNum0)  # This is to get the thing properly sorted...
    segmentlist = [x.segid for x in segmentlist]  # This is to get it back to names...
    chain_terminators = sorted(chain_terminators)
    tdict['chain_terminators'] = json.dumps(chain_terminators)
    tdict['segmentlist'] = json.dumps(segmentlist)

    print(tdict)

    return {
      'context': tdict
    }
