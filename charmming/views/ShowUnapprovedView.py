from django.contrib.auth.models import User, Group
from django.core.mail import send_mail

from charmming.views import BaseView
from charmming.models import StudentProfile

class ShowUnapprovedView(BaseView):
  template_name = 'admin/unapproveduser.html'

  def get(self, request, *args, **kwargs):
    unapproved_list = []
    approved_studentProfile_list = []
    disapproved_studentProfile_list = []

    unapproved_list = User.objects.filter(groups__name='preapprove')
    # obtains student and preapprove group objects
    try:
      student = Group.objects.get(name='student')
    except Group.DoesNotExist:
      return {
        'error': {
          'message': 'No student groups exist.'
        }
      }

    try:
      preapprove = Group.objects.get(name='preapprove')
    except Group.DoesNotExist:
      return {
        'error': {
          'message': 'No preapproved student groups exist.'
        }
      }

    # cycles through each unapproved user testing
    for user_profile in unapproved_list:
      mail_message = ""
      try:
         # check if an E-mail should be sent
        try:
          if(request.POST['squelch_' + user_profile.username]):
            send_email = 0
        except:
          send_email = 1
        # checks to see if the user's profile was checked
        # the name of each checkbox on the front-end webpage is just the user's username
        if(request.POST['approve_or_disapprove_' + user_profile.username] == 'approve_' + user_profile.username):
          # because users can be in multiple groups, remove them from preapprove first then add to students
          dict = {}
          user_profile.groups.remove(preapprove)
          user_profile.groups.add(student)
          user_profile.save()
          student_obj = StudentProfile.objects.filter(student=user_profile)[0]
          # creates a custom mail message
          # but first check and see if the checkbox NOT to send an E-mail to the user was marked
          if(send_email == 1):
            mail_message += "Hi " + user_profile.first_name + ",\n\n"
            mail_message += "Your account has been approved for using www.CHARMMing.org. If you have any problems "
            mail_message += "logging in, please E-mail btamiller@gmail.com. Please do not reply to this E-mail\n\n"
            if(request.POST['comments_' + user_profile.username] != ""):
              mail_message += "The following comments were appended to your approval:\n"
              mail_message += request.POST['comments_' + user_profile.username] + "\n\n"
              dict['comments'] = request.POST['comments_' + user_profile.username]
            mail_message += "Thanks for using CHARMMing,\n CHARMMing Development Team"
            send_mail('CHARMMing Approval', mail_message, 'CHARMMingApproval@learn.lobos.nih.gov', [user_profile.email], fail_silently=False)
          else:
            dict['comments'] = "No E-mail was sent."
          dict['username'] = user_profile.username
          dict['email'] = user_profile.email
          dict['institute'] = student_obj.institute
          dict['charmm_license'] = student_obj.charmm_license
          dict['date_joined'] = user_profile.date_joined
          approved_studentProfile_list.append(dict)
        elif(request.POST['approve_or_disapprove_' + user_profile.username] == 'disapprove_' + user_profile.username):
          # Send user an E-mail, then delete em'!
          dict = {}
          student_obj = StudentProfile.objects.filter(student=user_profile)[0]
          if(send_email == 1):
            mail_message += "Hi " + user_profile.first_name + ",\n\n"
            mail_message += "Your account has NOT been approved for using www.CHARMMing.org. If you have any questions "
            mail_message += "about why you were not approved, please E-mail btamiller@gmail.com. Please do not reply to this E-mail\n\n"
            if(request.POST['comments_' + user_profile.username] != ""):
              mail_message += "The following comments were appended to your disapproval:\n"
              mail_message += request.POST['comments_' + user_profile.username] + "\n\n"
              dict['comments'] = request.POST['comments_' + user_profile.username]
            mail_message += "Thanks for using CHARMMing,\n CHARMMing Development Team"
            send_mail('CHARMMing Disapproval', mail_message, 'CHARMMingDisapproval@learn.lobos.nih.gov', [user_profile.email], fail_silently=False)
          else:
            dict['comments'] = "No E-mail was sent."
          dict['username'] = user_profile.username
          dict['email'] = user_profile.email
          dict['institute'] = student_obj.institute
          dict['charmm_license'] = student_obj.charmm_license
          dict['date_joined'] = user_profile.date_joined
          disapproved_studentProfile_list.append(dict)
          StudentProfile.objects.filter(student=user_profile)[0].delete()
          user_profile.delete()

      except:
        pass

      unapproved_list = User.objects.filter(groups__name='preapprove')
      unapproved_studentProfile_list = []
      for user_profile in unapproved_list:
        try:
          unapproved_studentProfile_list.append(StudentProfile.objects.get(student=user_profile))
        except:
          pass
      return {
        'context': {
          'unapproved_list': unapproved_studentProfile_list,
          'approved_list': approved_studentProfile_list,
          'disapproved_list': disapproved_studentProfile_list
        }
      }
