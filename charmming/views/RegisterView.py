from django.contrib import auth
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from charmming import charmming_config
from charmming.utils.account import registerStudent
from charmming.views import BaseView

import random
import json
from urllib.parse import urlencode
from http.client import HTTPSConnection

class RegisterView(BaseView):
  template_name = 'registration/register.html'

  def get(self, request, *args, **kwargs):
    ordinals = ['first', 'second', 'third', 'fourth', 'fifth', 'sixth']
    form = UserCreationForm()

    # make them figure out who an author on the CHARMMing paper was
    authnum = random.randint(1, 6)
    authordinal = ordinals[authnum - 1]
    register_captcha = charmming_config.register_captcha
    register_paper = charmming_config.register_paper

    return {
      "context": {
        'form': form, 'authordinal': authordinal,
        'register_captcha': register_captcha,
        'register_paper': register_paper
      }
    }

  def post(self, request, *args, **kwargs):
    form = UserCreationForm(request.POST)
    authorln = {
      'first': 'miller',
      'second': 'singh',
      'third': 'klauda',
      'fourth': 'hodoscek',
      'fifth': 'brooks',
      'sixth': 'woodcock'
    }

    if form.is_valid():
      # check the captcha
      if charmming_config.register_captcha:
        try:
          verify_host = 'www.google.com'
          verify_path = '/recaptcha/api/siteverify'
          reqdict = {
            'response': request.POST.get('g-recaptcha-response'),
            'secret': charmming_config.recaptcha_secret,
            'remoteip': request.META.get('REMOTE_ADDR')
          }

        except:
          return {
            'error': {
              'message': 'CAPTCHA error. Please try again later.'
            }
          }

        try:
          reqstr = urlencode(reqdict)
          headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain", "User-Agent": "CHARMMing v0.10"}
          conn = HTTPSConnection(verify_host)
          conn.request('POST', verify_path, reqstr, headers)
          resp = conn.getresponse()
        except:
          return {
            'error': {
              'message': 'CAPTCHA error. Please try again later.'
            }
          }

        if resp.status != 200:
          # This is a Google error, not a CAPTCHA error, but it is likely transient,
          # so ask the user to resubmit their form.
          return {
            'error': {
              'message': 'Invalid CAPTCHA. Please try again.'
            }
          }
        else:
          redata = resp.read()
          ret = json.loads(redata)

          if not ret['success'] is True:
            return {
              'error': {
                'message': 'Invalid CAPTCHA. Please try again.'
              }
            }

      # end of captcha stuff...

      username = form.cleaned_data['username'].lower()
      password = form.cleaned_data['password1']
      institute = request.POST['institute'].lower()

      if username == institute:
        return {
          'error': {
            'message': 'Your first name cannot be the same as your institute.'
          }
        }

      first_name = request.POST['first_name']
      last_name = request.POST['last_name']
      if not first_name.strip() or not last_name.strip():
        return {
          'error': {
            'meesage': 'A complete name is required.'
          }
        }
      email = request.POST['email']
      if email.strip() == '':
        return {
          'error': {
            'message': 'An email address is required.'
          }
        }

      if charmming_config.register_paper:
        authordinal = request.POST['authordinal'].lower()
        authanswer = request.POST['authanswer'].lower()
        authordinal = authordinal.strip()
        authanswer = authanswer.strip()

        try:
          if authorln[authordinal] != authanswer:
            return {
              'error': {
                'message': 'Sorry - you got the {} CHARMMing paper author wrong. Please use your back button and try again.'.format(authordinal)
              }
            }
        except:
          return {
            'error': {
              'message': 'Missing secret question/answer data. '
            }
          }
      # end of paper register

      user = User.objects.create_user(username=username, email=email, password=password)
      user.first_name = first_name
      user.last_name = last_name
      user.save()

      user = auth.authenticate(username=username, password=password)
      auth.login(request, user)

      return registerStudent(request, user)
