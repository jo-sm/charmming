from charmming.views import BaseView
from charmming.models import Task

class TasksView(BaseView):
  def get(self, request, *args, **kwargs):
    user = request.session.user
    tasks = Task.objects.filter(structure__owner=user).order_by('-id')[:20]

    return {
      'context': {
        'tasks': tasks
      }
    }
