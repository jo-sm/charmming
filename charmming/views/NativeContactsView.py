from charmming.models import Structure, WorkingStructure, CGWorkingStructure
from charmming.views import BaseView
from charmming.utils.analysis import get_native_contacts, extract_xyz, calc_q
from charmming.utils.lesson import get_lesson_object, doLessonAct

from pychm3.io.pdb import get_model_from_pdb

import os

class NativeContactsView(BaseView):
  template_name = 'html/natqdisplay.html'

  def get(self, request, *args, **kwargs):
    # main routine for native contacts...
    # input.checkRequestData(request)

    # chooses the file based on if it is selected or not
    try:
      struct = Structure.objects.filter(owner=request.user, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'Please upload a structure first.',
        },
        'redirect': '/fileupload'
      }
    try:
      ws = WorkingStructure.objects.filter(structure=struct, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'Please build your structure first.',
        },
        'redirect': '/buildstruct'
      }

    try:
      cgws = CGWorkingStructure.objects.get(workingstructure_ptr=ws.id)
    except:
      return {
        'error': {
          'message': 'Native contact analysis is only available for CG models.'
        },
        'redirect': '/buildstruct'
      }

    if cgws.cg_type != 'go':
      return {
        'error': {
          'message': 'Native contact analysis is only available for KT Go models.'
        },
        'redirect': '/buildstruct'
      }

    pdbfile = struct.location + '/cgbase-' + str(cgws.id) + '.pdb'
    dcdfile = struct.location + '/' + ws.identifier + '-md.dcd'
    try:
      os.stat(pdbfile)
    except:
      return {
        'error': {
          'message': 'Please build your structure and run dynamics first.'
        },
        'redirect': '/buildstruct'
      }

    baddcd = False
    try:
      os.stat(dcdfile)
    except:
      baddcd = True

    if baddcd:
      dcdfile = struct.location + '/' + ws.identifier + '-ld.dcd'
      try:
        os.stat(dcdfile)
      except:
        return {
          'error': {
            'message': 'Please run dynamics first.'
          },
          'redirect': '/buildstruct'
        }

    natqlines = []

    # --- Frank's main routine here ---
    my_aa_mol = get_model_from_pdb(pdbfile)
    my_aa_mol.parse()

    # scan the AA Mol object, to build a list of residues that are in native
    # contact in the crystal structure.
    my_native_contacts = get_native_contacts(my_aa_mol)
    # read the xyz data from the DCD file, throw away the coordinates that
    # arent part of a native contact pair
    x, y, z = extract_xyz(dcdfile, my_native_contacts)
    q = calc_q(my_native_contacts, x, y, z)

    # we only have 1000 frames in a CHARMMing trajectory file by default.
    # One frame every 100 steps
    mystep = 100
    for i in range(len(q)):
      natqlines.append((mystep, '%5.3f' % q[i]))
      mystep += 100

    lesson_obj = get_lesson_object(ws.structure.lesson_type)

    if lesson_obj:
      doLessonAct(ws.structure, "onNATQSubmit", request.POST)

    return {
      'context': {
        'natqlines': natqlines
      }
    }
