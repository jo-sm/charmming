from charmming.views import BaseView
from charmming.models import DDProject

class DDSwitchProjectView(BaseView):
  template_name = 'html/ddswitchproject.html'

  def post(self, request, *args, **kwargs):
    switch_id = kwargs.get('switch_id')

    try:
      oldproject = DDProject.objects.filter(owner=request.user, selected='y')[0]
      oldproject.selected = ''
      oldproject.save()
    except:
      pass
    newproject = DDProject.objects.filter(owner=request.user, id=switch_id)[0]
    newproject.selected = 'y'
    newproject.save()

    return {
      'context': {
        'oldproject': oldproject,
        'newproject': newproject,
      }
    }
