from charmming.views import BaseView
from charmming.models import Structure, Segment, Patch
from charmming.utils.number import make_float
from charmming.utils.cg_structure import associate_segments_with_cg_structure, handle_cg
from charmming.utils.uri import data_uri
from charmming.utils.structure import create_child

import base64
import os

class StructureView(BaseView):
  def pre(self, request, *args, **kwargs):
    user = request.session.user
    structure_id = kwargs.get('id')
    structures = Structure.objects.filter(id=structure_id, owner=user)

    if len(structures) == 0:
      return {
        'error': {
          'message': 'Structure not found.'
        }
      }

    if len(structures) > 1:
      return {
        'error': {
          'message': 'An error occurred. Try again.'
        }
      }

    setattr(request, 'structure', structures[0])

  def get(self, request, *args, **kwargs):
    return {
      'context': {
        'structure': request.structure
      }
    }

  def post(self, request, *args, **kwargs):
    """
    Update the structure, or create a child from this structure
    """
    data = request.POST
    structure = request.structure

    if data.get('create_child'):
      create_child(structure, data)
    else:
      pass

  def put(self, request, *args, **kwargs):
    user = request.session.user
    structure = request.structure

    data = request.PUT
    structure_type = data.get('type')
    valid_types = [ 'aa', 'bln', 'go' ]

    if structure_type not in valid_types:
      return {
        'error': {
          'message': 'Submit a valid structure type.'
        }
      }

    segment_ids = [segment['id'] for segment in data.get('segments', [])]
    segments = Segment.objects.filter(structure=structure, id__in=segment_ids)

    if not segments or len(segments) < 1:
      return {
        'error': {
          'message': 'Select one segment to add to this structure.'
        }
      }

    if structure_type == 'aa':
      allowed_toppar_methods = [ 'standard', 'upload', 'autogen', 'redox' ]

      if user.admin:
        allowed_toppar_methods.extend([ 'dogmans', 'match', 'genrtf', 'antechamber' ])

      for segment in segments:
        # Verify that each segment data piece is valid (has top/par, valid first/last patch)
        # Determine if new segment needs to be created for new structure
        segment_data = [seg for seg in data.get('segments') if seg['id'] == segment.id][0]

        segment_data['copy'] = False

        if segment_data.get('toppar_method') not in allowed_toppar_methods:
          segment_data['toppar_method'] = 'standard'

        if segment.first_patch != segment_data.get('first_patch'):
          segment_data['copy'] = True

        if segment.last_patch != segment_data.get('last_patch'):
          segment_data['copy'] = True

        if segment.toppar_method != segment_data.get('toppar_method'):
          segment_data['copy'] = True

        if segment_data.get('toppar_method') == 'upload':
          rtf_raw = segment_data.get('rtf')

          if not rtf_raw:
            return {
              'error': {
                'message': 'Upload a valid RTF file for segment {}.'.format(segment.name)
              }
            }

          prm_raw = segment_data.get('prm')

          if not prm_raw:
            return {
              'error': {
                'message': 'Upload a valid PRM file for segment {}.'.format(segment.name)
              }
            }

      # Save Structure
      child_structure = structure.create_child()

      new_segments_list = []

      for _segment in segments:
        # Now that all of the segment data is verified, augment the segments if necessary
        segment_data = [seg for seg in data.get('segments') if seg['id'] == _segment.id][0]

        if segment_data.get('copy'):
          segment = _segment.copy()
        else:
          segment = _segment

        if segment.first_patch != segment_data.get('first_patch'):
          segment.first_patch = segment_data.get('first_patch')

        if segment.last_patch != segment_data.get('last_patch'):
          segment.last_patch = segment_data.get('last_patch')

        if segment.toppar_method != segment_data.get('toppar_method'):
          segment.toppar_method = segment_data.get('toppar_method')

        if segment_data.get('toppar_method') == 'upload':
          rtf_raw = segment_data.get('rtf')
          mimetype, file_encoded = data_uri(rtf_raw)
          rtf_encoded = base64.b64decode(file_encoded)
          rtf = rtf_encoded.decode('utf-8')
          path, file_name, file_ext = child_structure.save_file(rtf, file_name="{}.rtf".format(segment.name))

          prm_raw = segment_data.get('prm')
          mimetype, file_encoded = data_uri(prm_raw)
          prm_encoded = base64.b64decode(file_encoded)
          prm = prm_encoded.decode('utf-8')
          path, file_name, file_ext = child_structure.save_file(prm, file_name="{}.rtf".format(segment.name))

          segment.rtf_list = os.path.join(path, "{}.rtf".format(segment.name))
          segment.prm_list = os.path.join(path, "{}.prm".format(segment.name))

        # Handle protonation patching
        if segment_data.get('protonation'):
          for residue, name in list(segment_data.get('protonation').items()):
            if name in ['hsd', 'lys', 'glu', 'asp']:
              # These are default patches, so they can be skipped
              continue

            patch = Patch()
            patch.structure = child_structure
            patch.segment = segment
            patch.name = name
            patch.segment_residue = "{}".format(residue)
            patch.save()

        # Handle disulfide patching
        if segment_data.get('disulfide'):
          for patch_data in list(segment_data.get('disulfide')):
            segment_1 = patch_data.get('segment_1')
            segment_1_residue = patch_data.get('segment_1_residue')
            segment_2 = patch_data.get('segment_2')
            segment_2_residue = patch_data.get('segment_2_residue')

            patch = Patch()
            patch.structure = child_structure
            patch.name = 'disu'
            patch.segment_residue = '{} {} {} {}'.format(segment_1, segment_1_residue, segment_2, segment_2_residue)
            patch.save()

        segment.save()
        new_segments_list.append(segment)

      child_structure.segments.add(*new_segments_list)

      return {
        'context': {
          'structure': child_structure.id
        }
      }

    elif structure_type == 'bln':
      bln_options = {
        'helix_bond_force': make_float(data.get('helix_bond_force', 3.5), 3.5),
        'helix_angle_force': make_float(data.get('helix_angle_force', 8.37), 8.37),
        'b_sheet_bond_force': make_float(data.get('b_sheet_bond_force', 3.5), 3.5),
        'b_sheet_angle_force': make_float(data.get('b_sheet_angle_force', 8.37), 8.37),
        'coil_bond_force': make_float(data.get('coil_bond_force', 2.5), 2.5),
        'coil_angle_force': make_float(data.get('coil_angle_force', 5.98), 5.98),
      }

      child_structure = structure.create_child()
      child_structure.cg_model_type = 'bln'
      child_structure.save()

      associate_segments_with_cg_structure(child_structure, segments)
      handle_cg(child_structure, 'bln', **bln_options)

      return {
        'context': {
          'structure': child_structure.id
        }
      }

    elif structure_type == 'go':
      valid_contact_types = [ 'mj', 'kgs', 'bt' ]
      contact_type = data.get('contact_type', 'mj')

      if contact_type not in valid_contact_types:
        return {
          'error': {
            'message': 'Select a valid contact type for this GO model.'
          }
        }

      go_options = {
        'contact_type': contact_type,
        'calculate_nscale': data.get('calculate_nscale', False),
        'nscale': make_float(data.get('nscale', 1.00), 1.00),
        'nscale_temp': data.get('nscale_temp', 340),
        'contact_radius': make_float(data.get('contact_radius', 4.5), 4.5),
        'bond_force_const': make_float(data.get('bond_force_const', 50.0), 50.0),
        'angle_force_const': make_float(data.get('angle_force_const', 30.0), 30.0),
        'dihedral_alpha_1_helix': make_float(data.get('dihedral_alpha_1_helix', 0.30), 0.30),
        'dihedral_310_1_helix': make_float(data.get('dihedral_310_helix', 0.30), 0.30),
        'dihedral_helical_1': make_float(data.get('dihedral_helical_1', 0.55), 0.55),
        'dihedral_alpha_3_helix': make_float(data.get('dihedral_alpha_3_helix', 0.15), 0.15),
        'dihedral_310_3_helix': make_float(data.get('dihedral_310_3_helix', 0.15), 0.15),
        'dihedral_unhelical_3': make_float(data.get('dihedral_unhelical_3', 0.275), 0.275),
        'h_bond_alpha_helix': make_float(data.get('h_bond_alpha_helix', -0.25), -0.25),
        'h_bond_three_10_helix': make_float(data.get('h_bond_three_10_helix', -0.25), -0.25),
        'h_bond_no_helix': make_float(data.get('h_bond_no_helix', -0.50), -0.50),
        'non_attractive_lj': make_float(data.get('non_attractive_lj', 1e-12), 1e-12),
        'backbone_sidechain_lj': make_float(data.get('backbone_sidechain_lj', -0.37), -0.37),
      }

      # Create child structure
      child_structure = structure.create_child()
      child_structure.cg_model_type = 'go'
      child_structure.save()

      # Associate segments with new structure
      associate_segments_with_cg_structure(child_structure, segments)
      handle_cg(child_structure, 'go', **go_options)

      if go_options.get('calculate_nscale'):
        pass
        # TODO: Add this as a job instead of a separate command
        # cmd = '%s/find_nscale_async.py %s %s %f %f' % (charmming_config.data_home, working_structure.structure.location, basefname, cg_model.nScale, kwargs.get('nscale_temp'))
        # os.system(cmd)

      return {
        'context': {
          'structure': child_structure.id
        }
      }

  def delete(self, request, *args, **kwargs):
    user = request.session.user
    structure_id = kwargs.get('id')
    structures = Structure.objects.filter(id=structure_id, owner=user)

    if len(structures) == 0:
      return {
        'error': {
          'message': 'Structure not found.'
        }
      }

    if len(structures) > 1:
      return {
        'error': {
          'message': 'An error occurred. Try again.'
        }
      }

    structure = structures[0]

    # Keep the Structure ID before deleting, so we can send the UI
    # the structure deleted
    structure_id = structure.id

    structure.delete()

    return {
      'context': {
        'structure': structure_id
      }
    }
