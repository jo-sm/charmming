from charmming.views import BaseView

class AboutView(BaseView):
  template_name = 'html/about.html'
