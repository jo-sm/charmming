from django.contrib.auth.models import User

from charmming import charmming_config
from charmming.views import BaseView
from charmming.models import DDLigandSet, DDLigand, DDSource, DDFile, DDFileType, DDFileObject, DDLigandSetsLigands
from charmming.utils.dd_substrate import getChemicalName, getNewLigandOwnerIndex

import os
import glob

class DDRefreshMaybhitLigandsView(BaseView):
  def post(self, request, *args, **kwargs):
    location = charmming_config.user_dd_ligands_home + '/public/for_charmming/'
    os.system("cd %s\n" % (location))
    u = User.objects.get(username='public')
    count = 0
    newligands = []
    try:
      maybhit_set = DDLigandSet.objects.get(ligand_set_name="Maybridge Diversity Set", owner=u)
    except:
      maybhit_set = DDLigandSet()
      maybhit_set.ligand_set_name = "Maybridge Diversity Set"
      maybhit_set.description = "A subset of the Maybridge HitFinder set limited to molecules that had met docking by decomposition protocol requirements and limitations"
      maybhit_set.owner = u
      maybhit_set.save()

    for file in glob.glob(os.path.join(location, 'ZINC????????.mol2')):
      file_obj = open(file, 'r')
      basename = os.path.basename(file)
      mol_title = os.path.splitext(basename)[0]
      chemical_name = getChemicalName(file_obj)
      str_file = glob.glob(os.path.join(location, mol_title + ".str"))[0]
      psf_file = glob.glob(os.path.join(location, mol_title + ".psf"))[0]
      newligand = DDLigand()

      try:
        newligands = DDLigand.objects.filter(owner=u, ligand_name=mol_title)

      except:
        pass

      if len(newligands) == 0:
        newligand.owner = u
        newligand.ligand_name = mol_title
        newligand.ligand_owner_index = str(getNewLigandOwnerIndex(u))

        if chemical_name == "":
          newligand.description = "System preloaded drug-like compound"
        else:
          newligand.description = chemical_name

        source = DDSource.objects.get(source_name='Zinc Database')
        newligand.source = source
        newligand.save()

        new_location = charmming_config.user_dd_ligands_home + '/' + 'public' + '/' + 'ligand_' + str(newligand.ligand_owner_index) + '/'
        filename = 'ligand_' + str(newligand.ligand_owner_index) + '_1.mol2'
        str_filename = 'ligand_' + str(newligand.ligand_owner_index) + '_1.str'
        psf_filename = 'ligand_' + str(newligand.ligand_owner_index) + '_1.psf'
        # min_filename = 'ligand_' + str(newligand.ligand_owner_index) + '_2.mol2'

        os.system("mkdir " + new_location)
        os.system("chmod g+w " + new_location)

        ligand_file_type = DDFileType.objects.get(file_type_name="Ligand Structure File")
        newfile = DDFile()
        newfile.owner = u
        newfile.file_name = filename
        newfile.file_location = new_location
        newfile.file_type = ligand_file_type
        newfile.description = "Structure File for Ligand %s" % (newligand.ligand_name)
        newfile.save()
        os.system("cp %s %s%s" % (file, new_location, filename))

        str_file_type = DDFileType.objects.get(file_type_name="CHARMM Param Stream File")
        str_newfile = DDFile()
        str_newfile.owner = u
        str_newfile.file_name = str_filename
        str_newfile.file_location = new_location
        str_newfile.file_type = str_file_type
        str_newfile.description = "Stream File for Ligand %s" % (newligand.ligand_name)
        str_newfile.save()
        os.system("cp %s %s%s" % (str_file, new_location, str_filename))

        psf_file_type = DDFileType.objects.get(file_type_name="CHARMM Structure File")
        psf_newfile = DDFile()
        psf_newfile.owner = u
        psf_newfile.file_name = psf_filename
        psf_newfile.file_location = new_location
        psf_newfile.file_type = psf_file_type
        psf_newfile.description = "CHARMM Structure File for Ligand %s" % (newligand.ligand_name)
        psf_newfile.save()
        os.system("cp %s %s%s" % (psf_file, new_location, psf_filename))

        newfileobject = DDFileObject()
        newfileobject.owner = u
        newfileobject.file = newfile
        newfileobject.object_table_name = 'dd_substrate_ligands'
        newfileobject.object_id = newligand.id
        newfileobject.save()

        newfileobject = DDFileObject()
        newfileobject.owner = u
        newfileobject.file = str_newfile
        newfileobject.object_table_name = 'dd_substrate_ligands'
        newfileobject.object_id = newligand.id
        newfileobject.save()

        newfileobject = DDFileObject()
        newfileobject.owner = u
        newfileobject.file = psf_newfile
        newfileobject.object_table_name = 'dd_substrate_ligands'
        newfileobject.object_id = newligand.id
        newfileobject.save()

        # include ligand in the set
        set_ligand = DDLigandSetsLigands()
        set_ligand.ligands_set = maybhit_set
        set_ligand.ligands = newligand
        set_ligand.owner = u
        set_ligand.save()
        # end include in the set

      count = count + 1

    return {
      'statuses': {
        'success': 'Done.'
      }
    }
