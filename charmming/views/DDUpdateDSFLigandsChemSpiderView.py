from charmming.views import BaseView

from chemspipy import ChemSpider

class DDUpdateDSFLigandsChemSpiderView(BaseView):
  template_name = 'html/dddsfligandschemspi.html'

  def post(request, selected_set_id):
    # TODO: move this into separate file/env variable/Vault
    chemspipy_token = '15252a8d-a7dd-4c56-8678-26833c49f3e8'

    search = ChemSpider(chemspipy_token)
    compounds = search.search('chol')
    count = len(compounds)

    return {
      'context': {
        'ligands': compounds,
        'count': count
      }
    }
