from charmming import charmming_config
from charmming.views import BaseView
from charmming.models import Structure, WorkingStructure, Segment, Patch, CGWorkingStructure, LessonMakerLesson
from charmming.utils.working_structure import associate as associate_ws
from charmming.utils.cg_working_structure import associate as associate_cg
from charmming.utils.lesson import get_lesson_object, doLessonAct

class ModifyStructureView(BaseView):
  """
  TODO: This belongs in BuildStructureView, but Django doesn't
  support PUT in forms natively
  """
  template_name = 'html/built.html'

  def post(self, request, *args, **kwargs):
    # input.checkRequestData(request)

    nscale_msg = ''
    try:
      struct = Structure.objects.filter(owner=request.user, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'Please submit a structure.'
        },
        'redirect': '/fileupload'
      }

    if 'wsidentifier' not in request.POST:
      return {
        'error': {
          'message': 'You must give this working structure an identifier.'
        },
        'redirect': '/buildstruct'
      }
    else:
      if len(request.POST['wsidentifier']) > 20:
        return {
          'error': {
            'message': 'Working structure identifier too long. Please make sure your identifier is under 20 characters long.'
          },
          'redirect': '/buildstruct'
        }

    if 'buildtype' not in request.POST:
      return {
        'error': {
          'message': 'No build type specified.'
        },
        'redirect': '/buildstruct'
      }

    if request.POST['buildtype'] == 'aa':
      segs = Segment.objects.filter(structure=struct, is_working='n')
      seglist = []
      for s in segs:
        ulkey = 'select_' + s.name
        if ulkey in request.POST and request.POST[ulkey] == 'y':
          seglist.append(s.name)
      if len(seglist) < 1:
        return {
          'error': {
            'message': 'You must choose at least one segment.'
          },
          'redirect': '/buildstruct'
        }

      tpdict = {}
      filedata = request.FILES
      for seg in seglist:
        if 'toppar_' + seg in request.POST:
          tpdict[seg] = request.POST['toppar_' + seg]

          allowedList = ['standard', 'upload', 'autogen', 'redox']
          if request.user.is_superuser:
            allowedList.extend(['dogmans', 'match', 'genrtf', 'antechamber'])

          if tpdict[seg] not in allowedList:
            tpdict[seg] = 'standard'

          if tpdict[seg] == 'upload':
            try:
              uptop = filedata['topology_' + seg]
              uppar = filedata['parameter_' + seg]
            except:
              return {
                'error': {
                  'message': 'Topology/parameter files not uploaded.'
                },
                'redirect': '/buildstruct'
              }
            topfp = open(struct.location + '/' + request.POST['wsidentifier'] + '-' + seg + '.rtf', 'w+')
            prmfp = open(struct.location + '/' + request.POST['wsidentifier'] + '-' + seg + '.prm', 'w+')
            for chunk in uptop.chunks():
              topfp.write(chunk)
            for chunk in uppar.chunks():
              prmfp.write(chunk)
        else:
          tpdict[seg] = 'standard'

      new_ws = WorkingStructure()
      # to do, make this not contain spaces
      new_ws.identifier = request.POST['wsidentifier']
      new_ws.modelName = request.POST['basemodel']
      associate_ws(new_ws, struct, seglist, tpdict)

      # Figure out terminal patching
      for segobj in new_ws.segments.all():
        fpvar = segobj.name + '_firstpatch'
        lpvar = segobj.name + '_lastpatch'

        if fpvar in request.POST:
          segobj.patch_first = request.POST[fpvar]
          segobj.save()
        if lpvar in request.POST:
          segobj.patch_last = request.POST[lpvar]
          segobj.save()

      # Figure out protonation and disulfide patching
      for pkey in list(request.POST.keys()):
        if pkey.startswith('protostate_'):
          (junk, segid, resid) = pkey.split('_')

          if segid not in seglist:
            continue # not in the segments selected
          if request.POST[pkey] in ['hsd', 'lys', 'glu', 'asp']:
            continue # these are defaults, no need for a patch
          p = Patch()
          p.structure = new_ws
          p.patch_segid = Segment.objects.get(structure=struct, is_working='n', name=segid)
          p.patch_name = request.POST[pkey]
          p.patch_segres = "%s %s" % (segid, resid)
          p.save()

        if pkey.startswith('disul_'):
          (junk, segid1, resid1, segid2, resid2) = pkey.split('_')
          if not (segid1 in seglist and segid2 in seglist):
            continue # patch not valid for segments selected
          p = Patch()
          p.structure = new_ws
          p.patch_name = 'disu'
          p.patch_segres = "%s %s %s %s" % (segid1, resid1, segid2, resid2)
          p.save()

    elif request.POST['buildtype'] == 'go':
      seglist = []

      segs = Segment.objects.filter(structure=struct, is_working='n')
      for s in segs:
        ulkey = 'go_select_' + s.name
        if ulkey in request.POST and request.POST[ulkey] == 'y':
          seglist.append(s.name)

      if len(seglist) < 1:
        return {
          'error': {
            'message': 'You must choose at least one segment.'
          },
          'redirect': '/buildstruct'
        }

      new_ws = CGWorkingStructure()
      new_ws.identifier = request.POST['wsidentifier']
      new_ws.modelName = request.POST['basemodel']
      new_ws.cg_type = 'go'

      if request.POST['gm_known_nscale'] == 'false':
        findnscale = True
        nscale_msg = 'The nScale parameterization process will take 1-2 hours. Until it is done, you will not be able to run calculations\n'
        nscale_msg += 'Once you can run a calculation, download a tarball of structure files to see the new nScale\n'
        nscale_msg += 'This functionality is brand new ... please contact btmiller -at- nhlbi -dot- nih -dot- gov with issues.\n'
      else:
        findnscale = False
      try:
        gm_nscale_temp = float(request.POST['gm_nscale_temp'])
      except:
        gm_nscale_temp = 340.0

      associate_cg(new_ws, struct, seglist, contactSet=request.POST['gm_contact_type'], nScale=request.POST['gm_nscale'], kBond=request.POST['gm_kbond'], kAngle=request.POST['gm_kangle'], contactrad=request.POST['gm_contactrad'], findnscale=findnscale, gm_nscale_temp=gm_nscale_temp)

      new_ws.save()

    elif request.POST['buildtype'] == 'bln':
      seglist = []

      segs = Segment.objects.filter(structure=struct, is_working='n')
      for s in segs:
        ulkey = 'bln_select_' + s.name
        if ulkey in request.POST and request.POST[ulkey] == 'y':
          seglist.append(s.name)

      if len(seglist) < 1:
        return {
          'error': {
            'message': 'You must choose at least one segment.'
          },
          'redirect': '/buildstruct'
        }

      new_ws = CGWorkingStructure()
      new_ws.identifier = request.POST['wsidentifier']
      new_ws.modelName = request.POST['basemodel']
      new_ws.cg_type = 'bln'
      associate_cg(new_ws, struct, seglist, kBondHelix=request.POST['bln_kbondhelix'], kAngleHelix=request.POST['bln_kanglehelix'], kBondSheet=request.POST['bln_kbondsheet'], kAngleSheet=request.POST['bln_kanglesheet'], kBondCoil=request.POST['bln_kbondcoil'], kAngleCoil=request.POST['bln_kanglecoil'])
      new_ws.save()
    else:
      return {
        'error': {
          'message': 'Bad buildtype specified.'
        },
        'redirect': '/buildstruct'
      }

    # ToDo: Figure out restraints

    # switch to using the new work structure
    try:
      old_ws = WorkingStructure.objects.filter(structure=struct, selected='y')[0]
    except:
      pass
    else:
      old_ws.selected = 'n'
      old_ws.save()

    new_ws.selected = 'y'
    new_ws.save()

    lesson_obj = get_lesson_object(new_ws.structure.lesson_type)
    if lesson_obj:
      doLessonAct(new_ws.structure, "onBuildStructureSubmit", request.POST)
    if struct.lessonmaker_active and charmming_config.lessonmaker_enabled:
      lmaker = LessonMakerLesson.objects.filter(structure=struct)[0]
      lmaker.onBuildStructureSubmit()
      lmaker.save()

    return {
      'context': {
        'nscale_msg': nscale_msg
      }
    }
