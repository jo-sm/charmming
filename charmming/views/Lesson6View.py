from charmming.views import BaseView
from charmming.models import Structure, LessonProblem, Lesson6

class Lesson6View(BaseView):
  template_name = 'html/lesson6.html'

  def get(self, request, *args, **kwargs):
    try:
      file = Structure.objects.filter(owner=request.user, selected='y')[0]
    except:
      return {}

    if file.lesson_type == 'lesson6':
      lesson_obj = Lesson6.objects.filter(user=request.user, id=file.lesson_id)[0]
      html_step_list = lesson_obj.getHtmlStepList()
    else:
      lesson_obj = None
      html_step_list = None
    try:
      lessonproblems = LessonProblem.objects.filter(lesson_type='lesson6', lesson_id=lesson_obj.id, errorstep__lt=999)
    except:
      lessonproblems = None

    return {
      'context': {
        'lesson6': lesson_obj,
        'lessonproblems': lessonproblems,
        'html_step_list': html_step_list
      }
    }
