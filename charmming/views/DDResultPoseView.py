from django.contrib.auth.models import User

from charmming.views import BaseView
from charmming.models import Structure, WorkingStructure, Segment, DDFile

class DDResultPoseView(BaseView):
  template_name = 'html/ddviewresultposejmol.html'

  def get(self, request, *args, **kwargs):
    result_file_id = kwargs.get('result_file_id')

    try:
      struct = Structure.objects.filter(owner=request.user, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'Please submit a structure first.'
        },
        'redirect': '/fileupload'
      }

    try:
      ws = WorkingStructure.objects.filter(structure=struct, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'Please build a structure first.'
        },
        'redirect': '/buildstruct'
      }

    username = request.user.username
    u = User.objects.get(username=username)
    resultfile = DDFile.objects.get(owner=u, id=result_file_id)
    filename = resultfile.file_location + "/" + resultfile.file_name

    pro_segment = Segment.objects.filter(structure=ws.structure, type="pro", is_working="y")[0]  # filter[0] is on purpose
    pro_name = pro_segment.name + '-' + str(pro_segment.id)
    proteinfile = "%s/%s.pdb" % (ws.structure.location, pro_name)
    ligandfile = filename.replace("/home/schedd/", "/charmming/pdbuploads/")
    proteinfile = proteinfile.replace("/home/schedd/", "/charmming/pdbuploads/")
    proteinfile = proteinfile.replace("/var/tmp/", "/charmming/")

    try:
      username = request.user.username
      return {
        'context': {
          'ligandfile': ligandfile,
          'proteinfile': proteinfile
        }
      }
    except:
      return {
        'error': {
          'message': 'No PDB Uploaded'
        }
      }
