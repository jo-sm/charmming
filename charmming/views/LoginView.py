from charmming.views import BaseView
from charmming.models import User, Session
from charmming.utils.request import get_client_ip
from charmming.utils.token import generate_instance_token

import bcrypt
import datetime

class LoginView(BaseView):
  no_auth = True # This view doesn't require authentication

  def post(self, request, *args, **kwargs):
    username = request.POST.get('username')
    password = request.POST.get('password')

    # Check that username and password were given
    if not username:
      return {
        'error': {
          'message': 'Please enter your username.'
        }
      }

    if not password:
      return {
        'error': {
          'message': 'Please enter your password.'
        }
      }

    username = username.lower()
    password = password.encode('utf-8')
    users = User.objects.filter(username=username)

    # There should only be one user with given username
    if len(users) != 1:
      return {
        'error': {
          'message': 'User {} not found.'.format(username)
        }
      }

    user = users[0]

    # Verify password using bcrypt
    digest = user.digest.encode('utf-8')
    valid_password = bcrypt.checkpw(password, digest)

    if not valid_password:
      return {
        'error': {
          'message': 'Invalid password.'
        }
      }

    # Create user session
    session = Session()
    session.user = user
    session.ip = get_client_ip(request)
    session.user_agent = request.META.get('HTTP_USER_AGENT', 'Unknown')
    session.created_on = datetime.datetime.now()
    session.accessed_on = datetime.datetime.now()
    session.save()

    session_token = generate_instance_token(session)
    notification_token = generate_instance_token(user)

    if not session_token or not notification_token:
      # Somehow one/both of the tokens weren't generated properly, destroy the session and return an error
      session.delete()

      return {
        'error': {
          'message': 'An unknown error occurred. Try again.'
        }
      }

    return {
      'cookies': {
        'charmming-token': session_token,
        'notification-token': notification_token
      }
    }
