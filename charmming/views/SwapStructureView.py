from charmming.views import BaseView
from charmming.models import Structure, WorkingStructure

class SwapStructureView(BaseView):
  template_name = 'html/swapped.html'

  def post(self, request, *args, **kwargs):
    # input.checkRequestData(request)

    try:
      struct = Structure.objects.filter(owner=request.user, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'Please select a structure.'
        },
        'redirect': '/buildstruct'
      }

    if 'choosestruct' not in request.POST:
      return {
        'error': {
          'message': 'Invalid chosen structure.'
        },
        'redirect': '/buildstruct'
      }

    try:
      old_ws = WorkingStructure.objects.filter(structure=struct, selected='y')[0]
    except:
      pass
    else:
      old_ws.selected = 'n'
      old_ws.save()

    try:
      new_ws = WorkingStructure.objects.filter(structure=struct, identifier=request.POST['choosestruct'])[0]
    except:
      return {
        'error': {
          'message': 'Invalid structure selected.'
        },
        'redirect': '/buildstruct'
      }
    else:
      new_ws.selected = 'y'
      new_ws.save()

    return {
      'context': {
        'wsname': new_ws.identifier
      }
    }
