from django.http import HttpResponse

from charmming.views import BaseView
from charmming.utils.assays import get_dir

import mimetypes

class AssayDownloadView(BaseView):
  def download(request):
    work_dir = get_dir(request)
    filename_request = request.POST['filename_request']
    filename = work_dir + filename_request
    new_name = "pubchem-assays.sdf"
    mimetype, encoding = mimetypes.guess_type(filename)

    response = HttpResponse(mimetype=mimetype)
    response['Content-Disposition'] = 'attachment; filename=%s' % new_name
    response.write(open(filename, "rb").read())
    return response
