from charmming import charmming_config
from charmming.views import BaseView
from charmming.utils.qsar.common import objQSARJob
from charmming.models import QSARJobsModels, QSARJob

from datetime import datetime

class QSARJobsDivView(BaseView):
  template_name = 'qsar/viewjobsdiv.html'

  def get(self, request, *args, **kwargs):
    rows = QSARJob.objects.filter(
      job_scheduler_id__userid=request.user.id
    ).select_related().values(
      'id',
      'job_scheduler_id',
      'job_scheduler_id__state',
      'job_scheduler_id__queued',
      'job_scheduler_id__started',
      'job_scheduler_id__ended',
      'job_type__name',
      'job_owner_index',
      'description',
      'job_scheduler_id__queued'
    ).order_by('-job_owner_index')

    jobs = []

    for row in rows:
      if row.job_scheduler_id__state == 1:
        status = 'Queued'
      elif row.job_scheduler_id__state == 2:
        status = 'Running'
      elif row.job_scheduler_id__state == 3:
        status = 'Done'
      elif row.job_scheduler_id__state == 4:
        status = 'Failed'

      if status == 'Queued':
        waited_time = (datetime.now() - row.job_scheduler_id__queued).total_seconds()
      else:
        waited_time = (row.job_scheduler_id__ended - row.job_scheduler_id__queued).total_seconds()

      if status == 'Running':
        runtime_time = (datetime.now() - row.job_scheduler_id__started).total_seconds()
        total_time = (datetime.now() - row.job_scheduler_id__queued).total_seconds
      else:
        runtime_time = (row.job_scheduler_id__ended - row.job_scheduler_id__started).total_seconds()
        total_time = (row.job_scheduler_id__ended - row.job_scheduler_id__queued).total_seconds()

      job = objQSARJob()
      job.id = row.id
      job.scheduler_id = row.job_scheduler_id
      job.owner_index = row.job_owner_index
      job.description = row.description
      job.type = row.job_type__name
      job.status = status
      job.queued = row.job_scheduler_id__queued
      job.waited = waited_time
      job.runtime = runtime_time
      job.totaltime = total_time
      # try:
      output_data = ""
      try:
        # if 1==1:
        if job.type == "Predict":
          qsar_folder = charmming_config.user_home + "/" + request.user.username + "/qsar"
          # qsar_jobs_folder = qsar_folder + "/jobs"
          job_folder = qsar_folder + "/jobs/qsar_job_" + str(job.owner_index)
          # if os.path.isfile(job_folder + "/predict_output"):
          predict_output = open(job_folder + "/predict_output", "r")
          for line in predict_output:
            stripped_line = line.strip("\n")
            if stripped_line:
              output_data += stripped_line + "/"
          if output_data and output_data != "0.0/" and output_data != "0/0/":
            output_data = output_data[:-1]
          else:
            output_data = "N/A"
          predict_output.close()

      except:
        output_data = ""

      job.output_data = output_data
      try:
        qsar_job_model = QSARJobsModels.objects.get(owner_id=request.user.id, job_id=job.id)
        job.model_id = qsar_job_model.qsar_model_id
      except:
        job.model_id = 0

      jobs.append(job)

    return {
      'context': {
        'jobs': jobs
      }
    }
