from django.contrib.auth.models import User

from charmming.views import BaseView
from charmming.models import DDLigandSet
from charmming.utils.dd_substrate import LigandSetExists

class DDUpdateLigandSetInfoView(BaseView):
  template_name = 'html/ddnewligandsetinfo.html'

  def viewUpdateLigandSetInfo(request, ligandsetid, action):
    username = request.user.username
    owner = User.objects.get(username=username)

    # update or add is taking place
    if ((action == 'update') or (action == 'addnew')):
      if (len(request.POST['name'].strip()) == 0):
        if (action == 'update'):
          ligand_set = DDLigandSet.objects.get(owner=request.user, id=ligandsetid)
          return {
            'template': 'html/ddligandsetinfo.html',
            'error': {
              'message': 'Operation Failed! Set Name Cannot be Blank.'
            },
            'context': {
              'ligand_set': ligand_set
            }
          }
        else:
          return {
            'error': {
              'message': 'Operation Failed! Set Name Cannot be Blank.'
            },
            'context': {
              'description': request.POST['description'],
              'name': request.POST['name']
            }
          }
      else:
        if (action == 'update'):
          ligand_set = DDLigandSet.objects.get(owner=request.user, id=ligandsetid)
          ligand_set.ligand_set_name = request.POST['name'].strip()
          ligand_set.description = request.POST['description'].strip()
          ligand_set.save()
          set_id = ligandsetid

        elif (action == 'addnew'):
          if LigandSetExists(request.POST['name'], request.user) is True:
            return {
              'error': {
                'message': 'Operation Failed! Duplicate Project Found.'
              },
              'context': {
                'description': request.POST['description'],
                'name': request.POST['name']
              }
            }

          ligand_set = DDLigandSet()
          ligand_set.owner = owner
          ligand_set.ligand_set_name = request.POST['name'].strip()
          ligand_set.description = request.POST['description'].strip()
          ligand_set.save()

          added_ligand_set = DDLigandSet.objects.filter(owner=request.user).order_by('-id')[0]  # filter[0] is intended
          set_id = added_ligand_set.id

      ligand_set = DDLigandSet.objects.get(owner=request.user, id=set_id)
      return {
        'template': 'html/ddligandsetinfo.html',
        'error': {
          'message': 'Ligand set successfully created.'
        },
        'context': {
          'ligand_set': ligand_set
        }
      }

    # no action, just display blank or populated form
    else:
      if (ligandsetid != '0'):
        ligand_set = DDLigandSet.objects.get(owner=request.user, id=ligandsetid)
        return {
          'template': 'html/ddligandsetinfo.html',
          'context': {
            'ligand_set': ligand_set
          }
        }
      else:
        return {
          'context': {
            'description': ''
          }
        }
