from django.contrib.auth.models import User

from charmming.views import BaseView
from charmming.forms import AssayForm, AssayTrainingSubmitForm
from charmming.models import QSARModel, QSARModelType

class AssayContinueView(BaseView):
  def post(self, request, *args, **kwargs):
    if 'filename' in request.POST:
      filename_request = request.POST['filename']
      if 'back' in request.POST:
        query = request.POST['query']
        form = AssayForm(request.POST, query=query)
        return {
          'template': 'assays/assays.html',
          'context': {
            'form': form
          }
        }
      form = AssayTrainingSubmitForm(request.POST)
      if form.is_valid():
        filename = filename_request
        u = User.objects.get(username=request.user.username)
        newmodel = QSARModel()
        newmodel.model_owner = request.user
        newmodel.model_owner_index = newmodel.getNewModelOwnerIndex(u)
        newmodel.model_name = request.POST['model_name']
        newmodel.model_type = QSARModelType.objects.get(id=request.POST['model_type'])
        newmodel.save()
        query = request.POST['query']
        num_mols = request.POST['num_mols']
        return {
          'redirect': '/qsar/{}/{}/{}/{}'.format(filename, newmodel.id, query, num_mols)
        }
    elif 'filename_request' in request.POST:
      filename = request.POST['filename_request']
      query = request.POST['query']
      num_mols = request.POST['num_mols']
    form = AssayTrainingSubmitForm(filename=filename, query=query, num_mols=num_mols)

    return {
      'context': {
        'form': form,
        'query': query,
        'num_mols': num_mols
      }
    }
