from charmming.views import BaseView

import collections
import gc

class GCView(BaseView):
  admin = True

  def get(self, request, *args, **kwargs):
    objs = collections.Counter()

    for o in gc.get_objects():
      t = type(o)
      objs[str(t)] += 1

    return {
      "context": {
        "gc": objs
      }
    }
