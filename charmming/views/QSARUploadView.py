from charmming.views import BaseView
from charmming.forms import QsarTrainingUploadForm
from charmming.utils.qsar import handle_uploaded_file, get_dir, qsar_property

from tempfile import mkstemp
import os

class QSARUploadView(BaseView):
  template_name = 'qsar/upload.html'

  def get(self, request, *args, **kwargs):
    form = QsarTrainingUploadForm()

    return {
      'context': {
        'form': form
      }
    }

  def post(self, request, *args, **kwargs):
    form = QsarTrainingUploadForm(request.POST, request.FILES)
    if form.is_valid():
      (fh, name) = mkstemp(dir=get_dir(request))
      os.close(fh)
      handle_uploaded_file(request.FILES['trainfile'], name)
      return qsar_property(request, name)

    return {
      'context': {
        'form': form
      }
    }
