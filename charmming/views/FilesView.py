from charmming.views import BaseView
from charmming.helpers.views import Accountinator

import os

class FilesView(BaseView):
  template_name = 'html/viewfile.html'

  @Accountinator(struct_only=True)
  def get(self, request, filename, *args, **kwargs):
    """
    Lets the user view a file in their browser directly, as text. Only works for human-readable stuff like PDBs, PSFs, CRDs, inps, outs.
    """
    # NOTE: the below kwarg comes in from Accountinator
    struct = kwargs.get("struct")
    try:
      os.stat("%s/%s" % (struct.location, filename))
    except:
      return {
        'error': {
          'message': 'The file {}/{} does not exist.'.format(struct.location, filename)
        },
        'redirect': '/download'
      }

    fname = "%s/%s" % (struct.location, filename)
    fp = open(fname, 'r')
    fcontent = fp.read()
    fp.close()

    return {
      'context': {
        'fcontent': fcontent
      }
    }
