from charmming.views import BaseView
from charmming.models import Structure, WorkingStructure

class PropkaDisplayView(BaseView):
  template_name = 'html/propka_finished.html'

  def get(self, request, filename, *args, **kwargs):
    # displays PROPKA result, allows display without download of output from page
    tdict = {}
    try:
      struct = Structure.objects.filter(owner=request.user, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'Please upload a structure first.'
        },
        'redirect': '/fileupload'
      }
    try:
      ws = WorkingStructure.objects.filter(structure=struct, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'A working structure needs to be uploaded before PROPKA analysis can be performed on your system.'
        },
        'redirect': '/buildstruct'
      }

    output_lines = []
    try:
      propka_file = open(("%s/%s" % (ws.structure.location, filename)), "r")
      for line in propka_file:
        output_lines.append(line)
      propka_file.close()
      tdict['output_lines'] = output_lines
    except:
      return {
        'error': {
          'message': 'Your PROPKA output could not be displayed. Perhaps the file is no longer there. Please re-do your PROPKA calculations, then try again.'
        },
        'redirect': '/analysis/propka'
      }

    return {
      'context': tdict
    }
