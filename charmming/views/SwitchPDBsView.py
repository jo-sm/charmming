from charmming.views import BaseView
from charmming.models import Structure

class SwitchPDBsView(BaseView):
  template_name = 'html/switchpdb.html'

  def get(self, request, switch_id, *args, **kwargs):
    try:
      oldfile = Structure.objects.filter(owner=request.user, selected='y')[0]
      oldfile.selected = ''
      oldfile.save()
    except:
      pass
    newfile = Structure.objects.filter(owner=request.user, name=switch_id)[0]

    newfile.selected = 'y'
    newfile.save()

    return {
      'context': {
        'oldfile': oldfile,
        'newfile': newfile
      }
    }
