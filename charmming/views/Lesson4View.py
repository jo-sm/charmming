from charmming.views import BaseView
from charmming.models import Structure, LessonProblem, Lesson4

class Lesson4View(BaseView):
  template_name = 'html/lesson4.html'

  def get(self, request, *args, **kwargs):
    try:
      file = Structure.objects.get(owner=request.user, selected='y')
    except:
      return {}
    # If its a lesson1 object, get the id by the file id
    if file.lesson_type == 'lesson4':
      lesson_obj = Lesson4.objects.filter(user=request.user, id=file.lesson_id)[0]
      html_step_list = lesson_obj.getHtmlStepList()
    else:
      lesson_obj = None
      html_step_list = None

    try:
      lessonprob_obj = LessonProblem.objects.filter(lesson_type='lesson4', lesson_id=lesson_obj.id)[0]
    except:
      lessonprob_obj = None

    return {
      'context': {
        'lesson4': lesson_obj,
        'lessonproblem': lessonprob_obj,
        'html_step_list': html_step_list
      }
    }
