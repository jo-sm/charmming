from charmming.views import BaseView
from charmming.forms import AssaySearchForm

class AssaySearchView(BaseView):
  template_name = 'assays/search.html'

  def get(self, request, *args, **kwargs):
    form = AssaySearchForm()

    return {
      'context': {
        'form': form
      }
    }

  def post(self, request, *args, **kwargs):
    form = AssaySearchForm(request.POST)

    if form.is_valid():
      query = request.POST['query']

      return {
        'redirect': '/assays/assays?query={}'.format(query)
      }
    else:
      return {
        'context': {
          'form': form
        }
      }
