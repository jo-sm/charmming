from charmming.views import BaseView
from charmming.models import Task, WorkingFile
from charmming.helpers.views import Accountinator

class VisualizationView(BaseView):
  template_name = 'html/visualize.html'

  @Accountinator()
  def get(self, request, *args, **kwargs):
    """
    Allows the user to visualize their structure via various visualization methods.
    """
    # NOTE: The following two kwargs come in from accountinator
    cg_model = kwargs.get("cg_model")
    ws = kwargs.get("ws")
    filelist = []

    # files from segment construction
    if not cg_model:  # CG models don't have segments.
      for wseg in ws.segments.all():
        filelist.append((wseg.builtCRD.replace('.crd', '.pdb'), 'Segment %s' % wseg.name))

    # now get all tasks associated with the structure
    tasks = Task.objects.filter(workstruct=ws, active='y')
    for task in tasks:
      # check for PDB files associated with the task
      wfiles = WorkingFile.objects.filter(task=task, type='pdb')
      for wfile in wfiles:
        s = wfile.canonPath.split('/')[-1]
        filelist.append((s, wfile.description))

    return {
      'context': {
        'filelist': filelist
      }
    }
