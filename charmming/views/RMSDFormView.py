from charmming.views import BaseView
from charmming.models import Structure, WorkingStructure, Task
from charmming.helpers.input import checkRequestData
from charmming.utils.analysis import doRMSD

import os

class RMSDFormView(BaseView):
  template = 'html/rmsdform.html'

  def get(self, request, *args, **kwargs):
    checkRequestData(request)

    # chooses the file based on if it is selected or not
    try:
      struct = Structure.objects.filter(owner=request.user, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'Please upload a structure first.'
        },
        'redirect': '/fileupload'
      }
    try:
      ws = WorkingStructure.objects.filter(structure=struct, selected='y')[0]
    except:
      return {
        'error': {
          'message': 'Please build your structure first.'
        },
        'redirect': '/buildstruct'
      }

    # feedback = ''
    tasks = Task.objects.filter(workstruct=ws, status='C', active='y', modifies_coordinates=True)
    tdict = {}
    tdict['ws_identifier'] = ws.identifier
    tdict['tasks'] = tasks
    rmsd_list = []
    for t in tasks:
      if 'dotask_%d' % t.id in request.POST:
        rmsd_list.append(t)
    if len(rmsd_list) > 0:
      if len(rmsd_list) == 1:
        tdict['error'] = {
          'message': 'More than 1 structure must be selected.'
        }
      else:
        picklefile = struct.pickle
        if ws.localpickle is not None:
          picklefile = ws.localpickle
        return doRMSD(request.user, request.POST, ws.structure.location, ws.identifier, picklefile, rmsd_list)
    else:
      prior_matrix = ''
      try:
        os.stat(struct.location + '/rmsd-' + ws.identifier + '.html')
      except:
        pass
      else:
        fp = open(struct.location + '/rmsd-' + ws.identifier + '.html')
        for line in fp:
          prior_matrix += line
        fp.close()
        tdict['prior_matrix'] = prior_matrix

    return {
      'context': tdict
    }
