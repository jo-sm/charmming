from charmming.views import BaseView
from charmming.models import Structure

class JobBaseView(BaseView):
  def pre_post(self, request, *args, **kwargs):
    user = request.session.user
    data = request.POST

    structure_id = data.get('structure')

    if not structure_id:
      return {
        'error': {
          'message': 'Select a structure.'
        }
      }

    # The user may choose a different Structure (referred
    # in the UI as coordinates) for this job.
    #
    # The coordinates are a child of the given Structure id.
    if data.get('coords'):
      coordinates_id = data.get('coords')
      structures = Structure.objects.filter(id=coordinates_id, parent__id=structure_id, owner=user)
    else:
      structures = Structure.objects.filter(id=structure_id, owner=user)

    if len(structures) != 1:
      return {
        'error': {
          'message': 'Select a structure.'
        }
      }

    structure = structures[0]

    setattr(request, 'structure', structure)
