from charmming.views import BaseView
from charmming.models import DDProject

class DDProjectsView(BaseView):
  template_name = 'html/ddviewprojects.html'

  def get(self, request, *args, **kwargs):
    user_projects = DDProject.objects.filter(owner=request.user)

    return {
      'context': {
        'user_projects': user_projects
      }
    }
