#!/usr/bin/env python3

from charmming.utils.datagram import unpack
from charmming.utils.log import log_wrapper

from notificationd.NotificationProtocol import serve

import asyncio
import aioredis
import json
import signal
import os
import sys

log = log_wrapper('notificationd', 'info')

class NotificationDaemon:
  def __init__(self):
    self.redis_host = os.environ.get('REDIS_HOST')
    self.redis_port = 6379

    if not self.redis_host:
      raise Exception('Environment variable REDIS_HOST must be set.')

    self.loop = asyncio.get_event_loop()
    self.queue = {}
    self.ws_connections = {}

    self.loop.run_until_complete(self.__setup_redis())
    self.server_coroutine = serve(self.__connection_handler, self.__disconnection_handler, '127.0.0.1', 9487)

  def start(self):
    log('Starting Notification Daemon.')
    self.loop.run_until_complete(self.server_coroutine)
    self.loop.run_forever()

  def stop(self):
    self.loop.stop()

  async def __setup_redis(self):
    sub = await aioredis.create_redis((self.redis_host, self.redis_port))
    self.notification_channel = (await sub.subscribe('charmming.notifications'))[0]
    self.notification_task = self.loop.create_task(self.__handle_notification())

  async def __connection_handler(self, websocket, data):
    if type(data) is not dict:
      return

    if not data.get('username'):
      return

    username = data['username']

    self.ws_connections[username] = websocket

    peername = websocket.writer.get_extra_info('peername')

    log('Username {} connection from {}'.format(username, peername))

  async def __disconnection_handler(self, websocket):
    for username, socket in self.ws_connections.items():
      if socket == websocket:
        log('Disconnection from {}'.format(username))

        del self.ws_connections[username]

        break

  async def __handle_notification(self):
    while True:
      raw = await self.notification_channel.get()
      data = unpack(raw)
      log('Received notification {}'.format(data))

      notification_raw = data.get('data')

      # The notification should be JSON serializable
      try:
        notification = json.dumps(notification_raw)
      except json.decoder.JSONDecodeError:
        continue

      if not notification:
        # The data from Redis should contain a `data` key. If it doesn't
        # it is malformed and we can ignore it.
        continue

      if data.get('username'):
        username = data['username']

        if self.ws_connections.get(username):
          log('Sending {} to {}'.format(notification, username))
          await self.ws_connections[username].send(notification)
        else:
          log('Adding {} to {} queue'.format(notification, username))
          # Add message to queue for username

          if not self.queue.get(username):
            self.queue[username] = []

          self.queue[username].append(notification)
      else:
        for connection in list(self.ws_connections.items()):
          self.loop.create_task(connection.send(notification))

def handle_term(server):
  def result_function(*args) -> None:
    log("Got shut down signal.")
    server.stop()
    sys.exit(0)

  return result_function

if __name__ == "__main__":
  daemon = NotificationDaemon()

  signal.signal(signal.SIGINT, handle_term(daemon))
  signal.signal(signal.SIGTERM, handle_term(daemon))

  daemon.start()
