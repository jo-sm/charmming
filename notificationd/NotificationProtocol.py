from charmming.utils.encryption import encrypt
from charmming.utils.log import log_wrapper

from http.cookies import BaseCookie

import email.message

import asyncio
import aiohttp
import websockets
import json
import os

log = log_wrapper('notificationd', 'info')

async def serve(conn_handler, disconn_handler, host, port):
  """
  A modified version of `websockets.serve`. In addition to the
  connection handler (`conn_handler`), it also requires a
  disconnection handler (`disconn_handler`) to be set.

  This method is returns a coroutine.
  """
  loop = asyncio.get_event_loop()
  ws_server = websockets.server.WebSocketServer(loop)

  def factory():
    return NotificationProtocol(ws_server, conn_handler, disconn_handler, host, port)

  asyncio_server = await loop.create_server(factory, host, port)
  ws_server.wrap(asyncio_server)

  return ws_server

class NotificationProtocol(websockets.server.WebSocketServerProtocol):
  def __init__(self, ws_server, conn_handler, disconn_handler, host, port):
    self.loop = asyncio.get_event_loop()
    self.conn_handler = conn_handler
    self.disconn_handler = disconn_handler
    self.ws_server = ws_server

    super().__init__(self.conn_handler, self.ws_server, host=host, port=port, loop=self.loop)

  async def handler(self):
    """
    See https://github.com/aaugustin/websockets/blob/c46ab352f6046522b4ac096418367bbd12d76ae6/websockets/server.py#L65

    The original handler always closed the connection when the defined `conn_handler` is finished, and
    there is no way to not have that happen. This handles part of the original handling code, but the
    connection (self) is not automatically closed. Additionally, it adds a pinging mechanism to ensure
    that the connection is kept open.
    """
    def error(response):
      self.writer.write(response.encode('utf-8'))
      self.writer.close()
      self.ws_server.unregister(self)

    try:
      data = await self.handshake()
    except ConnectionError as exc:
      response = ('HTTP/1.1 500 Internal Server Error\r\n\r\n'
                  'Try again later.')
      return error(response)
    except Exception as exc:
      if self._is_server_shutting_down(exc):
          response = ('HTTP/1.1 503 Service Unavailable\r\n\r\n'
                      'Server is shutting down.')
      elif isinstance(exc, websockets.exceptions.InvalidOrigin):
          response = 'HTTP/1.1 403 Forbidden\r\n\r\n' + str(exc)
      elif isinstance(exc, websockets.exceptions.InvalidHandshake):
          response = 'HTTP/1.1 400 Bad Request\r\n\r\n' + str(exc)
      else:
          response = ('HTTP/1.1 500 Internal Server Error\r\n\r\n'
                      'Try again later.')
      return error(response)

    if data is None:
      response = ('HTTP/1.1 500 Internal Server Error\r\n\r\n'
                  'Try again later.')
      error(response)
      return

    try:
      await self.conn_handler(self, data)
    except Exception as exc:
      if self._is_server_shutting_down(exc):
        await self.fail_connection(1001)
      else:
        await self.fail_connection(1011)

      response = ('HTTP/1.1 500 Internal Server Error\r\n\r\n'
                  'Connection Handler: {}'.format(exc))
      return error(response)

    self.loop.create_task(self.pingpong())

  async def pingpong(self):
    try:
      await self.ping()
    except websockets.exceptions.ConnectionClosed:
      # The connection was closed
      try:
        await self.disconn_handler(self)
      except Exception as exc:
        log('Websocket disconnection handler returned an exception: {}'.format(exc), level='critical')

      return

    await asyncio.sleep(15)

    self.loop.create_task(self.pingpong())

  async def handshake(self):
      """
      See https://github.com/aaugustin/websockets/blob/c46ab352f6046522b4ac096418367bbd12d76ae6/websockets/server.py (websockets 3.3)

      This is overwritten due to the way that it is originally written. There's no way to handle auth
      during the handshake before the handshake response is sent, so this is copied essentially
      verbatim with the addition of checking for an auth cookie in the request headers.

      An additional change is that it returns a dict that contains the path, as well as
      the auth cookie username, in the following format:

      {
        'uri': <path>,
        'username': <username>
      }
      """

      # Read handshake request.
      try:
          path, headers = await websockets.http.read_request(self.reader)
      except ValueError as exc:
          raise websockets.exceptions.InvalidHandshake("Malformed HTTP message") from exc

      self.request_headers = headers
      self.raw_request_headers = list(headers.raw_items())

      # Verify the auth cookie
      if not headers.get('cookie'):
        raise websockets.exceptions.InvalidHandshake("Auth cookie missing")

      cookies = BaseCookie(headers.get('cookie'))
      auth_cookie = cookies.get('notification-token')

      if not auth_cookie:
        raise websockets.exceptions.InvalidHandshake("Auth cookie missing")

      key = os.environ.get('CHARMMING_SECRET_KEY').encode('utf-8')
      data = encrypt(key, json.dumps({ 'cookie': auth_cookie.value }).encode('utf-8'))

      async with aiohttp.request(
        'get',
        'http://127.0.0.1/verify_notification_cookie',
        params={ 'data': data },
        headers={
          'accept': 'application/json',
          'x-charmming-app': 'notificationd'
        }
      ) as response:
        try:
          data = await response.json()
        except aiohttp.client_exceptions.ClientResponseError:
          raise websockets.exceptions.InvalidHandshake("Invalid auth cookie")

      username = data.get('username')

      if not username:
        raise websockets.exceptions.InvalidHandshake("Invalid auth cookie")

      def get_header(key):
        return headers.get(key, '')

      key = websockets.handshake.check_request(get_header)

      headers = []

      def set_header(key, value):
        headers.append((key, value))

      set_header('Server', websockets.http.USER_AGENT)
      if self.subprotocol:
          set_header('Sec-WebSocket-Protocol', self.subprotocol)

      websockets.handshake.build_response(set_header, key)

      self.response_headers = email.message.Message()
      for name, value in headers:
          self.response_headers[name] = value
      self.raw_response_headers = headers

      # Send handshake response. Since the status line and headers only
      # contain ASCII characters, we can keep this simple.
      response = ['HTTP/1.1 101 Switching Protocols']
      response.extend('{}: {}'.format(k, v) for k, v in headers)
      response.append('\r\n')
      response = '\r\n'.join(response).encode()
      self.writer.write(response)

      assert self.state == websockets.protocol.CONNECTING
      self.state = websockets.protocol.OPEN
      self.opening_handshake.set_result(True)

      return {
        'uri': path,
        'username': username
      }
