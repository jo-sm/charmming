# Ensures that the script is running in the main project dir
ensure_correct_dir() {
  if [ ! -s "Makefile" ]; then
    echo "Error: Script must be run in the root directory of the project."
    exit 1
  fi
}

# Checks that postgres is running. Tests against the container, which exposes the default post for psql
test_postgres() {
  while true; do
    output=$(PGPASSWORD=${POSTGRES_PASS} psql -U ${POSTGRES_USER} -h localhost -d postgres -c "\\h" 2>/dev/null)

    if [ -n "$output" ]; then
      break
    fi
  done
}

test_charmming() {
  # Test it 20 times
  local count=$1

  if [ -z "$count" ]; then
    count=1
  fi

  if [ "$count" -gt "20" ]; then
    echo "\nThe charmming container was not able to be connected to."
    exit 3
  fi

  curl 127.0.0.1:8080 >/dev/null 2>&1

  if [ "$?" -ne "0" ]; then
    printf "."

    # Wait 1 second before testing again
    sleep 0.1
    test_charmming $((count+1))
  fi
}

detect_or_create_db() {
  PGPASSWORD=${POSTGRES_PASS} psql -U ${POSTGRES_USER} --host localhost -d postgres -c "create database ${1};" >/dev/null 2>/dev/null
}
