from charmming-base-extended:latest

COPY . /app
COPY ./config/nginx.conf /etc/nginx/nginx.conf
WORKDIR /app

# Create upload dir
RUN mkdir files 2>/dev/null

# Install libpq
RUN apt-get update && \
    apt-get install -y libpq5

# Installs make as well
RUN export COMPONENTS="make libffi-dev libpq-dev git gcc-6 g++-6" &&\
    apt-get update &&\
    apt-get install -y $COMPONENTS &&\
    export CC="gcc-6" &&\
    export CXX="g++-6" &&\
    pip install -r requirements.txt &&\
    apt-get purge -y $COMPONENTS &&\
    apt-get autoremove -y &&\
    apt-get clean

# Start app
ENV PYTHONPATH=/app:$PYTHONPATH
CMD ldconfig &&\
    gunicorn -b 0.0.0.0:8080 --error-logfile - -w 1 -p /tmp/gunicorn.pid charmming.wsgi:application
EXPOSE 8080
