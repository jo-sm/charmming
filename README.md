# CHARMMing -- CHARMM Interface and Graphics

CHARMMing is a web interface for CHARMM and Q-chem, with lessons for learning the interface and interfaces to tools such as drug design, visualization, and simulations.

## Installation

See [INSTALLATION.md](INSTALLATION.md) for detailed installation instructions.

## Running tests

We use `pytest` for the automated unit and integration test suite. To run the tests, run `test_runner.sh` from the root project directory. To enable debug information (log output, more verbosity) you can append `--debug` to the script during execution.

Tests are ran by the CI and are a requirement for any merge request.

## Type checking

We use `mypy` for type checking, with some additional `mypy` log parsing in place. To run the `mypy` checker, run `mypy_runner.py` in the root project directory. While mypy is optional and will not cause failure during CI, it is expected that any new code be type checked and have types for at least function definitions.

Note: Because of the way that imports are handled at runtime for certain modules, e.g. `charmming.models`, `charmming.views`, mypy will fail with obtuse errors such as `"module" is not callable`. Ensure that the type definition of the class is in the `__init__.pyi` file of the module directory.
