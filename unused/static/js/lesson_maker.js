var objective_count = 1; //keeps track of what objective we're at, so we can
//populate the fields usefully. The user will probably want the objectives displayed
//in the same order, and it's not totally trustable that they will be returned in the same order
function add_objective(){
  objective_count = objective_count + 1;
  $(document.getElementById("objectives")).append("<input type='text' class='objective_boxes' name='objective "+(objective_count+1)+"'><br>");
  //this adds a new objective box to the list, with 'objective 2' for example if you added when the
  //only input present was 'objective 1'.
}

function check_inputs(){
  //this function is going to take all of the textboxes we design
  //and then it's going to check them for <> so that there can be no
  //HTML tags inside of it. We don't want scripts to execute, after all
  //and django does autoescape for the rest.
  //TODO: replace these checks with a serverside check via AJAX
  $(".lesson_input").each(function(){
    var spec_chars = "!@#$%^&*()`~+=-[]\\\';,./{}|\":<>?";
    for (var i=0;i<this.value.length;i++){
      if(spec_chars.indexOf(this.value.charAt(i)) != -1){
        $("#dialog_spec_char_alert").html('You cannot have any of the following characters in your lesson text:<br>' + spec_chars);
        $("#dialog_spec_char_alert").dialog("open");
        return false;
      }
    }
  });
  /*
   Django's autoescape is virulent enough to not allow <> to be a problem, thankfully
    var spec_chars = "<>"; //these are the only ones we care about in the textareas since they won't get written to .py files
      //we may implement allowing tags in the future.
    $(".lesson_text").each(function(){
     for(var i=0;i<this.value.length;i++){
      if(spec_chars.indexOf(this.value.charAt(i)) != -1){
        $("#dialog_spec_char_alert").html('You cannot have any of the following characters in your ligand name:<br>' + spec_chars);
        $("#dialog_spec_char_alert").dialog("open");
        return false;
      }
    }
    });
    */
    return true;
}


$(function($){
    $("#dialog_spec_char_alert").dialog({
      resizable:false,
      height:200,
      width:600,
      modal:true,
      autoOpen:false,
      buttons:{
      "OK":function(){
      $(this).dialog("close");
      }
    }
  });
});

$("#add_objective").click(function(){
    add_objective();
  });

$(".lesson_submit_button").click(function(){
    var arf = check_inputs();
    if(arf){
      $("#submit_lesson").submit();
      }
    return false;
  });

$(".lesson_text").click(function(){
  $(this).val("");
  });
