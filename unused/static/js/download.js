var folder = new Object(); //This object will hold all the folders and files we get.
folder.path = $(".file_holder").attr("id") + "/"; //so you get ws.identifier
folder.contents =null;
folder.type = "folder"; //we branch here.
folder.name = folder.path; //We leave this here as a standard - we don't want to have to be doing splitting over and over again.
folder.root = null; //this holds "one level up"

var current_folder = folder; //this one keeps track of which folder we're at.


function check_folder_cache(path,check_folder){
  if (check_folder.contents == null){
    get_files(path,check_folder); //this should be done always, since we need to check this in both cases.
    check_folder.root = current_folder;
    current_folder = check_folder;
    return;
  }
  if (path == check_folder.path){ //if you're in the right path...
      update_file_container(check_folder); //use data that was either cached, or you just got.
      if(check_folder.path.indexOf(check_folder.root.path) == -1){ //if the root path isn't a substring of your current path - i.e. you're going "up"
        check_folder.root = current_folder;
        current_folder = check_folder;
      }
      //no need to update current_folder
  }else{ //you're not in the right path
    //first, let's check the root, since we can only go up one level at a time
    if(path == check_folder.root.path){
      update_file_container(check_folder.root);
    }else{
      for(var i=0;i<check_folder.contents.length;i++){ //check every folder
        var content = check_folder.contents[i];
        if(content.type == "folder"){ //if it is a folder
          if(content.path == path){ //oh man, it matched!
            if(content.contents == null){ //but it's empty, so we need to fetch the data
              get_files(path,content);
              content.root = current_folder;
              current_folder = content;
            }
            update_file_container(content); //now we can use the data!
            break;
          }else{ //we aren't in the right path
            if (path.indexOf(content.path) != -1){ //this should always be true. No folder names should be able to be substrings of each other.
              //even SGLD versus LD - /ws/sgld/ and /ws/ld/ - when you pass that whole string in, only one will return indexOf > -1
              //this means this is the subpath of the correct path, so we can recurse, but it won't balloon a lot because this will only
              //ever evaluate true once (I hope!)
              check_folder_cache(path,content);
              break;
            }
          }
        }
      }
    }
    }
}

function bind_click_events(){
  $(".file_holder a").click(function(){
    check_folder_cache(this.attr("href"),current_folder); //this handles all the updating anyway
  });
}
//This function just calls an AJAX request for getting the files and displays an error otherwise.
//The Python code can be modified to return failure in case of any other stuff.
function get_files(path,update_folder){
 //path can be "", so don't worry about it much
 $.ajax({
  contentType:"application/json",
  url:'/download_table/'+path,
  method:'post',
  success:function(responseJSON){
    var new_files = responseJSON; //responseData has all the data we need
    //first, let's add stuff to our folder object(s)
    if(new_files.type != null){ //Only if it's not null should we add it to our data structure. Else proceed to error display.
      update_folder.contents = new_files.contents; //that's it. root and everything else is the same, we only need to change contents.
      //Python should handle building the objects and making them into JSON.
    }
    update_file_container(new_files);
    return true;
  },
  failure:function(){
    $(".files_table").hide();
    $(".loading_tile").html("Error while loading file contents. Your files may have changed recently. Please reload the page.");
    return false;
  },
  error:function(){
    $(".files_table").hide();
    $(".loading_tile").html("Error while loading file contents. Your files may have changed recently. Please reload the page.");
    return false;
  }});
}

//This function actually processes the response from Python.
function update_file_container(new_files){
  if(new_files.type == null){ //something bad just happened - you tried to access a path that doesn't exist, so we just render the root.
    if (folder == null){
      $(".loading_tile").html("Error loading your working structure's files. They may not be there anymore. Check if your working structure still exists, and if not, please inform the system administrator.");
      return false;
    }else{
     $(".loading_tile").html("Your file/folder could not be found. It is possible that your working structure's files have changed recently. We're returning to the root of your files, for now.");
     update_file_container(folder);
    }
    $(".loading_tile").show();
    return;
  }
  $(".files_table").hide();
  $(".loading_tile").show();
  var inner_html = "<tr><th class='nosort'>Content Name</th><th class='text sortfirstdesc'>Content Type</th></tr>";
  if(new_files.root != null){
    inner_html = inner_html + "<tr><td><a href='" +current_folder.root.path +"'>Go up one level</a></td></tr>";
  }
  console.log(new_files);
  console.log(new_files.contents);
  var content = new_files.contents;
  if (content != null && content.length > 0){
    for(var i=0;i<content.length;i++){
        var new_cont = content[i];
      if (content[i].type == "file"){
        inner_html = inner_html + "<tr><td><a href='"+new_cont.download_link+"'>"+new_cont.name+"</a></td><td>";
        if (content[i].is_displayable){
          inner_html = inner_html + " <a href='"+new_cont.view_link+"'>File</a>";
        }else{
          inner_html = inner_html + "File";
        }
        inner_html = inner_html + "</td></tr>";
      }else{
        inner_html = inner_html + "<tr><td><a href='"+new_cont.path+"'>"+new_cont.name+"</a></td><td>Folder</td></tr>";
    }
  }

  }
  $(".files_table").html(inner_html);
  $(".loading_tile").hide();
  $(".files_table").jqTableKit();
  $(".sortfirstdesc").attr("class","sortdesc");
  $(".files_table").show();
}

  $(".files_table").click(function(event){
    if (event.target.nodeName == "A"){
      var arf = $(event.target).attr("href");
      if(arf.indexOf("pdbuploads") == -1 && arf.indexOf("viewprocessfiles") == -1){ //it's a folder
        event.preventDefault();
        check_folder_cache(arf,current_folder);
      }
    }
  });
check_folder_cache(folder.path,folder);
