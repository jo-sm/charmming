//functions for starting up jsmol

function initializeDisplay(){
  //this just calls scripts such that JSmol starts up with protein cartoons and everything else as ballandstick
  Jmol.script(jmolApplet,"select amino or [HSD] or [HSE] or [HSP] or [LSN];cartoons on;cpk 0;wireframe 0;color structure;slab off;spin off;trace off;set ambient 40;set specpower 40;select none;");
}
