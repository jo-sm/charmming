$(".region_options").hide(); //top priority - don't let anyone see these.

var selection = false; //This keeps track of if you have made a selection, so we don't have to keep going to getPropertyAsArray
var show_hydrogens = true;
var show_all_protein = false; //This is when it's selected...important for showing hydrogens

//This is for badhet selection on the build structure page
function showUploads(segdiv){
    var segname = segdiv.split("_");
    segname = segname[segname.length - 1]; //Get the last part...
    if ($("#"+segdiv).val() == "upload"){
      $("#toppar_upload_"+segname).css("display","inline");
    }else{
      $("#toppar_upload_"+segname).css("display","none");
    }
}

function change_atom_proto(selection_string,plus,resid){
  //Calls the Jmol script for protonating or deprotonating an atom.
  //plus is expected to be a boolean stating whether to increase or decrease charge by 1.
  //selection_string should be a valid Jmol selection string.
  //resid should be the selection string of the residue so that we can fix issues with selectionhalos.
  //selectionhalos are turned off, then assign atom is made, then selection made and halos turned back on
  //due to a bug in JSmol where calling assign atom selects all atoms in the structure.
  return true;
}

function change_proto_state(newresbox, oldresid, segid){
  if(show_hydrogens){ //If show_hydrogens is false, why should we go through all this work?
    var proto_state_identifier = $(newresbox).val(); //i.e. LSN, ASP, etc.
    //We can rely on formalCharge given that everything has hydrogens on it to balance out the valences
    //and formalCharge is updated by JSmol whenever we change the charge of something.
    //However, it should NOT be trusted for ANYTHING else!
    var isHSD = proto_state_identifier.toUpperCase() == "HSD"; //declare these up here so we don't have them clutterng up the if statement when we have to reuse them
    var isHSE = proto_state_identifier.toUpperCase() == "HSE";
    var isHSP = proto_state_identifier.toUpperCase() == "HSP";
    var selection_string = ":" + segid[0].toUpperCase() + ".atomname and resno="+oldresid; //atomname will be replaced by the name of the atom we want to (de)protonate
    //Now goes a series of if statements for every residue
    if (proto_state_identifier.toUpperCase() == "LSN" || proto_state_identifier.toUpperCase() == "LYS"){ //Lysine
      selection_string = selection_string.replace("atomname","NZ")
      var NZ = Jmol.getPropertyAsArray(jmolApplet, "atominfo", selection_string);
     if(NZ.length < 1){
      //Make a fancy error box
    }else{
      if (NZ[0].formalCharge > 0){
        if(proto_state_identifier.toUpperCase() == "LSN"){
          //Deprotonate
          Jmol.script(jmolApplet, 'assign atom ('+selection_string+') "Mi"');
        }
        //Otherwise do nothing
      }else{
        if(proto_state_identifier.toUpperCase() == "LYS"){
          //Protonate
          Jmol.script(jmolApplet, 'assign atom ('+selection_string+') "Pl"');
        }
        //If it's LSN and deprotonated, do nothing
      }
    }
    }else if(proto_state_identifier.toUpperCase() == "GLU" || proto_state_identifier.toUpperCase() == "GLUP"){ //Glutamic acid
      selection_string = selection_string.replace("atomname","OE2")
      var OE2 = Jmol.getPropertyAsArray(jmolApplet, "atominfo", selection_string);
      if(OE2.length < 1){
        //Fancy error box
      }else{
        if(OE2[0].formalCharge < 0){
          if(proto_state_identifier.toUpperCase() == "GLUP"){ 
            //Protonate
            Jmol.script(jmolApplet, 'assign atom ('+selection_string+') "Pl"');
          }
          //Do nothing otherwise, GLU is -1 charge
        }else{
          if(proto_state_identifier.toUpperCase() == "GLU"){
            //Deprotonate
            Jmol.script(jmolApplet, 'assign atom ('+selection_string+') "Mi"');
          }
        }
      }
    }else if(proto_state_identifier.toUpperCase() == "ASP" || proto_state_identifier.toUpperCase() == "ASPP"){
      selection_string = selection_string.replace("atomname","OD2");
      var OD2 = Jmol.getPropertyAsArray(jmolApplet, "atominfo", selection_string);
      if(OD2.length < 1){
        //Fancy error box
      }else{
        if(OD2[0].formalCharge < 0){
         if(proto_state_identifier.toUpperCase() == "ASPP"){
          Jmol.script(jmolApplet, 'assign atom ('+selection_string+') "Pl"');
        }
      }else{
        if(proto_state_identifier.toUpperCase() == "ASP"){
          Jmol.script(jmolApplet, 'assign atom ('+selection_string+') "Mi"');
        }
      }
    }
    }else if(isHSD || isHSP || isHSE){
      //get both nitrogens involved, selectively protonate. This is a long function.
      selection_ND1 = selection_string.replace("atomname","ND1");
      selection_NE2 = selection_string.replace("atomname","NE2");
      var ND1 = Jmol.getPropertyAsArray(jmolApplet, "atominfo", selection_ND1);
      var NE2 = Jmol.getPropertyAsArray(jmolApplet, "atominfo", selection_NE2);
      if (ND1.length < 1 || NE2.length < 1){
        //how do you get an incomplete residue anyway?
        return false;
      }else{
        if(ND1[0].formalCharge <= 0){ //ND1 is deprotonated
          if(isHSD || isHSP){ //if it's HSD, and delta nitrogen is deprotonated, protonate it. Deprotonation of E-nitrogen will be done later. Similarly, if it's HSP, you should protonate it too, because both should be
            Jmol.script(jmolApplet, 'assign atom ('+selection_ND1+') "Pl"');
          }
        }else{ //ND1 is protonated
          if(isHSE){ //if it's HSE, you need to keep it deprotonated
            Jmol.script(jmolApplet, 'assign atom ('+selection_ND1+') "Mi"');
          }
        }
        //ok, done with ND1 checks. Now NE2 checks.
        if(NE2[0].formalCharge < 0){ //NE2 is deprotonated
          if(isHSE || isHSP){ //if it's HSE, or HSP, epsilon nitrogen should be protonated.
            Jmol.script(jmolApplet, 'assign atom ('+selection_NE2+') "Pl"');
          }
        }else{
          if(isHSD){ //if it's HSD, you should deprotonate the epsilon nitrogen
            Jmol.script(jmolApplet, 'assign atom ('+selection_NE2+') "Mi"');
          }
        }
      }        
    }else{
      return false;
    }
  Jmol.script(jmolApplet, "select resno="+oldresid);
  }else{
    return false;
  }
}


function select_residue(resid,segid){
  //H are added AFTER everything is loaded, so our current work is broken
  //restart from scratch
  var selection_string = ":"+segid[0].toUpperCase() + " and resno="+resid; //Use the first character of the segid to make this work by selecting chain, which should be easy with standard PDB
  selection = true;
  Jmol.script(jmolApplet, "select " + selection_string);
  Jmol.script(jmolApplet, "display within(5.0,selected);cpk 30%;wireframe 55;ribbons off;cartoons off; color jmol;center selected; zoom 0;selectionhalos on;"); //should ballandstickify
  if(!(show_hydrogens)){
    Jmol.script(jmolApplet, "display displayed and not hydrogen;");
  }
  $(".hidden_options").show();
}

function click_proto_box(){
  if($('#proto_box').is(':checked')){
    $('#proton_divs').css('display','inline');
    $(".submit_button").css("top","470px");
/*    var jsmol_height = parseInt($("#protonation_jsmol").css("height").replace("px",""));
    if(jsmol_height > proto_height){
      $("#protonation").css("height",jsmol_height + "px");
    }else{
      $("#protonation_jsmol").css("height",(proto_height-500)+"px"); //this works, but I have no idea why protonation's height varies so much
    } */
  }else{
  $(".submit_button").css("top","")
  $('#proton_divs').hide();}
}


function show_protein(){
if(show_all_protein){ //If you're already showing the full thing...
  if(show_hydrogens){
    Jmol.script(jmolApplet, "display within(5.0,selected)");
  }else{
  Jmol.script(jmolApplet, "display within(5.0,selected) and not hydrogen");
  Jmol.script(jmolApplet, "display displayed or within(5.0,selected) and solvent"); //So the solvent hydrogens aren't killed
  }
  show_all_protein = false;
}else{
  show_all_protein = true;
  if(show_hydrogens){
    Jmol.script(jmolApplet, "display all");
  }else{
    Jmol.script(jmolApplet, "display all and not hydrogen;display displayed or solvent;");
  }
}
  
}
function show_hide_hydrogens(){
if(!(show_hydrogens)){ //Here you already have something selected and want to kill off hydrogens
  show_hydrogens = true;
  if(selection){
    if(show_all_protein){
      Jmol.script(jmolApplet, "display all");
    }else{
      Jmol.script(jmolApplet, "display within(5.0,selected)");
    }
  }else{
    Jmol.script(jmolApplet, "display displayed or hydrogen;");
  }
}else{
  if(selection){
    if(show_all_protein){
      Jmol.script(jmolApplet, "display all and not hydrogen; display displayed or solvent;");
    }else{
      Jmol.script(jmolApplet, "display within(5.0,selected) and not hydrogen;display displayed or solvent and within(5.0,selected)");
    }
  }else{
    Jmol.script(jmolApplet, "display displayed and not hydrogen;display displayed or solvent");
  }
  show_hydrogens = false;
}
}

$(".submit_button").click(function(event){
  event.preventDefault();
  $("#config_form").submit();
});
