/*
 * fileupload.js
 * -
 *  Contains functions related to the Submit Structure page
 */


function addResidue(residue)
{
 sequ_field = document.getElementById('id_sequ');
 sequ_text = document.getElementById('id_sequ').value;
 //If the line is not blank, add a space
 if(sequ_text.length > 1)
 {
  sequ_text += " ";
 }
 sequ_text += residue;
 sequ_field.value = sequ_text;
}

function send_ligand(){
  window.location = "/ligand_design/";
}

$(".col1").on("click","a",function(event){
  addResidue(this.id);
});

$("#options_table").on("click","input",function(){
  console.log("woof");
  if(this.id == "make_ligand"){
    send_ligand();
  }else{
    var control_id = this.id;
    $(".control_div").each(function(){
      if (this.className.indexOf(control_id) > -1){ //different meaning of "this", here meaning one of the control divs
        $(this).show();
      }else{
        $(this).hide();
      }
    });
  }
});


$("#propka").on("click",function(){
  $("#ph").toggle();
});
