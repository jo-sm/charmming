function get_reslist(radius){
  //Given a numeric radius, finds all residues within that distance of a certain point
  //which is defined as the center of the current selection in JSmol
  //If selection is none, this just gets whatever is around the center of the entire
  //molecule. This function then updates the field that holds the residue selections
  //and fills it in with the resids.

  //first, we get the array or selected atoms
  var atom_array = Jmol.getPropertyAsArray(jmolApplet,"atominfo","selected");
  var com = "";
  if (atom_array.length < 1){ //if there's no selection, take the center fo the molecule
    //otherwise this will be read by JSmol as 0 0 0 which may not necessarily be anywhere close to your molecule
    //first get the center of the molecule, which JSmol doesn't support using as a variable internally...
    //These below are both dependent on the version of JSmol...make sure we have the latest one installed.
   com = Jmol.scriptWaitAsArray(jmolApplet,"print {*}.xyz");
   
  }else{
    com = Jmol.scriptWaitAsArray(jmolApplet,"print {selected}.xyz"); //ditto as in the other case
  }
  if (radius > 0){
    com = com[0][0][3]; //Magic numbers...the CURRENT JSmol scriptWaitAsArray (as of version 2014.06.29, is as follows:
    //it returns Array[4]
    //index 0 - actual script output
    //index 1 - holds the script that was executed?
    //index 2 - holds the script termination status
    //index 3 - holds roughly the same as index 1 and 2, but in a composed form
    //index 0 gives an Array. So, com[0][0] gives:
    //Array [ 3, "scriptEcho", 0, "{29.41370212765958 43.51719148936171 44.4774255319149}" ]
    //The fourth index here being what we actually want, the center of the current selection, or of the whole molecule.
    //It's quite complex.
//    com = com.substr(1, com.length-1); //take out the {} from it, since they're useless
    Jmol.scriptWait(jmolApplet,"select within("+radius+","+com+");select within(GROUP,selected) and protein;");
  }else if (radius == 0){
    Jmol.scriptWait(jmolApplet, "select within(GROUP,selected) and protein");
  }else{
    console.error("Radius cannot be <0!!");
    return false;
  }
  atom_array = Jmol.getPropertyAsArray(jmolApplet,"atominfo","selected"); //Update atom array to get the new atoms
  return atom_array;
}

function parse_reslist(atom_array){
    //Now that we have everything selected, we need to figure out which residues are in here. Thus!
  //for now we'll naively print all of them so we can figure out what's going on
  var resno_array = []
  var resno_string = "";
  for(var i=0;i<atom_array.length;i++){
    if(resno_array.indexOf(atom_array[i].resno) < 0){ //if you can't find it in the resno array, then add it in
      resno_array.push(atom_array[i].resno);
      resno_string = resno_string + " " + atom_array[i].resno; //we separate these because for example if you have residue 115,
      if (i == 0){
        resno_string = resno_string.replace(" ","");
      }
//      indexOf method will return > 0 for the string, but not for the array, if checking for residue 11
    }
//    console.log(atom_array[i].atomno+" "+atom_array[i].resname+" "+atom_array[i].resno+" " + atom_array[i].name);
  }
  $("#res_select").val(resno_string);
}

function ball_and_stickify(){ //Turns current selection into ball-and-stick mode, while leaving the rest of the protein intact
  Jmol.script(jmolApplet, "cartoons off;ribbons off;cpk 20%;wireframe 35;color jmol;hide selected and hydrogen and (not connected(elemno=7 or elemno=8))");
}

function zoom_auto(){ //zooms to selected point
  Jmol.script(jmolApplet, "zoomto 0.1 (selected) 250");
}

function refresh_protein(){ //discards old ball and sticks we don't care about anymore and turns them to cartoons
  Jmol.scriptWait(jmolApplet, "selectionhalos off;save selection oldselect;select not selected and protein;cpk off;wireframe off;"+
      "cartoons on;color structure;restore selection oldselect;set picking group;selectionhalos on;");
}


$("#atomselect_buttons").on("click","button",function(){
  if(this.id == "reset"){
    Jmol.scriptWait(jmolApplet,"select none");
    refresh_protein();
    $("#res_select").val("");
    $("#sel_radius").val("");
  }else if(this.id == "residue"){
    Jmol.script(jmolApplet,"set picking group;selectionhalos on;");
  }else if(this.id=="compile_res"){
    var rad = parseFloat($("#sel_radius").val());
    if(rad > 0){ //no negative radii...
      var a_array = get_reslist(rad);
      refresh_protein();
      parse_reslist(a_array); //automatically include buried residues
      ball_and_stickify();
      zoom_auto();
    }
  }else if(this.id=="no_buried"){
    Jmol.scriptWaitOutput(jmolApplet, "select selected and not buried");
    refresh_protein();
    var a_array = Jmol.getPropertyAsArray(jmolApplet,"atominfo","selected");
    parse_reslist(a_array);
    ball_and_stickify();
  }else if(this.id =="compile_norad"){
    refresh_protein();
    var a_array = get_reslist(0);
    parse_reslist(a_array);
    ball_and_stickify();
    zoom_auto();
  }
});

$("#submit_button").on("click",function(){
    //TODO: Verify that django will actually escape <script> tags.
  $("#atomselect_form").submit();
});
