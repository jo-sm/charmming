
// The following is taken from the select/edit structures page
function send_select_form(form) {
   $.ajax({
      url: "/select/" + form.id,
      type:"post",
    });
//   new Ajax.Request("/select/" + form.id, {method:'post', asynchronous:true});
}

function send_delete_form(filename) {
   $.ajax({
      url: "/deletefile",
      type:"post",
      async:false,
      data:{'filename':filename}
    });

//   new Ajax.Request("/deletefile/", {method:'post', asynchronous:false, parameters: {'filename':filename}});
}
