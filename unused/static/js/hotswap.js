var ws = new WebSocket('ws://localhost:3002');

ws.onopen = function() {
  console.info('Hot Swap -- WebSocket connected');
}

ws.onmessage = function(evt) {
  var data = evt.data;
  var msg;

  try {
    msg = JSON.parse(data);
  } catch(e) {
    console.error("Cannot Hot Swap: Invalid JSON (" + e + ")");
  }

  if (msg.reload && msg.reload === 'js') {
    // We will reload the app, which will start a new Websocket instance
    ws.close();
  }

  if (msg.reload === 'css') {
    replaceCss();
  }

  console.info('Hot Swap -- Reloaded');
}

function replaceCss() {
  return new Promise(function(resolve) {
    const newCss = document.createElement('link');
    newCss.href = '/css/index.css';
    newCss.id = 'new_css';
    newCss.rel = 'stylesheet';
    newCss.type = 'text/css';
    newCss.onload = function() {
      // Remove old CSS
      const oldCss = document.getElementById('css');
      oldCss.parentNode.removeChild(oldCss);

      newCss.id = 'css';

      resolve();
    };

    document.head.appendChild(newCss);
  });
};
