/*viewpdbs.js
 * -
 *  Contains functions related to the Change/Delete Structures page.
 *  */

function send_delete_form(filename) {
   $.ajax({
      url: "/deletefile",
      type:"post",
      async:false,
      data:{'filename':filename}
    });
}

function send_select_form(form) {
   $.ajax({
      url: "/select/" + form.id,
      type:"post",
    });
}

$("#viewpdbtable").on('click','a',function(){
  if(this.className == "killPDB"){
    send_delete_form(this.id);
  }else if(this.className == "killAllPDB"){
    send_delete_form('all_files');
  }
});

$("#viewpdbtable").on('click','input',function(){
  if(this.className == "select_pdb"){
    send_select_form(this);
  }
});
