var layers = 1; //Holds current layers
var QMMM_VARS = ""; //holds all vars related to linkatom textboxes and the like; saved selection data, if any
var template_string = ""; //holds the template string for making layer boxes, which we call once.
var got_template = false; //indicates success of template, if failed, should not display any multiscale stuff
var got_qmmm_params = false; //indicates success of fetching vars

function showHideQM(){
    if($("#useqmmm").is(":checked")){
      //      $(".qmmm_params").show();
      $(".model_selection").show();
      if ($("#usepbc").length > 0){
        $("#usepbc").attr("disabled",true);
        $("#usepbc").attr("checked",false); //just in case
      }
    }else{
    //      $(".qmmm_params").hide();
      $(".qmmm_params").remove(); //If this is a 0-length, then it doesn't matter, it won't except
      $(".oniom_params").remove();
      $(".model_selection").hide();
      if($("#usepbc").length > 0){
        $("#usepbc").attr("disabled",false);
      }
    }
  }

function writeLinkAtomLines(seg_ids,modelType,num_linkatoms,current_div_id)
{
  if(isNaN(num_linkatoms) || num_linkatoms == 0){ //sanity check for the sake of the manual link atom selection...
    return false;
  }
 var seg_list = seg_ids;
 num_patches = num_linkatoms;
 var model_string = "";
 var layer_num = "";
 var real_div_id = "linkatom_lines"
 if (modelType == "oniom"){
  layer_num = current_div_id.split("_"); //e.g. layer_2 turns into [layer, 2]
  model_string = "_layer_" + layer_num[layer_num.length -1];
  real_div_id = real_div_id + model_string;
  }
 var text = '<table class="qmmm_table" style="margin-left:auto;margin-right:auto;">';
 optionvalues = "";
 for(var b =0; b < seg_list.length; b++)
 {
  optionvalues = optionvalues + '<option value="' + seg_list[b] + '">' + seg_list[b] + '</option>';
 }
 for(var i = 0; i < num_patches; i++)
 {
  tempi = i+1;
  text = text + '<tr><td>QMHost '+tempi+'.</td><td> QM SEGID:';
  text = text + '<select size="1" name="linkqmsegid' + model_string +"_"+ i +'">';
text = text + optionvalues + '</select><td> QM RESID:<input type="text" id="linkqm'+model_string+"_"+i+'" name="linkqm'+model_string+"_"+i+'" size=4></td> <td> QM Atom Type: <input type="text" id="qmatomtype'+model_string+"_"+i+'" name="qmatomtype' +model_string+"_"+i + '" size=5> </td></tr><tr><td>MMHost '+tempi+'.</td><td> MM SEGID: <select size="1" name="linkmmsegid'+model_string+"_"+i +'">' + optionvalues + '</select> </td><td>MM RESID:<input type="text" id="linkmm'+model_string+"_"+i+'" name="linkmm'+model_string+"_"+i+'" size=4></td><td> MM Atom Type: <input type="text" id="mmatomtype'+model_string+"_"+i+'" name="mmatomtype'+model_string+"_"+i + '" size=5> </td></tr>';
 }
  text= text + '</table>';
  document.getElementById(real_div_id).innerHTML = text;
}


function generate_from_selection(){
  if (QMMM_VARS.atomselection != null || QMMM_VARS.oniom_selections != null){ //check for any selections
    $("#useqmmm").attr("checked",true);
    $("#"+QMMM_VARS.old_task).attr("checked",true); //click proper source task
    var isOniom = typeof(QMMM_VARS.oniom_selections) == 'undefined' ? false : true;
    var atom_sels = isOniom ? QMMM_VARS.oniom_selections : QMMM_VARS.atomselection; //holds atom selections for iterations
    if (isOniom){ //generate ONIOM stuff
      $("#model_select_qmmm").prop("checked",false); //make sure only oniom is displayed
      $("#model_select_oniom").prop("checked",true);
      $("#oniom_layers").val(QMMM_VARS.total_layers);
      layers = parseInt(QMMM_VARS.total_layers);
      generateParams("oniom");
    }else{ //we aren't using ONIOM
      $("#model_select_qmmm").prop("checked",true);
      $("#model_select_oniom").prop("checked",false);
      layers = 1;
      atom_sels = QMMM_VARS.atomselection;
      generateParams("qmmm");
    }
      for(var i=0;i<atom_sels.length;i++){
        //If this isn't oniom, then this is a single loop
        var curr_sel = atom_sels[i]; //for easier typing below
        if(curr_sel.isQM || !(isOniom)){
          var ln = isOniom ? "_layer_"+curr_sel.layer_num : ""; //extra string to make our element names work
          $("#num_linkatoms"+ln).val(curr_sel.linkatom_num);
          $("#qmsele"+ln).val(curr_sel.selectionstring);
          var model = ln == "" ? "qmmm" : "oniom";
          writeLinkAtomLines(QMMM_VARS.seg_list,model,curr_sel.linkatom_num,"linkatom_lines"+ln);
          $("#qmmm_exchange"+ln).val(curr_sel.exchange);
          $("#qmmm_charge"+ln).val(curr_sel.charge);
          $("#qmmm_correlation"+ln).val(curr_sel.correlation);
          $("#qmmm_basisset"+ln).val(curr_sel.basis_set);
          $("#qmmm_multiplicity"+ln).val(curr_sel.multiplicity);
        }else{
          hideQMBoxes(curr_sel.layer_num);
        }
      }
      var lps = QMMM_VARS.lonepairs; //holds all lonepairs for iteration
      //The next line is  because we have strange nomenclature
      ln = ln == "" ? "_" : "";
      for(var i=0;i<lps.length;i++){
        var lp = lps[i];
        $("#linkqm"+ln+lp.divid).val(lp.qmresid);
        $("#linkmm"+ln+lp.divid).val(lp.mmresid);
        $("#qmatomtype"+ln+lp.divid).val(lp.qmatomname);
        $("#mmatomtype"+ln+lp.divid).val(lp.mmatomname);
        console.log(ln+lp.divid);
        $("select[name=linkqmsegid"+ln+lp.divid+"]").val(lp.qmsegid);
        $("select[name=linkmmsegid"+ln+lp.divid+"]").val(lp.mmsegid);
        console.log(ln+lp.divid);
    }
    $(".model_selection").show();
    if(isOniom){
      $(".oniom_params").show();
    }else{
      $(".qmmm_params").show();
    }
  }
}

function generateParams(modelType){
  //layers is kept track of by the global
    //We need to check whether qmmm_params is already there, then remove it if it is.
    $(".qmmm_params").remove();
    //Once qmmm_params is gone, we need to "spawn" the parameter HTML so the user can select.
    //However, ".oniom_params" HTML can appear more than once. Therefore, we "template" it with some request data,
    //then replace the string at points we need it to.
    //The points where a replace should occur are labeled "numero" in the template. ".oniom_params" is a "magic" value.
    //DO not change the magic value.
    //You can now use a div. However, this works fine as is./MSC
    //"numero" will either disappear, if you only have 2-layer MSCALE or regular additive model
    //or it will become "_layer2" or "_layer1" for 3-layer MSCALE (since layer 3 is the whole atomset).
    //".oniom_params" is infinitely scalable to as many layers and as many models as we want.
    //However, please note that after 10 or so layers the site will look strange.
    //It's guaranteed to be fast on most computers, though, since jQuery runs very, very quickly.
    //Since you can include django stuff wherever you want, let's make it into a template, then run .replace() on the strings.
    var numeroRegex = new RegExp("numero","g");
    var selection_button_string = "<button type='button' class='qmmm_params goto_atomselect'>Select atoms graphically</button>";
    if(modelType == "qmmm"){ //We replace numero with blank
      layers = 1;
      var our_param_string = template_string.replace(numeroRegex,"");
      our_param_string = our_param_string.replace("modelo","qmmm");
//      selection_button_string =  selection_button_string.replace("arfarf","qmmm");
//      selection_button_string = selection_button_string.replace("woofwoof","1");
      //Now we insert that HTML after ".oniom_params" div.
      $(".oniom_params").after(our_param_string);
      $(".oniom_params").after(selection_button_string);
      $("#mm_box").remove();
      $(".qmmm_params").show(); //Since the default style is display:none. ".oniom_params" means it doesn't get shown until loaded in.
      //We're done.
    }else if(modelType == "oniom"){ //Make sure to not use else - that leaves us open to lots of stuff.
      var layer_string = "<h2 id='layer_numero_title' class='qmmm_params'>Layer numero</h2>"
      if(layers < 2){
        //Bring up an error message.
        return false;
      }
      var current_layer = layers;
      var current_layer_string = "";
      //The following string gets included at the start of each layer
      while (current_layer >= 1){ //No forloops allowed on ".oniom_params" site
        current_layer_string = layer_string.replace(numeroRegex,current_layer);
        var current_param_string = template_string.replace(numeroRegex,"_layer_"+current_layer);
        current_param_string = current_param_string.replace("modelo","oniom");
//          selection_button_string =  selection_button_string.replace("arfarf","oniom");
//          selection_button_string = selection_button_string.replace("woofwoof",layers.toString());
        $(".oniom_params").after(current_param_string);
        $('.oniom_params').after(current_layer_string);
        if(current_layer == 1){
          $("#mm_box_layer_"+current_layer).remove();
          $("#layer_1_title").html("Layer 1 (innermost layer)");
        }
        if(current_layer == layers){
          $("#layer_"+current_layer+"_title").html("Layer "+current_layer+" (whole system)");
          $("#mm_box_layer_"+current_layer).prop('checked',true); //"hack" until the MM box event is fixed
        }
        //this will insert them in ascending order, since we start at the highest value then insert lower ones "on top"
        current_layer = current_layer - 1;
      }
      $(".oniom_params").after(selection_button_string);
      $(".qmmm_params").show();
  }
      $(".goto_atomselect").click(function(){
        goto_atomselect();
      });
  }


function changeLayerBox(){
  if ($("#oniom_layers").attr('display') == 'none'){
    layers = 1;
    return false;
  }else{
    layers = parseInt($("#oniom_layers").val());
    generateParams("oniom");
  }
}


function ONIOM_MM_toggle(checkbox){
  var layer_num = checkbox.name.split("_")[3];
  if(checkbox.checked){
    hideQMBoxes("_layer_"+layer_num);
    }
  else{
    $("#qmmm_params_layer_"+layer_num).show();
    $("#highest_qm_layer").val(layer_num);
  }
}

//For atom selection (QM/MM)
function goto_atomselect(){
  var inputs = document.getElementsByName("ptask");
  var form = null;
  if (inputs.length > 0){
    for(i=0;i<inputs.length;i++){
      if(inputs[i].checked){
        document.getElementById("task_id").value = inputs[i].value;
        break;
      }
    }
    var action = document.URL.split("/");
    var source = action[action.length -2];
    document.getElementById("source").value = source;
    if(source == "energy"){
      form = document.getElementById("ener_form");
    }
    if(source == "minimize"){
      form = document.getElementById("min_form");
    }if(source == "normalmodes"){
      form = document.getElementById("nma_form");
    }
//Note: Update here to create more QM/MM boxen in other pages
    form.action="/selection/";
    form.onsubmit= function(event){return true;};
    form.submit();
  }else{
    $("#dialog_coords_alert").dialog("open");
//    alert("No coordinates present. Please run at least one calculation on the full atom set before performing any QM/MM operations.")
  }
}


function hideQMBoxes(qmbox_number){
  var number_to_check = 0;
  if (typeof qmbox_number == "number"){
    number_to_check = qmbox_number;
  }else{
    var foo = qmbox_number.split("_");
    number_to_check = parseInt(foo[foo.length - 1]);
  }
  //We can make this recursive, but it's not useful.
  //We use the "layers" global included in the qmmm_params page.
  var current_layer = number_to_check;
  while(current_layer <= layers){
    $("#qmmm_params_layer_"+current_layer.toString()).hide();
    $("#mm_box_layer_"+current_layer.toString() + " input").attr("checked",true);
    current_layer = current_layer + 1;
  }
  $("#highest_qm_layer").val(number_to_check - 1);
  return true;
}

//Now setup events and error messages.
$("#model_select_qmmm").on("click",function(){
  $(".oniom_params").hide();
  layers = 1;
  generateParams('qmmm');
  });

$("#model_select_oniom").on("click",function(){
  $(".qmmm_params").hide();
  $(".oniom_params").show();
  layers = 2;
  generateParams("oniom");
});

$("#oniom_layers").on("change",changeLayerBox);


$(function($){
      $( "#dialog_coords_alert").dialog({
        resizable:false,
        height:200,
        width:600,
        modal:true,
        autoOpen:false,
        buttons:{
        "OK":function(){
        $(this).dialog("close");
        }
      }
    });
  });


$(function($){
  $("#dialog_bad_params").dialog({
        resizable:false,
        height:200,
        width:600,
        modal:true,
        autoOpen:false,
        buttons:{
        "OK":function(){
        $(this).dialog("close");
        }
      }
    });
  });

//template is smaller than the other
$.ajax({
  url: "/get_qmmm_tpl",
  type:"post",
  async:true,
  success:function(responseData){
    template_string=responseData;
    got_template = true;
    if (got_qmmm_params){
      $(document).ready(generate_from_selection());
    }
  },
  error:function(){
    console.log("Couldn't get QM/MM template");
}
});

$.ajax({
  url: "/get_qmmm_vars",
  type:"post",
  async:true,
  dataType:"json",
  success:function(responseData){
    QMMM_VARS = responseData; //This is gonna be a big object.
    got_qmmm_params = true;
    if (got_template){
      $(document).ready(generate_from_selection());
    }
  },
  error:function(){
    console.log("Couldn't get QM/MM atom selections."); //TODO: replace with proper error box.
  }
});
