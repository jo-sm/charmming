    /***********************************************
    * DHTML Window Widget- Â© Dynamic Drive (www.dynamicdrive.com)
    * This notice must stay intact for legal use.
    * Visit http://www.dynamicdrive.com/ for full source code
    ***********************************************/

    var myMenu;

    function getDocWidth() {
       return document.documentElement.clientWidth *.82;
    }
       myMenu = new SDMenu("my_menu");
       myMenu.init();
      //two "hacks" to make sure we get the boxen filled before the periodicalupdater fires
       $.ajax({
        url: "/status",
        async:true,
        success:function(responseData){
          $("#statusreport").html(responseData);
        }
      });
       $.ajax({
        url: "/lessonstatus",
        async:true,
        success:function(responseData){
          $("#lessonreport").html(responseData);
        }
      });
       $("#statusreport").PeriodicalUpdater('/status', {
         async: true,
         multiplier: 4,
         minTimeout: 200,
         maxTimeout: 6000
       }, function(remoteData) {
         document.getElementById("statusreport").innerHTML = remoteData;
       });
       $("#lessonreport").PeriodicalUpdater('/lessonstatus', {
         async: true,
         multiplier: 4,
         minTimeout: 200,
         maxTimeout: 6000
       }, function(remoteData) {
         document.getElementById("lessonreport").innerHTML = remoteData;
       });

