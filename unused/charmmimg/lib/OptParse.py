# extension of optparse to include 'required options'
# http://code.activestate.com/recipes/573441/
# 20/01/2010

from . import optparse

strREQUIRED = 'required'

class OptionWithDefault(optparse.Option):
  ATTRS = optparse.Option.ATTRS + [strREQUIRED]

  def __init__(self, *opts, **attrs):
    if attrs.get(strREQUIRED, False):
      attrs['help'] = '(Required) ' + attrs.get('help', "")
    optparse.Option.__init__(self, *opts, **attrs)

class OptionParser(optparse.OptionParser):
  def __init__(self, **kwargs):
    kwargs['option_class'] = OptionWithDefault
    optparse.OptionParser.__init__(self, **kwargs)

  def check_values(self, values, args):
    for option in self.option_list:
      if hasattr(option, strREQUIRED) and option.required:
        if not getattr(values, option.dest):
          self.error("option %s is required" % (str(option)))
    return optparse.OptionParser.check_values(self, values, args)
