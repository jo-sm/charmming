from schedulerd.Tracker import Tracker

class TrackerProtocolTest:
  """
  Tests the parse method
  """

  def setup_method(self, *args):
    self.tracker = Tracker()

  def handle_handshake_case(self):
    assert self.tracker.parse("HELLO SCHEDD", 'test') == "HELLO CLIENT"

  def bad_protocol_case(self):
    assert self.tracker._bad_protocol() == "BAD PROTOCOL"

  def handle_invalid_case(self):
    assert self.tracker.parse("WHATEVER", 'test') == "BAD PROTOCOL"

  def handle_status_case(self):
    self.tracker.lookup_job_status = lambda id: None

    assert self.tracker.parse('STATUS', 'test') == 'BAD PROTOCOL'

    assert self.tracker.parse("STATUS 1", 'test') == "BAD NO SUCH JOB"

    self.tracker.lookup_job_status = lambda id: 2

    assert self.tracker.parse("STATUS 1", 'test') == "OK 2"

    self.tracker.lookup_job_status = lambda id: 1
    assert self.tracker.parse("STATUS 1", 'test') == "OK 1"

    self.tracker.lookup_job_status = lambda id: 3
    assert self.tracker.parse("STATUS 1", 'test') == "OK 3"

    self.tracker.lookup_job_status = lambda id: 4
    assert self.tracker.parse("STATUS 1", 'test') == "OK 4"

  def handle_new_case(self):
    assert self.tracker.parse('NEW', 'test') == 'OK GIVE USER DIR SCRIPTS'

  def handle_uds_case(self):
    assert self.tracker.parse("UDS", 'test') == 'BAD PROTOCOL'
    assert self.tracker.parse("UDS one", 'test') == 'OK GIVE EXELIST'
    assert self.tracker.parse("UDS one two", 'test') == 'OK GIVE EXELIST'
    assert self.tracker.parse("UDS one two three", 'test') == 'OK GIVE EXELIST'

  def handle_exe_case(self):
    assert self.tracker.parse('EXELIST', 'test') == 'BAD PROTOCOL'
    assert self.tracker.parse('EXELIST one', 'test') == 'BAD PROTOCOL'

    self.tracker.parse('UDS one', 'test')

    assert self.tracker.parse('EXELIST one', 'test') == 'OK GIVE NPROCLIST'
    assert self.tracker.parse('EXELIST one two', 'test') == 'OK GIVE NPROCLIST'
    assert self.tracker.parse('EXELIST one two three', 'test') == 'OK GIVE NPROCLIST'

    assert self.tracker.parse('EXELIST one two', 'test2') == 'BAD PROTOCOL'

  def handle_nproc_case(self):
    assert self.tracker.parse('NPROCLIST', 'test') == 'BAD PROTOCOL'
    assert self.tracker.parse('NPROCLIST one', 'test') == 'BAD PROTOCOL'

    self.tracker.parse('UDS one', 'test')

    assert self.tracker.parse('NPROCLIST', 'test') == 'BAD PROTOCOL'
    assert self.tracker.parse('NPROCLIST one', 'test') == 'BAD PROTOCOL'

    self.tracker.start_job = lambda a, b, c: None

    self.tracker.parse('UDS one', 'test')
    self.tracker.parse('EXELIST one', 'test')

    assert self.tracker.parse('NPROCLIST one', 'test') == 'BAD JOB SPEC'

    self.tracker.start_job = lambda a, b, c: 1

    self.tracker.parse('UDS one', 'test')
    self.tracker.parse('EXELIST one', 'test')

    assert self.tracker.parse('NPROCLIST one', 'test') == 'OK 1'
    assert self.tracker.parse('NPROCLIST one', 'test') == 'BAD PROTOCOL'

  def handle_abort_case(self):
    assert self.tracker.parse('ABORT', 'test') == 'BAD PROTOCOL'
    assert self.tracker.parse('ABORT 1', 'test') == 'BAD PROTOCOL'

    self.tracker.kill_job = lambda i: 1
    assert self.tracker.parse('ABORT 1 1', 'test') == 'BAD JOB: ERR 1'

    self.tracker.kill_job = lambda i: 2
    assert self.tracker.parse('ABORT 1 1', 'test') == 'BAD JOB: ERR 2'

    self.tracker.kill_job = lambda i: 0
    assert self.tracker.parse('ABORT 1 1', 'test') == 'OK DONE'

  def handle_end_case(self):
    assert self.tracker.parse('END', 'test') == 'GOODBYE WORLD. IT WAS NICE KNOWIN\' YA'
