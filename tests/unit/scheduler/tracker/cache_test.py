from schedulerd.Tracker import Tracker

class TrackerCacheTest:
  """
  Tests the following methods:

  _add_to_cache
  _get_from_cache
  _reset_cache_for_addr
  """

  def setup_method(self, *args):
    self.tracker = Tracker()

  def handle_invalid_attr_case(self):
    assert self.tracker._get_from_cache('test', 'key') is None

  def handle_invalid_key_case(self):
    self.tracker._add_to_cache('test', 'key', 'value')

    assert self.tracker._get_from_cache('test', 'key') == 'value'
    assert self.tracker._get_from_cache('test', 'other_key') is None

  def handle_multiple_addr_case(self):
    self.tracker._add_to_cache('test', 'key1', 'value1')
    self.tracker._add_to_cache('test2', 'key2', 'value2')
    self.tracker._add_to_cache('test3', 'key3', 'value3')
    self.tracker._add_to_cache('test3', 'key4', 'value4')

    assert self.tracker._get_from_cache('test', 'key1') == 'value1'
    assert self.tracker._get_from_cache('test2', 'key2') == 'value2'
    assert self.tracker._get_from_cache('test3', 'key3') == 'value3'
    assert self.tracker._get_from_cache('test3', 'key4') == 'value4'

  def handle_cache_reset_case(self):
    self.tracker._add_to_cache('test', 'key', 'value')

    assert self.tracker._get_from_cache('test', 'key') == 'value'

    self.tracker._reset_cache_for_addr('test')

    assert self.tracker._get_from_cache('test', 'key') is None
