from django.test.utils import override_settings
from charmming.utils import template

class AssetTest:
  def invalid_type_case(self):
    "Should return empty string if given an invalid type"
    tag = template.asset('whatever', 'index')
    assert tag == ''

  def asset_css_type_case(self):
    "Should handle CSS asset type and return a link with valid attributes"
    tag = template.asset('css', 'index')
    assert tag == '<link rel="stylesheet" type="text/css" href="/css/index.css">'

  def asset_js_type_case(self):
    "Should handle JS asset type and return a script tag with closing tag and valid attributes"
    tag = template.asset('js', 'bundle')
    assert tag == '<script type="text/javascript" src="/js/bundle.js"></script>'

  def asset_dir_case(self, *args):
    "Should create an asset url based on settings.ASSET_DIR and settings.ASSET_URL"

    with override_settings(ASSET_DIR='dir/'):
      tag = template.asset('css', 'index')
      assert tag == '<link rel="stylesheet" type="text/css" href="/dir/css/index.css">'

    with override_settings(ASSET_DIR='other_dir/', ASSET_URL='https://example.org/'):
      tag = template.asset('css', 'index')
      assert tag == '<link rel="stylesheet" type="text/css" href="https://example.org/other_dir/css/index.css">'

  def asset_user_attrs_case(self):
    "Should handle valid user attrs and append them to the attributes, but before the url"
    tag = template.asset('css', 'index', '[("id", "css"), ("data-css", "true")]')
    assert tag == '<link rel="stylesheet" type="text/css" id="css" data-css="true" href="/css/index.css">'
