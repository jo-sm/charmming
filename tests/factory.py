from django.db import connection
from django.contrib.auth.models import User

import random
import string
import os

FIXTURES_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), 'fixtures'))

def create_user():
  user = User(username=''.join(random.choice(string.ascii_letters) for i in range(10)))
  user.save()

  return user

def truncate_tables(orm_models):
  if type(orm_models) != list:
    orm_models = [ orm_models ]
  cursor = connection.cursor()

  for model in orm_models:
    table_name = model._meta.db_table

    cursor.execute('TRUNCATE TABLE "{}" CASCADE'.format(table_name))

class SchedulerInterface:
  def __init__(self, *args, **kwargs):
    self.state = kwargs.get('state', 0)
    self.kill_state = kwargs.get('kill_state', True)
    self.new_job_state = kwargs.get('new_job_state', None)
    self._called = {
      'get_state': 0,
      'kill_job': 0,
      'new_job': 0
    }

  def called(self, name):
    return self._called.get(name) > 0

  def called_times(self, name):
    return self._called.get(name) or 0

  def get_state(self, *args):
    print('Called get_state')
    self._called['get_state'] += 1
    return self.state

  def kill_job(self, *args):
    self._called['kill_job'] += 1
    return self.kill_state

  def new_job(self, *args):
    self._called['new_job'] += 1
    return self.new_job_state
