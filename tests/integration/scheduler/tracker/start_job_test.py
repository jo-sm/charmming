from schedulerd.Tracker import Tracker

from charmming.models import SchedulerJob

from tests.factory import create_user, SchedulerInterface

class StartJobTest:
  def setup_method(self, *args):
    self.tracker = Tracker()
    self.tracker.sched_intf = SchedulerInterface()
    self.user = create_user()

  def invalid_user_case(self):
    assert self.tracker.start_job(['9999', '/test', 'test'], ['test'], ['test']) is None

  def bad_request_case(self):
    assert self.tracker.start_job([], [], []) is None
    assert self.tracker.start_job(['abc'], [], []) is None

  def wrong_lengths_case(self):
    assert self.tracker.start_job(['1', '/test', 'test'], [], []) is None
    assert self.tracker.start_job(['1', '/test', 'test', 'test'], ['test'], []) is None
    assert self.tracker.start_job(['1', '/test', 'test', 'test'], ['test'], [1]) is None
    assert self.tracker.start_job(['1', '/test', 'test', 'test'], ['test', 'test'], []) is None
    assert self.tracker.start_job(['1', '/test', 'test', 'test'], ['test', 'test'], [1]) is None

  def bad_job_submission_case(self):
    """
    SchedulerInterface will return None by default in this case
    """

    assert self.tracker.start_job(['1', '/test', 'test'], ['test'], [1]) is None

  def submit_job_case(self):
    self.tracker.sched_intf = SchedulerInterface(new_job_state=[10, 20, 30, 40])
    assert self.tracker.start_job(
      [self.user.id, '/test', 'test', 'test2', 'test3', 'test4'], # User, dir, scripts
      ['test.exe', 'test2.exe', 'test3.exe', 'test4.exe'], # Exe list
      [1, 1, 1, 1] # Nprocs list
    ).index('queued') == 0

    queue_job_10 = SchedulerJob.objects.get(sched_id=10)
    queue_job_20 = SchedulerJob.objects.get(sched_id=20)
    queue_job_30 = SchedulerJob.objects.get(sched_id=30)
    queue_job_40 = SchedulerJob.objects.get(sched_id=40)

    assert queue_job_10.batchid == '40'
    assert queue_job_20.batchid == '40'
    assert queue_job_30.batchid == '40'
    assert queue_job_40.batchid == '40'

    assert queue_job_10.script == 'test'
    assert queue_job_20.script == 'test2'
    assert queue_job_30.script == 'test3'
    assert queue_job_40.script == 'test4'

    assert queue_job_10.exe == 'test.exe'
    assert queue_job_20.exe == 'test2.exe'
    assert queue_job_30.exe == 'test3.exe'
    assert queue_job_40.exe == 'test4.exe'

  def submit_job_queued_case(self):
    self.tracker.sched_intf = SchedulerInterface(state=1, new_job_state=[50])
    assert self.tracker.start_job(
      [self.user.id, '/test', 'test'], # User, dir, scripts
      ['test.exe'], # Exe list
      [1] # Nprocs list
    ).index('queued') == 0

    queue_job_50 = SchedulerJob.objects.get(sched_id=50)

    assert queue_job_50.batchid == '50'
    assert queue_job_50.script == 'test'
    assert queue_job_50.exe == 'test.exe'
    assert queue_job_50.state == 1
