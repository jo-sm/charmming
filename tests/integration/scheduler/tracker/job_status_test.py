from charmming.models import SchedulerJob

from schedulerd.Tracker import Tracker

from tests.factory import create_user

import datetime

class JobStatusTest:
  def setup_method(self, *args):
    self.tracker = Tracker()
    self.user = create_user()
    self.queued_time = datetime.datetime.now()
    self.started_time = self.queued_time + datetime.timedelta(hours=1)
    self.ended_time = self.queued_time + datetime.timedelta(hours=2)

    self.job = SchedulerJob(userid=self.user, queued=self.queued_time, started=self.started_time, ended=self.ended_time)
    self.job.save()

  def no_job_case(self):
    job_id = self.job.id
    self.job.delete()

    assert self.tracker.lookup_job_status(job_id) is None

  def handle_submitted_case(self):
    self.job.state = 0
    self.job.save()

    status = self.tracker.lookup_job_status(self.job.id)
    assert status == "Job {} User {} submitted".format(self.job.id, self.user.id)

  def handle_queued_case(self):
    self.job.state = 1
    self.job.save()

    status = self.tracker.lookup_job_status(self.job.id)
    assert status == "Job {} User {} queued since {}".format(self.job.id, self.user.id, self.queued_time)

  def handle_running_case(self):
    self.job.state = 2
    self.job.save()

    status = self.tracker.lookup_job_status(self.job.id)
    assert status == "Job {} User {} running since {}".format(self.job.id, self.user.id, self.started_time)

  def handle_complete_case(self):
    self.job.state = 3
    self.job.save()

    status = self.tracker.lookup_job_status(self.job.id)
    assert status == "Job {} User {} complete since {}".format(self.job.id, self.user.id, self.ended_time)

  def handle_failed_case(self):
    self.job.state = 4
    self.job.save()

    status = self.tracker.lookup_job_status(self.job.id)
    assert status == "Job {} User {} failed".format(self.job.id, self.user.id)

  def handle_invalid_case(self):
    self.job.state = 5
    self.job.save()

    assert self.tracker.lookup_job_status(self.job.id) is None
