from charmming import charmming_config
from charmming.models import SchedulerJob

from schedulerd.Tracker import Tracker

from tests.factory import truncate_tables, SchedulerInterface, create_user, FIXTURES_DIR

import datetime
import os

class UpdateJobStatusTest:
  @classmethod
  def setup_class(*args):
    truncate_tables(SchedulerJob)

  def setup_method(self, *args):
    self.tracker = Tracker()
    self.user = create_user()
    self.job1 = SchedulerJob(userid=self.user, sched_id=1)
    self.job2 = SchedulerJob(userid=self.user, sched_id=2)

    self.job1.save()
    self.job2.save()

  def teardown_method(self, *args):
    if self.job1.id:
      self.job1.delete()

    if self.job2.id:
      self.job2.delete()

  def no_non_ended_jobs_case(self):
    self.job1.ended = datetime.datetime.now()
    self.job1.save()

    self.job2.ended = datetime.datetime.now()
    self.job2.save()

    self.tracker.sched_intf = SchedulerInterface(state=-1)
    assert self.tracker.update_status() is None

    assert self.tracker.sched_intf.called('get_state') is False
    assert self.tracker.sched_intf.called_times('get_state') == 0

    self.job1.ended = None
    self.job1.save()

    self.job2.ended = None
    self.job2.save()

    self.job1.delete()
    self.job2.delete()

    assert self.tracker.update_status() is None

  def no_job_in_queue_case(self):
    self.tracker.sched_intf = SchedulerInterface(state=-1)
    self.tracker.update_status()

    assert self.tracker.sched_intf.called('get_state') is True
    assert self.tracker.sched_intf.called_times('get_state') == 2

    job1 = SchedulerJob.objects.get(sched_id=1)
    job2 = SchedulerJob.objects.get(sched_id=2)

    assert job1.state == 0
    assert job2.state == 0

  def job_in_same_state_case(self):
    now = datetime.datetime.now()
    self.job1.started = now
    self.job1.state = 2
    self.job1.save()

    self.job2.started = now
    self.job2.state = 2
    self.job2.save()

    self.tracker.sched_intf = SchedulerInterface(state=2)
    self.tracker.update_status()

    assert self.tracker.sched_intf.called('get_state') is True
    assert self.tracker.sched_intf.called_times('get_state') == 2

    job1 = SchedulerJob.objects.get(sched_id=1)
    job2 = SchedulerJob.objects.get(sched_id=2)

    assert job1.started == now
    assert job2.started == now

    assert job1.state == 2
    assert job2.state == 2

  def job_in_diff_state_queued_case(self):
    self.tracker.sched_intf = SchedulerInterface(state=1)
    self.tracker.update_status()

    assert self.tracker.sched_intf.called('get_state') is True
    assert self.tracker.sched_intf.called_times('get_state') == 2

    job1 = SchedulerJob.objects.get(sched_id=1)
    job2 = SchedulerJob.objects.get(sched_id=2)

    assert job1.state == 1
    assert job2.state == 1

  def job_in_diff_state_running_case(self):
    self.tracker.sched_intf = SchedulerInterface(state=2)
    self.tracker.update_status()

    assert self.tracker.sched_intf.called('get_state') is True
    assert self.tracker.sched_intf.called_times('get_state') == 2

    job1 = SchedulerJob.objects.get(sched_id=1)
    job2 = SchedulerJob.objects.get(sched_id=2)

    assert job1.state == 2
    assert job1.started is not None
    assert job1.ended is None

    assert job2.state == 2
    assert job2.started is not None
    assert job2.ended is None

  def job_in_diff_state_ended_okay_charmm_case(self):
    self.job1.batchid = 1
    self.job1.exe = charmming_config.apps['charmm']
    self.job1.script = os.path.join(FIXTURES_DIR, 'charmm_okay.inp')
    self.job1.save()

    self.job2.batchid = 2
    self.job2.exe = charmming_config.apps['charmm']
    self.job2.script = os.path.join(FIXTURES_DIR, 'charmm_okay.inp')
    self.job2.save()

    self.tracker.sched_intf = SchedulerInterface(state=3)
    self.tracker.update_status()

    assert self.tracker.sched_intf.called('get_state') is True
    assert self.tracker.sched_intf.called_times('get_state') == 2

    job1 = SchedulerJob.objects.get(sched_id=1)
    job2 = SchedulerJob.objects.get(sched_id=2)

    assert job1.state == 3
    assert job2.state == 3

    assert job1.ended is not None
    assert job2.ended is not None

  def job_ended_okay_qchem_case(self):
    self.job2.delete()

    self.job1.batchid = 1
    self.job1.exe = charmming_config.apps['qchem']
    self.job1.script = os.path.join(FIXTURES_DIR, 'qchem_okay.inp')
    self.job1.save()

    self.tracker.sched_intf = SchedulerInterface(state=3)
    self.tracker.update_status()

    assert self.tracker.sched_intf.called('get_state') is True
    assert self.tracker.sched_intf.called_times('get_state') == 1

    job1 = SchedulerJob.objects.get(sched_id=1)

    assert job1.state == 3

    assert job1.ended is not None

  def job_ended_not_okay_qchem_case(self):
    self.job2.delete()

    self.job1.batchid = 1
    self.job1.exe = charmming_config.apps['qchem']
    self.job1.script = os.path.join(FIXTURES_DIR, 'qchem_not_okay.inp')
    self.job1.save()

    self.tracker.sched_intf = SchedulerInterface(state=3)
    self.tracker.update_status()

    assert self.tracker.sched_intf.called('get_state') is True
    assert self.tracker.sched_intf.called_times('get_state') == 1

    job1 = SchedulerJob.objects.get(sched_id=1)

    assert job1.state == 4

    assert job1.ended is not None

  def job_in_diff_state_ended_not_okay_charmm_case(self):
    self.job1.batchid = 1
    self.job1.exe = charmming_config.apps['charmm']
    self.job1.script = os.path.join(FIXTURES_DIR, 'charmm_not_okay.inp')
    self.job1.save()

    self.job2.batchid = 2
    self.job2.exe = charmming_config.apps['charmm']
    self.job2.script = os.path.join(FIXTURES_DIR, 'charmm_not_okay.inp')
    self.job2.save()

    self.tracker.sched_intf = SchedulerInterface(state=3)
    self.tracker.update_status()

    assert self.tracker.sched_intf.called('get_state') is True
    assert self.tracker.sched_intf.called_times('get_state') == 2

    job1 = SchedulerJob.objects.get(sched_id=1)
    job2 = SchedulerJob.objects.get(sched_id=2)

    assert job1.state == 4
    assert job2.state == 4

    assert job1.ended is not None
    assert job2.ended is not None

  def job_ended_invalid_outfile_case(self):
    self.job2.delete()

    self.job1.batchid = 1
    self.job1.exe = charmming_config.apps['charmm']
    self.job1.script = os.path.join(FIXTURES_DIR, 'invalid_input.inp')
    self.job1.save()

    self.tracker.sched_intf = SchedulerInterface(state=3)
    self.tracker.update_status()

    assert self.tracker.sched_intf.called('get_state') is True
    assert self.tracker.sched_intf.called_times('get_state') == 1

    job1 = SchedulerJob.objects.get(sched_id=1)

    assert job1.state == 4

    assert job1.ended is not None

  def job_ended_okay_batch_not_okay_case(self):
    self.job1.batchid = 1
    self.job1.exe = charmming_config.apps['charmm']
    self.job1.script = os.path.join(FIXTURES_DIR, 'charmm_okay.inp')
    self.job1.save()

    self.job2.batchid = 1
    self.job2.exe = charmming_config.apps['charmm']
    self.job2.script = os.path.join(FIXTURES_DIR, 'charmm_not_okay.inp')
    self.job2.save()

    self.tracker.sched_intf = SchedulerInterface(state=3)
    self.tracker.update_status()

    assert self.tracker.sched_intf.called('get_state') is True
    assert self.tracker.sched_intf.called_times('get_state') == 2

    job1 = SchedulerJob.objects.get(sched_id=1)
    job2 = SchedulerJob.objects.get(sched_id=2)

    assert job1.state == 4
    assert job2.state == 4

    assert job1.ended is not None
    assert job2.ended is not None

  def job_ended_okay_batch_not_okay_case_2(self):
    self.job1.batchid = 1
    self.job1.exe = charmming_config.apps['charmm']
    self.job1.script = os.path.join(FIXTURES_DIR, 'charmm_okay.inp')
    self.job1.save()

    self.job2.batchid = 1
    self.job2.exe = charmming_config.apps['charmm']
    self.job2.script = os.path.join(FIXTURES_DIR, 'missing.inp')
    self.job2.save()

    self.tracker.sched_intf = SchedulerInterface(state=3)
    self.tracker.update_status()

    assert self.tracker.sched_intf.called('get_state') is True
    assert self.tracker.sched_intf.called_times('get_state') == 2

    job1 = SchedulerJob.objects.get(sched_id=1)
    job2 = SchedulerJob.objects.get(sched_id=2)

    assert job1.state == 4
    assert job2.state == 4

    assert job1.ended is not None
    assert job2.ended is not None

  def job_with_unknown_app_ended_case(self):
    self.job2.delete()

    self.job1.exe = 'other.exe'
    self.job1.save()

    self.tracker.sched_intf = SchedulerInterface(state=3)
    self.tracker.update_status()

    assert self.tracker.sched_intf.called('get_state') is True
    assert self.tracker.sched_intf.called_times('get_state') == 1

    job1 = SchedulerJob.objects.get(sched_id=1)

    assert job1.state == 3
    assert job1.ended is not None
