from charmming.models import SchedulerJob

from schedulerd.Tracker import Tracker

from tests.factory import create_user, SchedulerInterface

class KillJobTest:
  def setup_method(self, *args):
    self.tracker = Tracker()
    self.tracker.sched_intf = SchedulerInterface(kill_job_state=True)
    self.user = create_user()

    self.job = SchedulerJob(state=0, userid=self.user)
    self.job.save()

  def kill_nonexistent_job_case(self):
    assert self.tracker.kill_job("9999 9999") == -3

  def kill_invalid_id_case(self):
    assert self.tracker.kill_job("abcd 9999") == -1
    assert self.tracker.kill_job("9999 abcb") == -1

  def kill_single_job_case(self):
    self.job.batchid = 1
    self.job.save()

    assert self.job.state == 0
    assert self.tracker.kill_job("{} {}".format(self.user.id, self.job.id)) == 0

    job = SchedulerJob.objects.get(id=self.job.id)

    assert job.state == 4

  def kill_multiple_jobs_case(self):
    self.job.batchid = 2
    self.job.save()

    job2 = SchedulerJob(state=0, batchid=2, userid=self.user)
    job2.save()

    assert self.job.state == 0
    assert job2.state == 0
    assert self.tracker.kill_job("{} {}".format(self.user.id, self.job.id)) == 0

    job = SchedulerJob.objects.get(id=self.job.id)
    job2 = SchedulerJob.objects.get(id=job2.id)

    assert job.state == 4
    assert job2.state == 4

  def kill_only_jobs_in_batch_case(self):
    self.job.batchid = 3
    self.job.save()

    job2 = SchedulerJob(state=0, batchid=4, userid=self.user)
    job2.save()

    assert self.job.state == 0
    assert job2.state == 0
    assert self.tracker.kill_job("{} {}".format(self.user.id, self.job.id)) == 0

    job = SchedulerJob.objects.get(id=self.job.id)
    job2 = SchedulerJob.objects.get(id=job2.id)

    assert job.state == 4
    assert job2.state == 0
