def pytest_sessionstart(*args):
  import os
  import django

  from django.test.runner import setup_test_environment
  from django.db import connection

  os.environ.setdefault("DJANGO_SETTINGS_MODULE", "charmming.settings")

  django.setup()
  setup_test_environment()
  connection.creation.create_test_db(1, True)

def pytest_sessionfinish(*args):
  from django.db import connection
  from django.test.runner import teardown_test_environment

  # Don't clobber the test case lines
  print("\n")
  connection.creation.destroy_test_db()
  teardown_test_environment()
