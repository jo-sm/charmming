#!/usr/bin/env python3

import os
import subprocess
import json

tables_raw = open('tables.json')
tables = json.load(tables_raw)

copied = {}

env = os.environ.copy()
env['PGPASSWORD'] = '9f89f5ec66778f9c468a775f'

def mysql_str(s):
 return ['mysql', '-u', 'root', '-D', 'prodcharmming', '-e', s]

def postgresql_str(s):
 return ['psql', '-t', '-U', 'postgres', '-h', 'localhost', '-d', 'charmming', '-c', s]

def test_postgres_container():
  output = subprocess.run(["docker", "inspect", "-f", "{{.State.Running}}", "postgres"], env=env, stdout=subprocess.PIPE)

  stdout = bytes.decode(output.stdout).strip()

  if stdout == 'true':
    return True
  else:
    return False

# Ensure that the docker container is running first
if not test_postgres_container():
  print("Error: Postgres container is not running. Please start postgres container first.")
  exit(1)

print('Please wait... Exporting from MySQL and exporting to PostgreSQL')


for table in tables:
  if not table['table_name']['mysql']:
    break

  file_name = os.path.join(os.path.dirname(os.path.abspath(__file__)), "{}.csv".format(table['table_name']['mysql']))

  columns_raw = table['columns']

  if type(columns_raw) is list:
    columns_mysql = columns_postgresql = columns_raw
  elif type(columns_raw) is dict:
    columns_mysql = columns_raw['mysql']
    columns_postgresql = columns_raw['postgresql']

  columns_mysql = ','.join("`{}`".format(c) for c in columns_mysql)
  columns_postgresql = ','.join('"{}"'.format(c) for c in columns_postgresql)

  # Export tables from MySQL

  print("Exporting {} from MySQL database...".format(table['table_name']['mysql']))
  subprocess.run(mysql_str("select {} from {} into outfile '{}' CHARACTER SET utf8 fields terminated by ',' enclosed by '\"' lines terminated by '\n';".format(columns_mysql, table['table_name']['mysql'], file_name)))

  # Truncate table and import into postgres
  csv_file = open(file_name)
  env = os.environ.copy()
  env['PGPASSWORD'] = '9f89f5ec66778f9c468a775f'

  print('Importing {} into PostgreSQL database...'.format(table['table_name']['postgresql']))
  out = subprocess.run(postgresql_str("SET client_min_messages TO WARNING; truncate table {0} cascade; copy {0} ({1}) from STDIN with (delimiter ',', format csv, null '\\N');".format(table['table_name']['postgresql'], columns_postgresql)), env=env, stdin=csv_file, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

  if out.stderr:
    print("Error: An error occurred during import. Double check the table structure and constaints.")
    print(bytes.decode(out.stderr))
    copied[table['table_name']['postgresql']] = -1

  if out.stdout and 'COPY ' in str(out.stdout):
    copied[table['table_name']['postgresql']] = int(bytes.decode(out.stdout, 'utf-8').split('COPY ')[1].split('\n')[0])

  id_column = table.get('id_column') or 'id'
  out = subprocess.run(postgresql_str('SELECT {0} FROM {1} ORDER BY {0} DESC LIMIT 1'.format(id_column, table['table_name']['postgresql'])), env=env, stdout=subprocess.PIPE)

  if out.stdout:
    raw = out.stdout.strip()

    if not raw:
      raw = "0"

    num_items = int(raw) + 1
  else:
    print('Warning: Could not determine number of items in {}'.format(table['table_name']['postgresql']))

  # Set the sequence of the table to the current copied value
  # Sequence must be greater than 0
  if copied[table['table_name']['postgresql']] > 0:
    subprocess.run(postgresql_str("ALTER SEQUENCE IF EXISTS {}_id_seq RESTART WITH {}".format(table['table_name']['postgresql'], num_items)), env=env, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

  os.remove(file_name)

for table, num_rows in list(copied.items()):
  out = subprocess.run(postgresql_str("SELECT count(*) from {}".format(table)), env=env, stdout=subprocess.PIPE)

  count = int(str.strip(bytes.decode(out.stdout, 'utf-8')))

  if count != num_rows:
    print('{} count is not correct. Expected {}, actual {}.'.format(table, num_rows, count))

print('Finishing migrating.')
