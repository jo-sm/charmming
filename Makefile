# CHARMMing Makefile

pip: requirements.txt
	pip install -r requirements.txt

npm: package.json
	npm i

run-tests: export PYTHONPATH := $(shell python -c "import os; print(os.getcwd())"):$(PYTHONPATH)
run-tests: export CHARMMING_ENV=test
run-tests: export CHARMMING_SECRET_KEY=test
run-tests: export PYTHONDONTWRITEBYTECODE=1
run-tests: pip
	# Test Python modules
	py.test --cov=charmming --cov=scheduler --cov-report html:coverage --cov-report term tests/

run-tests-debug: export PYTHONPATH := $(shell python -c "import os; print(os.getcwd())"):$(PYTHONPATH)
run-tests-debug: export CHARMMING_ENV=test
run-tests-debug: export CHARMMING_SECRET_KEY=test
run-tests-debug: export PYTHONDONTWRITEBYTECODE=1
run-tests-debug: pip
	py.test -s -rw --cov=charmming --cov=scheduler --cov-report html:coverage --cov-report term tests/

css: npm
	node -e "var build = require('./build'); build.createDirs(); build.buildCss();"