#!/usr/bin/env bash

source utils.sh
source constants.sh

end() {
  if [ ! -z ${watch_pid+x} ]; then
    echo "Killing watcher and hotswap daemon..."
    kill $watch_pid 2>/dev/null
  fi

  docker-compose down

  exit 0
}

trap end SIGINT SIGTERM

for arg in "$@"; do
  [ "$arg" == "--skip-npm" ] && SKIP_NPM=1 # Skip NPM installation (for offline mode)
done

# Start docker-compose with the development compose config for webapp
docker-compose -f docker-compose.yml -f webapp-dev.yml up -d

# Start the watcher
node watcher.js &

watch_pid=$!

docker-compose logs --follow webapp
